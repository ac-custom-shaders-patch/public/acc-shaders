A bunch of batch scripts for compiling shaders.

* First, run “install.bat” to install missing NPM packages.
* Edit “vars.bat” and update path to AC root directory (and to FXC compilers) if needed. I keep AC in Games/AssettoCorsa and use a symlink to redirect Steam there for an easier access, but your situation may be different.
* Remove “AssettoCorsa/extension/shaders.zip” and run “compile-all.bat” to recompile all shaders, so that CSP would use unpacked version (might take around to 15 minutes).
* After that, edit shaders and run “compile-changed.bat” to recompile changed ones.
