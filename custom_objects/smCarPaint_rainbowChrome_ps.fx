#define EXTRA_CARPAINT_FIELDS_4 \
  float extRainbowChromeK;\
  float extRainbowChromeMult;\
  float extRainbowMask;
#define NO_RAINBOW
#define HAS_RAINBOW_MASK
#define RAINBOW_REFL_COLOR
#include "smCarPaint_rainbow_ps.fx"