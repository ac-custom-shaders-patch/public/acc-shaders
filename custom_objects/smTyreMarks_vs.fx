// #define SPS_NO_POSC
#define SUPPORTS_COLORFUL_AO
#define SUPPORTS_DISTANT_FIX
#define RAINFX_STATIC_OBJECT
#define AO_MIX_INPUT 0
#ifdef ALLOW_RAINFX_
  #define ALLOW_RAINFX
#endif
// #define ALTER_POSV(X) X *= 0.9;
// #define ALTER_POSV(X) float _vxl = length(X); X *= (_vxl - min(_vxl * 0.1, 5)) / _vxl;
#include "include_new/base/_include_vs.fx"
#include "include/samplers.hlsl"

Texture2D<float> _txAreaDepth : register(t6);
cbuffer _cbVPSBuffer : register(b11) {
  float4x4 gAreaHeightmapTransform;
}

struct PS_IN_PerPixel_TM {
  STRUCT_POSH_MODIFIER float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  CUSTOM_STRUCT_FIELDS
  MOTION_BUFFER
  OPT_STRUCT_FIELD_FOG(float Fog : TEXCOORD6)
  float GroundOffset : TEXCOORD60;
  // SHADOWS_COORDS_ITEMS
  AO_COLORFUL
  RAINFX_REGISTERS
  CUSTOM_MODE_REGISTERS
};

PS_IN_PerPixel_TM main(VS_IN vin SPS_VS_ARG) {
  float groundOffset = 0;
  [branch]
  if (gAreaHeightmapTransform[0][0]) {
    float4 areaUV = mul(vin.PosL, gAreaHeightmapTransform);
    if (all(abs(areaUV.xyz - 0.5) < 0.5)) {
      float depth = _txAreaDepth.SampleLevel(samLinearClamp, areaUV.xy, 0);
      float distanceToGround = (depth - areaUV.z) * 300;
      if (distanceToGround > 0 && distanceToGround < 0.05) {
        // vin.PosL.y -= distanceToGround;
        groundOffset = distanceToGround;
      }
    }
  }

  GENERIC_PIECE_NOSHADOWS(PS_IN_PerPixel_TM);
  VERTEX_POSTPROCESS(vout);
  vout.GroundOffset = groundOffset;
  SPS_RET(vout);
}
