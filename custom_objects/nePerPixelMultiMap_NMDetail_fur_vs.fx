#define SUPPORTS_NORMALS_AO

#define CUSTOM_STRUCT_FIELDS\
  float Depth : DEPTH;\
  float MatExtra : DEPTH1;

#include "include_new/base/_include_vs.fx"

#ifdef ALLOW_FUR
  cbuffer cbMaterialVS : register(b6) {
    float furScale; /* = 0.0025 */
    float furFacingMult; /* = 0.2 */
    float furVerticalOffset; /* = -0.7 */
    float furFidelityScale; /* = 2.7 */
    float furThresholdExp; /* = 0.8 */
    float furThresholdMult; /* = 0.8 */
    float furOcclusion; /* = 0.5 */
    float furRimExp;
    float furAmbient; /* = 1.0 */
    float furDiffuse; /* = 0.5 */
    float furWorldUvScale;
  }

  cbuffer _cbFurVS : register(b10) {
    float furInstanceMult;
  }
#endif

#define PixelScale (max(extScreenSize.z, extScreenSize.w) / 4)

PS_IN_Nm main(VS_IN vin, uint instanceBaseID : SV_InstanceID SPS_VS_ARG_CD) {
  PS_IN_Nm vout;

  uint instanceID = 0;
  #ifdef ALLOW_FUR
    #ifdef USE_SPS
      vout.Depth = (float)(instanceBaseID / 2) * furInstanceMult;
      instanceID = instanceBaseID % 2;
    #else
      vout.Depth = (float)instanceBaseID * furInstanceMult;
    #endif
  #else
    vout.Depth = 0; // TODO: remove
  #endif

  vout.NormalW = normals(vin.NormalL);
  vout.MatExtra = 1 - vsLoadMat3f(vin.TangentPacked);

  float4 posW = mul(vin.PosL, ksWorld);
  APPLY_CFX(posW);
  
  #ifdef ALLOW_FUR
    float displacementScale = furScale;
    float pixelRadius = mul(mul(posW, ksView), ksProjection).w * PixelScale / extCameraTangent;
    displacementScale = min(displacementScale, pixelRadius * 10 / furInstanceMult);
    displacementScale = max(displacementScale, furScale * 0.1);

    float grazing = abs(dot(vout.NormalW, normalize(posW.xyz - ksCameraPosition.xyz)));
    displacementScale *= lerp(1, furFacingMult, grazing);
    float3 displacementOffset = normalize(vout.NormalW + float3(0, furVerticalOffset, 0)) * displacementScale * vout.Depth;

    posW.xyz += displacementOffset;
  #endif

  float4 posV = mul(posW, ksView);
  vout.PosH = mul(posV, ksProjection);

  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);

  #ifdef CREATE_MOTION_BUFFER
    vout.PosCS0 = vout.PosH.xyw;
    vout.PosCS1 = getMotionWorld(float4(mul(vin.PosL, ksWorldPrev).xyz + displacementOffset, 1), vout.PosH).xyw;
  #endif

  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
