#define SUPPORTS_NORMALS_AO
#include "common/bending.hlsl"
#include "common/uv2Utils.hlsl"
#include "include_new/base/_include_vs.fx"

BENDING_FX_IMPL

GENERIC_RETURN_TYPE(PS_IN_Nm) main(VS_IN vin SPS_VS_ARG) {
  PS_IN_Nm vout;
  float4 posW, posWnoflex, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_UV2;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(GENERIC_PIECE_RETURN(vout));
}
