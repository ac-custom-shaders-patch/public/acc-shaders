#define USE_BRAKE_DISC_MODE
#define USE_BLURRED_NM
#define INCLUDE_TYRE_CB
#define CARPAINT_NM
#define DIM_REFLECTTIONS_WITH_SAMPLE_PARAM
#define CALCULATELIGHTING_USETXSPECULAR_AS_REFLECTANCE_MODEL
#define SSLR_FORCE 1
#define IS_ADDITIVE_VAR 0
#define NO_ADJUSTING_COLOR

#ifndef MODE_KUNOS
  #define ALLOW_BRAKEDISCFX
#endif

#include "include_new/base/_include_ps.fx"
#include "common/brakeDiscFX.hlsl"

float3 getNormalW(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW, out float4 txNormalValue) {
  float3 T = normalize(tangentW);
  float3 B = normalize(bitangentW);
  float4 txNormalBaseValue = txNormal.Sample(samLinearSimple, uv);
  float4 txNormalBlurValue = txNormalBlur.Sample(samLinearSimple, uv);
  txNormalValue = lerp(txNormalBaseValue, txNormalBlurValue, blurLevel);
  float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
  float3x3 m = float3x3(T, normalW, B);
  return normalize(mul(transpose(m), txNormalAligned.xzy));
}

RESULT_TYPE main(PS_IN_BrakeDiscFX pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, 0);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txBlurValue = txBlur.Sample(samLinear, pin.Tex);
  txDiffuseValue = lerp(txDiffuseValue, txBlurValue, blurLevel);

  float4 txNormalValue;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, txNormalValue);

  // shadow = 1;
  // extraShadow = 1;

  float3 specularValue = 0;
  float reflectionOcclusion = 1;
  float3 resultGlow = 0;
  float baseWorn = saturate(extWorn);
  // baseWorn = 0.5 + 0.5 * sin(ksGameTime / 1000);
  // baseWorn = 0.5;
  // baseWorn = 1;
  // baseWorn = 0;
  // baseWorn = 0;
  float heatK = saturate(extGlowMult < 0 ? abs(extGlowMult) : glowLevel * extGlowMult);
  // heatK = 0.5 + 0.5 * sin(ksGameTime / 300);
  // heatK += 0.9;

  // float blurLevel = 0.5 + 0.5 * sin(ksGameTime / 60);

  float3 extWheelNormalW = pin.WheelNormalW;
  float3 extWheelUpW = pin.WheelUpW;
  float3 extWheelSideW = pin.WheelSideW;
  float rimMaskMult = saturate(dot(extWheelNormalW, normalize(pin.NormalW)));
  if (dot(pin.NormalW, extWheelNormalW) < 0) extWheelNormalW = -extWheelNormalW;

  float lfxMult = 1;
  #ifdef ALLOW_BRAKEDISCFX
    float3 posRel = SPSAwarePosC - pin.WheelCenterC;
    float3 reflDir;
    float brushedK, mapShading, cleanK, cleanK2, withinDisk, reflMask;
    brakeDiscFXCompute(posRel,
      extWheelNormalW, extWheelUpW, extWheelSideW, normalize(pin.NormalW), pin.Tex,
      baseWorn, heatK, rimMaskMult,
      txNormalValue, toCamera,
      txDiffuseValue, normalW, extraShadow, lfxMult,
      resultGlow, specularValue, reflectionOcclusion, reflDir,
      brushedK, mapShading, cleanK, cleanK2, withinDisk, reflMask);
  #else
    float brushedK = 0;
    float cleanK = 1;
    float cleanK2 = 1;
    float mapShading = 0;
    float withinDisk = 0;
    float reflMask = 1;
  #endif

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txSpecularValue = specularValue;
  L.specularValue = lerp(ksSpecular * txDiffuseValue.a, 1, brushedK * mapShading);
  float3 lighting = calculateLighting(L);
  L.txDiffuseValue.rgb *= lfxMult;
  LIGHTINGFX(lighting);

  normalW += extWheelUpW * ((cleanK - 0.5) * 0.05) * baseWorn * withinDisk;

  ReflParams R = getReflParamsBase();
  R.ksSpecularEXP = lerp(R.ksSpecularEXP, lerp(extEXPBase, extEXPWorn, lerp(0.3, 1, mapShading) * (1 - cleanK)), withinDisk);

  #ifdef ALLOW_BRAKEDISCFX
    if (extRimReflectionOffset && rimMaskMult > 0){
      // reflDir = normalize(reflect(toCamera, extWheelSideW));
      for (int i = 0; i < 3; ++i) {
        float3 posRefl = posRel + reflDir * (1 - (i / 2.) * extRimReflectionOffset / length(reflDir));
        float posY = -dot(posRefl, extWheelUpW);
        float posX = -dot(posRefl, normalize(cross(extWheelSideW, extWheelUpW)));
        float2 posUV = float2(posX, posY) * extRimReflectionTyreRadiusInv + 0.5;
        float mask = _txRimMask.SampleLevel(samLinearBorder0, posUV, 
          pow(lerpInvSat(R.ksSpecularEXP, 200, 10), 2) * 3.5 + blurLevel * 4);
        reflMask = min(reflMask, lerp(1, mask.r, rimMaskMult * GAMMA_OR(1, 0.9)));
      }
    }
  #endif

  R.useBias = true;
  R.finalMult = (0.5 + 0.5 * AO_REFLECTION) * lerp(0.7 + 0.3 * mapShading, 1, cleanK2) * reflMask;
  R.fresnelC = lerp(R.fresnelC, lerp(extF0Worn, extF0Base, cleanK2), withinDisk);
  R.fresnelMaxLevel = lerp(R.fresnelMaxLevel, 1, withinDisk);
  R.fresnelEXP = lerp(R.fresnelEXP, 5, withinDisk);
  R.reflectionSampleParam = reflectionOcclusion;

  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  // withReflection = normalW.y * 10 + 1;
  if (GAMMA_FIX_ACTIVE) {
    withReflection += pow(resultGlow, 2) * getEmissiveMult();
  } else {
    withReflection += resultGlow * getEmissiveMult();
  }
  R.resultBlur = max(R.resultBlur, lerp(4, 6, cleanK));
  // withReflection = cleanK;
  // RETURN(withReflection.rgb, 1);
  // withReflection.rgb = extAlphaAsMaskMul;
  // withReflection.rg = float3(blurLevel, 1 - blurLevel, 0).rg;
  // withReflection.rgb = reflMask;

  #ifdef ALLOW_BRAKEDISCFX_
    if (extRimReflectionOffset && rimMaskMult > 0){
      float3 posRefl = posRel + reflDir / abs(dot(reflDir, extWheelSideW)) * extRimReflectionOffset;
      float posY = -dot(posRefl, extWheelUpW);
      float posX = -dot(posRefl, normalize(cross(extWheelSideW, extWheelUpW)));
      float2 posUV = float2(posX, posY) * extRimReflectionTyreRadiusInv + 0.5;
      float mask = _txRimMask.SampleLevel(samLinearBorder0, posUV, 0);
      // withReflection.rg = float3(mask.r, 1 - mask.r, 0).rg;
      // withReflection.rg = posUV;
    }
  #endif

  RETURN(withReflection.rgb, 1);
}
