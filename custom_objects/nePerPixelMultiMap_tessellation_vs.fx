#define SUPPORTS_NORMALS_AO

#ifdef BENDING_VARIANT
  #include "common/bending.hlsl"
  #define gMinTessDistance gMinTessDistance
  #define gMaxTessDistance gMaxTessDistance
  #define gMinTessFactor gMinTessFactor
  #define gMaxTessFactor gMaxTessFactor
#else
  #include "common/tessellationParallaxShared.hlsl"
  #define gMinTessDistance extMinDisplacementDistance
  #define gMaxTessDistance extMaxDisplacementDistance
  #define gMinTessFactor extMinTesselationFactor
  #define gMaxTessFactor extMaxTesselationFactor
#endif

#include "include_new/base/_include_vs.fx"
#include "common/tessellation.hlsl"

VS_OUT main(VS_IN vin) {
  VS_OUT vout;

  float distanceToCamera;
  {
    #ifdef BENDING_VARIANT
      // Storing PosL in vout.PosC because bending points are in local mesh coordinates and actual bending
      // should happen post-tessellation
      float4 posW, posWnoflex, posV;
      toScreenSpace(vin.PosL, posW, posWnoflex, posV);
      distanceToCamera = length(posW.xyz - ksCameraPosition.xyz);
      vout.PosC = vin.PosL.xyz;
    #else
      // Storing PosW in vout.PosC because why not
      float4 posW, posWnoflex, posV;
      toScreenSpace(vin.PosL, posW, posWnoflex, posV);
      vout.PosC = posW.xyz;
      distanceToCamera = length(vout.PosC - ksCameraPosition.xyz);
    #endif
  }
  
  vout.Tex = vin.Tex;
  #ifndef MODE_SHADOWS_ADVANCED
    vout.NormalW = normals(vin.NormalL);
    PREPARE_TANGENT;
    PREPARE_AO(vout.Ao);
  #endif

  float d = distanceToCamera * extLODDistanceMult;
  float tess = saturate((gMinTessDistance - d) / max(1, gMinTessDistance - gMaxTessDistance));
  tess *= tess;
  vout.TessFactor = max(1, gMinTessFactor + tess * max(0, gMaxTessFactor - gMinTessFactor));

  return vout;
}
