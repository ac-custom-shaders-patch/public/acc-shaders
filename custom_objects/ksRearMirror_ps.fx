#define NO_CARPAINT
#define NO_SSAO
#define NO_PERVERTEX_AO

#define CB_MATERIAL_EXTRA_3\
	float extUseAltTexture;\
	float extShake;

#include "common/rainWindscreen.hlsl"
#include "include_new/base/_include_ps.fx"
#include "common/rainWindscreen_impl.hlsl"

Texture2D _txDiffuseAlt : register(TX_SLOT_MAT_6);

float3 sampleMirrorRefl(PS_IN_PerPixelCustom pin) {	
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);

	[branch]
	if (extShake){
		[unroll]
		for (int i = -4; i <= 4; ++i){
			if (!i) continue;
			txDiffuseValue += txDiffuse.SampleLevel(samLinearSimple, pin.Tex + float2(0, (float)i * extShake * 2), 0);
		}
		txDiffuseValue.rgb /= 9;
	}

  #ifndef MODE_KUNOS
    if (extUseAltTexture && pin.PosH.x < extAltCameraThreshold){
      txDiffuseValue = _txDiffuseAlt.Sample(samLinear, pin.Tex);
    }
  #endif
	
  return txDiffuseValue.xyz;
}

RESULT_TYPE main(PS_IN_PerPixelCustom pin) {
  READ_VECTORS
	pin.NormalW = normalW;
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);

	#if defined(ALLOW_RAINFX) && defined(APPLY_RAIN) && !defined(MODE_GBUFFER) && !defined(MODE_GBUFFER) && defined(ALLOW_EXTRA_VISUAL_EFFECTS)
		WindscreenRainData WR = getWindscreenRainData(pin.TexAlt, normalize(pin.NormalCar), pin.NormalW, toCamera, float3(0, 1, 0));
    float fresnel = saturate(pow(1 + dot(toCamera, WR.normal), 2));
    float3 refraction = lerp(toCamera, -WR.normal, 0.5);
    float3 reflection = reflect(toCamera, WR.normal);

    // float3 refractionColor = txDiffuse.SampleBias(samLinearClamp, pin.Tex + WR.fakeRefraction * float2(-0.05, 0.2), 0).rgb;
    float3 reflectionColor = sampleEnv(reflection, 3, 0);
    // lighting = lerp(lighting, lerp(refractionColor, reflectionColor, fresnel), saturate(WR.water * 2));

		pin.Tex += WR.fakeRefraction * float2(-0.05, 0.2) * saturate(WR.water);
		float3 lighting = sampleMirrorRefl(pin);
		if (!WINDSCREEN_FX_FLAG_WORKING_MIRRORS) {
			lighting = SAMPLE_REFLECTION_FN(reflect(toCamera, pin.NormalW), 0, false, REFL_SAMPLE_PARAM_DEFAULT);
		}
		lighting = lerp(lighting, reflectionColor, saturate(WR.water * 2) * fresnel);

		float shapeMult = lerp(1, saturate(dot(WR.normal, normalW) + (dot(WR.normal, toCamera) + 0.85) * 0.85), saturate(WR.water * 2));
		float3 rainNormal = WR.normal;
		#ifdef ALLOW_RAINFX
			RainParams RP = (RainParams)0;
		#endif
		normalW = -WR.normal;

		LightingParams L = getLightingParams(pin.PosC, toCamera, WR.normal, 0, shadow, 1);
		L.txDiffuseValue = 0;
		L.txSpecularValue = saturate(WR.water * 2);
		L.txEmissiveValue = 0;
		L.specularExp = 8;
		L.specularValue = 2;
  	LIGHTINGFX(lighting);

		lighting *= shapeMult;
		// lighting = -dot(pin.NormalW, toCamera);
		// lighting = dot(normalW, toCamera);
		// lighting = _txRainWater.Sample(samLinear, pin.TexAlt).z;
    // lighting = lerp(lighting, sampleEnv(toCamera, 3, 0), WR.condensation); // N/A for exterior shots
		if (WR.debugMode) {
			lighting = WR.debugColor;
		}
	#else
		float3 lighting = sampleMirrorRefl(pin);
	#endif

	float4 txDiffuseValue = 0;
  RETURN_BASE(lighting, 1);
}
