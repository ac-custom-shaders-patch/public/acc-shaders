#define CARPAINT_SIMPLE
#define CALCULATELIGHTING_SPECIAL_ALPHA
#define NO_CARPAINT
#define SUPPORTS_AO
#define INPUT_EMISSIVE3 float3(0,0,0)
#define CB_MATERIAL_EXTRA_0 float alpha; float extOil;
#define LIGHTINGFX_KSDIFFUSE 0
#define RAINFX_FORCE_ROUGH
#define RAINFX_STATIC_OBJECT
#define RAINFX_REGULAR_REFLECTIONS
#define GBUFF_MASKING_MODE true
#define seasonWinter 0
#define seasonAutumn 0
#define IS_ADDITIVE_VAR 0
// #define INPUT_DIFFUSE_K 0.05
// #define INPUT_AMBIENT_K 0.05
#ifdef ALLOW_RAINFX_
  #define ALLOW_RAINFX
#endif
#include "include_new/base/_include_ps.fx"

Texture2D<float> txOilMask : register(TX_SLOT_MAT_1);
Texture2D<float> txOilPuddles : register(TX_SLOT_MAT_2);

#ifdef ALLOW_EXTRA_VISUAL_EFFECTS
  float4 sampleNoise(float2 uv){
    float textureResolution = 32;
    uv = uv * textureResolution + 0.5;
    float2 i = floor(uv);
    float2 f = frac(uv);
    uv = i + f * f * (3 - 2 * f);
    uv = (uv - 0.5) / textureResolution;
    return txNoise.SampleLevel(samLinearSimple, uv, 0);
  }

  #include "include/poisson.hlsl"
  float sampleNoiseBlurred(float2 uv){
    POISSON_AVG(float, result, txOilPuddles, samLinearSimple, uv, 1./1024, 8);
    return result;
  }
#endif

struct PS_IN_PerPixel_TM {
  STRUCT_POSH_MODIFIER float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  CUSTOM_STRUCT_FIELDS
  MOTION_BUFFER
  OPT_STRUCT_FIELD_FOG(float Fog : TEXCOORD6)
  float GroundOffset : TEXCOORD60;
  // SHADOWS_COORDS_ITEMS
  AO_COLORFUL
  RAINFX_REGISTERS
  CUSTOM_MODE_REGISTERS
};

RESULT_TYPE main(PS_IN_PerPixel_TM pin) {
  READ_VECTORS
  pin.PosC += toCamera * (pin.GroundOffset / max(0.05, -toCamera.y));
  #ifdef NO_SHADOWS
    float shadow = 1;
  #else
    float3 posCShifted = pin.PosC; // - ksLightDirection.xyz * (pin.GroundOffset / max(0.0001, -ksLightDirection.y));
    float4 shadowCoords = mul(float4(posCShifted + ksRealCameraPosition, 1), ksShadowMatrix0);
    float shadow = getShadow(posCShifted, pin.PosH, normalW, shadowCoords, AO_FALLBACK_SHADOW);
    // shadow = pow(saturate(shadow), 200);
  #endif
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  txDiffuseValue.rgb *= GAMMA_OR(0.6, 1);
  // float alpha = 1;
  float retAlpha = txDiffuseValue.a * alpha;
  // retAlpha = 1;
  // txDiffuseValue.r = 2;

  RAINFX_WET(txDiffuseValue.xyz);
  #if defined(ALLOW_RAINFX) 
    txDiffuseValue *= 1 - RP.damp * 0.7;
    retAlpha *= 1 - RP.damp * 0.9;
  #endif

  // txDiffuseValue.rgb = float3(RP.damp, 1 - RP.damp, 0);
  // txDiffuseValue.xyz = lerp(luminance(txDiffuseValue.xyz), txDiffuseValue.xyz, 0);
  // txDiffuseValue.xyz = 0.5;
  if (GAMMA_ACTUAL_VALUE) {
    // txDiffuseValue.xyz = 0;
  }

  // float extOil = 1;
  clip(extOil ? 1 : retAlpha - 0.01);  
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);

  float density = saturate(retAlpha * 2 - 0.1);
  L.specularValue *= lerp(1, 2, density);
  L.specularExp *= lerp(1, 2, density);

  if (GAMMA_ACTUAL_VALUE) {
    L.specularValue *= 0.2;
  }

  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  
  // ReflParams R = (ReflParams)0;
  // R.fresnelMaxLevel = 0.02;
  // R.fresnelC = 0;
  // R.fresnelEXP = 30;
  // R.ksSpecularEXP = 20;
  // R.finalMult = 0.4;
  // R.metallicFix = 1;
  // R.forceReflectedDir = true;
  // R.useSkyColor = true;
  // R.forcedReflectedDir = lerp(normalW, GET_REFL_DIR(toCamera, normalW), 0.6);
  // lighting = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);

  #ifdef ALLOW_EXTRA_VISUAL_EFFECTS
    [branch]
    if (extOil){    
      float alphaMult = 1 / max(0.5, retAlpha);
      float3 posW = ksInPositionWorld;
      float4 noise1 = sampleNoise(posW.xz * 0.05);
      float3 reflDir = lerp(reflect(toCamera, normalW), normalW, 0);
      float oil = txOilMask.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw, 0) + txOilPuddles.Sample(samLinearSimple, posW.xz * 0.2) * 0.;
      
      // oil = txNoise.SampleLevel(samLinearSimple, posW.xz * 0.1, 0).r;

      float oilBaseAlpha = extOil * saturate(oil * 2.5 - 0.2) * clamp(txDiffuseValue.a, 0, 0.5);
      if (oilBaseAlpha > 0) {
        oilBaseAlpha = GAMMA_ALPHA(oilBaseAlpha);
        retAlpha = max(retAlpha, oilBaseAlpha);
        
        // float ndv = abs(dot(normalW, toCamera));
        // float rainbowOffset = posW.y * 5 + ksGameTime * 0.00003 + ndv + atan2(toCamera.x, toCamera.z) / M_PI;
        float3 rainbow0 = rainbow(frac(oil * 9));
        float3 rainbow1 = rainbow(frac(oil * 5));
        lighting += GAMMA_LINEAR(lerp(0.5, 1 - lerp(rainbow0, rainbow1, noise1.y), saturate(oil * 2.5 - 0.4)))
          * oilBaseAlpha
          * SAMPLE_REFLECTION_FN(reflDir, 5, false, REFL_SAMPLE_PARAM_DEFAULT) / retAlpha;
      }
    }
  #endif

  // #ifdef RAINFX_USE_PUDDLES_MASK
  //   if (SNOW_MODE) {
  //     retAlpha *= 0.25;
  //   }
  // #endif
  
  retAlpha = GAMMA_ALPHA(retAlpha);
  RETURN_BASE(lighting, retAlpha);
}
