#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#define GBUFF_NORMALMAPS_DISTANCE 5
#define GETNORMALW_NORMALIZED_TB
#define CARPAINT_NMDETAILS
#define CARPAINT_AT
#define nmObjectSpace 0
#define CB_MATERIAL_EXTRA_3 \
  float emAlphaFromDiffuse;
#define GAMMA_TWEAKABLE_ALPHA

#define EXTRA_CARPAINT_FIELDS\
  uint extStickersMode_uint;

#include "common/uv2Utils.hlsl"
#include "include_new/base/_include_ps.fx"
// alias: nePerPixelMultiMap_wiper_ps

Texture2D txStickers : register(TX_SLOT_MAT_5);
Texture2D txStickersMaps : register(TX_SLOT_MAT_6);

RESULT_TYPE main(GENERIC_INPUT_TYPE(PS_IN_Nm)) {
  GENERIC_PIECE_INPUT(PS_IN_Nm);
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);

  [branch]
  if (extStickersMode_uint){
    float4 txStickerValue = txStickers.Sample(samLinear, float2(pin.Tex2X, pin.Tex2Y));
    txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, txStickerValue.rgb, txStickerValue.a);
    if (extStickersMode_uint & 2) {
      txDiffuseValue.a = lerp(txDiffuseValue.a, 1, txStickerValue.a);
    }
    if (extStickersMode_uint & 4) {
      float4 txStickerMapsValue = txStickersMaps.Sample(samLinear, float2(pin.Tex2X, pin.Tex2Y));
      txMapsValue.rgb = lerp(txMapsValue.rgb, txStickerMapsValue.rgb, txStickerMapsValue.a);
    }
  }

  RAINFX_WET(txDiffuseValue.xyz);

  float alpha, directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha, directedMult);
  considerNmDetails(pin.Tex, tangentW, bitangentW, txDiffuseValue, txMapsValue.xyz, normalW, directedMult);
  // APPLY_VRS_FIX;

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING, 1);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = !(emAlphaFromDiffuse < 0);
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);

  if (abs(emAlphaFromDiffuse) == 1) alpha = txDiffuseValue.a;
  float4 withReflection = calculateReflection(float4(lighting, alpha), toCamera, pin.PosC, normalW, R);

  RAINFX_WATER(withReflection);
  RETURN(withReflection, emAlphaFromDiffuse == 2 ? 1 : emAlphaFromDiffuse < 0 ? withReflection.a : alpha);
}
