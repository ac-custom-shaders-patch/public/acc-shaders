#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO

#include "include_new/base/_include_ps.fx"
#include "common/heating.hlsl"

RESULT_TYPE main(PS_IN_NmPosL pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);

  float directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, directedMult);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);
  // APPLY_VRS_FIX;

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING, 1);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.txEmissiveValue = 0;
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  HEATINGFX(withReflection); 
  RETURN(withReflection, txMapsValue.a);
}
