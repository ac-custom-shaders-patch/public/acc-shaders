#define SET_AO_TO_ONE

#include "include_new/base/_flags.fx"
#if defined(EMISSIVE_MAPPING)
  #if defined(MODE_GBUFFER) || defined(ALLOW_EXTRA_VISUAL_EFFECTS)
    #define CUSTOM_STRUCT_FIELDS\
      float3 PosL : TEXCOORD20;\
      float3 NormalL : TEXCOORD21;\
      float4 GlExtra : TEXCOORD22;
  #else
    #define CUSTOM_STRUCT_FIELDS\
      float4 GlExtra : TEXCOORD22;
  #endif
#elif defined(MODE_GBUFFER) || defined(ALLOW_EXTRA_VISUAL_EFFECTS)
  #define CUSTOM_STRUCT_FIELDS\
    float3 PosL : TEXCOORD20;\
    float3 NormalL : TEXCOORD21;
#endif

#if defined(EMISSIVE_MAPPING)
  #include "emissiveMapping.hlsl"
#else
  #include "include_new/base/_include_vs.fx"
#endif

PS_IN_Nm main(VS_IN vin SPS_VS_ARG) {
  GENERIC_PIECE(PS_IN_Nm);
  PREPARE_TANGENT;
  #if defined(MODE_GBUFFER) || defined(ALLOW_EXTRA_VISUAL_EFFECTS)
    vout.PosL = vin.PosL.xyz;
    vout.NormalL = vin.NormalL.xyz;
  #endif
  #ifdef EMISSIVE_MAPPING
    vout.GlExtra = getExtraValue(vin.PosL.xyz);
  #endif
  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
