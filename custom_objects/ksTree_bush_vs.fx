#define SUPPORTS_COLORFUL_AO
#define SUPPORTS_DISTANT_FIX

#define CUSTOM_STRUCT_FIELDS\
  float ShadowMult : SHADOW1;

#include "include_new/base/_include_vs.fx"
#include "include_new/ext_functions/wind.fx"

PS_IN_PerPixel main(VS_IN vin SPS_VS_ARG) {
  float windStrength = vsLoadHeating(vin.TangentPacked);
  float4 posL = vin.PosL;
  vin.PosL.xz += windOffset(vin.PosL.xyz, windStrength, 1) * 2;
  // vin.NormalL.xz += windOffset(vin.PosL.xyz, windStrength, 1.3) * length(vin.NormalL);
  GENERIC_PIECE(PS_IN_PerPixel);

  float byteX = BYTE0(asuint(vin.TangentPacked.y));
  float byteZ = BYTE1(asuint(vin.TangentPacked.y));
  vout.ShadowMult = 1;
  if (byteX && byteZ) {
    float3 landscapeNormal = float3(byteX / 127. - 1, 0, byteZ / 127. - 1);
    landscapeNormal.xz = clamp(landscapeNormal.xz, -1, 1);
    landscapeNormal.y = sqrt(saturate(1 - dot2(landscapeNormal.xz)));
    landscapeNormal = normalize(landscapeNormal);
    vout.ShadowMult *= saturate(dot(landscapeNormal, -ksLightDirection.xyz) * 4);
  }

  // vout.NormalW = normalize(vout.NormalW);
  posL.xz += windOffsetPrev(posL.xyz, windStrength, 1) * 2;
  GENERIC_PIECE_MOTION(posL);

  // vout.Wind = windStrength;
  SPS_RET(vout);
}
