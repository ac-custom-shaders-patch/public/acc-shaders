#define NO_CARPAINT
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"
#include "common/heating.hlsl"

RESULT_TYPE main(PS_IN_PerPixelPosL pin) {
  READ_VECTORS

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);  
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txEmissiveValue = 0;
  APPLY_CAO;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  HEATINGFX(lighting);
  RETURN_BASE(lighting, txDiffuseValue.a);
}
