#define CARPAINT_SIMPLE
#define SUPPORTS_AO

#define INPUT_DIFFUSE_K 0.01
#define INPUT_AMBIENT_K 0.01
#define INPUT_SPECULAR_K 0.2
#define INPUT_SPECULAR_EXP 40
#define STENCIL_VALUE 1
#define TXEMISSIVE_PASSTHROUGH HAS_FLAG(FLAG_MATERIAL_1)

#include "include_new/base/_include_ps.fx"
#include "common/digitalScreen.hlsl"

Texture2D _txDiffuseAlt : register(TX_SLOT_MAT_6);

RESULT_TYPE main(PS_IN_SmDigitalScreen pin) {
  READ_VECTORS
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = 1;
  float lcdDistanceKMult = 1;
  RAINFX_INIT;

  #ifdef RAINFX_USE_DROPS
    float2 dtex = float2(ddx(pin.Tex.x), ddx(pin.Tex.y));
    float2 dtey = float2(ddy(pin.Tex.x), ddy(pin.Tex.y)) * extScreenSize.y / extScreenSize.x;
    float tiltX = dot(RP.normal, normalize(cross(toCamera, extDirUp))) * RP.alpha;
    float tiltY = dot(RP.normal, extDirUp) * RP.alpha;
    pin.Tex += dtex * tiltX * -5;
    pin.Tex += dtey * tiltY * RP.alpha * 5;
    lcdDistanceKMult = saturate(1 - dot2(float2(tiltX, tiltY)) * saturate(-dot(RP.normal, toCamera) * 3));
  #endif

  DigitalScreenData D = digitalScreenInit(pin.Tex, pin.DigitalScreenPos, lcdDistanceKMult);
  txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);

  if (smMirrorMode) {
    // txDiffuseValue.rgb = pow(txDiffuseValue.rgb, 0.4545);
  }

  #ifndef MODE_KUNOS
    // if (smMirrorMode && pin.PosH.x < extAltCameraThreshold){
    //   txDiffuseValue = _txDiffuseAlt.Sample(samLinear, pin.Tex);
    // }
  #endif

  float3 screenFix = digitalScreenCalculate(D, pin.PosC + ksCameraPosition.xyz, 
    pin.Tex, normalW, normalize(pin.BitangentW), toCamera, txDiffuseValue);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, 1, shadow, extraShadow.y * AO_LIGHTING);
  L.txEmissiveValue = screenFix;
  APPLY_CAO;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  ReflParams R = getReflParamsBase(EXTRA_SHADOW_REFLECTION * AO_REFLECTION);
  R.useBias = true;
  float4 withReflection = calculateReflection(float4(lighting, txDiffuseValue.a), toCamera, pin.PosC, normalW, R);
  // withReflection.rgb = txDiffuseValue.rgb;
  // withReflection.a = 1;

  RAINFX_WATER_ALPHA(withReflection.rgb, withReflection.a);
  RETURN(withReflection, withReflection.a);
}
