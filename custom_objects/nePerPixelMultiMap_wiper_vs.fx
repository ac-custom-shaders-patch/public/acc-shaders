#define SUPPORTS_NORMALS_AO
#include "common/uv2Utils.hlsl"
#include "include/samplers_vs.hlsl"
#include "include_new/base/_include_vs.fx"
#include "common/animatedWipers.hlsl"

GENERIC_RETURN_TYPE(PS_IN_Nm) main(VS_IN vin SPS_VS_ARG) {
  float3 originalPosL = vin.PosL.xyz;
  for (int i = 0; i < ENTRIES_COUNT; ++i){
    if (gWiperParams[i].calculate(vin.PosL.xyz, vin.NormalL.xyz, vsLoadMat0b(vin.TangentPacked), i, false)) break;
  }

  PS_IN_Nm vout;
  float4 posW, posWnoflex, posV;
  // if (vsLoadMat3b(vin.TangentPacked)) vin.PosL.y += 0.1;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_UV2;
  PREPARE_AO(vout.Ao);

  {
    vin.PosL.xyz = originalPosL;
    for (int i = 0; i < ENTRIES_COUNT; ++i){
      if (gWiperParams[i].calculate(vin.PosL.xyz, vin.NormalL.xyz, vsLoadMat0b(vin.TangentPacked), i, true)) break;
    }
    GENERIC_PIECE_MOTION(vin.PosL);
  }
  VERTEX_POSTPROCESS(vout);
  SPS_RET(GENERIC_PIECE_RETURN(vout));
}
