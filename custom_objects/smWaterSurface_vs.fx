#define SUPPORTS_NORMALS_AO
#define SUPPORTS_DISTANT_FIX

#define RAINFX_STATIC_OBJECT
#define RAINFX_REGULAR_REFLECTIONS
#define USE_PS_FOG

#define CB_MATERIAL_EXTRA_4\
  float useAlphaFromDiffuse;\
  float useAlphaFromNormal;\
  float unevenWaterFix;\
  float extDepthAware;\
  float extCausticsBrightness;\
  float extWaterOffset;

#define CUSTOM_MATERIAL_PROPERTIES
#define NO_CARPAINT

#include "include_new/base/_include_vs.fx"

PS_IN_Nm main(VS_IN vin SPS_VS_ARG) {
  PS_IN_Nm vout;
  float4 posW, posWnoflex, posV;
  vin.PosL.y += extWaterOffset * (extWaterOffset > 0 ? extSceneWater : 1 - extSceneWater);// * (0.8 + sin(extWindWave + vin.PosL.x / 1e3) * 0.2);
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
