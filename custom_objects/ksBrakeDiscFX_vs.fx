// I think, it’s safe to assume there is absolutely no need for AO for this shader
#define SET_AO_TO_ONE

#include "include_new/base/_include_vs.fx"
#include "common/brakeDiscFX.hlsl"

PS_IN_BrakeDiscFX main(VS_IN vin SPS_VS_ARG) {
  PS_IN_BrakeDiscFX vout;
  float4 posW, posWnoflex, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posWnoflex.xyz - ksCameraPosition.xyz; // using no-flex value so that other vectors would remain valid
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  vout.WheelCenterC = mul(float4(extWheelPos, 1), ksWorld).xyz - ksCameraPosition.xyz;
  vout.WheelNormalW = normalize(mul(extWheelNormal, (float3x3)ksWorld));
  vout.WheelSideW = normalize(mul(extWheelSide, (float3x3)ksWorld));
  vout.WheelUpW = normalize(mul(extWheelUp, (float3x3)ksWorld));
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  SPS_RET(vout);
}
