#define CUSTOM_STRUCT_FIELDS\
  float3 PosL : EXTRA0;\
  float3 PosL1 : EXTRA1;

#define CARPAINT_NM
#define GETNORMALW_SAMPLER samLinear
#define SUPPORTS_AO
#define SUPPORTS_DITHER_FADING

#ifndef NO_REFLECTIONS
	#define LIGHTINGFX_FUR
#endif

#ifdef USE_SHTOON
  #define SHTOON_HUMAN_SURFACE
  #include "shadingToon.hlsl"
#endif

#include "include_new/base/_include_ps.fx"
#include "common/humanSurface.hlsl"

float calculateEyeMask(PS_IN_Nm pin, EyeParams P, float eyeCorrection, 
    inout float2 uv, inout float3 normalW, inout float colorMult, inout float reflectionMult,
    inout float reflectionExtra, inout float3 specularExtra, inout float reflectionBoost){
  float2 eyeAreaHalfSize = P.area.zw / 2;
  float2 eyeAreaMiddle = P.area.xy + eyeAreaHalfSize;
  float2 relEyeBaseArea = (uv - eyeAreaMiddle) / eyeAreaHalfSize;

  float2 eyeUv = uv;
  eyeUv.y += P.tweakTwistAmount * saturate(relEyeBaseArea.y) * pow(saturate(remap(relEyeBaseArea.x, P.tweakTwistFrom, sign(P.tweakTwistFrom), 0, 1)), 2);  
  eyeUv.y += (P.tweakAOffsetPow1 + P.tweakASizePow1 * relEyeBaseArea.y) * saturate(relEyeBaseArea.x);
  eyeUv.y += (P.tweakAOffsetPow2 + P.tweakASizePow2 * relEyeBaseArea.y) * pow(saturate(relEyeBaseArea.x), 2);
  eyeUv.y += (P.tweakBOffsetPow1 + P.tweakBSizePow1 * relEyeBaseArea.y) * saturate(-relEyeBaseArea.x);
  eyeUv.y += (P.tweakBOffsetPow2 + P.tweakBSizePow2 * relEyeBaseArea.y) * pow(saturate(-relEyeBaseArea.x), 2);

  float2 relEyeArea = (eyeUv - eyeAreaMiddle) / eyeAreaHalfSize;
  float2 relAbsEyeArea = abs(relEyeArea);

  float eyeBaseShape = relAbsEyeArea.y / saturate(1 - pow(relAbsEyeArea.x, 2));
  float eyeShape = saturate(remap(eyeBaseShape, 0.9, 1, 1, 0)) * saturate(remap(relEyeArea.x, P.tweakTwistFalloff0, P.tweakTwistFalloff1, 1, 0));
  if (eyeShape == 0) return 0;

  float eyeYRelative = ((eyeUv - P.area.xy) / P.area.zw).y;
  if (extBlink > eyeYRelative){
    uv.y += extBlinkUVOffset;
    colorMult = lerp(0.2, 0.8, sqrt(eyeYRelative));
    return 0;
  }
  colorMult *= saturate((eyeYRelative - extBlink) * 7);

  float2 eyeMovementBase = eyeShape * float2(
    eyeAreaHalfSize.x * (extEyeMovement.x + eyeCorrection),
    eyeAreaHalfSize.y * extEyeMovement.y);
  float eyeFullMovement = saturate(P.eyeFullMovementMult * (length((uv + eyeMovementBase - P.center) * float2(1, P.eyeDistanceScaleY)) - extEyeIrisOuterSize));

  uv += eyeMovementBase * lerp(1, pow(1 - relAbsEyeArea, 2), eyeFullMovement);
  reflectionBoost = pow(saturate(eyeBaseShape * 1.2), 2) * lerp(0.5, 1, saturate(relEyeArea.y));
  reflectionMult = 1 - pow(eyeBaseShape, 8);

  float2 eyeDelta = uv - P.center;
  float2 eyeDir = normalize(eyeDelta);
  float eyeDistance = length(eyeDelta * float2(1, P.eyeDistanceScaleY));
  float eyeIrisInnerSize = lerp(extEyeIrisInnerSize, extEyeIrisOuterSize, 0.5 * extIrisGrow);
  float eyeBlackArea = saturate(remap(eyeDistance, eyeIrisInnerSize, eyeIrisInnerSize * 0.8, 0, 1));
  float eyeGreenArea = saturate(remap(eyeDistance, extEyeIrisOuterSize, extEyeIrisOuterSize * 0.9, 0, 1)) * (1 - eyeBlackArea);
  colorMult *= lerp(1, 1 - eyeBlackArea, eyeShape);
  reflectionExtra = lerp(reflectionExtra, eyeGreenArea, eyeShape);
  specularExtra = lerp(specularExtra, gHeadSide * -eyeDir.x + gHeadUp * -eyeDir.y * -0.25 + gHeadFwd * saturate(remap(eyeDistance, extEyeIrisOuterSize, 0, 0.5, 2)), eyeShape);

  float3 eyeNormal = normalize(gHeadSide * relEyeArea.x + gHeadUp * relEyeArea.y * -1 + gHeadFwd);
  normalW = normalize(lerp(normalW, eyeNormal, eyeShape));

  return eyeShape;
}

float calculateLocalShadow(float2 uv, float3 normalW){
  float ret = 0;
  float totalWeight = 0;

  for (int i = -3; i <= 3; ++i){
    float4 txNm = abs(txNormal.Sample(samLinearSimple, uv + float2(0, i * 0.001)) * 2 - 1);
    float w = (0.01 + txNm.y) / (1 + abs(i));
    ret += txNm.y * w;
    totalWeight += w;
  }

  return 1 - ret / totalWeight;
}

float calculateArea(float4 area, float2 uv, float distMin, float distMax){
  float2 d = (uv - (area.xy + area.zw * 0.5)) / (area.zw * 0.5);
  return saturate(remap(length(d), distMax, distMin, 1, 0));
}

float calculateHairMask(float4 albedo, float2 uv){
  float ret = saturate(remap(dot(albedo.rgb, 1), extHairAlbedoThreshold * 0.5, extHairAlbedoThreshold, 1, 0));

  ret = lerp(ret, 1, calculateArea(extHairInclusionInvArea0, uv, 1, 1.1));
  ret = lerp(ret, 1, calculateArea(extHairInclusionInvArea1, uv, 1, 1.1));

  ret = lerp(ret, 0, calculateArea(extHairExclusionArea0, uv, 1, 0.8));
  ret = lerp(ret, 0, calculateArea(extHairExclusionArea1, uv, 1, 0.8));
  ret = lerp(ret, 0, calculateArea(extHairExclusionArea2, uv, 1, 0.8));
  ret = lerp(ret, 0, calculateArea(extHairExclusionArea3, uv, 1, 0.8));

  return ret;
}

float calculateExtraShadow(float3 pos, float4 params){
  float3 d = pos - params.xyz;
  return saturate(remap(dot2(d), params.w, params.w * 1.1 + 0.00001, 0, 1));
}

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM
  
  float extraGeometryShadow = 1;
  extraGeometryShadow *= calculateExtraShadow(pin.PosL, extFakeShadowParams0);
  extraGeometryShadow *= calculateExtraShadow(pin.PosL, extFakeShadowParams1);

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  shadow = min(shadow, saturate(extraGeometryShadow * 2 - 1));
  APPLY_EXTRA_SHADOW
  extraShadow.y = min(extraShadow.y, 0.5 + 0.5 * saturate(extraGeometryShadow));

  float alpha, directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha, directedMult);

  float2 uv = frac(pin.Tex);
  float colorMult = 1;
  float reflectionMult = 1;
  float reflectionExtra = 0;
  float3 specularExtra = 0;
  float reflectionBoost = 0;
  float eyeMask = max(
    calculateEyeMask(pin, extEyeLParams, -extEyeCorrection, uv, normalW, colorMult, reflectionMult, reflectionExtra, specularExtra, reflectionBoost), 
    calculateEyeMask(pin, extEyeRParams, extEyeCorrection, uv, normalW, colorMult, reflectionMult, reflectionExtra, specularExtra, reflectionBoost));

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, uv);
  float brightnessK = saturate(remap(dot(txDiffuseValue.rgb, 1), extBrightnessFactorThreshold * 0.5, extBrightnessFactorThreshold, 0, 1));
  float hairArea = calculateHairMask(txDiffuseValue, uv);

  float3 skinTint = 1;
  skinTint = lerp(skinTint, float3(1.05, 0.95, 0.95), saturate(extSkinRed));
  skinTint = lerp(skinTint, float3(0.95, 1.0, 1.0), saturate(-extSkinRed));
  txDiffuseValue.rgb *= lerp(1, skinTint, (1 - hairArea) * (1 - eyeMask)) * colorMult;

  // float extMouthOpened = sin(ksGameTime * 0.01) * 0.005 + 0.005;

  #ifndef NO_REFLECTIONS
    if (extMouthMode == 2){
      float moK = pow(abs(pin.PosL1.x) / extMouth.x, 2);
      float moT0 = extMouth.y - 0.0001;
      float moT1 = extMouth.y + 0.0001 - extMouthOpened * (1 - moK * 2);
      [branch]
      if (pin.PosL1.y < moT0 && pin.PosL1.y > moT1){
        float moT2 = extMouth.y + 0.0001 - lerp(extMouthOpened, 0.005, 0.2) * (1 - pow(moK, 2) * 0.5);
        float mouthValue = max(saturate((pin.PosL1.y - moT1) * 400),
          saturate((moT0 - pin.PosL1.y) * 800));

        float teeth = lerp(0.5, 1, saturate((pin.PosL1.y - moT1) * 200)) * saturate((moT2 + 0.005 - pin.PosL1.y) * 800);
        float teethColor = lerp(0.55, 0.7, txNoise.Sample(samLinearSimple, pin.Tex * float2(15, 3)).g);
        txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, lerp(txDiffuseValue.rgb * 0.2, teethColor, teeth), mouthValue);
        normalW = normalize(lerp(normalW, pin.NormalW, mouthValue));
      }
    }
  #endif


  float localShadow = calculateLocalShadow(pin.Tex, normalW);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz * lerp(1, 1.1, eyeMask), shadow * localShadow * lerp(1, 0.3, hairArea), extraShadow.y * AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR;
  L.specularValue = lerp(0.1, 0.3, extSkinSweat);
  L.specularExp = lerp(20, 50, extSkinSweat);
  #ifndef NO_REFLECTIONS
    L.specularValue = lerp(L.specularValue, 0.05, hairArea);
    L.specularExp = lerp(L.specularExp, 1, hairArea);
    L.specularValue = lerp(L.specularValue, 0.5, eyeMask);
    L.specularExp = lerp(L.specularExp, 250, eyeMask);
  #endif
  APPLY_CAO;
  float3 lighting = L.calculate();

  #ifndef NO_REFLECTIONS
    float NdotL = dot(normalW, -ksLightDirection.xyz);
    // lighting *= GAMMA_OR(saturate(dot(normalW, -ksLightDirection.xyz) * 4), 1); // horrible hack

    // NdotL = GAMMA_OR(pow(NdotL, 2) * sign(NdotL), NdotL);
    lighting += ksLightColor.rgb * GAMMA_LINEAR(float3(0.7, 0.15, 0) * (shadow * saturate(remap(NdotL, -0, -0.4, 1, 0)))) 
      * extSubsurfaceScatteringIntensity * brightnessK * GAMMA_OR(0.7, 1);

    float rim = pow(saturate(1 - abs(dot(toCamera, normalW))), GAMMA_OR(6, 3)) * AO_LIGHTING.g * hairArea * saturate(dot(txDiffuseValue.rgb, 2)) 
      * extHairRimBrightness;
    // rim = GAMMA_OR(pow(rim, 2), rim);
    #ifdef ALLOW_LIGHTINGFX
      L.txDiffuseValue.rgb *= extraGeometryShadow;
      L.specularValue *= extraGeometryShadow;
      float furLightingMult = rim;
      LIGHTINGFX(lighting);
    #endif
    lighting += rim * SAMPLE_REFLECTION_FN(toCamera, 5, false, REFL_SAMPLE_PARAM_DEFAULT);
    lighting += rim * saturate(dot(-ksLightDirection.xyz, toCamera)) * saturate(dot(-ksLightDirection.xyz, normalW) * 2 + 0.5) * ksLightColor.xyz * shadow;
  #else
    L.txDiffuseValue.rgb *= extraGeometryShadow;
    L.specularValue *= extraGeometryShadow;
    LIGHTINGFX(lighting);
  #endif

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, 1);
  R.useBias = true; 
  R.fresnelEXP = 5;

  R.fresnelMaxLevel *= brightnessK;
  R.finalMult *= localShadow * reflectionMult * saturate(remap(reflect(toCamera, normalW).y, -0.1, -0.6, 1, 0));

  R.fresnelMaxLevel = lerp(0.15, 0.35, extSkinSweat);
  R.fresnelC = lerp(0, 0.15, extSkinSweat);
  R.ksSpecularEXP = lerp(20, 50, extSkinSweat);

  R.fresnelMaxLevel = lerp(R.fresnelMaxLevel, 0.1, hairArea);
  R.fresnelC = lerp(R.fresnelC, 0, hairArea);
  R.fresnelEXP = lerp(R.fresnelEXP, 10, hairArea);
  R.ksSpecularEXP = lerp(R.ksSpecularEXP, 5, eyeMask);

  R.fresnelMaxLevel = lerp(R.fresnelMaxLevel, lerp(0.3, 0.5, brightnessK), eyeMask);
  R.fresnelC = lerp(R.fresnelC, lerp(0.1, 0.15, brightnessK), eyeMask);
  R.fresnelMaxLevel = lerp(R.fresnelMaxLevel, 0.9, reflectionBoost);
  R.fresnelC = lerp(R.fresnelC, 0.5, reflectionBoost);
  R.fresnelEXP = lerp(R.fresnelEXP, 5, eyeMask);
  R.ksSpecularEXP = lerp(R.ksSpecularEXP, lerp(100, 255, reflectionBoost), eyeMask);

  float4 withReflection = float4(calculateReflection(lighting, toCamera, pin.PosC, normalW, R), 1);
  #ifndef NO_REFLECTIONS
    float3 boostedIrisColor = max(0, lerp(dot(txDiffuseValue.rgb, 0.333), txDiffuseValue.rgb, 2)) * reflectionExtra;
    boostedIrisColor = GAMMA_LINEAR(boostedIrisColor);
    withReflection.rgb += boostedIrisColor * getAmbientAt(pin.PosC, specularExtra, 1) * extIrisBrightness * 0.2;
    withReflection.rgb += boostedIrisColor * reflectanceModel(-toCamera, -ksLightDirection.xyz, normalize(specularExtra), 25) * ksLightColor.xyz * shadow * extIrisBrightness;
    // withReflection.rgb += float3(0, 2, 1) * reflectionExtra;
  #endif

  #ifdef ALLOW_EXTRA_VISUAL_EFFECTS
    if (extDebugMode){
      float dbg = hairArea;
      if (extDebugMode == 2) dbg = brightnessK;
      if (extDebugMode == 3) dbg = eyeMask;
      if (extDebugMode == 4) dbg = reflectionBoost;
      withReflection.rgb = float3(dbg, 1 - dbg, 0) * (normalW.y + 1);

      if (extDebugMode == 5) withReflection.rgb = txDiffuse.Sample(samLinear, pin.Tex).rgb * lerp(1, float3(2, 0.2, 0.2), eyeMask);
    }
    // withReflection.rgb = float3(extraGeometryShadow, 1 - extraGeometryShadow, 0) * 2;
  #endif

  #ifdef USE_SHTOON
    SHTOON_OUTLINE;
  #endif
  RETURN(withReflection.rgb, withReflection.a);
}
