#define MULTILAYER_SAME_MULTS
#define USE_PARALLAX
#define PARALLAX_UV texPosW
#define PARALLAX_UV_MULT multR

#define IN_PARALLAX_TANGENT float3(-1, 0, 0)
#define IN_PARALLAX_BITANGENT float3(0, 0, -1)
#define IN_PARALLAX_NORMAL float3(0, 1, 0)

#define PARALLAX_TEX_TO_WORLD (1/multR)
#define PARALLAX_ALWAYS_USE_TEX_TO_WORLD

#define PARALLAX_EXTRA_ARGS_DECL , float4 txMaskValue
#define PARALLAX_EXTRA_ARGS_PASS , txMaskValue

#include "fuPBR_multilayer_ps.fx"

#ifdef USE_PARALLAX
  Texture2D txDisplacement : register(TX_SLOT_MAT_11);
  float parallaxHeight(float2 uv, float2 dx, float2 dy, float4 txMaskValue){
    return dot(txDisplacement.SampleGrad(samLinear, uv, dx, dy), txMaskValue);
  }
#endif
