#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#define INLCUDE_DIGITALSCREEN_PARAMS
#include "emissiveMapping.hlsl"

#define MULTIEMISSIVE_MODE
#define USE_GLITCHING
#include "digitalScreen.hlsl"

RESULT_TYPE main(PS_IN_NmExtra4 pin) {
  READ_VECTORS_NM
  
  float4 emissiveMap;
  float3 emissiveValue = getEmissiveValue(txDiffuse.Sample(samLinear, pin.Tex), pin.Tex, pin.Extra, emissiveMap);
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);

  DigitalScreenData D = digitalScreenInit(pin.Tex, 0, 1, saturate(dot(smScreenEmissiveMask, emissiveMap)));
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 screenFix = digitalScreenCalculate(D, pin.PosC + ksCameraPosition.xyz, 
    pin.Tex, normalW, bitangentW, toCamera, txDiffuseValue);

  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  float directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, directedMult);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);
  // APPLY_VRS_FIX;

  txDiffuseValue *= 1 - D.mask;
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING, 1);
  L.txEmissiveValue = emissiveValue;
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  lighting += GAMMA_KSEMISSIVE(screenFix * ksEmissive) * getEmissiveMult();

  ReflParams R = getReflParams(L.txSpecularValue, AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  RETURN(withReflection, txMapsValue.a);
}
