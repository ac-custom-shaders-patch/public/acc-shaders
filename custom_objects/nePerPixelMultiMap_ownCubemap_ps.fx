#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#define SAMPLE_REFLECTION_FN sampleReflectionDoubleCube
float3 sampleReflectionDoubleCube(float3 reflDir, float blur, bool useBias, float4 extraParam);

#include "include_new/base/_include_ps.fx"

TextureCube txCubeSecond : register(TX_SLOT_MAT_4);

cbuffer cbCubeSecondTransform : register(b12) {  
  float4x4 extCubeSecondTransform;
}

float3 sampleReflectionDoubleCube(float3 reflDir, float blur, bool useBias, float4 extraParam){
  float3 reflCubeDir = float3(-reflDir.x, reflDir.y, reflDir.z);
  float3 reflDir2 = mul(reflDir, (float3x3)extCubeSecondTransform);
  float3 reflCube2Dir = float3(-reflDir2.x, reflDir2.y, reflDir2.z);

  float3 first;
  float4 second;

  if (useBias){
    float level = txCube.CalculateLevelOfDetail(samLinearSimple, reflDir);
    first = txCube.SampleLevel(samLinearSimple, reflCubeDir, blur + level * saturate(2 - blur) * 0.4).rgb;
    second = txCubeSecond.SampleLevel(samLinearSimple, reflCube2Dir, blur - 2 + level * saturate(2 - blur));
  } else {
    first = txCube.SampleLevel(samLinearSimple, reflCubeDir, blur).rgb;
    second = txCubeSecond.SampleLevel(samLinearSimple, reflCube2Dir, blur - 2);
  }

  second.rgb = applyGamma(second.rgb, 1);
  second.rgb *= AMBIENT_COLOR_NEARBY * perObjAmbientMultiplier * 0.9 + ksLightColor.rgb * extraParam.w * 0.8 + extraParam.rgb * 0.7;
  return lerp(first.rgb, second.rgb, second.a);
}

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  RAINFX_WET(txDiffuseValue.xyz);

  float directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, directedMult);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);
  // APPLY_VRS_FIX;

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING, 1);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  
  float3 lightingFX = LIGHTINGFX_GET;
  lighting.rgb = LIGHTINGFX_APPLY(lighting, lightingFX);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;

  // Gamma correction is not needed here: original local cubemap was shot in gamma space
  R.reflectionSampleParam = float4(lightingFX / max(ksDiffuse * txDiffuseValue.rgb, 0.0001), shadow);
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);

  R.resultPower = 0;
  R.resultColor = 0;
  RAINFX_WATER(withReflection);
  RETURN(withReflection, 1);
}
