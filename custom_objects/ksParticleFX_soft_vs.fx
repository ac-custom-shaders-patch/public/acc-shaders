#define INCLUDE_PARTICLE_CB
#define EXCLUDE_PEROBJECT_CB
// #define FOG_PARTICLE_VERSION
// #define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"

PS_IN_Particle main(VS_IN_Particle vin SPS_VS_ARG) {
  PS_IN_Particle vout;

  float4 posW = vin.PosL;
  float4 posV = mul(posW, ksView);
  vout.PosH = mul(posV, ksProjection);

  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));

  #ifndef NO_SHADOWS
    #ifdef WITH_SHADOWS
      shadows(posW, SHADOWS_COORDS);
    #else
      vout.ShadowTex0 = 0;
    #endif
  #endif

  vout.NormalW = 0;

  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_STATIC(vin.PosL);
  vout.Color = vin.Color;
  vout.Extra = -posV.z;
  SPS_RET(vout);
}
