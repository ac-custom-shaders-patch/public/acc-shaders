#define A2C_SHARPENED_THRESHOLD 0.3
#define ALPHATEST_THRESHOLD 10
#define A2C_SHARPENED
#define NO_CARPAINT
#define NO_GRASSFX_COLOR
#define SUPPORTS_AO
// #define FIX_BASIC_A2C_ALT 
#define RAINFX_STATIC_OBJECT
#define RAINFX_FOLIAGE
#define RAINFX_NO_RAINDROPS

#define CUSTOM_STRUCT_FIELDS\
  float CustomWind : WIND;

#include "include_new/base/_include_ps.fx"
#include "smoothA2C.hlsl"

RESULT_TYPE main(PS_IN_PerPixel pin PS_INPUT_EXTRA) {
  float extrasMult = saturate(pin.CustomWind * 4 - 0.2);

  READ_VECTORS
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.SampleBias(samLinearSimple, pin.Tex, -0.5);
  // float4 txDiffuseValue = txDiffuse.Sample(samLinearSimple, pin.Tex);
  // float4 txDiffuseValue = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 10);
  
  // float4 txDiffuseBlurred = txDiffuse.SampleBias(samLinear, pin.Tex, 5.5);
  // float foliageBaseHighlight = luminance(txDiffuseValue.rgb - txDiffuseBlurred.rgb) * 4;
  // float foliageSpecular = saturate(foliageBaseHighlight + lerp(1, 0.2, txDiffuseBlurred.a)) * extrasMult;
  float foliageSpecular = 0.5;

  ADJUSTCOLOR_PV(txDiffuseValue);
  RAINFX_WET(txDiffuseValue);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  RAINFX_SHINY(L);
  float3 lighting = L.calculate(); 
  
  // #ifdef ALLOW_RAINFX
  //   L.txDiffuseValue *= lerp(0.2, 2 + RP.damp * 4, foliageSpecular);
  // #else
  //   L.txDiffuseValue *= lerp(0.2, 2, foliageSpecular);
  // #endif
  LIGHTINGFX(lighting);

  #ifdef USE_CUSTOM_COVERAGE
    // txDiffuseValue.a *= 0.1;
  #endif
  // lighting.r = 10; 

  RAINFX_REFLECTIVE(lighting);
  A2C_ALPHA(txDiffuseValue.a);
  RETURN_BASE(lighting, txDiffuseValue.a);
}
