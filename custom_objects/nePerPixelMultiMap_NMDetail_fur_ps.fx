#define GBUFF_NORMALMAPS_DISTANCE 5
#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_NORMALIZED_TB
#define CARPAINT_NMDETAILS
#define CARPAINT_AT
#define GETNORMALW_XYZ_TX
#define SUPPORTS_AO
#define CARPAINT_NMDETAILS_ALPHA
#define NO_GRASSFX_COLOR
#define nmObjectSpace 0

#define CUSTOM_STRUCT_FIELDS\
  float Depth : DEPTH;\
  float MatExtra : DEPTH1;

#ifdef ALLOW_FUR
	#define LIGHTINGFX_FUR
#endif
#include "include_new/base/_include_ps.fx"

cbuffer cbMaterialVS : register(b6) {
  float furScale; /* = 0.0025 */
  float furFacingMult; /* = 0.2 */
  float furVerticalOffset; /* = -0.7 */
  float furFidelityScale; /* = 2.7 */
  float furThresholdExp; /* = 0.8 */
  float furThresholdMult; /* = 0.8 */
  float furOcclusion; /* = 0.5 */
  float furRimExp;
  float furAmbient; /* = 1.0 */
  float furDiffuse; /* = 0.5 */
  float furWorldUvScale;
}

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  RAINFX_WET(txDiffuseValue.xyz);

  float alpha, directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha, directedMult);

  float displacement;
  float detailAlpha = considerNmDetails(pin.Tex, tangentW, bitangentW, txDiffuseValue, txMapsValue.xyz, normalW, displacement, directedMult);
  ADJUSTCOLOR(txDiffuseValue);
  APPLY_VRS_FIX;

  txDiffuseValue.rgb *= GAMMA_LINEAR_SIMPLE(pin.MatExtra);

  #ifdef ALLOW_FUR
    if (furDiffuse < 0) {
      displacement = detailAlpha;
    }
    if (furFidelityScale){
      float2 uvOffset = ksInPositionWorld.xz * furWorldUvScale;
      float noise = txNoise.Sample(samLinear, uvOffset + pin.Tex * detailUVMultiplier * abs(furFidelityScale)).x;
      displacement = furFidelityScale < 0 ? noise : saturate(displacement * noise * txMapsValue.a * 2);
    }
    clip(displacement - pow(saturate(pin.Depth), furThresholdExp) * furThresholdMult);
  #endif

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING, 1);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();

  #ifdef ALLOW_FUR
    float rim = pow(saturate(1 - abs(dot(toCamera, normalW))), GAMMA_OR(furRimExp * 2, furRimExp)) * min(AO_LIGHTING.g, txMapsValue.r) * pin.Depth * pin.MatExtra;

    #ifdef ALLOW_LIGHTINGFX
      float furLightingMult = rim * abs(furDiffuse);
      LIGHTINGFX(lighting);
    #endif

    lighting *= lerp(1, furOcclusion, (1 - displacement) * (1 - pin.Depth));
    lighting += rim * furAmbient * SAMPLE_REFLECTION_FN(toCamera, 5, false, REFL_SAMPLE_PARAM_DEFAULT);
    lighting += rim * abs(furDiffuse) * saturate(dot(-ksLightDirection.xyz, toCamera)) * saturate(dot(-ksLightDirection.xyz, normalW) * 2 + 0.5) * ksLightColor.xyz * shadow;
  #else
    LIGHTINGFX(lighting);
  #endif

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);

  #if MODE_MAIN_NOFX
    withReflection.g += saturate(furScale + furFacingMult + furVerticalOffset) * 0.00001;
  #endif

  // withReflection.r = displacement;
  // withReflection.g = 1 - displacement;

  RAINFX_WATER(withReflection);
  RETURN(withReflection, alpha);
}
