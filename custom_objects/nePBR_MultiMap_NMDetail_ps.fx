#include "include/common.hlsl"
#include "include_new/base/_flags.fx"

#define GAMMA_TWEAKABLE_ALPHA
#define REFLECTION_FRESNELEXP_BOUND
// #define WITHOUT_NMDETAIL
#define USE_SHADOW_BIAS_MULT
#ifndef WITHOUT_NMDETAIL
  #define GETNORMALW_NORMALIZED_TB
  #define CARPAINT_NMDETAILS
  #define CARPAINT_NMDETAILS_PBR_MODE
#endif
#define GETNORMALW_XYZ_TX
// #define GBUFFER_FORCE_SHADOWS
#define SUPPORTS_AO
#define GBUFF_NORMAL_W_SRC normalWOuter

#define INPUT_AMBIENT_K 1
#define INPUT_SPECULAR_K 1
#define LIGHTINGFX_KSDIFFUSE 0.4
#define LIGHTINGFX_SPECULAR_COLOR lfxSpecularPower
#define LIGHTINGFX_SPECULAR_EXP lfxSpecularEXP
#define LIGHTINGFX_SUNSPECULAR_EXP 1
#define LIGHTINGFX_SUNSPECULAR_COLOR 0

#ifdef USE_CLEARCOAT
  #define EXTRA_CARPAINT_FIELDS_CLEARCOAT\
    float2 pbClearCoatSmoothness;\
    float pbClearCoatIntensity;\
    float pbClearCoatF0;\
    float pbClearCoatNormalShare;

  #ifdef ALLOW_CLEARCOAT
    #define LIGHTINGFX_TWO_NORMALS
    #define LIGHTINGFX_TWO_NORMALS_TWO_SPECULARS
    #define LIGHTINGFX_SPECULAR_EXP1 lfxSpecularEXP1
  #endif

  #define NMDETAILS_DEFAULT_SAMPLER samLinearSimple
#else
  #define EXTRA_CARPAINT_FIELDS_CLEARCOAT
  #define USE_HILL12_REORIENTATION
  // #define USE_HILL12_REORIENTATION_UDN
#endif

#ifdef USE_CLOTH_SHADING
  #define EXTRA_CARPAINT_FIELDS_CLOTH\
    float3 pbClothSheenColor;\
    float3 pbClothSubsurfaceColor;
#else
  #define EXTRA_CARPAINT_FIELDS_CLOTH
#endif

#ifdef WITHOUT_NMDETAIL
  Texture2D txEmissive : register(TX_SLOT_MAT_4);
  Texture2D txStickers : register(TX_SLOT_MAT_5);
  Texture2D txStickersMaps : register(TX_SLOT_MAT_6);
  #define EXTRA_CARPAINT_FIELDS_NMDETAIL
#else
  Texture2D txEmissive : register(TX_SLOT_MAT_5);
  Texture2D txStickers : register(TX_SLOT_MAT_6);
  Texture2D txStickersMaps : register(TX_SLOT_MAT_7);
  #define EXTRA_CARPAINT_FIELDS_NMDETAIL\
    float4 pbNormalDetailPBROccludedColor;\
    float pbNormalDetailInPBRFormat;\
    float3 pbNormalDetailPBRMults;\
    float pbBlurColors;\
    float pbRoughnessEXP;
#endif

#define EXTRA_CARPAINT_FIELDS\
  uint extStickersMode_uint;\
  float2 pbOcclusion;\
  float2 pbSmoothness;\
  float2 pbMetalness;\
  float2 pbReflectance;\
  float pbColorSource;\
  float pbOcclusionSource;\
  float pbOcclusionMult;\
  float pbReflectionBlurEnv;\
  EXTRA_CARPAINT_FIELDS_CLOTH\
  EXTRA_CARPAINT_FIELDS_NMDETAIL\
  EXTRA_CARPAINT_FIELDS_CLEARCOAT

#include "common/uv2Utils.hlsl"
#include "include_new/base/_include_ps.fx"
#include "pbr/pbr.hlsl"

#ifdef USE_CLEARCOAT
  #include "pbr/clear_coat.hlsl"
#endif

// alias: nePBR_MultiMap_NMDetail_skinned_ps
// alias: nePBR_MultiMap_NMDetail_bending_ps

// Please don’t copy this code as a base for your shader
// It’s all very experimental and WIP, and will be moved to utils_ps.fx as soon as it’s sorted and tested

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW
  // shadow *= saturate(-dot(normalW, ksLightDirection.xyz) * 20);

  // usual stuff
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);

  [branch]
  if (extStickersMode_uint){
    float4 txStickerValue = txStickers.Sample(samLinear, float2(pin.Tex2X, pin.Tex2Y));
    txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, txStickerValue.rgb, txStickerValue.a);
    if (extStickersMode_uint & 2) {
      txDiffuseValue.a = lerp(txDiffuseValue.a, 1, txStickerValue.a);
    }
    if (extStickersMode_uint & 4) {
      float4 txStickerMapsValue = txStickersMaps.Sample(samLinear, float2(pin.Tex2X, pin.Tex2Y));
      txMapsValue.rgb = lerp(txMapsValue.rgb, txStickerMapsValue.rgb, txStickerMapsValue.a);
    }
  }
  
  RAINFX_WET(txDiffuseValue.xyz);

  // Some PBR stuff
  float occlusionTexValue = 1;
  if (pbOcclusionSource == 1){
    occlusionTexValue = lerp(max(max(txDiffuseValue.x, max(txDiffuseValue.y, txDiffuseValue.z)), 0.05), 1, 0.25);
  } else if (pbOcclusionSource == 2){
    occlusionTexValue = txMapsValue.w;
  }

  // occlusionTexValue = 1;

  /*TODO:GAMMA*/

  if (abs(pbColorSource) == 1){
    txDiffuseValue.xyz /= occlusionTexValue;
    occlusionTexValue = GAMMA_OR(sqrt(occlusionTexValue), occlusionTexValue);
  } else if (abs(pbColorSource) == 2){
    txDiffuseValue.xyz = 1;
  } else if (abs(pbColorSource) == 3){
    txDiffuseValue.xyz = luminance(txDiffuseValue.xyz);
  }

  occlusionTexValue = saturate(occlusionTexValue * pbOcclusionMult);
  if (pbColorSource < 0) txDiffuseValue.a = 0;
  txDiffuseValue.xyz *= INPUT_DIFFUSE_K;

  // normals piece  
  #ifdef USE_HILL12_REORIENTATION
    float directedMult = 1;
    float4 txNormalValue = txNormal.Sample(samLinearSimple, pin.Tex);
  #else    
    float directedMult;
    normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, directedMult);
    float4 txNormalValue = 1;
  #endif
  float3 pbrMults = float3(1, 0, 1);
  float baseOcclusion = saturate(lerp(pbOcclusion.x, pbOcclusion.y, occlusionTexValue));
  float baseMetalness = saturate(lerp(pbMetalness.x, pbMetalness.y, txMapsValue.y));
  
  float3 normalWBase = normalW;
  #ifdef WITHOUT_NMDETAIL
    considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);
    float pbRoughnessEXP = 1;
  #else
    float3 pbrMultsBase = pbrMults;
    // float3 dcbak = txDiffuseValue.rgb; 
    considerNmDetails(pin.Tex, tangentW, bitangentW, txDiffuseValue, txMapsValue.xyz, normalW,
      #ifdef USE_HILL12_REORIENTATION
      txNormalValue.xyz, 
      #endif
      pbNormalDetailInPBRFormat, pbBlurColors, pbrMultsBase, directedMult);
    // pbrMultsBase.x = pow(pbrMultsBase.x, 1. / GAMMA_ACTUAL_VALUE);
    // pbrMultsBase.x = GAMMA_LINEAR(pbrMultsBase.x);
    // txDiffuseValue.rgb = dcbak;
    pbrMults = saturate(lerp(pbrMults, pbrMultsBase, pbNormalDetailPBRMults));
    // pbrMults.x = float3(1, 0, 1).x;
    baseMetalness *= pbrMults.z;
    txDiffuseValue.rgb = saturate(lerp(txDiffuseValue.rgb, pbNormalDetailPBROccludedColor.rgb, pbNormalDetailPBROccludedColor.a * (1 - pbrMultsBase.x)));
  #endif

  float diffLum = dot(txDiffuseValue.rgb, 0.333);
  float saturation = max(txDiffuseValue.r, max(txDiffuseValue.g, txDiffuseValue.b)) - diffLum;
  txDiffuseValue.rgb = lerp(diffLum, txDiffuseValue.rgb, saturate(saturation * 100 - 1));

  float roughnessBaseInner = saturate(1 - lerp(pbSmoothness.x, pbSmoothness.y, txMapsValue.x)) * pow(saturate(1 - pbrMults.y), pbRoughnessEXP);
  #if defined(USE_CLEARCOAT) && defined(ALLOW_CLEARCOAT)
    float roughnessBaseOuter = lerp(
      roughnessBaseInner,
      saturate(1 - lerp(pbClearCoatSmoothness.x, pbClearCoatSmoothness.y, txMapsValue.x)),
      pbClearCoatIntensity);
  #else
    float roughnessBaseOuter = roughnessBaseInner;
  #endif

  #if defined(USE_CLEARCOAT) && defined(ALLOW_CLEARCOAT)
    float3 normalWInner = normalize(lerp(normalW, normalWBase, pbClearCoatNormalShare * pbClearCoatIntensity));
    float3 normalWOuter = normalize(lerp(normalW, normalWBase, (1 - pbClearCoatNormalShare) * pbClearCoatIntensity));
  #else
    float3 normalWInner = normalW;
    float3 normalWOuter = normalW;
  #endif

  // lighting
  float4 txEmissiveValue = txEmissive.Sample(samLinear, pin.Tex);
  LightingParamsPBR L;
  L.txDiffuseValue = txDiffuseValue.rgb;
  L.emissiveMult = dot(txEmissiveValue, 1) ? txEmissiveValue.rgb : 1;
  L.shadow = shadow;
  L.globalOcclusion = saturate(min(baseOcclusion, _AO_VAO));
  L.localOcclusion = saturate(min(extraShadow.y, _AO_SSAO));
  // L.occlusion = min(baseOcclusion, _AO_VAO) * min(extraShadow.y, _AO_SSAO);
  L.reflectionOcclusion = baseOcclusion * EXTRA_SHADOW_REFLECTION * AO_REFLECTION;
  L.roughness = pow(roughnessBaseInner, 2);
  L.skipDiffuseForEmissive = false;
  APPLY_CAO_CUSTOM(L.shadow, L.localOcclusion);

  float f0Mult = lerp(pbReflectance.x, pbReflectance.y, txMapsValue.z) * saturate(pbrMults.x);
  #ifdef USE_CLOTH_SHADING
    L.f0 = pbClothSheenColor * f0Mult;
    L.subsurfaceColor = pbClothSubsurfaceColor;
  #else
    L.metalness = baseMetalness;
    L.reflectance = f0Mult;
  #endif

  float reflBlur = getReflBlur(roughnessBaseInner);
  L.reflDir = normalize(reflect(toCamera, normalWInner));
  L.reflDir = normalize(lerp(L.reflDir, normalWInner, roughnessBaseInner));
  L.reflDir = fixReflDir(L.reflDir, pin.PosC, normalWInner, reflBlur, 1 + roughnessBaseInner * 2);

  LightingOutput lightingOutput = calculateLighting(pin.PosC, toCamera, normalWInner, tangentW, bitangentW, L);
  lightingOutput.F = GAMMA_OR(pow(lightingOutput.F, 2), lightingOutput.F);

  #if defined(USE_CLEARCOAT) && defined(ALLOW_CLEARCOAT)
    float3 reflDirInner = L.reflDir;
    float reflBlurOuter = getReflBlur(roughnessBaseOuter);
    L.reflDir = normalize(reflect(toCamera, normalWOuter));
    L.reflDir = normalize(lerp(L.reflDir, normalWOuter, roughnessBaseOuter));
    L.reflDir = fixReflDir(L.reflDir, pin.PosC, normalWOuter, reflBlurOuter, 1 + roughnessBaseOuter * 2);

    L.txDiffuseValue = 1;
    L.metalness = 0;
    L.roughness = pow(roughnessBaseOuter, 2);
    L.reflectance = pbClearCoatF0;
    LightingOutput lightingOutputClearCoat = calculateLighting(pin.PosC, toCamera, normalWOuter, tangentW, bitangentW, L, true);

    ClearCoatParams ccP;
    ccP.intensity = pbClearCoatIntensity;
    ccP.f0 = pbClearCoatF0;
    ClearCoat cc = CalculateClearCoat(toCamera, normalWOuter, ccP);
    lightingOutput.diffuse = cc.ProcessDiffuse(lightingOutput.diffuse);
    lightingOutput.specular = cc.ProcessSpecular(lightingOutput.specular, lightingOutputClearCoat.specular);
    float3 reflColorInner = getReflectionAt(reflDirInner, -toCamera, lerp(pbReflectionBlurEnv, 6, reflBlur), true, 0) * GET_EXTRA_SHADOW_OCCLUSION(reflDirInner);  
    float3 reflFinalInner = lightingOutput.F * reflColorInner;
    float3 reflColorOuter = getReflectionAt(L.reflDir, -toCamera, lerp(pbReflectionBlurEnv, 6, reflBlurOuter), true, 0) * GET_EXTRA_SHADOW_OCCLUSION(L.reflDir);  
    float3 reflFinalOuter = lightingOutputClearCoat.F * reflColorOuter;
    float3 reflFinal = cc.ProcessSpecular(reflFinalInner, reflFinalOuter);

    // For G-buffer:
    lightingOutput.F = cc.ProcessSpecular(lightingOutput.F, lightingOutputClearCoat.F);
    reflBlur = reflBlurOuter;

    // For dynamic lights:
    float ccF0Mult = 2 * pbClearCoatF0 * cc.fresnel;
    f0Mult *= 2 * pow(1 - cc.fresnel, 2);
  #else
    float3 reflColor = getReflectionAt(L.reflDir, -toCamera, lerp(pbReflectionBlurEnv, 6, reflBlur), true, 0) * GET_EXTRA_SHADOW_OCCLUSION(L.reflDir);  
    // lightingOutput.F = 1;
    float3 reflFinal = lightingOutput.F * reflColor;
    f0Mult *= 4;
  #endif
  
  float3 lighting = LFM_SUM(LFM_CONTRIB(lightingOutput.diffuse) + LFM_CONTRIB(lightingOutput.specular));

  L.txDiffuseValue = txDiffuseValue.rgb;
  #ifndef USE_CLOTH_SHADING
    L.txDiffuseValue.rgb *= occlusionTexValue * lerp(lerp(1, 0.3, L.reflectance), 0.03, L.metalness);
  #endif
  L.txDiffuseValue *= saturate(L.localOcclusion * L.globalOcclusion * 2);
  // float lfxSpecularBase = pow(1 - roughnessBaseOuter, 4);
  // float lfxSpecularPower = lfxSpecularBase * sqrt(saturate(f0Mult));
  // float lfxSpecularEXP = 1 + lfxSpecularBase * 250;
  float lfxSpecularBase = pow(1 - roughnessBaseInner, 4);
  float lfxSpecularPower = lfxSpecularBase * sqrt(saturate(f0Mult));
  float lfxSpecularEXP = 1 + lfxSpecularBase * 250;

  {
    #if defined(USE_CLEARCOAT) && defined(ALLOW_CLEARCOAT)
      float3 normalW0 = normalWInner;
      float3 normalW1 = normalWOuter;
      float lfxSpecularBase1 = pow(1 - roughnessBaseOuter, 4);
      float lfxSpecularPower1 = lfxSpecularBase1 * sqrt(saturate(ccF0Mult)) * 10;
      float lfxSpecularEXP1 = 1 + lfxSpecularBase1 * 1000;
      float normalMix = lfxSpecularBase1 / (lfxSpecularBase + lfxSpecularBase1);
      lfxSpecularPower = lerp(lfxSpecularPower, lfxSpecularPower1, normalMix);
    #endif
    LIGHTINGFX(lighting);
  }

  float3 withReflection = LFM_SUM(LFM_CONTRIB(lighting) + LFM_CONTRIB(reflFinal));
  ReflParams R = (ReflParams)0;
  R.resultBlur = clamp(reflBlur * reflBlur * 7 - 1, 0, 6);
  R.resultPower = 1;
  R.coloredReflectionsColor = lightingOutput.F;
  R.coloredReflections = 1;
  R.resultColor = reflFinal;

  // R.resultColor = 4;

  // withReflection = float3(frac(pin.Tex * 1000), 0);
  // withReflection = txDiffuseValue.a;

  RAINFX_WATER(withReflection);
  RETURN(withReflection, txNormalValue.a);
}
