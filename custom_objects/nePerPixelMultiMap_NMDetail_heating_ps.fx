#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_NORMALIZED_TB
#define CARPAINT_NMDETAILS
#define GETNORMALW_XYZ_TX
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"
#include "common/heating.hlsl"

RESULT_TYPE main(PS_IN_NmPosL pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  
  float directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, false, directedMult);
  considerNmDetails(pin.Tex, tangentW, bitangentW, txDiffuseValue, txMapsValue.xyz, normalW, directedMult);
  // APPLY_VRS_FIX;

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING, 1);
  L.txEmissiveValue = 0;
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  HEATINGFX(withReflection);
  RETURN(withReflection, 1);
}
