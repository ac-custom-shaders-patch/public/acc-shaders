#define SUPPORTS_NORMALS_AO
// #undef ALLOW_CAR_FLEX
#include "include_new/base/_include_vs.fx"
#include "common/refractingCover.hlsl"

PS_IN_RefractingCover main(VS_IN vin SPS_VS_ARG) {
  PS_IN_RefractingCover vout;
  float4 posW, posWnoflex, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);

  float distanceToMirrorPlane = distanceToPlane(extMirrorPlane, posWnoflex.xyz);
  vout.PosV_IsMirrored.w = distanceToMirrorPlane < 0;
  vout.PosV_IsMirrored.xyz = posWnoflex.xyz - ksCameraPosition.xyz;
  if (vout.PosV_IsMirrored.w){    
    vout.PosV_IsMirrored.xyz -= distanceToMirrorPlane * extMirrorPlane.xyz * 2;
  }

  #ifdef USE_LOCAL_TAA
    float4 posWPrev = mul(float4(vin.PosL.xyz, 1), ksWorldPrevFTaa);
    applyPointFlex_prev(posWPrev.xyz);
    vout.PosCS0 = vout.PosH.xyw * float3(extCSMult, 1);
    float4x4 vpPrev = ksVpPrev;
    #ifdef USE_SPS
      if (instanceID) vpPrev = ksVpPrev0;
    #endif
    vout.PosCS1 = mul(float4(posWPrev.xyz, 1), vpPrev).xyw * float3(extCSMult, 1);
  #endif

  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
