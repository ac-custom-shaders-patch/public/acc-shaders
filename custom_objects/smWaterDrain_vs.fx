#define CUSTOM_MATERIAL_PROPERTIES
#define SUPPORTS_NORMALS_AO
#define RAINFX_STATIC_OBJECT

#define CUSTOM_STRUCT_FIELDS\
  float Depth : TEXCOORD20;\
  float3 FlowDir : TEXCOORD21;

#define CB_MATERIAL_EXTRA_3\
  float waterOffset; /* 0.04 */\
  float waterIntensityMult; /* 2 */\
  float waterWaterMult; /* 1.6 */

#include "include_new/base/_include_vs.fx"

PS_IN_PerPixel main(VS_IN vin SPS_VS_ARG) {
  PS_IN_PerPixel vout;
  float4 posW, posWnoflex, posV;

  vin.PosL.y -= waterOffset;
  vin.PosL.y += waterOffset * lerp(saturate(vsLoadHeating(vin.TangentPacked) * extSceneRain * waterIntensityMult), 1, 
    sqrt(saturate(extSceneWater * waterWaterMult)));

  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  vout.Depth = -posV.z;
  vout.FlowDir = vsLoadTangent(vin.TangentPacked);
  shadows(posW, SHADOWS_COORDS);
  // PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
