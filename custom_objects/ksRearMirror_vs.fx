#define NO_PERVERTEX_AO

#define CB_MATERIAL_EXTRA_3\
	float extUseAltTexture;\
	float extShake;
  
#include "common/rainWindscreen.hlsl"
#include "include_new/base/_include_vs.fx"

PS_IN_PerPixelCustom main(VS_IN vin SPS_VS_ARG) {
  GENERIC_PIECE(PS_IN_PerPixelCustom);
  FILL_VS(vin, vout);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
