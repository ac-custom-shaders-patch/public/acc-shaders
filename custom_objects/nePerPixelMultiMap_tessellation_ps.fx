#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define TXDETAIL_VARIATION (ksAlphaRef == -193)
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"
#ifndef BENDING_VARIANT
  #include "common/tessellationParallaxShared.hlsl"
#endif
#include "common/tessellation.hlsl"

RESULT_TYPE main(DS_OUT pin) {
  #ifndef MODE_SHADOWS_ADVANCED

    READ_VECTORS_NM
    float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
    APPLY_EXTRA_SHADOW

    float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
    float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);

    float directedMult;
    normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, directedMult);
    considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);
    APPLY_VRS_FIX;

    /*#ifndef BENDING_VARIANT
      // Experiment with deforming ground
      if (extSurfaceMapping){
        normalW = float3(0, 1, 0);
        float b0 = txNormal.Sample(samLinear, ksInPositionWorld.xz * 20 / 1024).r;
        float bx = txNormal.Sample(samLinear, (ksInPositionWorld.xz + float2(0.1, 0)) * 20 / 1024).r;
        float bz = txNormal.Sample(samLinear, (ksInPositionWorld.xz + float2(0, 0.1)) * 20 / 1024).r;

        normalW = normalize(float3(b0 - bx, 0.1, b0 - bz));

        // float o = abs(b0) * 10;
        // o = o / (1 + o);
        // txDiffuseValue *= 1 - o;
        // txDiffuseValue *= dot(frac(ksInPositionWorld.xz * 2) * 0.5 + 0.5, 0.5);
      }
    #endif*/

    LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING, 1);
    L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
    L.applyTxMaps(txMapsValue.xyz);
    float3 lighting = L.calculate();
    LIGHTINGFX(lighting);

    ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue.xyz);
    R.useBias = true;
    R.isCarPaint = true;
    float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
    RETURN(withReflection, HAS_FLAG(FLAG_MATERIAL_0) ? txMapsValue.w : 1);

  #else

    float4 txMapsValue = txMaps.Sample(samLinearSimple, pin.Tex);
    clip((HAS_FLAG(FLAG_MATERIAL_0) ? txMapsValue.w : 1) - ksAlphaRef);
    return (RESULT_TYPE)0;

  #endif
}
