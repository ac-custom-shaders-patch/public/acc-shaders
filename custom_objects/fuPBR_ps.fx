#define USE_UV2
#define PBR_MATERIAL\
  float uvScale;

#include "fuPBR_gen_ps.hlsl"

#define FLAG_APPLY_AO_MAP 256
#define FLAG_APPLY_VARIANCE_MAP 512
#define FLAG_TILING_FIX 1024
#define FLAG_USE_SPECULAR_TEXTURE 2048
#define FLAG_USE_UV2 4096

Texture2D txAlbedo : register(TX_SLOT_MAT_0);
Texture2D txEmAoMeRo : register(TX_SLOT_MAT_1);
Texture2D txNormal2 : register(TX_SLOT_MAT_2);
Texture2D txSpecular : register(TX_SLOT_MAT_3);
Texture2D txVarianceAO : register(TX_SLOT_MAT_4);

PBRInput getPBRInput(PS_IN_Nm pin, inout float shadowMult){
  PBRInput I = createPBRInput();

  float2 uvAlt = (pbrFlags_uint & FLAG_USE_UV2) != 0 ? PIN_TEX2 : pin.Tex;
  pin.Tex *= uvScale;

  #if defined(USE_MULTIMAP_TEXTURE_VARIATION) && !defined(USE_PARALLAX)
    float4 txAlbedoValue;
    float4 txCombinedValue;
    float4 txNormalBaseValue;

    [branch]
    if (pbrFlags_uint & FLAG_TILING_FIX){
      txAlbedoValue = textureSampleVariation(txAlbedo, samLinear, pin.Tex);
      txCombinedValue = textureSampleVariation(txEmAoMeRo, samLinear, pin.Tex);
      txNormalBaseValue = textureSampleVariation(txNormal2, samLinear, pin.Tex);
    } else {
      txAlbedoValue = txAlbedo.Sample(samLinear, pin.Tex);
      txCombinedValue = txEmAoMeRo.Sample(samLinear, pin.Tex);
      txNormalBaseValue = txNormal2.Sample(samLinear, pin.Tex);
    }
  #else
    float4 txAlbedoValue = txAlbedo.Sample(samLinear, pin.Tex);
    float4 txCombinedValue = txEmAoMeRo.Sample(samLinear, pin.Tex);
    float4 txNormalBaseValue = txNormal2.Sample(samLinear, pin.Tex);
  #endif

  I.albedo = txAlbedoValue.rgb;
  I.alpha = txAlbedoValue.a;
  I.nmBase = (pbrFlags_uint & FLAG_DXT5NM_NORMAL) ? txNormalBaseValue.wy : txNormalBaseValue.xy;
  I.emissive = txCombinedValue.x;
  I.localAO = txCombinedValue.y;
  I.roughness = txCombinedValue.w;

  float4 txVarianceAOValue = txVarianceAO.Sample(samLinear, uvAlt);
  if (pbrFlags_uint & FLAG_APPLY_AO_MAP) I.globalAO = txVarianceAOValue.w;
  if (pbrFlags_uint & FLAG_APPLY_VARIANCE_MAP) I.albedo *= txVarianceAOValue.rgb;

  float3 f0MapValue = txCombinedValue.z;
  if (pbrFlags_uint & FLAG_USE_SPECULAR_TEXTURE){      
    #ifdef USE_MULTIMAP_TEXTURE_VARIATION
      [branch]
      if (pbrFlags_uint & FLAG_TILING_FIX){
        f0MapValue = textureSampleVariation(txSpecular, samLinear, pin.Tex).xyz;
      } else {
        f0MapValue = txSpecular.Sample(samLinear, pin.Tex).xyz;
      }
    #else
      f0MapValue = txSpecular.Sample(samLinear, pin.Tex).xyz;
    #endif
  }
  I.setF0(f0MapValue);

  return I;
}