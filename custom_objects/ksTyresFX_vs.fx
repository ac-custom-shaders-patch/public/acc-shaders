// #define SUPPORTS_NORMALS_AO
#define INCLUDE_TYRE_CB
#define SET_AO_TO_ONE
// #define RAINFX_STATIC_OBJECT

#define CUSTOM_STRUCT_FIELDS\
  float Blown : TEXCOORD20;\
  float BlownFlap : TEXCOORD21;

#define RAINFX_CAR_WETNESS_MIP 1.5
#define RAINFX_NO_PERVERTEX_OCCLUSION
#include "include_new/base/_include_vs.fx"

#ifdef ALLOW_TYRESFX
  #include "include_new/ext_tyresfx/_include_vs.fx"
#endif

PS_IN_Nm main(VS_IN vin SPS_VS_ARG) {
  PS_IN_Nm vout;
  float4 posW, posV;
  posW = mul(vin.PosL, ksWorld);
  // APPLY_CFX(posW);

  vout.Blown = vsLoadMat0f(vin.TangentPacked);
  vout.BlownFlap = saturate(vsLoadMat0f(vin.TangentPacked) * 2);
  #ifdef ALLOW_TYRESFX
    if (vout.Blown) {
      posW.xyz -= cross(dirUp, dirForward) * (vsLoadMat1f(vin.TangentPacked) * 2 - 1) * 0.2 * saturate(tyreAngularSpeed / 60 - 0.5);
      // posW.xyz -= cross(dirUp, dirForward) * (vsLoadMat1f(vin.TangentPacked) * 2 - 1) * 0.2 * (1 - pow(saturate(vout.Blown * 2), 1));
      vout.Blown = vout.Blown > 0.5 ? 1 : 0.01;
    }
  #endif

  vout.NormalW = normals(vin.NormalL);

  #ifdef ALLOW_TYRESFX
    ADJUST_WORLD_POS(posW, vout.NormalW);

    float4 posWnoflex = posW;
    applyPointFlex(posW.xyz);
    posV = mul(float4(posW.xyz, 1), ksView);
    vout.PosH = mul(posV, ksProjection);
  #else
    float4 posWnoflex;
    vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  #endif

  vout.PosC = posWnoflex.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  shadows(posW, SHADOWS_COORDS);

  PREPARE_TANGENT;
  
  PREPARE_AO(vout.Ao);

  #if defined(CREATE_MOTION_BUFFER) && defined(ALLOW_TYRESFX)
    #ifndef ZERO_MOTION_BUFFER
      vout.PosCS0 = STORE_CS_POS(vout.PosH);
      float4 prevPosW = mul(vin.PosL, ksWorldPrev);
      float3 prevNormalW = vout.NormalW;
      ADJUST_WORLD_POS(prevPosW, prevNormalW);
      applyPointFlex_prev(prevPosW.xyz);
      vout.PosCS1 = STORE_CS_POS(mul(prevPosW, ksViewProjPrev));
    #else
      vout.PosCS0 = CS_POS_ZERO;
      vout.PosCS1 = CS_POS_ZERO;
    #endif
  #else
    GENERIC_PIECE_MOTION(vin.PosL);
  #endif

  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);

  #if defined(ALLOW_TYRESFX) && defined(ALLOW_RAINFX) && !defined(MODE_SIMPLIFIED_FX)
    float3 posD = posW.xyz - wheelPos;
    vout.PosR = float3(dot(dirSide, posD), dot(dirUp, posD), dot(dirForward, posD));
    vout.NormalR = float3(dot(dirSide, vout.NormalW), dot(dirUp, vout.NormalW), dot(dirForward, vout.NormalW));
  #endif

  SPS_RET(vout);
}
