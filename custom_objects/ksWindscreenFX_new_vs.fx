#define SUPPORTS_COLORFUL_AO
// #define SPS_NO_POSC

#define CB_MATERIAL_EXTRA_3\
  float bannerMode;\
  float3 extEdgePosL;\
  float extEdgeRefractionBias;\
  float extEdgeThreshold;\
	float reflectionsIntensity;\
	float alphaGamma;

#include "include_new/base/_flags.fx"
#if defined(ALLOW_EXTRA_VISUAL_EFFECTS)
  #define CUSTOM_STRUCT_FIELDS_EXTRA\
    float3 PosL : TEXCOORD20;\
    float3 NormalL : TEXCOORD21;
#endif

#ifdef NO_WINDSCREENFX
	#undef BLUR_SHADOWS
#endif

#include "common/rainWindscreen.hlsl"
#include "include_new/base/_include_vs.fx"

#ifdef BLUR_SHADOWS
  #include "include_new/ext_windscreenfx/_include_vs.fx"
#endif

PS_IN_PerPixelCustom main(VS_IN vin SPS_VS_ARG) {
  PS_IN_PerPixelCustom vout;
  float4 posW, posWnoflex, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.NormalW = normalize(normals(vin.NormalL));
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  FILL_VS(vin, vout);

  #if !defined(NO_SHADOWS)
    shadows(posW, SHADOWS_COORDS);
  #endif

  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  #if defined(ALLOW_EXTRA_VISUAL_EFFECTS)
    vout.PosL = vin.PosL.xyz;
    vout.NormalL = vin.NormalL.xyz;
  #endif
  SPS_RET(vout);
}
