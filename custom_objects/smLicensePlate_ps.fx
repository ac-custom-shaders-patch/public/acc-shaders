#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinear
#define LIGHTINGFX_SPECULAR_COLOR (txMapsValue.x * sunSpecular)
#define LIGHTINGFX_SPECULAR_EXP (txMapsValue.y * sunSpecularEXP + 1)
#define LIGHTINGFX_FIND_MAIN
#define USE_TXEMISSIVE_AS_EMISSIVE
#define TXEMISSIVE_VALUE emissiveMap
#define SUPPORTS_AO

float getHeight(float2 uv, float2 dx, float2 dy);
#define GET_HEIGHT(UV, DX, DY) getHeight(UV, DX, DY)
#define parallaxScale extParallaxHeight
#ifndef FORCED_PARALLAX_MODE
// #define USE_BASIC_PARALLAX
// #define USE_STEEP_PARALLAX
// #define USE_OCCLUSION_PARALLAX
#define USE_RELIEF_PARALLAX
#endif

cbuffer cbRefractingCover : register(b6) {
  float extGlowingLettersLampsPI /* = num×PI */;
  float3 ksEmissive1;
  
  float extGlowingLettersLampsFactor /* = 1.0 */;
  float extEmissiveShapeMult /* = 1.6 */;
  float extEmissiveFadeX /* = 0.8 */;
  float extEmissiveFadeY /* = 0.8 */;
  
  float extDarkeningFactor /* = 0.85 */;
  float extOcclusionFactor /* = 0.6 */;
  float extParallaxHeight /* = 1 */;
  float extLettersExtrude /* = 1 */;
  
  float4 extLettersArea /* = 1 / halfSize, -center / halfSize; halfSize = float2(0.35, 0.4); center = 0.5 */;
  float4 extBorderedArea /* = 1 / halfSize, -center / halfSize; halfSize = float2(0.482, 0.44); center = 0.5 */;
  
  float2 extBorderedRadius /* = 1 / cornerRadius; cornerRadius = float2(0.03, 0.12) */;
  float extLettersMult /* = -3 */;
  float extLettersAdd /* = 1 */;
  
  float extWidthRatio;
  float extDebugMode;
}

#include "lightingBounceBack.hlsl"
#include "include_new/base/_include_ps.fx"
#include "tessellationParallaxShared.hlsl"
#include "parallax.hlsl"

float estimateLetters(float2 uv, float4 texValue) {
  float2 hit = uv * extLettersArea.xy + extLettersArea.zw;
  float2 hitAbs = 1 - abs(hit);
  float mv = max(texValue.r, max(texValue.g, texValue.b));
  float map = saturate(mv * extLettersMult + extLettersAdd);
  return all(hitAbs > 0) ? map : 0;
}

float estimateElevation(float2 uv, float4 texValue) {  
  float2 hit = uv * extBorderedArea.xy + extBorderedArea.zw; // gives -1…1 if value within box, 0 for center, linear
  float2 hitAbs = 1 - abs(hit); // gives 1…0 value within box, 1 for center
  float ret = saturate(pow(length(1 - saturate(hitAbs * extBorderedRadius)), 8));
  if (extLettersExtrude) ret = max(ret, estimateLetters(uv, texValue));
  return ret;
}

float getHeight(float2 uv, float2 dx, float2 dy) {
  return 1 - estimateElevation(uv, txDiffuse.SampleGrad(samLinearSimple, uv, dx/4, dy/4));
}

float3 getNormalWCustom(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW) {
  #ifndef GETNORMALW_NORMALIZED_INPUT
    normalW = normalize(normalW);
  #endif

  #ifdef NO_NORMALMAPS
    return normalW;
  #else
    float3 T = tangentW;
    float3 B = bitangentW;
    float3 txNormalValue = txNormal.Sample(samLinear, uv).xyz;
    float3 txNormalAligned = txNormalValue * 2 - (float3)1;
    float3x3 m = float3x3(T, normalW, B);
    return normalize(mul(transpose(m), txNormalAligned.xzy));
  #endif
}

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float extMinDisplacementDistance = 30;
  float extMaxDisplacementDistance = 0.5;

  float d = length(pin.PosC);
  float tess = saturate((extMinDisplacementDistance - d) / (extMinDisplacementDistance - extMaxDisplacementDistance));
  float height = 1;

  float2 texDx = ddx(pin.Tex);
  float2 texDy = ddy(pin.Tex);
  pin.Tex = frac(pin.Tex);

  #if defined(ALLOW_PARALLAX)
    [branch]
    if (tess > 0) {
      float3 eyeTS;
      eyeTS.x = dot(-toCamera, tangentW);
      eyeTS.y = dot(-toCamera, bitangentW) * extWidthRatio;
      eyeTS.z = dot(-toCamera, normalW);
      eyeTS = normalize(eyeTS);
      pin.Tex = parallaxMap(pin.Tex, eyeTS, texDx, texDy, tess, height);

      #if defined(USE_RELIEF_PARALLAX) && !defined(NO_SHADOWS)
        float3 lightTS;
        lightTS.x = dot(-ksLightDirection.xyz, tangentW);
        lightTS.y = dot(-ksLightDirection.xyz, bitangentW) * extWidthRatio;
        lightTS.z = dot(-ksLightDirection.xyz, normalW);
        lightTS = normalize(lightTS);    
        shadow *= parallaxHardShadowMultiplier(lightTS, pin.Tex, height, texDx, texDy, tess);
      #endif
    }
  #endif

  #if defined(ALLOW_RAINFX) && !defined(MODE_SIMPLIFIED_FX)
    pin.PosR -= pin.NormalR * height * parallaxScale;
  #endif

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  RAINFX_WET(txDiffuseValue.xyz);

  float2 tex = pin.Tex;
  float isElevated = estimateElevation(tex, txDiffuseValue);
  float isLetter = estimateLetters(tex, txDiffuseValue);

  float sideDimK0 = 8;
  float sideDimK1 = 2;
  float elevatedMult = 1 - min(saturate(sideDimK0 - height * sideDimK0), saturate(sideDimK1 - (1 - height) * sideDimK1));
  txDiffuseValue.rgb *= elevatedMult * (1 - isElevated * extDarkeningFactor);
  txMapsValue.xz *= elevatedMult * (1 - isElevated * extOcclusionFactor);

  normalW = getNormalWCustom(pin.Tex, normalW, tangentW, bitangentW);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);

  float fadeX = pow(abs(tex.x * 2 - 1), 2);
  float emissiveEstimate = lerp(1, extEmissiveShapeMult * (extEmissiveFadeX < 0 ? fadeX : 1 - fadeX), abs(extEmissiveFadeX));
  emissiveEstimate *= lerp(1, 1 - pow(extEmissiveFadeY < 0 ? 1 - tex.y : tex.y, 2), abs(extEmissiveFadeY));

  float lettersGlow = pow(isLetter, 2) * saturate(lerp(1,
    (1 - pow(abs(tex.y * 2 - 1), 2)) * (0.2 + 0.8 * abs(sin(tex.x * extGlowingLettersLampsPI))), extGlowingLettersLampsFactor));

  // First channel is used for fake lighting, not really an emissive thing
  float4 emissiveMap = float4(0, lettersGlow, 0, 0);
  float3 emissiveValue = emissiveEstimate * txDiffuseValue.rgb * ksEmissive;
  emissiveValue += ksEmissive1 * emissiveMap.y;

  if (extDebugMode){
   txDiffuseValue.rgb = float3(isLetter, isElevated, 0);
  }

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txEmissiveValue = emissiveValue;
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = calculateMapsLighting_woSun(L);
  BOUNCEBACKFX(lighting, 1 - isElevated);

  float3 lightingBasic = lighting;
  #ifdef ALLOW_LIGHTINGFX
    LFX_MainLight mainLight;
    LIGHTINGFX(lighting);
  #endif

  #if defined(ALLOW_LIGHTINGFX) && defined(ALLOW_PARALLAX) && defined(USE_RELIEF_PARALLAX) && !defined(NO_SHADOWS)
    [branch]
    if (tess > 0 && dot(mainLight.dir, 1)) {
      float3 lightTS;
      lightTS.x = dot(mainLight.dir, tangentW);
      lightTS.y = dot(mainLight.dir, bitangentW) * extWidthRatio;
      lightTS.z = dot(mainLight.dir, normalW);
      lightTS = normalize(lightTS);    
      float lightingFocus = saturate(mainLight.power / max(dot(lighting - lightingBasic, 1), 0.001) * 2 - 1);
      float3 dynShadow = parallaxHardShadowMultiplier(lightTS, pin.Tex, height, texDx, texDy, tess);
      lighting = lerp(lightingBasic, lighting, lerp(1, dynShadow, lightingFocus * 0.8 * tess));
    }
  #endif

  ReflParams R = getReflParams(1, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);

  RAINFX_WATER(withReflection);
  RETURN(withReflection, saturate(1 + useEverything()));
}
