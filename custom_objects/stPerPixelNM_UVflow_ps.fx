#define CARPAINT_NM
#define GETNORMALW_SAMPLER samLinear
#define SUPPORTS_AO
#define GAMMA_TWEAKABLE_ALPHA

#define CB_MATERIAL_EXTRA_4\
  float2 offsetDSpeed;\
  float2 offsetNMSpeed;\
  float NMmult;\
  float detailNMmult;\
  float2 offsetNMdetailSpeed;\
  float2 pauseTiming;\
  float uvMultX;\
  float uvMultY;\
  float uvOffsetX;\
  float uvOffsetY;\
  float emAlphaFromDiffuse;\
  float emClipOutside;

#include "include_new/base/_include_ps.fx"

// object space removed cause it doesn't make sense for the 2nd uvmap to have it
float3 getNormalW_tex(float2 uv, float3 normalW, float3 targentW, float3 bitangentW, Texture2D txNorm){
  #ifndef GETNORMALW_NORMALIZED_INPUT
    normalW = normalize(normalW);
  #endif
  #ifdef GETNORMALW_NORMALIZED_TB
      float3 T = targentW;
      float3 B = bitangentW;
  #else
      float3 T = normalize(targentW);
      float3 B = normalize(bitangentW);
  #endif
  B = cross(normalW, T);
  T = cross(normalW, B);
  float3 txNormalValue = txNorm.Sample(samLinearSimple, uv).xyz;
  float3 txNormalAligned = txNormalValue * 2 - (float3)1;
  float3x3 m = float3x3(T, normalW, B);
  return normalize(mul(transpose(m), txNormalAligned.xzy));
}

RESULT_TYPE main(PS_IN_Nm pin) {
  if (uvMultX || uvMultY) {
    pin.Tex *= float2(uvMultX, uvMultY);
  }
  pin.Tex += float2(uvOffsetX, uvOffsetY);

  float v = ksGameTime * 0.001;
  if (pauseTiming.x > 0) {
    float modval = v%(1+pauseTiming.x+pauseTiming.y);
    float decrate = 0;
    if (pauseTiming.y > -1) {
      decrate = pauseTiming.x/(1+pauseTiming.y);
    }
    v -= min(modval,pauseTiming.x) - decrate *  max(0, modval - pauseTiming.x);
  }
  float2 movUV = pin.Tex + offsetDSpeed * v;
  float2 movnmUV = pin.Tex * (1.0+NMmult) + offsetNMSpeed * v;
  float2 movUVd = pin.Tex * detailNMmult + offsetNMdetailSpeed * v;

  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float alpha, directedMult;
  normalW = getNormalW(movnmUV, normalW, tangentW, bitangentW, alpha, directedMult);
  #if !defined(NO_OPTIONAL_MAPS) 
    normalW = getNormalW_tex(movUVd, normalW, tangentW, bitangentW, txDetailNM);
  #endif

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, movUV);
  RAINFX_WET(txDiffuseValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR;
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, 1);
  R.useBias = true;
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);

  if (abs(emAlphaFromDiffuse) == 1) alpha = txDiffuseValue.a;

  #ifdef MODE_SHADOWS_ADVANCED
    float4 withReflection = float4(lighting, alpha);
  #else
    float4 withReflection = calculateReflection(float4(lighting, alpha), toCamera, pin.PosC, normalW, R);
    if (HAS_FLAG(FLAG_ALPHA_TEST)
    #ifdef MODE_GBUFFER
      || HAS_FLAG(FLAG_GBUFFER_PREMULTIPLIED_ALPHA)
    #endif
    ) withReflection.a = alpha;
  #endif
  
  RAINFX_WATER(withReflection);
  alpha = emClipOutside && any(abs(movUV - 0.5) > 0.5) ? 0 : emAlphaFromDiffuse <= 0 ? withReflection.a : alpha;

  #ifdef MODE_SHADOWS_ADVANCED
    clip(alpha - ksAlphaRef - 0.0001);
  #endif

  RETURN(withReflection, alpha);
}
