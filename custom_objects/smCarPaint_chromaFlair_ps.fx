#define HAS_CHROMA_FLAIR

#define EXTRA_CARPAINT_FIELDS_2\
  float extChromaFlairMask;\
  float extChromaFlairFlakesMult;

#include "smCarPaint_ps.fx"
