#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define NO_SH_FOR_REFLECTIONS
#define GETNORMALW_XYZ_TX

#ifdef GETNORMALW_SAMPLER
  #undef GETNORMALW_SAMPLER
#endif
#define GETNORMALW_SAMPLER samLinearSimple

#define SUPPORTS_AO
#define IS_ADDITIVE_VAR 0
#define SAMPLE_REFLECTION_FN sampleReflectionDoubleCube

#ifdef USE_LOCAL_TAA
  #define FOG_FEEDBACK
#endif

struct ReflSample {
  float shadow;
  float3 extraLighting;
  bool mirrorLocalCubemap;
  float3 mirrorLocalCubemapDir;
};

#define REFL_SAMPLE_PARAM_TYPE ReflSample
float3 sampleReflectionDoubleCube(float3 reflDir, float blur, bool useBias, ReflSample extraParam);

#include "include_new/base/_include_ps.fx"
#include "common/refractingCover.hlsl"

TextureCube txCubeSecond : register(TX_SLOT_MAT_8);

cbuffer cbCubeSecondTransform : register(b10) {  
  float4x4 extCubeSecondTransform;
}

#ifdef USE_LOCAL_TAA
  float2 getMotion(float3 pos0, float3 pos1){
    float2 vVelocity = (pos0.xy / pos0.z) - (pos1.xy / pos1.z);
    vVelocity *= 0.5f;
    vVelocity.y *= -1;
    return vVelocity;
  }
#endif

#define GET_MOTION(x) getMotion(x.PosCS0, x.PosCS1)

float3 sampleReflectionDoubleCube(float3 reflDir, float blur, bool useBias, ReflSample extraParam){
  float3 reflCubeDir = float3(-reflDir.x, reflDir.y, reflDir.z);
  float3 localReflDir = reflDir;
  if (extraParam.mirrorLocalCubemap) localReflDir = reflect(localReflDir, extraParam.mirrorLocalCubemapDir);
  float3 reflDir2 = mul(localReflDir, (float3x3)extCubeSecondTransform);
  float3 reflCube2Dir = float3(-reflDir2.x, reflDir2.y, reflDir2.z);

  float3 first;
  float4 second;

  if (useBias){
    float level = txCube.CalculateLevelOfDetail(samLinearSimple, reflDir);
    first = txCube.SampleLevel(samLinearSimple, reflCubeDir, blur + level * saturate(2 - blur)).rgb;
    second = txCubeSecond.SampleLevel(samLinearSimple, reflCube2Dir, blur - 2 + level * saturate(2 - blur));
  } else {
    first = txCube.SampleLevel(samLinearSimple, reflCubeDir, blur).rgb;
    second = txCubeSecond.SampleLevel(samLinearSimple, reflCube2Dir, blur - 2);
  }

  second.rgb = applyGamma(second.rgb, 1);
  second.rgb *= AMBIENT_COLOR_NEARBY * perObjAmbientMultiplier + ksLightColor.rgb * extraParam.shadow + extraParam.extraLighting;
  return lerp(first.rgb, second.rgb, second.a);
}

float3 getInnerUVDepth(float3 posC){
  float3 dif = posC - (extInnerPos - ksCameraPosition.xyz);
  float up = dot(dif, extInnerUp);
  float side = dot(dif, extInnerSide);
  float2 uv = float2(up, side) / extInnerRadius * 0.5 + 0.5;
  float depth = dot(dif, extInnerDir) / extInnerDepth;
  return float3(uv.yx * float2(-1, -1), depth);
}

float3 getInnerUVDepthDir(float3 dir, out float dirZ){
  float3 dif = dir;
  float up = dot(dif, extInnerUp);
  float side = dot(dif, extInnerSide);
  float2 uv = float2(up, side) / extInnerRadius * 0.5;
  dirZ = dot(dif, extInnerDir);
  float depth = dirZ / extInnerDepth;
  return float3(uv.yx * float2(-1, -1), depth);
}

float3 getPosFromUVDepth(float3 uvDepth){
  return (extInnerPos - ksCameraPosition.xyz) 
    + extInnerUp * (-uvDepth.y * 2 - 1) * extInnerRadius
    + extInnerSide * (-uvDepth.x * 2 - 1) * extInnerRadius
    + extInnerDir * uvDepth.z * extInnerDepth;
}

struct Hit {
  float2 uv;
  float depth;
  float3 pos;
  bool outsideHit;
};

void snap(inout float3 v){
  v.x = round(v.x * 1e2) / 1e2;
  v.y = round(v.y * 1e2) / 1e2;
  v.z = round(v.z * 1e2) / 1e2;
}

Hit raycast_(float3 origin, float3 dir, bool includeOutside, int steps = 8){
  // snap(origin);
  // snap(dir);

  float3 uvValue = getInnerUVDepth(origin);
  float3 rayDir = dir;
  float3 rayPos = origin;
  float stepSize = extRaytraceStepStart;
  bool goForOutside = false;
  float depthValue;
  if (steps){
    for (int i = 0; i < steps; i++){
      float3 oldRayPos = rayPos;
      rayPos += rayDir * stepSize * min(1, 8 / (float)steps);
      stepSize *= extRaytraceStepIncrease;
      uvValue = getInnerUVDepth(rayPos);
      depthValue = txInnerDepth.SampleLevel(samLinear, uvValue.xy, 0);
      if (includeOutside && uvValue.z <= 0.01){
        goForOutside = true;
        break;
      }
      if (depthValue < uvValue.z + (includeOutside ? -0.0001 : 0.001)) {
        rayPos -= rayDir * stepSize;
        break;
      }
    }
  }

  if (goForOutside){
    rayPos = rayPos + rayDir * uvValue.z / abs(dot(rayDir, extInnerDir));
    uvValue = getInnerUVDepth(rayPos);
  } else {
    for (int j = 0, t = steps ? 4 : 6; j < t; j++){
      rayPos = rayPos + rayDir * (depthValue - uvValue.z) / 2;
      uvValue = getInnerUVDepth(rayPos);
      depthValue = txInnerDepth.SampleLevel(samLinear, uvValue.xy, 0);
    }
  }

  Hit ret;
  ret.uv = uvValue.xy;
  ret.depth = uvValue.z;
  ret.pos = rayPos;
  ret.outsideHit = uvValue.z <= 0.01 
    || txSurfaceNormal.SampleLevel(samLinearBorder0, 1 + uvValue.xy, 3.5).a < 0.01;
  return ret;
}

Hit raycast(float3 origin, float3 dir, bool includeOutside, int steps = 8){
  // snap(origin);
  // snap(dir);

  float3 uvValue = getInnerUVDepth(origin);
  float rayDirZ;
  float3 rayDir = getInnerUVDepthDir(dir, rayDirZ);
  
  // snap(rayPos);
  // snap(rayDir);

  float stepSize = extRaytraceStepStart * min(4, 8 / (float)steps);
  bool goForOutside = false;
  float depthValue;

  if (includeOutside) uvValue += rayDir * 0.03;
  for (int j = 0, t = max(steps, 2); j < t; j++){
    if (includeOutside){
      depthValue = txInnerDepth.SampleLevel(samLinear, uvValue.xy, 0);
      uvValue += rayDir * (depthValue - uvValue.z);

      if (uvValue.z <= 0.1){
        break;
      }
    } else {
      float4 depthG = txInnerDepth.GatherRed(samLinear, uvValue.xy, int2(0, 0));
      float depthMin = min(min(depthG.x, depthG.y), min(depthG.z, depthG.w));
      float depthMax = max(max(depthG.x, depthG.y), max(depthG.z, depthG.w));
      if (depthMin < uvValue.z && depthMax > uvValue.z){
        break;
      }

      depthValue = txInnerDepth.SampleLevel(samLinear, uvValue.xy, 0);
      uvValue += rayDir * (depthValue - uvValue.z) * 0.4;
    }
  }

  Hit ret;
  ret.uv = uvValue.xy;
  ret.depth = uvValue.z;
  ret.pos = getPosFromUVDepth(uvValue);
  ret.outsideHit = uvValue.z <= 0.1 
    || txSurfaceNormal.SampleLevel(samLinearBorder0, 1 + uvValue.xy, 3.5).a < 0.01;
  return ret;
}

struct Result {
  float3 debug;
  float3 lighting;
  float3 emissive;
  float3 rayDir;
  float reflectionPart;
  float reflectionMult;
  float glassDistance;
  float sideK;
  float bulbShape;
};

struct CalculateInput {
  float3 baseNormal;
  float3 glassExtNormal;
  float3 glassIntNormal;
  float3 posC;
  float3 toCamera;
  bool isMirrored;
};

float schlick(float f0, float f90, float u){
  return f0 + (f90 - f0) * pow(1 - saturate(-u), 5);
}

float reflectionMult(float2 uv, float minMult = 0.5){
  float3 surfaceColor = txInnerDiffuse.Sample(samLinearSimple, uv).rgb;
  return max(minMult, saturate(extReflectionDiffuseMult * luminance(surfaceColor)));
}

float4 getEmissiveColor(float4 emissiveMap, bool bulbMode, bool isMirrored){
  // return emissiveMap;
  if (ksEmissiveSeparate){
    if (bulbMode){
      emissiveMap = max(emissiveMap - 0.1, 0);
    } else {
      emissiveMap = saturate(emissiveMap * 10);
    }
    float4 baseColor = float4(emissiveMap.r * ksEmissive
      + emissiveMap.g * ksEmissive1
      + emissiveMap.b * (emMirrorChannel2As5 && isMirrored ? ksEmissive5 : ksEmissive2)
      + emissiveMap.a * (ksEmissiveSeparate < 0 && isMirrored ? ksEmissive4 : ksEmissive3), saturate(dot(emissiveMap, 1)));
    // baseColor += ksEmissive3;
    // float3 baseColor = (emissiveMap.rgb + emissiveMap.a) * 20;
    return max(baseColor, 0);
  } else {
    return float4(INPUT_EMISSIVE3 * (bulbMode ? emissiveMap.rgb * emissiveMap.a : emissiveMap.rgb), emissiveMap.a);
  }
}

float4 bulbContributionMain(float2 uv, bool isMirrored, float blur = 0){
  // float4 val = 0;
  // float count = 0;
  // [unroll] for (int x = -1; x <= 1; x++){
  //   [unroll] for (int y = -1; y <= 1; y++){
  //     if (abs(x) + abs(y) == 2) continue;
  //     val += txInnerEmissive.SampleLevel(samLinear, uv, blur, int2(x, y));
  //     count++;
  //   }
  // }
  float4 val = txInnerEmissive.SampleLevel(samLinear, uv, blur);
  float count = 1;
  return getEmissiveColor(val / count, true, isMirrored);
}

float4 bulbContributionBlurred(float2 uv, bool isMirrored, float blur = 0){
  float4 val = 0;
  float count = 0;
  [unroll] for (int x = -1; x <= 1; x++){
    [unroll] for (int y = -1; y <= 1; y++){
      if (abs(x) + abs(y) == 2) continue;
      val += txInnerEmissive.SampleLevel(samLinear, uv, blur, int2(x, y));
      count++;
    }
  }
  return getEmissiveColor(val / count, true, isMirrored);
}

float4 bulbContribution(float2 uv, bool isMirrored){
  // return bulbContributionMain(uv, 4);
  return getEmissiveColor(txInnerEmissive.SampleLevel(samLinear, uv, extBlurMult), true, isMirrored);
}

float sideFalloff(float2 uv){
  float2 val = 0;
  [unroll] for (int x = -1; x <= 1; x++){
    [unroll] for (int y = -1; y <= 1; y++){
      if (abs(x) + abs(y) == 2) continue;
      float4 sam = txSurfaceNormal.SampleLevel(samLinearSimple, uv, max(1, extSideFalloff), int2(x, y));
      val += float2(sam.a, 1);
    }
  }
  return val.x / val.y;
}

float4 innerNormal(float2 uv){
  float4 texValue = txInnerNormal.SampleLevel(samLinear, uv, 0);
  return float4(normalize(mul(texValue.xyz, (float3x3)extNormalTransform)), 
    saturate((texValue.a + 0.01) * extReflectionMult));
}

float reflectionBounceMult(float3 ray, float4 normal, out float f0){
  f0 = pow(saturate(extF0 * normal.a), extReflectiveGamma);
  float f90 = saturate(f0 * 50);
  return schlick(f0, f90, dot(ray, normal.xyz));
}

float reflectionBounceMult(float3 ray, float4 normal){
  float f0;
  return reflectionBounceMult(ray, normal, f0);
}

float3 outerNormal(float2 uv){
  return normalize(mul(txSurfaceNormal.SampleBias(samLinearSimple, uv, 1).xyz, (float3x3)extNormalTransform));
}

float3 outerSmoothNormal(float2 uv){
  return normalize(mul(txSurfaceNormal.SampleBias(samLinearSimple, uv, 4).xyz, (float3x3)extNormalTransform));
}

void refractRay(inout float3 rayDir, inout float3 rayPos, inout float glassDistance,
    float3 normal, float ior){
  rayDir = refract(rayDir, normal, ior);
  float distanceMult = 1 / max(0.05, saturate(dot(normal, -rayDir)));
  glassDistance += distanceMult;
  rayPos += rayDir * extGlassExtraThickness * distanceMult;
}

Result calculateValue(CalculateInput IN, float surfaceShadow){
  Result R;
  R.debug = 0;
  R.lighting = 0;
  R.emissive = 0;
  R.reflectionPart = 1;
  R.reflectionMult = 1;
  R.bulbShape = 0;

  float3 startUVDepth = getInnerUVDepth(IN.posC);
  float startDepth = startUVDepth.z;
  R.sideK = pow(saturate((1 - abs(dot(IN.baseNormal, extInnerDir))) * 1.2), 2);
  R.glassDistance = pow(R.sideK, 2) * extExtraSideThickness;
  float glassReflDistance = R.glassDistance;

  R.rayDir = IN.toCamera;
  refractRay(R.rayDir, IN.posC, R.glassDistance, IN.glassExtNormal, extIORInv);
  refractRay(R.rayDir, IN.posC, R.glassDistance, IN.glassIntNormal, extIOR);

  if (dot(R.rayDir, 1) == 0){
    R.rayDir = reflect(IN.toCamera, IN.glassExtNormal);
    R.reflectionPart = 1;
    R.reflectionMult = schlick(extF0, 1, dot(IN.toCamera, IN.glassExtNormal));
  } else {
    Hit hit0 = raycast(IN.posC, R.rayDir, false, 12);
    float4 bulbHit = bulbContributionMain(hit0.uv, IN.isMirrored) * 3;

    float firstHitF0;
    float4 innerSurface = innerNormal(hit0.uv);
    R.reflectionMult *= reflectionBounceMult(R.rayDir, innerSurface, firstHitF0);
    R.rayDir = normalize(reflect(R.rayDir, innerSurface.xyz));
    Hit hit1 = raycast(hit0.pos, R.rayDir, true);
    bulbHit += hit1.outsideHit ? 0 : bulbContribution(hit1.uv, IN.isMirrored) * R.reflectionMult;

    float3 reflectedRay = R.rayDir;

    if (!hit1.outsideHit){
      float4 innerSurfaceSecond = innerNormal(hit1.uv);
      R.reflectionMult *= reflectionBounceMult(R.rayDir, innerSurfaceSecond);
      R.rayDir = normalize(reflect(R.rayDir, innerSurfaceSecond.xyz));
      hit1 = raycast(hit1.pos, R.rayDir, true, 4);
      bulbHit += hit1.outsideHit ? 0 : bulbContribution(hit1.uv, IN.isMirrored) * R.reflectionMult;
    }

    // #ifndef USE_MULTISAMPLING_SHADING
    //   if (!hit1.outsideHit){
    //     float4 innerSurfaceSecond = innerNormal(hit1.uv);
    //     R.reflectionMult *= reflectionBounceMult(R.rayDir, innerSurfaceSecond);
    //     R.rayDir = normalize(reflect(R.rayDir, innerSurfaceSecond.xyz));
    //     hit1 = raycast(hit1.pos, R.rayDir, true, 2);
    //     bulbHit += hit1.outsideHit ? 0 : bulbContribution(hit1.uv, IN.isMirrored) * R.reflectionMult;
    //   }
    // #endif

    float3 outerSurfaceNormal = outerNormal(hit1.uv);
    float3 outerSurfaceSmoothNormal = outerSmoothNormal(hit1.uv);
    float3 outerExtNormal = normalize(lerp(outerSurfaceSmoothNormal, outerSurfaceNormal, extNmShareExt));
    float3 outerInNormal = normalize(lerp(outerSurfaceSmoothNormal, outerSurfaceNormal, extNmShareInt));

    glassReflDistance = R.glassDistance;

    refractRay(R.rayDir, IN.posC, glassReflDistance, -outerInNormal, extIORInv);
    refractRay(R.rayDir, IN.posC, glassReflDistance, -outerExtNormal, extIORFlyOut);
    if (dot(R.rayDir, 1) == 0){
      R.rayDir = -extInnerDir;
      R.reflectionMult *= extBouncedBackMult;
    }

    R.bulbShape = bulbHit.w;
    {
      float3 surfaceColor = txInnerDiffuse.Sample(samLinearSimple, hit0.uv).rgb;
      surfaceColor = applyGamma(surfaceColor, 1);
      // surfaceColor += float3(0.25, 0.25, 0) * saturate(bulbHit);
      float3 toCamera = normalize(hit0.pos);
      float3 lightDir = -ksLightDirection.xyz;
      float shadow = surfaceShadow;

      Hit lightHit = raycast(hit0.pos, -ksLightDirection.xyz, true, 4);
      if (!lightHit.outsideHit) shadow = 0;
      // float3 outerSurfaceNormal = outerNormal(lightHit.uv);
      // float3 outerSurfaceSmoothNormal = outerSmoothNormal(lightHit.uv);
      // float3 outerExtNormal = normalize(lerp(outerSurfaceSmoothNormal, outerSurfaceNormal, extNmShareExt));
      // float3 outerInNormal = normalize(lerp(outerSurfaceSmoothNormal, outerSurfaceNormal, extNmShareInt));
      // float lightGlassDistance = 0;
      // refractRay(lightDir, IN.posC, lightGlassDistance, -outerInNormal, extIORInv);
      // refractRay(lightDir, IN.posC, lightGlassDistance, -outerExtNormal, extIOR);

      float specularBase = saturate(dot(normalize(-toCamera + lightDir), innerSurface.xyz));
      float3 specularPart = pow(specularBase, GAMMA_BLINNPHONG_EXP(extInnerSpecularEXP)) * GAMMA_BLINNPHONG_ADJ(extInnerSpecular) * extSpecularColor;

      float extraReflectionMult = saturate(bulbHit.a * extBulbReflectionK + saturate(extReflectionDiffuseMult * max(0.2, luminance(surfaceColor))));
      float3 reflectedHit = hit0.pos + reflectedRay * (hit0.depth - startDepth) / abs(dot(reflectedRay, extInnerDir));
      float3 reflectedHitUV = getInnerUVDepth(reflectedHit);
      float2 coverUV = lerp(hit0.uv, reflectedHitUV.xy, saturate((hit0.depth - startDepth) * 8));
      extraReflectionMult *= extHasOverlay ? 1 - txOverlayMask.SampleLevel(samLinearSimple, coverUV, 0).a : 1;

      float LdotN = pow(saturate(dot(innerSurface.xyz, lightDir)), extLambertGamma);
      float ambientShadow = extHasOverlay ? 1 - txOverlayMask.SampleLevel(samLinearSimple, hit0.uv, max(hit0.depth - startDepth, 0) * 80).a : 1;
      R.lighting += (getAmbientBaseAt(IN.posC, innerSurface.xyz, ambientShadow) * extAmbientMult + LdotN * ksLightColor.xyz * shadow) * surfaceColor / 2;
      R.lighting += LdotN * specularPart * shadow * firstHitF0 * extraReflectionMult;
      #ifdef ALLOW_LIGHTINGFX
        #ifdef LIGHTINGFX_RAINFX
          R.lighting += getDynamicLights(innerSurface.xyz, innerSurface.xyz, hit0.pos, normalize(hit0.pos), surfaceColor, 
            extInnerSpecularEXP / 4, extInnerSpecular / 4, 0, 0, (float3)0, 0);
        #else
          R.lighting += getDynamicLights(innerSurface.xyz, hit0.pos, normalize(hit0.pos), surfaceColor, 
            extInnerSpecularEXP / 4, extInnerSpecular / 4, 0, 0);
        #endif
      #endif

      float3 baseHit = getEmissiveColor(txInnerEmissive.SampleBias(samLinearSimple, startUVDepth.xy, 3), false, IN.isMirrored).rgb;
      float3 surfaceHit = getEmissiveColor(txInnerEmissive.SampleBias(samLinearSimple, hit0.uv, extBlurMult), false, IN.isMirrored).rgb;
      R.emissive += extKsEmissiveMult * (bulbHit.rgb * saturate(dot(baseHit, 1))
        + extBaseEmissiveK * surfaceHit
        + lerp(0.2, 1, extBaseEmissiveK) * bulbContributionBlurred(hit0.uv, IN.isMirrored, 4.5).rgb * 2);
      // R.emissive += extKsEmissiveMult * surfaceHit;
      // R.emissive += bulbHit * 10;

      R.reflectionMult *= extraReflectionMult;
    }

    // R.debug += hit1.outsideHit ? 2 : txInnerEmissive.SampleLevel(samLinearSimple, hit1.uv, 0).xyz * 2;
    // R.debug += 0.001 + txOverlayMask.SampleLevel(samLinearSimple, hit0.uv, max(hit0.depth - startDepth, 0) * 80).xyz;
    // R.debug += 0.001 + txOverlayMask.SampleLevel(samLinearSimple, getInnerUVDepth(IN.posC).xy, 0).xyz;
    // R.debug += txSurfaceNormal.SampleLevel(samLinearSimple, hit0.uv, 0).xyz;
    // R.debug += txSurfaceNormal.SampleLevel(samLinearSimple, getInnerUVDepth(IN.posC).xy, 0).xyz;
    // R.debug += txInnerEmissive.SampleLevel(samLinearSimple, getInnerUVDepth(IN.posC).xy, 5).w * 100 + 0.0001;
    // R.debug += bulbHit + 0.01;
    // R.debug += innerNormal(hit0.uv);
    // R.debug.xy += frac(abs(hit0.uv) * 10);
    R.debug += outerSurfaceNormal;
    // R.debug += innerSurface.a;
  }

  // R.reflectionMult *= exp(-extAbsorptionFactor * glassReflDistance);
  // R.emissive += emissiveInput / 1000l;

  // float surfaceLdotN = saturate(dot(IN.glassExtNormal, -ksLightDirection.xyz));
  // float3 glassColor = extGlassColor * (ksAmbientColor.xyz * (IN.glassExtNormal.y * 0.4 + 0.6) * extGlassAmbientMult
  //   + surfaceLdotN * ksLightColor.xyz * surfaceShadow 
  //   + extGlassEmissiveMult * emissiveInput * (1 - sideK));
  // R.lighting = lerp(glassColor, R.lighting, exp(-extAbsorptionFactor * R.glassDistance));
  // R.absorption = exp(-extAbsorptionFactor * glassDistance);

  return R;
}

float3 getNormalWBias(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW, out float alpha){
  float3 T = tangentW;
  float3 B = bitangentW;
  float bias = extLODBias;
  #ifdef MODE_COLORSAMPLE
    bias = 0;
  #endif

  // float4 txNormalValue = txNormal.SampleBias(samLinearSimple, uv, bias);
  float level = txNormal.CalculateLevelOfDetail(samLinear, uv);
  float4 txNormalValue = txNormal.SampleLevel(samLinear, uv, bias < 0 ? min(level + bias, 1) : level);

  float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
  float3x3 m = float3x3(T, normalW, B);
  alpha = txNormalValue.a;
  return normalize(mul(transpose(m), txNormalAligned.xzy));
}

struct Contribution {
  float3 innerLighting;
  float3 innerEmissive;
  float3 reflection;
  float glassDistance;
  float sideK;
  float3 debug;
};

Contribution getContribution(inout CalculateInput I0, float3 baseNormalW, float3 normalW, float3 posC,
    float shadow, float2 extraShadow, ReflSample RS){  
  float3 normalBase = normalize(baseNormalW);
  float3 normalExtInv = reflect(-normalW, normalBase);
  I0.glassExtNormal = normalize(lerp(normalBase, normalExtInv, extNmShareExt));
  I0.glassIntNormal = normalize(lerp(normalBase, normalW, extNmShareInt));
  Result R0 = calculateValue(I0, shadow);

  if (RS.mirrorLocalCubemap){
    R0.rayDir = reflect(R0.rayDir, extMirrorPlane.xyz);
  }
  
  #ifndef ADVANCED_GLASS
    R0 = (Result)0;
  #endif

  float3 bulbColor = lerp(1, extBulbColor, saturate(R0.bulbShape * 2));

  ReflParams R;
  R = getReflParamsBase(EXTRA_SHADOW_REFLECTION, 1);
  R.fresnelMaxLevel = R0.reflectionPart;
  R.fresnelC = 1;
  R.fresnelEXP = 5;
  R.ksSpecularEXP = 255 * pow(R0.reflectionMult, 2);
  R.finalMult = R0.reflectionMult;
  R.metallicFix = 0;
  R.useBias = true;
  R.isCarPaint = false;
  R.forceReflectedDir = true;
  R.forcedReflectedDir = R0.rayDir;
  R.reflectionSampleParam = RS;
  
  Contribution C;
  C.innerLighting = R0.lighting * GAMMA_LINEAR_SIMPLE(bulbColor);
  C.innerEmissive = R0.emissive;
  C.reflection = calculateReflection((float3)0, I0.toCamera, posC, I0.glassExtNormal, R);
  C.reflection *= GAMMA_LINEAR_SIMPLE(bulbColor * lerp(0.3, 1, saturate(remap(R0.rayDir.y, -0.01, 0.01, 0, 1))));
  C.glassDistance = R0.glassDistance;
  C.sideK = R0.sideK;
  C.debug = R0.debug;
  return C;
}

float3 getContribValue(Contribution C, float3 glassFiltering){
  glassFiltering = GAMMA_LINEAR_SIMPLE(glassFiltering);
  return (C.innerLighting + GAMMA_KSEMISSIVE(C.innerEmissive) * getEmissiveMult() + C.reflection * glassFiltering) * glassFiltering;
}

float4 calculatePrevFrame(float4 currentValue, float2 prevUV, inout float blendFactor){
  // return txPrevFrame.SampleLevel(samLinearClamp, prevUV, 0);
  // blendFactor = 1;
  // return 0;

  // float4 nbh[9];
  // nbh[0] = txPrevFrame.SampleLevel(samPointClamp, prevUV, 0, int2(-1, -1));
  // nbh[1] = txPrevFrame.SampleLevel(samPointClamp, prevUV, 0, int2(0, -1));
  // nbh[2] = txPrevFrame.SampleLevel(samPointClamp, prevUV, 0, int2(1, -1));
  // nbh[3] = txPrevFrame.SampleLevel(samPointClamp, prevUV, 0, int2(-1, 0));
  // nbh[4] = txPrevFrame.SampleLevel(samLinearClamp, prevUV, 0, int2(0, 0));
  // nbh[5] = txPrevFrame.SampleLevel(samPointClamp, prevUV, 0, int2(1, 0));
  // nbh[6] = txPrevFrame.SampleLevel(samPointClamp, prevUV, 0, int2(-1, 1));
  // nbh[7] = txPrevFrame.SampleLevel(samPointClamp, prevUV, 0, int2(0, 1));
  // nbh[8] = txPrevFrame.SampleLevel(samPointClamp, prevUV, 0, int2(1, 1));
  // float4 nbhMin = nbh[0];
  // float4 nbhMax = nbh[0];
  // float4 nbhAvg = 0;
  // float4 nbhBlur = 0;    
  // {
  //   [unroll]
  //   for (uint i = 1; i < 9; i++) {
  //     float4 n = nbh[i];
  //     nbhMin = min(nbhMin, n);
  //     nbhMax = max(nbhMax, n);
  //   }
  // }
  // if (any(currentValue.rgb < nbhMin.rgb || currentValue.rgb > nbhMax.rgb)){
  //   blendFactor = 1;
  // }
  // return nbh[4];

  float4 ret = txPrevFrame.SampleLevel(samLinearClamp, prevUV, 0);
  prevUV += extScreenSize.zw * 0.5;

  float4 nbh[9];
  nbh[0] = txPrevFrame.GatherGreen(samPointClamp, prevUV, int2(-2, -2));
  nbh[1] = txPrevFrame.GatherGreen(samPointClamp, prevUV, int2(0, -2));
  nbh[2] = txPrevFrame.GatherGreen(samPointClamp, prevUV, int2(2, -2));
  nbh[3] = txPrevFrame.GatherGreen(samPointClamp, prevUV, int2(-2, 0));
  nbh[4] = txPrevFrame.GatherGreen(samPointClamp, prevUV, int2(0, 0));
  nbh[5] = txPrevFrame.GatherGreen(samPointClamp, prevUV, int2(2, 0));
  nbh[6] = txPrevFrame.GatherGreen(samPointClamp, prevUV, int2(-2, 2));
  nbh[7] = txPrevFrame.GatherGreen(samPointClamp, prevUV, int2(0, 2));
  nbh[8] = txPrevFrame.GatherGreen(samPointClamp, prevUV, int2(2, 2));

  float4 vmin = nbh[0];
  float4 vmax = nbh[0];
  [unroll]
  for (uint i = 1; i < 9; i++) {
    vmin = min(vmin, nbh[i]);
    vmax = max(vmax, nbh[i]);
  }

  float nmin = min(min(vmin.x, vmin.y), min(vmin.z, vmin.w));
  float nmax = max(max(vmax.x, vmax.y), max(vmax.z, vmax.w));
  if (currentValue.g < nmin || currentValue.g > nmax){ 
    blendFactor = lerp(blendFactor, 1, 0.6);
  }
  return ret;
}

RESULT_TYPE main(PS_IN_RefractingCover pin) {
  READ_VECTORS_NM

  // discard;
  // return (RESULT_TYPE)0;

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseBaseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txDiffuseValue = lerp(float4(extGlassColor, 0.1), txDiffuseBaseValue, extDiffuseMapMult);
  RAINFX_WET(txDiffuseValue.xyz);
  float refractionMask = extUseNormalAlpha < 0 ? txDiffuseBaseValue.a : 1;

  float alpha;
  float3 normalWBase = normalW;
  normalW = getNormalWBias(pin.Tex, normalW, tangentW, bitangentW, alpha);
  alpha = abs(extUseNormalAlpha) == 1 ? alpha : txDiffuseValue.a;

  // float4 flat1 = txNormal.SampleLevel(samLinearSimple, pin.Tex, 0);
  // float4 flat2 = txNormal.SampleLevel(samLinearSimple, pin.Tex, 1);
  // if (dot2(flat1.xyz - float3(0.5, 0.5, 1)) < 0.00001
  //   && dot2(flat2.xyz - float3(0.5, 0.5, 1)) < 0.00001){
  //   discard;
  // }

  #ifdef MODE_NORMALSAMPLE
    // TODO: Instead, add a flag used by utils_ps_gbuff to disable 0…1 remapping
    normalW = normalW * 2 - 1;
    { RETURN(normalW, 1); }
  #endif

  #ifdef ADVANCED_GLASS  
  // #if 1

    ReflSample RS;
    RS.shadow = shadow;
    RS.extraLighting = 0;
    RS.mirrorLocalCubemap = pin.PosV_IsMirrored.w;
    RS.mirrorLocalCubemapDir = extMirrorPlane.xyz;

    // Raytracing stage
    CalculateInput I0;
    I0.posC = pin.PosV_IsMirrored.xyz;
    I0.toCamera = toCamera;
    I0.baseNormal = pin.NormalW;
    I0.isMirrored = pin.PosV_IsMirrored.w;
    float3 normalWM = normalW;

    if (pin.PosV_IsMirrored.w){
      I0.toCamera = reflect(I0.toCamera, extMirrorPlane.xyz);
      I0.baseNormal = reflect(I0.baseNormal, extMirrorPlane.xyz);
      normalWM = reflect(normalWM, extMirrorPlane.xyz);
    }

    float3 startUVDepth = getInnerUVDepth(I0.posC);
    float totalDepth = txInnerDepth.SampleLevel(samLinear, startUVDepth.xy, 0) - startUVDepth.z;

    // Preparing color filtering
    float maxDiffuse = max(txDiffuseValue.x, max(txDiffuseValue.y, txDiffuseValue.z));
    float3 filteringTo = txDiffuseValue.xyz / max(0.01, maxDiffuse);
    float filteringK = abs(extDiffuseMapEmissiveMult) * alpha;
    float3 glassFiltering = extDiffuseMapEmissiveMult > 0 
      ? lerp(1, filteringTo, saturate(filteringK)) : saturate(lerp(1, filteringTo, filteringK));

    // Part with raytracing
    #ifdef ADVANCED_GLASS
      float3 Ct = 0;
      Contribution C;
      #ifdef USE_MULTISAMPLING
        float2 dx = ddx(pin.Tex);
        float2 dy = ddy(pin.Tex);
        // float3 cdx = ddx(toCamera);
        // float3 cdy = ddy(toCamera);
        // [loop] for (int x = 0; x <= 1; x++)
        [loop] for (int y = 0; y <= 1; y++){
          float2 offset = (float2(float(y), float(y)) - 0.5) * 0.5;

          float a_;
          float3 normalW2 = getNormalWBias(pin.Tex + dx * offset.x + dy * offset.y, normalWBase, tangentW, bitangentW, a_);
          float3 normalWM2 = normalW2;
          if (pin.IsMirrored){
            normalWM2 = reflect(normalWM2, extMirrorPlane.xyz);
          }
          // I0.toCamera = normalize(toCamera + cdx * offset.x + cdy * offset.y);
          C = getContribution(I0, I0.baseNormal, normalWM2, pin.PosC, shadow, extraShadow, RS);
          Ct += getContribValue(C, glassFiltering);
        }
        Ct /= 2;
      #else
        C = getContribution(I0, I0.baseNormal, normalWM, pin.PosC, shadow, extraShadow, RS);
        Ct += getContribValue(C, glassFiltering);
      #endif
    #else
      I0.glassExtNormal = normalWBase;
    #endif

    float3 bak = C.innerEmissive;
    
    // Getting it all together
    float2 surfaceUV = getInnerUVDepth(pin.PosV_IsMirrored.xyz).xy;
    LightingParams L = getLightingParams(pin.PosC, toCamera, I0.glassExtNormal, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
    L.txEmissiveValue = 0;
    L.applyTxMaps(1);
    float3 lighting = L.calculate();

    lighting += getEmissiveMult() * GAMMA_KSEMISSIVE(extKsEmissiveMult * extGlassEmissiveMult * glassFiltering
      * getEmissiveColor(txInnerEmissive.SampleLevel(samLinearSimple, surfaceUV, 1 + 3 * saturate(totalDepth * 20)), false, I0.isMirrored).rgb);

    float3 lightingFX = LIGHTINGFX_GET;
    lighting.rgb = LIGHTINGFX_APPLY(lighting, lightingFX);

  // Gamma correction is not needed here: original local cubemap was shot in gamma space
    RS.extraLighting = lightingFX / max(ksDiffuse * txDiffuseValue.rgb, 0.0001);

    #ifdef MODE_GBUFFER
      clip(refractionMask - 0.1);
    #endif

    #ifdef ADVANCED_GLASS  
      float glassDistance = C.glassDistance;
      float mask = lerp(1, pow(sideFalloff(surfaceUV), 2), saturate(extSideFalloff));
      float glassAbsorption = exp(-extAbsorptionFactor * glassDistance / (1.01 - alpha));
      
      alpha = refractionMask;  
      glassAbsorption *= alpha;

      lighting.rgb *= 1 - glassAbsorption;
      lightingFX *= 1 - glassAbsorption * 0.5;
      lighting += Ct * glassAbsorption;
      lighting *= mask;
    #else
      float mask = 1;
    #endif

  #else

    LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
    L.txSpecularValue = GET_SPEC_COLOR;
    L.txEmissiveValue = 0;
    L.applyTxMaps(1);
    RAINFX_SHINY(L);
    float3 lighting = L.calculate();

    float2 surfaceUV = getInnerUVDepth(pin.PosV_IsMirrored.xyz).xy;
    lighting += extKsEmissiveMult 
      * getEmissiveColor(txInnerEmissive.SampleLevel(samLinearSimple, surfaceUV, 2), false, pin.PosV_IsMirrored.w).rgb;

    LIGHTINGFX(lighting);

  #endif

  alpha = max(alpha, 0.001);
  float3 withReflection;
  ReflParams R;

  {
    R = getReflParamsBase(EXTRA_SHADOW_REFLECTION * AO_REFLECTION, 1);
    R.metallicFix = 1;
    R.useBias = true;
    R.isCarPaint = false;
    #ifdef ADVANCED_GLASS  
      R.finalMult = mask;
      R.reflectionSampleParam = RS;
      if (pin.PosV_IsMirrored.w){
        I0.glassExtNormal = reflect(I0.glassExtNormal, extMirrorPlane.xyz);
      }

      lighting += calculateReflection(0..xxx, toCamera, pin.PosC, I0.glassExtNormal, R) / alpha;
      normalW = I0.glassExtNormal;
    #else
      lighting = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
    #endif
  }

  // float2 surfaceUV2 = getInnerUVDepth(pin.PosV_IsMirrored.xyz).xy;
  // lighting.rgb = extKsEmissiveMult 
  //     * getEmissiveColor(txInnerEmissive.SampleLevel(samLinearSimple, surfaceUV2, 2), false, pin.PosV_IsMirrored.w).rgb;

  RAINFX_WATER(lighting);
  #ifdef USE_LOCAL_TAA
    PS_OUT ret;

    float fogIntensity = 0;
    ret.result = withFog(applyAoMult(lighting.rgb, AO_LIGHTING), pin.Fog, alpha, fogIntensity);
    // ret.result.rgb = applyAoMult(lighting.rgb, AO_LIGHTING);
    // ret.result.rgb = ksFogColor.rgb;

    float2 velocity = GET_MOTION(pin);
    float speedK = saturate(length(velocity) * 100 - 0.1);
    float2 prevUV = pin.PosH.xy * extScreenSize.zw - velocity;
    float blendFactor = any(abs(prevUV - 0.5) > 0.5) ? 1 : lerp(0.1 + saturate(fogIntensity * 4) * 0.9, 1, speedK);
    float4 prevFrame = calculatePrevFrame(ret.result, prevUV, blendFactor);
    //  prevFrame = txPrevFrame.SampleLevel(samLinearClamp, prevUV - float2(0, 0.05), 0);
    //  blendFactor = 0;
    ret.result.rgb = lerp(prevFrame.rgb, ret.result.rgb, blendFactor);

    //  ret.result.w = 1;

    // ret.result = withFog(ret.result.rgb, pin.Fog, alpha);
    
    // ret.result.rgb.b = 0;
    // ret.result.rgb = prevFrame.rgb;
    // ret.result.rgb = txPrevFrame.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw - float2(0, 0.05), 0).rgb;
    
    
    // TODO:DEV
    // ret.result.rgb = C.debug;
    // ret.result.rg = abs(velocity) * 100;
    // ret.result.b = 0;
    
    return ret;
  #else
    // lighting.rgb = txPrevFrame.SampleLevel(samLinearClamp, pin.PosH.xy * extScreenSize.zw, 0).rgb;
    // lighting.g = 0;
    RETURN(lighting.rgb, alpha);
  #endif
}
