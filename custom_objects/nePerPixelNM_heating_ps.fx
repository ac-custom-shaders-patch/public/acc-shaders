#define CARPAINT_NM
#define GETNORMALW_SAMPLER samLinear
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"
#include "common/heating.hlsl"

RESULT_TYPE main(PS_IN_NmPosL pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float alpha, directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha, directedMult);

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.txEmissiveValue = 0;
  L.txSpecularValue = GET_SPEC_COLOR;
  APPLY_CAO;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, 1);
  R.useBias = true;
  float4 withReflection = calculateReflection(float4(lighting, alpha), toCamera, pin.PosC, normalW, R);
  HEATINGFX(withReflection.xyz);
  RETURN(withReflection, withReflection.a);
}
