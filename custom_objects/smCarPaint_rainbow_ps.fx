#ifdef NO_RAINBOW
  #define EXTRA_CARPAINT_FIELDS_3
#else
  // extRainbowAnisAspect: regular anisotropic is 0.8, so, √(1−0.8×0.9) = 0.53
  // extRainbowGlossInvSqr5: regular gloss is 0.7, so, (1−0.7)²×5 = 0.45
  // extRainbowX: extRainbowGlossInvSqr5 / extRainbowAnisAspect
  // extRainbowY: extRainbowGlossInvSqr5 * extRainbowAnisAspect
  #define EXTRA_CARPAINT_FIELDS_3\
    float extRainbowMult;\
    float extRainbowX;\
    float extRainbowY;\
    float extRainbowMask;\
    float extRainbowAnisMult;
  #define HAS_RAINBOW_MASK
#endif

#ifndef EXTRA_CARPAINT_FIELDS_4
  #define EXTRA_CARPAINT_FIELDS_4
#endif

#define HAS_EFFECTS_MASK
#define EXTRA_CARPAINT_FIELDS_2\
  float extChameleonEXP;\
  float3 extChameleon0Color;\
  float3 extChameleon1Color;\
  float extChameleonCenter;\
  float extChameleon0Alpha;\
  float extChameleon1Alpha;\
  float extChameleonMask;\
  EXTRA_CARPAINT_FIELDS_4;\
  EXTRA_CARPAINT_FIELDS_3
#define HAS_CHAMELEON_MASK

#define ALTER_DIFFUSE_COLOR alterDiffuseColor(txDiffuseValue.xyz,\
  chameleonMask, normalW, toCamera, flakesValue);

#ifndef NO_RAINBOW
  #define ALTER_LIGHTING calculateRainbowSpecular(result.diffuse, result.ambient, result.specular,\
    extraMask, toCamera, normalW, tangentW, bitangentW, specBasePower, P.shadow, P.specMult);
#endif

void alterDiffuseColor(inout float3 color, float mask, float3 normalW, float3 toCamera, float flakesValue);
void calculateRainbowSpecular(inout float3 resultDiffuse, inout float3 resultAmbient, inout float3 resultSpecular, 
  float3 mask, float3 toCamera, float3 normalW, float3 tangentW, float3 bitangentW, float specSunPower, float3 Pshadow, float3 PspecMult);

#include "smCarPaint_ps.fx"

float remapGradient(float x){
  return smoothstep(0, 1, x);
}

void alterDiffuseColor(inout float3 color, float mask, float3 normalW, float3 toCamera, float flakesValue){  
  float NdotL = saturate(dot(normalW, -toCamera));
  NdotL = pow(NdotL, extChameleonEXP);
  float Qa = abs(1 - NdotL / extChameleonCenter);
  float Qy = NdotL > extChameleonCenter ? 0 : Qa;
  color = lerp(color, extChameleon0Color, remapGradient(saturate(1 - Qa)) * extChameleon0Alpha * mask);
  color = lerp(color, extChameleon1Color, remapGradient(saturate(Qy)) * extChameleon1Alpha * mask);
}

#define sqr(x) ((x) * (x))

#ifndef NO_RAINBOW
float3 getSpecularRainbow(float v, float specSunPower){
  v = pow(saturate(v), 1.5);
  if (extRainbowMult < 0) v = 1 - v;
  float3 r = GAMMA_LINEAR_SIMPLE(rainbow(v));
  if (extRainbowAnisMult < 0) r = 1 - r;
  return r * specSunPower;
}

float wardAnisotropicNormalDistribution(float anisotropic, float NdotL, float NdotV, float NdotH, float HdotX, float HdotY, out float rK){
  float exponent = -(sqr(HdotX / extRainbowX) + sqr(HdotY / extRainbowY)) / sqr(NdotH);
  float distribution = 1.0 / (4.0 * 3.14159265 * extRainbowX * extRainbowY * sqrt(NdotL * NdotV));
  rK = exp(exponent);
  return distribution * rK;
}

void calculateRainbowSpecular(inout float3 resultDiffuse, inout float3 resultAmbient, inout float3 resultSpecular, 
    float3 mask, float3 toCamera, float3 normalW, float3 tangentW, float3 bitangentW, float specSunPower, float3 Pshadow, float3 PspecMult){
  float NdotL = max(0.01, dot(normalW, -ksLightDirection.xyz));
  float3 H = normalize(-toCamera - ksLightDirection.xyz); 
  float NdotH = max(0.01, dot(normalW, H));
  float NdotV = max(0.01, dot(normalW, -toCamera));
  mask *= Pshadow;
  // mask = 1;
  
  float K = 0;
  float3 newSpecular = wardAnisotropicNormalDistribution(0.8, NdotL, NdotV, NdotH, dot(H, tangentW), dot(H, bitangentW), K);
  float spK = saturate(luminance(extSpecularColor) / 4);
  spK *= spK * abs(extRainbowMult);

  newSpecular *= lerp(abs(extRainbowAnisMult), getSpecularRainbow(K * sqrt(NdotV), specSunPower), spK);
  newSpecular *= extSpecularColor * NdotL * PspecMult;
  newSpecular += resultSpecular;
  
  float3 drK = lerp(1, pow(saturate(1 - K), 20), spK * mask);
  resultSpecular = lerp(resultSpecular, newSpecular, mask);
  resultAmbient *= drK;
  resultDiffuse *= drK;
  // resultDiffuse = mask * 10;
}
#endif
