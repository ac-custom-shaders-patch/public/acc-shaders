// Redefining ksEmissive in a different cbuffer, so it would be 
// available in vertex shader without extra hacks
#define REDEFINE_KSEMISSIVE
#define INPUT_EMISSIVE3 0

#define GETNORMALW_SAMPLER samLinear
#define CARPAINT_NM
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

#define EXTRA_PARAMETERS_2\
  float extUseTxEmissive;\
  float extPad0;

#include "common/distantGlow.hlsl"

Texture2D txEmissive : register(TX_SLOT_MAT_2);

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  // RAINFX_WET(txDiffuseValue.xyz);

  float directedMult, alphaMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alphaMult, directedMult);

  float4 txEmissiveValue = txEmissive.Sample(samLinear, pin.Tex);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txEmissiveValue = 0;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  lighting += calculateEmissivePart(pin.PosC, normalW, 
    extUseTxEmissive ? txEmissiveValue.xyz * txDiffuseValue.xyz : txDiffuseValue.xyz);

  // lighting = txEmissiveValue.xyz;
  // lighting = txEmissive.Sample(samLinear, pin.Tex * 10).rgb * 0.1;
  // lighting = float3(frac(pin.Tex * 4) * 0.1, 0);

  RETURN_BASE(lighting, txDiffuseValue.a * alpha);
}
