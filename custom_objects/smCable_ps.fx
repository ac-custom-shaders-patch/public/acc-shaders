#define NO_CARPAINT
#define SUPPORTS_AO
#define INPUT_DIFFUSE_K 0.25
#define INPUT_AMBIENT_K 0.25
#define INPUT_SPECULAR_K 0.1
#define INPUT_SPECULAR_EXP 18
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, 1);
  float3 lighting = L.calculate();
  lighting = lerp(ksFogColor, lighting, saturate(0.5 + (AO_LIGHTING).x));
  LIGHTINGFX(lighting);
  RETURN_BASE(lighting, 1);
}
