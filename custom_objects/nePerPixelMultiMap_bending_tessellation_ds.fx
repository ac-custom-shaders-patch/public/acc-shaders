#define SUPPORTS_NORMALS_AO
#include "common/bending.hlsl"

#include "include_new/base/_include_vs.fx"
#include "common/tessellation.hlsl"

BENDING_FX_IMPL
#define GET(V) (bary.x * tri[0].V + bary.y * tri[1].V + bary.z * tri[2].V)
#define SET(V) dout.V = GET(V);

[domain("tri")]
GENERIC_RETURN_TYPE(DS_OUT) main(PatchTess patchTess, float3 bary : SV_DomainLocation, const OutputPatch<HS_OUT, 3> tri) {
  DS_OUT dout;

  SET(PosC);
  SET(Tex);

  #ifndef MODE_SHADOWS_ADVANCED
    SET(NormalW);
    #ifndef NO_NORMALMAPS
      SET(TangentW);
      SET(BitangentW);
    #endif
    OPT_STRUCT_FIELD_FOG(dout.Fog = 0);
    #ifdef ALLOW_PERVERTEX_AO
      SET(Ao);
    #endif
  #endif

  float4 posL = float4(GET(PosC), 1);
  float4 posW, posWnoflex, posV;
  dout.PosH = __toScreenSpace(posL, 0, posW, posWnoflex, posV SPS_VS_TOSS_PASS);
  dout.PosC = posW.xyz - ksCameraPosition.xyz;

  #ifndef MODE_SHADOWS_ADVANCED
    #define vout dout
    OPT_STRUCT_FIELD_FOG(dout.Fog = calculateFog(posV));
    shadows(posW, SHADOWS_COORDS);
    GENERIC_PIECE_MOTION(posL);  
  #endif

  VERTEX_POSTPROCESS(dout);
  SPS_RET(GENERIC_PIECE_RETURN(dout));
}