#define USE_PARALLAX
#define PARALLAX_UV pin.Tex
#define PARALLAX_UV_MULT uvScale

#include "fuPBR_ps.fx"

#ifdef USE_PARALLAX
  Texture2D txDisplacement : register(TX_SLOT_MAT_5);
  float parallaxHeight(float2 uv, float2 dx, float2 dy){
    return txDisplacement.SampleGrad(samLinear, uv, dx, dy).x;
  }
#endif
