#define SUPPORTS_NORMALS_AO
#include "include/samplers.hlsl"
#include "include_new/base/_include_vs.fx"
#include "common/tessellationParallaxShared.hlsl"
#include "common/tessellation.hlsl"

// Texture2D txDiffuse : register(t0);
Texture2D txNormal : register(t1);

#define SET(V)\
  dout.V = bary.x * tri[0].V + bary.y * tri[1].V + bary.z * tri[2].V;

[domain("tri")]
DS_OUT main(PatchTess patchTess, float3 bary : SV_DomainLocation, const OutputPatch<HS_OUT, 3> tri) {
  DS_OUT dout;

  // bary /= dot(bary, 1);

  SET(PosC);
  SET(Tex);
  SET(NormalW);
  OPT_STRUCT_FIELD_FOG(dout.Fog = 0);
  #ifndef NO_NORMALMAPS
    SET(TangentW);
    SET(BitangentW);
  #endif
  #ifdef ALLOW_PERVERTEX_AO
    SET(Ao);
  #endif

  dout.NormalW = normalize(dout.NormalW);

  float2 dispUV = dout.Tex;
  float h;

  if (extSurfaceMapping && false){
    // Experiment with deforming ground
    dispUV = (dout.PosC + (ksCameraPosition.xyz - extSceneOffset)).xz * 20 / 1024;
    h = txNormal.SampleLevel(samLinearWrap, dispUV, 0).r;
    dout.PosC.y += h;
  } else {
    const float MipInterval = 20;
    float mipLevel = clamp((length(dout.PosC) - MipInterval) / MipInterval, 0, 6);
    h = txNormal.SampleLevel(samLinearWrap, dispUV, mipLevel).a;
      
    if (!extDisplacementInvert) h = 1 - h;
    dout.PosC += (extHeightScale * h) * dout.NormalW;
  }

  // h = (h 
  //   + txNormal.SampleLevel(samLinearWrap, dout.Tex + 0.001, mipLevel).a
  //   + txNormal.SampleLevel(samLinearWrap, dout.Tex - 0.001, mipLevel).a
  //   + txNormal.SampleLevel(samLinearWrap, dout.Tex + float2(1, -1) * 0.001, mipLevel).a
  //   + txNormal.SampleLevel(samLinearWrap, dout.Tex - float2(1, -1) * 0.001, mipLevel).a
  //   ) / 5;
  // h = txNormal.Load(int3(frac(dout.Tex) * float2(4096, 512), 0)).a;
  // h = frac(dout.Tex.y);
  // dout.PosC.y = (291.49 - ksCameraPosition.y) + h;
  // dout.PosC.y = 91.49 + h;

  float4 posW = float4(dout.PosC, 1);
  float4 posV = mul(posW, ksView);
  dout.PosH = mul(posV, ksProjection);
  dout.PosC -= ksCameraPosition.xyz;

  // dout.Tex.x = h;

  #define vout dout
  OPT_STRUCT_FIELD_FOG(dout.Fog = calculateFog(posV));
  shadows(posW, SHADOWS_COORDS);
  GENERIC_PIECE_STATIC(posW);

  // dout.Fog = frac(h * 10);
  
  return dout;
}