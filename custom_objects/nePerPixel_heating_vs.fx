#define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"
// alias: nePerPixelReflection_heating_vs

PS_IN_PerPixelPosL main(VS_IN vin SPS_VS_ARG) {
  GENERIC_PIECE(PS_IN_PerPixelPosL);
  vout.PosL = vin.PosL.xyz;
  vout.NormalL = vin.NormalL.xyz;
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
