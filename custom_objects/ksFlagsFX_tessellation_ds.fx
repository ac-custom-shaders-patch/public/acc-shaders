#define SUPPORTS_COLORFUL_AO
#define NO_NORMALMAPS
#define INCLUDE_FLAGS_CB
#define TESSELLATION_FLAGS_MODE
#include "include_new/base/_include_vs.fx"
#include "common/flagsFX.hlsl"
#include "common/tessellationParallaxShared.hlsl"
#include "common/tessellation.hlsl"

#define GET(V) (bary.x * tri[0].V + bary.y * tri[1].V + bary.z * tri[2].V)
#define SET(V) dout.V = GET(V);

[domain("tri")]
GENERIC_RETURN_TYPE(PS_IN_PerPixel) main(PatchTess patchTess, float3 bary : SV_DomainLocation, const OutputPatch<HS_OUT, 3> tri) {
  PS_IN_PerPixel dout;

  SET(PosC);
  SET(Tex);
  #ifndef MODE_SHADOWS_ADVANCED
    SET(NormalW);
    OPT_STRUCT_FIELD_FOG(dout.Fog = 0);
    #ifdef ALLOW_PERVERTEX_AO
      SET(Ao);
    #endif
  #endif

  // float extWindSpeed = 0;
  // float2 extWindVel = 0;
  // float extWindWave = 0;

  #ifndef MODE_KUNOS
    dout.PosC.xyz -= extSceneOffset;
    #ifdef MODE_SHADOWS_ADVANCED
      float3 normalW = float3(1, 1, 1);
    #else
      float3 normalW = dout.NormalW;
    #endif
    if (HAS_FLAG(FLAG_MATERIAL_2)) {
      float ao = 1;
      #if defined(ALLOW_PERVERTEX_AO) && !defined(MODE_SHADOWS_ADVANCED)
        ao = FFX_restoreOriginalVAO(dout.Ao);
      #endif
      customWaveTent(dout.PosC.xyz, normalW, GET(FlagYRel), ao);
    } else {
      customWaveNm(dout.Tex, dout.PosC.xyz, normalW, tri[0].FlagSize, GET(FlagYRel), tri[0].WindOffset,
        extWindVel, extWindSpeed, extWindWave);
    }
    #ifndef MODE_SHADOWS_ADVANCED
      dout.NormalW = normalW;
    #endif
    dout.PosC.xyz += extSceneOffset;
  #endif

  float4 posW = float4(dout.PosC, 1);
  float4 posV = mul(posW, ksView);
  dout.PosH = mul(posV, ksProjection);
  dout.PosC -= ksCameraPosition.xyz;

  #define vout dout
  OPT_STRUCT_FIELD_FOG(dout.Fog = calculateFog(posV));
  float4 posWShadow = posW + float4(ksLightDirection.xyz * -0.2, 0);
  shadows(posWShadow, SHADOWS_COORDS);
  GENERIC_PIECE_STATIC(posW);  
  SPS_RET(GENERIC_PIECE_RETURN(dout));
}