#define SUPPORTS_COLORFUL_AO
#define SUPPORTS_DISTANT_FIX
#define POSL_IS_POSW

#define CUSTOM_STRUCT_FIELDS\
  float4 LineColor : EXTRA0;\
  float Edge : EXTRA1;\
  float AgeFactor : EXTRA3;\
  nointerpolation uint TexID : EXTRA2;\

#include "include_new/base/_include_vs.fx"
#include "include/samplers.hlsl"

Texture2D<float> _txAreaDepth : register(t6);
Texture2D<float> _txAreaAO : register(t7);
cbuffer _cbVPSBuffer : register(b11) {
  float4x4 gAreaHeightmapTransform;
}

static float stth(float v) { return abs(v - floor(v) - 0.5); }

PS_IN_PerPixel main(VS_IN vin SPS_VS_ARG) {
  float thicknessFactor = BYTE1(asuint(vin.TangentPacked.z)) * (0.02 / 255.);
  float ageFactor = BYTE2(asuint(vin.TangentPacked.z)) / 255.;
  uint texID = (BYTE3(asuint(vin.TangentPacked.z)) >> 1) & 7;
  bool isEdge = (BYTE3(asuint(vin.TangentPacked.z)) & 1) != 0;
  float4 origPosL0 = vin.PosL;
  float aoValue = 1;
  float4 color = unpackColor(asuint(vin.TangentPacked.y));

  [branch]
  if (gAreaHeightmapTransform[0][0]) {
    // color = float4(1, 0, 0, 1);
    float4 areaUV = mul(vin.PosL, gAreaHeightmapTransform);
    if (all(abs(areaUV.xyz - 0.5) < 0.5)) {
      // color = float4(0, 1, 0, 1);
      float depth = _txAreaDepth.SampleLevel(samLinearClamp, areaUV.xy, 0);
      float distanceToGround = (depth - areaUV.z) * 300;
      aoValue = _txAreaAO.SampleLevel(samLinearClamp, areaUV.xy, 0);
      if (distanceToGround > -0.1 && distanceToGround < 0) {
        // color = float4(0, 0, 1, 1);
        vin.PosL.y -= distanceToGround;
      }
    }
  }

  vin.PosL = mul(vin.PosL, ksWorld);
  float4 origPosL = vin.PosL;

  {    
    vin.PosL.y += isEdge ? -thicknessFactor : thicknessFactor;

    float3 toCamera = SPS_CAMERA_POS - vin.PosL.xyz;  
    vin.PosL.xyz += normalize(toCamera) * 0.05;
  }

  // GENERIC_PIECE(PS_IN_PerPixel);
  GENERIC_PIECE_NOSHADOWS(PS_IN_PerPixel);
  shadows(origPosL, SHADOWS_COORDS);

  // {
  //   float4 altPosL = origPosL; // mul(origPosL0, ksWorld);
  //   float3 toCamera = SPS_CAMERA_POS - altPosL.xyz;  
  //   // altPosL.xyz += toCamera * min(0.1, 0.1 / (0.5 + abs(toCamera.y))) * (1 + entryIndex / 64.);
  //   // altPosL.xyz += toCamera * 0.1 * (1 + entryIndex / 64.);
  //   float4 newPosH = mul(mul(altPosL, ksView), ksProjection);
  //   float zMult = 1 - 0.1 * (1 + entryIndex / 64.);
  //   // newPosH.z *= zMult;
  //   // newPosH.w /= zMult;
  //   // vout.PosH.xy = vout.PosH.xy * newPosH.w / vout.PosH.w;
  //   // vout.PosH.zw = newPosH.zw;
  // }

  RAINFX_VERTEX(vout);
  vout.LineColor = color;
  vout.Edge = isEdge ? 1 : 0;
  vout.AgeFactor = ageFactor;
  vout.TexID = texID;
  OPT_STRUCT_FIELD_POSC(vout.PosC = origPosL.xyz - ksCameraPosition.xyz);
  vout.Ao = aoValue;
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout); 
} 
 