#define CB_MATERIAL_EXTRA_4\
  float useAlphaFromDiffuse;\
  float useAlphaFromNormal;\
  float unevenWaterFix;\
  float extDepthAware;\
  float extCausticsBrightness;

#define SSLR_FORCE !(useAlphaFromDiffuse || useAlphaFromNormal)
#define LIGHTMAP_CAUSTICS
#define CUSTOM_MATERIAL_PROPERTIES
#define GETNORMALW_SAMPLER samAnisotropic
// #define FORCE_BLURREST_REFLECTIONS
// #define SUPPORTS_AO
#define SHADOWS_FILTER_SIZE 2
#define GBUFF_NORMALMAPS_DISTANCE 400
#define GBUFF_NORMALMAPS_NEUTRAL float3(0, 1, 0)
#define IS_ADDITIVE_VAR 0
#define INPUT_NM_OBJECT_SPACE 0
#define NO_CARPAINT
#define RAINFX_WATER_SURFACE_MODE
#define RAINFX_STATIC_OBJECT

const static float fresnelEXP = 1;
const static float fresnelMaxLevel = 0;
const static float fresnelC = 0;
const static float detailUVMultiplier = 0;
const static float useDetail = 0;
const static float extColoredReflectionNorm = 0;
const static float extColoredBaseReflection = 0;
const static float sunSpecularExp = 0;
const static float seasonWinter = 0;
const static float seasonAutumn = 0;

#define INPUT_DIFFUSE_K 0.05
#define INPUT_AMBIENT_K 0.14
#define INPUT_EMISSIVE3 0
#define INPUT_SPECULAR_K 0.4
#define INPUT_SPECULAR_EXP 360

#define RAINFX_STATIC_OBJECT
#define RAINFX_REGULAR_REFLECTIONS
#define USE_PS_FOG
#define PASS_GBUFF_PIN PS_IN_Nm

#include "include_new/base/_include_ps.fx"

bool isPool(){
  return ksAlphaRef == 1;
}

bool isFilmed(){
  return ksAlphaRef == 3;
}

bool isLake(){
  return ksAlphaRef == -1;
}

bool isNatural(){
  return ksAlphaRef <= 0;
}

bool isEnclosed(){
  return int(ksAlphaRef) & 1;
}

bool withWhiteBits(){
  return ksAlphaRef == -2;
}

#define MAX_WIND_SPEED 10

#ifdef MODE_KUNOS
  #define extWindSpeed 4
  #define extWindWave (ksGameTime / 1000)
  #define extWindWater (float2)(ksGameTime * (8. / 1000.))
#endif

#if defined(ADVANCED_WATER_SHADING) //|| defined(MODE_GBUFFER)
  #define DEPTH_AWARE_WATER
#endif

float4 textureSampleVariationTimeShifting(Texture2D tx, SamplerState sam, float2 uv, float bias, 
    float noiseMult){
  float k0 = txNoise.Sample(samLinear, noiseMult * uv + extWindWave * 0.1).x;
  float k1 = txNoise.Sample(samLinear, noiseMult * uv + 0.3 + extWindWave * -0.05).x;
  float k2 = txNoise.Sample(samLinear, noiseMult * uv + 0.7 + extWindWave * 0.05).x;
  float k = lerp(k1, k2, k0);

  float2 duvdx = ddx(uv) * bias;
  float2 duvdy = ddy(uv) * bias;
    
  float l = k * 1.0;
  float i = floor(l);
  float f = frac(l);

  float2 offa = sin(float2(3.0, 7.0) * (i + 0.0));
  float2 offb = sin(float2(3.0, 7.0) * (i + 1.0));
  float4 cola = tx.SampleGrad(sam, uv + offa, duvdx, duvdy);
  float4 colb = tx.SampleGrad(sam, uv + offb, duvdx, duvdy);

  // return lerp(1, 0, smoothstep(0.4, 0.6, f - 0.1 * dot(cola - colb, 1)));
  return lerp(cola, colb, smoothstep(0.4, 0.6, f - 0.1 * dot(cola - colb, 1)));
}

float4 textureSampleVariation2(Texture2D tx, float2 uv, float2 uvNoise, float bias){
  #ifdef MODE_KUNOS
    float k = 0;
  #else
    float k = txNoise.Sample(samAnisotropic, uvNoise).x;
  #endif

  float2 duvdx = ddx(uv) * bias;
  float2 duvdy = ddy(uv) * bias;
    
  float l = k * 8.0;
  float i = floor(l);
  float f = frac(l);

  float2 offa = sin(float2(3.0, 7.0) * (i + 0.0));
  float2 offb = sin(float2(3.0, 7.0) * (i + 1.0));
  float4 cola = tx.SampleGrad(samAnisotropic, uv + offa, duvdx, duvdy);
  float4 colb = tx.SampleGrad(samAnisotropic, uv + offb, duvdx, duvdy);

    // cola = frac((uv + offa).xyxy);
    // colb = frac((uv + offb).xyxy);

  // return lerp(1, 0, smoothstep(0, 1, f - 0.1 * dot(cola - colb, 1)));
  // return lerp(1, 0, smoothstep(0.2, 0.8, f - 0.1 * dot(cola - colb, 1)));
  return lerp(cola, colb, smoothstep(0, 1, f - 0.1 * dot(cola - colb, 1)));
}

// float4 textureSampleVariation2(Texture2D tx, SamplerState sam, float2 uv, float bias = 1){
//   return textureSampleVariation2(tx, sam, uv, bias, 0.05);
// }

void sampleTextures(float2 uv, float scale, out float2 uv1, out float2 uv2, out float4 tx1, out float4 tx2){
  float time = ksGameTime * 0.00001;
  uv1 = uv * scale + float2(time, 0);
  uv2 = uv * scale + float2(0, time);

  float2 uvN1 = (uv * scale + float2(time * 2.17, 0)) * 0.5;
  float2 uvN2 = (uv * scale + float2(0, time * 2.17)) * 0.5;

  #if defined(ADVANCED_WATER)
    tx1 = textureSampleVariation2(txNormal, uv1, uvN2, 1.5);
    tx2 = textureSampleVariation2(txDetailNM, uv2 * 3.7, uvN1, 1.5);
  #else
    tx1 = txNormal.SampleBias(samLinearSimple, uv1, 1);
    tx2 = txDetailNM.SampleBias(samLinearSimple, uv2 * 3.7, 1);
  #endif
}

float3 getNormalF(float2 uv, out float2 uvWave, out float scale, out float2 uv1, out float2 uv2, out float4 _tx1){
  float openK = isEnclosed() ? 0.05 : 1;
  float sizeK = openK + isLake() * 0.4;
  float waveScale = saturate(0.01 + extWindSpeed * sizeK / MAX_WIND_SPEED);

  uv -= extWindWater * openK;
  uvWave = uv;

  // float scale = lerp(0.02, 0.005, waveScale) * (2 - openK);

  #ifdef ADVANCED_WATER
    float4 txA1, txA2;
    float4 txB1, txB2;
    sampleTextures(uv, 0.02 * (2 - sizeK), uv1, uv2, txA1, txA2);
    sampleTextures(uv, 0.005 * (2 - sizeK), uv1, uv2, txB1, txB2);
    scale = 0.005 * (2 - openK);

    float4 tx1 = lerp(txA1, txB1, waveScale);
    float4 tx2 = lerp(txA2, txB2, waveScale);
  #else
    float4 tx1, tx2;
    sampleTextures(uv, 0.01 * (2 - openK), uv1, uv2, tx1, tx2);
    scale = 0.01 * (2 - openK);
  #endif

  float4 v1 = tx1 * 2 - 1;
  float4 v2 = tx2 * 2 - 1;
  v1.x *= -1;
  v2.x *= -1;

  float4 vA = (v1 + v2) * (2 + waveScale * 3) / 4 + v1 * v2;
  vA = lerp(vA, float4(0, 0, 1, 0), 0.9 - waveScale * 0.5);
  vA.xy *= -1;

  _tx1 = tx1;
  return normalize(vA.xzy);
}

#include "include_new/ext_functions/depth_map.fx"
#include "refraction.hlsl"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS
  // pin.NormalW = float3(0, 1, 0);

  float3 posW = ksInPositionWorld;
  float openK = isEnclosed() ? 0.03 : 1;
  float waveScale = saturate(0.01 + extWindSpeed * openK / MAX_WIND_SPEED);
  
  #ifdef ADVANCED_WATER
    float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
    APPLY_EXTRA_SHADOW
  #elif defined(MODE_LIGHTMAP)
    float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
    float2 extraShadow = 1;
  #else
    float shadow = 1;
    float2 extraShadow = 1;
  #endif

  float uvScale;
  float2 uvWave, uv1, uv2;
  float4 tx1;
  normalW = getNormalF(posW.xz, uvWave, uvScale, uv1, uv2, tx1);
  // normalW = normalize(pin.NormalW);

  if (dot(normalW, toCamera) > -0.2){
    normalW -= toCamera * saturate(remap(dot(normalW, toCamera), -0.2, -0.1, 0, 1)) * unevenWaterFix;
    normalW = normalize(normalW);
  }

  // RAINFX_INIT;

  float dist = length(pin.PosC);
  float distK = pow(saturate(dist / 1500 - 0.1), 0.3);
  float directK = 0.1 * saturate(-dot(normalW, toCamera));

  #ifdef ALLOW_RAINFX
    float _spot, _wetSpot;
    float4 ripple = _rainFx_calculateRipple(lerp((pin.PosC + ksCameraPosition.xyz).xz, uvWave, 0.5), _spot, _wetSpot, float3(0, 1, 0), float2x2(1, 0, 0, 1));
    normalW = normalize(normalW + (ripple.xyz - float3(0, 1, 0)) * saturate(ripple.a) * lerpInvSat(dist, 50, 0) * 2);
  #endif

  #ifdef MODE_LIGHTMAP
    toCamera = -normalW;
    directK = 0.1;
  #endif

  float4 txDiffuseValue = 1;
  RAINFX_WET(txDiffuseValue.xyz);
  txDiffuseValue = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 100);

  #if defined(ALLOW_RAINFX) && !defined(MODE_SIMPLIFIED_FX)
    float2 rainUV = ksInPositionWorld.xz * RAIN_UV_MAP_XZ;
    float4 rainMapValue = textureSampleVariation(txRainDrops, samLinear, rainUV * 0.16, 1.5, 0.05);
    float3 nmValue = _rainFx_remapNormalRipple(rainMapValue.xy, float3(0, 0, 1), float3(1, 0, 0), float3(0, 1, 0));
    rainMapValue.z *= _rainFx_rainShadow(pin.PosC + ksCameraPosition.xyz, pin.NormalW);
    normalW = lerp(normalW, nmValue, rainMapValue.z);
    txDiffuseValue.xyz *= lerp(1, 0.9, rainMapValue.z);
  #else
    float4 rainMapValue = 0;
  #endif

  float3 normalWB = normalW;

  float alphaValue = 1;
  if (useAlphaFromDiffuse) alphaValue *= txDiffuse.Sample(samLinearSimple, pin.Tex).a;
  if (useAlphaFromNormal) alphaValue *= txNormal.Sample(samLinearSimple, pin.Tex).a;

  // TODO:
  // alphaValue = 1;

  float3 color = txDiffuseValue.rgb * (isNatural() ? (float3)0.3 : float3(0, 0.9, 1));
  color += directK * (isPool() ? 1 : float3(0.3, 0.4, 0.3) * (1 - waveScale));

  // #ifdef RAINFX_USE_PUDDLES_MASK
  //   normalW = lerp(normalW, RP.normal, RP.alpha);
  // #endif

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y);

  #if defined(ALLOW_RAINFX) || defined(MODE_GBUFFER)
    float rain = saturate(extSceneRain - rainMapValue.z * 0.05);
  #else
    float rain = 1;
  #endif

  L.specularExp = lerp(L.specularExp, 30, rain) * GAMMA_OR(2, 1);
  L.specularValue = lerp(L.specularValue, 0.05, rain) * GAMMA_OR(4, 1);
 
  float pixelDepth = 0;
  float depth = 0;
  #ifdef DEPTH_AWARE_WATER
    if (extDepthAware) {
      depth = txDepth.SampleLevel(samLinearClamp, pin.PosH.xy * extScreenSize.zw, 0);
      if (depth) {
        pixelDepth = linearizeAccurate(pin.PosH.z);
        depth = linearizeAccurate(depth) - pixelDepth;
        // L.specularExp = lerp(4, L.specularExp, saturate(depth - 0.1));
        // L.specularValue = lerp(0.5, L.specularValue, saturate(depth - 0.1));
      }
    }
  #endif

  float3 lighting = L.calculate();
  #ifdef ADVANCED_WATER
    float sunSpecularBase = saturate(dot(normalize(-toCamera - ksLightDirection.xyz), normalW));
    float3 sunSpecularPart = pow(sunSpecularBase, lerp(2500, 500, rain)) * shadow * 10 * extSpecularColor;
    lighting += sunSpecularPart * lerp(1, 0.01, rain);
  #endif

  ReflParams R;
  R.fresnelMaxLevel = 0.5 - 0.3 * isPool() + 0.2 * isFilmed();
  R.fresnelC = 0.1;
  R.fresnelEXP = isFilmed() ? 3 : 6;
  R.ksSpecularEXP = lerp(300, 3, extSceneRain);
  R.finalMult = 1;
  R.metallicFix = 1;
  R.useBias = true;
  R.isCarPaint = false;
  R.useSkyColor = false;
  R.coloredReflections = 0;
  R.coloredReflectionsColor = 0;
  R.useStereoApproach = false;
  R.stereoExtraMult = 0;
  R.reflectionSampleParam = 0;
  R.forceReflectedDir = false;
  R.forcedReflectedDir = 0;
  R.globalOcclusionMult = 1;
  R.useGlobalOcclusionMult = false;
  R.useMatteFix = true;

  #ifdef KEEP_REFLECTION_PARAMS
    R = (ReflParams)0;
  #endif

  normalW = normalize(normalW + float3(0, saturate(dist / 500) * 10, 0));

  float alphaBase = 1 - directK;
  float alphaResult = saturate(alphaBase * alphaBase * alphaValue);

  #ifdef DEPTH_AWARE_WATER
    [branch]
    if (depth){
      float depthY = depth * max(0.1, abs(toCamera.y));
      //alphaResult *= smoothstep(0, 1, saturate(depthY * 10));
      //alphaResult = smoothstep(0, 1, saturate(depthY * 10));

      float2 uvBase = pin.PosH.xy * extScreenSize.zw;
      float uvOffset = 1 - pow(lerpInvSat(max(max(uvBase.x, uvBase.y), max(1 - uvBase.x, 1 - uvBase.y)), 0.9, 1), 2);
      float2 offset = -float2(normalW.x, normalW.z) * saturate(depth / 4);
      // offset = float2(0.01, 0); // TODO
      float2 uvRefr;
      float4 prev;
      float prevDepth;
      for (int i = 0; i < 3; ++i) {
        uvRefr = uvBase + uvOffset * offset * saturate(depth * 10);
        prevDepth = linearizeAccurate(txDepth.SampleLevel(samLinearClamp, uvRefr, 0));
        if (prevDepth > pixelDepth - 0.2) {
          break;
        } else {
          offset *= 0.2;
        }
      }
      prev = calculateRefractionColor(uvRefr, -2);
      if (prev.a > 0){
        float2 uvOffset = toCamera.xz * depth * uvScale + float2(normalW.x, normalW.z) * saturate(depth / 4) * 0.04;
        uv1 += uvOffset;
        uv2 += uvOffset;
        uv1 *= 20;
        uv2 *= 20;

        float cau1 = txNormal.SampleBias(samLinearSimple, uv1, 0).a;
        float cau2 = txNormal.SampleBias(samLinearSimple, uv2 + float2(0.371, 0.179), 0).a;
        prev *= 1 + extCausticsBrightness * shadow //* ksLightColor 
          * GAMMA_LINEAR_SIMPLE(saturate(depthY - 0.2) * saturate(1 - extSceneRain)) * GAMMA_OR(8, 2) * smin(cau1, cau2);
        // lighting = lerp(lighting, prev.rgb, 1 / (1 + depth));
        alphaResult *= saturate(depthY * 2);
        alphaResult = GAMMA_ALPHA(alphaResult);
        lighting = lerp(prev.rgb, lighting, alphaResult);
        // R.finalMult *= alphaResult / max(0.05, saturate(depthY * 10));
        // alphaResult = max(0.05, saturate(depthY * 10));
        R.finalMult *= alphaResult;
        alphaResult = 1;
      }
    } else {
      alphaResult = GAMMA_ALPHA(alphaResult);
    }

    #ifdef MODE_GBUFFER
      // alphaResult = 0;
    #endif
  #else
    alphaResult = GAMMA_ALPHA(alphaResult);
  #endif

  float3 withReflection = calculateReflection(float4(lighting, 1), toCamera, pin.PosC, normalW, R).rgb;

  #ifdef ADVANCED_WATER_SHADING
    // float waterThing = pow(saturate(saturate(dot(normalW, ksLightDirection.xyz) * 2 + 0.8) 
    //     * saturate(dot(toCamera, -ksLightDirection.xyz))
    //     - 0.2), 4) * 3 * openK * shadow;
    // withReflection += 
    //     (dot(ksLightColor.rgb, 0.33) 
    //       * txDiffuseValue.rgb 
    //       * float3(0.1 + waterThing * 0.5, 0.4 + waterThing * 0.2, 0.4))
    //     * waterThing;

    withReflection += 0.25
      * shadow
      * openK
      * dot(ksLightColor.rgb, 0.03) 
      * float3(0, 1, 0.5) 
      // * (1 - dot(normalW, ksLightDirection.xyz))
      * pow(saturate(dot(toCamera, -ksLightDirection.xyz)), 4) 
      * pow(1 - R.resultPower, 4);

    [branch]
    if (withWhiteBits()){
      float foamP = pow(saturate(1 - dot(abs(txNormal.SampleLevel(samLinearSimple, uvWave, 6) - 0.5).xy, 1)), 
        lerp(400, 40, waveScale));
      float yDif = fwidth(normalWB.y);
      float foamK = saturate(1 - yDif * lerp(1000, 200, waveScale)) * pow(saturate(normalWB.y), lerp(40000, 2000, waveScale));
      withReflection += 1
        // * foamP
        * lerp(0.2, 1, txNoise.SampleLevel(samLinearSimple, posW.xz * 2, 0).x)
        * txNoise.SampleLevel(samLinearSimple, posW.xz * 0.02, 0).x
        * lerpInvSat(normalWB.y, 0.998, 1)
        * dot(ksAmbientColor_sky0.rgb, 0.4) 
        * saturate(1 - dist / 100) 
        * pow(waveScale, 4)
        // * pow(saturate(1 - yDif * lerp(1000, 200, waveScale)), 4)
        ;
    }

    // withReflection = saturate(normalW * 100);
  #endif


  R.resultColor *= alphaResult;
  R.resultPower *= alphaResult;
  // withReflection = tx1.x;

  // depth = txDepth.SampleLevel(samLinearClamp, pin.PosH.xy * extScreenSize.zw, 0);
  // withReflection = frac(depth * 100);
  // withReflection.rg = frac(uvWave / 10);
  // withReflection.rg = frac((posW.xz - extWindWater) / 10);
  // alphaResult = 1;  

  // withReflection.r = RP.alpha;

  // RAINFX_WATER(withReflection);
  RETURN(withReflection, alphaResult);
}
