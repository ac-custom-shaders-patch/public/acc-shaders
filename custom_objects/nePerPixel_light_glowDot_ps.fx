// Redefining ksEmissive in a different cbuffer, so it would be 
// available in vertex shader without extra hacks
#define REDEFINE_KSEMISSIVE
#define INPUT_EMISSIVE3 0

#define NO_CARPAINT
#define SUPPORTS_AO
#define NO_SSAO
#define NO_SSGI
#define SKIP_PS_FOG
#include "include_new/base/_include_ps.fx"
#include "common/distantGlow.hlsl"

RESULT_TYPE main(PS_IN_PerPixelExtra1 pin) {
  READ_VECTORS

  {
    // float alpha = 1;
    // float3 lighting = float3(1, 0, 0);
    // RETURN_BASE(lighting, alpha);
  }

  #if defined(ORIGINAL_MODE)
    clip(-1);
  #endif

  float4 txDiffuseValue = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 2);
  txDiffuseValue = lerp(1, txDiffuseValue, extDistantUseTexture);

  float3 glowBase = txDiffuseValue.rgb * ksEmissive;
  float3 glowNormalized = normalizeGlow(glowBase) * extDotBrightnessBase;
  float3 lighting = glowNormalized;
  // lighting = GAMMA_KSEMISSIVE(lighting) * getEmissiveMult();

  float glowK = GAMMA_LINEAR_SIMPLE(saturate(1 - length(pin.NormalW.xy)));
  // if (!GAMMA_FIX_ACTIVE) 
  glowK = smoothstep(0, 1, glowK);
  
  lighting *= 1 + extDotBrightnessCenter * pow(glowK, GAMMA_GENERIC_EXP(extDotCenterEXP));
  lighting *= pin.Extra;
  lighting = GAMMA_KSEMISSIVE(lighting) * getEmissiveMult();
  lighting *= glowK;
  // lighting *= 1e6;
  // clip(dot(lighting, 1) - GAMMA_OR(0.00001, 0.01) * extMainBrightnessMult);

  float alpha = 0.005;
  lighting /= alpha;

  // alpha = 1;
  // lighting = float3(1, 0, 0);

  RETURN_BASE(lighting, alpha);
}
