#define NO_CARPAINT
#define SUPPORTS_AO
#define SUPPORTS_DITHER_FADING
// #define STRUCT_POSH_MODIFIER centroid noperspective

#define CUSTOM_STRUCT_FIELDS\
  float4 LineColor : EXTRA0;\
  float Edge : EXTRA1;\
  float AgeFactor : EXTRA3;\
  nointerpolation uint TexID : EXTRA2;\

#include "include_new/base/_include_ps.fx"
#include "include/bicubic.hlsl"

Texture2D txMark1 : register(TX_SLOT_MAT_1);
Texture2D txMark2 : register(TX_SLOT_MAT_2);
Texture2D txMark3 : register(TX_SLOT_MAT_3);
Texture2D txMark4 : register(TX_SLOT_MAT_4);

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS

  #ifdef NO_SELFSHADOW
    float shadow = getCloudShadow(pin.PosC);
    float2 extraShadow = 1;
  #else
    float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
    APPLY_EXTRA_SHADOW
  #endif

  float4 txDiffuseValue = 1; 

  // txDiffuseValue.rgb = float3(pin.Tex, 0);
  // txDiffuseValue.a = min(min(pin.Tex.x, pin.Tex.y), min(1 - pin.Tex.x, 1 - pin.Tex.y));

  float2 noiseUV = ksInPositionWorld.xz;
  float4 cracks = textureSampleVariation(txDiffuse, samLinearSimple, noiseUV, 1, 0.02);
  // float4 cracks = txDiffuse.Sample(samLinear, noiseUV);

  float noiseLevel = txNoise.CalculateLevelOfDetail(samLinearSimple, noiseUV * 16) - 4;
  float4 noise = (txNoise.SampleLevel(samLinearSimple, noiseUV * 0.976 + dot(pin.LineColor, 0.75955), noiseLevel - 0.03) 
    + txNoise.SampleLevel(samLinearSimple, rotate2d(noiseUV, 1) * 0.977 + dot(pin.LineColor, 0.75955), noiseLevel - 0.03)) / 2 * 0.4
    + txNoise.SampleLevel(samLinearSimple, rotate2d(noiseUV, 1) * 2.161, noiseLevel + 1.11) * 0.2
    + txNoise.SampleLevel(samLinearSimple, rotate2d(noiseUV, 2) * 5.747, noiseLevel + 2.52) * 0.2
    + txNoise.SampleLevel(samLinearSimple, noiseUV * 11.415, noiseLevel + 3.51) * 0.2;

  noiseUV /= 37.37;
  float noiseLargeLevel = txNoise.CalculateLevelOfDetail(samLinearSimple, noiseUV * 2) - 1;
  float4 noiseLarge = (sampleBicubic(txNoise, noiseUV * 0.976 + dot(pin.LineColor, 0.75955), noiseLargeLevel) 
    + sampleBicubic(txNoise, rotate2d(noiseUV, 1) * 0.977 + dot(pin.LineColor, 0.75955), noiseLargeLevel)) / 2 * 0.5
    + txNoise.SampleLevel(samLinearSimple, rotate2d(noiseUV, 1) * 2.161, noiseLargeLevel + 1.11) * 0.3
    + txNoise.SampleLevel(samLinearSimple, rotate2d(noiseUV, 2) * 5.747, noiseLargeLevel + 2.52) * 0.2;
  // noise = saturate(1 - noise);
  // txDiffuseValue.b = noise.r;

  float dim = lerp(0.6, 0.2, pin.AgeFactor);
  txDiffuseValue.rgb = lerp(0.2 + 0.8 * dot(pin.LineColor.rgb, 1 / 3.), pin.LineColor.rgb, 1 - pin.AgeFactor * 0.5) * lerp(dim, 1, noise.r) * lerp(dim, 1, noiseLarge.r);

  if (pin.TexID) {
    bool fontMode = pin.TexID > 4;
    if (fontMode) {
      pin.TexID -= 4;
    }
    float4 txValue = 1;
    switch (pin.TexID) {
      case 1: txValue = txMark1.Sample(samLinearClamp, pin.Tex); break;
      case 2: txValue = txMark2.Sample(samLinearClamp, pin.Tex); break;
      case 3: txValue = txMark3.Sample(samLinearClamp, pin.Tex); break;
      case 4: txValue = txMark4.Sample(samLinearClamp, pin.Tex); break;
    }    
    // txDiffuseValue = 0;
    if (fontMode) {
      txDiffuseValue.a *= txValue.r;
    } else {
      txDiffuseValue *= txValue;
    }
  }

  // txDiffuseValue.rg = frac(noiseUV) > 0.1;
  float cracksModifier = 2 - pin.AgeFactor * 0.4;
  float edgeFade = (1 - max(pin.Edge, 1 - txDiffuseValue.a) - pow(saturate(noise.g), 3.5 - pin.AgeFactor - noiseLarge.b) * 2 
    - saturate(noiseLarge.g * 4 - cracksModifier) * lerpInvSat(cracks.r, 1, 0.5)
    - saturate((1 - noiseLarge.a) * 4 - cracksModifier) * lerpInvSat(cracks.b, 1, 0.5)
    - saturate(noiseLarge.a * 4 - cracksModifier) * lerpInvSat(cracks.g, 1, 0.5));
  txDiffuseValue.a *= pin.LineColor.w * saturate(edgeFade * 4);
  txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, luminance(txDiffuseValue.rgb), max(pin.Edge, noise.g) * 0.25) * (0.8 + 0.2 * cracks.a);
  // txDiffuseValue.rgb *= 0.4 + 0.6 * saturate(edgeFade * 2);
  if (!HAS_FLAG(FLAG_MATERIAL_0)){
    txDiffuseValue.a = saturate((txDiffuseValue.a - 0.5) / max(2 * fwidth(txDiffuseValue.a), 0.0001) + 0.5);
  }
  RAINFX_WET(txDiffuseValue.xyz);

  normalW.xyz += (noise.bag - 0.5) * (0.07 / edgeFade);
  normalW.xyz = normalize(normalW.xyz);

  // txDiffuseValue.r = pin.Edge;
  // txDiffuseValue.rgb = noiseLarge.rgb;
  // txDiffuseValue.rgb = cracks.rgb;
  // txDiffuseValue.rgb = float3(pin.Ao, 1 - pin.Ao, 0);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  #ifndef NO_SELFSHADOW
    APPLY_CAO;
  #endif
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  RAINFX_REFLECTIVE_WATER_ROUGH(lighting);
  RETURN(lighting, txDiffuseValue.a);
}
