#define SUPPORTS_COLORFUL_AO
#define AO_MIX_INPUT 0
#define USE_OPTIMIZED_SHADOWS
#define NO_SHADOWS_CASCADES_TRANSITION
#define RAINFX_STATIC_OBJECT

#define CUSTOM_STRUCT_FIELDS\
  float CustomWind : WIND;

#include "include_new/base/_include_vs.fx"
#include "include_new/ext_functions/wind.fx"

PS_IN_PerPixel main(VS_IN vin SPS_VS_ARG) {
  float windStrength = vsLoadHeating(vin.TangentPacked);

  float4 posL = vin.PosL;
  vin.PosL.xz += windOffset(vin.PosL.xyz, windStrength, 1) * 2;
  GENERIC_PIECE(PS_IN_PerPixel);
  posL.xz += windOffsetPrev(posL.xyz, windStrength, 1) * 2;
  GENERIC_PIECE_MOTION(posL);

  vout.CustomWind = windStrength;
  SPS_RET(vout);
}
