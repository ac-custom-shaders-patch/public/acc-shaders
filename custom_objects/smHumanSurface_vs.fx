#define CUSTOM_STRUCT_FIELDS\
  float3 PosL : EXTRA0;\
  float3 PosL1 : EXTRA1;

#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"
#include "common/humanSurface.hlsl"
// alias: smHumanSurface_toon_vs
 
PS_IN_Nm main(VS_IN vin SPS_VS_ARG) {
  PS_IN_Nm vout;
  // vout.Mouth = 0;

  #ifndef NO_REFLECTIONS
    float offsetY = 0;
    float offsetX = 0;
    float2 offsetYZ = 0;

    // float extMouthMovement = sin(ksGameTime * 0.001) * 0.002;
    // float extMouthOpened = sin(ksGameTime * 0.01) * 0.005 + 0.005;

    float mouthK = saturate(remap(length(vin.PosL.yz - extMouth.yz), 0.02, 0.04, 1, 0));
    float mouthP = clamp(vin.PosL.x / extMouth.x, -1, 1);
    offsetY += extMouthMovement * mouthK * pow(mouthP, 2);

    if (extMouthMode){
      if (vin.PosL.y < extMouth.y + 0.001){
        offsetYZ.x = -extMouthOpened * max(1 - abs(mouthP) * 0.5, saturate((extMouth.y - vin.PosL.y) * 40)) * lerpInvSat(extMouth.y - vin.PosL.y, -0.001, 0) * saturate(1 - abs(vin.PosL.z - extMouth.z) * 20);
        offsetYZ.y = offsetYZ.x * 0.5;
        // if (vin.PosL.y > extMouth.y - 0.001 - extMouthOpened * (1 - abs(vin.PosL.x) / (extMouth.x * 0.2))){
        //   vout.Mouth = 1;
        // }
      }

      float lfNoise = sin(ksGameTime * 0.0011) + sin(ksGameTime * 0.0029) * 0.7 + sin(ksGameTime * 0.0047) * 0.5;
      offsetX = (lfNoise * 0.003 - 0.003) * saturate(extMouthOpened * 100) * sign(vin.PosL.x) * mouthK * pow(mouthP, 2);
      // offsetX = -0.004 * saturate(extMouthOpened * 100) * sign(vin.PosL.x) * mouthK * pow(mouthP, 2);
    }

    float3 eyebrowD = (float3(abs(vin.PosL.x), vin.PosL.yz) - extEyebrowCorner) * float3(0.25, 2, 0.25);
    float eyebrowK = saturate(remap(length(eyebrowD), 0.01, 0.02, 1, 0));
    offsetY += extEyebrowCornerMovement * eyebrowK;

    float3 cheekD = (float3(abs(vin.PosL.x), vin.PosL.yz) - extCheek) * float3(0.25, 0.5, 0.25);
    float cheekK = saturate(remap(length(cheekD), 0.01, 0.02, 1, 0));
    offsetY += extBlink * cheekK * 0.0005;

    vin.PosL.yz += clamp(offsetYZ, -0.1, 0.1);
    vout.PosL1 = vin.PosL.xyz;
    vin.PosL.x += clamp(offsetX, -0.1, 0.1);
    vin.PosL.y += clamp(offsetY, -0.005, 0.005);
  #else
    vout.PosL1 = vin.PosL.xyz;
  #endif

  float4 posW, posWnoflex, posV;
  vout.PosL = vin.PosL.xyz;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
