// #define TXDETAIL_VARIATION
#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT

#ifndef CARPAINT_DAMAGES
  #define GETNORMALW_XYZ_TX
  #define GETNORMALW_SAMPLER samLinearSimple
#endif

#define SUPPORTS_AO

#define EXTRA_CARPAINT_FIELDS\
  float stAmbientSpec;\
  float stNondetailSpec;\
  float stDetailRefl;\
  uint extStickersMode_uint;\
  float extClearCoatThickness;\
  float extClearCoatIntensity;\
  float extClearCoatIOR;\
  PARAM_CSP(float, _pad1);\
  float3 extClearCoatTint;\
  PARAM_CSP(float, _pad2);

#define LIGHTINGFX_SPECULAR_COLOR (txMapsValue.x * txSpecularColor)
#define LIGHTINGFX_SPECULAR_EXP (txMapsValue.y * ksSpecularEXP + 1)
#define LIGHTINGFX_SUNSPECULAR_COLOR (txMapsValue.x * sunSpecular)
#define LIGHTINGFX_SUNSPECULAR_EXP (txMapsValue.y * sunSpecularEXP + 1)
#define LIGHTINGFX_TXDIFFUSE_COLOR txDiffuseValue.rgb
#define GAMMA_TWEAKABLE_ALPHA

#include "common/uv2Utils.hlsl"
#include "include_new/base/_include_ps.fx"
#include "common/carDirt.hlsl"

#ifdef CARPAINT_DAMAGES
  Texture2D txSpecular : register(TX_SLOT_MAT_7);
  Texture2D txStickers : register(TX_SLOT_MAT_8);
  Texture2D txStickersMaps : register(TX_SLOT_MAT_9);
#else
  Texture2D txSpecular : register(TX_SLOT_MAT_4);
  Texture2D txStickers : register(TX_SLOT_MAT_5);
  Texture2D txStickersMaps : register(TX_SLOT_MAT_6);
#endif

float3 GetReflection(float3 reflected, float specularExp) {
  return txCube.SampleLevel(samLinear, reflected, saturate(1 - specularExp / 255) * 8).rgb;
}

struct LightingOutput {
  float3 diffuse;
  float3 specular;
};

LightingOutput calculateMapsLighting(float3 txDiffuseValue, float3 posC, float3 toCamera, float3 normalW, 
    float4 txMapsValue, float3 txSpecularColor, float3 txReflectColor, float specularA, float3 shadow, float4 ambientMult){
  //float r3w = txMapsValue.y * ksSpecularEXP + 1;
  float r3w = pow(0.5, 6 - txMapsValue.y * 6) * ksSpecularEXP;
  //float r4x = (saturate(2*txMapsValue.z) * txMapsValue.y) * sunSpecular * extSunSpecularMult;
  float r4x = (txMapsValue.z * saturate(r3w / 8)) * GAMMA_BLINNPHONG_ADJ(sunSpecular) * extSunSpecularMult;
  //float r4y = txMapsValue.y * sunSpecularEXP + 1;
  float r4y = saturate(r3w / 8) * sunSpecularEXP + 1;
  float r4z = LAMBERT(normalW, -ksLightDirection.xyz);
  float3 diffuse = ksLightColor.rgb * GAMMA_OR(1, ksDiffuse) * applyGamma(txDiffuseValue, ksDiffuse) * r4z;

  ambientMult *= sunSpecular > 0 ? txMapsValue.w : 1;
  float3 diffuseAmbient = diffuse * shadow + calculateAmbient(posC, normalW, ambientMult) * applyGamma(txDiffuseValue, ksAmbient);

  float specularBase = saturate(dot(normalize(-toCamera - ksLightDirection.xyz), normalW));
  float3 baseSpecularResult = pow(specularBase, GAMMA_BLINNPHONG_EXP(max(r3w, 1))) * (r3w + 1) / 101.0 * shadow;
  float3 sunSpecularResult = pow(specularBase, GAMMA_BLINNPHONG_EXP(max(r4y, 1))) * shadow;
  float3 specularPart = baseSpecularResult * txSpecularColor * ksLightColor.rgb
                        + sunSpecularResult * r4x * ksLightColor.rgb * txReflectColor;
  // test new ambient specular
  // old
  // float specularAmbBase = saturate(0.5+0.5*dot(normalize(reflect(toCamera, normalW)),float3(0,1.0,0)));
  // new simpler
  float specularAmbBase = saturate(0.5 + 0.5 * normalize(reflect(toCamera, normalW)).y);
  // todo? decide if a constant 2.5 is good or if it needs to vary w/ spec exp.
  float3 specularAmbPart = pow(specularAmbBase, GAMMA_GENERIC_EXP(2.5)) * 0.4 * stAmbientSpec * txMapsValue.w * specularA * txSpecularColor * ksAmbientColor_sky.rgb * perObjAmbientMultiplier;

  LightingOutput result;
  result.diffuse = diffuseAmbient;
  result.specular = specularPart + specularAmbPart;
  return result;
}

void considerDetailsSpec(float2 uv, inout float4 txDiffuseValue, inout float specularModifier){
  if (useDetail){
    float4 txDetailValue = txDetail.Sample(samLinear, uv * detailUVMultiplier);
    txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, 1 - txDiffuseValue.a);
    specularModifier = specularModifier * lerp(stNondetailSpec, txDetailValue.a * 3.1875, 1 - txDiffuseValue.a);
  }
}

#include "pbr/clear_coat.hlsl"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow1 = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  // damage-related
  #ifdef CARPAINT_DAMAGES
    float4 txDamageMaskValue = txDamageMask.Sample(samLinear, pin.Tex);
    float4 txDamageValue = txDamage.Sample(samLinear, pin.Tex);
    float normalMultiplier = dot(txDamageMaskValue, damageZones);
    float damageInPoint = saturate(normalMultiplier * txDamageValue.a);
  #endif

  // normals piece
  float normalAlpha, directedMult;
  #ifdef CARPAINT_DAMAGES
    normalW = getDamageNormalW(pin.Tex, normalW, tangentW, bitangentW, normalMultiplier, normalAlpha);
  #else    
    normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, directedMult);
    normalAlpha = 1;
  #endif

  // usual stuff
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);

  [branch]
  if (extStickersMode_uint){
    float4 txStickerValue = txStickers.Sample(samLinear, PIN_TEX2);
    txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, txStickerValue.rgb, txStickerValue.a);
    if (extStickersMode_uint & 2) {
      txDiffuseValue.a = lerp(txDiffuseValue.a, 1, txStickerValue.a);
    }
    if (extStickersMode_uint & 4) {
      float4 txStickerMapsValue = txStickersMaps.Sample(samLinear, float2(pin.Tex2X, pin.Tex2Y));
      txMapsValue.rgb = lerp(txMapsValue.rgb, txStickerMapsValue.rgb, txStickerMapsValue.a);
    }
  }
  
  RAINFX_INIT;

  // new Stereo stuff
  txMapsValue.z = txMapsValue.z * txMapsValue.w;
  float4 txSpecularValue = txSpecular.Sample(samLinear, pin.Tex);
  float specularAmbient = txSpecularValue.w;
  float specularModifier = 1;
  considerDetailsSpec(pin.Tex, txDiffuseValue, specularModifier);
  
  txSpecularValue.rgb = GAMMA_LINEAR(txSpecularValue.rgb);
  float3 txReflectColor = lerp(lerp(1, specularModifier, stDetailRefl) * txSpecularValue.rgb,  1, saturate(2 * txMapsValue.x));
  float3 txSpecularColor = GAMMA_BLINNPHONG_ADJ(ksSpecular) * specularModifier * lerp(txSpecularValue.rgb, 1, saturate(2 * txMapsValue.x - 1));

  #ifdef CARPAINT_DAMAGES
    // new damaged txDiffuseValue
    txDiffuseValue.xyz = damageInPoint > 0 
        ? lerp(txDiffuseValue.xyz, txDamageValue.xyz, damageInPoint) 
        : txDiffuseValue.xyz;

    // dirt
    float4 txDustValue = txDust.Sample(samLinear, pin.Tex);
    COMPUTE_DIRT_LEVEL;
    txDiffuseValue.xyz = lerp(txDiffuseValue.xyz, txDustValue.xyz, dirtLevel);

    // both damages and dirt affecting txMaps
    float damageInverted = 1 - damageInPoint;
    float mapsMultiplier = saturate(1 - dirtLevel * 10);
    float damageFactor = damageInPoint * (normalAlpha - 1) + 1;
    mapsMultiplier *= damageInverted;
    mapsMultiplier *= damageFactor;
    txMapsValue.x *= mapsMultiplier;
  #else
    float mapsMultiplier = 1;
    float dirtLevel = 0;
  #endif

  // clear coat
  ClearCoatParams ccP;
  ccP.intensity = extClearCoatIntensity * (1 - dirtLevel);
  ccP.thickness = extClearCoatThickness;
  ccP.ior = extClearCoatIOR;
  ClearCoat cc = CalculateClearCoat(toCamera, normalW, ccP);

  // lighting
  float4 occlusion = AO_LIGHTING;
  float3 shadow3 = shadow1;
  APPLY_CAO_CUSTOM(shadow3, occlusion.rgb);
  LightingOutput lightingOutput = calculateMapsLighting(txDiffuseValue.rgb, pin.PosC, toCamera, normalW, txMapsValue, txSpecularColor, txReflectColor, specularAmbient, shadow3, occlusion);

  /*TODO:GAMMA*/
  float3 lighting = lightingOutput.diffuse;  
  lighting += calculateEmissive(pin.PosC, txDiffuseValue.rgb, 1).rgb;
  lighting = cc.ProcessDiffuse(lighting, fresnelMaxLevel);
  lighting += lightingOutput.diffuse * extClearCoatTint * (1 - cc.absorption);
  lighting += lightingOutput.specular;

  LIGHTINGFX(lighting);
  #ifdef SIMPLEST_LIGHTING
    lighting = SIMPLEST_LIGHTING_FN(txDiffuseValue.rgb);
  #endif

  // reflections
  float extraMultiplier = isAdditive ? mapsMultiplier * EXTRA_SHADOW_REFLECTION : EXTRA_SHADOW_REFLECTION;
  ReflParams R;
  R.fresnelMaxLevel = fresnelMaxLevel * extraMultiplier;
  R.fresnelC = fresnelC /*  * extraMultiplier */;
  R.fresnelEXP = fresnelEXP;
  R.ksSpecularEXP = txMapsValue.y * ksSpecularEXP;
  R.finalMult = txMapsValue.z * AO_REFLECTION;
  R.coloredReflections = 1;
  R.coloredReflectionsColor = txReflectColor;
  R.metallicFix = 1;
  R.useStereoApproach = true;
  R.stereoExtraMult = extraMultiplier;  
  R.reflectionSampleParam = 0;
  R.useBias = true;
  R.isCarPaint = true;
  R.useSkyColor = false;
  R.forceReflectedDir = false;
  R.forcedReflectedDir = 0;
  R.globalOcclusionMult = 1;
  R.useGlobalOcclusionMult = false;
  R.useMatteFix = false;

  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE_SKIP(R);
  float3 withReflection = calculateReflection(float4(lighting, 1), toCamera, pin.PosC, normalW, R).rgb;
  // withReflection += lightingOutput.specular;
  
  float shadow = shadow1;
  RAINFX_WATER(withReflection);
  RETURN(withReflection, normalAlpha);
}
