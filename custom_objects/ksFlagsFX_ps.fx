#define NO_CARPAINT
#define SUPPORTS_AO

#define CB_MATERIAL_EXTRA_1\
  float translucencyMult;

#ifndef GAMMA_FIX
  #define SUNLIGHT_LAMBERT(N, L) saturate(dot(N, L) * 0.6 + 0.4) // TODO: apply lambert gamma?
#endif

#define STENCIL_VALUE 1
#include "include_new/base/_include_ps.fx"
// alias: ksFlagsFX_tessellation_ps

RESULT_TYPE main(GENERIC_INPUT_TYPE(PS_IN_PerPixel), bool ff : SV_IsFrontFace) {
  GENERIC_PIECE_INPUT(PS_IN_PerPixel);
  READ_VECTORS
  // if (ff) normalW *= -1;

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);

  #if defined(MAIN_SHADERS_SET) || defined(SIMPLIFIED_EFFECTS)
    float wetK = (extSceneWetness * 40 / (1 + extSceneWetness * 40)) * pin.Ao * 0.35;
    txDiffuseValue.rgb *= 1 - wetK;
    txDiffuseValue.a = lerp(txDiffuseValue.a, 1, saturate(txDiffuseValue.a * 3 - 1) * wetK * 2);
  #endif

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  #if defined(MAIN_SHADERS_SET) || defined(SIMPLIFIED_EFFECTS)
    L.specularValue = lerp(L.specularValue, 1, wetK);
    L.specularExp = lerp(L.specularExp, 100, wetK);
  #endif
  float3 lighting = L.calculate();
  lighting += shadow
    * ksLightColor.xyz 
    * applyGamma(txDiffuseValue.xyz * GAMMA_OR(1, INPUT_DIFFUSE_K), INPUT_DIFFUSE_K)
    * saturate(sqrt(dot(ksLightDirection.xyz, normalW))) 
    * translucencyMult;

  if (HAS_FLAG(FLAG_ALPHA_TEST)) {
    clip(txDiffuseValue.a - 0.01);
  }

  LIGHTINGFX(lighting);
  RETURN_BASE(lighting, txDiffuseValue.a);
}
