#define SUPPORTS_COLORFUL_AO
#define NO_NORMALMAPS
#define NO_FLAGS_LOGIC
#define TESSELLATION_FLAGS_MODE
#include "include_new/base/_include_vs.fx"
#include "common/flagsFX.hlsl"
#include "common/tessellationParallaxShared.hlsl"
#include "common/tessellation.hlsl"

#define gMinTessDistance 40
#define gMaxTessDistance 5

#define gMinTessFactor 1
#ifdef ALLOW_PARALLAX
  #define gMaxTessFactor 5
#else
  #define gMaxTessFactor 1
#endif

VS_OUT main(VS_IN vin) {
  VS_OUT vout;
  {
    float4 posW, posWnoflex, posV;
    toScreenSpace(vin.PosL, posW, posWnoflex, posV);
    vout.PosC = posW.xyz;
  }
  vout.Tex = vin.Tex;

  float2 aoHeight = FFX_loadVector(asuint(vin.TangentPacked.x));
  #ifndef MODE_SHADOWS_ADVANCED
    vout.NormalW = normalize(vin.NormalL);
    #ifdef ALLOW_PERVERTEX_AO
      if (HAS_FLAG(FLAG_MATERIAL_2)) {
        PREPARE_AO(vout.Ao);
      } else {
        vout.Ao = aoHeight.x;
      }
    #endif
  #endif
  
  vout.FlagSize.x = length(vin.NormalL) - 1000.0;
  if (abs(vout.FlagSize.x) > 20) vout.FlagSize.x = 0;
  vout.FlagSize.y = aoHeight.y;
  vout.FlagYRel = vin.TangentPacked.y;
  vout.WindOffset = vin.TangentPacked.z;
  
  #ifdef MODE_KUNOS
    // TODO: Rewrite properly, extLODDistanceMult is not available
    vout.TessFactor = 1;
  #else
    float maxFactor = gMaxTessFactor;
    if (HAS_FLAG(FLAG_MATERIAL_0)){
      maxFactor *= 3;
    }
    #ifdef MODE_SHADOWS_ADVANCED
      maxFactor *= 2;
    #endif

    float d = length(vout.PosC - ksCameraPosition.xyz) * extLODDistanceMult;
    float tess = saturate((gMinTessDistance - d) / (gMinTessDistance - gMaxTessDistance));
    vout.TessFactor = lerp(gMinTessFactor, maxFactor, tess);
  #endif
  return vout;
}
