#define SUPPORTS_NORMALS_AO
// #define SUPPORTS_DISTANT_FIX // for it to work, it needs for other shader vars to match
#define RAINFX_STATIC_OBJECT
#define AO_MIX_INPUT 0
#define USE_PS_FOG_IN_MAIN
#include "include_new/base/_include_vs.fx"

// alias: fuPBR_multilayer_parallax4_vs
// alias: fuPBR_multilayer_parallax2_vs

PS_IN_Nm main(VS_IN vin SPS_VS_ARG) {
  PS_IN_Nm vout;
  float4 posW, posWnoflex, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
