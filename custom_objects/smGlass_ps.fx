// #define USE_OLD_CODE
#ifdef USE_OLD_CODE
#include "../recreated/ksPerPixelReflection_ps.fx"
#else

#define USE_PBR
#define CARPAINT_SIMPLE
#define SUPPORTS_AO

#define CARPAINT_NM
#define GETNORMALW_SAMPLER samLinear
#define INPUT_NM_OBJECT_SPACE 0

#define LIGHTINGFX_GLASS_BACKLIT
#define LIGHTINGFX_KSDIFFUSE 1
#define GAMMA_LIGHTINGFX_KSDIFFUSE_ONE
#define LIGHTINGFX_SPECULAR_COLOR lfxSpecularPower
#define LIGHTINGFX_SPECULAR_EXP lfxSpecularEXP
#define LIGHTINGFX_SUNSPECULAR_EXP 1
#define LIGHTINGFX_SUNSPECULAR_COLOR 0
#define RAINFX_GLASS_BACKLIT

#ifdef USE_PBR

  cbuffer cbGlassParams : register(b6) {
    float extMaskPass;
    float3 extMaskPassColor;

    float extIOR;
    float extThicknessMult;
    float extThicknessProfileFix;
    float _blendingMode;

    float stAmbientSpec;
    float stAmbientEXP;
    float extUseNormalMap;
    float extRefraction;

    float extRefractionRainbow;
    float extSaturation;
    float extRefractionNormalFactor;
    float extRefractionBias;

    float extEdgeRefractionBias;
    float3 extEdgePosL;
    float extEdgeThreshold;

    #ifdef USE_PHOTOELASTICITY_EFFECT
      float extPhelMult/* = 0.5 */;
      float extPhelBaseScale/* = 6 */;
      float extPhelNoiseScale/* = 0.01 */;
      float extPhelSpotsScale/* = 0.33 */;
      float extPhelSpotsOffset/* = 0.15 */;
      float extPhelStripesScale/* = 20 */;
    #endif
  }
#endif

#include "include_new/base/_flags.fx"
#if defined(EMISSIVE_MAPPING)
  #if defined(MODE_GBUFFER) || defined(ALLOW_EXTRA_VISUAL_EFFECTS)
    #define CUSTOM_STRUCT_FIELDS\
      float3 PosL : TEXCOORD20;\
      float3 NormalL : TEXCOORD21;\
      float4 GlExtra : TEXCOORD22;
  #else
    #define CUSTOM_STRUCT_FIELDS\
      float4 GlExtra : TEXCOORD22;
  #endif
#elif defined(MODE_GBUFFER) || defined(ALLOW_EXTRA_VISUAL_EFFECTS)
  #define CUSTOM_STRUCT_FIELDS\
    float3 PosL : TEXCOORD20;\
    float3 NormalL : TEXCOORD21;
#endif

#ifdef EMISSIVE_MAPPING
  #include "emissiveMapping.hlsl"
#else
  #include "include_new/base/_include_ps.fx"
#endif

#ifdef USE_PBR
  #include "pbr/pbr.hlsl"
  #include "pbr/clear_coat.hlsl"
  #include "common/ambientSpecular.hlsl"
  #define pbReflectionBlurEnv 0
#endif

float estimateV(float x){
  // https://jsfiddle.net/x4fab/txu6amyb/11/
	x *= 0.99;
	return pow(saturate(x - pow(x, 4)), 0.84) * 0.61;
}

#ifdef USE_PHOTOELASTICITY_EFFECT
  float random(in float2 st) {
    return txNoise.SampleLevel(samLinearSimple, st.xy * extPhelNoiseScale, 0).x;
  }

  float noise(float2 st) {
    float2 i = floor(st);
    float2 f = frac(st);
    float2 u = f * f * (3 - 2 * f);
    float v1 = random(i + float2(0, 0));
    float v2 = random(i + float2(1, 0));
    float v3 = random(i + float2(0, 1));
    float v4 = random(i + float2(1, 1));
    return lerp(lerp(v1,v2, u.x), lerp(v3, v4, u.x), u.y);
  }

  float3 birefringence(float3 N, float3 PL, float3 NL, float3 V){  
    float3 L = float3(0, 1, 0);
    float3 H = normalize(L + V);
    float NdotV = saturate(dot(N, V));
    float NdotH = saturate(dot(H, N));

    float S = noise((PL.xz + float2(0.2 * NdotV, 0.5 * NdotH)) * extPhelBaseScale);
    float W = frac(extPhelStripesScale * abs(S));
    float R = floor(extPhelStripesScale * abs(S));
    float3 C = saturate(1 - rainbow(W) * extPhelMult * extPhotoElBoost);
    float U = (R % 4 != 3) * pow(saturate(txNoise.SampleLevel(samLinearSimple, 
      extPhelSpotsOffset + PL.xy * extPhelSpotsScale, 0).x * 2 - 0.2), 3);
    return lerp(1, GAMMA_LINEAR_SIMPLE(C), U);
  }
#endif

#include "refraction.hlsl"

RESULT_TYPE main(PS_IN_Nm pin){
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float inputAlpha = txDiffuseValue.a;
  RAINFX_WET(txDiffuseValue.xyz);

  [branch]
  if (extUseNormalMap) {
    float alpha, directedMult;
    normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha, directedMult);
    if (extUseNormalMap == 2){
      inputAlpha = alpha;
    }
  }

  // inputAlpha = frac(pin.PosC.x * 3);

  float4 txDiffuseValueOrigSaturation = txDiffuseValue;

  #ifdef USE_PBR
    #if defined(ALLOW_PBR_EXTRAS_PLUS)
      txDiffuseValue.rgb = saturate(lerp(luminance(txDiffuseValue.rgb), txDiffuseValue.rgb, extSaturation));

      float4 txDiffuseValueOrig = txDiffuseValue;
      txDiffuseValue.rgb *= abs(ksDiffuse);
    #endif

    float roughnessBase = 0;
    float baseMetalness = 0;

    LightingParamsPBR L;
    L.txDiffuseValue = txDiffuseValue.rgb;
    #ifdef EMISSIVE_MAPPING
      L.emissiveMult = 0;
    #else
      L.emissiveMult = 1;
    #endif
    L.shadow = shadow;
    L.globalOcclusion = 1;
    L.localOcclusion = extraShadow.y;
    L.reflectionOcclusion = EXTRA_SHADOW_REFLECTION;
    L.roughness = roughnessBase * roughnessBase;
    L.metalness = baseMetalness;
    L.reflectance = fresnelC;
    L.skipDiffuseForEmissive = false;
    APPLY_CAO_CUSTOM(L.shadow, L.localOcclusion);
    
    float reflBlur = getReflBlur(roughnessBase);
    L.reflDir = normalize(reflect(toCamera, normalW));
    L.reflDir = fixReflDir(L.reflDir, pin.PosC, normalW, reflBlur);

    LightingOutput lightingOutput = calculateLighting(pin.PosC, toCamera, normalW, 0, 0, L);

    #ifdef EMISSIVE_MAPPING
      float4 emissiveMap;
      lightingOutput.diffuse += GAMMA_LINEAR(getEmissiveValue(txDiffuseValue, pin.Tex, pin.GlExtra, emissiveMap)) * getEmissiveMult();
    #endif

    float specularAmbientMult;
    lightingOutput.specular += getAmbientSpecular(toCamera, normalW, stAmbientSpec, stAmbientEXP, specularAmbientMult);

    #if defined(USE_PHOTOELASTICITY_EFFECT) && defined(ALLOW_EXTRA_VISUAL_EFFECTS)
      float3 photoElasticityMult = birefringence(normalW, pin.PosL, pin.NormalL, -toCamera);
    #else
      float3 photoElasticityMult = 1;
    #endif
    float3 lighting = lightingOutput.diffuse + lightingOutput.specular * photoElasticityMult 
      * lerp(1, getBaseSpecularColor(txDiffuseValue.xyz, extColoredReflectionNorm), extColoredReflection);

    #if defined(ALLOW_PBR_EXTRAS_PLUS)
      float lfxSpecularPower = 4;
      float lfxSpecularEXP = 1000;
      {
        L.txDiffuseValue = txDiffuseValueOrig.rgb * ksAmbient;
        LIGHTINGFX(lighting);
      }
    #endif

    L.roughness = roughnessBase * roughnessBase;
    float3 reflColor = getReflectionAt(L.reflDir, -toCamera, lerp(pbReflectionBlurEnv, 6, reflBlur), true, 0)
      * lerp(1, getBaseSpecularColor(txDiffuseValue.xyz, extColoredReflectionNorm), extColoredReflection);
    reflColor *= photoElasticityMult;

    float NdotV = saturate(dot(-toCamera, normalW));
    float ior = extIOR;
    float V = estimateV(inputAlpha);
    float thickness = extThicknessMult * V / max(saturate(1 - inputAlpha), 0.0000001);
    float thicknessFix = extThicknessProfileFix / max(0.000001, NdotV) > 0.5;
    lightingOutput.F = saturate(lightingOutput.F + thicknessFix * lightingOutput.F);
    lightingOutput.F = GAMMA_OR(pow(lightingOutput.F, 1.8), lightingOutput.F);

    float ccDistance = ClearCoatDistance(toCamera, normalW, ior);
    float alpha = 1 - ClearCoatAttenuation(thickness, ccDistance);
    alpha = lerp(alpha, 1, lightingOutput.F.g);
    alpha = lerp(alpha, 1, specularAmbientMult);
    alpha = saturate(alpha + thicknessFix);
    if (_blendingMode) lightingOutput.F /= max(alpha, 0.001);
    alpha = GAMMA_ALPHA(alpha);

    float3 reflFinal = lightingOutput.F * lerp(reflColor, luminance(reflColor), baseMetalness) * getCPLMult(L.reflDir);
    float3 withReflection = lighting + reflFinal;
    ReflParams R = (ReflParams)0;
    R.resultBlur = clamp(reflBlur * reflBlur * 7 - 1, 0, 6);
    R.resultPower = saturate(dot(lightingOutput.F, 0.33)) * dot(L.localOcclusion, 1./3) * getCPLMult(L.reflDir);
    R.resultColor = reflFinal;
    
    // withReflection = 1;
    // alpha = extIOR;

    #ifdef KEEP_REFLECTION_PARAMS
      R.finalMult = 1;
      R.fresnelMaxLevel = 1;
      R.fresnelC = 0.2;
      R.fresnelEXP = 5;
      R.ksSpecularEXP = 500;
    #endif
  #else
    float3 lighting = calculateLighting(pin.PosC, toCamera, normalW, txDiffuseValue.rgb, shadow, 1, 1, extraShadow.y);

    ReflParams R = getReflParams(EXTRA_SHADOW_REFLECTION * AO_REFLECTION);
    R.useBias = true;
    float alpha = 1;
    float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  #endif  

  #ifdef MODE_COLORMASK
    withReflection = txDiffuseValue.rgb / max(txDiffuseValue.r, max(txDiffuseValue.g, max(txDiffuseValue.b, 0.01)));
  #else
    // clip(-1);
  #endif

  // float extRefraction = 0.04;

  RAINFX_WATER_ALPHA(withReflection, alpha);

  #if defined(MODE_GBUFFER) || defined(ALLOW_EXTRA_VISUAL_EFFECTS)
    [branch]
    if (extRefraction || extEdgeRefractionBias){
      float refractionMultBase = abs(dot(normalW, normalize(pin.NormalW)));
      refractionMultBase = saturate(refractionMultBase * extRefractionNormalFactor - (extRefractionNormalFactor - 1));
      float refractionMult = refractionMultBase * saturate((abs(dot(normalW, toCamera)) * 2 - 0.5) * 6);
      float refractionOffset = refractionMultBase * saturate((abs(dot(normalW, toCamera)) * 2 - 0.2) * 2);

      float2 offset = calculateRefractionOffset(pin.PosC, toCamera, normalW, 0.3 * lerp(extRefraction, 0.0, refractionOffset));

      float edgeK = 0;
      [branch]
      if (extEdgeRefractionBias){
        float edgeBase = dot(normalize(extEdgePosL - pin.PosL), normalize(pin.NormalL));
        edgeK = saturate(remap(edgeBase, extEdgeThreshold - 0.01, extEdgeThreshold, 0, 1));
      }

      #ifdef MODE_GBUFFER
        float4 refracted = 1;
      #elif defined(ALLOW_EXTRA_VISUAL_EFFECTS)
        float2 ssUV = pin.PosH.xy * extScreenSize.zw;
        float reflectionBias = lerp(extRefractionBias, extEdgeRefractionBias, edgeK);
        float level = max(txPrevFrame.CalculateLevelOfDetail(samLinearClamp, ssUV + offset),
          reflectionBias / log2(2 + length(pin.PosC) / extCameraTangent));
        float4 refracted = txPrevFrame.SampleLevel(samLinearClamp, ssUV + offset, level);

        [branch]
        if (extRefractionRainbow){
          float4 refracted0 = txPrevFrame.SampleLevel(samLinearClamp, ssUV + offset * extRefractionRainbow, level);
          float4 refracted2 = txPrevFrame.SampleLevel(samLinearClamp, ssUV + offset / extRefractionRainbow, level);
          refracted = float4(refracted0.r, refracted.g, refracted2.b, refracted.a);
        }

        refracted.a *= HAS_FLAG(FLAG_MATERIAL_0) ? 1 : max(edgeK, saturate((1 - refractionMult) * (extRefraction / 0.04)));

        [branch]
        if (extMaskPass){
          float3 c = txDiffuseValueOrigSaturation.rgb;
          c = c.rgb / max(c.r, max(c.g, max(c.b, 0.01)));
          c += extMaskPassColor;
          c = max(c, 0.00001);
          c = c / max(c.r, max(c.g, c.b));
          float a = saturate(alpha * extMaskPass);
          refracted.rgb *= lerp(1, c, a);
        }
      #else
        float4 refracted = 0;
      #endif

      withReflection = lerp(refracted.xyz, withReflection, lerp(1, alpha, refracted.a));
      alpha = lerp(alpha, 1, refracted.a);

      if (extEdgeRefractionBias < 0){
        withReflection = float3(1 - edgeK, edgeK, 0) * 3;
        alpha = 1;
      }
    }
  #endif

  RETURN(withReflection, alpha);
}

#endif
