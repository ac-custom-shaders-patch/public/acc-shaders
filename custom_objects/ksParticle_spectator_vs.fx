#define INCLUDE_PARTICLE_CB
#define EXCLUDE_PEROBJECT_CB
#define CUSTOM_STRUCT_FIELDS float RandomTint : COLOR;
// #define FOG_PARTICLE_VERSION
// #define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"
#include "include_new/ext_shadows/_include_vs.fx"

Texture2D txNoise : register(TX_SLOT_NOISE);

PS_IN_PerPixelCustom main(VS_IN vin SPS_VS_ARG) {
  PS_IN_PerPixelCustom vout;

  float4 posW = vin.PosL;
  posW.xyz += extSceneOffset;

  float3 posC = posW.xyz - ksCameraPosition.xyz;
  #ifdef MODE_SHADOWS_ADVANCED
    float3 dirSide = normalize(cross(ksLightDirection.xyz, float3(0, 1, 0)));
    posW.xyz += ksLightDirection.xyz * abs(vin.NormalL.x) * (vin.NormalL.y > 0 ? 1 : 0.1);
  #else
    float3 dirSide = normalize(cross(isScreenVR ? posC : extDirLook, float3(0, 1, 0)));
  #endif

  float distSquared = dot2(posC) * 2 / extCameraTangent;
  #ifdef LITE_VERSION  
    if (distSquared < vin.TangentPacked.y || distSquared > vin.TangentPacked.x){
      DISCARD_VERTEX(PS_IN_PerPixelCustom);
    }
  #elif defined(MODE_SHADOWS_ADVANCED)
    if (distSquared > vin.TangentPacked.x / 4){
      DISCARD_VERTEX(PS_IN_PerPixelCustom);
    }
  #elif defined(MODE_GBUFFER)
    if (distSquared > vin.TangentPacked.x / 8){
      DISCARD_VERTEX(PS_IN_PerPixelCustom);
    }
  #else
    if (distSquared >= vin.TangentPacked.y){
      DISCARD_VERTEX(PS_IN_PerPixelCustom);
    }
  #endif

  float3 toCamera = vin.PosL.xyz - ksCameraPosition.xyz;

  if (vin.NormalL.y > 0){
    float noiseRow = ksGameTime / 4e3;
    float noisePeriod = lerp(5, 10, frac(vin.TangentPacked.z * 579.495));
    noiseRow += sin(noiseRow * noisePeriod + vin.NormalL.z * 0.08523) / noisePeriod;

    float4 noise = txNoise.SampleLevel(samLinearSimple, float2(vin.NormalL.z * 0.017, noiseRow * 0.018 + vin.NormalL.z * 0.0711), 0);
    noise.xyz = noise.xyz * 2 - 1;
    float movementX = clamp(noise.x * 8, -1, 1);
    float movementY = clamp(noise.y * 8, -1, 1);
    float movementT = clamp(noise.z * 8, -1, 1);

    posW.xz += dirSide.xz * movementX / 30;
    posW.y += movementY / 60;
    dirSide.xz += movementT * dirSide.zx * float2(1, -1) * 0.1;
  }

  posW.y += vin.NormalL.y;
  posW.xz += dirSide.xz * vin.NormalL.x;
  #ifdef LITE_VERSION  
    float3 posWShadow = (vin.PosL.xyz + posW.xyz) / 2;
  #else
    posW.xz += (posW.xz - ksCameraPosition.xz) * saturate(vin.NormalL.y) / (1 + 2 * dot2((posW.xz - ksCameraPosition.xz)));
  #endif
  vin.PosL.xyz = posW.xyz;

  float4 posV = mul(posW, ksView);
  vout.PosH = mul(posV, ksProjection);

  vout.PosC = posC;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  vout.NormalW = -dirSide;
  #ifdef LITE_VERSION  
    posW.xyz -= ksLightDirection.xyz * 0.5;
  #endif
  shadows(posW, SHADOWS_COORDS);
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_STATIC(vin.PosL);
  vout.Ao = 1;
  vout.RandomTint = lerp(0.8, 1.2, frac(vin.TangentPacked.z * 914.875));

  #ifdef MODE_SHADOWS_ADVANCED
    OPT_STRUCT_FIELD_FOG(vout.Fog = 0);
    vout.NormalW = 0;
    vout.PosC = 0;
  #elif defined(LITE_VERSION) 
    // vout.NormalW = -normalize(posC);
    #ifdef NO_SHADOWS
      vout.Ao = 1;
    #else
      vout.Ao = getShadowBiasMult(posC, 0, float3(0, 1, 0), vout.ShadowTex0, 0, 1);
    #endif
  #endif
  SPS_RET(vout);
}
