// Redefining ksEmissive in a different cbuffer, so it would be 
// available in vertex shader without extra hacks
#define REDEFINE_KSEMISSIVE
#define INPUT_EMISSIVE3 0

#define NO_CARPAINT
#define SUPPORTS_AO
#define NO_SSAO
#define NO_SSGI
#define SKIP_PS_FOG
#include "include_new/base/_include_ps.fx"
#include "include_new/ext_functions/depth_map.fx"
#include "common/distantGlow.hlsl"

RESULT_TYPE result_red(PS_IN_PerPixelExtra2 pin){
  float3 color = float3(3, 0, 0);
  float alpha = 1;
  float3 toCamera = 0;
  float3 normalW = 0;
  RETURN_BASE(color, alpha);
}

static const float2 poissonDisk[10] = {
  float2(-0.2027472f, -0.7174203f),
  float2(-0.4839617f, -0.1232477f),
  float2(0.4924171f, -0.06338801f),
  float2(-0.6403998f, 0.6834511f),
  float2(-0.8817205f, -0.4650014f),
  float2(0.04554421f, 0.1661989f),
  float2(0.1042245f, 0.9336259f),
  float2(0.6152743f, 0.6344957f),
  float2(0.5085323f, -0.7106467f),
  float2(-0.9731231f, 0.1328296f)
};

RESULT_TYPE main(PS_IN_PerPixelExtra1 pin) {
  // if (max(pin.NormalW.x, pin.NormalW.y) > 0.995 || min(pin.NormalW.x, pin.NormalW.y) < -0.995) return result_red(pin);
  // return result_red(pin);
  // clip(-1);

  READ_VECTORS

  #ifdef NO_DEPTH_MAP
    float softK = 1.0;
    float depthZ = 0;
    float depthC = 0;
  #else
    float depthZ = getDepthAccurate(pin.PosH);
    float depthC = linearizeAccurate(pin.PosH.z);
    float softK = saturate((depthZ - depthC) * pin.NormalW.z);
    // softK = 1;

    // softK = 0;
	  // float2 random = normalize(txNoise.SampleLevel(samPoint, pin.NormalW.xy * 397, 0).xy);
    // for (int i = 0; i < 4; i++){
    //   float2 offset = float2(i & 1, i >> 1) * 2 - 1;
		//   float2 randomDirection = reflect(offset, random);
    //   float depthZ = getDepthAccurate(pin.PosH + float4(randomDirection * pin.Extra.y, 0, 0));
    //   softK += saturate((depthZ - depthC) * pin.NormalW.z);
    // }
    // softK /= 4;
  #endif

  #if !defined( ALLOW_VERTEX_ADJUSTMENTS ) || defined( ORIGINAL_MODE )
    clip(-1);
  #endif

  float4 txDiffuseValue = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 2);
  txDiffuseValue = lerp(1, txDiffuseValue, extDistantUseTexture);

  float3 glowBase = txDiffuseValue.rgb * ksEmissive;
  float3 glowNormalized = normalizeGlow(glowBase);
  float3 lighting = glowNormalized;

  float glowK = smoothstep(0, 1, saturate(1 - length(pin.NormalW.xy)));
  lighting *= pow(glowK, GAMMA_OR(extDistantEXP / 2, extDistantEXP));
  lighting *= pin.Extra.x;
  lighting = GAMMA_KSEMISSIVE(lighting) * getEmissiveMult();
  clip(dot(lighting, 1) - 0.003 * GAMMA_OR(0.0003, 1) * extMainBrightnessMult);

  float alpha = 0.001;
  lighting /= alpha;
  alpha *= softK;

  // pin.Fog = 1;

  // lighting.r = 1e6;
  RETURN_BASE(lighting, alpha);
}
