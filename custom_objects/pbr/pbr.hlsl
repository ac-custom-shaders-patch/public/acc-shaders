#include "include/common.hlsl"
#include "include_new/base/_flags.fx"
#define CLEARCOAT_GOOGLE

#ifdef USE_COMPLEX_OCCLUSION
  #if defined(GAMMA_PARAM)
    #define BRIGHTNESS_FIX_DIFFUSE GAMMA_OR(1, 0.4)
    #define BRIGHTNESS_FIX_SPECULAR GAMMA_OR(1, 0.4)
    #define GAMMA_APPLY(X) pow(max(0, X), GAMMA_PARAM)
    #define GAMMA_REVERT(X) pow(max(0, X), 1./GAMMA_PARAM)
  #else
    #define BRIGHTNESS_FIX_DIFFUSE 1
    #define BRIGHTNESS_FIX_SPECULAR 1
    #define GAMMA_PARAM 1
    #define GAMMA_APPLY(X) X
    #define GAMMA_REVERT(X) X
  #endif
#else
  #define BRIGHTNESS_FIX_DIFFUSE 0.4
  #define BRIGHTNESS_FIX_SPECULAR 0.4
  #define GAMMA_PARAM 1
  #define GAMMA_APPLY(X) X
  #define GAMMA_REVERT(X) X
#endif

struct LightingParamsPBR {
  float3 txDiffuseValue;
  float3 emissiveMult;
  float3 specMult;
  float3 reflDir;
  float roughness;
  float reflectionOcclusion;
  float3 shadow;

  #ifdef USE_COMPLEX_OCCLUSION
    float localOcclusion; 
    float4 globalOcclusion; 
  #else
    float3 localOcclusion; 
    float globalOcclusion; 
  #endif

  #ifdef USE_CLOTH_SHADING
    float3 f0;
    float3 subsurfaceColor;
  #elif defined(USE_SPECULAR_WORKFLOW)
    float3 f0;
  #else
    float reflectance;
    float metalness;
  #endif

  bool skipDiffuseForEmissive;
};

struct LightingOutput {
  float3 diffuse;
  float3 specular;
  float3 F;
};

float F_Schlick(float f0, float f90, float u){
  return f0 + (f90 - f0) * pow(1.f - u, 5.f);
}

float3 F_Schlick(float3 f0, float f90, float u){
  return f0 + (f90 - f0) * pow(1.f - u, 5.f);
}

float3 sqr(float3 x){
  return x * x;
}

float3 F_Fresnel(float3 SpecularColor, float VoH){
  float3 SpecularColorSqrt = sqrt(min(SpecularColor, float3(0.99, 0.99, 0.99)));
  float3 n = (1 + SpecularColorSqrt) / (1 - SpecularColorSqrt);
  float3 g = sqrt(n*n + VoH*VoH - 1);
  return 0.5 * sqr((g - VoH) / (g + VoH)) * (1 + sqr(((g + VoH)*VoH - 1) / ((g - VoH)*VoH + 1)));
}

float V_SmithGGXCorrelated(float NdotL, float NdotV, float alphaG2) {
  float Lambda_GGXV = NdotL * sqrt((-NdotV * alphaG2 + NdotV) * NdotV + alphaG2);
  float Lambda_GGXL = NdotV * sqrt((-NdotL * alphaG2 + NdotL) * NdotL + alphaG2);
  return 0.5 / (Lambda_GGXV + Lambda_GGXL);
}

float V_SmithGGXCorrelatedFast(float NoV, float NoL, float roughness) {
  return 0.5 / lerp(2.0 * NoL * NoV, NoL + NoV, roughness);
}

float V_Kelemen(float LoH) {
  return 0.25 / (LoH * LoH);
}

float V_Neubelt(float NoV, float NoL) {
  // Neubelt and Pettineo 2013, "Crafting a Next-gen Material Pipeline for The Order: 1886"
  return 1.0 / (4.0 * (NoL + NoV - NoL * NoV));
}

float D_GGX(float NoH, float roughness) {
  float oneMinusNoHSquared = 1.0 - NoH * NoH;
  float a = NoH * roughness;
  float k = roughness / (oneMinusNoHSquared + a * a);
  float d = k * k * (1.0 / M_PI);
  return d;

  // float f = (NoH * roughness - NoH) * NoH + 1;
  // return roughness / (f * f);
}

float D_Charlie(float NoH, float roughness) {
  // Estevez and Kulla 2017, "Production Friendly Microfacet Sheen BRDF"
  float invAlpha  = 1 / roughness;
  float cos2h = NoH * NoH;
  float sin2h = max(1 - cos2h, 0.0078125); // 2^(-14/2), so sin2h^2 > 0 in fp16
  return (2 + invAlpha) * pow(sin2h, invAlpha * 0.5) / 2;
}

float DiffuseBurley(float LdotH, float NdotV, float NdotL, float roughness){
  float FD90 = 0.5 + 2.0 * LdotH * LdotH * roughness;
  float FdV = F_Schlick(1, FD90, NdotV);
  float FdL = F_Schlick(1, FD90, NdotL);
  return FdV * FdL;
}

float DiffuseBurley_Custom(float LdotH, float NdotV, float NdotL, float roughness){
  float FD90 = 0.5 + 1.2 * LdotH * LdotH * roughness;
  float FdV = F_Schlick(1, FD90, NdotV);
  float FdL = F_Schlick(1, FD90, NdotL);
  return FdV * FdL;
}

float DiffuseDisney(float LdotH, float NdotV, float NdotL, float roughness) {
  float energyBias = lerp(0, 0.5, roughness);
  float energyFactor = lerp(1, 1 / 1.51, roughness);
  float FD90 = energyBias + 2.0 * LdotH * LdotH * roughness;
  float FdV = F_Schlick(1, FD90, NdotV);
  float FdL = F_Schlick(1, FD90, NdotL);
  return FdV * FdL * energyFactor;
}

#include "ambientSpecular.hlsl"

Texture2D<float3> txBDRFMap : register(TX_SLOT_BDRF);

float computeSpecularAO(float NoV, float ao, float roughness) {
  return saturate(pow(max(0, NoV + ao), exp2(-16.0 * roughness - 1)) - 1 + ao);
}

float3 getAmbientPBR(float3 posC, float3 dirW, float localOcclusion, float4 globalOcclusion){
  #ifndef NO_AMBIENT_MULT
    globalOcclusion.xyz *= perObjAmbientMultiplier;
  #endif

  // #if defined(ALLOW_SH_AMBIENT) && !defined(NO_EXTAMBIENT)
  //   float3 base = GAMMA_APPLY(getAmbientBaseAt(dirW, globalOcclusion)) * localOcclusion;
  //   float3 sampled = SAMPLE_REFLECTION_FN(dirW, 100, false, REFL_SAMPLE_PARAM_DEFAULT);
  //   return lerp(base, 
  //     max(
  //       base * extIBLAmbientBaseThreshold, 
  //       extIBLAmbientBrightness * lerp(dot(sampled, 0.333), sampled, extIBLAmbientSaturation)) * localOcclusion, 
  //     IS_TRACK_MATERIAL ? 0 : extIBLAmbient);
  // #else
  // #endif
  
  return GAMMA_APPLY(getAmbientBaseAt(posC, dirW, globalOcclusion)) * localOcclusion;
}

float EnvBRDFApproxNonmetal( float Roughness, float NoV )
{
	// Same as EnvBRDFApprox( 0.04, Roughness, NoV )
	const float2 c0 = { -1, -0.0275 };
	const float2 c1 = { 1, 0.0425 };
	float2 r = Roughness * c0 + c1;
	return min( r.x * r.x, exp2( -9.28 * NoV ) ) * r.x + r.y;
}

#ifdef USE_COMPLEX_OCCLUSION
  #define FN_APPLY_GAMMA applyGammaPBR
#else
  #define FN_APPLY_GAMMA applyGamma
#endif

LightingOutput calculateLighting(float3 posC, float3 toCamera, float3 normalW, float3 tangentW, float3 bitangentW, 
    LightingParamsPBR P, bool clearCoatMode = false){
  float3 N = normalW;
  float3 V = -toCamera;
  float3 L = -ksLightDirection.xyz;
  float3 H = normalize(L + V);

  float NdotV = saturate(abs(dot(N, V)) + 1e-20f);
	float NdotL = saturate(dot(L, N));
	float LdotH = saturate(dot(L, H));
	float HdotV = saturate(dot(H, V));
	float NdotH = saturate(dot(H, N));
  float HdotX = dot(H, tangentW);
  float HdotY = dot(H, bitangentW);

  LightingOutput result;
  P.roughness = clamp(P.roughness, 0.0001, 1);
  #ifndef USE_COMPLEX_OCCLUSION
    P.globalOcclusion = saturate(P.globalOcclusion);
    P.localOcclusion = saturate(P.localOcclusion);
  #endif

  #if defined(USE_CLOTH_SHADING) 
    float3 txDiffuseValue_linear = FN_APPLY_GAMMA(P.txDiffuseValue.rgb, 0.4);
  #elif !defined(GAMMA_PARAM) 
    float3 txDiffuseValue_linear = FN_APPLY_GAMMA(P.txDiffuseValue.rgb, 1);
  #else
    float3 txDiffuseValue_linear = P.txDiffuseValue.rgb;
  #endif

  #ifdef USE_CLOTH_SHADING
    float3 f0 = P.f0;
    float3 diffColor = txDiffuseValue_linear;
    float3 diffBaseMult = GAMMA_APPLY(ksLightColor.rgb * BRIGHTNESS_FIX_DIFFUSE) * P.shadow;
  #elif defined(USE_SPECULAR_WORKFLOW)
    float3 f0 = P.f0;
    float3 diffColor = txDiffuseValue_linear;
    float3 diffBaseMult = GAMMA_APPLY(ksLightColor.rgb * BRIGHTNESS_FIX_DIFFUSE) * NdotL * P.shadow;
  #else
    P.reflectance = lerp(P.reflectance, 1, P.metalness);
    float3 f0 = clearCoatMode ? P.reflectance : lerp(P.reflectance, txDiffuseValue_linear, P.metalness);
    float3 diffColor = lerp(lerp(txDiffuseValue_linear, 0, P.reflectance), 0, P.metalness);
    float3 diffBaseMult = GAMMA_APPLY(ksLightColor.rgb * BRIGHTNESS_FIX_DIFFUSE) * NdotL * P.shadow;
  #endif

  float3 specBaseMult = GAMMA_APPLY(extSpecularColor.rgb * BRIGHTNESS_FIX_SPECULAR) * NdotL * P.shadow;
  #if defined(USE_CLOTH_SHADING) || !defined(GAMMA_PARAM) 
  #else
    diffColor = FN_APPLY_GAMMA(diffColor, 0.45);
  #endif

  // float diffModel = DiffuseBurley(LdotH, NdotV, NdotL, P.roughness);
  // float diffModel = DiffuseBurley_Custom(LdotH, NdotV, NdotL, P.roughness);
  #ifdef USE_COMPLEX_OCCLUSION
    float diffModel = DiffuseDisney(LdotH, NdotV, NdotL, P.roughness);
  #else
    float diffModel = DiffuseBurley_Custom(LdotH, NdotV, NdotL, P.roughness);
  #endif
  // float diffModel = 1; // Lambert

  #ifndef ALLOW_PBR_EXTRAS_BASE
    diffModel = 1;
    #ifdef USE_CLEARCOAT 
      if (clearCoatMode){
        return (LightingOutput)0;
      }
    #endif
  #endif

  #ifdef USE_COMPLEX_OCCLUSION
    float3 indirectDiffuse = 0;

    // result.diffuse = diffModel * diffBaseMult 
    //   + GAMMA_APPLY(calculateAmbient(P.reflDir, P.globalOcclusion) * BRIGHTNESS_FIX_DIFFUSE) * P.localOcclusion;

    // result.diffuse = GAMMA_APPLY((ksLightColor.rgb * GAMMA_REVERT(NdotL * P.shadow * diffModel * diffColor) 
    //   + getAmbientPBR(posC, N, 1, P.globalOcclusion) * GAMMA_REVERT(P.localOcclusion * diffColor)) * BRIGHTNESS_FIX_DIFFUSE);

    // diffColor = pow(max(0, diffColor), 2.2);
    result.diffuse = GAMMA_APPLY(ksLightColor.rgb * BRIGHTNESS_FIX_DIFFUSE) * NdotL * P.shadow * diffModel 
      + getAmbientPBR(posC, N, 1, P.globalOcclusion * BRIGHTNESS_FIX_DIFFUSE * 0.5 /* yup that’s right */) * P.localOcclusion;

    result.diffuse *= diffColor;

    // result.diffuse *= diffColor;
  #else
    result.diffuse = diffModel * diffBaseMult * diffColor;
    float3 indirectDiffuse = diffColor * getAmbientPBR(posC, N, 1, P.globalOcclusion) * BRIGHTNESS_FIX_DIFFUSE * P.localOcclusion;
  #endif
  float3 emissive = calculateEmissive(posC, P.skipDiffuseForEmissive ? 1 : P.txDiffuseValue.rgb, P.emissiveMult);

  #if defined(USE_CLEARCOAT) && defined(ALLOW_CLEARCOAT)
    if (!clearCoatMode){
      f0 = lerp(f0, pow((1 - 5 * sqrt(f0)) / (5 - sqrt(f0)), 2), pbClearCoatIntensity);
    }
  #endif

  P.reflectionOcclusion *= saturate(50.0 * dot(f0, 0.33));

  // float3 BRDF = txBDRFMap.Sample(samLinearClamp, float2(NdotV, 1 - sqrt(P.roughness)));
  float3 BRDF = txBDRFMap.SampleLevel(samLinearClamp, float2(NdotV, 1 - sqrt(P.roughness)), 0);

  #ifdef USE_CLOTH_SHADING

    // P.roughness = max(P.roughness, 0.1); // dirty hack (UPD: why?)

    float Vis = V_Neubelt(NdotV, NdotL);
    float D = D_Charlie(NdotH, clamp(P.roughness, 0.1, 1));
    if (dot(P.subsurfaceColor, 1) > 0.001){
      result.diffuse *= saturate((NdotL + 0.5) / 2.25);
      result.diffuse *= saturate(P.subsurfaceColor + NdotL);
      indirectDiffuse *= saturate(P.subsurfaceColor + NdotV);
    } else {
      result.diffuse *= NdotL;
    }

    result.diffuse = LFM_CONTRIB(result.diffuse);
    result.diffuse += LFM_CONTRIB(indirectDiffuse);
    result.diffuse += LFM_CONTRIB(emissive);

    float3 Fr = (D * Vis) * f0 / M_PI;
    result.specular = max(Fr * specBaseMult, 0) * P.reflectionOcclusion;

    // BRDF.z = BRDF.z / (1 + BRDF.z); // dirtiest of hacks, very temporary, figure out DFG (UPD: what car was broken before?)
    result.F = BRDF.z * f0;
    result.F *= computeSpecularAO(NdotV, P.reflectionOcclusion, P.roughness);
    
  #else

    result.diffuse = LFM_CONTRIB(result.diffuse);
    result.diffuse += LFM_CONTRIB(indirectDiffuse);
    result.diffuse += LFM_CONTRIB(emissive);

    // f0 = 0.001;

    float3 F = F_Schlick(f0, 1, HdotV);
    float Vis = clearCoatMode ? V_Kelemen(LdotH) : V_SmithGGXCorrelatedFast(NdotV, NdotL, P.roughness);
    float D = D_GGX(NdotH, P.roughness);

    float3 Fr = (D * Vis) * (clearCoatMode ? 1 : F);
    float3 energyCompensation = 1 + f0 * (1 / BRDF.y - 1);
    Fr *= energyCompensation;
    result.specular = max(Fr * specBaseMult, 0) * P.reflectionOcclusion;
    result.F = lerp(BRDF.x, BRDF.y, f0);
    // result.F = f0 * BRDF.x + BRDF.y;
    // result.F = EnvBRDFApproxNonmetal(sqrt(P.roughness), NdotV);
    result.F *= energyCompensation;
    result.F *= computeSpecularAO(NdotV, P.reflectionOcclusion, P.roughness);
    // result.F = NdotV;
    
    float horizon = saturate(1 + dot(normalize(reflect(toCamera, normalW)), normalW));
    // result.F *= horizon * horizon;

  #endif

  #ifndef ALLOW_PBR_EXTRAS_BASE
    result.specular = 0;
    result.F = 0;
  #endif

  result.diffuse = LFM_SUM(result.diffuse);
  return result;
}

float getReflBlur(float roughtness){
  return pow(saturate(roughtness), 0.1);
}