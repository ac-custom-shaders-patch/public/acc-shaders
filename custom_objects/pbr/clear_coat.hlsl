float ClearCoatDistance(float3 toCamera, float3 normalW, float clearCoatIOR){
  float3 refracted = refract(toCamera, normalW, 1 / clearCoatIOR);
  return 1 / max(0.001, saturate(dot(normalW, -refracted)));
}

float ClearCoatAttenuation(float attenuationFactor, float distance){
  // http://in2gpu.com/2014/07/22/create-fog-shader/
  return exp(-attenuationFactor * distance);
}

float SchlickFresnel(float i){
  return pow(saturate(1 - i), 5);
}

float SchlickIORFresnelFunction(float LdotH, float ior){
  float f0 = (ior - 1) / (ior + 1);
  f0 *= f0;
  return f0 + (1 - f0) * SchlickFresnel(LdotH);
}

struct ClearCoatParams {
  float intensity;
  #ifdef CLEARCOAT_GOOGLE
    float f0;
  #else
    float thickness;
    float ior;
  #endif
};

struct ClearCoat {
  #ifndef CLEARCOAT_GOOGLE
    float absorption;
  #endif
  float fresnel;

  float3 ProcessDiffuse(float3 diffuse, float fresnelLimit = 1){
    #ifdef CLEARCOAT_GOOGLE
      return diffuse * (1 - fresnel);
    #else
      return diffuse * absorption * (1.0 - fresnel * fresnelLimit);
    #endif
  }

  float3 ProcessSpecular(float3 specular, float3 clearCoatSpecular){
    #ifdef CLEARCOAT_GOOGLE
      return specular * pow(1 - fresnel, 2) + clearCoatSpecular * fresnel;
    #else
      return specular * absorption + clearCoatSpecular;
    #endif
  }
};

ClearCoat CalculateClearCoat(float3 toCamera, float3 normalW, ClearCoatParams P){  
  float3 H = normalize(-ksLightDirection.xyz - toCamera);
  float LdotH = saturate(dot(-ksLightDirection.xyz, H));
  float NdotV = saturate(dot(normalW, -toCamera));

  ClearCoat result;
  #ifdef CLEARCOAT_GOOGLE
    result.fresnel = F_Schlick(P.f0, 1.0, NdotV) * P.intensity;
    result.fresnel = GAMMA_LINEAR_SIMPLE(result.fresnel);
  #else
    float ccDistance = ClearCoatDistance(toCamera, normalW, P.ior);
    result.absorption = lerp(1, ClearCoatAttenuation(P.thickness, ccDistance), P.intensity);
    result.fresnel = P.intensity * SchlickIORFresnelFunction(NdotV, P.ior);
    result.absorption = GAMMA_LINEAR_SIMPLE(result.absorption);
    result.fresnel = GAMMA_LINEAR_SIMPLE(result.fresnel);
  #endif
  return result;
}