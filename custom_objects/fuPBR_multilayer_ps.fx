#ifdef MULTILAYER_SAME_MULTS
  #define PBR_MATERIAL\
    float detailNMMult;\
    float multR;\
    float magicMult;
  #define multG multR
  #define multB multR
  #define multA multR
#else
  #define PBR_MATERIAL\
    float detailNMMult;\
    float multR;\
    float multG;\
    float multB;\
    float multA;\
    float magicMult;
#endif
#define RAINFX_STATIC_OBJECT

#define FLAG_TILING_FIX 256
#define FLAG_NORMALIZE_MASK 512
#define FLAG_GLOBAL_AO_FROM_DIFFUSE_ALPHA 1024

#define EARLY_PREP\
  float2 texPosW = ksInPositionWorld.xz;\
  float4 txMaskValue = txMask.Sample(samLinear, pin.Tex);\
  if (pbrFlags_uint & FLAG_NORMALIZE_MASK) txMaskValue /= max(1, dot(txMaskValue, 1));
#define PBRINPUT_DECL , float2 texPosW, float4 txMaskValue
#define PBRINPUT_PASS , texPosW, txMaskValue

#include "fuPBR_gen_ps.hlsl"

Texture2D txDetailRAlbedoAO : register(TX_SLOT_MAT_2);
Texture2D txDetailRNmMeRo : register(TX_SLOT_MAT_3);
Texture2D txDetailGAlbedoAO : register(TX_SLOT_MAT_4);
Texture2D txDetailGNmMeRo : register(TX_SLOT_MAT_5);
Texture2D txDetailBAlbedoAO : register(TX_SLOT_MAT_6);
Texture2D txDetailBNmMeRo : register(TX_SLOT_MAT_7);
Texture2D txDetailAAlbedoAO : register(TX_SLOT_MAT_8);
Texture2D txDetailANmMeRo : register(TX_SLOT_MAT_9);
Texture2D txNormal2 : register(TX_SLOT_MAT_10);

float tiltBias(PS_IN_Nm pin);

PBRInput getPBRInput(PS_IN_Nm pin, inout float shadowMult PBRINPUT_DECL){
  PBRInput I = createPBRInput();

  float4 txNormalBaseValue = txNormal2.Sample(samAnisotropic, pin.Tex * detailNMMult);
  I.nmBase = (pbrFlags_uint & FLAG_DXT5NM_NORMAL) ? txNormalBaseValue.wy : txNormalBaseValue.xy;

  float4 txDiffuseValue = txDiffuse.SampleBias(samLinear, pin.Tex, tiltBias(pin));  
  if (pbrFlags_uint & FLAG_GLOBAL_AO_FROM_DIFFUSE_ALPHA) I.globalAO = txDiffuseValue.w;

  float4 txDetailRAlbedoAOValue;
  float4 txDetailGAlbedoAOValue;
  float4 txDetailBAlbedoAOValue;
  float4 txDetailAAlbedoAOValue;
  float4 txDetailRNmMeRoValue;
  float4 txDetailGNmMeRoValue;
  float4 txDetailBNmMeRoValue;
  float4 txDetailANmMeRoValue;
  #if defined(USE_MULTIMAP_TEXTURE_VARIATION) && !defined(USE_PARALLAX)
    [branch]
    if (pbrFlags_uint & FLAG_TILING_FIX){
      txDetailRAlbedoAOValue = textureSampleVariation(txDetailRAlbedoAO, samLinear, texPosW * multR);
      txDetailGAlbedoAOValue = textureSampleVariation(txDetailGAlbedoAO, samLinear, texPosW * multG);
      txDetailBAlbedoAOValue = textureSampleVariation(txDetailBAlbedoAO, samLinear, texPosW * multB);
      txDetailAAlbedoAOValue = textureSampleVariation(txDetailAAlbedoAO, samLinear, texPosW * multA);
      txDetailRNmMeRoValue = textureSampleVariation(txDetailRNmMeRo, samLinear, texPosW * multR);
      txDetailGNmMeRoValue = textureSampleVariation(txDetailGNmMeRo, samLinear, texPosW * multG);
      txDetailBNmMeRoValue = textureSampleVariation(txDetailBNmMeRo, samLinear, texPosW * multB);
      txDetailANmMeRoValue = textureSampleVariation(txDetailANmMeRo, samLinear, texPosW * multA);
    } else {
      txDetailRAlbedoAOValue = txDetailRAlbedoAO.Sample(samLinear, texPosW * multR);
      txDetailGAlbedoAOValue = txDetailGAlbedoAO.Sample(samLinear, texPosW * multG);
      txDetailBAlbedoAOValue = txDetailBAlbedoAO.Sample(samLinear, texPosW * multB);
      txDetailAAlbedoAOValue = txDetailAAlbedoAO.Sample(samLinear, texPosW * multA);
      txDetailRNmMeRoValue = txDetailRNmMeRo.Sample(samLinear, texPosW * multR);
      txDetailGNmMeRoValue = txDetailGNmMeRo.Sample(samLinear, texPosW * multG);
      txDetailBNmMeRoValue = txDetailBNmMeRo.Sample(samLinear, texPosW * multB);
      txDetailANmMeRoValue = txDetailANmMeRo.Sample(samLinear, texPosW * multA);
    }
  #else
    txDetailRAlbedoAOValue = txDetailRAlbedoAO.Sample(samLinear, texPosW * multR);
    txDetailGAlbedoAOValue = txDetailGAlbedoAO.Sample(samLinear, texPosW * multG);
    txDetailBAlbedoAOValue = txDetailBAlbedoAO.Sample(samLinear, texPosW * multB);
    txDetailAAlbedoAOValue = txDetailAAlbedoAO.Sample(samLinear, texPosW * multA);
    txDetailRNmMeRoValue = txDetailRNmMeRo.Sample(samLinear, texPosW * multR);
    txDetailGNmMeRoValue = txDetailGNmMeRo.Sample(samLinear, texPosW * multG);
    txDetailBNmMeRoValue = txDetailBNmMeRo.Sample(samLinear, texPosW * multB);
    txDetailANmMeRoValue = txDetailANmMeRo.Sample(samLinear, texPosW * multA);
  #endif

  float4 combinedAlbedoAOValue = 
      txDetailRAlbedoAOValue * txMaskValue.x
    + txDetailGAlbedoAOValue * txMaskValue.y
    + txDetailBAlbedoAOValue * txMaskValue.z
    + txDetailAAlbedoAOValue * txMaskValue.w;
  float4 combinedNmMeRoValue = 
      (txDetailRNmMeRoValue * float4(2, 2, 1, 1) - float4(1, 1, 0, 0)) * txMaskValue.x
    + (txDetailGNmMeRoValue * float4(2, 2, 1, 1) - float4(1, 1, 0, 0)) * txMaskValue.y
    + (txDetailBNmMeRoValue * float4(2, 2, 1, 1) - float4(1, 1, 0, 0)) * txMaskValue.z
    + (txDetailANmMeRoValue * float4(2, 2, 1, 1) - float4(1, 1, 0, 0)) * txMaskValue.w;
  txDiffuseValue.rgb *= combinedAlbedoAOValue.rgb;
  txDiffuseValue.rgb *= magicMult;
  I.albedo = saturate(txDiffuseValue.rgb);
  I.nmExtra = saturate(combinedNmMeRoValue.xy * 0.5 + 0.5);
  I.useNmExtra = true;
  I.localAO = saturate(combinedAlbedoAOValue.w);
  I.roughness = saturate(combinedNmMeRoValue.w);
  I.setF0(saturate(combinedNmMeRoValue.z));

  return I;
}
