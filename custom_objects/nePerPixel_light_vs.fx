// Redefining ksEmissive in a different cbuffer, so it would be 
// available in vertex shader without extra hacks
#define REDEFINE_KSEMISSIVE
#define INPUT_EMISSIVE3 0

#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"

PS_IN_Nm main(VS_IN vin SPS_VS_ARG) {
  GENERIC_PIECE_NM(PS_IN_Nm);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
