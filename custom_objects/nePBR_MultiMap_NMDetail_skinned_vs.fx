#define SUPPORTS_NORMALS_AO
#define SKINNED_VERTEX

#include "common/uv2Utils.hlsl"
#include "include_new/base/_include_vs.fx"

// alias: nePBR_MultiMap_Cloth_skinned_vs

PS_IN_Nm main(VS_IN_Skinned vin SPS_VS_ARG) {
  PS_IN_Nm vout;
  float4 posW, posWnoflex, posV;
  float3 normalW, tangentW;
  vout.PosH = toScreenSpaceSkinned(vin.PosL, vin.NormalL, vin.TangentPacked, vin.BoneWeights, vin.BoneIndices,
    posW, posWnoflex, normalW, tangentW, posV);

  normalW = normalize(normalW);
  tangentW = normalize(tangentW);

  vout.NormalW = normalW;
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  shadows(posW, SHADOWS_COORDS);

  #ifndef NO_NORMALMAPS
    vout.TangentW = tangentW;
    vout.BitangentW = -cross(normalW, tangentW);
  #endif

  PREPARE_UV2;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION_SKINNED(vin.PosL);
  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
