#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"
// alias: nePerPixelNM_heating_vs
// alias: nePerPixelMultiMap_NMDetail_heating_vs

PS_IN_NmPosL main(VS_IN vin SPS_VS_ARG) {
  PS_IN_NmPosL vout;
  float4 posW, posWnoflex, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  vout.PosL = vin.PosL.xyz;
  vout.NormalL = vin.NormalL.xyz;
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
