// Redefining ksEmissive in a different cbuffer, so it would be 
// available in vertex shader without extra hacks
#define REDEFINE_KSEMISSIVE
#define INPUT_EMISSIVE3 0
#define FOG_VS_COMPUTE_SECOND

#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"
#include "common/distantGlow.hlsl"

#define PixelScale max(extScreenSize.z, extScreenSize.w)

PS_IN_PerPixelExtra1 main(VS_IN vin SPS_VS_ARG) {
  #ifdef MODE_KUNOS
    DISCARD_VERTEX(PS_IN_PerPixelExtra1);
  #else
    // vin.PosL.xyz += extSceneOffset;
    float4 posW = mul(vin.PosL, ksWorld);
    float3 posC = posW.xyz - ksCameraPosition.xyz;
    float distance = length(posC);
    float3 look = normalize(posC);
    float3 side = normalize(cross(float3(0, 1, 0), posC));
    float3 up = -normalize(cross(side, posC));

    float ambientK = extGlowBrightness;
    float fovK = tan(3.141592 / 180 * ksFOV / 2.4);
    float pixelRadius = mul(mul(posW, ksView), ksProjection).w * PixelScale;
    float distanceK = max(pixelRadius * fovK, 0);

    float3 fogPosC = normalize(posC) * min(pow(length(posC) / 1000, 1.6) * 1000, length(posC));

    float fogK;
    if (GAMMA_FIX_ACTIVE) {
      fogK = calculateFogNewFn(fogPosC, normalize(fogPosC), true);
    } else {
      fogK = calculateFogImpl(0, 0, fogPosC);
    }

    fogK = pow(fogK, 2);
    float multSize = 1 + GAMMA_OR(2, 4) * fogK;
    float multOpacity = 1 - fogK;

    float2 angle = vsLoadAltTex(vin.TangentPacked);
    posW.xyz += (up * angle.y + side * angle.x) * distanceK * extDotRadiusMult 
      * ambientK * multSize;
    posW.xyz -= look;

    float3 normalW = normals(vin.NormalL.xyz);
    float normalLength = length(normalW);
    normalW /= normalLength;
    
    float normalConcentration = pow(saturate(normalLength), 2);
    vin.NormalL.xyz = 0;

    PS_IN_PerPixelExtra1 vout;
    float4 posV = mul(posW, ksView);
    vout.PosH = mul(posV, ksProjection);
    vout.NormalW = normalW;
    vout.PosC = posW.xyz - ksCameraPosition.xyz;
    vout.Tex = vin.Tex;
    OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFogImpl(posV, posW, fogPosC));
    GENERIC_PIECE_MOTION(vin.PosL);
    PREPARE_AO(vout.Ao);
    shadows(posW, SHADOWS_COORDS);

    float NdotV = pow(saturate(dot(-look, normalW)), 0.25);
    vout.NormalW.xy = angle;
    vout.NormalW.z = lerp(1, NdotV, normalConcentration);

    float multExtra = smoothstep(4, 24, ksFOV) * saturate(distance / 80 - 0.3);
    #ifdef INCREASE_GLOW_DOT
      // multExtra = 1 * saturate(distance / 40 - 0.3);
    #endif

    vout.Extra = vout.NormalW.z * vout.NormalW.z
      * multExtra * multOpacity
      * saturate(2 - distance * extMaxDistanceHalfInv);
    if (vout.Extra.x <= 0.001){
      DISCARD_VERTEX(PS_IN_PerPixelExtra1);
    }
    vout.Extra = vout.Extra * extGlowBrightness;
    if (GAMMA_FIX_ACTIVE) {
      OPT_STRUCT_FIELD_FOG(vout.Extra.x *= 1 - pow(vout.Fog, 3));
    }

    SPS_RET(vout);
  #endif
}
