#define INCLUDE_TYRE_CB
#define CARPAINT_NM
#define USE_BLURRED_NM
#define SPECULAR_DIFFUSE_FIX
// #define FORCE_BLURREST_REFLECTIONS
#define EXTRA_SHADOW_AFFECTS_REFLECTION
// #define LIGHTING_MODEL_COOKTORRANCE
// #define RAINFX_STATIC_OBJECT
#define GETSHADOW_BIAS_MULTIPLIER 4
#define NO_ADJUSTING_COLOR

#define CB_MATERIAL_EXTRA_4\
	float extNeutralReflectionOcclusion;\
	float extGroundOcclusion;\
  float extRoughnessExp;\
  float extAlphaMultInv;\
  uint extTyresFlags_uint;

#define CUSTOM_STRUCT_FIELDS\
  float Blown : TEXCOORD20;\
  float BlownFlap : TEXCOORD21;

#include "include_new/base/_flags.fx"
#define CALCULATEREFLECTION_USEFRESNELMULT_AS_VALUE

#include "include_new/base/_include_ps.fx"

struct TyresMaterialParams {
  float aoMult;
  float rainOcclusion;
  float specularValue;
  float specularExp;
  float reflectivity;
  float wetMaskBlend; // how much alpha of txDiffuse is used as a mask for wetness
  float2 uv;
};

float3 normalTexToW(float3 txNormalValue, float3 normalW, float3 tangentW, float3 bitangentW){
  float3x3 m = float3x3(tangentW, normalW, bitangentW);
  return normalize(mul(transpose(m), (txNormalValue * 2 - 1).xzy));
}

#ifdef MODE_NORMALSAMPLE
  #define ALLOW_TYRESFX
#endif

#ifdef ALLOW_TYRESFX
  #include "include_new/ext_tyresfx/_include_ps.fx"
#endif

float3 getHeatColor(float h) {
  float T = h * 5600;
  if (T < 400) return 0;
  float3 f = float3(1, 1.5, 2);
  float3 O = 100 * f * f * f / (exp(f * 19e3 / T) - 1);
  return 3 * O;
}

RESULT_TYPE main(PS_IN_Nm pin, bool isFrontFace : SV_IsFrontFace) {
  READ_VECTORS_NM

  float3 normalShapeW = normalW;

  #ifdef ALLOW_TYRESFX
    float shadow = getShadowBiasMult(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, shadowBiasMult, AO_FALLBACK_SHADOW);
  #else
    float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  #endif
  APPLY_EXTRA_SHADOW
  #ifdef ALLOW_TYRESFX
    shadow *= lerp(1, lerp(extraShadow.x, 1, pow(saturate(dot(normalShapeW, -contactNormal)), 2)), distanceMult);
  #endif

  #ifdef MODE_NORMALSAMPLE
    float blurLevel = 0;
    float dirtyLevel = 0;
  #endif

  float texBias = isFrontFace ? 10 : 0;
  float4 txDiffuseValue = lerp(txDiffuse.SampleBias(samLinear, pin.Tex, texBias), txBlur.SampleBias(samLinear, pin.Tex, texBias), blurLevel);
  // if (!isFrontFace) {
  //   txDiffuseValue.r = 1;
  // }
  #ifndef NO_NORMALMAPS
    float4 txNormalValue = lerp(txNormal.SampleBias(samLinear, pin.Tex, texBias), txNormalBlur.SampleBias(samLinear, pin.Tex, texBias), blurLevel);
    txNormalValue.xy = (extTyresFlags_uint & uint2(1, 2)) ? 1 - txNormalValue.xy : txNormalValue.xy;
  #else
    float4 txNormalValue = 0;
  #endif

  if (isFrontFace) txDiffuseValue.rgb *= 0.5;

  #ifdef ALLOW_TYRESFX
    float4 txDirtyValue = txDirty.Sample(samLinear, pin.Tex);
    float dirtyLevelAdj = dirtyLevel * txDirtyValue.a;
    float reflectionMult = lerp((1 - dirtyLevelAdj) * (extRoughnessExp ? txDiffuseValue.a : 1), 1, dirtFade);
    txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, txDirtyValue.rgb, dirtyLevelAdj * (1 - dirtFade));
  #else
    float reflectionMult = 1;
  #endif

  float blown = pin.Blown.x;
  // float4 noiseLf = txNoise.SampleLevel(samLinearSimple, float2(pin.Blown4.z * 4, pin.Blown4.w), 0.8);
  // float4 noiseHf = txNoise.SampleLevel(samLinearSimple, float2(0, pin.Blown4.w * 4) + 17.31, 0);

  txDiffuseValue = lerp(txDiffuseValue, float4(0.1, 0.1, 0.1, 1), saturate(blown * 20 - 1));
  txNormalValue = lerp(txNormalValue, float4(0.5, 0.5, 1, 0.5), saturate(blown * 20 - 1));
  
  // txDiffuseValue.rgb = noiseLf.rgb;
  // txDiffuseValue.rgb = pin.Blown4.w;

  // clip(saturate(noiseLf.x * noiseHf.x * 3) - pin.Blown4.y);
  // txDiffuseValue.rgb = pin.Blown4.y;

  #ifndef NO_NORMALMAPS
    bool useAlphaFromNormals = extTyresFlags_uint & 4;
    bool hasRoughtnessMap = extRoughnessExp && useAlphaFromNormals;
  #else
    bool useAlphaFromNormals = false;
    bool hasRoughtnessMap = false;
  #endif

  TyresMaterialParams TM = (TyresMaterialParams)0;
  TM.aoMult = 1;
  TM.specularValue = ksSpecular * txDiffuseValue.a;
  TM.specularExp = lerp(ksSpecularEXP, extRoughnessExp, hasRoughtnessMap ? txNormalValue.a : 0);
  TM.reflectivity = fresnelMaxLevel * (hasRoughtnessMap ? txDiffuseValue.a : 1);
  TM.wetMaskBlend = 1;
  TM.uv = pin.Tex;

  #ifndef NO_NORMALMAPS
    normalW = normalTexToW(txNormalValue.xyz, normalW, tangentW, bitangentW);
  #endif

  #ifdef ALLOW_TYRESFX
    ReflectionCoverParams coverParams;
    #if defined(ALLOW_RAINFX) && !defined(MODE_SIMPLIFIED_FX)
      float rainOcclusion = saturate(pin.RainOcclusion);
    #else
      float rainOcclusion = 1;
    #endif
    float water = getTyreColorDirt(SPSAwarePosC, toCamera, normalize(pin.NormalW), blown, pin.BlownFlap, normalW, normalShapeW, txDiffuseValue.xyz, TM, rainOcclusion, coverParams);
  #else
    float4 txDirtyValue = txDirty.Sample(samLinear, pin.Tex);
    float dirtyLevelAdj = dirtyLevel * txDirtyValue.a;
    txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, txDirtyValue.rgb, dirtyLevelAdj);
    TM.wetMaskBlend = 1 - dirtyLevelAdj;
    TM.specularExp *= 1 - dirtyLevelAdj;
    TM.reflectivity = fresnelMaxLevel * (1 - dirtyLevelAdj) * extNeutralReflectionOcclusion;
    float water = 0;
  #endif

  float wetK = water * lerp(1, saturate(txDiffuseValue.a * 10), TM.wetMaskBlend);
  txDiffuseValue.xyz *= lerp(1, 0.2, wetK);
  RAINFX_INIT;

  // extraShadow *= abs(1 - 2 * blown);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.multAO(TM.aoMult * saturate(extraShadow.y + saturate(-normalShapeW.y)));
  L.specularValue = lerp(TM.specularValue, 1, wetK);
  L.specularExp = lerp(TM.specularExp, 400, wetK);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  lighting *= lerp(1, sqrt(saturate(normalShapeW.y + 1)), extGroundOcclusion);
  // lighting = 1;

  ReflParams R = getReflParamsBase(EXTRA_SHADOW_REFLECTION * AO_REFLECTION);
  R.finalMult = saturate(normalShapeW.y + 1) * EXTRA_SHADOW_REFLECTION;

  // if (noiseLf.w > 0.6) wetK = max(wetK, shine);
  R.fresnelC = lerp(R.fresnelC, 0.01, wetK);
  R.fresnelMaxLevel = lerp(TM.reflectivity * reflectionMult, 0.15, wetK);
  R.fresnelEXP = lerp(R.fresnelEXP, 4, wetK);
  R.ksSpecularEXP = lerp(TM.specularExp, 100, wetK);

  // R.ksSpecularEXP = 100;
  // R.fresnelC = 0.01;
  // R.fresnelEXP = 2;
  // R.fresnelMaxLevel = 0.15;
  // R.fresnelMaxLevel = 10.15;

  #if defined(ALLOW_TYRESFX)
    R.finalMult *= lerp(coverParams.valueOccluded, coverParams.valueBase, pow(coverParams.occlusionParam, lerp(1, 4, wetK)));
    // lighting = float3(pow(coverParams.occlusionParam, lerp(1, 4, wetK)), 1 -  pow(coverParams.occlusionParam, lerp(1, 4, wetK)), 0);
  #endif

  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  R.resultColor = R.resultPower = 0;
  // withReflection = lighting;

  #ifdef ALLOW_RAINFX
    RP.alpha *= EXTRA_SHADOW_REFLECTION;
  #endif

  RAINFX_WATER(withReflection);
  // withReflection.xyz = withReflection.xyz * 0.0001 + saturate(normalW);
  #ifdef ALLOW_TYRESFX
    // withReflection.xyz = withReflection.xyz * 0.0001 + abs(customNormalsScaleY);
    // withReflection.xyz = float3(water, 1 - water, 0) * 1.6;
  #endif
    
  // withReflection.xyz = float3(pin.Tex, 0) * 4;
  // withReflection.xyz = txDiffuseValue.rgb * 4;

  #ifdef ALLOW_TYRESFX
    float btt = blownTyresTemperature;
    [branch]
    if (btt > 0){
      float3 dir = normalize(pin.PosC + ksCameraPosition.xyz - wheelPos);
      float sideDot = dot(dirSide, dir);
      float3 glow = getHeatColor(blownTyresTemperature * lerp(0.3, 1, saturate(abs(sideDot) * 2))) * saturate(blown * 20 - 1);

      if (GAMMA_FIX_ACTIVE) {
        glow = pow(glow * 0.1, 4);
      }
      // float dirYZ = dot2(dir - dirSide * sideDot) - rimRadiusSqr - 0.8;
      withReflection += glow * getEmissiveMult();
      // withReflection += max(0, dirYZ) * 100;
    }
  #endif

  // withReflection = (normalW + 1) * 0.003;

  RETURN(withReflection, (1 - extAlphaMultInv) * (useAlphaFromNormals ? txNormalValue.a : 1));
}
