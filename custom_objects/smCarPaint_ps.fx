// alias: smCarPaint_bending_ps

// #undef ALLOW_RAINFX
// #define USE_OLD_CODE
#ifdef USE_OLD_CODE
// # include "../recreated/__ksPerPixelMultiMap_damage_dirt_ps.fx"
#else

#define DIFFUSE_USUALLY_CONTAINS_AO
#define LIGHTINGFX_TXDIFFUSE_COLOR txDiffuseValue.rgb
#define TXDETAIL_VARIATION (extCarPaintFlags_uint & 1)
#define MATTE_FIX (extCarPaintFlags_uint & 2)
#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define CARPAINT_DAMAGES
#define SUPPORTS_AO
#define INPUT_EMISSIVE3 0
#define CARPAINT_DAMAGEZONESDIRT
#define USE_SECONDARY_REFLECTION_LAYER

#ifndef EXTRA_CARPAINT_FIELDS_2
  #define EXTRA_CARPAINT_FIELDS_2
#endif
#ifndef ALTER_NORMAL
  #define ALTER_NORMAL
#endif
#ifndef ALTER_DIFFUSE_COLOR
  #define ALTER_DIFFUSE_COLOR
#endif
#ifndef ALTER_LIGHTING
  #define ALTER_LIGHTING
#endif

#define EXTRA_CARPAINT_FIELDS\
  float extClearCoatSpecular;\
  float3 extClearCoatTint;\
  \
  float extClearCoatThickness;\
  float extClearCoatIntensity;\
  float extClearCoatIOR;\
  float extClearCoarMask;\
  \
  uint extStickersMode_uint;\
  float3 extSpecColor;\
  \
  float extPearlSpecular;\
  float extColoredSpecular;\
  float stAmbientSpec;\
  float stAmbientEXP;\
  \
  uint extCarPaintFlags_uint;\
  float extEffectsMask;\
  float extFlakesK;\
  float extColoredReflections;\
  \
  float extDiffuseSmoothnessFrom;\
  float extDiffuseSmoothnessTo;\
  float extNormalizeAO;\
  float extSecondaryReflectionLayer;\
  EXTRA_CARPAINT_FIELDS_2

#define LIGHTINGFX_SPECULAR_EXP (txMapsValue.y * ksSpecularEXP + 1)
#define LIGHTINGFX_SPECULAR_COLOR (L.specMult * txMapsValue.x * ksSpecular)
#define LIGHTINGFX_SUNSPECULAR_EXP (txMapsValue.y * sunSpecularEXP + 1)
#define LIGHTINGFX_SUNSPECULAR_COLOR (L.specMult * txMapsValue.x * sunSpecular)

#ifdef HAS_CHROMA_FLAIR
  float3 chromaFlairMod(float3 toCamera, float3 normalW, float3 lightDir, float altFlakesValue, bool fixedLevel = false);
  #define LIGHTINGFX_FUNC_PARAM_DECLARE , float altFlakesValue
  #define LIGHTINGFX_FUNC_PARAM_PASS , altFlakesValue
  #define LIGHTINGFX_FUNC_SPEC_PREPARE(x) x * chromaFlairMod(toCamera, normalW, toLight.xyz, altFlakesValue, true)
#endif

#include "common/uv2Utils.hlsl"
#include "include_new/base/_include_ps.fx"
#include "common/ambientSpecular.hlsl"
#include "common/carDirt.hlsl"

Texture2D txStickers : register(TX_SLOT_MAT_7);
Texture2D txStickersMaps : register(TX_SLOT_MAT_8);

#ifdef HAS_CHROMA_FLAIR
  Texture2D txGradient : register(TX_SLOT_MAT_9);

  float3 chromaFlairMod(float3 toCamera, float3 normalW, float3 lightDir, float altFlakesValue, bool fixedLevel){
    float2 uv = float2(dot(normalW, -toCamera) * 0.5 + 0.5, dot(normalW, lightDir) * 0.5 + 0.5);
    uv = lerp(uv.x, uv, altFlakesValue);

    float3 ret;
    if (fixedLevel){
      ret = txGradient.SampleLevel(samLinearClamp, uv, 1).rgb;
    } else {
      ret = txGradient.Sample(samLinearClamp, uv).rgb;
    }

    return GAMMA_LINEAR(ret) * lerp(extChromaFlairFlakesMult, 1, altFlakesValue);
  }
#endif

// Please don’t copy this code as a base for your shader
// It’s all very experimental and WIP, and will be moved to utils_ps.fx as soon as it’s sorted and tested

struct LightingParamsCar {
  float3 txMapsValue;
  float3 specMult;
  float4 ambientAoMult;
  float3 shadow;
  float ccAbsorption;
  float ccMask;
  float baseEffectsMask;
  float waterMask;
};

struct LightingOutput {
  float3 ambient;
  float3 diffuse;
  float3 specular;
  float3 ccSpecular;
};

float getSpecValueSun(float k, float power, float exp){
  return pow(k, GAMMA_BLINNPHONG_EXP(max(exp, 1))) * GAMMA_BLINNPHONG_ADJ(power);
  // return pow(k, (max(exp, 1))) * GAMMA_BLINNPHONG_ADJ(power);
}

float getSpecValue(float k, float power, float exp){
  return pow(k, GAMMA_BLINNPHONG_EXP(max(exp, 1))) * GAMMA_BLINNPHONG_ADJ(power);
  // return pow(k, (max(exp, 1))) * GAMMA_BLINNPHONG_ADJ(power);
}

float3 pearlSpecularColorOffset(float3 base){
  float3 result = base;
  result += float3(0.0, 1, 0.4) * saturate(base.r - base.g) * extPearlSpecular;
  result += float3(0.4, 0.0, 0.2) * saturate(base.g - base.r) * extPearlSpecular;
  result += float3(0.1, 0.4, 0.0) * saturate(base.b * 3 - base.r * 3) * extPearlSpecular;
  return lerp(1, result * 2, max(-extColoredSpecular, abs(extColoredSpecular) * saturate(max(base.x, max(base.y, base.z)) * 5)));
}

float3 estimateSpecularColor(float3 base){
  #ifdef SIMPLEST_LIGHTING
    return 0;
  #endif
  return pearlSpecularColorOffset(base) * extSpecColor;
}

LightingOutput calculateLighting(float3 posC, float extraMask, float3 toCamera, float3 normalW, float3 tangentW, float3 bitangentW, LightingParamsCar P){
  LightingOutput result;
  result.ambient = calculateAmbient(posC, normalW, P.ambientAoMult);

  float lightValue = LAMBERT(normalW, -ksLightDirection.xyz);
  P.shadow *= lightValue;

  result.diffuse = ksLightColor.rgb * GAMMA_OR(1, INPUT_DIFFUSE_K) * P.shadow;

  float specBasePower = P.txMapsValue.x * INPUT_SPECULAR_K;
  float specBaseExp = P.txMapsValue.y * INPUT_SPECULAR_EXP + 1;
  float specSunPower = (P.txMapsValue.z * P.txMapsValue.y) * sunSpecular * extSunSpecularMult;
  float specSunExp = P.txMapsValue.y * sunSpecularEXP + 1;
  float specK = saturate(dot(normalize(-toCamera - ksLightDirection.xyz), normalW));
  float3 specM = P.shadow * extSpecularColor;

  result.specular = 0;
  #ifndef SIMPLIFED_SPECULARS
    result.specular += max(0, getSpecValue(specK, specBasePower, specBaseExp) * P.specMult * specM);
  #endif
  ALTER_LIGHTING
  
  result.specular += getAmbientSpecular(toCamera, normalW, stAmbientSpec, stAmbientEXP) * lerp(1, P.specMult, P.ccAbsorption) * P.baseEffectsMask;
  result.specular += getSpecValueSun(specK, specSunPower, specSunExp) * (extClearCoatSpecular ? (extClearCoatSpecular == 2 ? extClearCoatTint : 1) : P.specMult) * specM * P.ccMask;
  result.specular *= 1 - P.waterMask;
  return result;
}

#include "pbr/clear_coat.hlsl"
// Texture2D txLocalNormal : register(t_23);

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  // damage-related
  float4 txDamageMaskValue = txDamageMask.Sample(samLinear, pin.Tex);
  float4 txDamageValue = txDamage.Sample(samLinear, pin.Tex);
  float normalMultiplier = dot(txDamageMaskValue, damageZones);
  float damageInPoint = saturate(normalMultiplier * txDamageValue.a);

  // float3 normalW2 = normalize(mul(txLocalNormal.SampleLevel(samLinear, pin.Tex, 3.5).xyz * 2 - 1, (float3x3)ksWorld));
  // normalW = normalize(lerp(normalW, normalW2, saturate(dot(normalW, normalW2) * 4 - 3)));

  // usual stuff
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);

  float baseOcclusion = lerp(1, max(max(txDiffuseValue.x, max(txDiffuseValue.y, txDiffuseValue.z)), 0.05), extNormalizeAO);
  txDiffuseValue.xyz /= baseOcclusion;
  baseOcclusion = GAMMA_LINEAR(baseOcclusion);

  // normals piece
  float normalAlpha;
  normalW = getDamageNormalW(pin.Tex, normalW, tangentW, bitangentW, (extCarPaintFlags_uint & 4) ? 1 : normalMultiplier, normalAlpha);
  ALTER_NORMAL

  float flakesValue = considerDetails(pin.Tex, txDiffuseValue, true);

  if (extStickersMode_uint & 64) {
    flakesValue *= normalAlpha;
    normalAlpha = 1;
  }

  txMapsValue.r *= lerp(flakesValue, 1, extFlakesK);

  [branch]
  if (extStickersMode_uint & 63){
    float4 txStickerValue = txStickers.Sample(samLinear, float2(pin.Tex2X, pin.Tex2Y));
    txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, txStickerValue.rgb, txStickerValue.a);
    if (extStickersMode_uint & 2) {
      txDiffuseValue.a = lerp(txDiffuseValue.a, 1, txStickerValue.a);
    }
    if (extStickersMode_uint & 4) {
      float4 txStickerMapsValue = txStickersMaps.Sample(samLinear, float2(pin.Tex2X, pin.Tex2Y));
      txMapsValue.rgb = lerp(txMapsValue.rgb, txStickerMapsValue.rgb, txStickerMapsValue.a);
    }
  }

  RAINFX_INIT;

  #ifdef HAS_CHAMELEON_MASK
    float chameleonMask = (extChameleonMask ? (extChameleonMask == 2 ? txDiffuseValue.a : 1 - txDiffuseValue.a) : 1);
  #endif

  #ifdef HAS_CHROMA_FLAIR
    #ifdef SIMPLEST_LIGHTING
      float altFlakesValue = 0;
    #else
      float4 _dv0 = 0;
      float chromaFlairMask = (extChromaFlairMask ? (extChromaFlairMask == 2 ? txDiffuseValue.a : 1 - txDiffuseValue.a) : 1);
      float altFlakesValue = chromaFlairMask * considerDetails(-pin.Tex, _dv0, true);
    #endif
  #endif

  // chameleon effect
  #ifndef SIMPLEST_LIGHTING
    ALTER_DIFFUSE_COLOR;
  #endif

  // new damaged txDiffuseValue
  txDiffuseValue.xyz = damageInPoint > 0 
      ? lerp(txDiffuseValue.xyz, txDamageValue.xyz, damageInPoint) 
      : txDiffuseValue.xyz;

  // txMapsValue = 0;
  // txDiffuseValue.rgb = float3(0, 0.2, 0.2);

  // dirt
  float4 txDustValue = txDust.Sample(samLinear, pin.Tex);
  COMPUTE_DIRT_LEVEL;
  txDiffuseValue.xyz = lerp(txDiffuseValue.xyz, txDustValue.xyz, dirtLevel);

  // both damages and dirt affecting txMaps
  float damageInverted = 1 - damageInPoint;
  float mapsMultiplier = saturate(1 - dirtLevel * 10);
  float damageFactor = damageInPoint * (normalAlpha - 1) + 1;
  mapsMultiplier *= damageInverted;
  mapsMultiplier *= damageFactor;
  txMapsValue.x *= mapsMultiplier;
  float damageEffectsMult = 1 - dirtLevel;

  float baseEffectsMask = (extEffectsMask ? (extEffectsMask == 2 ? txDiffuseValue.a : 1 - txDiffuseValue.a) : 1) * damageEffectsMult;
  float clearCoarMask = (extClearCoarMask ? (extClearCoarMask == 2 ? txDiffuseValue.a : 1 - txDiffuseValue.a) : 1) * damageEffectsMult;
  #ifdef HAS_RAINBOW_MASK
    float extraMask = (extRainbowMask ? (extRainbowMask == 2 ? txDiffuseValue.a : 1 - txDiffuseValue.a) : 1) * damageEffectsMult;
  #else
    float extraMask = 1;
  #endif

  // clear coat
  ClearCoatParams ccP;
  ccP.intensity = extClearCoatIntensity * clearCoarMask * (MATTE_FIX ? txMapsValue.z : 1);
  ccP.thickness = extClearCoatThickness;
  ccP.ior = extClearCoatIOR;
  ClearCoat cc = CalculateClearCoat(toCamera, normalW, ccP);

  if (extDiffuseSmoothnessTo){
    float m = max(txDiffuseValue.r, max(txDiffuseValue.g, txDiffuseValue.b));
    float k = saturate(remap(m, extDiffuseSmoothnessFrom, extDiffuseSmoothnessTo, 0, 1));
    txMapsValue.x *= saturate(lerp(k, 1, 0.2));
    txMapsValue.y *= saturate(lerp(k, 1, 0.07));
    txMapsValue.z *= saturate(lerp(k, 1, 0.8));
  }

  float3 specColor = estimateSpecularColor(txDiffuseValue.rgb);
  #if !defined(SIMPLEST_LIGHTING) && defined(HAS_CHROMA_FLAIR)
    specColor = chromaFlairMod(toCamera, normalW, -ksLightDirection.xyz, altFlakesValue);
  #endif
  // specColor = 1;
  specColor = GAMMA_LINEAR(specColor);

  // lighting
  LightingParamsCar L;
  txMapsValue.y *= lerp(1, 0.8 + flakesValue * 0.8, extFlakesK * baseEffectsMask);
  L.txMapsValue = txMapsValue.xyz;
  L.ambientAoMult = extraShadow.y * AO_LIGHTING_OCC(baseOcclusion);
  L.specMult = lerp(1, specColor, baseEffectsMask);
  L.shadow = shadow;
  L.ccAbsorption = cc.absorption;
  L.ccMask = clearCoarMask;
  L.baseEffectsMask = baseEffectsMask;
  L.waterMask = 0;
  APPLY_CAO;
  #ifdef ALLOW_RAINFX
    L.waterMask = RP.alpha;
  #endif
  LightingOutput lightingOutput = calculateLighting(pin.PosC, extraMask, toCamera, normalW, tangentW, bitangentW, L);
  
  float3 lighting = applyGamma(lightingOutput.diffuse, lightingOutput.ambient, txDiffuseValue.rgb, INPUT_DIFFUSE_K, INPUT_AMBIENT_K);
  float emissiveMult = 1;
  lighting += calculateEmissive(pin.PosC, txDiffuseValue.rgb, emissiveMult); // GAMMA:TODO
  lighting = cc.ProcessDiffuse(lighting, fresnelMaxLevel);
  lighting += (lightingOutput.ambient + lightingOutput.diffuse) * extClearCoatTint * (1 - cc.absorption);

  LIGHTINGFX(lighting);
  #ifdef SIMPLEST_LIGHTING
    lighting = SIMPLEST_LIGHTING_FN(txDiffuseValue.rgb);
  #elif defined(SIMPLER_LIGHTING)
    lighting = saturate(0.6 + 0.4 * normalW.y) * INPUT_AMBIENT_K * L.ambientAoMult.xyz * txDiffuseValue.rgb
    + calculateEmissive(pin.PosC, txDiffuseValue.rgb, emissiveMult);
  #endif

  // reflections
  float extraMultiplier = isAdditive ? mapsMultiplier * EXTRA_SHADOW_REFLECTION : EXTRA_SHADOW_REFLECTION;
  ReflParams R;
  R.fresnelMaxLevel = fresnelMaxLevel * extraMultiplier;
  R.fresnelC = fresnelC * extraMultiplier;
  R.fresnelEXP = fresnelEXP;
  R.ksSpecularEXP = txMapsValue.y * (HAS_FLAG(FLAG_MATERIAL_0) ? sunSpecularEXP : ksSpecularEXP);
  R.finalMult = txMapsValue.z;
  R.coloredReflections = 1;
  R.coloredReflectionsColor = lerp(1, L.specMult, extColoredReflections);
  R.metallicFix = R.finalMult;
  R.useStereoApproach = false;
  R.globalOcclusionMult = 1;
  R.useGlobalOcclusionMult = false;
  R.stereoExtraMult = 0;
  R.reflectionSampleParam = 0;
  R.forceReflectedDir = false;
  R.forcedReflectedDir = 0;

  R.secondaryLevel = lerp(4, 3, flakesValue);
  R.secondaryIntensity = lerp(0.5, 1, flakesValue) * lerp(1, specColor, baseEffectsMask * 0.7) * extSecondaryReflectionLayer;

  #ifdef RAINBOW_REFL_COLOR
    float rainbowIn = saturate(-dot(toCamera, normalW) * abs(extRainbowChromeK));
    float3 rainbowColor = rainbow(extRainbowChromeK < 0 ? 1 - rainbowIn : rainbowIn);
    if (extRainbowChromeMult < 0) rainbowColor = 1 - rainbowColor;
    rainbowColor = lerp(1, rainbowColor, saturate(dot(rainbowColor, 1)));
    rainbowColor = lerp(1, rainbowColor, saturate(rainbowIn * 2) * abs(extRainbowChromeMult) * extraMask);
    R.coloredReflectionsColor = lerp(rainbowColor, txDiffuseValue.rgb, extColoredReflections);
    R.coloredReflectionsColor = GAMMA_LINEAR(R.coloredReflectionsColor);
  #endif

  #ifdef HAS_CHROMA_FLAIR
    R.secondaryIntensity = chromaFlairMod(toCamera, normalW, normalize(normalW + reflect(toCamera, normalW)), altFlakesValue) * extSecondaryReflectionLayer;
  #endif
  
  R.useBias = true;
  R.isCarPaint = true;
  R.useSkyColor = false;
  R.useMatteFix = MATTE_FIX;

  // lighting.xy = normalW.y;
  
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE_SKIP(R);
  float3 withReflection = calculateReflection(float4(lighting, 1), toCamera, pin.PosC, normalW, R).rgb;
  #ifndef SIMPLEST_LIGHTING
    withReflection += lightingOutput.specular;
  #endif

  // withReflection = dot(normalW, -ksLightDirection.xyz);
  txMapsValue.a = saturate(txMapsValue.a);
  // withReflection = chromaFlairMod(toCamera, normalW, normalize(normalW + reflect(toCamera, normalW)));
  // withReflection = float3(pin.RainOcclusion, 1 - pin.RainOcclusion, 0);

  #ifdef SIMPLEST_LIGHTING
    // lighting = txDiffuseValue.rgb * INPUT_AMBIENT_K;
    // txMapsValue.a = 1;
    // withReflection.gb = 0;
    // withReflection.r = 10;
  #endif
  
  RAINFX_WATER(withReflection);
  RETURN(withReflection, txMapsValue.a);
}

#endif