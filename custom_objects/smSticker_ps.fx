#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define CARPAINT_AT
// #define A2C_SHARPENED
// #define GETNORMALW_UV_MULT normalUVMultiplier // Not used, apparently?
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#define GAMMA_TWEAKABLE_ALPHA

#define CB_MATERIAL_EXTRA_3\
  float extUseDiffuseAlpha;\
  float extNoiseMult;\
  float extNoiseScale;\
  float extReflectionScaleX;\
  float extReflectionBaseTexMult;

#include "lightingBounceBack.hlsl"
#include "include_new/base/_include_ps.fx"

Texture2D txReflectionColor : register(TX_SLOT_MAT_4);

float4 sampleNoise(float2 uv, bool fixUv = false){
  if (fixUv){
    float textureResolution = 32;
    uv = uv * textureResolution + 0.5;
    float2 i = floor(uv);
    float2 f = frac(uv);
    uv = i + f * f * (3 - 2 * f);
    uv = (uv - 0.5) / textureResolution;
  }
	return txNoise.SampleLevel(samLinearSimple, uv, 0);
}

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  RAINFX_WET(txDiffuseValue.xyz);

  float alpha, directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha, directedMult);

  if (extUseDiffuseAlpha){
    float a = alpha;
    alpha = txDiffuseValue.a;
    txDiffuseValue.a = a;
  }

  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);

  float2 baseUV = float2(abs(dot(toCamera, normalW)), txMapsValue.w);
  #ifndef MODE_KUNOS
    baseUV.x += sampleNoise(float2(dot(toCamera, tangentW), dot(toCamera, bitangentW)) * extNoiseScale, true).x * extNoiseMult;
  #endif
  baseUV.x *= extReflectionScaleX;
  float4 reflColor = txReflectionColor.Sample(samLinearClampY, baseUV + float2(dot(pin.Tex, extReflectionBaseTexMult), 0));
  reflColor = GAMMA_LINEAR(reflColor);
 
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.txSpecularValue *= reflColor.rgb;
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_DIFFUSEMASK);
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue.xyz);
  R.coloredReflections = 1;
  R.coloredReflectionsColor = reflColor.rgb;
  
  R.useBias = true;
  R.isCarPaint = true;
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);

  RAINFX_WATER(withReflection);
  RETURN(withReflection, alpha);
}
