#define NO_CARPAINT
#define NO_SSAO
#define SUPPORTS_BLUR_SHADOWS
#define ALPHATEST_THRESHOLD 0.5
// #define LIGHTINGFX_GLASS_BACKLIT
// #define LIGHTINGFX_EXTRA_FOCUSED

#define CB_MATERIAL_EXTRA_3\
	float bannerMode;\
	float solidBrightnessAdjustment;\
	float3 extEdgePosL;\
	float extEdgeRefractionBias;\
	float extEdgeThreshold;\
	float reflectionsIntensity;\
	float alphaGamma;

#include "include_new/base/_flags.fx"
#if defined(ALLOW_EXTRA_VISUAL_EFFECTS)
  #define CUSTOM_STRUCT_FIELDS_EXTRA\
    float3 PosL : TEXCOORD20;\
    float3 NormalL : TEXCOORD21;
#endif

#ifdef NO_WINDSCREENFX
	#undef BLUR_SHADOWS
#endif

#include "common/rainWindscreen.hlsl"
#include "include_new/base/_include_ps.fx"

#ifdef BLUR_SHADOWS
	#include "include_new/ext_windscreenfx/_include_ps.fx"
	#define REL_POS_CAMERA (pin.PosC + (extAltCameraPosition - ksCameraPosition.xyz) * (pin.PosH.x < extAltCameraThreshold ? 1 : 0))
#else
	#define REL_POS_CAMERA pin.PosC
#endif

#include "common/rainWindscreen_impl.hlsl"
#include "include_new/ext_functions/depth_map.fx"

#define WINDSCREEN_SHADOW_REQUIRED
#include "common/windscreenBase.hlsl"
#include "include/bicubic.hlsl"

RESULT_TYPE main(PS_IN_PerPixelCustom pin) {
	READ_VECTORS

	float shadowMult = 1;
	float dirtMult = 1;
	#if defined(APPLY_RAIN) && !defined(MODE_GBUFFER) && defined(ALLOW_EXTRA_VISUAL_EFFECTS)
		WindscreenRainData WR = getWindscreenRainData(pin.TexAlt, pin.NormalCar, pin.NormalW, toCamera, gCameraUp);
		shadowMult = 1 - saturate(WR.water * 2 - 1) * 0.7;
		dirtMult = WR.dirtMult;
	#endif

	float3 resultColor;
	float resultAlpha;
	float4 txDiffuseValue;
	float shadow;
	doWindscreenBase(pin, normalW, toCamera, resultColor, resultAlpha, txDiffuseValue, shadow, shadowMult, dirtMult);

	bool earlyExit;
	#if defined(MODE_GBUFFER) || defined(MODE_KUNOS)
		earlyExit = true;
	#elif defined(MODE_COLORMASK)
		earlyExit = false;
	#else
		earlyExit = extMotionStencil == 17;
		[branch]
	#endif
	if (earlyExit){
		#if defined(ALLOW_EXTRA_VISUAL_EFFECTS)
			float depthZ = 10;
			depthZ = min(depthZ, getDepthAccurate(pin.PosH + float4(+1, -1, 0, 0)));
			depthZ = min(depthZ, getDepthAccurate(pin.PosH + float4(+1, +1, 0, 0)));
			depthZ = min(depthZ, getDepthAccurate(pin.PosH + float4(-1, +1, 0, 0)));
			depthZ = min(depthZ, getDepthAccurate(pin.PosH + float4(-1, -1, 0, 0)));
			
			float depthC = linearizeAccurate(pin.PosH.z);
			clip(depthC - depthZ);
		#else
			clip(resultAlpha - 0.9);
		#endif
		RETURN_BASE(resultColor, resultAlpha);
	}

	float edgeK = 0;
	#if defined(ALLOW_EXTRA_VISUAL_EFFECTS)
		[branch]
		if (extEdgeRefractionBias){
			float edgeBase = dot(normalize(extEdgePosL - pin.PosL), normalize(pin.NormalL));
			edgeK = saturate(remap(edgeBase, extEdgeThreshold - 0.01, extEdgeThreshold, 0, 1));
		}
	#endif

	float extraBackgroundBlur = max(0, bannerMode - 1);
	#if defined(APPLY_RAIN) && !defined(MODE_GBUFFER) && defined(ALLOW_EXTRA_VISUAL_EFFECTS)
		// WR.applyOpaqueMask(resultAlpha);
    float fresnel = saturate(pow(1 - WR.facingShare, 3)) * 0.5;
    float3 refraction = normalize(lerp(toCamera, -WR.normal, 0.5 * (1 - WR.facingShare)));
    float3 reflection = reflect(toCamera, WR.normal);
    float3 reflectionColor = sampleEnv(lerp(reflection, WR.normal, 0.7), 2, 0);
    float3 refractionColor = sampleEnv(refraction, 0, 0) * 0.7;
    float3 dirtColor = sampleEnv(toCamera, 1, 0);

		#ifdef ALLOW_EXTRA_FEATURES
			if (WINDSCREEN_FX_FLAG_USE_COLOR){
				float2 ssUV = pin.PosH.xy * extScreenSize.zw;
				refractionColor = lerp(refractionColor, 
					txPrevFrame.SampleBias(samLinearClamp, ssUV + WR.fakeRefraction * lerp(0.25, 0.5, perObjAmbientMultiplier * extSceneRain), -2 + WR.refractionBlur + extraBackgroundBlur).rgb, 
					saturate(remap(WR.facingShare, 0.3, 0.4, 0, 1)));
				// dirtColor = txPrevFrame.SampleLevel(samLinearClamp, ssUV, 2 + WR.dirtBlur + extraBackgroundBlur).rgb;
				dirtColor = sampleBicubicFixed(txPrevFrame, ssUV, 4).rgb;

				// refractionColor = txPrevFrame.SampleBias(samLinearClamp, ssUV, 2).rgb;
				// refractionColor = float3(WR.trace, 1 - WR.trace, 0) * 0.01;
				// RETURN_BASE(refractionColor, 1);
			}
		#endif

		bool useRefraction = true;
		#if defined(NO_REFLECTIONS) || defined(NO_REFLECTIONS_FOR_CUBEMAP)
			fresnel = lerp(0.2, 1, saturate(1 + dot(toCamera, WR.normal)));
			reflectionColor = getAmbientBaseAt(pin.PosC, reflection, 0.35) * saturate(reflection.y * 10);
			dirtColor = getAmbientBaseAt(pin.PosC, toCamera, 0.2) * (0.5 + 0.5 * saturate(toCamera.y * 10));
			useRefraction = false;
		#endif

		// dirtColor *= float3(1, 0, 1);
		// refractionColor *= WR.debugColor;
		// refractionColor *= float3(1, 0, 1);
		// refractionColor *= txDiffuseValue.xyz;

		refractionColor *= WR.refractionOcclusion;
		reflectionColor *= WR.occlusion;
		dirtColor *= WR.occlusion;

		fresnel += lerp(0.2, 0, WR.wiperOcclusion);
		reflectionColor *= lerp(0.5, 1, WR.wiperOcclusion);
		float alphaMult = max(fresnel, WR.wiperOcclusion);
		if (!WINDSCREEN_FX_FLAG_USE_COLOR){
			alphaMult *= saturate(remap(WR.facingShare, 0.8, 1, 1, 0));
		}

		float3 waterColor = useRefraction ? lerp(refractionColor, reflectionColor, fresnel) : reflectionColor;
		float waterAlpha = saturate(WR.water * 2 - 1) * alphaMult;
		if (!useRefraction){
			waterAlpha *= fresnel;
		}

		float3 rainNormal = WR.normal;
		#ifdef ALLOW_RAINFX
			RainParams RP = (RainParams)0;
		#endif
		normalW = -WR.normal;
		LightingParams L = getLightingParams(pin.PosC, toCamera, -WR.normal, 0, shadow, 1);
		L.txDiffuseValue = 0;
		L.txSpecularValue = saturate(WR.water * 2);
		L.txEmissiveValue = 0;
		L.specularExp = 20;
		L.specularValue = 400;
		float3 lighting = L.calculate();

		L.specularExp = 8;
		L.specularValue = 10;
  	LIGHTINGFX(lighting);

		waterColor += lighting * WR.occlusion;
		mixLayer(waterColor, waterAlpha, dirtColor, WR.trace);
		mixLayer(waterColor, waterAlpha, resultColor, resultAlpha);

		float3 colorMask = GAMMA_LINEAR(max(txDiffuseValue.rgb, 0.4));
		colorMask = colorMask / max(colorMask.r, max(colorMask.g, colorMask.b));

		resultColor = waterColor * lerp(colorMask, 1, !bannerMode && txDiffuseValue.a < 0.5 ? 1 : edgeK);
		resultAlpha = waterAlpha;
		mixLayer(resultColor, resultAlpha, dirtColor, WR.condensation);

		if (WR.debugMode) {
			resultColor = WR.debugColor;
			resultAlpha = 0.8;
		}
		
		if (resultColor.g > -1) {
			// resultColor = dirtColor;
			// resultAlpha = 1;
		}

		// resultColor = txWiperShape.Sample(samLinear, pin.TexAlt).w;
		// resultAlpha = 0.5;

		// resultColor = float3(water, 1 - water, 0);
		// resultColor = float3(trail, 1 - trail, 0);
		// resultAlpha = 0.8;
		// resultColor = refractionColor.rgb;
		// resultAlpha = 1;
	#endif
	
  #ifdef NO_LIGHTING
    resultColor = 0.0;
  #endif  

	#if defined(ALLOW_EXTRA_VISUAL_EFFECTS)
		resultAlpha = GAMMA_ALPHA(resultAlpha);

		[branch]
		if (edgeK > 0 || bannerMode > 1){
			float2 ssUV = pin.PosH.xy * extScreenSize.zw;
			float4 refracted = txPrevFrame.SampleLevel(samLinearClamp, ssUV, extEdgeRefractionBias + extraBackgroundBlur);
			if (GAMMA_FIX_ACTIVE) refracted.rgb *= lerp(1, GAMMA_LINEAR(txDiffuseValue.rgb), txDiffuseValue.a);
			mixLayerBelow(resultColor, resultAlpha, refracted.rgb, edgeK * refracted.a);

			if (bannerMode > 1){
				resultColor = lerp(refracted.rgb, resultColor, resultAlpha);
				resultAlpha = 1;
			}

			if (extEdgeRefractionBias < 0){
				resultColor = float3(1 - edgeK, edgeK, 0) * 3;
				resultAlpha = 1;
			}
		} else {

		}
	#endif

  #ifdef MODE_COLORMASK
    resultColor = txDiffuseValue.rgb;
		resultAlpha = txDiffuseValue.a;
  #endif
	
  RETURN_BASE(resultColor, resultAlpha);
}
