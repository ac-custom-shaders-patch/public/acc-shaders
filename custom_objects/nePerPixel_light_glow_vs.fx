// Redefining ksEmissive in a different cbuffer, so it would be 
// available in vertex shader without extra hacks
#define REDEFINE_KSEMISSIVE
#define INPUT_EMISSIVE3 0
#define FOG_VS_COMPUTE_SECOND

#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"
#include "common/distantGlow.hlsl"

#define PixelScale max(extScreenSize.z, extScreenSize.w)

PS_IN_PerPixelExtra1 main(VS_IN vin SPS_VS_ARG) {
  // float4 posW = mul(vin.PosL, ksWorld);
  vin.PosL.xyz += extSceneOffset;
  float3 posC = vin.PosL.xyz - ksCameraPosition.xyz;
  float distance = length(posC);
  float3 look = normalize(posC);
  float3 side = normalize(cross(float3(0, 1, 0), posC));
  float3 up = -normalize(cross(side, posC));

  float fogRadiusK = calculateFogNew(posC * 2);
  // fogRadiusK = pow(saturate((fogRadiusK - 0.4) / 0.6), 0.6);
  float distanceFarGrowK = clamp(distance / 800 - 0.8, 0, 3);
  float distanceFarFadeK = saturate(1.2 - distance / 800);
  distanceFarFadeK = lerp(distanceFarFadeK, distanceFarFadeK * 0.75 + 0.25, saturate(-posC.y / 20 - 0.5));
  float fillRateFix = saturate(distance / 67 - 0.2);

  float3 fogPosC = posC * 0.4;
  float fogK = calculateFogImpl(0, 0, fogPosC);

  float normalConcentration = pow(saturate(length(vin.NormalL.xyz)), 2);
  vin.NormalL.xyz = normalize(vin.NormalL.xyz);

  float2 angle = vsLoadAltTex(vin.TangentPacked);
  float heightMult = lerp(extDistantHeightMult, 1, abs(look.y));
  float halfSize = fogRadiusK * (1 + distanceFarGrowK) * extDistantRadiusMult * fillRateFix;
  vin.PosL.xyz += vin.NormalL.xyz * float3(1, 0.25, 1) * normalConcentration * halfSize / 4;
  vin.PosL.xyz += (up * angle.y * heightMult + side * angle.x) * halfSize;
  vin.PosL.xyz -= look * (2 + distance / 50 + fogK * 20);

  GENERIC_PIECE(PS_IN_PerPixelExtra1);

  #if !defined( ALLOW_VERTEX_ADJUSTMENTS ) || defined( NO_DEPTH_MAP )
    DISCARD_VERTEX(PS_IN_PerPixelExtra1);
  #endif

  float baseFog = 0;
  OPT_STRUCT_FIELD_FOG(baseFog = vout.Fog); 
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFogImpl(posV, posW, fogPosC));

  float NdotV = dot(-look, vout.NormalW);
  vout.NormalW.xy = angle;
  vout.NormalW.z = 1 / (extDistantSoftK + distance / 30 + fogK * 20);
  vout.Extra.x = extDistantMult * fogRadiusK * saturate(distance / 100 - 0.5) * distanceFarFadeK * extGlowBrightness;
  // vout.Extra.y = 0;
  // OPT_STRUCT_FIELD_FOG(vout.Extra.y = 2 * vout.Fog);

  if (vout.Extra.x <= 0.001 || dot(ksEmissive, 1) < 1){
    DISCARD_VERTEX(PS_IN_PerPixelExtra1);
  }

  vout.Extra.x = GAMMA_KSEMISSIVE(vout.Extra.x);
  if (GAMMA_FIX_ACTIVE) {
    OPT_STRUCT_FIELD_FOG(vout.Extra.x *= 1 - pow(vout.Fog, 4));
  }
  SPS_RET(vout);
}
