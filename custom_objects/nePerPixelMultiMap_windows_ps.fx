#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple

// #define NO_INTERIOR_LIGHTING

#define SUPPORTS_AO
#ifndef NO_INTERIOR_LIGHTING
  #define INPUT_EMISSIVE3 0
#endif
#include "include_new/base/_include_ps.fx"

cbuffer cbEmissiveLight : register(b12) {  
  float extTxEmissiveMode;
  float extRoomScale;
  float extRoomOffset;
  float extRoomRotation;

  float2 extRoomCeilingOffset;
  float2 extRoomCeilingScale;

  float extSnapToTexCoords;
  float extDebugMode;
  float extNearEdgeOffset;
  float extFarDistanceInv;

  float extBrightnessBase;
  float extBrightnessVolume;
  float extBrightnessLights;
  float extBrightnessGlow;

  float shapeXS;
  float shapeXES0Neg;
  float shapeXES1Neg;
  float shapePower;

  float shapeYS;
  float shapeYES0Neg;
  float shapeYES1Neg;
  float shapeChance;

  float shapeX1S;
  float shapeX1ES0Neg;
  float shapeX1ES1Neg;
  float shapeX1Mult;

  float shapeY1S;
  float shapeY1ES0Neg;
  float shapeY1ES1Neg;
  float shapeY1Mult;

  float extTexCoordsSnapScale;
  float extEmissiveDiffuseMult;
  float extEmissiveDiffuseEXP;
  float extNormalsFactor;

  float extTexCoordsSnapOffset;
  float extBrightnessAmbientVolume;
  float extNormalNoiseScale;
  float extNormalNoiseAmount;
}

Texture2D txEmissive : register(t4);

float2x2 rotate2d(float _angle){
  float sinX = sin ( _angle );
  float cosX = cos ( _angle );
  float sinY = sin ( _angle );
  return float2x2( cosX, -sinX, sinY, cosX);
}

float closeTo(float v, float w = 0.5, float s = 6){
  float e0 = 0.5 - w / 2 - 1 / s;
  float e1 = 0.5 + w / 2 + 1 / s;
  return saturate((v - e0) * s) * saturate((e1 - v) * s);
}

float closeToP(float v, float s, float es0, float es1){
  return saturate(v * s + es0) * saturate(-v * s + es1);
}

float3 shapeSquares(float2 v){
  return float3( closeTo(v.x, 0.2), closeTo(v.y, 0.2), 1 );
}

float3 shapeCircles(float2 v){
  return float3( closeTo(v.x, 0.0, 1.5), closeTo(v.y, 0.0, 1.5), 3 );
}

float3 shapeOffice(float2 v){
  return float3( closeTo(v.x, 0.1), closeTo(v.y, 0.6), 1 );
}

float3 shapeOfficeDouble(float2 v){
  return float3( closeTo(v.x, 0.14) - closeTo(v.x, 0.02, 100) * 0.05, closeTo(v.y, 0.7), 1 );
}

float3 shapePar(float2 v){
  return float3( 
    closeToP(v.x, shapeXS, shapeXES0Neg, shapeXES1Neg)
      + closeToP(v.x, shapeX1S, shapeX1ES0Neg, shapeX1ES1Neg) * shapeX1Mult, 
    closeToP(v.y, shapeYS, shapeYES0Neg, shapeYES1Neg)
      + closeToP(v.y, shapeY1S, shapeY1ES0Neg, shapeY1ES1Neg) * shapeY1Mult, 
    shapePower);
}

float3 shape(float2 v){
  float2 u = frac(v);
  float2 w = floor(v);
  float3 noise = txNoise.SampleLevel(samPoint, w / 32, 0).xyz;
  return noise.x < shapeChance ? shapePar(u) : 0.01;
}

float distanceContour(float3 shape){
  shape.xy = saturate(shape.xy);
  return pow( 
    saturate(1.0001 - pow( length(float2(1 - shape.x, 1 - shape.y)) , shape.z) ),
    shape.z);
}

float4 sampleNoise(float2 uv){
	float textureResolution = 32;
	uv = uv * textureResolution + 0.5;
	float2 i = floor(uv);
	float2 f = frac(uv);
	uv = i + f * f * (3 - 2 * f);
	uv = (uv - 0.5) / textureResolution;
	return txNoise.SampleLevel(samLinearSimple, uv, 0);
}

float4 sampleNoise(float3 posW, float3 side, float scale){
  float2 uv = float2(dot(side, posW), posW.y) * scale;
  return sampleNoise(uv);
}

float sampleEmissiveNoise(float3 posW, float3 posC, float3 normalW){
  posW += normalize(posC) * 2;
  float4 noise0 = sampleNoise(posW, float3(1, 0, 0), 0.07);
  float4 noise1 = sampleNoise(posW, float3(0, 0, 1), 0.07);
  return lerp(noise0.x, noise1.x, abs(normalW.x));
}

void addNormalNoise(float3 posC, inout float3 normalW){
  float4 noise0 = sampleNoise(ksCameraPosition.xyz + posC, float3(1, 0, 0), extNormalNoiseScale);
  float4 noise1 = sampleNoise(ksCameraPosition.xyz + posC, float3(0, 0, 1), extNormalNoiseScale);
  float4 noise = lerp(noise0.x, noise1.x, abs(normalW.x));
  normalW += (noise.xyz - 0.5) * extNormalNoiseAmount;
}

RESULT_TYPE main(PS_IN_Windows pin) {
  // float emissiveNoise = sampleEmissiveNoise(ksCameraPosition.xyz + pin.PosC, pin.PosC, pin.NormalW);
  addNormalNoise(pin.PosC, pin.NormalW);

  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 txMapsValue = txMaps.Sample(samLinear, pin.Tex).xyz;
  float4 txEmissiveValue = txEmissive.Sample(samLinear, pin.Tex);

  float3 emissiveDiffuse = saturate(txDiffuseValue.xyz * extEmissiveDiffuseMult);
  if (extEmissiveDiffuseEXP){
    emissiveDiffuse = pow(luminance(emissiveDiffuse), extEmissiveDiffuseEXP);
  }

  if (extTxEmissiveMode == 0){
    txEmissiveValue.xyz = emissiveDiffuse;
  } else if (extTxEmissiveMode == 1){
    txEmissiveValue.xyz *= emissiveDiffuse;
  } else if (extTxEmissiveMode == 2){
    txEmissiveValue.xyz = lerp(emissiveDiffuse, txEmissiveValue.xyz, txEmissiveValue.a);
  } else {
    txEmissiveValue.xyz = 1;
  }

  float directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, directedMult);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue);
  // APPLY_VRS_FIX;

  float lightingMult = 0;
  #ifdef NO_INTERIOR_LIGHTING
    lightingMult = 1;
  #endif

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.txEmissiveValue = lightingMult;
  L.applyTxMaps(txMapsValue.xyz);
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  float NdotV = lerp(0.2, saturate(dot(-toCamera, normalW)), extNormalsFactor);
  #ifdef NO_INTERIOR_LIGHTING
    NdotV = 0;
  #endif
  // lighting *= 1 - NdotV;

  float roomScale = extRoomScale;
  float roomOffset = extRoomOffset;
  float roomRotation = extRoomRotation;
  float2 roomCeilingOffset = extRoomCeilingOffset;
  float2 roomCeilingScale = extRoomCeilingScale;

  float3 posW = ksInPositionWorld;
  float3 posAdjW = posW;
  float3 roomOrigin;
  float roomHeight;

  [branch]
  if (extSnapToTexCoords){
    float3 difV = posW - pin.PosV;
    float difVT = dot(difV, tangentW);
    float difVB = dot(difV, bitangentW);
    float2 uvDir = pin.Tex - pin.TexV;
    float2 texR = frac(pin.Tex / extTexCoordsSnapScale + extTexCoordsSnapOffset) * extTexCoordsSnapScale;
    roomOrigin = posW 
      - tangentW * difVT * texR.x / uvDir.x
      - bitangentW * difVB * texR.y / uvDir.y;
    float3 roomBottom = posW 
      + tangentW * difVT * (1 - texR.x) / uvDir.x
      + bitangentW * difVB * (1 - texR.y) / uvDir.y;
    roomHeight = roomBottom.y - roomOrigin.y;
  } else {
    posAdjW.y -= roomOffset;
    roomOrigin = ceil(posAdjW / roomScale) * roomScale;
    roomHeight = roomScale;
  }

  float difY = roomOrigin.y - posAdjW.y;
  float3 roomRay = toCamera * (toCamera.y < 0 ? difY - roomHeight : difY) / toCamera.y;
  float3 roomPoint = posAdjW + roomRay;
  float3 roomOriginAdj = posAdjW;
  roomOriginAdj.y = roomOrigin.y;
  float ceilingDistance = abs(dot(roomOriginAdj - roomPoint, normalize(pin.NormalW)));
  roomPoint.xz = mul(rotate2d(roomRotation), roomPoint.xz);
  roomPoint.xz += roomCeilingOffset;
  roomPoint.xz *= roomCeilingScale;

  // float emissiveNoise = sampleEmissiveNoise(ksCameraPosition.xyz + pin.PosC, pin.PosC, pin.NormalW);
  // float emissiveNoise = sampleNoise(roomPoint.xz * 0.1).x;
  // float emissiveNoise = 1;

  float distCheck = saturate(ceilingDistance * 4 - extNearEdgeOffset);
  float lightsShape = distanceContour(shape(roomPoint.xz)) * distCheck;

  lightsShape = saturate(lightsShape);
  lightsShape = extBrightnessLights * saturate((lightsShape > 0.99) + lightsShape * lightsShape * extBrightnessGlow);
  lightsShape *= pow(saturate(2 - length(roomRay) * extFarDistanceInv), 2);
  if (toCamera.y < 0) lightsShape = 0;

  float interiorVolume = saturate(difY / roomScale - pow(abs(toCamera.y), 1));
  float interiorLight = extBrightnessBase + extBrightnessVolume * interiorVolume;
  // float3 interiorCombined = txEmissiveValue.xyz * (interiorLight + lightsShape) * getEmissiveMult() * NdotV;
  float3 interiorCombined = GAMMA_KSEMISSIVE(ksEmissive * txEmissiveValue.xyz * (interiorLight + lightsShape) * NdotV) * getEmissiveMult();

  lighting += applyGamma(lerp(0.2, 1, interiorVolume) * extBrightnessAmbientVolume * NdotV, ksAmbient) * getAmbientBaseAt(pin.PosC, float3(0, 1, 0), 1);
  lighting += interiorCombined;
  if (extDebugMode) {
    lighting.r += difY < 0.15 ? 10 : 0;
    lighting.g += difY > (roomScale - 0.15) ? 10 * extSnapToTexCoords : 0;
    lighting.b += length(posW - roomOrigin) < 0.2 ? 10 : 0;
  }

  ReflParams R = getReflParamsBase(EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue);
  R.useBias = true;
  R.isCarPaint = true;
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  RETURN(withReflection, txDiffuseValue.a);
}
