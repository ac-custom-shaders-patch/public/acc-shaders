#undef OBJECT_SHADER
#define FULLY_CUSTOM_MATERIAL
#define USE_COMPLEX_OCCLUSION
#define USE_GAMMA_REFLECTIONS
#define ACTUAL_PBR

// #define ALLOW_CUSTOM_A2C
// #define A2C_SHARPENED_THRESHOLD 0.3
// #define A2C_SHARPENED
// #define USE_CUSTOM_COVERAGE
// #define extA2COffset 0

#ifndef GAMMA_FIX
  #ifndef GAMMA_PARAM
    #define GAMMA_PARAM GAMMA_OR(1, 2.2)
  #endif
  #define INPUT_EMISSIVE3 pow(max(0, ksEmissive), GAMMA_PARAM)
  #ifndef MODE_GBUFFER
    #define LFX_COLOR_PREPARE(X) pow(max(0, X), GAMMA_PARAM)
  #endif
#endif

#include "include/common.hlsl"

#ifdef USE_UV2
  #include "common/uv2Utils.hlsl"
#endif

#define FLAG_SKIP_DIFFUSE_FOR_EMISSIVE 1
#define FLAG_USE_SPECULAR_WORKFLOW 2
#define FLAG_DXT5NM_NORMAL 4
#define FLAG_MATERIAL_AO 8

#define LIGHTINGFX_KSDIFFUSE 1
#define GAMMA_LIGHTINGFX_KSDIFFUSE_ONE
#define LIGHTINGFX_SPECULAR_COLOR lfxSpecularPower
#define LIGHTINGFX_SPECULAR_EXP lfxSpecularEXP
#define LIGHTINGFX_SUNSPECULAR_EXP 1
#define LIGHTINGFX_SUNSPECULAR_COLOR 0
#define LIGHTINGFX_FIND_MAIN
#define INPUT_NM_OBJECT_SPACE 0
#define IS_ADDITIVE_VAR 0
#define INPUT_AMBIENT_K 1
#define INPUT_DIFFUSE_K 1
#define USE_SPECULAR_WORKFLOW
#define USE_PS_FOG_IN_MAIN

cbuffer cbMaterial : register(b4) {
  float3 ksEmissive;
  float ksAlphaRef;

  float3 pbrAlbedoGain;
  float pbrBlurCubemap;
  float pbrF0;
  PBR_MATERIAL
  PARAM_CSP(uint, pbrFlags_uint);
  PARAM_CSP(uint, extFlags);
  PARAM_CSP(float, extExtraSharpLocalReflections);

  #ifdef USE_PARALLAX
    float extParallaxScale;
    float extParallaxSteps;
    float extParallaxOffset;
    #ifndef PARALLAX_TEX_TO_WORLD
      float extParallaxTexToWorld;
      #define PARALLAX_TEX_TO_WORLD extParallaxTexToWorld
    #endif
    float extDisplacementRangeBase; // min / (max - min)
    float extDisplacementRangeInv; // -1 / (max-min)
    float extDisplacementGrazingFactor;
  #endif

  #ifdef PBR_MATERIAL_0
    PBR_MATERIAL_0
  #endif
}

#include "include_new/base/_include_ps.fx"
#include "common/smoothA2C.hlsl"
#include "pbr/pbr.hlsl"

#ifdef USE_CLEARCOAT
  #include "pbr/clear_coat.hlsl"
#endif

struct PBRInput {
  float3 albedo;
  float3 albedoLinearMult;
  float alpha;

  float2 nmBase;
  float2 nmExtra;
  bool useNmExtra;

  float emissive;
  float localAO;
  float globalAO;
  float roughness;
  float3 f0;

  void setF0(float3 mapValue);
};

PBRInput createPBRInput(){
  PBRInput I;
  I.albedoLinearMult = 1;
  I.useNmExtra = false;
  I.globalAO = 1;
  I.f0 = 0.04;
  I.alpha = 1;
  I.emissive = 1;
  return I;
}

void PBRInput::setF0(float3 mapValue){
  if (pbrFlags_uint & FLAG_USE_SPECULAR_WORKFLOW){
    f0 = pow(saturate(mapValue), 2.2);
  } else {
    float reflectance = lerp(pbrF0, 1, mapValue.x);
    f0 = lerp(reflectance, albedo.rgb, mapValue.x);
    albedoLinearMult *= (1 - reflectance) * (1 - mapValue.x);
  }
}

#ifdef PBRINPUT_PASS
  PBRInput getPBRInput(PS_IN_Nm pin, inout float shadowMult PBRINPUT_DECL);
#else
  #define PBRINPUT_PASS
  PBRInput getPBRInput(PS_IN_Nm pin, inout float shadowMult);
#endif

#ifdef USE_PARALLAX
  #define USE_RELIEF_PARALLAX
  #undef USE_MULTIMAP_TEXTURE_VARIATION

  #define IN_PARALLAX_SCALE extParallaxScale
  #define IN_PARALLAX_STEPS extParallaxSteps
  #define IN_PARALLAX_OFFSET extParallaxOffset
  #include "common/parallaxAltTwoDir.hlsl"

  #ifndef IN_PARALLAX_TANGENT
    #define IN_PARALLAX_TANGENT tangentW
    #define IN_PARALLAX_BITANGENT bitangentW
    #define IN_PARALLAX_NORMAL normalize(pin.NormalW)
  #endif
#endif

RESULT_TYPE main(PS_IN_Nm pin PS_INPUT_EXTRA) {
  READ_VECTORS_NM
  float3 normalWBak = normalW;

  #ifdef EARLY_PREP
    EARLY_PREP
  #endif
  
  float shadowMult = 1;
  #ifdef USE_PARALLAX
    float4 randomTex = txNoise.SampleLevel(samPoint, pin.Tex * 7139.2646, 0);
    float tess = saturate(length(pin.PosC) * extLODDistanceMult * extDisplacementRangeInv + extDisplacementRangeBase);
    float2 texDx = 0, texDy = 0;
    float height = 1;

    [branch]
    if (tess > 0){
      float3 eyeTS = -normalize(float3(dot(toCamera, IN_PARALLAX_TANGENT), dot(toCamera, IN_PARALLAX_BITANGENT), dot(toCamera, IN_PARALLAX_NORMAL)));
      float2 pUV = (PARALLAX_UV) * (PARALLAX_UV_MULT);
      texDx = ddx(pUV);
      texDy = ddy(pUV);
      pUV = parallaxMap(pUV, eyeTS, texDx, texDy, tess, height PARALLAX_EXTRA_ARGS_PASS);

      #ifdef ALLOW_EXTRA_VISUAL_EFFECTS
        #ifndef PARALLAX_ALWAYS_USE_TEX_TO_WORLD
          [branch]
          if (PARALLAX_TEX_TO_WORLD > 0){
        #else
          {
        #endif
          float2 duv = pUV / (PARALLAX_UV_MULT) - (PARALLAX_UV);
          float3 offsetW = 0;
          float size = PARALLAX_TEX_TO_WORLD * saturate(abs(dot(toCamera, normalW)) * extDisplacementGrazingFactor - 1);
          offsetW -= IN_PARALLAX_TANGENT * duv.x * size;
          offsetW -= IN_PARALLAX_BITANGENT * duv.y * size;
          offsetW += IN_PARALLAX_NORMAL * (height - extParallaxOffset) * extParallaxScale * size / (PARALLAX_UV_MULT);
          pin.PosC += offsetW;
          pin.ShadowTex0.xyz += mul(offsetW, (float3x3)ksShadowMatrix0);
        }
      #endif

      PARALLAX_UV = pUV / (PARALLAX_UV_MULT);

      #ifdef USE_RELIEF_PARALLAX
        float3 lightTS = -normalize(float3(dot(ksLightDirection.xyz, IN_PARALLAX_TANGENT), dot(ksLightDirection.xyz, IN_PARALLAX_BITANGENT), dot(ksLightDirection.xyz, IN_PARALLAX_NORMAL)));
        shadowMult = parallaxSoftShadowMultiplier(lightTS, pUV, height, texDx, texDy, tess, randomTex.x PARALLAX_EXTRA_ARGS_PASS);
      #endif
    }
  #endif

  PBRInput I = getPBRInput(pin, shadowMult PBRINPUT_PASS);

  float shadow = shadowMult * getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  #if !defined(NO_NORMALMAPS)
    float3 normalTangent;
    float2 txNormalValue = I.nmBase * 2 - 1;
    if (I.useNmExtra) {
      float2 txNormalAltValue = I.nmExtra * 2 - 1;
      float3 t = float3(txNormalValue, 1 + sqrt(saturate(1 - dot2(txNormalValue))));
      float3 u = float3(-txNormalAltValue, sqrt(saturate(1 - dot2(txNormalAltValue))));
      normalTangent = t * dot(t, u) - u * t.z;
    } else { 
      normalTangent = float3(txNormalValue.xy, sqrt(saturate(1 - dot2(txNormalValue))));
    }
    normalW = normalize(-tangentW * normalTangent.x + bitangentW * normalTangent.y + normalW * normalTangent.z);
  #endif

  if (pbrFlags_uint & FLAG_MATERIAL_AO){
    I.localAO = lerp(1, I.localAO, abs(dot(normalize(pin.NormalW), toCamera)));
    shadow *= saturate(abs(dot(-ksLightDirection.xyz, normalW)) + 2 * pow(I.localAO, 2) - 1);
  }

  float4 txDiffuseValue = float4(I.albedo, 1);
  RAINFX_WET(txDiffuseValue.xyz);
  txDiffuseValue.rgb = saturate(GAMMA_APPLY(txDiffuseValue.rgb * pbrAlbedoGain)) * I.albedoLinearMult;

  // Preparing parameters
  LightingParamsPBR L;
  L.txDiffuseValue = txDiffuseValue.rgb;
  L.emissiveMult = I.emissive;
  L.shadow = shadow;
  // L.shadow = 1;
  L.f0 = I.f0;
  L.localOcclusion = I.localAO;
  L.globalOcclusion = AO_LIGHTING * I.globalAO;
  L.reflectionOcclusion = I.localAO * I.globalAO * EXTRA_SHADOW_REFLECTION * AO_REFLECTION;
  L.roughness = I.roughness;
  L.skipDiffuseForEmissive = pbrFlags_uint & FLAG_SKIP_DIFFUSE_FOR_EMISSIVE;
  APPLY_CAO_CUSTOM(L.shadow, L.globalOcclusion.xyz);

  float reflBlur = sqrt(L.roughness);
  L.reflDir = normalize(reflect(toCamera, normalW));
  L.reflDir = normalize(lerp(L.reflDir, normalW, L.roughness));
  L.reflDir = fixReflDir(L.reflDir, pin.PosC, normalW, reflBlur);

  // Lighting computing
  RAINFX_SHINY_PBR(L);
  LightingOutput lightingOutput = calculateLighting(pin.PosC, toCamera, normalW, tangentW, bitangentW, L);
  float3 reflColor = GAMMA_APPLY(getReflectionAt(L.reflDir, -toCamera, lerp(pbrBlurCubemap, 6, reflBlur), true, 0) * GET_EXTRA_SHADOW_OCCLUSION(L.reflDir));

  // reflColor *= GAMMA_OR(1, 0.4);  
  // reflColor *= float3(1, 0, 0);  

  // reflColor = float3(10, 0, 0);
  // lightingOutput.F = 0.3;

  // float3 reflFinal = pow(saturate(lightingOutput.F), GAMMA_OR(1, 2)) * reflColor;
  float3 reflFinal = lightingOutput.F * reflColor * GAMMA_OR(1, 1);
  float3 lighting = lightingOutput.diffuse + lightingOutput.specular;

  // lighting = 1 + lightingOutput.diffuse;

  // reflFinal = float3(2, 0, 0);

  // Dynamic lights
  float3 lightingBasic = lighting;
  L.txDiffuseValue *= saturate(L.localOcclusion * 2) * BRIGHTNESS_FIX_DIFFUSE;
  float lfxSpecularBase = pow(1 - L.roughness, 4);
  float lfxSpecularPower = lfxSpecularBase * sqrt(dot(L.f0, 0.1));
  float lfxSpecularEXP = 1 + lfxSpecularBase * 500;
  #ifdef ALLOW_EXTRA_VISUAL_EFFECTS
    LFX_MainLight mainLight;
    LIGHTINGFX(lighting);

    #ifdef USE_PARALLAX    
      [branch]
      if (tess > 0 && dot(mainLight.dir, 1)) {
        float2 pUV = (PARALLAX_UV) * (PARALLAX_UV_MULT);
        float3 lightTS = normalize(float3(dot(mainLight.dir, IN_PARALLAX_TANGENT), dot(mainLight.dir, IN_PARALLAX_BITANGENT), dot(mainLight.dir, IN_PARALLAX_NORMAL)));  
        float lightingFocus = saturate(mainLight.power / max(dot(lighting - lightingBasic, 1), 0.001) * 2 - 1);
        float3 dynShadow = parallaxSoftShadowMultiplier(lightTS, pUV, height, texDx, texDy, tess, randomTex.x PARALLAX_EXTRA_ARGS_PASS);
        lighting = lerp(lightingBasic, lighting, lerp(1, dynShadow, lightingFocus * tess));
      }
    #endif
  #endif

  // Summing things up
  float3 withReflection = lighting + reflFinal;

  // Fake reflection parameters for G-buffer version of a shader
  ReflParams R = (ReflParams)0;
  R.resultBlur = clamp(reflBlur * reflBlur * 7 - 1, 0, 6);
  R.resultPower = 1;
  R.coloredReflectionsColor = saturate(lightingOutput.F);
  R.coloredReflections = 1;
  R.resultColor = reflFinal;

  // withReflection = lightingOutput.diffuse + lightingOutput.specular;
  withReflection = GAMMA_REVERT(withReflection);
  // withReflection.rgb *= 0.2;
  // withReflection = lightingOutput.F;
  // withReflection = reflFinal;

  // lightingOutput.diffuse = txDiffuseValue.rgb;
  // withReflection = GAMMA_REVERT(lightingOutput.diffuse);

  // withReflection = L.localOcclusion;
  // withReflection = sqrt(L.f0);
  // withReflection = sqrt(txCombinedValue.z);
  // withReflection = sqrt(pow(max(0, txEmAoMeRo.Sample(samLinear, pin.Tex).xyz.z), 2.2));
  // withReflection = sqrt(pow(max(0, txSpecular.Sample(samLinear, pin.Tex).xyz.z), 2.2));
  // withReflection = normalW;
  // withReflection = shadow * normalW.y;

  #ifdef SIMPLEST_LIGHTING
    withReflection = I.albedo * GAMMA_EXPECTED_KS / 2;
  #endif

  RAINFX_WATER(withReflection);
  // A2C_ALPHA(txDiffuseValue.a);
  RETURN(withReflection, I.alpha);
}

float tiltBias(PS_IN_Nm pin){
  float3 toCamera = normalize(pin.PosC);
  return TILT_BIAS;
}