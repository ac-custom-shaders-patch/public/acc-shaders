// #define SUPPORTS_COLORFUL_AO
// #define INCLUDE_FLAGS_CB
#define WIND_COORDS_MULT 0.1
#include "include_new/base/_include_vs.fx"
#include "include_new/ext_functions/wind.fx"

#define PixelScale (max(extScreenSize.z, extScreenSize.w) / 4)

void calculateWind(inout float4 posL, float maxWindOffset, bool prev){
  float2 baseWind = prev ? windOffsetPrev(posL.xyz, 1, 0.8) : windOffset(posL.xyz, 1, 0.8);
  float2 wind = saturate(baseWind / 2) * maxWindOffset;
  posL.xz += wind;
  // posL.y += maxWindOffset - sqrt(maxWindOffset * maxWindOffset - dot(wind, wind));
}

PS_IN_PerPixel main(VS_IN vin SPS_VS_ARG) {
  #ifdef MODE_KUNOS
    DISCARD_VERTEX(PS_IN_PerPixel);
  #else
    float fade = 1;
    if (dot(vin.NormalL, vin.NormalL) < 0.9){
      float pixelRadius = mul(mul(vin.PosL, ksView), ksProjection).w * PixelScale;
      float radius = max(length(vin.NormalL), pixelRadius);
      fade = length(vin.NormalL) / radius;
      vin.PosL.xyz = vin.PosL.xyz - vin.NormalL + normalize(vin.NormalL) * radius;
    }

    float maxWindOffset = min(sqrt(abs(vin.TangentPacked.x) / 2), 2);
    calculateWind(vin.PosL, maxWindOffset, false);

    GENERIC_PIECE(PS_IN_PerPixel);

    // Wind doesn’t go to motion buffer to keep things a bit faster.
    // It’s not really noticeable.
    GENERIC_PIECE_MOTION(vin.PosL);

    #ifdef ALLOW_PERVERTEX_AO
      vout.Ao = fade;
    #endif
    #ifndef NO_SHADOWS
      vout.ShadowTex0.z -= 0.01;
    #endif
    SPS_RET(vout);
  #endif
}
