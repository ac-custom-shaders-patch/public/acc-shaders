#ifndef _REFRACTION_HLSL_
#define _REFRACTION_HLSL_
#ifdef ALLOW_EXTRA_FEATURES

float2 calculateRefractionOffset(float3 posC, float3 toCamera, float3 normalW, float intensity = 0.07){
  float2 dir = float2(dot(normalW, normalize(cross(extDirUp, toCamera))), dot(normalW, extDirUp));
  // float facingShare = sqrt(saturate(1 - dot2(pin.Tex)));
  // dir = float2(0, 1);
  return dir * intensity
    // * saturate(1 - abs(dot(normalW, toCamera))) 
    * lerp(1, 0.5, saturate(abs(dot(toCamera, normalW))))
    * float2(extScreenSize.y / extScreenSize.x, 1) 
    * extCameraTangent / length(posC)
    ;
}

float2 calculateRefractionUV(float4 posH, float3 posC, float3 toCamera, float3 normalW, float intensity = 0.07){
  return posH.xy * extScreenSize.zw + calculateRefractionOffset(posC, toCamera, normalW, intensity);
}

float4 calculateRefractionColor(float2 uv, float bias = 0){
  float level = txPrevFrame.CalculateLevelOfDetail(samLinearClamp, uv);
  return txPrevFrame.SampleLevel(samLinearClamp, uv, bias + level / 2);
}

float4 calculateRefraction(float4 posH, float3 posC, float3 toCamera, float3 normalW, float intensity = 0.07, float blurK = 0){
  float2 uv = calculateRefractionUV(posH, posC, toCamera, normalW, intensity);
  return calculateRefractionColor(uv, blurK * 2);
}

#endif
#endif
