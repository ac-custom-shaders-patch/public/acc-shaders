#include "include/common.hlsl"
#include "include/rain.hlsl"
#include "include_new/base/_flags.fx"
#include "rainFX_consts.hlsl"

#ifdef RAINFX_FORCE_ROUGH
  #define RAINFX_IS_ROUGH 1
#else
  #define RAINFX_IS_ROUGH HAS_FLAG(FLAG_RAINFX_ROUGH)
#endif

#define PUDDLES_MAP_SIDE 7
#define PUDDLES_MAX_OFFSET ((PUDDLES_MAP_SIDE - 1) / 2)

cbuffer _cbRainParams : register(b5) {
  float4x4 extTransform;
  float4x4 extMaskTransformG;
  float4x4 extCarWetnessTransformC;

  float2 extRotate0;
  float2 extRotate1;

  float extRainMult;
  float extStreamMult;
  float extRainHeatingK;
  float extRotateMix;

  float3 extDir0;
  float extCarWetness;

  float3 extDir1;
  float extPuddleAmount;

  float4x4 extShadowTransformG;
  float4x4 extReliefTransformW;
  float4 extPuddleIndex[PUDDLES_MAP_SIDE * PUDDLES_MAP_SIDE];
}

#ifdef RAINFX_USE_PUDDLES_MASK
  #define SNOW_MODE (extStreamMult < 0)
#else
  #define SNOW_MODE false
#endif

#define LIGHTINGFX_RAINFX

#ifdef TARGET_VS

  #include "include/samplers_vs.hlsl"

  #ifndef RAINFX_CAR_WETNESS_MIP
    #define RAINFX_CAR_WETNESS_MIP 0
  #endif

  Texture2D<float> txCarWetness : register(TX_SLOT_RAIN_CAR_WETNESS);

  void _rainFx_vs(float3 posC, float3 normalW, out float3 posR, out float3 normalR){
    posR = mul(float4(posC, 1), extTransform).xyz;
    normalR = mul(normalW.xyz, (float3x3)extTransform);
  }

  void _rainFx_occlusion(float3 posC, float3 tangentPacked, float ao, out float rainOcclusion, SamplerState samplerState){
    float occBase = vsLoadWet(tangentPacked);
    float heatingBase = vsLoadHeating(tangentPacked);
    rainOcclusion = lerp(saturate((occBase > 0.9 ? ao : occBase) * 2), 0, heatingBase * extRainHeatingK);

    #ifndef RAINFX_USE_PUDDLES_MASK
      #ifdef RAINFX_NO_PERVERTEX_OCCLUSION
        rainOcclusion = 1;
      #endif
      rainOcclusion *= extCarWetness; // optimization trick
      if (extCarWetness == 2){
        float2 uv = mul(float4(posC, 1), extCarWetnessTransformC).xz;
        float wetnessValue = txCarWetness.SampleLevel(samplerState, uv, RAINFX_CAR_WETNESS_MIP);
        rainOcclusion *= wetnessValue; // this needs to be multiplied by 2 without that trick
      }
    #endif

    rainOcclusion = clamp(rainOcclusion, 0, 2);
  }

  #if defined(RAINFX_USE_OCCLUSION_REGISTER)
    #define RAINFX_VERTEX(vout)\
      _rainFx_occlusion(posWnoflex.xyz - ksCameraPosition.xyz, vin.TangentPacked, ORIGINAL_AO_VALUE, vout.RainOcclusion, samLinearClamp);
  #elif defined(RAINFX_USE_REGISTERS)
    #define RAINFX_VERTEX(vout)\
      _rainFx_vs(posWnoflex.xyz - ksCameraPosition.xyz, vout.NormalW.xyz, vout.PosR, vout.NormalR);\
      _rainFx_occlusion(posWnoflex.xyz - ksCameraPosition.xyz, vin.TangentPacked, ORIGINAL_AO_VALUE, vout.RainOcclusion, samLinearClamp);
  #else
    #define RAINFX_VERTEX(vout)
  #endif

#else

  Texture2D<float> txPuddles : register(TX_SLOT_RAIN_PUDDLES_PATTERN);
  Texture2D txRainDrops : register(TX_SLOT_RAIN_DROPS);
  #ifdef RAINFX_USE_PUDDLES_MASK
    Texture2DArray<float2> txPuddlesMask : register(TX_SLOT_RAIN_PUDDLES_MASK);  
    Texture2D<float> txWetSkidmarks : register(TX_SLOT_RAIN_WET_SKIDMARKS);  
  #else
    Texture2D txRainStreaks : register(TX_SLOT_RAIN_STREAKS);
    Texture2D txRainTiny : register(TX_SLOT_RAIN_TINY_DROPS);
  #endif
  Texture2D<float> txRainRelief : register(TX_SLOT_RAIN_RELIEF_MAP);  
  Texture2DArray<float> txRainShadow : register(TX_SLOT_RAIN_SHADOW);

  #ifdef RAINFX_USE_WIPERS_MASK
    Texture2D<float> txWipersMask : register(TX_SLOT_RAIN_WIPERS_MASK);
  #endif

  #include "include/poisson.hlsl"

  float _rainFx_sampleShadow(float2 uv, float comparison){
    #ifdef MODE_SIMPLIFIED_FX
      return 1;
    #else
      float sum = 0;
      float sampleScale = 0.002;
      #ifdef RAINFX_USE_PUDDLES_MASK
        #define RAIN_DISK_SIZE 6
      #else
        #define RAIN_DISK_SIZE 3
      #endif
      if (comparison >= 1) return 1; 

      for (uint i = 0; i < RAIN_DISK_SIZE; ++i) {
        float2 sampleUV = uv.xy + SAMPLE_POISSON(RAIN_DISK_SIZE, i) * sampleScale;
        #ifdef RAINFX_USE_PUDDLES_MASK
          float comparisonUV = comparison - 0.001 * (1 + SAMPLE_POISSON_LENGTH(RAIN_DISK_SIZE, i));
        #else
          float comparisonUV = comparison;
        #endif
        sum += txRainShadow.SampleCmpLevelZero(samShadow, float3(sampleUV, HAS_FLAG(FLAG_IS_TRACK_MATERIAL)), comparisonUV);
      }
      return sum / RAIN_DISK_SIZE;
    #endif
  }

  float4 textureSampleVariation(Texture2D tx, SamplerState sam, float2 uv, float bias, float noiseMult);

  float3 _rainFx_remapNormalRipple(float2 val, float3 up, float3 side, float3 nm){
    float3 v = float3(val, 0);
    v.z = sqrt(saturate(1 - dot2(v.xy)));
    return v.x * side + v.y * up + v.z * nm;
  }

  float _rainFx_rippleWetSpot(float v){
    return saturate(v * 11);
  }

  float _rainFx_rippleSmallSplash(float v){
    return saturate((v - 0.1) / 0.9);
  }

  float4 _rainFx_calculateRipple(float2 rainUV, out float spot, out float wetSpot, float3 normalW, float2x2 rot){
    #ifdef RAINFX_USE_PUDDLES_MASK
      // float4 rainMapValue = txRainDrops.Sample(samLinear, rainUV * 0.16);
      float4 rainMapValue = textureSampleVariation(txRainDrops, samLinear, rainUV * 0.16, 1.5, 0.05);
    #else
      float4 rainMapValue = textureSampleVariation(txRainDrops, samLinear, rainUV, 1, 0.05);
    #endif
    #ifndef RAINFX_USE_DROPS
      float3 nmValue = _rainFx_remapNormalRipple(rainMapValue.xy, float3(0, 0, 1), float3(1, 0, 0), float3(0, 1, 0));
    #else
      float3 dir0 = extDir0;
      float3 dir1 = extDir1;
      dir0.yz = mul(rot, dir0.yz);
      dir1.yz = mul(rot, dir1.yz);
      float3 nmValue = remapNormal(rainMapValue.xy, -dir1, -dir0, normalW);
    #endif
    spot = rainMapValue.z;
    #ifdef RAINFX_USE_PUDDLES_MASK
      wetSpot = _rainFx_rippleWetSpot(rainMapValue.w);
      return float4(nmValue, _rainFx_rippleSmallSplash(rainMapValue.w));
    #else
      wetSpot = 0;
      return float4(nmValue, rainMapValue.w);
    #endif
  }

  struct StreakMapping {
    float2 uv1;
    float2 uv2;
    float share;
  };

  StreakMapping _rainFx_streakMapping(float3 posL, float3 normalW){
    StreakMapping ret;
    posL *= RAIN_UV_MAP_STREAK;
    float2 normalH = normalize(normalW.xz);
    ret.uv1 = abs(normalH.y) < 0.7071 ? posL.zy : posL.xy;
    if (max(normalH.x, -normalH.y) > 0.7071) ret.uv1.x = -ret.uv1.x;
    float2 rainUVTilt = normalH.x * normalH.y < 0 ? 0.7071 : float2(0.7071, -0.7071);
    ret.uv2 = float2(dot(posL.xz, rainUVTilt), posL.y);
    ret.uv1 *= float2(1, -1);
    ret.uv2 *= float2(1, -1);
    if (normalH.y < 0) ret.uv2.x *= -1;
    ret.share = saturate(abs(dot(normalH, rainUVTilt * float2(-1, 1))) * 4 - 3);
    return ret;
  }

  float3 remapNormal(float2 val, float3 up, float3 side, float3 nm, float boost){
    float3 v = float3((val * 2 - 1) * boost, 0);
    v.z = sqrt(saturate(1 - dot2(v.xy)));
    return v.x * side + v.y * up + v.z * nm;
  }

  float4 _rainFx_calculateStreak(float3 posL, float3 normalL, float3 normalW, float distance, float2x2 rot){
    #ifndef RAINFX_USE_PUDDLES_MASK
      StreakMapping SM = _rainFx_streakMapping(posL, normalL);
      
      float4 streak1 = txRainStreaks.Sample(samLinear, SM.uv1);
      float4 streak2 = txRainStreaks.Sample(samLinear, SM.uv2);
      float4 streak = lerp(streak1, streak2, SM.share);

      float3 upBase = float3(0, 1, 0);
      upBase.yz = mul(rot, upBase.yz);
      float3 sideW = normalize(cross(normalW, upBase));
      float3 upW = normalize(cross(normalW, sideW));
      float3 nm = remapNormal(streak.xy, -upW, -sideW, normalW);
      return float4(nm, streak.a);
    #else
      return 0;
    #endif
  }

  float4 _rainFx_calculateTinyRipple(float2 rainUV, out float tinyMask){
    #ifndef RAINFX_USE_PUDDLES_MASK
      // float4 ripple = textureSampleVariation(txRainTiny, samLinear, rainUV, 1, 0.05);
      float4 ripple = txRainTiny.Sample(samLinear, rainUV);
      float3 nmValue = remapNormal(ripple.xy, -extDir1, -extDir0, float3(0, 1, 0), 1.5);
      tinyMask = ripple.z;
      return float4(nmValue, ripple.a);
    #else
      tinyMask = 0;
      return 0;
    #endif
  }

  float4 _rainFx_calculateTinyStreak(float3 posL, float3 normalL, float3 normalW, out float tinyMask){
    #ifndef RAINFX_USE_PUDDLES_MASK
      StreakMapping SM = _rainFx_streakMapping(posL, normalL);
      
      // float4 streak1 = textureSampleVariation(txRainTiny, samLinear, SM.uv1, 1, 0.05);
      // float4 streak2 = textureSampleVariation(txRainTiny, samLinear, SM.uv2, 1, 0.05);
      // float4 streak = lerp(streak1, streak2, SM.share);
      
      float4 streak1 = txRainTiny.Sample(samLinear, SM.uv1);
      float4 streak2 = txRainTiny.Sample(samLinear, SM.uv2);
      float4 streak = lerp(streak1, streak2, SM.share);

      float3 sideW = normalize(cross(normalW, float3(0, 1, 0)));
      float3 upW = normalize(cross(normalW, sideW));
      float3 nm = remapNormal(streak.xy, upW, -sideW, normalW, 1.5);
      tinyMask = streak.z;
      return float4(nm, streak.a);
    #else
      tinyMask = 0;
      return 0;
    #endif
  }

  #define RIPPLE_START 0.4
  #define RIPPLE_END 0.8

  struct RainParams {
    float3 normal;
    float alpha;
    float damp;
    float wetness;
    float rainIntensity;
    float water;
    float dimming;
    float distorted;
    float occlusion;
    float specMask;
    float puddleValue;
    float reflStrength;
    float3 reflColorMult;
    bool taaDisable;
  };

  float _rainFx_rainShadow(float3 posG, float3 normalW){
    float4 posH = mul(float4(posG + normalW * 0.2, 1), extShadowTransformG);
    return any(abs(posH.xy - 0.5) > 0.5) ? 1 : _rainFx_sampleShadow(posH.xy, posH.z);
  }

  float _rainFx_rainRelief(float3 posW){
    float4 posH = mul(float4(posW, 1), extReliefTransformW);
    return any(abs(posH.xy - 0.5) > 0.5) ? 1 : saturate((txRainRelief.SampleLevel(samLinearSimple, posH.xy, 0) - posH.z) * -150 + 0.2);
  }

  float2 _rainFx_puddlesMask(float3 posW){
    #ifdef RAINFX_USE_PUDDLES_MASK
      float2 uv = posW.xz * extPuddleIndex[0].y + extPuddleIndex[0].zw;

      float2 uvFloor = floor(uv);
      int2 index = (int2)uvFloor;
      int indexBase = (index.y + PUDDLES_MAX_OFFSET) * PUDDLES_MAP_SIDE + (index.x + PUDDLES_MAX_OFFSET);
      float fallbackIndex = 63;
      #ifdef MODE_PUDDLES
        fallbackIndex = extPuddleIndex[1].y;
      #endif
      float arrayIndex = any(abs(index) > PUDDLES_MAX_OFFSET) || extPuddleIndex[indexBase].x < 0 ? fallbackIndex : extPuddleIndex[indexBase].x;

      float paddingAdj = 0.54;
      float2 uvAdj = (frac(1 - uv) * 2 - 1) * (0.5 / paddingAdj) * 0.5 + 0.5;
      // if (arrayIndex == 63) return -1;
      return txPuddlesMask.SampleLevel(samLinearSimple, float3(uvAdj, arrayIndex), 0);
    #else
      return 0.5;
    #endif
  }

  struct RainSurfaceInput {
    float4 posH;
    float3 posC;
    float3 posW;
    float3 posG;
    float3 normalW;
    float3 posL;
    float3 normalL;
    float3 toCamera;
    #ifdef RAINFX_USE_PUDDLES_MASK
      float wetSkidmarks;
    #endif
  };

  float _rainFx_sceneWetness(){
    return extSceneWetness;
  }

  float _rainFx_sceneWater(){
    return extSceneWater;
  }

  void rotate2d(inout float3 vec, float angle){
    float sinX = sin(angle);
    float cosX = cos(angle);
    float sinY = sin(angle);
    vec.yz = mul(float2x2(cosX, -sinX, sinY, cosX), vec.yz);
  }

  float4 _rainFx_rainMapSample(RainSurfaceInput input, float2 rotate, float distance, float rainShadow,
      inout bool taaDisable, inout float wetSpot, out float puddleSpot){    
    #ifdef MODE_SIMPLIFIED_FX
      taaDisable = false;
      wetSpot = 0;
      puddleSpot = 0;
      return 0;
    #endif

    #ifdef RAINFX_USE_DROPS
      float2x2 rot = float2x2(rotate.y, -rotate.x, rotate.x, rotate.y);
      input.posL.yz = mul(rot, input.posL.yz);
      input.normalL.yz = mul(rot, input.normalL.yz);
    #else
      float2x2 rot = float2x2(1, 0, 0, 1);
    #endif

    float2 rainUV = input.posL.xz * RAIN_UV_MAP_XZ;
    float4 ripple;
    float distanceMult = 0;
    puddleSpot = 0;

    [branch]
    if (input.normalL.y > RIPPLE_START){
      float wetSpotBase;
      ripple = _rainFx_calculateRipple(rainUV, puddleSpot, wetSpotBase, input.normalW, rot);
      #ifdef RAINFX_USE_PUDDLES_MASK
        distanceMult = saturate(remap(distance, 3, 30, 1, 0));
        taaDisable = distanceMult > 0;
      #else
        distanceMult = saturate(remap(distance, 20, 30, 1, 0));
      #endif
      ripple.a *= distanceMult;

      #ifdef RAINFX_USE_PUDDLES_MASK
        wetSpot = lerp(saturate(wetSpot - 0.005), saturate(lerp(wetSpot, 
          max(wetSpot - 0.005, saturate((wetSpot + 0.015 * saturate(wetSpot * 400)) * wetSpotBase)), 
          distanceMult * lerpInvSat(wetSpot, 0.01, 0.005) * saturate(extSceneRain * 10))), rainShadow);
      #else
        wetSpot = 0;
      #endif
    } else {
      ripple = float4(input.normalW, 0);
    }
    
    float4 streak;
    #ifndef RAINFX_USE_DROPS
      streak = float4(input.normalW, 0);
    #else
      [branch]
      if (input.normalL.y < RIPPLE_END && distance < 20){
        streak = _rainFx_calculateStreak(input.posL, input.normalL, input.normalW, distance, rot);
        streak.a *= saturate(remap(distance, 14, 20, 1, 0));
      } else {
        streak = float4(input.normalW, 0);
      }  
    #endif

    #ifdef RAINFX_USE_DROPS
      ripple.yz = mul(float2x2(rotate.y, rotate.x, -rotate.x, rotate.y), ripple.yz);
      return lerp(streak, ripple, saturate(remap(input.normalL.y, RIPPLE_START, RIPPLE_END, 0, 1)));    
    #else
      return ripple;
    #endif
  }

  float4 _rainFx_rainMapTinySample(RainSurfaceInput input, float distance, out float tinyMask){
    #ifdef RAINFX_USE_PUDDLES_MASK
      return 0;
    #else

      float mixParam = saturate(remap(abs(input.normalL.y), RIPPLE_START, RIPPLE_END, 0, 1));

      float4 ripple;
      float rippleTinyMask;
      [branch]
      if (mixParam > 0){
        ripple = _rainFx_calculateTinyRipple(input.posL.xz * RAIN_UV_TINY_XZ, rippleTinyMask);
      } else {
        ripple = float4(input.normalW, 0);
        rippleTinyMask = 0;
      }
      
      float4 streak;
      float streakTinyMask;
      [branch]
      if (mixParam < 1){
        streak = _rainFx_calculateTinyStreak(input.posL * RAIN_UV_TINY_STREAK, input.normalL, input.normalW, streakTinyMask);
      } else {
        streak = float4(input.normalW, 0);
        streakTinyMask = 0;
      }

      tinyMask = lerp(streakTinyMask, rippleTinyMask, mixParam);
      return lerp(streak, ripple, mixParam);
    #endif
  }

  float _rainFx_wipersMask(float3 posG, out float maskEdge){
    #ifdef RAINFX_USE_WIPERS_MASK
      float4 posH = mul(float4(posG, 1), extMaskTransformG);
      float mask = txWipersMask.Sample(samLinearBorder1, posH.xy);

      // option 1: per-pixel derivative
      // maskEdge = fwidth(mask);

      // option 2: single .GatherRed() call
      // float4 around = txWipersMask.GatherRed(samLinearBorder1, posH.xy);
      // float aroundMax = max(max(around.x, around.y), max(around.z, around.w));
      // float aroundMin = min(min(around.x, around.y), min(around.z, around.w));
      // maskEdge = aroundMax - aroundMin;

      // option 3: poisson sampling of the area
      float minValue = mask;
      float maxValue = mask;
      #define WIPERS_MASK_EDGE_SIZE 4
      [unroll]
      for (uint i = 1 /* skipping first as it is closest to center and we already have a sample from there */; i < WIPERS_MASK_EDGE_SIZE; ++i) {
        float v = txWipersMask.SampleLevel(samLinearBorder1, posH.xy + SAMPLE_POISSON(WIPERS_MASK_EDGE_SIZE, i) * 0.005, 0);
        minValue = min(v, minValue);
        maxValue = max(v, maxValue);
      }
      maskEdge = pow(maxValue - minValue, 2);

      return mask;
    #else
      return 1;
    #endif
  }

  void _rainFx_rainMapDrops(RainSurfaceInput input, float waterMult, float wetnessBoost, out float wetSpot, out float puddleSpot, out float mixedAlpha, inout RainParams ret){    
    wetSpot = _rainFx_sceneWetness();
    puddleSpot = 0;
    mixedAlpha = 0;

    #if !defined(RAINFX_NO_RAINDROPS) && (!defined(MODE_GBUFFER) || defined(RAINFX_USE_PUDDLES_MASK))
      float distance = length(input.posC.xyz) * extLODDistanceMult;
      float distanceK = distance / (abs(input.normalW.y) > RIPPLE_START ? 30 : 20);
      [branch]
      if (distanceK > 1){ 
        ret.normal = input.normalW;
      } else {
        float mask = 1;
        float maskEdge = 1;
        #ifdef RAINFX_USE_WIPERS_MASK
          mask = _rainFx_wipersMask(input.posG, maskEdge);
        #endif

        float rainShadow = _rainFx_rainShadow(input.posG, input.normalW);
        rainShadow = lerp(rainShadow, 1, wetnessBoost);

        #ifdef RAINFX_USE_PUDDLES_MASK
          float bigMult = 1;
          float tinyMult = 0;
        #else
          float bigMult = extRainMult * waterMult * rainShadow * saturate(extStreamMult) * saturate(extSceneRain * 10);
          float tinyMult = extRainMult * waterMult * saturate(remap(input.normalW.y, -0.95, -0.7, 0, 1));
          if (HAS_FLAG(FLAG_RAINFX_NO_RUNNING_WATER)){
            bigMult = 0;
          }
        #endif

        #ifdef RAINFX_USE_WIPERS_MASK
          bigMult *= saturate(mask * 2);
          tinyMult *= pow(mask, 4);
        #endif

        float4 mixed = _rainFx_rainMapSample(input, extRotate0, distance, rainShadow, ret.taaDisable, wetSpot, puddleSpot);
        #ifndef RAINFX_USE_PUDDLES_MASK
          [branch]
          if (extRotateMix){
            mixed = lerp(mixed, _rainFx_rainMapSample(input, extRotate1, distance, rainShadow, ret.taaDisable, wetSpot, puddleSpot), extRotateMix);
          }

          bigMult *= saturate(input.normalW.y * 5 + 3);
          mixed.a = saturate(mixed.a * 4 - (1 - bigMult) * 6);

          float tinyDistanceMult = saturate(1.5 - distance / 10);
          tinyDistanceMult = 1;

          [branch]
          if (tinyDistanceMult > 0){
            // tinyMult = 0.5 + 0.5 * sin(ksGameTime * 0.01);
            // tinyMult = 1;

            float tinyMask;
            float4 tiny = _rainFx_rainMapTinySample(input, distance, tinyMask);
            tiny.a *= saturate((tinyMult - tinyMask) * 20);
            tiny.a *= 1 - distanceK;
            mixed = lerp(tiny, mixed, mixed.a);
            // mixed = tiny;
          }
        #endif

        #ifdef RAINFX_USE_WIPERS_MASK
          mixed.a = lerp(mixed.a, 1, maskEdge * 0.5);
        #endif

        #ifdef RAINFX_USE_PUDDLES_MASK
          mixedAlpha = mixed.a * rainShadow;
          ret.alpha = rainShadow * saturate(extSceneRain * 10) * waterMult * (1 - distanceK);
          ret.normal = lerp(input.normalW, mixed.xyz / (1 + distance * 0.5), pow(1 - distanceK, 4));
        #else
          ret.alpha = mixed.a;
          ret.normal = normalize(lerp(input.normalW, mixed.xyz, ret.alpha));
        #endif

        float occlusionBase = dot(ret.normal, input.normalW);
        float occlusionOffset = (1 + dot(ret.normal, input.toCamera))  * 0.6;
        ret.occlusion = pow(saturate(occlusionBase + occlusionOffset), 2);

        #ifdef RAINFX_USE_WIPERS_MASK
          ret.occlusion = lerp(ret.occlusion, 2, maskEdge);
        #endif
      }
    #endif
  }

  float _rainFx_puddleValue(float randomMask, float waterMask, float hillMask0, float hillMask1, float randomFreqMask){
    return saturate(waterMask * lerp(1, 0.5, extPuddleAmount)
      + saturate(waterMask * 40) * (randomFreqMask - 0.3) * 0.1
      + pow(saturate(randomMask * 0.8 + randomFreqMask * 0.2), lerp(12, 6, extPuddleAmount)) * extPuddleAmount
      ) * lerp(pow(1 - hillMask0, 2), 1, pow(waterMask, 4));
  }

  void _rainFx_rainCalculate(float sceneRain, float sceneWet, float sceneWater, float aoValue, float puddleValue, float reliefValue, float racingLineOffset, inout RainParams ret){
    rainCalculate(extPuddleAmount, sceneRain, sceneWet, sceneWater, aoValue, puddleValue, reliefValue, racingLineOffset, ret.damp, ret.wetness, ret.water);
  }

  RainParams _rainFx_rainMap(inout RainSurfaceInput input, inout float4 txDiffuseValue, float vaoValue){
    input.posG = input.posC + ksCameraPosition.xyz;
    input.posW = input.posG - extSceneOffset;
    RainParams ret = (RainParams)0;

    // Input locational layers
    float randomMask = 0.5;
    float randomFreqMask = 0;
    float waterMask = 0;
    float racingLineOffset = 0;

    #ifdef RAINFX_USE_PUDDLES_MASK
      randomMask = txPuddles.Sample(samLinearSimple, input.posW.xz * 0.021);
      randomFreqMask = txPuddles.Sample(samLinearSimple, input.posW.xz * 0.071);
      float reliefMask = _rainFx_rainRelief(input.posW);

      float2 puddlesMask = _rainFx_puddlesMask(input.posW);
      waterMask = pow(puddlesMask.x * 0.5 + 0.5, 2);
      racingLineOffset = puddlesMask.y;

      // Hill coefficient
      float horizontalMask = saturate(remap(input.normalW.y, RIPPLE_START, RIPPLE_END, 0, 1));
      float hillMask0 = saturate(remap(input.normalW.y, 0.985, 0.995, 1, 0));
      float hillMask1 = saturate(remap(input.normalW.y, 0.98, 0.985, 1, 0));
      float hillWaves = saturate(remap(input.normalW.y, 0.997, 0.9995, 1, 0));
    #endif

    // Occlusion
    #ifdef RAINFX_USE_PUDDLES_MASK
      float waterMult = saturate(remap(vaoValue, 0.35, 0.45, 0, 1));
      float wetnessBoost = 0;
    #else
      float occlusionMult = saturate(remap(vaoValue, 0, 0.8, 0, 1));
      float waterMult = saturate(remap(vaoValue, 0.2, 1, 0, 1));
      float wetnessBoost = saturate(remap(vaoValue, 1, 2, 0, 1));
    #endif

    // if (HAS_FLAG(FLAG_IS_TRACK_MATERIAL)) input.posL *= 0.5;
    input.posL *= HAS_FLAG(FLAG_IS_TRACK_MATERIAL) ? 0.5 : 0.75;

    // Rain drops & wet spot
    float sceneWet, puddleSpot, mixedAlpha;
    #if defined(RAINFX_USE_DROPS) || defined(RAINFX_USE_PUDDLES_MASK)
      _rainFx_rainMapDrops(input, waterMult, wetnessBoost, sceneWet, puddleSpot, mixedAlpha, ret);
    #else
      sceneWet = _rainFx_sceneWetness();

      float distance = length(input.posC.xyz) * extLODDistanceMult;
      ret.alpha = _rainFx_rippleSmallSplash(_rainFx_rainMapSample(input, float2(0, 1), distance, 1, ret.taaDisable, mixedAlpha, puddleSpot).a) * lerp(0.05, 0.2, extSceneRain);
    #endif

    // Water thickness
    float sceneWater = _rainFx_sceneWater();
    ret.rainIntensity = extSceneRain * waterMult;

    // Output material layers
    #ifdef RAINFX_USE_PUDDLES_MASK
      float puddleValue = _rainFx_puddleValue(randomMask, waterMask, hillMask0, hillMask1, randomFreqMask);
      _rainFx_rainCalculate(extSceneRain, sceneWet, sceneWater, vaoValue, puddleValue, reliefMask, racingLineOffset, ret);

      ret.damp = 5 * ret.damp / (1 + 4 * ret.damp);

      ret.wetness = saturate(lerp(ret.wetness, 0, input.wetSkidmarks * lerp(1, 0.8, pow(sceneWet, 2))));
      ret.damp = saturate(lerp(ret.damp, 1, saturate(input.wetSkidmarks * 1.6)));
      ret.distorted = lerpInvSat(input.wetSkidmarks, 0.4, 0.8);
      ret.wetness = lerpInvSat(ret.damp, 0.9, 1) * pow(ret.wetness, 0.2);// * saturate(ret.wetness * 20);

      // ret.damp = 0;
      // ret.wetness = 0;
      // ret.wetness = 0;

      ret.puddleValue = puddleValue;
      ret.specMask = lerp(0.9, 1, saturate(puddleValue * 40));
    #else
      float nmOcc = saturate(input.normalW.y) * 0.5 + 0.5;
      float dampMult = saturate(occlusionMult / nmOcc) * lerp(saturate(sceneWet * 10), 1, nmOcc);
      ret.damp = saturate(sceneWet * 100);
      ret.damp = 5 * ret.damp / (1 + 4 * ret.damp);
      ret.damp *= dampMult;
      ret.puddleValue = 0;
      ret.specMask = 0;

      #ifdef MODE_SIMPLIFIED_FX
        ret.wetness = saturate(sceneWet * 2) * dampMult;
        ret.water = saturate(sceneWet * 2 - 1) * lerp(0.2, 0.4, ret.rainIntensity) * dampMult;
      #endif
    #endif

    // Fix for non-horizontal surfaces 
    #ifdef RAINFX_USE_PUDDLES_MASK
      ret.water *= horizontalMask;
      ret.wetness *= horizontalMask;
      ret.damp = lerp(_rainFx_sceneWetness(), ret.damp, horizontalMask);

      #ifndef MODE_PUDDLES
        // ret.wetness *= saturate(0.8 + 0.15 * randomMask + 0.05 * randomFreqMask);
        // ret.water *= randomMask;
        // ret.water = lerp(ret.water, 1, saturate(remap(ret.wetness, 0.84, 1, 0, 1)));
      #endif
    #endif

    // Dimming for damp surfaces
    float dimmingWet = 1 - 0.35 * HAS_FLAG(FLAG_RAINFX_SOAKING);
    #ifdef MODE_SIMPLIFIED_FX
      dimmingWet -= 0.35 * !HAS_FLAG(FLAG_RAINFX_DROPS);
    #elif !defined(RAINFX_USE_DROPS)
      dimmingWet -= 0.35;
    #endif
    ret.dimming = lerp(1, dimmingWet, ret.damp);

    // Puddle-receiving material drop effect
    #ifdef RAINFX_USE_PUDDLES_MASK
      float puddleSpotMult = saturate(ret.water * 10);
      ret.alpha = lerp(mixedAlpha * lerp(0.05, 0.2, extSceneRain), ret.alpha * puddleSpot * 0.03, puddleSpotMult);
      ret.normal = normalize(lerp(input.normalW, ret.normal, puddleSpotMult * min(4, ret.alpha * 80)));
    #endif
    ret.taaDisable = ret.taaDisable && saturate(extSceneRain * 10) * ret.water > 0.99;

    // Water rolling down for tilted puddles
    #ifdef RAINFX_USE_PUDDLES_MASK
      float rK = saturate(extSceneRain * 10) * hillWaves * ret.water * saturate(randomMask * 2 - 0.8) * 0.6; 
      float rI0 = input.posW.y * 100 + ksGameTime * 0.01 + randomMask * M_PI * 40;
      float rI1 = input.posW.y * 400 + ksGameTime * 0.01 + randomMask * M_PI * 40;
      float rW = 0.5 + 0.5 * lerp(sin(rI0), sin(rI1), saturate(remap(input.normalW.y, 0.99, 0.995, 1, 0)));
      ret.normal = normalize(ret.normal * (1 + float3(1, 0, 1) * rK * rW));
    #endif

    // Debug
    // txDiffuseValue.rgb = float3(mask, 1 - mask, 0);
    // txDiffuseValue.rgb = float3(puddleValue, 1 - puddleValue, 0);
    // waterMask = 0;
    // txDiffuseValue.rgb = float3(waterMask, 1 - waterMask, 0);
    // txDiffuseValue.rgb = float3(sqrt(hill), 1 - sqrt(hill), 0);
    // txDiffuseValue.rgb = float3(sceneWet, 1 - sceneWet, 0);
    // txDiffuseValue.rgb = float3(ret.wetness, 1 - ret.wetness, 0);
    // txDiffuseValue.rgb = float3(ret.water, 1 - ret.water, 0);
    // txDiffuseValue.rgb = float3(saturate(ret.water * 10), 1 - saturate(ret.water * 10), 0);
    // txDiffuseValue.rgb = float3(ret.alpha, 1 - ret.alpha, 0);
    // txDiffuseValue.rgb = float3(ret.damp, 1 - ret.damp, 0);
    // txDiffuseValue.rgb = float3(input.wetSkidmarks, 1 - input.wetSkidmarks, 0);
    // txDiffuseValue.rgb = float3(vaoValue, 1 - vaoValue, 0);
    // txDiffuseValue.rgb = float3(extSceneRain, 1 - extSceneRain, 0);
    // txDiffuseValue.rgb = float3(reliefMask, 1 - reliefMask, 0);
    // txDiffuseValue.rgb = float3(ret.specMask, 1 - ret.specMask, 0);

    #ifdef RAINFX_USE_PUDDLES_MASK
      if (racingLineDebug) {
        txDiffuseValue.rgb = float3(saturate(racingLineOffset), 1 - saturate(racingLineOffset), saturate(-racingLineOffset));
      }
    #endif

    // txDiffuseValue.rgb = float3(saturate(racingLineOffset), 0.2, saturate(-racingLineOffset));
    // txDiffuseValue.rgb = float3(abs(racingLineOffset), 1 - abs(racingLineOffset), 0);

    #ifdef MODE_PUDDLES
      #ifdef RAINFX_USE_PUDDLES_MASK
        ret.water = puddleValue;     // returned as R, see utils_ps_gbuff.fx
        ret.wetness = reliefMask;    // returned as G
        ret.damp = racingLineOffset; // for accRainNearbyPuddlesGen_ps
      #else
        ret.wetness = 0;
        ret.water = 0;
        ret.damp = 0;
      #endif
    #endif

    ret.reflStrength = ret.alpha * pow(saturate(1 - dot(ret.normal, -input.toCamera)), 2);
    ret.reflColorMult = lerp(1, saturate(lerp(dot(txDiffuseValue.rgb, 0.333), txDiffuseValue.rgb, 2)), 0.9);
    #ifdef RAINFX_USE_PUDDLES_MASK
      if (SNOW_MODE) {
        ret.reflStrength = randomMask * 0.7 + randomFreqMask * 0.3;
      }
    #endif
    return ret;
  }

  #define _RAINFX_GETWATERREFL_ARGS float4 posH, float3 posW, float3 txDiffuseValue, float3 _rfxNormalWBase, float3 posC, float3 toCamera, float shadow, RainParams R, float3 giLighting
  #define _RAINFX_GETWATERREFL_CALL posH, posW, txDiffuseValue, _rfxNormalWBase, posC, toCamera, shadow, R, giLighting

  float4 _rainFx_water_impl(float4 base, _RAINFX_GETWATERREFL_ARGS);
  
  float4 _rainFx_water(float4 base, _RAINFX_GETWATERREFL_ARGS){
    return _rainFx_water_impl(base, _RAINFX_GETWATERREFL_CALL);
  }

  float3 _rainFx_water(float3 base, _RAINFX_GETWATERREFL_ARGS){
    return _rainFx_water_impl(float4(base, 1), _RAINFX_GETWATERREFL_CALL).rgb;
  }

  float3 _rainFx_applyDim(float3 color, float dimming) { return color * dimming; }
  float4 _rainFx_applyDim(float4 color, float dimming) { return color * float4(dimming.xxx, 1); }
  float4 _rainFx_applyDirt(float4 color, float dimming) { return color * float4(dimming.xxx, 1) * dimming; }

  void _rainFx_applyShiny(inout RainParams RP, float3 _rfxNormalWBase, inout float3 normalW, inout float3 txDiffuseValue, inout float specExp, inout float specValue, 
      inout float smoothNormal){
    #ifdef RAINFX_USE_DROPS
      smoothNormal = 0;
      return;
    #endif

    if (RAINFX_IS_ROUGH) {
      smoothNormal = 0;
      return;
    }
    #ifndef RAINFX_USE_DROPS
      // RP.specMask *= lerpInvSat(dot(normalW, _rfxNormalWBase), 0.9, 1);
      // RP.water = lerp(RP.water, saturate(RP.wetness * 2), pow(1 - RP.specMask, 2) * saturate(RP.puddleValue * 40));
    #endif

    if (SNOW_MODE) {
      RP.damp = 1;
    }

    specExp = lerp(specExp, 40, RP.damp);
    specValue = lerp(specValue, 0.3, RP.damp * RP.specMask);

    #ifdef RAINFX_USE_PUDDLES_MASK
      specExp = lerp(specExp, 120, RP.wetness * lerp(0.5, 1, RP.specMask));
      specValue = lerp(specValue, 1.0, RP.wetness * RP.specMask);

      smoothNormal = 0;
      // smoothNormal = RP.wetness * 0.5;
      // smoothNormal = lerp(smoothNormal, 1, saturate(remap(RP.wetness, 0.84, 1, 0, 1)));
      // smoothNormal = pow(RP.wetness, 16) * 0.8;
      smoothNormal = lerp(smoothNormal, 1, saturate((RP.water - 0.02) * lerp(4, 20, saturate(RP.puddleValue * 40 - 2) )));

      if (SNOW_MODE) {
        smoothNormal = lerp(smoothNormal, 1, 0.2 + pow(RP.reflStrength, 2) * 0.65);
      }

      normalW = normalize(lerp(normalW, RP.normal, lerp(smoothNormal, -0.2, RP.distorted)));

      smoothNormal *= lerp(1, 0.8, RP.rainIntensity);
      specExp = lerp(specExp, 1500 * lerp(1, 0.4, RP.rainIntensity), smoothNormal);
      specValue = lerp(specValue, 1.2 * lerp(1, 0.2, RP.rainIntensity), smoothNormal);

      // specExp *= lerp(1, 0.4, pow(RP.rainIntensity, 2));
      // specValue *= lerp(1, 0.4, pow(RP.rainIntensity, 2));

      // specExp = lerp(specExp, 0.1, RP.alpha);
      // specValue = lerp(specValue, 200, RP.alpha);
      // txDiffuseValue = lerp(txDiffuseValue, 40, RP.alpha);
    #else
      smoothNormal = RP.water;
    #endif
  }

  void _rainFx_applyShinyPBR(RainParams RP, float3 _rfxNormalWBase, float aoValue, inout float3 normalW, inout float roughness, inout float3 f0,
      inout float smoothNormal){
    #ifdef RAINFX_USE_DROPS
      smoothNormal = 0;
      return;
    #endif

    if (RAINFX_IS_ROUGH) {
      smoothNormal = 0;
      return;
    }

    float waterLayer = lerp(RP.damp, 0.2, pow(roughness, 8)) * pow(aoValue, 2);
    roughness = lerp(roughness, 0.2, waterLayer);
    f0 = lerp(f0, 0.04, waterLayer);

    #ifdef RAINFX_USE_PUDDLES_MASK
      roughness = lerp(roughness, 0.1, RP.wetness * lerp(0.5, 1, RP.specMask));
      f0 = lerp(f0, 0.04, RP.wetness * lerp(0.5, 1, RP.specMask));

      smoothNormal = RP.wetness * 0.9;
      smoothNormal = lerp(smoothNormal, 1, RP.water);
      smoothNormal = pow(smoothNormal, 4);
      normalW = normalize(lerp(normalW, RP.normal, lerp(smoothNormal, -0.2, RP.distorted)));

      roughness = lerp(roughness, 0, smoothNormal);
      f0 = lerp(f0, 0.04, smoothNormal);
    #else
      smoothNormal = RP.water;
    #endif
  }

  void _rainFx_applyReflective(RainParams RP, float smoothNormal, inout float fresnelC, inout float fresnelMaxLevel, 
      inout float fresnelEXP, inout float specExp, inout float finalMult){
    #ifdef RAINFX_USE_DROPS
      return;
    #endif

    if (RAINFX_IS_ROUGH) return;

    // specExp = max(2, lerp(specExp, max(specExp, 2), RP.damp));
    // fresnelMaxLevel = lerp(fresnelMaxLevel, 0.05, RP.damp);
    // fresnelC = lerp(fresnelC, 0.02, RP.damp);

    // specExp = lerp(specExp, 200, RP.wetness);
    // fresnelMaxLevel = lerp(fresnelMaxLevel, 0.3, RP.wetness * RP.specMask);
    // fresnelC = lerp(fresnelC, 0.04, RP.wetness * RP.specMask);

    // specExp = lerp(specExp, 300 * lerp(1, 0.1, RP.rainIntensity), smoothNormal);
    // fresnelMaxLevel = lerp(fresnelMaxLevel, 0.5, smoothNormal);
    // fresnelC = lerp(fresnelC, 0.1, smoothNormal);
    
    specExp = max(2, lerp(specExp, max(specExp, 2), RP.damp));
    fresnelMaxLevel = lerp(fresnelMaxLevel, 0.1, RP.damp);
    fresnelC = lerp(fresnelC, 0.04, RP.damp);

    specExp = lerp(specExp, 200, RP.wetness);
    fresnelMaxLevel = lerp(fresnelMaxLevel, GAMMA_OR(0.3, 0.6), RP.wetness * RP.specMask);
    fresnelC = lerp(fresnelC, 0.08, RP.wetness * RP.specMask);

    specExp = lerp(specExp, 300 * lerp(1, 0.1, RP.rainIntensity), smoothNormal);
    fresnelMaxLevel = lerp(fresnelMaxLevel, 1, smoothNormal);
    fresnelC = lerp(fresnelC, 0.2, smoothNormal);
    
    // specExp *= lerp(1, 0.1, RP.rainIntensity);
    fresnelEXP = lerp(fresnelEXP, 5, RP.damp);
    finalMult = lerp(finalMult, 1, RP.damp);
  }

  void _rainFx_gBufferNormal(float3 posC, inout float3 normalW, float3 _rfxNormalWBase){    
    #if defined(MODE_GBUFFER) && defined(RAINFX_USE_PUDDLES_MASK)
      float flattening = 0.95;
      normalW = normalize(lerp(normalW, _rfxNormalWBase, flattening));
    #endif
  }

  #ifdef RAINFX_USE_PUDDLES_MASK
    #define _RAINFX_INIT_SCREENSPACE RP_input.wetSkidmarks = txWetSkidmarks.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw, 0);
  #else
    #define _RAINFX_INIT_SCREENSPACE
  #endif

  #define _RAINFX_INIT_BASE \
    float3 _rfxNormalWBase = normalize(pin.NormalW);\
    RainSurfaceInput RP_input;\
    RP_input.posH = pin.PosH;\
    RP_input.posC = pin.PosC;\
    RP_input.normalW = _rfxNormalWBase;\
    RP_input.toCamera = toCamera;\
    _RAINFX_INIT_SCREENSPACE

  #ifdef RAINFX_USE_REGISTERS
    #define RAINFX_INIT \
      _RAINFX_INIT_BASE\
      RP_input.posL = pin.PosR;\
      RP_input.normalL = normalize(pin.NormalR);\
      RainParams RP = _rainFx_rainMap(RP_input, txDiffuseValue, pin.RainOcclusion);\
      float3 rainNormal = RP.normal;
  #else
    float restoreOriginalVAO(float value) {
      return saturate(pow(saturate(value), GAMMA_VAO_EXP_INV(1 / extVAOExp)) - extVAOAdd) / extVAOMult;
    }

    #define RAINFX_INIT \
      _RAINFX_INIT_BASE\
      RP_input.posL = ksInPositionWorld;\
      RP_input.normalL = normalize(pin.NormalW);\
      RainParams RP = _rainFx_rainMap(RP_input, txDiffuseValue, restoreOriginalVAO(((float3)(_AO_VAO)).g));\
      float3 rainNormal = RP.normal;
  #endif

  float _rainFx_localHighlight(float3 color, float3 blurred){
    return saturate((luminance(color) - luminance(blurred)) * 4);
  }

  #ifdef RAINFX_FOLIAGE
    #define RAINFX_WET(X) RAINFX_INIT; X.rgb *= lerp(X.g, 1, saturate(remap(RP.dimming, 0.3, 1, 0, 1)));
  #else
    #define RAINFX_WET(X) RAINFX_INIT; RAINFX_WET_DIM(X.rgb);
  #endif

  #define RAINFX_WET_DIM(X) X = _rainFx_applyDim(X, RP.dimming);
  #define RAINFX_WET_DIRT(X) X = _rainFx_applyDirt(X, RP.dimming);
  #define RAINFX_SHINY(X) \
    float smoothNormal;\
    _rainFx_applyShiny(RP, _rfxNormalWBase, normalW, X.txDiffuseValue, X.specularExp, X.specularValue, smoothNormal);\
    X.normalW = normalW;
  #define RAINFX_SHINY_PBR(X) \
    float smoothNormal;\
    _rainFx_applyShinyPBR(RP, _rfxNormalWBase, X.localOcclusion, normalW, X.roughness, X.f0, smoothNormal);
  #define RAINFX_REFLECTIVE_SKIP(X) \
    X.fresnelCDry = X.fresnelC;\
    X.fresnelMaxLevelDry = X.fresnelMaxLevel;\
    X.fresnelEXPDry = X.fresnelEXP;

  #ifdef RAINFX_FOLIAGE
    #if !defined(MODE_GBUFFER)
      #define RAINFX_REFLECTIVE(X) \
        if (!RAINFX_IS_ROUGH){\
          ReflParams R = getReflParamsZero();\
          RAINFX_REFLECTIVE_SKIP(R);\
          R.finalMult = RP.damp;\
          R.fresnelMaxLevel = foliageSpecular * lerp(0.1, 0.3, extSceneRain);\
          R.fresnelC = 1;\
          R.fresnelEXP = 1;\
          X = calculateReflection(X, toCamera, pin.PosC, normalW, R);\
        }
    #else
      #define RAINFX_REFLECTIVE(X)
    #endif
  #else
    #define RAINFX_REFLECTIVE(X) \
      RAINFX_REFLECTIVE_SKIP(X);\
      _rainFx_applyReflective(RP, smoothNormal, X.fresnelC, X.fresnelMaxLevel, X.fresnelEXP, X.ksSpecularEXP, X.finalMult);
    // #define RAINFX_WATER(X) 
  #endif

  #define RAINFX_WATER(X) \
    X = _rainFx_water(X, pin.PosH, ksInPositionWorld, txDiffuseValue.rgb, _rfxNormalWBase, pin.PosC, toCamera, shadow, RP, GI_LIGHTING);\
    _rainFx_gBufferNormal(pin.PosC, normalW, _rfxNormalWBase);
  #define RAINFX_WATER_ALPHA(X, Y) {\
    float4 v = _rainFx_water(float4(X, Y), pin.PosH, ksInPositionWorld, txDiffuseValue.rgb, _rfxNormalWBase, pin.PosC, toCamera, shadow, RP, GI_LIGHTING);\
    X = v.xyz; Y = v.w;\
    _rainFx_gBufferNormal(pin.PosC, normalW, _rfxNormalWBase);\
  }

  #define RAINFX_REFLECTIVE_WATER_ROUGH(X) \
    ReflParams R = getReflParamsZero();\
    if (!RAINFX_IS_ROUGH) {\
      APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);\
      X = calculateReflection(X, toCamera, pin.PosC, normalize(lerp(normalW, _rfxNormalWBase, 0.9 * (1 - RP.water))), R);\
      RAINFX_WATER(X);\
    }

#endif