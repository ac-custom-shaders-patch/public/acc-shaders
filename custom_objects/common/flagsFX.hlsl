#define VS_OUT_EXTRAS float FlagYRel : EXTRA0; float WindOffset : EXTRA1; float2 FlagSize : EXTRA2;
#define HS_OUT_EXTRAS VS_OUT_EXTRAS

float2 FFX_loadVector(uint vec){
  return float2(f16tof32(vec), f16tof32(vec >> 16));
}

float3 FFX_combineNormals(float3 nm1, float3 nm2){
  // hill12
  float3 t = nm1 + float3(0, 1, 0);
  float3 u = float3(-nm2.x, nm2.y, -nm2.z);
  return normalize(t * dot(t, u) - u * t.y);
}

#ifdef TARGET_DS
  float FFX_restoreOriginalVAO(float value) {
    return saturate(pow(saturate(value), GAMMA_VAO_EXP_INV(1 / extVAOExp)) - extVAOAdd) / extVAOMult;
  }
#endif

#if !defined(TARGET_HS) && !defined(NO_FLAGS_LOGIC)

  #include "include_new/ext_functions/wind.fx"

  float3 kunosWave(float2 uv) {
    float2 offset = sin(uv.xy * ksGameTime * frequency / 1000);
    return saturate(uv.x) * distortion * float3(offset.x, offset.xy);
  }

  #define WAVE(FREQ, AXIS, MULT) lerp(\
    sin(time * FREQ * 1.1 + posW.AXIS * MULT),\
    sin(time * FREQ * 0.9 - posW.AXIS * MULT),\
    0.5 + 0.4 * sin(time * FREQ * 0.27 + posW.AXIS * MULT))

  void customWaveTent(inout float3 posW, inout float3 normal, float mult, float ao) {
    uint mode = uint(mult);

    float mainFreq = 20;
    float mainMult = 0.25;
    float subFreq = 47;
    float subMult = 7;
    float hangingWet = 5;
    float minAllowed = -1e30;
    float intensityTotal = frac(mult) * 2;
    float windShift = 1.5;

    if (mode == 1) {
      mainMult = 5;
      minAllowed = -0.01;
      intensityTotal = sqrt(frac(mult) * 100) * 0.01;
      // hangingWet = 0.25;
      windShift = 0;
    }

    float intensityBase = extWindSpeed / (1 + extWindSpeed * 0.07);
    #define TENT_STEP max(minAllowed, ((lerp(WAVE(mainFreq, x, mainMult) * sign(extWindDir.x), WAVE(mainFreq, z, mainMult) * sign(extWindDir.y), abs(extWindDir.y))\
      + lerp(WAVE(subFreq, x, subMult) * sign(extWindDir.x), WAVE(subFreq, z, subMult) * sign(extWindDir.y), abs(extWindDir.y)) * 0.05) * intensityBase - hangingWet * (extSceneWetness * 100 / (1 + extSceneWetness * 100))) * intensityTotal)

    float time = extWindWave;
    float2 extWindDir = normalize(extWindVel);
    float o0 = TENT_STEP;
    float o1, o2;
    float3 posWBak = posW;
    {
      posW.x += 0.05;
      o1 = TENT_STEP;
    }
    {
      posW.xz += float2(-0.05, 0.05);
      o2 = TENT_STEP;
    }
    float intensity = intensityBase * intensityTotal;
    posW = posWBak + intensity * windShift * float3(extWindDir.x, 0, extWindDir.y);
    posW.y += o0;
    normal = FFX_combineNormals(normal, normalize(float3(o0 - o1, 0.05 / max(1e-9, intensity), o0 - o2)));
  }

  void customWave(float2 uv, inout float3 posW, inout float3 normal, out float3 dir, float2 flagSize, float flagYRel, float windOffset,
      float2 extWindVel, float extWindSpeed, float extWindWave) {
    uv.x = frac(uv.x * 0.99999);
    // windOffset = 0;

    float3 fwd = cross(normal, float3(0, -1, 0));
    fwd.y = 0;
    fwd = normalize(fwd);

    bool flipped = flagSize.x < 0;
    if (flipped){
      fwd = -fwd;
      flagSize.x = -flagSize.x;
    }
    
    bool isVertical = flagSize.x < 0.01;

    // #define WAVE(FREQ, AXIS, MULT) \
    //   sin(time * FREQ * 0.3 + posW.AXIS * MULT) * \
    //   sin(time * FREQ * 0.5 + posW.AXIS * MULT)

    float speedK = saturate(extWindSpeed / MAX_WIND_SPEED * 2);
    float wobbleK = saturate(extWindSpeed / MAX_WIND_SPEED * 2 - 0.5);

    float time = extWindWave + windOffset * 20;
    float departureMult = lerp(0.5, 1, 0.5 + 0.5 * WAVE(4, y, 0)) * wobbleK * flagSize.y;
    float departureK = (1 - pow(abs(1 - flagYRel * 2), 2)) * departureMult;
    float horPos = uv.x * flagSize.x;
    uv.x += isVertical ? 0 : departureK * 0.1;
    // time -= uv.x / (1 + extWindSpeed * 0.4);
    time -= uv.x * 0.15;
    // time -= uv.x * extWindWave * 0.0001;
    
    float foldK = pow(max(0.001, saturate(1.2 - extWindSpeed * lerp(0.2, 0.4, frac(windOffset * 17 + 0.3)))), 4);
    float yMult = isVertical ? 1.2 : 2;
    float hOffsetBase = lerp(WAVE(10, y, yMult), WAVE(20, y, 1.2 * yMult), saturate(1 + wobbleK - speedK));
    float hOffsetK = pow(saturate(uv.x), 0.5) * speedK * saturate(1.4 - wobbleK) * 0.5;
    float hOffsetSlowBase = WAVE(4, y, 1.2 * yMult / 2);
    float hOffsetSlowK = saturate(uv.x) * lerp(0, 0.3, saturate(speedK * 2));
    float2 newDir = normalize(extWindVel + fwd.xz * 0.0013);

    [branch]
    if (isVertical){
      normal = normalize(normal);

      float3 diffDir = normal;
      if (dot(diffDir, float3(1, 0, 1)) > 0) diffDir = -diffDir;
      float offsetDist = (WAVE(7.7, x, 1.2) + hOffsetBase) * hOffsetK;
      posW += diffDir * offsetDist;
      posW += fwd * pow(abs(offsetDist), 2);
      posW += float3(extWindVel.x, 0, extWindVel.y) * hOffsetK 
        * (1.5 + WAVE(3.1, x, 1) + WAVE(2.7, z, 1)) * lerp(2, 4, uv.x) / (20 + extWindSpeed)
        * saturate(dot(float3(newDir.x, 0, newDir.y), fwd) * 0.5 + 0.5);
      dir = fwd;
    } else {
      // posW -= fwd * uv.x * flagSize.x / 2;

      float3 offset = -fwd * horPos;
      float3 newFwd = float3(newDir.x, 0, newDir.y);
      dir = newFwd;
      posW += (newFwd - fwd) * horPos; 
      posW += newFwd * departureK * 0.1; 

      // foldK = 0.5 + 0.5 * sin(ksGameTime / 500);

      normal = normalize(cross(newFwd, float3(0, 1, 0)));      
      float hOffset = hOffsetBase * hOffsetK * min(flagSize.x, 4);
      hOffset += hOffsetSlowBase * hOffsetSlowK * flagSize.x * foldK;
      float yOffset = flagSize.x * uv.x * saturate((0.99 + 0.002 * uv.x * WAVE(20, y, 4)) * foldK * 0
        + (0.2 * (1 - speedK - foldK) + speedK * 0.1 * WAVE(5, z, 0.2) * WAVE(3, x, 0.2)))
        + departureMult * uv.x * pow(2 * flagYRel - 1, 2) * sign(0.5 - flagYRel) * 0.1;
      posW.y -= yOffset * 0.8;
      // posW += normal * uv.x * (0.3 - abs(flagYRel)) * 0.8 * foldK * 0.5;
      // posW -= newFwd * uv.x * saturate(abs(flagYRel) - 0.5) * foldK;

      float stiffK = saturate(flagSize.y / flagSize.x * 2 - 2);
      foldK = saturate(foldK);
      foldK *= lerp(0.96, 0.8, stiffK);

      float vNoise = lerp(4, 6, 0.5 + 0.5 * sin(windOffset + flagYRel));
      float bStraight1 = saturate((1 - flagYRel) * flagSize.y / flagSize.x);
      float bStraight2 = saturate(1 - flagYRel * flagSize.y / flagSize.x);
      float foldMult = lerp(1, 0.7, sqrt(saturate(flagYRel)));
      float budgeK = 1 - (1 - pow(abs(1 - flagYRel * 2), 2)) * 0.0;
      // float budgeK = flagYRel;
      float foldDir = frac(windOffset) > 0.5 ? 1 : -1;
      float foldT = 1 - uv.x - frac(windOffset * 13) * 0.8;
      float foldC = smoothstep(0, 1, saturate(remap(bStraight1, foldT - 0.6, foldT - 0.0, 0, 1)));
      float foldW = sin(1 - flagYRel * max(flagSize.y / flagSize.x, 1.2) * vNoise);
      float foldB = foldK * pow(1 - foldC, 2);
      foldK *= lerp(0.1, 1, foldC);
      posW -= newFwd * horPos * foldK * lerp(budgeK, foldMult, stiffK);
      hOffset += foldDir * horPos * pow(foldK, 2) * foldW * lerp(0.4, 0.2, flagYRel) * bStraight1;
      posW.y -= horPos * sqrt(1 - pow(1 - foldK, 2)) * lerp(1, uv.x, bStraight1 * (1 - bStraight2) * 0.5) * foldMult;
      hOffset += foldDir * horPos * foldB * saturate(1 - uv.x);

      posW += normal * hOffset;

      float zOffset = horPos - sqrt(max(0, horPos * horPos - hOffset * hOffset - yOffset * yOffset));
      posW -= newFwd * zOffset;
    }
  }

  void customWaveNm(float2 uv, inout float3 posW, inout float3 normalW, float2 flagSize, float flagYRel, float windOffset,
      float2 extWindVel, float extWindSpeed, float extWindWave){
    float3 dir;
    float3 baseResult = posW; 
    float3 normal = normalW;
    customWave(uv, baseResult, normal, dir, flagSize, flagYRel, windOffset, extWindVel, extWindSpeed, extWindWave);
    
    float dx = uv.x > 0.5 ? -0.1 : 0.1;
    float dy = 0.1;

    float3 higher = posW - float3(0, dy, 0); 
    normal = normalW;
    customWave(uv, higher, normal, dir, flagSize, flagYRel + dy / flagSize.y, windOffset, extWindVel, extWindSpeed, extWindWave);
    
    float3 further = posW + dir * dx; 
    normal = normalW;
    customWave(uv + float2((flagSize.x < 0.01 ? 0 : dx / flagSize.x * (flagSize.x < 0 ? -1 : 1)), 0), further, normal, dir, flagSize, flagYRel, windOffset, extWindVel, extWindSpeed, extWindWave);

    posW = baseResult;
    normalW = normalize(cross(further - baseResult, higher - baseResult) * -sign(dx));
    if (flagSize.x < 0){
      normalW = -normalW;
      // posW.y += 1e5;
    }
  }

#endif