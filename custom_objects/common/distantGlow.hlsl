#ifndef EXTRA_PARAMETERS_2
  #define EXTRA_PARAMETERS_2 
#endif

cbuffer cbEmissiveLight : register(b12) {  
  float3 ksEmissive;
  float extMaxDistanceHalfInv;

  float alpha;
  float extDistanceMult;
  float extProjectorPower;
  float extProjectorEXP;

  float extDistantMult;
  float extDistantEXP;
  float extDotRadiusMult;
  float extDotBrightnessBase;

  float extDotBrightnessCenter;
  float extDotCenterEXP;
  float extDistantUseTexture;
  float extDistantHeightMult;

  float extDistantRadiusMult;
  float extDistantSoftK;
  EXTRA_PARAMETERS_2
}

// #define ORIGINAL_MODE

float3 normalizeGlow(float3 v){
  // float a = max(max(v.x, v.y), v.z);
  float a = dot(v, 0.33);
  return v / max(1, a);
}

#ifdef TARGET_PS
  float3 calculateEmissivePart(float3 posC, float3 normalW, float3 emissiveMult){
    float3 toCamera = normalize(posC);
    float3 emissivePart = GAMMA_KSEMISSIVE(ksEmissive * emissiveMult) * getEmissiveMult();
    float NdotV = dot(normalize(-toCamera), normalize(normalW));
    emissivePart += GAMMA_KSEMISSIVE(ksEmissive * pow(saturate(NdotV), max(extProjectorEXP, 1)) * extProjectorPower);
    float fovK = sin(3.141592 / 180 * ksFOV);
    #ifndef ORIGINAL_MODE
      float distantFade = pow(1 / max(length(posC) * extDistanceMult - 30, 1), GAMMA_GENERIC_EXP(0.85));
      emissivePart *= saturate(distantFade / fovK);
    #endif
    #ifdef DIM_NEARBY_EMISSIVES
      if (RENDERING_REFLECTION_CUBEMAP){
        emissivePart *= saturate(dot(posC, posC) / 200 - 0.25);
      }
    #endif
    return emissivePart;
  }
#endif