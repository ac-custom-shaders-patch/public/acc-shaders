#ifdef USE_LOCAL_TAA
  cbuffer _cbCustomTAA : register(b11) {
    float4x4 ksVpPrev;
    float4x4 ksWorldPrevFTaa;
    float2 extCSMult;
    float2 extPad2;

    #ifdef USE_SPS
      float4x4 ksVpPrev0;
    #endif
  }
#endif

cbuffer cbRefractingCover : register(b6) {
  float3 ksEmissive1;
  float3 ksEmissive2;
  float3 ksEmissive3;
  float3 ksEmissive4;
  float3 ksEmissive5;
}

cbuffer _cbRefractingCover : register(b12) {  
  float3 extInnerPos;
  float extInnerRadius;

  float3 extInnerDir;
  float extInnerDepth;

  float3 extInnerUp;
  float extIOR;

  float3 extInnerSide;
  float extIORInv;

  float extF0;
  float extAbsorptionFactor;
  float extNmShareExt;
  float extNmShareInt;

  float extReflectionMult;
  float extReflectionDiffuseMult;
  float extRaytraceStepStart;
  float extRaytraceStepIncrease;

  float extLODBias;
  float3 extGlassColor;

  float extBaseEmissiveK;
  float extAmbientMult;
  float extInnerSpecular;
  float extInnerSpecularEXP;

  float extBouncedBackMult; // 0.4
  float extExtraSideThickness; // 100
  float extGlassExtraThickness; // 0.25
  float extGlassEmissiveMult; // 0.25

  float extKsEmissiveMult;
  float extIORFlyOut; // extIOR→1
  float extSideFalloff;
  float extHasOverlay;

  float4 extMirrorPlane;

  float extUseKsEmissive;
  float extDiffuseMapMult;
  float extReflectiveGamma;
  float extDiffuseMapEmissiveMult;

  float ksEmissiveSeparate;
  float extBulbReflectionK;
  float extBlurMult;
  float extUseNormalAlpha;

  float emMirrorChannel2As5;
  float3 extBulbColor;

  float4x4 extNormalTransform;
}

struct PS_IN_RefractingCover {
  STRUCT_POSH_MODIFIER float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  float Fog : TEXCOORD8;
  AO_NORMALS
  float4 PosV_IsMirrored : TEXCOORD12;

  #ifdef USE_LOCAL_TAA
    float3 PosCS0 : TEXCOORD10;
    float3 PosCS1 : TEXCOORD11;
  #endif
  RAINFX_REGISTERS
  CUSTOM_MODE_REGISTERS
};

#ifdef TARGET_PS
  Texture2D txOverlayMask : register(TX_SLOT_MAT_2);
  Texture2D txSurfaceNormal : register(TX_SLOT_MAT_3);
  Texture2D txInnerNormal : register(TX_SLOT_MAT_4);
  Texture2D txInnerDiffuse : register(TX_SLOT_MAT_5);
  Texture2D txInnerEmissive : register(TX_SLOT_MAT_6);
  Texture2D<float> txInnerDepth : register(TX_SLOT_MAT_7);
#endif