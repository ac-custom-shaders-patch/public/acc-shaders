#ifndef _RAINUTILS_HLSL_
#define _RAINUTILS_HLSL_

#ifdef TARGET_VS

  #ifndef MODE_MAIN_NOFX
    #define AO_EXTRA_LIGHTING 1
    #define GI_LIGHTING 0
    #define LIGHTINGFX_NOSPECULAR
    #define LIGHTINGFX_KSDIFFUSE 1
	  #define GAMMA_LIGHTINGFX_KSDIFFUSE_ONE
    #define LIGHTINGFX_SIMPLEST
    #ifndef SHADER_MIRROR
      #define LIGHTINGFX_FIND_MAIN
    #endif
    #define POS_CAMERA vout.PosC
  #endif

  static const float2 RAIN_BILLBOARD[] = {
    float2(-1, -1),
    float2(1, -1),
    float2(-1, 1),
    float2(-1, 1),
    float2(1, -1),
    float2(1, 1),
  };
  
  #define LOAD_DROP_BILLBOARD\
    uint vertexID = fakeIndex % VERTICES_PER_PARTICLE;\
    uint instanceID = fakeIndex / VERTICES_PER_PARTICLE;\
    StructuredBuffer<RainDrop> particleBuffer : register(t0);

  float3 rainBillboard(uint vertexID, float3 posC, float3 velC, float particleSize,  
      out float2 tex, out float3 normalBase, out float blur, float stretchMult = 1);

  float3 rainBillboard(uint vertexID, float3 posC, float3 velC, float particleSize,  
      out float2 tex, float stretchMult = 1){
    float3 _0;
    float _1;
    return rainBillboard(vertexID, posC, velC, particleSize, tex, _0, _1, stretchMult);
  }

  #ifdef LIGHTINGFX_FIND_MAIN
    #define CALCULATE_LIGHTING(vout) {\
      LFX_MainLight mainLight;\
      float3 lightingFX = 0;\
      LIGHTINGFX(lightingFX);\
      vout.LightFocus = saturate(1.4 * mainLight.power / max(dot(lightingFX, 1), 1));\
      vout.LightDir = mainLight.dir;\
      vout.LightVal = lightingFX;\
			vout.Shadow = getShadowBiasMult(vout.PosC, 0, float3(0, 1, 0), mul(float4(posW.xyz, 1), ksShadowMatrix0), 0, 1); }
  #else
    #define CALCULATE_LIGHTING(vout) {\
      float3 lightingFX = 0;\
      LIGHTINGFX(lightingFX);\
      vout.LightVal = lightingFX; }
  #endif

  #ifdef MODE_SHADOW
    #define COMMON_SHADING(vout)
  #elif defined(MODE_SHADOW)
    #define COMMON_SHADING(vout) {\
      vout.PosC = posW.xyz - ksCameraPosition.xyz; }
  #elif defined(MODE_GBUFFER)
    #define COMMON_SHADING(vout) {\
      vout.PosC = posW.xyz - ksCameraPosition.xyz;\
      GENERIC_PIECE_VELOCITY(posW, particle.velocity * extFrameTime); }
  #else
    #define COMMON_SHADING(vout) {\
      vout.PosC = posW.xyz - ksCameraPosition.xyz;\
      vout.Fog = calculateFog(posV);\
      CALCULATE_LIGHTING(vout); }
  #endif

#endif

#ifdef TARGET_PS

  #ifndef NO_DEPTH_MAP
    #include "include_new/ext_functions/depth_map.fx"
  #endif

  #ifdef USE_FAST_WAY
    #undef PARAM_BLUR
    #undef PARAM_USE_COLOR
    #define PARAM_BLUR 1
    #ifdef USE_COLOR_BUFFER
      #define PARAM_USE_COLOR 1
    #else
      #define PARAM_USE_COLOR 0
    #endif
  #endif

  #define gStretching 0

  float simpleReflectanceModelPart(float3 viewDir, float3 lightDir, float3 normalW, float exp){
    float specularBase = saturate(dot(normalize(viewDir + lightDir), normalW));
    return pow(specularBase, exp) * LAMBERT(normalW, lightDir);
  }

  float simpleReflectanceModel(float3 viewDir, float3 lightDir, float3 normalW){
    #ifdef USE_FAST_WAY
      return simpleReflectanceModelPart(viewDir, lightDir, normalW, 20) * 0.1;
    #else
      return simpleReflectanceModelPart(viewDir, lightDir, normalW, 40)
        + simpleReflectanceModelPart(viewDir, lightDir, -reflect(normalW, viewDir), 10) * 0.1;
    #endif
  }

  float3 sampleEnv(float3 ray, float bias, float dimming){
    float dimmingMult = saturate(remap(ray.y, -1, 0.2, 0, 1));
    return SAMPLE_REFLECTION_FN(ray, bias, false, REFL_SAMPLE_PARAM_DEFAULT) * lerp(1, dimmingMult, dimming);
  }

  float calculateSoft(float4 posH, float3 posC, float blur){    
    #ifdef NO_DEPTH_MAP
      return 1.0;
    #else
      float depthZ = getDepthAccurate(posH);
      float depthC = linearizeAccurate(posH.z);
      return saturate((depthZ - depthC) * lerp(10, 1, blur * saturate(dot2(posC) / 2 - 1)));
    #endif
  }

#endif

#endif