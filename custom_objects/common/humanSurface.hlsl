struct EyeParams {
  float2 center;
  float eyeFullMovementMult; // 100
  float eyeDistanceScaleY; // 0.5

  float4 area;
  
  float tweakAOffsetPow1; // -0.005
  float tweakAOffsetPow2; // -0.004
  float tweakASizePow1; // 0.005
  float tweakASizePow2; // 0
  
  float tweakBOffsetPow1;
  float tweakBOffsetPow2; // -0.0015
  float tweakBSizePow1; // -0.004
  float tweakBSizePow2; // -0.005

  float tweakTwistAmount; // -0.006
  float tweakTwistFrom; // 0.6
  float tweakTwistFalloff0; // 0.8
  float tweakTwistFalloff1; // 0.9
};

EyeParams getParams(float4 p1, float4x4 p2){
  EyeParams r;
  r.center = p1.xy;
  r.eyeFullMovementMult = p1.z;
  r.eyeDistanceScaleY = p1.w;
  r.area = p2[0];
  r.tweakAOffsetPow1 = p2[1][0];
  r.tweakAOffsetPow2 = p2[1][1];
  r.tweakASizePow1 = p2[1][2];
  r.tweakASizePow2 = p2[1][3];
  r.tweakBOffsetPow1 = p2[2][0];
  r.tweakBOffsetPow2 = p2[2][1];
  r.tweakBSizePow1 = p2[2][2];
  r.tweakBSizePow2 = p2[2][3];
  r.tweakTwistAmount = p2[3][0];
  r.tweakTwistFrom = p2[3][1];
  r.tweakTwistFalloff0 = p2[3][2];
  r.tweakTwistFalloff1 = p2[3][3];
  return r;
}

cbuffer cbStaticData : register(b6) {  
  float3 extMouth;
  float extEyeIrisOuterSize;

  float3 extEyebrowCorner;
  float extEyeIrisInnerSize;

  float3 extCheek;
  float extEyeCorrection;

  float4 extHairInclusionInvArea0;
  float4 extHairInclusionInvArea1;
  float4 extHairExclusionArea0;
  float4 extHairExclusionArea1;
  float4 extHairExclusionArea2;
  float4 extHairExclusionArea3;

  EyeParams extEyeLParams;
  EyeParams extEyeRParams;

  float extBlinkUVOffset;
  float extSubsurfaceScatteringIntensity;
  float extHairRimBrightness;
  float extIrisBrightness;

  float extHairAlbedoThreshold;
  float extSurfaceTiltThreshold;
  float extBrightnessFactorThreshold;
  float extDebugMode;

  float extMouthMode;
  float3 _hsPad;

  float4 extFakeShadowParams0;
  float4 extFakeShadowParams1;
}

cbuffer _cbDynamicData : register(b12) {  
  float extMouthMovement;
  float extEyebrowCornerMovement;
  float extMouthOpened;
  float extBlink;

  float2 extEyeMovement;
  float extIrisGrow;
  uint extSkinSweat_extSkinRed;

  #define extSkinSweat loadVector(extSkinSweat_extSkinRed).x
  #define extSkinRed loadVector(extSkinSweat_extSkinRed).y
}

#define gHeadSide mul(float3(1, 0, 0), (float3x3)ksWorld)
#define gHeadFwd mul(float3(0, 0, 1), (float3x3)ksWorld)
#define gHeadUp mul(float3(0, 1, 0), (float3x3)ksWorld)
