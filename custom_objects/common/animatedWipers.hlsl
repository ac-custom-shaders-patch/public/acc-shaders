#include "include/common.hlsl"
#include "include/samplers_vs.hlsl"

Texture2DArray<float> txOffsetMap : register(TX_SLOT_MAT_12);

bool isVertexStatic(uint vertexRole, uint layer){ return vertexRole == 255; }
bool isVertexAffected(uint vertexRole, uint layer){ return vertexRole > layer * 32 && vertexRole < (layer + 1) * 32; }
bool isVertexBase(uint vertexRole, uint layer){ return vertexRole == layer * 32 + 2; }
bool isVertexArm(uint vertexRole, uint layer){ return vertexRole == layer * 32 + 3; }
bool isVertexTurning(uint vertexRole, uint layer){ return vertexRole == layer * 32 + 4 || vertexRole == layer * 32 + 5; }
bool isVertexLaggingBehind(uint vertexRole, uint layer){ return vertexRole == layer * 32 + 5; }
bool isVertexExtra0(uint vertexRole, uint layer){ return vertexRole == layer * 32 + 6; }
bool isVertexExtra1(uint vertexRole, uint layer){ return vertexRole == layer * 32 + 7; }
bool isVertexCleaningWater(uint vertexRole, uint layer){ return vertexRole == layer * 32 + 1 || vertexRole > layer * 32 + 3 && vertexRole < layer * 32 + 6; }

#ifndef ANIMATED_WIPERS_PREV_BOOST
  #define ANIMATED_WIPERS_PREV_BOOST 2
#endif

struct WiperParams {
  float3 lineFrom;
  float lineDistSqrInv;
  float3 lineDelta;
  float armEndWiperPos;

  float3 offsetAxis;
  float animPos;

  float bendingTransitionStart;
  float bendingTransitionEnd;
  float twistTransitionStart;
  float twistTransitionEnd;

  float4x4 rotateBending;
  float4x4 rotateBase;
  float4x4 rotateTurning;
  float4x4 rotateExtra0;
  float4x4 rotateExtra1;

  float4x4 rotateBendingPrev;
  float4x4 rotateBasePrev;
  float4x4 rotateTurningPrev;
  float4x4 rotateExtra0Prev;
  float4x4 rotateExtra1Prev;

  float4x4 twist;
  float4x4 twistPrev;

  float laggingBehindAmount;
  float laggingBehindPivot;
  float laggingBehindRangeInv;
  float animPosPrev;

  float4 rubberFrom;
  float4 rubberTo;
  float rubberOffsetPos;
  float rubberOffsetAmount;
  float rubberOffsetExp;
  float rubberClipThreshold;

  float calculateWiperPos(float3 posL) {
    return dot(posL - lineFrom, lineDelta) * lineDistSqrInv;
  }

  float3 rotateVec(float3 pos, float4x4 rotate){
    return mul(float4(pos, 1), rotate).xyz;
  }

  float3 rotateVec(float3 pos, float4x4 rotate, float4x4 rotatePrev, bool usePrev){
    return usePrev ? lerp(rotateVec(pos, rotate), rotateVec(pos, rotatePrev), ANIMATED_WIPERS_PREV_BOOST) : rotateVec(pos, rotate);
  }

  float3 rotateVec(float3 pos, float4x4 rotate, float4x4 rotatePrev, bool usePrev, float laggingBehind){
    return lerp(rotateVec(pos, rotate), rotateVec(pos, rotatePrev), usePrev * ANIMATED_WIPERS_PREV_BOOST + laggingBehind);
  }

  float3 calculateInner(float3 pos, uint vertexRole, float layer, float wiperPos, float3 offsetValue, bool usePrev){
    if (isVertexArm(vertexRole, layer)){
      offsetValue *= saturate(remap(wiperPos, 0, armEndWiperPos, 0, 1));
    }

    pos = lerp(pos, rotateVec(pos, usePrev ? twistPrev : twist), saturate(remap(wiperPos, twistTransitionStart, twistTransitionEnd, 0, 1)));

    if (isVertexTurning(vertexRole, layer)) {
      float laggingBehind = isVertexLaggingBehind(vertexRole, layer) ? pow(wiperPos * laggingBehindRangeInv + laggingBehindPivot, 2) * laggingBehindAmount : 0;
      return rotateVec(pos, rotateTurning, rotateTurningPrev, usePrev, laggingBehind) + offsetValue;
    } else if (isVertexExtra0(vertexRole, layer)) {
      return rotateVec(pos, rotateExtra0, rotateExtra0Prev, usePrev) + offsetValue;
    } else if (isVertexExtra1(vertexRole, layer)) {
      return rotateVec(pos, rotateExtra1, rotateExtra1Prev, usePrev) + offsetValue;
    } else {
      float bendingMix = isVertexBase(vertexRole, layer) ? 0 : saturate(remap(wiperPos, bendingTransitionStart, bendingTransitionEnd, 0, 1));
      float3 posBase = rotateVec(pos, rotateBase, rotateBasePrev, usePrev);
      float3 posBending = rotateVec(pos, rotateBending, rotateBendingPrev, usePrev) + offsetValue;
      return lerp(posBase, posBending, bendingMix);
    }
  }

  float3 calculateOffset(float layer, float wiperPos, bool usePrev){
    return offsetAxis * txOffsetMap.SampleLevel(samLinearClamp, float3(wiperPos, usePrev ? animPosPrev : animPos, layer), 0);
  }

  bool calculate(inout float3 pos, inout float3 normal, uint vertexRole, float layer, bool usePrev, out float wiperPos){
    if (!isVertexAffected(vertexRole, layer) || lineDistSqrInv == 0) {
      wiperPos = 0;
      return false;
    }

    wiperPos = calculateWiperPos(pos);
    if (isVertexStatic(vertexRole, layer)) {
      return true;
    }

    float3 offsetValue = calculateOffset(layer, wiperPos, usePrev);
    float3 origPos = pos;
    pos = calculateInner(pos, vertexRole, layer, wiperPos, offsetValue, usePrev);
    #ifndef MODE_SHADOWS_ADVANCED
      normal = normalize(calculateInner(origPos + normal * 0.001, vertexRole, layer, wiperPos, offsetValue, usePrev) - pos);
    #endif
    return true;
  }

  bool calculate(inout float3 pos, inout float3 normal, uint vertexRole, float layer, bool usePrev){
    float wiperPos;
    return calculate(pos, normal, vertexRole, layer, usePrev, wiperPos);
  }

  float3 calculateRubber(float3 pos, float layer, bool usePrev, uint partIndex, bool withLagging){
    float wiperPos = calculateWiperPos(pos);    
    float3 offsetValue = calculateOffset(layer, wiperPos, usePrev);
    pos = lerp(pos, rotateVec(pos, usePrev ? twistPrev : twist), saturate(remap(wiperPos, twistTransitionStart, twistTransitionEnd, 0, 1)));

    float laggingBehind = withLagging ? pow(wiperPos * laggingBehindRangeInv + laggingBehindPivot, 2) * laggingBehindAmount : 0;
    float4x4 rotate, rotatePrev;
    if (partIndex == 1) {
      rotate = rotateBase;
      rotatePrev = rotateBasePrev;
    } else if (partIndex == 2) {
      rotate = rotateTurning;
      rotatePrev = rotateTurningPrev;
    } else if (partIndex == 3) {
      rotate = rotateExtra0;
      rotatePrev = rotateExtra0Prev;
    } else if (partIndex == 4) {
      rotate = rotateExtra1;
      rotatePrev = rotateExtra1Prev;
    } else {
      rotate = rotateBending;
      rotatePrev = rotateBendingPrev;
    }

    return rotateVec(pos, rotate, rotatePrev, usePrev, laggingBehind) + offsetValue;
  }

  float3 rubberLocalPos(float rubberPos){
    float offsetAmount = rubberPos > rubberOffsetPos
      ? remap(rubberPos, 1, rubberOffsetPos, 0, 1)
      : remap(rubberPos, 0, rubberOffsetPos, 0, 1);
    return lerp(rubberFrom.xyz, rubberTo.xyz, rubberPos)
      + offsetAxis * ((1 - pow(saturate(1 - offsetAmount), rubberOffsetExp)) * rubberOffsetAmount);
  }

  float4 rubberPos(float wiperPos){
    // .xyz for local coordinate, .w for normalized position along wiper
    float rubberPos = remap(wiperPos, rubberFrom.w, rubberTo.w, 0, 1);
    return float4(rubberLocalPos(rubberPos), rubberPos);
  }

  float2 rubberPosDistance(float3 inPos, float wiperPos){
    return float2(remap(wiperPos, rubberFrom.w, rubberTo.w, 0, 1), abs(dot(inPos - rubberPos(wiperPos).xyz, offsetAxis)));
  }
};

#define ENTRIES_COUNT 4

cbuffer _cbWiperParams : register(b5) {
  WiperParams gWiperParams[ENTRIES_COUNT];
}

float3 animatedWipersCalculate(float3 inPos, uint vertexRole, bool usePrev){
  float3 n = 0;
  for (int i = 0; i < ENTRIES_COUNT; ++i){
    if (gWiperParams[i].calculate(inPos, n, vertexRole, i, usePrev)) break;
  }
  return inPos;
}

float3 animatedWipersCalculate(float3 inPos, uint vertexRole, bool usePrev, out float2 rubberPosDistance, out float rubberClipThreshold, inout float wiperIndex){
  float3 basePos = inPos;
  float3 n = 0;
  for (int i = 0; i < ENTRIES_COUNT; ++i){
    float wiperPos;
    if (gWiperParams[i].calculate(inPos, n, vertexRole, i, usePrev, wiperPos)) {
      if (isVertexCleaningWater(vertexRole, i)) {
        rubberPosDistance = gWiperParams[i].rubberPosDistance(basePos, wiperPos);
        rubberClipThreshold = gWiperParams[i].rubberClipThreshold;
        wiperIndex = i;
      }
      break;
    }
  }
  return inPos;
}
