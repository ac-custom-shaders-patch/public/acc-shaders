#ifdef SUPPORTS_COLORFUL_AO
  #undef SUPPORTS_COLORFUL_AO
  #define SUPPORTS_NORMALS_AO
#endif

#ifdef ALLOW_PERVERTEX_AO
  #define AO_NORMALS centroid float Ao : COLOR;
  #define AO_COLORFUL centroid float Ao : COLOR1;
#else
  #define AO_NORMALS 
  #define AO_COLORFUL 
#endif

#ifdef NO_SHADOWS
  #define SHADOWS_COORDS_ITEMS
#else
  #define SHADOWS_COORDS_ITEMS \
    float4 ShadowTex0 : TEXCOORD3;
#endif

#ifdef CREATE_MOTION_BUFFER
  #define MOTION_BUFFER float3 PosCS0 : TEXCOORD10; float3 PosCS1 : TEXCOORD5;
#else 
  #define MOTION_BUFFER
#endif

#ifndef VS_OUT_EXTRAS
  #define VS_OUT_EXTRAS
#endif

struct VS_OUT {
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  #ifndef MODE_SHADOWS_ADVANCED
    float3 NormalW : TEXCOORD0;
    #ifndef NO_NORMALMAPS
      float3 TangentW : TEXCOORD6;
      float3 BitangentW : TEXCOORD7;
    #endif
    #ifdef SUPPORTS_COLORFUL_AO
      AO_COLORFUL
    #else
      AO_NORMALS
    #endif
  #endif
  float TessFactor : TESS;
  VS_OUT_EXTRAS
};

#ifndef HS_OUT_EXTRAS
  #define HS_OUT_EXTRAS
#endif

struct HS_OUT {
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  #ifndef MODE_SHADOWS_ADVANCED
    float3 NormalW : TEXCOORD0;
    #ifndef NO_NORMALMAPS
      float3 TangentW : TEXCOORD6;
      float3 BitangentW : TEXCOORD7;
    #endif
    #ifdef SUPPORTS_COLORFUL_AO
      AO_COLORFUL
    #else
      AO_NORMALS
    #endif
  #endif
  HS_OUT_EXTRAS
};

#ifndef DS_OUT_EXTRAS
  #define DS_OUT_EXTRAS
#endif

#ifndef CUSTOM_MODE_REGISTERS
  #define CUSTOM_MODE_REGISTERS
#endif

#ifndef OPT_STRUCT_FIELD_FOG
  #ifdef NO_FOG
    #define OPT_STRUCT_FIELD_FOG(X)
  #else
    #define OPT_STRUCT_FIELD_FOG(X) X;
  #endif
#endif

struct DS_OUT {
  float4 PosH : SV_POSITION;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  #ifndef MODE_SHADOWS_ADVANCED
    float3 NormalW : TEXCOORD0;
    MOTION_BUFFER
    SHADOWS_COORDS_ITEMS
    #ifndef NO_NORMALMAPS
      float3 TangentW : TEXCOORD6;
      float3 BitangentW : TEXCOORD7;
    #endif
    OPT_STRUCT_FIELD_FOG(float Fog : TEXCOORD8)
    #ifdef SUPPORTS_COLORFUL_AO
      AO_COLORFUL
    #else
      AO_NORMALS
    #endif
  #endif
  DS_OUT_EXTRAS
  CUSTOM_MODE_REGISTERS
};

struct PatchTess {
  float EdgeTess[3] : SV_TessFactor;
  float InsideTess  : SV_InsideTessFactor;
};

