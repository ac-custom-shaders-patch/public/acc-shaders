#include "refraction.hlsl"

float4 _rainFx_water_impl(float4 base, _RAINFX_GETWATERREFL_ARGS){
  #if !defined(MODE_GBUFFER) && !defined(RAINFX_FORCE_ROUGH)
    #if defined(RAINFX_USE_PUDDLES_MASK) || !defined(RAINFX_USE_DROPS)
      base.rgb = lerp(base.rgb, getAmbientBaseAt(posC, float3(0, 1, 0), 1), R.alpha * (1 - R.water));
      // base.rgb = lerp(base.rgb, getAmbientBaseAt(posC, float3(0, 1, 0), 1), R.alpha);
    #else
      if (base.a == 0) return base;

      float VdotN = saturate(-dot(R.normal, toCamera));
      float3 reflDir = lerp(reflect(toCamera, R.normal), R.normal, 0);
      float3 reflColor = SAMPLE_REFLECTION_FN(reflDir, 2, false, REFL_SAMPLE_PARAM_DEFAULT);
      float reflStrength = lerp(0.0, 0.5, pow(saturate(1 - dot(R.normal, -toCamera)), 5));

      #ifdef RAINFX_GLASS_BACKLIT
        float4 refractionColor = calculateRefraction(posH, posC, toCamera, R.normal, 0.02 * pow(R.alpha, 2));
        float refractionIntensity = (1 - base.a) * saturate(R.alpha * 4 - 2) * refractionColor.a;
        base.rgb = lerp(base.rgb, refractionColor.rgb, refractionIntensity);
        base.a = lerp(base.a, 1, refractionIntensity);
      #endif

      shadow *= saturate(dot(_rfxNormalWBase, ksLightDirection.xyz) * -3);
      float3 specular = reflectanceModel(-toCamera, -ksLightDirection.xyz, R.normal, 200) * shadow * 2 * extSpecularColor;
      base.rgb += R.reflStrength * shadow * extSpecularColor * saturate(-dot(_rfxNormalWBase, toCamera)) * 0.15 * R.reflColorMult;
      // base.rgb += giLighting * R.alpha * 0.5 * R.reflColorMult;
      base.rgb += R.reflStrength * reflColor * 0.2;
      if (!HAS_FLAG(FLAG_ALPHA_TEST)) {
        base.a = lerp(base.a, 1, reflStrength * R.alpha);
      }
      // base.rgb *= lerp(1, R.occlusion, R.alpha * 0.8);
      base.rgb = lerp((AMBIENT_COLOR_NEARBY + giLighting) * R.reflColorMult * 0.03, base.rgb, lerp(1, R.occlusion, R.alpha * 0.8));
      base.rgb += specular * R.alpha;
    #endif
  #endif
  return base;
}