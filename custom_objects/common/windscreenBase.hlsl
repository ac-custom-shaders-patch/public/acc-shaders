#ifdef WINDSCREEN_ADVANCED_REFLECTION
	#include "include/poisson.hlsl"
	Texture2D _txTracingColor : register(t6);
#endif

void doWindscreenBase(PS_IN_PerPixel pin, float3 normalW, float3 toCamera,
		out float3 resultColor, out float resultAlpha, out float4 txDiffuseValue, out float shadow, float shadowMult = 1, float dirtMult = 1) {
	txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
	if (solidBrightnessAdjustment){
		txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, saturate(txDiffuseValue.rgb + solidBrightnessAdjustment), saturate(txDiffuseValue.a * 10 - 5));
	}

	if (alphaGamma){
		txDiffuseValue.a = pow(saturate(txDiffuseValue.a), alphaGamma);
	}
	bool useLightBounceEffect = !bannerMode && txDiffuseValue.a < 0.5;

	#ifdef WINDSCREEN_ADVANCED_REFLECTION
		float4 traced = reflectionsIntensity ? _txTracingColor.SampleLevel(samLinearBorder0, pin.PosH.xy * extScreenSize.zw, 0) : 0;
	#else
		float4 traced = 0;
	#endif
	#ifdef WINDSCREEN_SHADOW_REQUIRED
		shadow = getShadow(REL_POS_CAMERA, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW) * shadowMult;
	#endif

	resultColor = 0;
	resultAlpha = 0;
 
	#ifdef WINDSCREEN_ADVANCED_REFLECTION
		[branch]
		if (traced.w){
			// #define TRACED_DISK_SIZE 12
			// for (uint i = 0; i < TRACED_DISK_SIZE; ++i) {
			// 	float2 offset = SAMPLE_POISSON(TRACED_DISK_SIZE, i);
			// 	float2 sampleUV = pin.PosH.xy + offset;
			// 	traced += _txTracingColor.SampleLevel(samLinearClamp, sampleUV * extScreenSize.zw, 0);
			// }
			// traced /= TRACED_DISK_SIZE + 1;

			if (useLightBounceEffect) {
				#ifndef WINDSCREEN_SHADOW_REQUIRED
					shadow = 1;
				#endif
				if (reflectionsIntensity == 1) {
					resultColor = traced.rgb;
					resultAlpha = 1;
				} else {
					float reflectionBoost = pow(saturate(1 + dot(normalW, toCamera)), 6);
					traced.rgb *= getCPLMult(reflect(toCamera, normalW));

					if (reflectionsIntensity < 0) {
						resultColor = traced.rgb * lerp(-reflectionsIntensity * GAMMA_OR(0.89, 1), 50, reflectionBoost);
						resultAlpha = 0.02;
					} else {
						resultColor = traced.rgb * lerp(reflectionsIntensity * GAMMA_OR(0.89, 1), 1000, reflectionBoost);
						resultAlpha = 0.001;
					}
				}
				return;
			}
		}
	#endif

	if (GAMMA_FIX_ACTIVE) txDiffuseValue.a = GAMMA_LINEAR(txDiffuseValue.a);
	float alphaBase = txDiffuseValue.a * saturate(luminance(ksLightColor.rgb) * GAMMA_OR(0.1, 1));

  #ifdef REL_POS_CAMERA
    // Version with shadow blurring
		#ifndef WINDSCREEN_SHADOW_REQUIRED
    	shadow = getShadow(REL_POS_CAMERA, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW) * shadowMult;
		#endif
		
		float lightValue = GAMMA_LINEAR_SIMPLE(saturate(dot(toCamera, -ksLightDirection.xyz) * shadow));
    #ifndef ALLOW_EXTRA_VISUAL_EFFECTS
      lightValue = 0;
    #endif
    
		lightValue *= dirtMult;
    float3 diffuse = ksLightColor.rgb * GAMMA_OR(1, ksDiffuse) * lightValue;
    #ifdef BLUR_SHADOWS
      resultAlpha = useLightBounceEffect ? (disableTexture ? 0 : lightValue * alphaBase) : txDiffuseValue.a;
    #else
      resultAlpha = useLightBounceEffect ? txDiffuseValue.a * lightValue : txDiffuseValue.a;
    #endif
  #else
    // Version without shadow blurring
    shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);

		float lightValue = GAMMA_LINEAR_SIMPLE(saturate(dot(toCamera, -ksLightDirection.xyz) * shadow));
    #ifndef ALLOW_EXTRA_VISUAL_EFFECTS
      lightValue = 0;
    #endif
    
    float3 diffuse = ksLightColor.rgb * GAMMA_OR(1, ksDiffuse) * lightValue;
    resultAlpha = useLightBounceEffect ? lightValue * alphaBase : txDiffuseValue.a;
  #endif

	resultColor = applyGamma(diffuse, calculateAmbient(pin.PosC, normalW, (float4)1), 
		txDiffuseValue.rgb, ksDiffuse, ksAmbient);
	resultColor += traced.rgb * 0.2;
	resultColor += calculateEmissive(pin.PosC, txDiffuseValue.rgb, 1);
	// if (useLightBounceEffect && GAMMA_FIX_ACTIVE) resultAlpha *= 0.1;

  #ifdef NO_LIGHTING
    resultColor = 0.0;
  #endif 
}