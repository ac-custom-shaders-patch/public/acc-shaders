#define COMPUTE_DIRT_LEVEL \
  float dirtLevel;\
  if (dirt < 0) {\
    dirtLevel = -dirt * saturate(txDustValue.a * 2);\
    txDustValue.xyz = 0.8 + 0.2 * txDustValue.a;\
  } else {\
    RAINFX_WET_DIRT(txDustValue);\
    dirtLevel = dirt * txDustValue.a;\
  }