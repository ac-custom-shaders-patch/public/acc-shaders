#ifdef TARGET_VS

  #ifndef RAIN_BILLBOARD_STRETCH
    #define RAIN_BILLBOARD_STRETCH 0.005
  #endif

  float3 rainBillboard(uint vertexID, float3 posW, float3 velC, float particleSize,  
      out float2 tex, out float3 normalBase, out float blur, float stretchMult){
    float2 quadPos = RAIN_BILLBOARD[vertexID];

    float3 posC = posW - ksCameraPosition.xyz;
    float3 billboardAxis = normalize(posC);
    #ifdef MODE_SHADOW
      billboardAxis = ksLightDirection.xyz;
    #endif

    float3 velocity = dot(velC, 1) ? velC : float3(0, 1, 0);
    float speed = length(velocity);
    float3 velocityDir = velocity / speed;

    float3 side = normalize(cross(billboardAxis, velocityDir));
    float3 up = normalize(cross(billboardAxis, side));

    tex = quadPos;
    normalBase = up * tex.y + side * tex.x;
    blur = saturate(remap(length(velC), 2, 3, 0, 1));

    float3 offset = (side * quadPos.x + up * quadPos.y) * particleSize;
    float3 stretchOffset = velocity * dot(velocityDir, normalize(offset));
    offset += RAIN_BILLBOARD_STRETCH * stretchMult * stretchOffset;

    #ifdef RAIN_WIDE_SPLASH
      float3 splashDir = normalize(cross(velocity, float3(0, 1, 0)));
      float splashOffset = quadPos.x * abs(billboardAxis.y);
      offset += splashDir * clamp(abs(splashOffset), 0, lerp(0.02, 0.05, frac(stretchMult * 1567.263))) * sign(splashOffset);
      // offset += float3() * clamp(abs(splashOffset), 0, lerp(0.02, 0.05, frac(stretchMult * 1567.263))) * sign(splashOffset);
    #endif

    return offset;
  }

#endif
