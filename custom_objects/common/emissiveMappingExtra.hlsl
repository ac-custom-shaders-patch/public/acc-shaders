// Somewhat similar to emissive_mapping.hlsl, but instead of having all those
// options for any occation, this one is designed to work primarily with dashboard 
// indicators. You know those, tens of lights on a single bit of mesh. For that reason,
// this shader has 25 emissive channels, that should cover all the bases. Might be
// more expensive, but compare it with even 5 different draw calls, 5 different materials,
// 5 different constant buffers to update on a regular basis.

// Areas are defined with geometical way only, allowing to create either rectangles 
// with optionally rounded corners or circles, with optional falloff set by exponent
// function. For dashboard lights, that should be enough. Local position-based mirroring 
// is available for channels 1-6, 2-5 and 3-4, as usual. With them disabled, 
// parameterized geometry will be used.

// Options related to mirrored UV or diffuse usage are still there, txEmissive is used 
// as an optional colored mask on top.

Texture2D txEmissive : register(t4);

#ifdef MODE_KUNOS
  #define EMISSIVE_CHANNEL(ID) float3 ksEmissive##ID;
#else
  // #define EMISSIVE_CHANNEL(ID) float3 ksEmissive##ID; float3 _ksEmissive##ID##_pad;
  #define EMISSIVE_CHANNEL(ID) float3 ksEmissive##ID;
#endif

#define EMISSIVE_SHAPE(ID)\
  float4 emGeoChannel##ID##S;\
  float4 emGeoChannel##ID##P;

#define EMISSIVE_PART(ID)\
  { float geo = procGeo(EME_ARGS_PASS, emGeoChannel##ID##S, emGeoChannel##ID##P); summaryEmissive += ksEmissive##ID * geo; summaryGeo += geo; }

#define RUN_MACRO(MACRO)\
  MACRO(1)\
  MACRO(2)\
  MACRO(3)\
  MACRO(4)\
  MACRO(5)\
  MACRO(6)\
  MACRO(7)\
  MACRO(8)\
  MACRO(9)\
  MACRO(10)\
  MACRO(11)\
  MACRO(12)\
  MACRO(13)\
  MACRO(14)\
  MACRO(15)\
  MACRO(16)\
  MACRO(17)\
  MACRO(18)\
  MACRO(19)\
  MACRO(20)\
  MACRO(21)\
  MACRO(22)\
  MACRO(23)\
  MACRO(24)

cbuffer cbEmissiveChannels : register(b12) { 
  RUN_MACRO(EMISSIVE_CHANNEL);
};

cbuffer cbProceduralStuff : register(b11) {  
  float emAlphaFromDiffuse;
  float emMirrorChannel3As4;
  float emMirrorChannel2As5;
  float emMirrorChannel1As6;

  float emSkipDiffuseMap;
  float emSkipEmissiveMap;
  float emMaskResultDiffuseBlur;
  float emMaskResultNormalAlphaBlur;

  float3 emMirrorDir;
  float emMirrorOffset;

  EMISSIVE_SHAPE(0)
  RUN_MACRO(EMISSIVE_SHAPE);

  float2 emDiffuseAvgColorAsMultiplier0;
  float2 emDiffuseAvgColorAsMultiplier1;
  float2 emDiffuseAvgColorAsMultiplier2;
  float2 emDiffuseAvgColorAsMultiplier3;
  float2 emDiffuseAlphaAsMultiplier0;
  float2 emDiffuseAlphaAsMultiplier1;
  float2 emDiffuseAlphaAsMultiplier2;
  float2 emDiffuseAlphaAsMultiplier3;

  float2 emMirrorUV;
  float emMirrorUVOffset;
  float emUseChannel0AsFallback;
}

#define USE_TXEMISSIVE_AS_EMISSIVE
#define TXEMISSIVE_VALUE float4(luminance(txDiffuseValue.xyz), 0, 0, 0)

#if defined(TARGET_VS)
  #ifdef EME_TARGET_VS_OVERRIDE
    #include "include_new/base/cbuffers_ps.fx"
  #endif
  #include "include_new/base/_include_vs.fx"

  float getExtraValue(float3 posL){
    return dot(emMirrorDir, 1) ? dot(posL, emMirrorDir) - emMirrorOffset : 1;
  }
#endif

#if defined(TARGET_PS) || defined(EME_TARGET_VS_OVERRIDE)
  #ifndef EME_TARGET_VS_OVERRIDE
    #include "include_new/base/_include_ps.fx"
  #endif

  #ifndef EME_SAMPLER
    #define EME_SAMPLER samLinear
  #endif

  #ifdef TARGET_VS
    #ifndef EME_ARGS_DECL 
      #define EME_ARGS_DECL float2 uv, float2 uvR, bool bgMode
    #endif

    #ifndef EME_ARGS_PASS 
      #define EME_ARGS_PASS uvFrac, uvR, bgMode
    #endif
  #else
    #ifndef EME_ARGS_DECL 
      #define EME_ARGS_DECL float2 uv
    #endif

    #ifndef EME_ARGS_PASS 
      #define EME_ARGS_PASS uvFrac
    #endif
  #endif

  float procGeo(EME_ARGS_DECL, float4 p0, float4 p1){
    // hit = (uv - center) / halfSize
    // hit = uv / halfSize - center / halfSize
    // p0.xy = 1 / halfSize
    // p0.zw = -center / halfSize
    float2 hit = uv * p0.xy + p0.zw; // gives -1…1 if value within box, 0 for center, linear
    float2 hitAbs = 1 - abs(hit); // gives 1…0 value within box, 1 for center
    #ifdef TARGET_VS
      // if (!bgMode) hitAbs = min(1, hitAbs + uvR * p0.xy);
    #endif
    return pow(saturate(1 - length(1 - saturate(hitAbs * p1.xy))), max(p1.z, 0.0001));
  }

  float3 getEmissiveValue(float4 diffuseValue, EME_ARGS_DECL, bool isMirrored){

    float2 uvFrac = frac(uv);
    float3 summaryEmissive = 0;
    float summaryGeo = 0;
    RUN_MACRO(EMISSIVE_PART);

    if (emDiffuseAvgColorAsMultiplier3.x){
      summaryEmissive *= saturate(lerp(1, pow(saturate(dot(diffuseValue.rgb, 0.333) * emDiffuseAvgColorAsMultiplier0.x + emDiffuseAvgColorAsMultiplier1.x), 
        emDiffuseAvgColorAsMultiplier2.x), emDiffuseAvgColorAsMultiplier3.x));
    }

    if (emDiffuseAlphaAsMultiplier3.x){
      summaryEmissive *= saturate(lerp(1, pow(saturate(diffuseValue.a * emDiffuseAlphaAsMultiplier0.x + emDiffuseAlphaAsMultiplier1.x), 
        emDiffuseAlphaAsMultiplier2.x), emDiffuseAlphaAsMultiplier3.x));
    }

    float3 emissiveBase = ksEmissive;
    if (emDiffuseAvgColorAsMultiplier3.y){
      emissiveBase *= saturate(lerp(1, pow(saturate(dot(diffuseValue.rgb, 0.333) * emDiffuseAvgColorAsMultiplier0.y + emDiffuseAvgColorAsMultiplier1.y), 
        emDiffuseAvgColorAsMultiplier2.y), emDiffuseAvgColorAsMultiplier3.y));
    }

    if (emDiffuseAlphaAsMultiplier3.y){
      emissiveBase *= saturate(lerp(1, pow(saturate(diffuseValue.a * emDiffuseAlphaAsMultiplier0.y + emDiffuseAlphaAsMultiplier1.y), 
        emDiffuseAlphaAsMultiplier2.y), emDiffuseAlphaAsMultiplier3.y));
    }

    if (emUseChannel0AsFallback) {
      summaryEmissive += emissiveBase * (emUseChannel0AsFallback == 2 ? 1 : saturate(1 - summaryGeo * 10));
    } else {
      summaryEmissive += emissiveBase * procGeo(EME_ARGS_PASS, emGeoChannel0S, emGeoChannel0P);
    }

    #ifdef TARGET_VS
      summaryEmissive *= emSkipEmissiveMap ? 1 : txEmissive.SampleLevel(EME_SAMPLER, uv, getTextureLevel(txEmissive, uvR)).rgb;
      float4 maskDiffuse = txDiffuse.SampleLevel(EME_SAMPLER, uv, getTextureLevel(txDiffuse, uvR) + emMaskResultDiffuseBlur);
      float4 maskNormalAlpha = txNormal.SampleLevel(EME_SAMPLER, uv, getTextureLevel(txNormal, uvR) + emMaskResultNormalAlphaBlur);
    #else
      summaryEmissive *= emSkipEmissiveMap ? 1 : txEmissive.Sample(EME_SAMPLER, uv).rgb;
      float4 maskDiffuse = txDiffuse.SampleBias(EME_SAMPLER, uv, emMaskResultDiffuseBlur);
      float4 maskNormalAlpha = txNormal.SampleBias(EME_SAMPLER, uv, emMaskResultNormalAlphaBlur);
    #endif
    summaryEmissive *= lerp(1, pow(saturate(saturate(dot(maskDiffuse.rgb, 1)) * maskDiffuse.a), 1 + emMaskResultDiffuseBlur), saturate(emMaskResultDiffuseBlur));
    summaryEmissive *= lerp(1, pow(saturate(maskNormalAlpha.a), 1 + emMaskResultNormalAlphaBlur), saturate(emMaskResultNormalAlphaBlur));
    summaryEmissive *= lerp(diffuseValue.rgb, 1, saturate(emSkipDiffuseMap));
    return summaryEmissive;
  }
#endif
