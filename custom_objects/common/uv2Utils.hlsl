#define CUSTOM_STRUCT_FIELDS\
  float Tex2X : TEXCOORD20;\
  float Tex2Y : TEXCOORD21;

#define PREPARE_UV2\
  vout.Tex2X = f16tof32(asuint(vin.TangentPacked.y));\
  vout.Tex2Y = f16tof32(asuint(vin.TangentPacked.y) >> 16)

#define PIN_TEX2 float2(pin.Tex2X, pin.Tex2Y)