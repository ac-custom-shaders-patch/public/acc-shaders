#define GETSHADOW_BIAS_MULTIPLIER 0.1

#ifdef SHTOON_HUMAN_SURFACE

  #define CB_MATERIAL_EXTRA_1\
    float3 toonOutlineColor;\
    float toonOutlineAlpha; /* 0.8 */\
    float toonRadiusMult;\
    float toonRangeMin; /* 0.008 */\
    float toonRangeMax; /* 0.018 */\
    float toonShadowSaturation; /* 2 */\

#else

  #define CB_MATERIAL_EXTRA_1\
    float3 toonOutlineColor;\
    float toonOutlineAlpha; /* 0.8 */\
    float toonRadiusMult;\
    float toonRangeMin; /* 0.008 */\
    float toonRangeMax; /* 0.018 */\
    float toonShadowSaturation; /* 2 */\
    float extSubsurfaceScatteringIntensity; /* 0.05 */\
    float extHairRimBrightness; /* 0.2 */\

#endif

#define SUMMARIZE_DIFFUSE_COMPONENT(X, Y, Z)\
  (X) * lerp(dot(Y, 1/3.), Y, lerp(toonShadowSaturation, 1, GAMMA_OR(0.5 + 0.5 * Z, Z)))
#define LIGHTINGFX_SUMMARIZE_DIFFUSE_COMPONENT(X, Y, Z)\
  SUMMARIZE_DIFFUSE_COMPONENT(X, Y, Z)

#include "include_new/base/_include_ps.fx"
#include "include_new/ext_functions/depth_map.fx"

#ifdef ALLOW_EXTRA_VISUAL_EFFECTS
  #define SHTOON_SSS \
    float brightnessK = 1 - abs(txMapsValue.w * 2 - 1);\
    lighting += ksLightColor.rgb * float3(1, 0.2, 0) * extSubsurfaceScatteringIntensity\
      * shadow * brightnessK\
      * saturate(remap(dot(normalW, -ksLightDirection.xyz), -0.1, -0.4, 1, 0))\
      * lerp(0.4, 1, saturate(-dot(ksLightDirection.xyz, toCamera)));\
    float hairArea = max(txMapsValue.w * 2 - 1, 0);\
    float rim = pow(saturate(1 - abs(dot(toCamera, normalW))), 3) * AO_LIGHTING.g * hairArea * saturate(dot(txDiffuseValue.rgb, 2)) * extHairRimBrightness;\
    lighting += rim * SAMPLE_REFLECTION_FN(toCamera, 5, false, REFL_SAMPLE_PARAM_DEFAULT);\
    lighting += rim * saturate(dot(-ksLightDirection.xyz, toCamera)) * saturate(dot(-ksLightDirection.xyz, normalW) * 2 + 0.5) * ksLightColor.xyz * shadow;

  #define SHTOON_OUTLINE \
    if (transparentOutlinePass && toonOutlineAlpha){\
      float toon = 0;\
      float n = linearizeAccurate(pin.PosH.z);\
      float2 dd = float2(ddx(n), ddy(n));\
      float M = 1 / (1 + length(pin.PosC) / max(1, extCameraTangent) * 0.2);\
      float R = toonRadiusMult * M;\
      for (int y = -1; y <= 1; ++y){\
        for (int x = -1; x <= 1; ++x){\
          if (x == 0 && y == 0) continue;\
          float d = linearizeAccurate(txDepth.Sample(samLinear, (pin.PosH.xy + float2(x, y) * R) * extScreenSize.zw));\
          float f = d - (n + dd.x * x + dd.y * y);\
          toon = max(toon, M * lerpInvSat(f, toonRangeMin, toonRangeMax));\
        }\
      }\
      withReflection = float4(toonOutlineColor * AMBIENT_COLOR_NEARBY, toon * toonOutlineAlpha);\
      clip(withReflection.a - 0.1);\
    }
#else
  #define SHTOON_SSS 
  #define SHTOON_OUTLINE 
#endif

