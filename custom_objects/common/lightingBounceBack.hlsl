#include "include_new/base/_flags.fx"

#define USE_BOUNCEBACK
#ifdef ALLOW_BOUNCEBACK_LIGHTING
  #ifdef BOUNCEBACK_EXTERNALMULT
    #define BOUNCEBACK_COREMULT 1
  #else
    #define CB_MATERIAL_EXTRA_4\
      float extBounceBack /* = 2 */;
    #define BOUNCEBACK_COREMULT abs(extBounceBack)
  #endif
  
  #define LIGHTINGFX_BOUNCEBACK
  #define LIGHTINGFX_BOUNCEBACK_EXP GAMMA_OR_STAT(400, 80)

  #define BOUNCEBACKFX_DIFFUSEMASK (extBounceBack < 0 ? 1 - txDiffuseValue.a : 1)
  #define BOUNCEBACKFX(lighting, mask)\
    float3 bounceBackMult = BOUNCEBACK_COREMULT * (mask) * GAMMA_OR(GAMMA_LINEAR(2 * txDiffuseValue.rgb), txDiffuseValue.rgb);\
    lighting += calculateBounceBackLighting(toCamera, normalW, txDiffuseValue.rgb, bounceBackMult, shadow, LIGHTINGFX_BOUNCEBACK_EXP);
#else
  #ifndef BOUNCEBACK_EXTERNALMULT
    #define CB_MATERIAL_EXTRA_4\
      float extBounceBack /* = 2 */;
  #endif
  #define BOUNCEBACKFX_DIFFUSEMASK 0
  #define BOUNCEBACKFX(lighting, mask) float3 bounceBackMult = 0;
#endif