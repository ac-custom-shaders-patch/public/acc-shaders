cbuffer cbBrakeDiscFX : register(b11) {
  float3 extWheelPos;
  float extNoiseOffset;

  float3 extWheelNormal;
  float extBrakeDiscRadius;

  float3 extWheelUp;
  float extBrakeDiscInnerRadius;

  float3 extWheelSide;
  float _pad0;

  float extRimRadius;
  float extRimHeight;
  float extGlowMult;
  float _pad;

  float extAlphaAsMaskMul;
  float extAlphaAsMaskAdd;
  float extLuminocityAsMaskMul;
  float extLuminocityAsMaskAdd;

  float extNormalAsMaskMul;
  float extNormalAsMaskAdd;
  float extF0Base;
  float extF0Worn;
  
  float extEXPBase;
  float extEXPWorn;
  float extCarvedPattern;
  float extCeramicBrakes;

  float extDebugMode;
  float extOverrideNormalMap;
  float extOverrideDiffuseMap;
  float extGlowOffset;

  float extSpecularBase;
  float extSpecularWorn;
  float extRoughnessX;
  float extRoughnessY;

  float extCutsPattern;
  float extCutsAspectRatio;
  float extCutsWidth;
  float extCutsYExp;

  float2 extCutsP0;
  float2 extCutsP1;

  float extRimReflectionOffset;
  float extRimReflectionTyreRadiusInv;
}

cbuffer cbBrakeDiscFXDynamic : register(b13) {
  float extWorn;
  float extTemperature;
}

struct PS_IN_BrakeDiscFX {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  float3 WheelCenterC : POSITION0;
  float3 WheelNormalW : POSITION1;
  float3 WheelUpW : POSITION2;
  float3 WheelSideW : POSITION3;
  float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  #ifndef NO_NORMALMAPS
    float3 TangentW : TEXCOORD6;
    float3 BitangentW : TEXCOORD7;
  #endif
  float Fog : TEXCOORD8;
  AO_NORMALS
};

#if defined(TARGET_PS) || defined(BDFX_TARGET_VS_OVERRIDE)

  #ifndef BDFX_SAMPLER
    #define BDFX_SAMPLER samLinear
  #endif

  #include "include_new/base/textures_ps.fx"
  #include "include/samplers.hlsl"
  Texture2D<float> _txRimMask : register(TX_SLOT_MAT_5);

  float posWithinCircle(float3 delta, float3 wheelNormal) {
    return length(delta - wheelNormal * dot(delta, wheelNormal));
  }

  float sdfLine(float2 p, float2 a, float2 b, float2 ara) {
    float2 ba = (b - a) * ara;
    float2 pa = (p - a) * ara;
    return length(pa - saturate(dot(pa, ba) / dot(ba, ba)) * ba);
  }

  float2 distanceToLine(float2 p, float2 a, float2 b, float2 ara) {
    float2 ba = (b - a) * ara;
    float2 pa = (p - a) * ara;
    return pa - saturate(dot(pa, ba) / dot(ba, ba)) * ba;
  }

  float diskAngle(float3 delta, float3 wheelNormal, float3 wheelUp){
    float3 p = delta - wheelNormal * dot(delta, wheelNormal);
    float3 mdir0 = normalize(wheelUp);
    float3 mdir1 = normalize(cross(mdir0, wheelNormal));
    return M_PI + atan2(dot(p, mdir0), dot(p, mdir1));
  }

  float carvedShape(float diskAngle) {  
    #ifdef TARGET_PS
      float angle = diskAngle * extCarvedPattern;
      return frac(angle) < fwidth(angle);
    #else
      return 0;
    #endif
  }

  float cutsShape(float diskArea, float diskAngle, out float3 delta) {
    float2 aspectRatioAdjustment = float2(1, extCutsAspectRatio);
    delta.xy = distanceToLine(float2(frac(diskAngle * extCutsPattern), pow(diskArea, extCutsYExp)), extCutsP0, extCutsP1, aspectRatioAdjustment);
    delta.z = saturate(remap(length(delta.xy), 0, extCutsWidth, 1, 0));
    return delta.z > 0;
  }

  void cutsShapeNM(inout float3 normalW, float3 delta, float3 wheelNormal, float3 wheelUp){
    float3 mdir0 = normalize(wheelUp);
    float3 mdir1 = normalize(cross(mdir0, wheelNormal));
    delta.xy = normalize(delta.xy) * pow(1 - delta.z, 4) * 0.4;
    normalW += mdir0 * delta.y;
    normalW += mdir1 * delta.x;
  }

  float3 getHeatColor(float h, bool alternative) {
    if (alternative) {
      // https://www.shadertoy.com/view/dlBczm
      float t = h * 1500;
      return float3(
        pow(saturate(0.3 + 0.7 * (t - 770.) / (1040. - 770.)), 1.), 
        pow(saturate((t - 985.) / (1320. - 985.)), 1.2) * 0.98, 
        pow(saturate((t - 1160.) / (1480. - 1160.)), 0.9) * 0.96) * pow(h, 2) * 3.5;
    } else {
      float T = h * 5600;
      if (T < 400) return 0;
      float3 f = float3(1, 1.5, 2);
      float3 O = 100 * f * f * f / (exp(f * 19e3 / T) - 1);
      return 3 * O;
    }
  }

  float anisReflModel(float3 viewDir, float3 lightDir, float3 normalW, 
      float3 dirX, float3 dirY, float x, float y) {
    x = GAMMA_OR(x * 0.8, x);
    y = GAMMA_OR(y * 0.8, y);
    float3 halfwayVector = normalize(lightDir + viewDir);
    float dotLN = dot(lightDir, normalW);  
    float dotHN = dot(halfwayVector, normalW);
    float dotVN = dot(viewDir, normalW);
    float dotHTAlphaX = dot(halfwayVector, dirX) / x;
    float dotHBAlphaY = dot(halfwayVector, dirY) / y;
    return sqrt(max(0, dotLN / dotVN)) * exp(-2 * (dotHTAlphaX * dotHTAlphaX + dotHBAlphaY * dotHBAlphaY) / (1 + dotHN));
  }

  void brakeDiscFXCompute(float3 posRel, 
      float3 extWheelNormalW, float3 extWheelUpW, float3 extWheelSideW, float3 pinNormalW, float2 pinTex, 
      float baseWorn, float heatK, float rimMaskMult,
      float4 txNormalValue, float3 toCamera,
      inout float4 txDiffuseValue, inout float3 normalW, inout float2 extraShadow, inout float lfxMult,
      out float3 resultGlow, out float3 specularValue, out float reflectionOcclusion, out float3 reflDir, 
      out float brushedK, out float mapShading, out float cleanK, out float cleanK2, out float withinDisk, out float reflMask){
    float withinCircle = posWithinCircle(posRel, extWheelNormalW);
    float angle = diskAngle(posRel, extWheelNormalW, extWheelUpW);

    float holeValue = saturate(txDiffuseValue.a * extAlphaAsMaskMul + extAlphaAsMaskAdd);
    float mapNormalsValue = withinCircle < extBrakeDiscRadius - 0.01 ? saturate(length(txNormalValue.xy * 2 - 1) * extNormalAsMaskMul + extNormalAsMaskAdd) : 1;
    float mapValue = saturate(dot(txDiffuseValue.rgb, 0.33) * extLuminocityAsMaskMul + extLuminocityAsMaskAdd);
    mapValue *= mapNormalsValue;

    float diskArea = saturate((withinCircle - extBrakeDiscInnerRadius) / (extBrakeDiscRadius - extBrakeDiscInnerRadius));
    float3 cutsDelta = 0;

    [branch]
    if (extCutsPattern){
      mapValue *= 1 - cutsShape(diskArea, angle, cutsDelta);
    }

    float3 dir0 = normalize(cross(extWheelNormalW, posRel));
    float3 dir1 = normalize(cross(extWheelNormalW, dir0));

    withinDisk = (withinCircle > extBrakeDiscInnerRadius) * pow(dot(pinNormalW, extWheelNormalW), 4);

    float diskEdge = (withinCircle > (extBrakeDiscRadius + extBrakeDiscInnerRadius) / 2) 
      * lerpInvSat(abs(dot(pinNormalW, extWheelNormalW)), 0.1, 0.05);

    float glowArea = diskArea;
    glowArea = 1 - abs(glowArea * (1 + extGlowOffset) - extGlowOffset);
    glowArea *= 1.1;
    float glowAreaUnclamped = glowArea;
    glowArea = saturate(glowArea);

    float wearGradient = glowArea;

    #ifdef TARGET_VS
      float4 noiseBase = txNoise.SampleLevel(samLinearSimple, extNoiseOffset + pinTex * 0.4, 1);
      float4 noiseCircle = txNoise.SampleLevel(BDFX_SAMPLER, extNoiseOffset + float2(withinCircle * 37, withinCircle * 13), 1);
      float4 noiseCircleChunk = txNoise.SampleLevel(BDFX_SAMPLER, extNoiseOffset + float2(withinCircle * 7, withinCircle * 5), 1);
    #else
      float4 noiseBase = txNoise.Sample(samLinearSimple, extNoiseOffset + pinTex * 0.4);
      float4 noiseCircle = txNoise.Sample(BDFX_SAMPLER, extNoiseOffset + float2(withinCircle * 37, withinCircle * 13));
      float4 noiseCircleChunk = txNoise.Sample(BDFX_SAMPLER, extNoiseOffset + float2(withinCircle * 7, withinCircle * 5));
    #endif

    float groove = lerp(1, noiseCircle.x, mapValue);
    float grooveLarge = lerp(1, (saturate(remap(noiseCircleChunk.x, 0.45, 0.55, 0, 1))
      + saturate(remap(noiseCircleChunk.y, 0.45, 0.55, 0, 1))) / 2, mapValue);
    float wearK = lerp(baseWorn * 0.4, 1, saturate(wearGradient + baseWorn * 2 - 1 + lerp(-0.4, 0.4, noiseBase.x * noiseBase.y) * baseWorn) * groove);
    mapShading = lerp(mapValue, 1, pow(1 - wearK, 2) * 0.8);

    float ceramicTexture = lerp(dot(txNoise.Sample(BDFX_SAMPLER, pinTex * 13).xyz 
        * txNoise.Sample(BDFX_SAMPLER, (pinTex + 0.5) * 21).xyz, 0.33), 0.1 + dot(noiseCircle, 0.07), blurLevel) * 0.8 + 0.2;
      
    float ceramic = withinDisk * extCeramicBrakes;    
    brushedK = withinCircle > extBrakeDiscInnerRadius ? withinDisk * lerp(1, mapValue, baseWorn) : 0;
    float2 roughness = lerp(0.22, float2(extRoughnessX, extRoughnessY), withinDisk * mapValue);

    wearK = lerp(0, wearK, withinDisk);
    wearK = lerp(groove * baseWorn / 2, 1, wearK);
    cleanK = 1 - wearK;
    cleanK2 = pow(cleanK, 2);
    float cleanKS = 1 - pow(wearK, 2);

    float carvedBase = withinDisk * cleanK2 * cleanK2 * mapValue;
    float carved = 0;
    [branch] 
    if (extCarvedPattern) {
      carved = carvedShape(angle) * carvedBase * (1 - blurLevel);
    }

    roughness *= lerp(0.2, 1, cleanK);
    ceramicTexture *= lerp(groove * 0.7 + grooveLarge * 0.3, 1, cleanK);

    #ifdef TARGET_PS
      [branch] 
      if (extOverrideNormalMap) {
        normalW = lerp(normalW, extWheelSideW, withinDisk * mapNormalsValue);
        if (cutsDelta.z > 0) {
          cutsShapeNM(normalW, cutsDelta, extWheelSideW, extWheelUpW);
        }
        normalW = lerp(normalW, 
          normalize(extWheelSideW * noiseCircle.x + dir0 * (noiseCircle.y - 0.5) + dir1 * (noiseCircle.z - 0.5)),
          brushedK * wearK * extOverrideNormalMap * 0.01);
        normalW = normalize(normalW);
      }

      [branch] 
      if (extOverrideDiffuseMap) {
        float3 baseColor = lerp(lerp(0.05, 0.1, cleanK2), ceramicTexture * 0.6, ceramic) * extOverrideDiffuseMap;
        txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, baseColor * lerp(0.9, 1, mapValue) * lerp(1, 0.9, carved) * holeValue, withinDisk);
      }
    #endif

    roughness = lerp(roughness, lerp(0, 0.2, ceramicTexture), ceramic * mapValue);
    roughness = lerp(roughness, 0.2, carvedBase * (1 - carved) * 0.3);
    reflMask = lerp(1, ceramicTexture, ceramic * 0.5) * lerp(1, holeValue, withinDisk);

    // float extDebugMode = 1;

    [branch]
    if (extDebugMode) {
      if (withinCircle < extBrakeDiscInnerRadius) {
        txDiffuseValue.rgb = float3(0, 0, 0.5);
      } else if (withinCircle < extBrakeDiscRadius) {
        txDiffuseValue.rgb = float3(1 - mapValue, 1, mapValue) * holeValue;
      } else {
        txDiffuseValue.rgb = float3(1, 0, 1); 
      }
      if (abs(withinCircle - extBrakeDiscInnerRadius) < 0.002) {
        txDiffuseValue.rgb = float3(3, 0, 0); 
      }
      if (abs(withinCircle - extBrakeDiscRadius) < 0.002) {
        txDiffuseValue.rgb = float3(3, 0, 0); 
      }
    }

    #ifndef TARGET_PS
      txDiffuseValue = 0.1;
    #endif

    if (extTemperature){
      // float blurLevel = 0;
      // float heatK = clamp(remap(ksFOV, 60, 30, 1500, 500), 500, 1500) / 1500;
      // float heatK = 800. / 1500;
      float heatK = extTemperature;
      heatK *= diskEdge ? 0.6 : 1;
      // float grooveOffset = mapValue * lerp(0, groove * 0.1 + grooveLarge * 0.4, (1 - heatK * heatK * 0.9)) * saturate(1 - pow(saturate(glowArea * 1.2), 2));
      float heatL = heatK * lerp(1, 0.9, noiseBase.z * noiseBase.w * (1 - blurLevel));
      // glowAreaUnclamped = glowAreaUnclamped / (1 + abs(glowAreaUnclamped));
      // glowAreaUnclamped = smin(1, glowAreaUnclamped);
      // float heatF = (heatL + glowArea * 0.35 - 0.35 - grooveOffset) * lerp(lerp(0.6, 1.1, heatK), 1, mapValue);
      float heatF = (heatL - lerp(1, 0.3, pow(saturate(heatK * 2 - 1), 2)) * pow(max(0, 1 - glowArea) * lerp(0.8 + groove * 0.1, 0.6, 0), 2)) * lerp(lerp(0.6, 1.1, heatK), 1, mapValue);
      // float heatF = (heatL + glowArea * 0.35 - 0.35 - 0);
      // heatF = grooveLarge;
      // heatF = grooveOffset * 10;
      heatF = lerp(heatF, heatL * (1 - pow(txDiffuseValue.g, 2)), diskEdge);
      resultGlow = getHeatColor(heatF, true);
    } else {
      heatK *= diskEdge ? 0.6 : 1;
      float grooveOffset = mapValue * lerp(0, groove * 0.1 + grooveLarge * 0.4, (1 - heatK * heatK * 0.9)) * saturate(1 - pow(saturate(glowArea * 1.2), 2));
      float heatL = saturate(heatK * 20) * saturate(heatK * 0.7 + 0.3) * lerp(1, 0.9, noiseBase.z * noiseBase.w * (1 - blurLevel));
      float heatF = (heatL + glowArea * 0.35 - 0.35 - grooveOffset) * lerp(lerp(0.6, 1.1, heatK), 1, mapValue);
      heatF = lerp(heatF, heatL, diskEdge);
      resultGlow = getHeatColor(heatF, false);
    }
    resultGlow *= lerp(holeValue * withinDisk, 1, diskEdge);

    specularValue = anisReflModel(-toCamera, -ksLightDirection.xyz, normalW, dir0, dir1, roughness.x, roughness.y) 
      * lerp(extSpecularWorn, extSpecularBase, cleanK2) * lerp(1, pow(ceramicTexture, 2), ceramic * cleanK);

    reflDir = normalize(reflect(toCamera, normalW));
    float reflDirN = saturate(dot(reflDir, extWheelSideW));
    reflDir *= extRimHeight / reflDirN;
    float reflectedPos = posWithinCircle(posRel + reflDir, extWheelSideW);
    float shadeBlur = lerp(lerp(0.9, cleanKS, brushedK), 1.4, ceramic);
    float result = (extRimRadius - reflectedPos) / lerp(1, 10, shadeBlur) + 0.005 * shadeBlur;
    reflectionOcclusion = lerp(1, saturate(result * 150.0), extRimReflectionOffset ? 1 - rimMaskMult : 1);
    specularValue *= lerp(0.3, 1, reflectionOcclusion) * saturate(dot(normalW, -ksLightDirection.xyz) * 4) * holeValue;
    specularValue = lerp(reflectanceModel(-toCamera, -ksLightDirection.xyz, normalW, ksSpecularEXP), specularValue, withinDisk);

    if (extRimReflectionOffset && rimMaskMult > 0){
      float posY = -dot(posRel, extWheelUpW);
      float posX = -dot(posRel, normalize(cross(extWheelSideW, extWheelUpW)));
      float2 posUV = float2(posX, posY) * extRimReflectionTyreRadiusInv + 0.5;
      float mask = _txRimMask.SampleLevel(samLinearBorder0, posUV, 1.8 + blurLevel * 4);
      extraShadow = lerp(extraShadow, pow(mask, 2), rimMaskMult);
      lfxMult = lerp(1, pow(mask, 2), rimMaskMult);
    }
  }

#endif