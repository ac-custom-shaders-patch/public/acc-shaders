#include "include/common.hlsl"

#if defined(TARGET_PS) && defined(ALLOW_EXTRA_VISUAL_EFFECTS)

	float3 sampleEnv(float3 ray, float bias, float dimming){
		float dimmingMult = saturate(remap(ray.y, -1, 0.2, 0, 1));
		return SAMPLE_REFLECTION_FN(ray, bias, false, REFL_SAMPLE_PARAM_DEFAULT) * lerp(1, dimmingMult, dimming);
	}

	WindscreenRainData getWindscreenRainData(float2 uv, float3 normalL, float3 normalW, float3 toCamera, float3 cameraUp){
		float4 layerRain = _txRainWater.Sample(samLinear, uv);
		float4 layerRainBlurred = _txRainWater.SampleLevel(samLinearSimple, uv, 2.5);

		float layerBlurred = _txRainWaterBlurred.SampleLevel(samLinearSimple, uv, 0);
		float blurredBase = saturate(layerBlurred * 2) / 3;
		float blurredForced = saturate(layerBlurred * 2 - 1);

		float4 layerWiper = _txRainWiper.Sample(samLinear, uv);
		float4 layerOcclusion = _txOcclusion.SampleLevel(samLinear, uv, 1 - extOcclusionDebug);

		float actualWater = saturate(remap(layerRain.a + extSceneRain * 0.2, 0.45, 0.75, 0, 1));
		// float trail = saturate(remap(layerRain.z * lerp(1, 0.8, saturate(blurredBase * 9 - 2.5)), 0.83, 0.93, 0, 1));
		// float trail = saturate(remap(layerRain.z, 0.73, 0.93, 0, 1));
		float trail = layerRain.z;
		float water = max(actualWater, trail);

		float4 detail = _txRainDetail.Sample(samLinearSimple, uv * extDetailScale);
		float detailMult = gFadingInv;
		detailMult *= saturate(4 - 10 * max(layerRainBlurred.z * 0.9, layerRainBlurred.a));
		detailMult *= saturate(1 - layerWiper.y);
		detailMult *= saturate(1 - layerRain.z);
		detailMult *= saturate(layerOcclusion.a * 2 - 1);

		float detailBase = detail.a * detailMult;
		float dif = (detailBase * 2 - lerp(1, 0, saturate(blurredBase * 9 - 1))) - saturate(0.3 - blurredBase * 10);
		detailBase = saturate(detailBase * saturate(dif * 3) * 2);
		detail.a = max(detailBase, detail.a * blurredForced * 1.15);

		// float dif = (detail.a * 2 - lerp(1, 0, saturate(layerBlurred * 9 - 2))) - saturate(1.5 - layerBlurred * 10);
		// detail.a = saturate(detail.a * saturate(dif * 3) * 2);
		// detail.a = layerBlurred > 0.5;

		float2 normalMapValue = (layerRain.xy * 2 - 1) * lerp(0.1, 1, actualWater);
		// float2 normalMapValue = layerRain.xy * 2 - 1;
		normalMapValue = lerp(normalMapValue, detail.xy, detail.a * (1 - water));
		water = lerp(water, 1, detail.a);
		water *= saturate(layerOcclusion.a * 10);

    float facingShare = sqrt(saturate(1 - dot2(normalMapValue)));
		float3 normalX = layerOcclusion.xyz * 2 - 1;
		float3 normalY = normalize(cross(normalL, normalX));
		float3 normalZ = normalL;
		float3 normal = normalize(normalMapValue.x * normalX + normalMapValue.y * normalY + facingShare * normalZ);
		float3 normalDebug = normal;
		normal = mul(normal, (float3x3)gCarTransform);

		float wiperTrace = layerWiper.x;
		wiperTrace = lerp(wiperTrace, 1, lerp(0.6, 0.4, water) * saturate(layerBlurred * 8 - 0.8) * (1 - layerWiper.y));
		wiperTrace = saturate(wiperTrace - lerp(0.2, 0, saturate(2.5 - layerRainBlurred.a * 10) * saturate(1 - layerRain.z * 5)));

		float3 fakeCameraSide = normalize(cross(cameraUp, normalW));
		float3 fakeCameraUp = normalize(cross(normalW, fakeCameraSide));
    float2 fakeRefraction = float2(dot(fakeCameraSide, normal), dot(fakeCameraUp, normal)) * float2(-1, 1);

		WindscreenRainData ret;
    // ret.wiperOcclusion = saturate(layerWiper.w * 5 - 3);
    ret.wiperOcclusion = saturate(1 - layerRain.w * 4 * saturate(1 - layerRain.z * 4));
		ret.water = water;
		ret.trace = wiperTrace;
		ret.condensation = layerWiper.z;
		ret.facingShare = facingShare;
		ret.normal = normal;
		ret.fakeRefraction = fakeRefraction;
		ret.occlusion = lerp(0.5, 1, (0.5 + 0.5 * layerWiper.w) * saturate(layerWiper.w * 2.5 - 0.5)) * layerOcclusion.a;
		ret.refractionOcclusion = 0.95 * (0.5 + 0.5 * layerWiper.w) * layerOcclusion.a;
    ret.dirtBlur = max(layerWiper.x, layerWiper.z);
    // ret.wiperOcclusion = 1;
    // ret.occlusion = 1;
    // ret.refractionOcclusion = 1;
    ret.debugMode = extOcclusionDebug;
    ret.debugColor = layerOcclusion.xyz;
		// ret.dirtMult = saturate(1 - layerBlurred * 5) * layerWiper.z;
		ret.dirtMult = lerp(1, saturate(1 - layerBlurred * 5) * layerWiper.z, gFadingInv);

		// ret.debugColor.x = layerBlurred;
    // ret.debugMode = true;
    // ret.debugColor = float3(trail, 1 - trail, 0);
    // ret.debugMode = true;
    // ret.debugColor = float3(layerWiper.z, 1 - layerWiper.z, 0);

		ret.refractionBlur = pow(layerBlurred, 2) * lerp(3, 5, layerRain.a);
		// ret.refractionBlur = 0;
    // ret.debugMode = true;
    // ret.debugColor = float3(ret.refractionBlur, 1 - ret.refractionBlur, 0) * 3;

    return ret;
	}

#endif