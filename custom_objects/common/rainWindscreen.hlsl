#include "include/common.hlsl"
#define RAINFX_STATIC_OBJECT
#define RAINFX_STATIC_BUT_KEEP_FLEX

#define CB_MATERIAL_EXTRA_4\
	float extDetailScale;\
	float extOcclusionDebug;

#ifndef CUSTOM_STRUCT_FIELDS_EXTRA
	#define CUSTOM_STRUCT_FIELDS_EXTRA
#endif

#ifndef SIMPLIFIED_EFFECTS
	#define CUSTOM_STRUCT_FIELDS\
		CUSTOM_STRUCT_FIELDS_EXTRA\
		float2 TexAlt : TEXCOORD24;\
		float3 NormalCar : TEXCOORD25;

	cbuffer _cbData : register(b12) {
		float4x4 gCarTransform;
		float4x4 gCarTransformInv;
		float3 gCameraSide;
		uint gWindscreenFXFlags;
		float3 gCameraUp;
		float gFadingInv;
	}

	#define WINDSCREEN_FX_FLAG_USE_COLOR (gWindscreenFXFlags & 1)
	#define WINDSCREEN_FX_FLAG_WORKING_MIRRORS (gWindscreenFXFlags & 2)

	#define FILL_VS(vin, vout)\
		vout.TexAlt = vin.TangentPacked.xy;\
		vout.NormalCar = mul(vout.NormalW.xyz, (float3x3)gCarTransformInv);
#else
	#define CUSTOM_STRUCT_FIELDS\
		CUSTOM_STRUCT_FIELDS_EXTRA
	#define FILL_VS(vin, vout)
#endif

#ifdef TARGET_PS

	float3 sampleEnv(float3 ray, float bias, float dimming);

	// #ifdef ALLOW_RAINFX // TODO
		#define APPLY_RAIN
	// #endif

	#ifdef APPLY_RAIN
		Texture2D _txRainWater : register(t1);
		Texture2D _txRainDetail : register(t2);
		Texture2D _txRainWiper : register(t3);
		Texture2D _txOcclusion : register(t4);
		Texture2D<float> _txRainWaterBlurred : register(t5);
	#endif

	struct WindscreenRainData {
		float water;
		float trace;
		float condensation;
		float facingShare;
		float dirtMult;
		float3 normal;
		float2 fakeRefraction;
		float occlusion;
		float refractionOcclusion;
		float dirtBlur;
		float wiperOcclusion;
		float refractionBlur;

		bool debugMode;
		float3 debugColor;

		void applyOpaqueMask(float alpha){			
			water *= 1 - alpha;
			trace *= 1 - alpha;
		}
	};

#endif