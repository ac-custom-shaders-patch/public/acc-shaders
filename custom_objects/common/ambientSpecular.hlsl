#ifndef _AMBIENTSPECULAR_HLSL_
#define _AMBIENTSPECULAR_HLSL_

float3 getAmbientSpecular(float3 toCamera, float3 normalW, float stAmbientSpec, float stAmbientEXP){
  #ifdef BLACK_REFLECTIONS
    return 0;
  #endif
  stAmbientEXP = stAmbientEXP * GAMMA_ACTUAL_VALUE;
  stAmbientSpec = GAMMA_OR(pow(saturate(stAmbientSpec), 2), stAmbientSpec);

  float specularAmbientBase = saturate(0.5 + 0.5 * normalize(reflect(toCamera, normalW)).y);
  return pow(specularAmbientBase, stAmbientEXP) * stAmbientSpec * SAMPLE_REFLECTION_FN(reflect(toCamera, normalW), 5.5, false, REFL_SAMPLE_PARAM_DEFAULT);
}

float3 getAmbientSpecular(float3 toCamera, float3 normalW, float stAmbientSpec, float stAmbientEXP, out float finalMult){
  #ifdef BLACK_REFLECTIONS
    finalMult = 0;
    return 0;
  #endif
  stAmbientEXP = stAmbientEXP * GAMMA_ACTUAL_VALUE;
  stAmbientSpec = GAMMA_OR(pow(saturate(stAmbientSpec), 2), stAmbientSpec);

  float specularAmbientBase = saturate(0.5 + 0.5 * normalize(reflect(toCamera, normalW)).y);
  finalMult = pow(specularAmbientBase, stAmbientEXP) * stAmbientSpec;
  return finalMult * SAMPLE_REFLECTION_FN(reflect(toCamera, normalW), 5.5, false, REFL_SAMPLE_PARAM_DEFAULT);
}
 
#endif