// This version works with more commonly used height textures

#ifndef GET_HEIGHT
// #define GET_HEIGHT(UV, DX, DY) txDiffuse.SampleGrad(samLinear, UV, DX, DY).a
#define GET_HEIGHT(UV, DX, DY) txNormal.SampleGrad(samLinearSimple, UV, DX, DY).a
#endif

#ifndef parallaxScale
#define parallaxScale 0.01
#endif

float parallaxHardShadowMultiplier(in float3 L, in float2 initialTexCoord, float initialHeight, float2 dx, float2 dy, 
    float distanceMult, float random) {
  #ifndef USE_RELIEF_PARALLAX
    return 1;
  #endif

  const float maxLayers = 20;
  const float minLayers = 4;
  float currentLayerHeight = 0;

  [branch]
  if (L.z > 0) {
    float numSamplesUnderSurface = 0;
    float numLayers = lerp(minLayers, maxLayers, (1 - saturate(L.z)) * distanceMult);

    float boostMult = sign(parallaxScale) * max(parallaxScale < 0 ? initialHeight : 1 - initialHeight, 0.2);
    float layerHeight = boostMult / numLayers;
    float2 texStep = boostMult * -parallaxScale * L.xy / L.z / numLayers;

    currentLayerHeight = initialHeight + layerHeight;
    float2 currentTextureCoords = initialTexCoord + texStep * (1 + random);
    float heightFromTexture = GET_HEIGHT(currentTextureCoords, dx, dy);

    while (currentLayerHeight > 0 && currentLayerHeight < 1) {
      if (parallaxScale < 0 ? heightFromTexture < currentLayerHeight : heightFromTexture > currentLayerHeight) {
        currentLayerHeight = -1;
        break;
      }
      currentLayerHeight += layerHeight;
      currentTextureCoords += texStep;
      heightFromTexture = GET_HEIGHT(currentTextureCoords, dx, dy);
    }
  }
  return currentLayerHeight != -1;
}

float parallaxSoftShadowMultiplier(in float3 L, in float2 initialTexCoord, float initialHeight, float2 dx, float2 dy, 
    float distanceMult, float randomValue) {
  #ifndef USE_RELIEF_PARALLAX
    return 1;
  #endif

  const float maxLayers = 20;
  const float minLayers = 8;
  float currentLayerHeight = 0;
  float shadowMultiplier = 1;

  [branch]
  if (L.z > 0) {
    float numSamplesUnderSurface = 0;
    float numLayers = lerp(minLayers, maxLayers, (1 - saturate(L.z)) * distanceMult);

    float boostMult = sign(parallaxScale) * max(parallaxScale < 0 ? initialHeight : 1 - initialHeight, 0.2);
    float layerHeight = boostMult / numLayers;
    float2 texStep = boostMult * -parallaxScale * L.xy / max(L.z, 0.25) / numLayers;

    currentLayerHeight = initialHeight + layerHeight;
    float2 currentTextureCoords = initialTexCoord + texStep * (1 + randomValue);
    float heightFromTexture = GET_HEIGHT(currentTextureCoords, dx, dy);

    while (currentLayerHeight > 0 && currentLayerHeight < 1) {
      float dif = sign(parallaxScale) * (heightFromTexture - currentLayerHeight);
      if (dif > 0) {
        shadowMultiplier = lerp(shadowMultiplier, 0, saturate(dif * 8));
      }
      currentLayerHeight += layerHeight;
      currentTextureCoords += texStep;
      heightFromTexture = GET_HEIGHT(currentTextureCoords, dx, dy);
    }
  }
  return shadowMultiplier;
}

float2 parallaxMult(float3 V){
  return pow(saturate(V.z), 0.2) * parallaxScale * V.xy / (V.z + 0.1);
}

#ifdef USE_BASIC_PARALLAX
  float2 parallaxMap(float2 vUv, float3 V, float2 dx, float2 dy, float distanceMult, out float height) {
    float initialHeight = GET_HEIGHT(vUv, dx, dy);
    float2 texCoordOffset = parallaxMult(V) * distanceMult * initialHeight;
    height = initialHeight;
    return vUv - texCoordOffset;
  }
#else
  float2 parallaxMap(float2 vUv, float3 V, float2 dx, float2 dy, float distanceMult, out float height) {
    #ifdef PARALLAX_MAX_LAYERS
      const float maxLayers = PARALLAX_MAX_LAYERS;
      const float minLayers = PARALLAX_MIN_LAYERS;
    #else
      const float maxLayers = 20;
      const float minLayers = 4;
    #endif
  
    float numLayers = lerp(minLayers, maxLayers, (1 - saturate(V.z)) * distanceMult);
    float layerHeight = sign(parallaxScale) / numLayers;
    float currentLayerHeight = parallaxScale < 0 ? 0 : 1.0;
    float2 dtex = sign(parallaxScale) * parallaxMult(V) * distanceMult / numLayers;
    float2 currentTextureCoords = vUv;
    if (parallaxScale > 0) currentTextureCoords -= dtex * numLayers;
    float heightFromTexture = GET_HEIGHT(vUv, dx, dy);

    for (int i = 0; i < numLayers; i += 1) {
      if (parallaxScale < 0 ? heightFromTexture <= currentLayerHeight : heightFromTexture >= currentLayerHeight) {
        break;
      }

      currentLayerHeight -= layerHeight;
      currentTextureCoords += dtex;
      heightFromTexture = GET_HEIGHT(currentTextureCoords, dx, dy);
    }

    #ifdef USE_STEEP_PARALLAX

      height = currentLayerHeight;
      return currentTextureCoords;
      
    #elif defined( USE_RELIEF_PARALLAX )
      float2 deltaTexCoord = dtex / 2.0;
      float deltaHeight = layerHeight / 2.0;
      currentTextureCoords -= deltaTexCoord;
      currentLayerHeight += deltaHeight;
      #ifdef PARALLAX_NUM_SEARCHES
        const int numSearches = PARALLAX_NUM_SEARCHES;
      #else
        const int numSearches = 5;
      #endif
      if (parallaxScale < 0) { deltaTexCoord = -deltaTexCoord; deltaHeight = -deltaHeight; }
      for (int j = 0; j < numSearches; j += 1 ) {
        deltaTexCoord /= 2.0;
        deltaHeight /= 2.0;
        heightFromTexture = GET_HEIGHT(currentTextureCoords, dx, dy);
        if (heightFromTexture > currentLayerHeight) {
          currentTextureCoords -= deltaTexCoord;
          currentLayerHeight += deltaHeight;
        } else {
          currentTextureCoords += deltaTexCoord;
          currentLayerHeight -= deltaHeight;
        }
      }
      height = currentLayerHeight;
      return currentTextureCoords;
    #elif defined( USE_OCCLUSION_PARALLAX )
      float2 prevTCoords = currentTextureCoords + dtex;
      float nextH = heightFromTexture - currentLayerHeight;
      float prevH = GET_HEIGHT(prevTCoords, dx, dy) - currentLayerHeight + layerHeight;
      float weight = nextH / ( nextH - prevH );
      height = currentLayerHeight;
      return prevTCoords * weight + currentTextureCoords * ( 1.0 - weight );
    #else
      return vUv;
    #endif
  }
#endif
