// This version works with even more commonly used displacement textures going both ways, with grey for neutral

#ifndef PARALLAX_EXTRA_ARGS_DECL
  #define PARALLAX_EXTRA_ARGS_DECL
  #define PARALLAX_EXTRA_ARGS_PASS
#endif

float parallaxHeight(float2 uv, float2 dx, float2 dy PARALLAX_EXTRA_ARGS_DECL);

float parallaxHardShadowMultiplier(in float3 L, in float2 initialTexCoord, float initialHeight, float2 dx, float2 dy, 
    float distanceMult, float random PARALLAX_EXTRA_ARGS_DECL) {
  #ifndef USE_RELIEF_PARALLAX
    return 1;
  #endif

  const float maxLayers = 20;
  const float minLayers = 4;
  float currentLayerHeight = 0;

  [branch]
  if (L.z > 0) {
    float numSamplesUnderSurface = 0;
    float numLayers = lerp(minLayers, maxLayers, saturate(2 - L.z) * distanceMult);

    float boostMult = max(1 - initialHeight, 0.2);
    float layerHeight = boostMult / numLayers;
    float2 texStep = boostMult * -IN_PARALLAX_SCALE * L.xy / L.z / numLayers;

    currentLayerHeight = initialHeight + layerHeight;
    float2 currentTextureCoords = initialTexCoord + texStep * (1 + random);
    float heightFromTexture = parallaxHeight(currentTextureCoords, dx, dy PARALLAX_EXTRA_ARGS_PASS);

    while (currentLayerHeight < 1) {
      if (heightFromTexture > currentLayerHeight) {
        currentLayerHeight = -1;
        break;
      }
      currentLayerHeight += layerHeight;
      currentTextureCoords += texStep;
      heightFromTexture = parallaxHeight(currentTextureCoords, dx, dy PARALLAX_EXTRA_ARGS_PASS);
    }
  }
  return currentLayerHeight != -1;
}

float parallaxSoftShadowMultiplier(in float3 L, in float2 initialTexCoord, float initialHeight, float2 dx, float2 dy, 
    float distanceMult, float randomValue PARALLAX_EXTRA_ARGS_DECL) {
  #ifndef USE_RELIEF_PARALLAX
    return 1;
  #endif

  const float maxLayers = 20;
  const float minLayers = 8;
  float currentLayerHeight = 0;
  float shadowMultiplier = 1;

  [branch]
  if (L.z > 0) {
    float numSamplesUnderSurface = 0;
    float numLayers = lerp(minLayers, maxLayers, (1 - saturate(L.z)) * distanceMult);

    float boostMult = max(1 - initialHeight, 0.2);
    float layerHeight = boostMult / numLayers;
    float2 texStep = boostMult * -IN_PARALLAX_SCALE * L.xy / max(L.z, 0.1) / numLayers;

    currentLayerHeight = initialHeight + layerHeight;
    float2 currentTextureCoords = initialTexCoord + texStep * (1 + randomValue);
    float heightFromTexture = parallaxHeight(currentTextureCoords, dx, dy PARALLAX_EXTRA_ARGS_PASS);

    while (currentLayerHeight < 1) {
      float dif = heightFromTexture - currentLayerHeight;
      if (dif > 0) {
        shadowMultiplier = lerp(shadowMultiplier, 0, saturate(dif * 8));
      }
      currentLayerHeight += layerHeight;
      currentTextureCoords += texStep;
      heightFromTexture = parallaxHeight(currentTextureCoords, dx, dy PARALLAX_EXTRA_ARGS_PASS);
    }
  }
  return shadowMultiplier;
}

#ifdef USE_BASIC_PARALLAX
  float2 parallaxMap(float2 vUv, float3 V, float2 dx, float2 dy, float distanceMult, out float height PARALLAX_EXTRA_ARGS_DECL) {
    float initialHeight = parallaxHeight(vUv, dx, dy PARALLAX_EXTRA_ARGS_PASS);
    float2 texCoordOffset = distanceMult * initialHeight;
    height = initialHeight;
    return vUv - texCoordOffset;
  }
#else
  float2 parallaxMap(float2 vUv, float3 V, float2 dx, float2 dy, float distanceMult, out float height PARALLAX_EXTRA_ARGS_DECL) {
    #ifdef PARALLAX_MAX_LAYERS
      const float maxLayers = PARALLAX_MAX_LAYERS;
      const float minLayers = PARALLAX_MIN_LAYERS;
    #else
      const float maxLayers = 20;
      const float minLayers = 4;
    #endif
  
    float numLayers = max(1, ceil(lerp(0.2, 1, saturate(2 - V.z * 2) * distanceMult) * IN_PARALLAX_STEPS));
    float layerHeight = 1. / numLayers;
    float currentLayerHeight = 1;
    float2 dtex = IN_PARALLAX_SCALE * V.xy / V.z / numLayers;
    float2 currentTextureCoords = vUv - dtex * numLayers * IN_PARALLAX_OFFSET;
    float heightFromTexture = parallaxHeight(vUv, dx, dy PARALLAX_EXTRA_ARGS_PASS);

    for (int i = 0; i < numLayers; i += 1) {
      if (heightFromTexture >= currentLayerHeight) {
        break;
      }

      currentLayerHeight -= layerHeight;
      currentTextureCoords += dtex;
      heightFromTexture = parallaxHeight(currentTextureCoords, dx, dy PARALLAX_EXTRA_ARGS_PASS);
    }

    #ifdef USE_STEEP_PARALLAX

      height = currentLayerHeight;
      return currentTextureCoords;
      
    #elif defined( USE_RELIEF_PARALLAX )
      float2 deltaTexCoord = dtex / 2.;
      float deltaHeight = layerHeight / 2.;
      currentTextureCoords -= deltaTexCoord;
      currentLayerHeight += deltaHeight;
      #ifdef PARALLAX_NUM_SEARCHES
        const int numSearches = PARALLAX_NUM_SEARCHES;
      #else
        const int numSearches = 5;
      #endif
      for (int j = 0; j < numSearches; j += 1 ) {
        deltaTexCoord /= 2.;
        deltaHeight /= 2.;
        heightFromTexture = parallaxHeight(currentTextureCoords, dx, dy PARALLAX_EXTRA_ARGS_PASS);
        if (heightFromTexture > currentLayerHeight) {
          currentTextureCoords -= deltaTexCoord;
          currentLayerHeight += deltaHeight;
        } else {
          currentTextureCoords += deltaTexCoord;
          currentLayerHeight -= deltaHeight;
        }
      }
      height = currentLayerHeight;
      return currentTextureCoords;
    #elif defined( USE_OCCLUSION_PARALLAX )
      float2 prevTCoords = currentTextureCoords - dtex;
      float nextH = heightFromTexture - currentLayerHeight;
      float prevH = parallaxHeight(prevTCoords, dx, dy PARALLAX_EXTRA_ARGS_PASS) - currentLayerHeight - layerHeight;
      float weight = nextH / ( nextH - prevH );
      height = currentLayerHeight;
      return prevTCoords * weight + currentTextureCoords * ( 1.0 - weight );
    #else
      return vUv;
    #endif
  }
#endif
