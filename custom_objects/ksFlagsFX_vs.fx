#define SUPPORTS_COLORFUL_AO
#define INCLUDE_FLAGS_CB
#include "include_new/base/_include_vs.fx"
#include "common/flagsFX.hlsl"

GENERIC_RETURN_TYPE(PS_IN_PerPixel) main(VS_IN vin SPS_VS_ARG) {
  float4 vinPosL = vin.PosL;
  float3 vinNormalL = vin.NormalL;

  float aoHeight = 1;
  #ifdef MODE_KUNOS
    vin.PosL.xyz += kunosWave(vin.Tex);
  #else
    if (HAS_FLAG(FLAG_MATERIAL_2)) {
      PREPARE_AO(aoHeight);
      customWaveTent(vin.PosL.xyz, vin.NormalL, vin.TangentPacked.y, ORIGINAL_AO_VALUE);
    } else {
      float flagWidth = length(vin.NormalL) - 1000.0;
      if (abs(flagWidth) > 20) flagWidth = 0;
      float2 extraData = FFX_loadVector(asuint(vin.TangentPacked.x));
      customWaveNm(vin.Tex, vin.PosL.xyz, vin.NormalL, float2(flagWidth, extraData.y), vin.TangentPacked.y, vin.TangentPacked.z,
        extWindVel, extWindSpeed, extWindWave);
      aoHeight = extraData.x;
    }
  #endif

  GENERIC_PIECE(PS_IN_PerPixel);
  GENERIC_PIECE_MOTION(vin.PosL);
  #ifdef ALLOW_PERVERTEX_AO
    vout.Ao = aoHeight.x;
  #endif

  SPS_RET(GENERIC_PIECE_RETURN(vout));
}
