#define INCLUDE_PARTICLE_CB
#define NO_CARPAINT
#define NO_EXTAMBIENT
// #define SHADOWS_FILTER_SIZE 2
// #define LIGHTINGFX_SIMPLEST
// #define LIGHTINGFX_STATIC_PARTICLES
#define LIGHTINGFX_NOSPECULAR
#define LIGHTINGFX_KSDIFFUSE (ksDiffuse * 0.32 / 0.035)
#define LIGHTINGFX_TXDIFFUSE_COLOR txDiffuseValue.rgb
// #define SUPPORTS_AO

#define ALLOW_BRAKEDISCFX
#define ADVANCED_FAKE_SHADOWS
#define ALLOW_EXTRA_LIGHTING_MODELS
// #define ALLOW_EXTRA_SHADOW
#define ALLOW_TREE_SHADOWS
#define BLUR_SHADOWS
#define WINDSCREEN_RAIN
#define CUSTOM_STRUCT_FIELDS float RandomTint : COLOR;

#include "include_new/base/_include_ps.fx"

float3 getNormalW(float4 txNormalValue, float3 normalW, float3 tangentW, float3 bitangentW, bool objectSpace){
  float3 T = tangentW;
  float3 B = bitangentW;
  float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
  float3x3 m = float3x3(T, normalW, B);
  return normalize(mul(transpose(m), txNormalAligned.xzy));
}

RESULT_TYPE main(PS_IN_PerPixelCustom pin) {
  float3 color;
  float alpha;

  float3 toCamera = normalize(pin.PosC);
  float3 normalW = pin.NormalW;

  #ifdef LITE_VERSION
    float shadow = pin.Ao;
  #else
    float shadow = getShadow(pin.PosC, pin.PosH, float3(0, 1, 0), SHADOWS_COORDS, 1);
  #endif

  #ifdef MODE_SHADOWS_ADVANCED
    float4 txDiffuseValue = txDiffuse.SampleBias(samLinearSimple, pin.Tex, -2);
    color = 0;
  #else
    float4 txDiffuseValue = txDiffuse.Sample(samLinearSimple, pin.Tex) * float4(pin.RandomTint.xxx, 1);
    float4 txNormalValue = txNormal.Sample(samLinearSimple, pin.Tex);

    bool hasNmMap = dot(txNormalValue, 1) != 0;
    float3 nX = normalize(pin.NormalW);
    float3 nY = float3(0, 1, 0);
    float3 nZ = normalize(cross(nY, nX));

    // normalW = normalize(normalW + toCamera * 0.5);
    if (hasNmMap) {
      normalW = getNormalW(txNormalValue, nZ, nX, nY, false);
      normalW = normalize(lerp(nZ, normalW, 1 + 10 * saturate(2 * (1 - dot(nZ, normalW)))));
    } else {
      float a0 = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 4.5).w;
      float a1 = txDiffuse.SampleLevel(samLinearSimple, pin.Tex - float2(0.01, 0), 4.5).w;
      float a2 = txDiffuse.SampleLevel(samLinearSimple, pin.Tex - float2(0, 0.01), 4.5).w;
      txNormalValue.rgb = 0.5 + float3(a0 - a1, a0 - a2, 0.5) * float3(4, 4, 1);
      normalW = getNormalW(txNormalValue, nZ, nX, nY, false);
      // normalW = float3(0, 1, 0);
    }

    float dotValue = pow(saturate(dot(normalW, -ksLightDirection.xyz) * 0.5 + 0.5), 1.8);
    float3 light = ksLightColor.rgb * dotValue;
    float3 ambient = max(0, getAmbientBaseAt(pin.PosC, normalW, AO_LIGHTING) * (hasNmMap ? txNormalValue.w : 1));
    float diffuseMult = ksDiffuse * 0.32 / 0.035;
    float ambientMult = ksAmbient * 0.42 / 0.1;
    color = applyGamma(light * shadow * GAMMA_OR(1, diffuseMult), ambient * GAMMA_OR(1, ambientMult), txDiffuseValue.rgb, diffuseMult, ambientMult);
    txDiffuseValue.rgb *= hasNmMap ? 1 : 0.8;
    txDiffuseValue.a = (txDiffuseValue.a - 0.5) / max(fwidth(txDiffuseValue.a), 0.0001) + 0.5;

    // if (frac(ksGameTime / 1e3) > 0.99)
    //   color = 1e3;

    // #ifndef LITE_VERSION
      LIGHTINGFX(color);
    // #endif

    // color = max(color, 0);
    // color = normalW;

    // color = txDiffuseValue.rgb;

  #endif

  alpha = txDiffuseValue.a;
  clip(alpha - 0.01);
  RETURN_BASE(color, alpha);
}
