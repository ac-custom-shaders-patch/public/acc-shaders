#define CUSTOM_MATERIAL_PROPERTIES
#define CARPAINT_SIMPLE
#define SUPPORTS_AO
#define RAINFX_STATIC_OBJECT

static const float isAdditive = 1;
static const float fresnelEXP = 1;
static const float fresnelC = 0;
static const float fresnelMaxLevel = 0;
static const float seasonAutumn = 0;
static const float seasonWinter = 0;
static const float extColoredReflectionNorm = 0;
static const float extColoredReflection = 0;

#define CUSTOM_STRUCT_FIELDS\
  float Depth : TEXCOORD20;\
  float3 FlowDir : TEXCOORD21;

#define CB_MATERIAL_EXTRA_3\
  float waterOffset; /* 0.04 */\
  float waterIntensityMult; /* 2 */\
  float waterWaterMult; /* 1.6 */\
  float flowSpeed0; /* 0.0001 */\
  float flowSpeed1; /* 0.00017 */\
  float fadeStart; /* 1.86 */\
  float fadeEnd; /* 0.39 */\
  float3 refractionTint; /* float3(0.6, 0.68, 0.7) */\
  float refractionMult0; /* 0.005 */\
  float refractionMult1; /* 0.08 */\
  float dimMult0; /* 0.4 */\
  float dimMult1; /* 0.9 */\
  float specularMult; /* 1 */\
  float causticsMult; /* 0.5 */\

#define INPUT_DIFFUSE_K 0
#define INPUT_AMBIENT_K 0
#define INPUT_EMISSIVE3 0
#define INPUT_SPECULAR_K 0.4
#define INPUT_SPECULAR_EXP 360

#include "include_new/base/_include_ps.fx"
#include "refraction.hlsl"

float4 sampleNoise(float2 uv, bool fixUv = false, bool useMips = false){
  if (fixUv){
    float textureResolution = 32;
    uv = uv * textureResolution + 0.5;
    float2 i = floor(uv);
    float2 f = frac(uv);
    uv = i + f * f * (3 - 2 * f);
    uv = (uv - 0.5) / textureResolution;
  }
	return useMips ? txNoise.Sample(samLinearSimple, uv) : txNoise.SampleLevel(samLinearSimple, uv, 0);
}

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS
  
  float3 posW = ksInPositionWorld;
  float distance = length(pin.PosC);
  float depth = txDepth.SampleLevel(samLinearClamp, pin.PosH.xy * extScreenSize.zw, 0);
  float depthY;
  if (depth){
    depth = 2 * ksNearPlane * ksFarPlane / (ksFarPlane + ksNearPlane - (2 * depth - 1) * (ksFarPlane - ksNearPlane));
    depth = depth - pin.Depth;
    if (depth > 10 || depth < 0) discard;
    depthY = depth * max(0.1, abs(toCamera.y));
  } else {
    depth = 0.1;
    depthY = 0.1;
  }

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float2 uv1 = posW.xz - pin.FlowDir.xz * ksGameTime * flowSpeed0;
  float2 uv2 = posW.xz - pin.FlowDir.xz * ksGameTime * flowSpeed1;

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, uv1 * 0.5);
  txDiffuseValue = lerp(txDiffuseValue, txDiffuse.Sample(samLinear, uv2 * 0.5), 0.5);
  normalW = (txDiffuseValue.xzy * 2 - 1) * float3(1, 1, -1);
  normalW = normalize(lerp(pin.NormalW, normalW, saturate(depthY * 10) * lerp(0.3, 0.7, extSceneRain)));
  
  // float rK = saturate(extSceneRain * 2) * hillWaves * ret.water * saturate(randomMask * 2 - 1) * 0.5; 
  // float rK = 2; 
  // float rI0 = posW.y * 200 + ksGameTime * 0.01;
  // float rI1 = posW.y * 1000 + ksGameTime * 0.01;
  // float rW = 0.5 + 0.5 * lerp(sin(rI0), sin(rI1), saturate(remap(pin.NormalW.y, 0.99, 0.995, 1, 0)));
  // rW = rW * 0.5 + 0.5 * txNoise.SampleLevel(samLinearSimple, posW.xz + rI0, 0);
  // normalW = normalize(normalW * (1 + float3(1, 0, 1) * rK * rW));

  txDiffuseValue = 0;
  RAINFX_WET(txDiffuseValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR;
  L.specularExp = specularMult * lerp(10, 1200, saturate(depthY * 50));
  L.specularValue = lerp(1, 2, pow(saturate(depthY * 50), 2));
  // L.txEmissiveValue *= emissiveNoise;

  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  float alpha = 1;
  float4 noise = sampleNoise(posW.xz / 3, true);

  float2 uvBase = pin.PosH.xy * extScreenSize.zw;
  float uvOffset = 1 - pow(lerpInvSat(max(max(uvBase.x, uvBase.y), max(1 - uvBase.x, 1 - uvBase.y)), 0.9, 1), 2);
  float2 offset = calculateRefractionOffset(pin.PosC, toCamera, normalW, lerp(refractionMult0, refractionMult1, pow(toCamera.y, 4)));
  float4 prev = calculateRefractionColor(uvBase + uvOffset * offset * saturate(depth * 10), saturate(depth * 10) * saturate(extSceneRain * 10 - 1) * pow(saturate(normalW.y), 20));
  if (prev.a > 0){

    uv1 += toCamera.xz * depth;
    uv2 += toCamera.xz * depth;

    // float2 uv1 = posW.xz - pin.FlowDir.xz * ksGameTime * 0.0001;
    // float2 uv2 = posW.xz - pin.FlowDir.xz * ksGameTime * 0.00017;
    // float2 cpos = posW.xz + toCamera.xz * depth + ksGameTime * 0.0000001;
    // float4 noiseC0 = sampleNoise(cpos, true, true);
    // float4 noiseC1 = sampleNoise(cpos + ksGameTime * float2(1, 0) * 0.00002 + noiseC0.xy * 0.1, true, true);
    // float4 noiseC2 = sampleNoise(cpos + ksGameTime * float2(0, 1) * 0.00002 + noiseC0.zw * 0.1, true, true);
    // float cau = lerpInvSat(smin(abs(noiseC2.x - 0.5), abs(noiseC1.x - 0.5)), 0.1, 0.01);
    // prev *= 1 + 0.4 * lerpInvSat(depthY, 0.02, 0.04) * cau * ksLightColor * pow(saturate(1 - distance / 4), 2);

    float cau1 = txDiffuse.SampleBias(samLinearSimple, uv1 * 1.4, 0).a;
    float cau2 = txDiffuse.SampleBias(samLinearSimple, uv2 * 1.4, 0).a;
    prev *= 1 + causticsMult * shadow * smin(cau1, cau2) * ksLightColor * lerpInvSat(depthY, 0, 0.03) * saturate(1 - extSceneRain);

    lighting += prev.rgb * lerp(dimMult0, dimMult1, smoothstep(0, 1, saturate(depthY * lerp(150, 200, noise.x)))) * 
      lerp(1, refractionTint, smoothstep(0, 1, saturate(depthY * 20)));
  } else {
    lighting += txCube.SampleBias(samLinearSimple, toCamera, 5).rgb * 0.3;
    alpha = 0.8;
  }

  ReflParams R;
  R.fresnelMaxLevel = 0.2 * smoothstep(0, 1, saturate(depthY * 20));
  R.fresnelC = 0.05;
  R.fresnelEXP = 4;
  R.ksSpecularEXP = L.specularExp;
  R.finalMult = 1;
  R.metallicFix = 1;
  R.useBias = true;
  R.isCarPaint = false;
  R.useSkyColor = false;
  R.coloredReflections = 0;
  R.coloredReflectionsColor = 0;
  R.useStereoApproach = false;
  R.stereoExtraMult = 0;
  R.reflectionSampleParam = 0;
  R.forceReflectedDir = false;
  R.forcedReflectedDir = 0;
  R.globalOcclusionMult = 1;
  R.useGlobalOcclusionMult = false;
  R.useMatteFix = true;

  #ifdef MODE_GBUFFER
    R.fresnelMaxLevel = 0.2;
    R.ksSpecularEXP = 1200;
  #endif

  #ifdef KEEP_REFLECTION_PARAMS
    R = (ReflParams)0;
  #endif

  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);
  float4 withReflection = calculateReflection(float4(lighting, txDiffuseValue.a), toCamera, pin.PosC, normalW, R);
  RAINFX_WATER(withReflection);


  // withReflection.rg = frac(uv1 * 4);


  // float2 cpos = posW.xz + toCamera.xz * depth + ksGameTime * 0.0000001;
  // float4 noiseC0 = sampleNoise(cpos, true, true);
  // float4 noiseC1 = sampleNoise(cpos + ksGameTime * float2(1, 0) * 0.00002 + noiseC0.xy * 0.1, true, true);
  // float4 noiseC2 = sampleNoise(cpos + ksGameTime * float2(0, 1) * 0.00002 + noiseC0.zw * 0.1, true, true);
  // float cau = lerpInvSat(smin(abs(noiseC2.x - 0.5), abs(noiseC1.x - 0.5)), 0.1, 0.01);
  // prev *= 1 + 0.4 * lerpInvSat(depthY, 0.02, 0.04) * cau * ksLightColor * pow(saturate(1 - distance / 4), 2);
  // prev.rgb = noiseC1.xyz;

  // float4 prev = txPrevFrame.SampleLevel(samLinearClamp, pin.PosH.xy * extScreenSize.zw + noise.xy * saturate(depth * 10) * 0.01, 0);
  // withReflection.xy = frac(cpos * 8);

  float fadeStart = 1.86;
  float fadeEnd = 0.39;
  if (pin.Tex.y > 0) {
    depthY *= lerp(0.3, 1, lerpInvSat(pin.Tex.y, fadeStart, fadeStart + 0.02));
    alpha *= saturate(pin.Tex.y / fadeStart);
  } else {
    depthY *= lerp(0.3, 1, lerpInvSat(-pin.Tex.y, fadeEnd, fadeEnd + 0.02));
    alpha *= saturate(-pin.Tex.y / fadeEnd);
  }

  alpha *= smoothstep(0, 1, saturate(depthY * 200)); // * saturate(pin.Tex.y < 0 ? pin.Tex.y / );


  #ifdef MODE_GBUFFER
    R.resultColor = 0;
    R.resultPower = 0;
  #endif

  // withReflection.rg = frac(pin.Tex.y);
  // alpha = 1;

  // float alphaBase = 1 - directK;
  // float alphaResult = saturate(alphaBase * alphaBase * alphaValue);
  // R.resultColor *= alphaResult;
  // R.resultPower *= alphaResult;
  // withReflection = tx1.x;
  RETURN(withReflection, alpha);
}
