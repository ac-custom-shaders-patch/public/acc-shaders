#define NO_CARPAINT
#include "include_new/base/_include_ps.fx"

struct PS_IN_Basic {
  float4 PosH : SV_POSITION;
  float3 PosC : TEXCOORD1;
};

#define DISCARD_AREAS_NUM 4
cbuffer cbDiscardAreas : register(b5) {
  float3 gCarUp;
  float _pad0;
  float4 area[DISCARD_AREAS_NUM];
}

float main(PS_IN_Basic pin) : SV_TARGET {
  bool d = dot2(pin.PosC) < pow(0.3, 2) && abs(dot(normalize(pin.PosC), gCarUp)) < 0.5;
  [unroll] for (int i = 0; i < DISCARD_AREAS_NUM; i++){ 
    float3 dif = pin.PosC - area[i].xyz;
    if (dot2(dif) < area[i].w) d = true;
  }
  clip(-d);
  return 0;
}
