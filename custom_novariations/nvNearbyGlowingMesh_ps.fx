#define NO_CARPAINT
#include "include_new/base/_include_ps.fx"

struct PS_IN_Basic {
  float4 PosH : SV_POSITION;
  float3 PosC : TEXCOORD1;
};

cbuffer _cbData : register(b5) {
  float4 gColor;
  float3 gCenter;
  float gRadiusInner;
  float gRadiusOuter;
  float3 gStretchDir;
  float gStretchAmount;
}

float4 main(PS_IN_Basic pin) : SV_TARGET {
  // bool d = dot2(pin.PosC) < pow(0.3, 2) && abs(dot(normalize(pin.PosC), gCarUp)) < 0.5;
  // [unroll] for (int i = 0; i < DISCARD_AREAS_NUM; i++){ 
  //   float3 dif = pin.PosC - area[i].xyz;
  //   if (dot2(dif) < area[i].w) d = true;
  // }
  // clip(-d);
  // return float4(100, 0, 0, 1);


  float3 delta = pin.PosC - gCenter;
  float distance = length(delta);
  distance -= pow(dot(normalize(delta), gStretchDir), 2) * gStretchAmount;

  float fade = smoothstep(0, 1, saturate(remap(distance, gRadiusInner, gRadiusOuter, 1, 0)));
  clip(fade - 0.001);



  return gColor * float4(1, 1, 1, fade);
}
