#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float4x4 gViewProj;
  float3 ksCameraPosition_;
  float lPad1;
}

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct PS_IN_puddlesGen {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float3 PosW : TEXCOOR1;
  float3 NormalW : TEXCOOR2;
  float Ao : TEXCOORD3;
};

float4 toScreenSpaceAlt(float4 posW){
  float4 posH = mul(posW, gViewProj);
  if (posH.z < 0) posH.z = 0;
  return posH;
} 

PS_IN_puddlesGen main(VS_IN_ac vin) {
  PS_IN_puddlesGen vout;
  float4 posW = mul(vin.PosL, ksWorld);
  vout.PosH = toScreenSpaceAlt(posW);
  vout.Tex = vin.Tex;
  vout.PosW = posW.xyz;
  vout.NormalW = mul(vin.NormalL, (float3x3)ksWorld);
  vout.Ao = vsLoadAo0(vin.TangentPacked);
  return vout;
}

