#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float4x4 gViewProj;
}

cbuffer cbPerObject2 : register(b1) {
  float4x4 _ksWorld;
  float _extSceneRain;
  float _extSceneWater;
}

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD;
};

float4 toScreenSpaceAlt(float4 posL){
  float4 posH = mul(mul(posL, _ksWorld), gViewProj);
  if (posH.z < 0) posH.z = 0;
  return posH;
} 

PS_IN main(VS_IN_ac vin) {
  PS_IN vout;

  vin.PosL.y -= 0.04;
  vin.PosL.y += 0.04 * lerp(saturate(vsLoadHeating(vin.TangentPacked) * _extSceneRain * 2), 1, sqrt(saturate(_extSceneWater * 1.6)));

  vout.PosH = toScreenSpaceAlt(vin.PosL);
  // vout.PosH = 1;
  vout.Tex = vin.Tex;
  return vout;
}

