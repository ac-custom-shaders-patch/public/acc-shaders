#define STREAMS
#define PARAM_BLUR pin.BlurK
#define PARAM_USE_COLOR gUseColor
#include "accRainDrops.hlsl"

#define ALLOW_EXTRA_FEATURES
#include "refraction.hlsl"

PS_OUT main(PS_IN pin) {
  INTERIOR_TEST;
  READ_VECTOR_TOCAMERA
  float softK = calculateSoft(pin.PosH, pin.PosC, PARAM_BLUR);

  float d = 0;

  float3 posW = pin.PosC + ksCameraPosition.xyz;
  float4 noise = txNoise.SampleLevel(samLinearSimple, pin.Tex * 0.07 + posW.xz + pin.NoiseOffset * 0.1, 0);
  pin.Tex *= 1 + noise.xy * 0.1;

  if (pin.NoiseOffset > 0){
    if (pin.Tex.y > 0) {
      pin.Tex.y /= saturate(pin.NoiseOffset * 20);
    }
  } else {
    // float t = lerp(1, 0, pow(-pin.NoiseOffset, 1));
    // float t = 0;
    // if (pin.Tex.y > t){
    //   clip(abs(toCamera.y) * sqrt(1 - pow(pin.Tex.x, 2)) - (pin.Tex.y - t));
    // }

    pin.Tex.y *= lerp(1, 40, pow(-pin.NoiseOffset, 4));
    pin.Tex.x *= lerp(2, 1, -pin.NoiseOffset * pin.SideK);

    float t = pin.Tex.y;// - lerp(1, 0, pow(-pin.NoiseOffset, 0.5));
    if (t > 0){
      clip(abs(toCamera.y) * sqrt(1 - pow(pin.Tex.x, 2)) - t);
    }
  }

  float sharpness = 0.95;
  float facingShare = sqrt(saturate(1 - dot2(pin.Tex)));
  // if (pin.Tex.y < 0) pin.Tex.y *= lerp(1, 1.4, abs(pin.Tex.y));
  float3 normal = normalize(pin.DirUp * pin.Tex.y * lerp(1, 4, gStretching)
    + pin.DirSide * pin.Tex.x
    + -toCamera * facingShare);

  float fresnelBase = saturate(0.05 + pow((1 + dot(toCamera, normal)) * lerp(1, 2, gStretching), 3));
  float fresnel = max(fresnelBase, saturate(abs(pin.Tex.y) * 1.3) * gStretching);

  float alpha = saturate(remap(length(pin.Tex), sharpness, 1, 1, 0)) * softK;

  // float4 noiseSplash = txNoise.SampleLevel(samLinearSimple, pin.Tex * 0.1 + pin.NoiseOffset, 0);
  // alpha *= noiseSplash.x;

  float3 refraction = lerp(toCamera, -normal, 0.5);
  float3 reflection = reflect(toCamera, normal);
  float3 reflectionColor = sampleEnv(reflection, 3, 0);
  float3 refractionColor = sampleEnv(refraction, 3, 0);
  float3 refractionBlurred = sampleEnv(toCamera, 4 + alpha, 1);

  if (gUseColor){
    float2 uv = calculateRefractionUV(pin.PosH, pin.PosC, toCamera, normal);
    refractionColor = calculateRefractionColor(uv).rgb;
    refractionBlurred = txPrevFrame.SampleLevel(samLinearClamp, uv, 3).rgb;
  }

  refractionColor = lerp(refractionColor, refractionBlurred, pin.BlurK);

  clip(alpha - 0.05);

  float3 lighting;
  if (pin.NoiseOffset && gUseColor){
    lighting = lerp(refractionColor, reflectionColor, fresnel);
    lighting += pin.LightVal * max(fresnel * pow(saturate(dot(pin.LightDir, toCamera)), 20) * 0.4,
      lerp(0.02, simpleReflectanceModel(-toCamera, pin.LightDir, normal), pin.LightFocus));
    alpha = 1;
  } else {
    alpha = 0.1;
    lighting = fresnel * reflectionColor / alpha;
    lighting += pin.LightVal * max(fresnel * pow(saturate(dot(pin.LightDir, toCamera)), 20),
      lerp(0.05, 0.1 * pow(saturate(dot(normal, pin.LightDir)), 40), pin.LightFocus)) / alpha;
  }

  if (d){
    lighting.r = 4;
  }

  lighting += simpleReflectanceModel(-toCamera, -ksLightDirection.xyz, normal) * ksLightColor.rgb * pin.Shadow;
  alpha *= lerp(1, saturate(remap(abs(pin.Tex.y), 0.2, 0.8, 1, 0)) * 0.75, pin.BlurK) * softK * interiorMult;

  // lighting = float3(2, 0, 0);
  // alpha = 1;
  
  // {    
  //   float3 lighting = txPrevFrame.SampleLevel(samLinearClamp, pin.PosH.xy * extScreenSize.zw, 1).rgb;
  //   float alpha = 1;
  //   RETURN_BASE(lighting, alpha);
  // }

  //  discard;

  RETURN_BASE(lighting, alpha);
}