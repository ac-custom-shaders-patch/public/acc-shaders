#define USE_RAIN_DIR_FIXED
#include "accRainDrops.hlsl"

#ifdef SNOW
  cbuffer cbCSBuffer : register(b0) {
    float2 gAir0;
    float2 gAir1;
  }
  Texture2D<float2> txAirMap : register(t2);
#endif

[numthreads(32, 32, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  float2 basePosXZ = gSpawnPointA + float2(float(threadID.x), float(threadID.y)) * gMapStep;
  RAND_INIT(gSpawnPointAWorld + float2(float(threadID.x), float(threadID.y)) * gMapStep);
  if (gDensity < RAND) return;
  // if (RAND > 0.05) return;

  float verticalRandom = RAND + RAND * 0.1;
  float mixParam = RAND;
  float3 randomDir = rainDropRandomDir(float2(RAND, RAND));
  float3 dir = rainDropSummaryDir(mixParam, randomDir);
  float3 pos = rainDropPos(mixParam, basePosXZ, randomDir, verticalRandom);
  float alterDropPosRand = RAND;

  float dist = length(pos - gPos);
  float distCamera = length(pos - gCameraPosition);
  float fade = saturate(remap(dist * gDistanceFadeMult, 16, 12, 0, 1))
    * (gClipNearby ? saturate(remap(distCamera, 0.8, 1.2, 0, 1)) : 1);

  if (!fade || !isVisible(pos)) return;

  float3 baseGroundPos = groundPos(pos);
  float distanceToGround = pos.y - baseGroundPos.y;
  if (distanceToGround < -0.13) return;

  alterDropPos(pos, baseGroundPos.y, alterDropPosRand);

  RainDrop drop;
  drop.pos = pos;
  drop.fade = fade;
  drop.dir = dir + gCameraVelocity * TIME_MULT / -30;
  #ifdef SNOW
    drop.size = RAND;
  #else
    drop.size = 0.13;
  #endif

  #ifdef SNOW
    float2 airUV = (pos.xz - gAir1) / (gAir0 - gAir1);
    float shiftMult = lerpInvSat(distanceToGround, 5, 2);
    float2 airValue = txAirMap.SampleLevel(samLinearBorder0, airUV, 2.5);
    float pr0 = dot2(airValue);
    float prX = dot2(txAirMap.SampleLevel(samLinearBorder0, airUV + float2(1, 0) / (gAir0 - gAir1), 2.5));
    float prY = dot2(txAirMap.SampleLevel(samLinearBorder0, airUV + float2(0, 1) / (gAir0 - gAir1), 2.5));

    airValue.x += (pr0 - prX) * 4;
    airValue.y += (pr0 - prY) * 4;
    drop.pos.y += length(airValue) * 1.2;    
    drop.pos.xz += airValue * 2;
    drop.dir = float3(gWindDirX * 3, -1, gWindDirY * 3) + gCameraVelocity * TIME_MULT / -5;
    // if (dot(airValue, 1)) drop.size = 10;

    // if (baseGroundPos.y < -2) drop.size = 10;
    // float4 rst = mul(float4(drop.pos, 1), gRainShadowTransform);
    // if (!all(rst.xy > 0 && rst.xy < 1)) drop.size = 10;
  #endif

  buResultDrops.Append(drop);
}