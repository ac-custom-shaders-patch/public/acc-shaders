#include "include/common.hlsl"
#include "include_new/base/_include_ps.fx"
#include "include/poisson.hlsl"

Texture2D txColor : register(t0);
Texture2D<float> txDepth2 : register(t1);

cbuffer cbCamera : register(b10) {
  float2 gDir;
  float gOffset;
  float gIntensity;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
  noperspective float2 AbsTex : TEXCOORD1;
};

float4 blurred(float2 uv) : SV_TARGET {
  float4 avg = 0;
  #define PUDDLES_DISK_SIZE 32
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * float2(0.02, 0.2);
    float4 value = txColor.SampleLevel(samLinearClamp, uv + offset, 0);
    avg += float4(value.rgb, 1) * GAMMA_OR(1 / (1e-20 + sqrt(dot(value.rgb, 1))), 1 + dot(value.rgb, 1));
  }
  avg /= avg.w;
  avg.rgb = avg.rgb / (1 + avg.rgb);
  return avg;
}

float sampleNoise(float2 rainUV, float depth, float scale) {
  float4 v = textureSampleVariation(txNoise, samLinearSimple, rainUV * scale, 0);
  v.w *= lerp(0.7, 1, pow(saturate(gIntensity), 0.4));
  return lerpInvSat(v.w, 0.6, 0.7) * 0.35 * pow(depth, 1);
}

float4 main(VS_Copy pin) : SV_TARGET {
  float v = pow(saturate(txDepth2.SampleLevel(samLinearSimple, pin.Tex, 0)), 100);
  // return float4(v, 1 - v, 0, 1);
  // return blurred(pin.Tex);

  float2 rainUV;  
  #ifdef SNOW_MODE
    rainUV = pin.Tex * float2(4, -1);// * float2(8, 0.2);
    rainUV = gDir.y * rainUV.xy + gDir.x * float2(-1, 1) * rainUV.yx;
    rainUV.y -= gOffset * 0.01;
    rainUV *= float2(2, 2);
  #else
    rainUV = pin.Tex * float2(4, -1);// * float2(8, 0.2);
    rainUV = gDir.y * rainUV.xy + gDir.x * float2(-1, 1) * rainUV.yx;
    rainUV.y -= gOffset * 0.5;
    rainUV *= float2(2, 0.1);
  #endif

  // return float4(3, 0, 0, sampleNoise(rainUV, pow(v, 1), 1.27));

  float4 n = sampleNoise(rainUV, pow(v, 1), 1.27)
    + sampleNoise(rainUV, pow(v, 2), 3.71)
    + sampleNoise(rainUV, pow(v, 3), 11.39)
    + sampleNoise(rainUV, pow(v, 4), 33.71);
  #ifdef SNOW_MODE
    return float4(ksAmbientColor_sky0.rgb, n.w);
  #endif
  return float4(lerp(blurred(pin.Tex).rgb, ksAmbientColor_sky0.rgb * 0.3, 0.3), n.w * lerp(0.5, 1, gIntensity) * 2);
  // return txColor.SampleLevel(samLinearSimple, pin.Tex, 0);
  // return txDiffuse.SampleLevel(samPoint, pin.Tex, 0);
  // return txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);// + dithering(pin.PosH.xy) * 0;
}
