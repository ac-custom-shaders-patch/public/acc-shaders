#include "include/common.hlsl"

Texture2D<float> txState : register(t0);
Texture2D txNoise : register(TX_SLOT_NOISE);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#define R 1
float sampleBlur(float2 uv, float level){
  float currentShot = 0;
  float totalWeight = 0;
  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    float weight = 1;
    currentShot += txState.SampleLevel(samLinearClamp, uv, level, int2(x, y)) * weight;
    totalWeight += weight;
  }
  return currentShot / totalWeight;
}

float get(float2 uv, float level, float4 rnd){
  return txState.SampleLevel(samLinearClamp, uv, level);
}

float2 getN(float2 uv, float radius, float level, float4 rnd){
  float tb = get(uv, level, rnd);
  float tl = get(uv - float2(radius, 0), level, rnd);
  float tt = get(uv - float2(0, radius), level, rnd);
  return -float2(tl - tb, tb - tt);
}

float4 main(VS_Copy pin) : SV_TARGET {
  float4 rnd = txNoise.SampleLevel(samLinear, pin.Tex, 0);
  float t0 = get(pin.Tex, 0, rnd);  
  float2 nt = getN(pin.Tex, 0.002, 0, rnd) + getN(pin.Tex, 0.004, 2.5, rnd);
  return float4(nt.xy * float2(-0.5, -0.5) + 0.5, 0, t0);
}