#define USE_RAIN_DIR_FIXED
#include "accRainDrops.hlsl"

void spawnBubble(float3 pos, float4 rand4, float scale){
  RainDrop drop;
  drop.pos = pos;
  drop.fade = 1 - scale;
  drop.dir = float3(rand4.y, 0, 0);
  #ifdef SNOW
  drop.size = scale * 0.01;
  #else
  drop.size = scale * 0.06 * lerp(0.5, 1, rand4.x);
  #endif
  // drop.size *= 5;
  // drop.fade = 2;
  buResultHits.Append(drop);
}

#include "include/poisson.hlsl"

[numthreads(32, 32, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  float2 basePosXZ = gSpawnPointA + float2(float(threadID.x), float(threadID.y)) * gMapStep;
  RAND_INIT(gSpawnPointAWorld + float2(float(threadID.x), float(threadID.y)) * gMapStep);
  if (gDensity * gSplashesDensity < RAND) return;
  // if (RAND > 0.05) return;

  float verticalRandom = RAND + RAND * 0.1;
  float mixParam = RAND;
  float3 randomDir = rainDropRandomDir(float2(RAND, RAND));
  float3 dir = rainDropSummaryDir(mixParam, randomDir);
  float3 pos = rainDropPos(mixParam, basePosXZ, randomDir, verticalRandom);
  float alterDropPosRand = RAND;
  float3 posBase = pos;
  float3 ground = groundPos(pos);
  float4 rand4 = float4(RAND, RAND, RAND, RAND);

  #ifdef SNOW
    float scale = sqrt(lerpInvSat(pos.y, ground.y - RAIN_SPEED / 4., ground.y));
    if (scale == 0 || scale == 1) return;
  #else
    float scale = lerpInvSat(pos.y, ground.y, ground.y - RAIN_SPEED / 8.);
    if (scale == 0 || scale == 1) return;
  #endif

  alterDropPos(pos, ground.y, alterDropPosRand);
  pos += dir * (ground.y - pos.y) / dir.y;
  pos += dir * (groundPos(pos).y - pos.y) / dir.y;
  if (!isVisible(pos)) return;

  spawnBubble(pos, rand4, scale);
}