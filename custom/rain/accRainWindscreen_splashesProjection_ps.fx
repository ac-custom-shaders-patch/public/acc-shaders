#include "accRainWindscreen.hlsl"

Texture2D txDiffuse : register(t0);
Texture2DArray<float> txRainShadow : register(t1);

cbuffer cbData : register(b11) {
  float4x4 gTransform;
  float4x4 gShadowTransform;

  float3 gBreathPoint;
  float gBreathRadius;

  float3 gHandRayPoint0;
  float gHandRadiusInv;
  
  float3 gHandRayPoint1;
  float gBreathPhase;

  float3 gHandRayPoint2;
  float gHandRadiusInv1Alt;

  float3 gHandRayPoint3;
}

#include "include/poisson.hlsl"
float2 sampleShadow(float2 uv, float comparison){
  float2 sum = 0;
  float sampleScale = 0.0015;
  if (comparison >= 1) return 1; 

  #define RAIN_DISK_SIZE 4
  for (uint i = 0; i < RAIN_DISK_SIZE; ++i) {
    float2 sampleUV = uv.xy + SAMPLE_POISSON(RAIN_DISK_SIZE, i) * sampleScale;
    float comparisonUV = comparison - 0.001 * (1 + SAMPLE_POISSON_LENGTH(RAIN_DISK_SIZE, i));
    sum.x += txRainShadow.SampleCmpLevelZero(samShadow, float3(sampleUV, 0), comparisonUV);
    sum.y += txRainShadow.SampleCmpLevelZero(samShadow, float3(sampleUV, 1), comparisonUV);
  }
  return saturate(float2(sum.x, sum.y) / RAIN_DISK_SIZE);
}

float distanceToTriangle(float3 p, float3 a, float3 b, float3 c) {
  float3 ba = b - a; float3 pa = p - a;
  float3 cb = c - b; float3 pb = p - b;
  float3 ac = a - c; float3 pc = p - c;
  float3 nor = cross(ba, ac);
  return sqrt((sign(dot(cross(ba, nor), pa)) + sign(dot(cross(cb, nor), pb)) + sign(dot(cross(ac, nor), pc))<2.0)
     ? min(min(
       dot2(ba * saturate(dot(ba, pa) / dot2(ba)) - pa),
       dot2(cb * saturate(dot(cb, pb) / dot2(cb)) - pb)),
       dot2(ac * saturate(dot(ac, pc) / dot2(ac)) - pc))
     : dot(nor, pa) * dot(nor, pa) / dot2(nor));
}

float4 sampleNoise(float2 uv){
	float textureResolution = 32;
	uv = uv * textureResolution + 0.5;
	float2 i = floor(uv);
	float2 f = frac(uv);
	uv = i + f * f * (3 - 2 * f);
	// uv = i + f * f * f * (f * (f * 6 - 15) + 10);
	uv = (uv - 0.5) / textureResolution;
	return txNoise.SampleLevel(samLinearSimple, uv, 0);
}

float4 main(PS_IN_splashesProject pin) : SV_TARGET {
  float shadow = sampleShadow(pin.PosShadow.xy, pin.PosShadow.z).y;
  float4 splash = txDiffuse.Sample(samLinear, pin.Tex);

  float4 ret = float4(splash.xyz, lerp(0.5 * shadow, 1, splash.a));
  ret.xyz = ret.xyz * 0.5 + 0.5;

  float4 noise = sampleNoise(float2(pin.TexAlt.x * 0.5, pin.TexAlt.y * 0.5 + gBreathPhase * 0.05));
  // float breath = gBreathRadius - length(pin.PosCar - gBreathPoint) + (noise.x - 0.5) * 0.1;
  float breath = 0.2 - length(pin.PosCar - gBreathPoint) + (noise.x - 0.5) * 0.1;
  ret.x = lerp(saturate(breath * 40), ret.x, saturate(splash.a * 10));
  // ret.x = 1;

  float hand = 0;

  if (gHandRadiusInv != 0){
    if (gHandRadiusInv1Alt){
      // VR mode: point 0 and point 1 are for index fingers, point 2 and point 3 for hands
      float minDistance = length(pin.PosCar - gHandRayPoint0) * gHandRadiusInv;
      minDistance = min(minDistance, length(pin.PosCar - gHandRayPoint1) * gHandRadiusInv);
      minDistance = min(minDistance, length(pin.PosCar - gHandRayPoint2) * gHandRadiusInv1Alt);
      minDistance = min(minDistance, length(pin.PosCar - gHandRayPoint3) * gHandRadiusInv1Alt);
      hand = saturate(1 - minDistance);
    } else {
      float distance = distanceToTriangle(pin.PosCar, gHandRayPoint0, gHandRayPoint1, gHandRayPoint2);
      hand = saturate(1 - distance * gHandRadiusInv);
    }
  }

  ret.y = lerp(hand, ret.y, saturate(splash.a * 100));
  return ret;
}