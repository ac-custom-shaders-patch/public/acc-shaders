#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

bool isToRemove(float2 uv){
  return dot(float4(txDiffuse.SampleLevel(samPoint, uv, 0, int2(0, -1)).z,
     txDiffuse.SampleLevel(samPoint, uv, 0, int2(0, 1)).z,
     txDiffuse.SampleLevel(samPoint, uv, 0, int2(-1, 0)).z,
     txDiffuse.SampleLevel(samPoint, uv, 0, int2(1, 0)).z), 1) > 0;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float4 base = txDiffuse.SampleLevel(samPoint, pin.Tex, 0);
  // With these rules a drop eliminates all the drops around. Looks cool, but I need more drops!
  // return base.w && isToRemove(pin.Tex) ? float4(0, 0, 1, 0) : base.z ? 0 : base;
  return base.w && isToRemove(pin.Tex) ? float4(0, 0, 0, 0) : base.z ? 0 : base;
}