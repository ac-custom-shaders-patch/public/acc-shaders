#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#define R 1
float4 sampleBlur(float2 uv, float level){
  float4 currentShot = 0;
  float totalWeight = 0;
  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    float weight = 1;
    currentShot += txDiffuse.SampleLevel(samLinearClamp, uv, level, int2(x, y)) * weight;
    totalWeight += weight;
  }
  return currentShot / totalWeight;
}

float3 getNT(float2 uv, float level){
  float4 prevB = level == 0 ? txDiffuse.SampleLevel(samLinearClamp, uv, 0) : sampleBlur(uv, level);
  float thickness = prevB.x * (level ? 1 : 4)
    + (prevB.z + prevB.y * 0.1)
      // * saturate(remap(prevB.y, 0.2, 0.1, 0, 1)) 
      // * saturate(remap(prevB.y, 0.2, 0.3, 0, 1)) 
      // * saturate(remap(prevB.y, 0.5, 0.3, 0, 1)) 
      // * (level ? 0.5 : 0.3);  
      * (level ? 0.5 : 1);  
  float dx = ddx(thickness);
  float dy = ddy(thickness);
  float2 normal = float2(dx, dy) * 20;
  return float3(normal, thickness);
}

float4 main_(VS_Copy pin) : SV_TARGET {
  float3 nb = getNT(pin.Tex, 0);
  float3 nt = getNT(pin.Tex, 1.2);
  nt += getNT(pin.Tex + float2(-0.005, 0), 1.6);
  nt += getNT(pin.Tex + float2(0.005, 0), 2.2);
  nt += getNT(pin.Tex + float2(0, -0.005), 1.6);
  nt += getNT(pin.Tex + float2(0, 0.005), 2.2);
  nt /= 5;
  return float4(nt.xy * float2(-0.5, -0.5) + 0.5, 0, saturate(nb.z * 2));
}

float get(float2 uv, float level){
  float4 prevB = txDiffuse.SampleLevel(samLinearClamp, uv, level);
  return prevB.x * 2
    + (prevB.z + prevB.y * 0.1) * 0.5; 

  // return prevB.x * (level ? 1 : 4)
  //   + (prevB.z + prevB.y * 0.1)
  //     // * saturate(remap(prevB.y, 0.2, 0.1, 0, 1)) 
  //     // * saturate(remap(prevB.y, 0.2, 0.3, 0, 1)) 
  //     // * saturate(remap(prevB.y, 0.5, 0.3, 0, 1)) 
  //     // * (level ? 0.5 : 0.3);  
  //     * (level ? 0.5 : 1); 
}

float2 getN(float2 uv, float radius, float level){
  float tb = get(uv, level);
  float tl = get(uv - float2(radius, 0), level);
  float tt = get(uv - float2(0, radius), level);
  return -float2(tl - tb, tt - tb);
}

float4 main(VS_Copy pin) : SV_TARGET {
  float standingMask = 1 - saturate(txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0).y * 10);
  float t0 = get(pin.Tex, 0);
  float mx = dot(txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 2.5), float4(10, 10, 10, 0));
  float2 nt = getN(pin.Tex, 0.0005, 0) + getN(pin.Tex, 0.002, 1.5) + getN(pin.Tex, 0.006, 3);
  // return float4(nt.xy * float2(-0.5, -0.5) + 0.5, saturate(mx), saturate(mx));
  // return float4(nt.xy * float2(-0.5, -0.5) + 0.5, saturate(mx), saturate(standingMask));
  return float4(nt.xy * standingMask * float2(-0.5, -0.5) + 0.5, saturate(mx), saturate(t0 * 4));
}