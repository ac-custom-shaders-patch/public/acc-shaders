#define ALLOW_RAINFX
#define RAINFX_USE_PUDDLES_MASK
#define MODE_PUDDLES
#include "include_new/base/_include_ps.fx"

struct PS_IN_puddlesGen {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD;
};

float2 main(PS_IN_puddlesGen pin) : SV_TARGET {
  return 0;
}

