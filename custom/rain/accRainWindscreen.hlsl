#include "include/common.hlsl"
#include "include/normal_encode.hlsl"

#define MIN_WIPERS_SPEED 0.02
#define CB_DATA b8

Texture2D txInput : register(t0);
Texture2D<float2> txVelocity : register(t1);
Texture2D txNoise : register(TX_SLOT_NOISE);

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct VS_IN_skinned {
  AC_INPUT_SKINNED_ELEMENTS
};

struct PS_IN_project {
  noperspective float4 PosH : SV_POSITION;
  noperspective float4 PosL : TEXCOORD0;
  noperspective float2 Tex : TEXCOORD2;
};

struct PS_IN_splashesProject {
  noperspective float4 PosH : SV_POSITION;
  noperspective float3 PosShadow : TEXCOORD0;
  noperspective float3 PosCar : TEXCOORD1;
  noperspective float2 Tex : TEXCOORD2;
  noperspective float2 TexAlt : TEXCOORD3;
};

struct PS_IN_wipersProject {
  float4 PosH : SV_POSITION;
  float3 PosW : TEXCOORD0;
  float2 Tex : TEXCOORD1;
  float2 TexWiper : TEXCOORD2;
};

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float4x4 gWorld;
  float4 gValue;
}

cbuffer cbData : register(b11) {
  float4x4 gWipersTransform;
}

cbuffer cbData : register(b12) {
  float gFadeFast;
  float gFadeNormal;
  float gFadeSlow;
  float gFadeExtraSlow;
  float gFading;
  float3 gWindscreenPad0;
}

float4 texToScreen(float2 tex){
  float2 pos = frac(tex);
  pos.y = 1 - pos.y;
  return float4(pos * 2 - 1, 0, 1);
}