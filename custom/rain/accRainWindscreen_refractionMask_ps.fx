struct PS_IN {
  float4 PosH : SV_POSITION;
};

cbuffer cbCamera : register(b10) {
  float2 gValue;
}

float2 main(PS_IN pin) : SV_TARGET {
  return gValue;
}

