#define SPS_NO_POSC
#include "../../custom_objects/common/bending.hlsl"
#include "accRainWindscreen.hlsl"

BENDING_FX_IMPL

cbuffer cbCamera : register(b0) {
  float4x4 ksView_base;
  float4x4 ksProjection_base;
}

cbuffer cbData : register(CB_DATA) {
  float gInstanceMult;
  float gDistanceThreshold;
  float gResolutionStep;
  float gVelocityMult;
}

cbuffer cbDataObject : register(b11) {
  float4x4 gTransform;
  float3 gMeshOffset;
  float gPad;
}

cbuffer cbDataObject : register(b11) {
  float4x4 gWorld0;
}

struct PS_IN {
  float4 PosH : SV_POSITION;
};

#ifndef SKINNED_WEIGHT_CHECK
  #define SKINNED_WEIGHT_CHECK(a) (a != 0)
#endif

cbuffer cbBones : register(b13) {
  float4x4 bones[55];
}

float4 toWorldSpace(float4 posL, float4 boneWeights, float4 boneIndices){
  float3 posW = 0;
  for (int i = 0; i < 4; i++){
    float weight = boneWeights[i];
    if (SKINNED_WEIGHT_CHECK(weight)){
      uint index = (uint)boneIndices[i];
      float4x4 bone = bones[index];
      posW += (mul(bone, posL).xyz + gMeshOffset) * weight;
    }
  }

  return mul(float4(posW, 1), gTransform);
} 

#include "../../recreated/include_new/base/sps_utils.fx"

PS_IN main(VS_IN_skinned vin SPS_VS_ARG) {
  PS_IN vout;
  float4 posW = toWorldSpace(vin.PosL, vin.BoneWeights, vin.BoneIndices);
  vout.PosH = mul(mul(posW, ksView), ksProjection);
  SPS_RET(vout);
}
