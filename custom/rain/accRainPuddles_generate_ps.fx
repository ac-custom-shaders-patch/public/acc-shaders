#include "include/common.hlsl"
Texture2D<float> txHR : register(t0);
// Texture2D<float> txLR : register(t1);
Texture2D<float> txWireframe : register(t2);
Texture2D<float> txDetailedMask : register(t3);
Texture2D<float> txPuddles : register(TX_SLOT_RAIN_PUDDLES_PATTERN);
Texture2D<float> txNoise : register(TX_SLOT_NOISE);

cbuffer cbPiece : register(b11) {
  float4x4 gViewProj;
  float2 gArea0;
  float2 gArea1;
};

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float tonemap_uchimura(float x, float P, float a, float m, float l, float c, float b) {
  // Uchimura 2017, "HDR theory and practice"
  // Math: https://www.desmos.com/calculator/gslcdxvipg
  // Source: https://www.slideshare.net/nikuque/hdr-theory-and-practicce-jp
  float l0 = ((P - m) * l) / a;
  float L0 = m - m / a;
  float L1 = m + (1.0 - m) / a;
  float S0 = m + l0;
  float S1 = m + a * l0;
  float C2 = (a * P) / (P - S1);
  float CP = -C2 / P;

  float w0 = 1.0 - smoothstep(0.0, m, x);
  float w2 = step(m + l0, x);
  float w1 = 1.0 - w0 - w2;

  float T = m * pow(x / m, c) + b;
  float S = P - (P - S1) * exp(CP * (x - S0));
  float L = m + a * (x - m);

  return T * w0 + L * w1 + S * w2;
}

float tonemap_uchimura(float x) {
  const float P = 1.0;  // max display brightness
  const float a = 1.0;  // contrast
  const float m = 0.22; // linear section start
  const float l = 0.4;  // linear section length
  const float c = 1.33; // black
  const float b = 0.0;  // pedestal
  return tonemap_uchimura(x, P, a, m, l, c, b);
}
  
#include "include/poisson.hlsl"

float sampleWir(float2 uv){
  float ret = 0;
  #define R 1
  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    ret = max(ret, txWireframe.SampleLevel(samLinearSimple, uv, 1, int2(x, y) * 1));
  }
  return saturate(ret * 4);
}

float detailedMask(float2 uv){
  POISSON_AVG(float, v, txDetailedMask, samLinearSimple, uv, 0.003, 12);
  return 1 - v;
}

// #define CLIPPING_FIX 0.9
#define CLIPPING_FIX 0.2

float sampleDifference(float2 uv, float2 uvOrig, float4 posH){
  float wireframeAmount = sampleWir(uv) * detailedMask(uv);
  float fallbackValue = txPuddles.SampleLevel(samLinearSimple, lerp(gArea0, gArea1, uvOrig) * 0.02, 0);
  fallbackValue = pow(saturate(remap(fallbackValue, 0.5, 0.7, 0, 0.25)), 1.6);

  float4 noiseLR = txNoise.SampleLevel(samLinear, uv * (2 * 0.54), 0) - 0.5;
  float local = txHR.SampleLevel(samLinearClamp, uv, 0);
  float cy = ddy(local) * 1024;
  float cx = ddx(local) * 1024;

  float sum = 0;
  float sampleScale = lerp(0.006, 0.009, noiseLR.x);

  #define RAIN_DISK_SIZE 32
  for (uint i = 0; i < RAIN_DISK_SIZE; ++i) {
    float2 dir = SAMPLE_POISSON(RAIN_DISK_SIZE, i);
    float2 offset = (dir) * sampleScale;
    float refHeight = local + dot(1, offset * float2(cx, cy));

    float nearby;
    nearby = txHR.SampleLevel(samLinearClamp, uv + offset, 0);

    float dif = refHeight - nearby;
    sum += dif < 0 ? 0 : dif;
  }

  sum = abs(sum) > 0.5 ? 0.2 : sum * (CLIPPING_FIX * 0.5e5 / RAIN_DISK_SIZE);
  // sum = sum / (1 + sum);
  sum = tonemap_uchimura(saturate(sum));
  sum = lerp(sum, fallbackValue * CLIPPING_FIX, wireframeAmount);
  return sqrt(sum);
}

float main(VS_Copy pin) : SV_TARGET {
  float2 uvAdj = pin.Tex;
  return sampleDifference(uvAdj, pin.Tex, pin.PosH);
}