struct PS_IN {
  noperspective float4 PosH : SV_POSITION;
  noperspective float3 PosL : TEXCOORD0;
  noperspective float3 NormalL : TEXCOORD1;
  noperspective float WiperRubberPos : TEXCOORD2;
  noperspective float WiperRubberDistance : TEXCOORD3;
  noperspective float WiperMapPos : TEXCOORD4;
};

float4 main(PS_IN pin) : SV_TARGET {
  clip(min(min(pin.WiperRubberPos, 1 - pin.WiperRubberPos), pin.WiperRubberDistance));
  return 0;
}

