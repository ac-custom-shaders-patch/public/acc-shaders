#include "../../custom_objects/common/bending.hlsl"
#include "accRainWindscreen.hlsl"

BENDING_FX_IMPL

cbuffer cbCamera : register(b0) {
  float4x4 ksView;
  float4x4 ksProjection;
}

cbuffer cbData : register(CB_DATA) {
  float gInstanceMult;
  float gDistanceThreshold;
  float gResolutionStep;
  float gVelocityMult;
}

cbuffer cbDataObject : register(b11) {
  float4x4 gTransform;
  float3 gMeshOffset;
  float gPad;
}

struct PS_IN_wipers {
  noperspective float4 PosH : SV_POSITION;
  noperspective float3 PosL : TEXCOORD0;
  noperspective float3 NormalL : TEXCOORD1;
};

#ifndef SKINNED_WEIGHT_CHECK
  #define SKINNED_WEIGHT_CHECK(a) (a != 0)
#endif

cbuffer cbBones : register(b13) {
  float4x4 bones[55];
}

cbuffer cbBonesPrev : register(b10) {
  float4x4 bonesPrev[55];
}

float4 toWorldSpace(float4 posL, float4 boneWeights, float4 boneIndices, bool usePrevBuffer){
  float3 posW = 0;
  for (int i = 0; i < 4; i++){
    float weight = boneWeights[i];
    if (SKINNED_WEIGHT_CHECK(weight)){
      uint index = (uint)boneIndices[i];
      float4x4 bone = usePrevBuffer ? bonesPrev[index] : bones[index];
      posW += (mul(bone, posL).xyz + gMeshOffset) * weight;
    }
  }

  return mul(float4(posW, 1), gTransform);
} 

PS_IN_wipers main(VS_IN_skinned vin, uint instanceID : SV_InstanceID) {
  PS_IN_wipers vout;

  float4 posW0 = toWorldSpace(vin.PosL, vin.BoneWeights, vin.BoneIndices, false);
  float4 posW1 = toWorldSpace(vin.PosL, vin.BoneWeights, vin.BoneIndices, true);
  float4 posW = lerp(posW0, posW1, (float)instanceID * gInstanceMult - 0.5);
  float4 posV = mul(posW, ksView);
  float3 velocity = mul(mul(posW0.xyz - posW1.xyz, (float3x3)ksView), (float3x3)ksProjection) * gVelocityMult;
  // float3 velocity = mul(posW0.xyz - posW1.xyz, (float3x3)ksView) * 25;
  vout.PosH = mul(posV, ksProjection);
  // vout.PosH.xy = frac(vout.PosH.xy * 0.5 + 0.5) * 2 - 1;
  // vout.PosH.z = 0;
  // vout.PosH.w = 1;
  vout.PosL = vin.PosL.xyz;
  vout.NormalL = velocity; // TODO: car transform?
  return vout;
}
