#include "accRainDrops.hlsl"

[numthreads(64, 16, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  // float2 basePosXZ = gStreamPointA + float2(float(threadID.x), float(threadID.y)) * gStreamStep;

  // if (gDensity < RAND) return;
  // float3 dir = rainDropDir(RAND);
  // float3 pos = rainDropPos(basePosXZ, dir, RAND);

  StreamSource S = gStreamSources[threadID.y];
  RAND_INIT(S.p1.xz + threadID.x);

  uint type = (uint)S.type;
  float intensity = frac(S.type) / 0.99;
  float visible = lerp(saturate(gWetnessLevel_ * 10) * 0.2, 1, gDensity) - RAND;
  if (!type || RAND > intensity || visible < 0) return;

  float3 pos = S.p1 + gGraphicsOffset;
  if (type == 1 || type == 101) pos = lerp(S.p1, S.p2, threadID.x / 256.) + gGraphicsOffset;
  if (type == 2 && threadID.x > 64) return;
  // 512 / 64 = 8

  RainDrop_ext drop;

  float3 dir = normalize(gRainDir + float3(0, -3, 0));
  float timeHang = STREAM_TIME_HANG * (1 + RAND);
  float phaseBase = fmod(RAND * (STREAM_TIME_FLY + timeHang) + gTime * 1, STREAM_TIME_FLY + timeHang) - timeHang;
  float phase = phaseBase < 0 ? phaseBase / timeHang : phaseBase / STREAM_TIME_FLY;
  pos += dir * 0.5 * -STREAM_GRAVITY * pow(max(phase * STREAM_TIME_FLY, 0), 2);

  drop.pos = pos + (float3(RAND - 0.5, 0, RAND - 0.5)) * lerp(0.01, 0.1, phase);
  drop.dir = normalize(S.p2 - S.p1);
  drop.fade = phase;
  drop.size = (0.2 + RAND) * lerp(0.015, 0.04, saturate(phase * 3)) * saturate(visible * 20 + saturate(gDensity * 20 - 19));
  drop.ext1 = saturate(drop.fade * 2.6 - 0.2);
  drop.ext3 = -lerp(float3(0, -1, 0), float3(0, -3, 0) + gRainDir, drop.ext1);

  if (type == 101){
    float2 wall = normalize((S.p1 - S.p2).zx) * float2(1, -1);
    drop.ext3 = lerp(-float3(wall.x, 0, wall.y), drop.ext3, saturate(phase * 10));
  }

  buResultStreams.Append(drop.pack());
}