#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float Random : TEXCOORD1;
  float Life : TEXCOORD2;
};

#define MAX_ITEMS 2048
cbuffer cbRipple : register(b10) {
  float4 gOpacityPad;
  float4 gItems[MAX_ITEMS];
}

float4 main(VS_Copy pin) : SV_TARGET {
  float dist = length(pin.Tex);
  float2 nm = pin.Tex;
  float opacity = saturate(pin.Life * 2 - dist * 0.25);
  opacity *= saturate(remap(dist, 0.95, 1, 1, 0));
  opacity *= saturate(remap(dist - lerp(0, 0.2, saturate(pin.Life)), 0.4, 0.3, 1, 0));
  // opacity *= saturate(remap(dist, 0.9, 0.8, 1, 0));

  float cosInput = (1 - dist + lerp(0.5, 0, saturate(pin.Life * 2)) - pin.Random) * 20;
  // float cosInput = (1 - dist + lerp(0.5, 0, saturate(pin.Life * 2)) - pin.Random) * 5;
  opacity *= pow(saturate(cos(cosInput)), 1) * gOpacityPad.x;
  // opacity *= saturate(remap(dist, 0.65, 0.6, 1, 0));

  // nm = lerp(-pin.Tex, pin.Tex, saturate(remap(dist, 0.9, 1.0, 0, 1)));
  nm = lerp(-pin.Tex, pin.Tex, saturate(-sin(cosInput) * 40 * 0.5 + 0.5)) * saturate(opacity * 10) * saturate(pin.Life * 2 - 0.8) * gOpacityPad.x;

  // if (sin(cosInput) < 0) nm *= -1;

  // opacity = 

  float shortSpot = saturate(remap(dist, 0.15, 0.25, 1, 0)) * saturate(remap(dist, 0.2, 0.1, 1, 0)) * pow(saturate(pin.Life * 2 - 1), 2);
  clip(max(opacity, shortSpot) - 0.001);
  // nm = 0;

  // return float4(nm * float2(-0.5, 0.5) + 0.5, 1, shortSpot);
  // return float4(0..xx + 0.5, opacity, shortSpot);
  // return float4(nm * opacity * float2(-0.5, 0.5) + 0.5, opacity, shortSpot * 0.8);
  return float4(nm, opacity, shortSpot * 0.8);
}