#include "accRainWindscreen.hlsl"
#include "include/poisson.hlsl"

Texture2D txWiper : register(t0);
Texture2D txPrevious : register(t1);

float blur(float2 uv) {
  float ret = 0;

  #define PUDDLES_DISK_SIZE 16
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.03;
    ret += txWiper.SampleLevel(samLinearBorder0, uv + offset, 3.5).a;
  }

  return ret / PUDDLES_DISK_SIZE;
}

float2 D(float v){
  return float2(ddx(v), ddy(v));
}

cbuffer cbData : register(b11) {
  float4x4 gWipersTransform2;
  float4x4 gViewProjInv;
}

float4 main(PS_IN_wipersProject pin) : SV_TARGET {
  float4 previous = txPrevious.Load(int3(pin.PosH.xy, 0));
  float4 shape = txWiper.SampleLevel(samPointBorder0, pin.TexWiper, 0);
  float4 shapeLinear = txWiper.SampleLevel(samLinearBorder0, pin.TexWiper, 0);

  if (any(abs(pin.TexWiper - 0.5) > 0.499)){
    shape = 0;
    shapeLinear = 0;
  }

  float3 velocityW = mul(float3(shape.xy * 2 - 1, 0), (float3x3)gViewProjInv);
  float3 dirX = normalize(ddx(pin.PosW));
  float3 dirY = normalize(ddy(pin.PosW));

  float2 velocity = abs(dot(velocityW, 1)) > 0.001 ? float2(dot(velocityW, dirX), dot(velocityW, dirY)) * 0.5 + 0.5 : 0.5;
  return float4(
    lerp(previous.xy, velocity, saturate(shape.w * 10)), 
    shape.z ? shape.z : previous.z, 
    shape.w);
}