struct RippleItem {
  float2 pos;
  float size;
  float life;
};

#define MAX_ITEMS 2048
cbuffer cbRipple : register(b10) {
  float4 gOpacityPad;
  float4 gItems[MAX_ITEMS];
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float Random : TEXCOORD1;
  float Life : TEXCOORD2;
};

static const float2 BILLBOARD[] = {
	float2(-1, -1),
	float2(1, -1),
	float2(-1, 1),
	float2(-1, 1),
	float2(1, -1),
	float2(1, 1),
};

static const float2 OFFSET[] = {
	float2(-2, -2),
	float2(-2, 0),
	float2(-2, 2),
	float2(0, -2),
	float2(0, 0),
	float2(0, 2),
	float2(2, -2),
	float2(2, 0),
	float2(2, 2),
};

VS_Copy main(uint id: SV_VertexID) {
  float4 item = gItems[id / (6 * 9)];
  float2 offset = OFFSET[(id / 6) % 9];
  float2 vertex = BILLBOARD[id % 6];

  VS_Copy vout;
  vout.Tex = vertex;
  vout.PosH = float4(offset + item.xy + vertex * lerp(0.03, 0.09, item.z) * saturate(2 - item.w * 2), 0, 1);
  vout.Random = frac(item.z * 1357);
  vout.Life = item.w;
  // vout.Opacity = 1;
  // vout.PosH = float4(vertex, 0, 1);
  return vout;
}
