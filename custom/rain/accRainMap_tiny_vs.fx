#include "include/common.hlsl"
#include "include/samplers_vs.hlsl"

#define MAX_ITEMS 128
cbuffer cbRipple : register(b10) {
	float4 gShadowSamplePos;
  float4 gItems[MAX_ITEMS];
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float Random : TEXCOORD1;
};

static const float2 BILLBOARD[] = {
	float2(-1, -1),
	float2(1, -1),
	float2(-1, 1),
	float2(-1, 1),
	float2(1, -1),
	float2(1, 1),
};

static const float2 OFFSET[] = {
	float2(-2, -2),
	float2(-2, 0),
	float2(-2, 2),
	float2(0, -2),
	float2(0, 0),
	float2(0, 2),
	float2(2, -2),
	float2(2, 0),
	float2(2, 2),
};

Texture2DArray<float> txRainShadow : register(t0);

#include "include/poisson.hlsl"
float2 sampleShadow(float2 uv, float comparison){
  float2 sum = 0;
  float sampleScale = 0.02;
  if (comparison >= 1) return 1; 

  #define RAIN_DISK_SIZE 4
  for (uint i = 0; i < RAIN_DISK_SIZE; ++i) {
    float2 sampleUV = uv.xy + SAMPLE_POISSON(RAIN_DISK_SIZE, i) * sampleScale;
    sum.x += txRainShadow.SampleCmpLevelZero(samShadow, float3(sampleUV, 0), comparison);
    sum.y += txRainShadow.SampleCmpLevelZero(samShadow, float3(sampleUV, 1), comparison);
  }
  return saturate(float2(sum.x, sum.y) / RAIN_DISK_SIZE);
}

VS_Copy main(uint id: SV_VertexID) {
  float4 item = gItems[id / (6 * 9)];
  float2 offset = OFFSET[(id / 6) % 9];
  float2 vertex = BILLBOARD[id % 6];

	if (gShadowSamplePos.w != 0 && frac(item.z * 2577.2291) > sampleShadow(gShadowSamplePos.xy, gShadowSamplePos.z).x){
		DISCARD_VERTEX(VS_Copy);
	}

  VS_Copy vout;
  vout.Tex = vertex;
  vout.PosH = float4(offset + item.xy + vertex * lerp(0.04, 0.06, item.z) * 0.9, 0, 1);
  vout.Random = lerp(0.5, 1, frac(item.z * 3357.61267));
  return vout;
}
