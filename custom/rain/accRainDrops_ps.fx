#define PARAM_BLUR gStretching
#define PARAM_USE_COLOR gUseColor
#include "accRainDrops.hlsl"

#define ALLOW_EXTRA_FEATURES
#include "refraction.hlsl"

float stth(float v) { return abs(v - floor(v) - 0.5); }

float hash12(inout float2 p){
    float3 p3  = frac(float3(p.xyx) * .1031);
    p3 += dot(p3, p3.yzx + 33.33);
    p.x += 0.432812;
    p.y += 0.936645;
    return frac((p3.x + p3.y) * p3.z);
}

PS_OUT main(PS_IN pin) {
  INTERIOR_TEST;
  float3 toCamera = normalize(pin.PosC);
  float softK = calculateSoft(pin.PosH, pin.PosC, PARAM_BLUR);

  #ifdef SNOW
  {  
    // lighting = pin.LightVal;
    float3 lighting = AMBIENT_COLOR_NEARBY + ksLightColor.rgb * (pin.Shadow * 0.3) + pin.LightVal * 0.2;
    float alpha = 1;

    // float2 rand = float2(pin.NoiseOffset, frac(pin.NoiseOffset * 17.38928 + 1.212655));
    float rand = pin.NoiseOffset;
    #define RAND (rand = frac(rand * 7.816454365 + 0.48575921))
    // #define RAND hash12(rand)

    float distant = lerpInvSat(dot(abs(fwidth(pin.Tex)), 1), 0.1, 0.5);
    pin.Tex = rotate2d(pin.Tex, RAND * M_PI * 2);
    pin.Tex.x /= lerp(0.5 + 0.5 * RAND, 1, distant);

    float radd = atan2(pin.Tex.x, pin.Tex.y) + M_PI / 6;
    float hex = stth(radd / M_PI * 3);
    float dist = length(pin.Tex) * (1 + hex * smoothstep(0, 1, RAND) * 0.2);

    float shape = lerpInvSat(dist, 1, 0.5);
    if (gHitsIntensity < 0) {
      shape = lerpInvSat(dist, 1, 0.3);
      lighting *= 0.01;
    } else if (distant < 1) {
      // if (dist > 1) discard;
      float dist2 = length(pin.Tex) * (1 - pow(hex, 0.5) * lerp(-2, 2, smoothstep(0, 1, RAND)));
      float dist3 = length(pin.Tex) * (1 + pow(hex, 0.5) * lerp(-1, 1, smoothstep(0, 1, RAND)));
      shape *= lerp(saturate(4 * max(
        max(0, 0.05 * (1 + RAND) - hex * dist),
        (0.2 * (0.5 + RAND) - stth(dist2 * lerp(2, 1.5, RAND))) * 0.5) 
        * (1 - saturate(stth(dist3 * 2) * 3 - 0.5)))
        , 1, 0.1 + 0.9 * pow(distant, 4));
    }

    alpha = saturate(shape);
    lighting *= 4;
    // lighting += (1 - distant) * saturate(alpha * 10 - 9) * sampleEnv(float3(0, 1, 0), 0, 0);

    // if (pin.NoiseOffset > 9){
    //   lighting = float3(4, 0, 0);
    //   alpha *= 10;
    // }

    RETURN_BASE(lighting, saturate(alpha));
  }
  #endif

  float fresnel = 0;
  #ifdef USE_FAST_WAY

    float alpha = saturate(remap(abs(pin.Tex.y), 0.7, 1, 1, 0));
    alpha *= saturate(remap(length(pin.Tex), 0.5, 1, 1, 0)) * softK;

    #ifdef USE_COLOR_BUFFER
      float3 lighting = txPrevFrame.SampleLevel(samLinearClamp, pin.PosH.xy * extScreenSize.zw, alpha).rgb;
    #else
      float3 lighting = sampleEnv(toCamera, 4 + alpha, 1);
    #endif

    pin.Tex.y = saturate(remap(abs(pin.Tex.y), lerp(0.9, 0.8, abs(pin.Tex.x)), 1, 0, 1)) * sign(pin.Tex.y);
    float facingShare = sqrt(saturate(1 - dot2(pin.Tex)));
    float3 normal = normalize(pin.DirUp * pin.Tex.y * lerp(1, 4, gStretching)
      + pin.DirSide * pin.Tex.x
      + -toCamera * facingShare);

  #else

    float4 noise = txNoise.SampleLevel(samLinearSimple, pin.Tex * 0.07 + pin.NoiseOffset, 0);
    pin.Tex *= 1 + noise.xy * 0.1;

    float sharpness = lerp(0.95, 0, abs(pin.Tex.y) * gStretching);
    float facingShare = sqrt(saturate(1 - dot2(pin.Tex)));
    // if (pin.Tex.y < 0) pin.Tex.y *= lerp(1, 1.4, abs(pin.Tex.y));
    float3 normal = normalize(pin.DirUp * pin.Tex.y * lerp(1, 4, gStretching)
      + pin.DirSide * pin.Tex.x
      + -toCamera * facingShare);

    float fresnelBase = saturate(pow((1 + dot(toCamera, normal)) * lerp(1, 2, gStretching), 3));
    fresnel = max(fresnelBase, saturate(abs(pin.Tex.y) * 1.3) * gStretching);

    float alpha = saturate(remap(length(pin.Tex), sharpness, 1, 1, 0)) * softK;
    float3 refraction = lerp(toCamera, -normal, 0.5);
    float3 reflection = reflect(toCamera, normal);
    float3 reflectionColor = sampleEnv(reflection, lerp(3, 5, gStretching), 0);
    float3 refractionColor = sampleEnv(refraction, lerp(1, 3, gStretching), 0);

    if (PARAM_USE_COLOR){
      refractionColor = calculateRefraction(pin.PosH, pin.PosC, toCamera, normal).rgb;
    }
    
    float3 lighting = lerp(refractionColor, reflectionColor, fresnel);
    pin.Opacity = 1;

  #endif

  #ifdef USE_FAST_WAY
    lighting += pin.LightVal * gAlphaMult * 0.07;
  #else
    lighting += pin.LightVal * max(fresnel * pow(saturate(dot(pin.LightDir, toCamera)), 20) * 0.4,
      lerp(0.02, simpleReflectanceModel(-toCamera, pin.LightDir, normal), pin.LightFocus));
  #endif
  alpha *= pin.Opacity * interiorMult;

  #ifdef USE_FAST_WAY
    lighting += AMBIENT_COLOR_NEARBY * 0.02;
    alpha *= gAlphaMult * GAMMA_OR(1.4, 1);
  #endif

  #ifdef SNOW_
    // lighting = pin.LightVal;
    lighting = AMBIENT_COLOR_NEARBY * 0.5;
    alpha = 1;

    float radd = atan2(pin.Tex.x * 2 - 1, pin.Tex.y * 2 - 1);
    clip(frac(radd * 3) - 0.5);
  #endif
    // lighting = float3(2, 0, 0);
    // alpha = 1;

  RETURN_BASE(lighting, saturate(alpha));
}