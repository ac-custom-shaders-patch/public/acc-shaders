#include "include/common.hlsl"
#include "include/packing_cs.hlsl"
#include "include/rain.hlsl"

#ifdef SNOW
  #undef USE_RAIN_DIR_FIXED
#endif

#define STREAM_TIME_HANG 0.5
#define STREAM_TIME_FLY 2
#define STREAM_GRAVITY -9.81

struct RainDrop {
  float3 pos;
	float size;

  float3 dir;
	float fade;
};

struct RainDrop_packed {
  float3 pos;
	float size;

  uint2 dir_fade;
  uint2 ext3_ext1;
};

struct RainDrop_ext : RainDrop {
  float3 ext3;
  float ext1;

  RainDrop_packed pack(){
    RainDrop_packed ret;
    ret.pos = pos;
    ret.size = size;
    __pack(ret.dir_fade, dir, fade);
    __pack(ret.ext3_ext1, ext3, ext1);
    return ret;
  }
};

RainDrop_ext unpack(RainDrop_packed packed){
  RainDrop_ext ret;
  ret.pos = packed.pos;
  ret.size = packed.size;
  __unpk(packed.dir_fade, ret.dir, ret.fade);
  __unpk(packed.ext3_ext1, ret.ext3, ret.ext1);
  return ret;
}

inline float rand(inout float seed, in float2 uv) {
	float result = frac(sin(seed * dot(uv, float2(12.9898, 78.233))) * 43758.5453);
	seed += 1.0;
	return result;
}

#if defined(TARGET_VS) || defined(TARGET_PS)

  #if defined(TARGET_PS)
    #ifdef PARTICLES_WITHOUT_NORMALS
      #define GBUFF_NORMAL_W_SRC float3(0, 1, 0)
      #define GBUFF_NORMAL_W_PIN_SRC float3(0, 1, 0)
    #endif
    #include "include_new/base/_include_ps.fx"
    // Texture2D txMain : register(TEXTURE_SLOT); 
  #endif

  cbuffer cbVPSBuffer : register(b11) {
    float gStretching;
    uint gUseColor;
    float gAlphaMult;
    float gHitsIntensity;	

    float3 gRainDir;
    float gCarCollision;
    
    float4x4 gCarCollisionTransform;
  };

  #ifdef TARGET_PS
    Texture2D<float> txCarCollision : register(t1);
    float checkCarCollision(float3 pos){
      float4 uv = mul(float4(pos, 1), gCarCollisionTransform);
      return !gCarCollision || any(abs(uv.xyz - 0.5) > float3(0.5, 0.5, 0.48)) ? 1 : saturate((txCarCollision.SampleLevel(samLinear, uv.xy, 0) - uv.z) * 50);
    }
  #endif

	struct PS_IN {
    float4 PosH : SV_POSITION;

    #ifndef MODE_GBUFFER
      float2 Tex : TEXCOORD1;

      #ifdef WINDSCREEN_SPLASHES
        float3 Velocity : TEXCOORD2;
      #else
        float3 PosC : POSITION;
        float Fog : TEXCOORD6;
        float Opacity : COLOR2;
        // float StretchedK : COLOR3;
        // float EdgeK : COLOR4;
        float3 LightVal : COLOR5;
        float3 LightDir : POSITION3;
        float LightFocus : POSITION4;
        float Shadow : COLOR6;
        float3 DirUp : POSITION1;
        float3 DirSide : POSITION2;
        float NoiseOffset : TEXCOORD2;

        #ifdef STREAMS
          float SideK : TEXCOORD3;
          float BlurK : TEXCOORD4;
        #endif
      #endif
    #endif
	};

  #define INTERIOR_TEST\
    float interiorMult = checkCarCollision(pin.PosC + ksCameraPosition.xyz);\
    if (interiorMult == 0){ clip(-1); return (PS_OUT)0; }

  #include "../common/rainUtils.hlsl"

#elif defined(TARGET_CS)

  #include "include/samplers_vs.hlsl"
  #define RAND rand(seed, uv)
  #define RAND_INIT(x) float seed = 0.12345;float2 uv = fmod(x, 256);
  #define TIME_MULT 1
  // #define TIME_MULT 0.25
  #ifdef SNOW
    #define RAIN_SPEED_BASE 1
  #else
    #define RAIN_SPEED_BASE 15
  #endif
  #define RAIN_SPEED (TIME_MULT * RAIN_SPEED_BASE)

  AppendStructuredBuffer<RainDrop> buResultDrops : register(u0);
  AppendStructuredBuffer<RainDrop> buResultSplashes : register(u1);
  AppendStructuredBuffer<RainDrop> buResultHits : register(u2);
  AppendStructuredBuffer<RainDrop_packed> buResultStreams : register(u3);
  Texture2DArray<float> txRainShadow : register(t0);
  Texture2D<float4> txRainPuddles : register(t1);
  // Texture2D<float> txRoofs : register(t2);

  struct StreamSource {
    float type;
    float3 p1;
    float3 p2;
    float param;
  };

  cbuffer cbCSBuffer : register(b11) {
    float2 gSpawnPointAWorld;
    float2 gSpawnPointA;

    float gMapStep;
    float gTime;
    float gDistanceFadeMult;
    float gPad1;

    float3 gPos;
    uint gTripleFrustum;

    float3 gSize;
    float gClipNearby;

    float4x4 gRainShadowTransform;
    float4x4 gRainShadowTransformInv;

    float4 gViewFrustum1;
    float4 gViewFrustum2;
    float4 gViewFrustum3;
    float4 gViewFrustum4;

    float3 gCameraPosition;
    float gDensity;

    float3 gCameraVelocity;
    float gWetnessLevel_;

    float gWaterLevel;
    float3 gRainDir;

    float3 gRainPos;
    float gSplashesDensity__;

    float3 gRainDirFixed;
    float gPuddleAmount;

    float3 gRainPosFixed;
    float gRainIntensity;

    float3 gGraphicsOffset;
    float gWindDirX;
			
    float3 gRainDirFixed2;
    float gWindDirY;
    
    float3 gRainPosFixed2;
    float gPad5;

    StreamSource gStreamSources[128];
  }

  #define gSplashesDensity 1

  bool isVisible(float3 pos){
    if (gTripleFrustum){
      return gTripleFrustum == 2 
        || dot(gViewFrustum1.xyz, pos) - gViewFrustum1.w > -0.5
        || dot(gViewFrustum2.xyz, pos) - gViewFrustum2.w > -0.5;
    } else {
      return dot(gViewFrustum1.xyz, pos) - gViewFrustum1.w > -0.5
        && dot(gViewFrustum2.xyz, pos) - gViewFrustum2.w > -0.5
        && dot(gViewFrustum3.xyz, pos) - gViewFrustum3.w > -0.5
        && dot(gViewFrustum4.xyz, pos) - gViewFrustum4.w > -0.5;
    }
  }

  float rainShadowEdge(float2 uv, float comparison){
    float mi = 1, ma = 0;
    [unroll] for (int y = -1; y <= 1; y++)
    [unroll] for (int x = -1; x <= 1; x++) {
      if (abs(x) + abs(y) != 2) continue;
      float v = txRainShadow.SampleCmpLevelZero(samShadow, float3(uv, 1), comparison, int2(x, y) * 2);
      mi = min(mi, v);
      ma = max(ma, v);
    }
    return saturate((ma - mi) * 10000);
  }

  float waterThickness(float3 rainPos){
    float4 posH = mul(float4(rainPos, 1), gRainShadowTransform);

    // R: puddleValue, G: reliefValue, B: racingLineOffset (0…1)
    float4 puddleValue = txRainPuddles.SampleLevel(samLinearBorder0, posH.xy, 0);
    puddleValue.xyz = DECODE_NZN_UINT8(puddleValue.xyz);
    puddleValue.b = puddleValue.b * 2 - 1;

    float damp, wetness, water;
    rainCalculate(gPuddleAmount, gRainIntensity, gWetnessLevel_, gWaterLevel, 1, puddleValue.x, puddleValue.y, puddleValue.z, damp, wetness, water);
    return puddleValue.y == 1 ? 100 : water * 5;
  }

  float waterThickness(float3 rainPos, out bool isWaterSurface){
    float4 posH = mul(float4(rainPos, 1), gRainShadowTransform);

    // R: puddleValue, G: reliefValue, B: racingLineOffset (0…1)
    float4 puddleValue = txRainPuddles.SampleLevel(samLinearBorder0, posH.xy, 0);
    puddleValue.xyz = DECODE_NZN_UINT8(puddleValue.xyz);
    puddleValue.b = puddleValue.b * 2 - 1;

    float damp, wetness, water;
    rainCalculate(gPuddleAmount, gRainIntensity, gWetnessLevel_, gWaterLevel, 1, puddleValue.x, puddleValue.y, puddleValue.z, damp, wetness, water);

    isWaterSurface = puddleValue.y == 1;
    return puddleValue.y == 1 ? 100 : water * 5; 
  }

  float3 groundPos(float3 rainPos){
    float4 posH = mul(float4(rainPos, 1), gRainShadowTransform);
    float shadowValue = txRainShadow.SampleLevel(samLinearBorder1, float3(posH.xy, 1), 0);
    return mul(float4(posH.xy, shadowValue, 1), gRainShadowTransformInv).xyz;
  }

  float3 groundPosNoCar(float3 rainPos){
    float4 posH = mul(float4(rainPos, 1), gRainShadowTransform);
    float shadowValue = txRainShadow.SampleLevel(samLinearBorder1, float3(posH.xy, 0), 0);
    return mul(float4(posH.xy, shadowValue, 1), gRainShadowTransformInv).xyz;
  }

  float3 groundPosNoCar(float3 rainPos, out bool groundOccludedByCar){
    float4 posH = mul(float4(rainPos, 1), gRainShadowTransform);
    float shadowValue = txRainShadow.SampleLevel(samLinearBorder1, float3(posH.xy, 0), 0);
    groundOccludedByCar = txRainShadow.SampleLevel(samLinearBorder1, float3(posH.xy, 1), 0) != shadowValue;
    return mul(float4(posH.xy, shadowValue, 1), gRainShadowTransformInv).xyz;
  }

  float3 rainDropRandomDir(float2 rand){
    float mult = 1;
    #ifdef SNOW
      mult = 5;
    #endif
    return normalize(float3(rand.x - 0.5, -5, rand.y - 0.5)) * mult;
  }

  float3 rainDropSummaryDir(float mixParam, float3 randomDir){
    #ifdef SNOW
      return normalize(
        (mixParam > 0.67 ? gRainDirFixed2 : mixParam > 0.33 ? gRainDirFixed : gRainDir)
        + randomDir * float3(1, 0, 1));
    #endif
    #ifdef USE_RAIN_DIR_FIXED
      return normalize(gRainDirFixed + randomDir * float3(1, 0, 1));
    #else
      return normalize(gRainDir + randomDir * float3(1, 0, 1));
    #endif
  }

  float3 rainDropPos(float mixParam, float2 basePosXZ, float3 randomDir, float rand){
    float3 gStart = gPos - gSize / 2;
    float3 pos = float3(basePosXZ.x, 0, basePosXZ.y) 
      #ifdef SNOW
        + (mixParam > 0.67 ? gRainPosFixed2 : mixParam > 0.33 ? gRainPosFixed : gRainPos)
      #elif defined(USE_RAIN_DIR_FIXED)
        + gRainPosFixed 
      #else
        + gRainPos 
      #endif
      + randomDir * float3(1, 0, 1) * gTime * RAIN_SPEED 
      + float3(0, gSize.y * rand, 0);

    float randX = frac(rand * 1397);
    float randZ = frac(rand * 3971);
    float3 ret = frac((pos - gStart) / gSize) * gSize + gStart 
      + float3(randX - 0.5, 0, randZ - 0.5) * gMapStep;
    return ret;
  }

  void alterDropPos(inout float3 pos, float groundPos, float rand) {
    #ifdef SNOW
      // pos.x += sin((max(pos.y, groundPos) + rand * 97.948) * (0.25 + 0.75 * frac(rand * 314.591))) * (0.15 + 0.35 * frac(rand * 256.646));
    #endif
  }

#endif