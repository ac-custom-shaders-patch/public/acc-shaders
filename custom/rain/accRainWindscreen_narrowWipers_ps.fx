#include "include/common.hlsl"
#include "include/poisson.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#define R 2
float4 main(VS_Copy pin) : SV_TARGET {
  float4 o = txDiffuse.SampleLevel(samPoint, pin.Tex, 0);
  // return o;

  float v = o.a;

  [branch]
  if (v == 0){
    return 0;
  }

  float sum = 0;
  float count = 0;
  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    sum += txDiffuse.SampleLevel(samLinear, pin.Tex, 1, int2(x, y) * 2).a;
    count += 1;
  }

  if (fwidth(sum) > 0.5  || sum / count < 0.8) return float4(o.xyz, 0.33);
  return float4(o.xyz, 1);
}