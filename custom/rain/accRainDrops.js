add(`accRainDrops_vs.fx`, { saveAs: `accRainDrops_vs_gbuffer.fxo`, defines: { 'MODE_GBUFFER': 1 } });
add(`accRainSplashes_vs.fx`, { saveAs: `accRainSplashes_vs_gbuffer.fxo`, defines: { 'MODE_GBUFFER': 1 } });
add(`accRainStreams_vs.fx`, { saveAs: `accRainStreams_vs_gbuffer.fxo`, defines: { 'MODE_GBUFFER': 1 } });

add(`accRainHits_vs.fx`, { saveAs: `accSnowHits_vs.fxo`, defines: { 'SNOW': 1 } });
add(`accRainHits_ps.fx`, { saveAs: `accSnowHits_ps.fxo`, defines: { 'SNOW': 1 } });
add(`accRainDrops_vs.fx`, { saveAs: `accSnowDrops_vs.fxo`, defines: { 'SNOW': 1 } });
add(`accRainDrops_vs.fx`, { saveAs: `accSnowDrops_vs_gbuffer.fxo`, defines: { 'SNOW': 1, 'MODE_GBUFFER': 1 } });
add(`accRainDrops_ps.fx`, { saveAs: `accSnowDrops_ps.fxo`, defines: { 'SNOW': 1 } });
add(`accRainDrops_ps_fast.fx`, { saveAs: `accSnowDrops_ps_fast.fxo`, defines: { 'SNOW': 1 } });
add(`accRainDrops_ps_fastColor.fx`, { saveAs: `accSnowDrops_ps_fastColor.fxo`, defines: { 'SNOW': 1 } });
add(`accRainDrops_cs_generation.fx`, { saveAs: `accSnowDrops_cs_generation.fxo`, defines: { 'SNOW': 1 } });
add(`accRainHits_cs_generation.fx`, { saveAs: `accSnowHits_cs_generation.fxo`, defines: { 'SNOW': 1 } });