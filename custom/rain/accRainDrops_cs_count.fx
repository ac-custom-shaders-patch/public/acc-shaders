cbuffer PerDrapeBuffer : register(b11) {
	uint gCount0;
	uint gCount1;
	uint gCount2;
	uint gCount3;
};

RWByteAddressBuffer indirectBuffers : register(u0);

[numthreads(1, 1, 1)]
void main(uint3 id : SV_DispatchThreadID) {
	indirectBuffers.Store4(0, uint4(gCount0 * 6, 1, 0, 0));
	indirectBuffers.Store4(16, uint4(gCount1 * 6, 1, 0, 0));
	indirectBuffers.Store4(32, uint4(gCount2 * 6, 1, 0, 0));
	indirectBuffers.Store4(48, uint4(gCount3 * 6, 1, 0, 0));
}