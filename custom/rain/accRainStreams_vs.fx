#define STREAMS
#include "accRainDrops.hlsl"
#include "include_new/base/_include_vs.fx"
#include "include_new/ext_shadows/_include_vs.fx"

static const float3 BILLBOARD[] = {
  float3(-1, -1, 0),
  float3(1, -1, 0),
  float3(-1, 1, 0),
  float3(-1, 1, 0),
  float3(1, -1, 0),
  float3(1, 1, 0),
};

StructuredBuffer<RainDrop_packed> particleBuffer : register(t3);

#ifndef MODE_MAIN_NOFX
	#include "include_new/base/common_ps.fx"
	#include "include_new/ext_lightingfx/_include_ps.fx"
#endif

PS_IN main(uint fakeIndex : SV_VERTEXID) {
  uint vertexID = fakeIndex % 6;
  uint instanceID = fakeIndex / 6;
  RainDrop_ext particle = unpack(particleBuffer[instanceID]);
  float3 quadPos = BILLBOARD[vertexID];

	float3 billboardAxis = normalize(particle.pos - ksCameraPosition.xyz);
	float distance = length((particle.pos - ksCameraPosition.xyz).xz);
	#ifdef MODE_SHADOW
		billboardAxis = ksLightDirection.xyz;
	#endif

	float blurK = particle.ext1;

	// float3 upMoving = -normalize(lerp(float3(0, -1, 0), float3(0, -3, 0) + gRainDir, blurK));
	// float3 sideMoving = -normalize(cross(billboardAxis, upMoving));
	// float3 sideHanging = normalize(cross(billboardAxis, extDirUp));
	// float3 upHanging = normalize(cross(sideHanging, billboardAxis));
	// float3 up = normalize(lerp(upHanging, upMoving, saturate(particle.fade * 2)));
	// float3 side = normalize(lerp(sideHanging, sideMoving, saturate(particle.fade * 2)));

	float3 up = normalize(particle.ext3);
	float3 side = normalize(cross(up, billboardAxis));

  float upK = particle.size + (gStretching ? 0.5 : 0) * blurK;
  float sideK = (1 + saturate(1 - particle.fade) * abs(dot(side, particle.dir))) 
		* particle.size * lerp(1, 0.5, saturate(blurK * 4)) * 0.75;

  if (particle.fade > 0){
    sideK /= 2;
  }

	// upK = 0.4;
	// sideK = 0.4;

	float3 offset = (up * quadPos.y * upK + side * quadPos.x * sideK);

	#ifdef MODE_GBUFFER
		// offset *= 10;
	#endif

  // particle.pos.y -= particle.size * lerp(1, 0, pow(saturate(-particle.fade), 0.5));

  particle.pos -= billboardAxis * particle.size;
	float4 posW = float4(particle.pos, 1);
	posW.xyz += offset;

	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

	PS_IN vout;
	vout.PosH = posH;

	#ifndef MODE_GBUFFER
		vout.Tex = quadPos.xy;
		vout.Opacity = 1;
		vout.NoiseOffset = particle.fade;

		vout.DirUp = up;
		vout.DirSide = side;
		vout.SideK = abs(dot(side, particle.dir));
		vout.BlurK = blurK;

		float3 toCamera = posW.xyz - ksCameraPosition.xyz;
		COMMON_SHADING(vout);
	#endif

	return vout;
}