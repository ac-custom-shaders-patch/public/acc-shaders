#include "include/common.hlsl"
Texture2D<float> txHR : register(t0);
Texture2D<float> txLR : register(t1);
Texture2D<float> txWireframe : register(t2);
Texture2D<float> txDetailedMask : register(t3);
Texture2D<float> txPuddles : register(TX_SLOT_RAIN_PUDDLES_PATTERN);
Texture2D txNoise : register(TX_SLOT_NOISE);

cbuffer cbPiece : register(b11) {
  float4x4 gViewProj;
  float2 gArea0;
  float2 gArea1;
};

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};
  
#include "include/poisson.hlsl"

float main(VS_Copy pin) : SV_TARGET {
  float neutral = txHR.SampleLevel(samLinearClamp, pin.Tex, 0);
  float currentShot = neutral;
  float total = 1;

  float2 random = normalize(txNoise.SampleLevel(samPoint, pin.PosH.xy / 32, 0).xy);

  #define PUDDLES_DISK_SIZE 32
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i);
    // offset = reflect(offset, random);
    float val = txHR.SampleLevel(samPoint, pin.Tex + offset * 0.1, 0);
    if (val == 0 || val == 1) continue;
    currentShot += val;
    total++;
  }

  // float2 rotate_at = float2(frac(10000 * uv.x), frac(11000 * uv.y));

  // return txNoise.SampleLevel(samPoint, pin.Tex*7, 0);

  float avg = currentShot / total;
  return max(neutral - avg, 0) * 1000;
}