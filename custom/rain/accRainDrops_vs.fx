#include "accRainDrops.hlsl"
#include "include_new/base/_include_vs.fx"
#include "include_new/ext_shadows/_include_vs.fx"

static const float3 BILLBOARD[] = {
  float3(-1, -1, 0),
  float3(1, -1, 0),
  float3(-1, 1, 0),
  float3(-1, 1, 0),
  float3(1, -1, 0),
  float3(1, 1, 0),
};

StructuredBuffer<RainDrop> particleBuffer : register(t0);

#ifndef MODE_MAIN_NOFX
	#include "include_new/base/common_ps.fx"
	#include "include_new/ext_lightingfx/_include_ps.fx"
#endif

PS_IN main(uint fakeIndex : SV_VERTEXID) {
  uint vertexID = fakeIndex % 6;
  uint instanceID = fakeIndex / 6;
  RainDrop particle = particleBuffer[instanceID];
  float3 quadPos = BILLBOARD[vertexID];

	float3 billboardAxis = normalize(particle.pos - ksCameraPosition.xyz);
	float distance = length((particle.pos - ksCameraPosition.xyz).xz);
	#ifdef MODE_SHADOW
		billboardAxis = ksLightDirection.xyz;
	#endif
	
	float stretchedK = gStretching;
	// float stretchedK = 0.5 + 0.5 * sin(ksGameTime * 0.001);

	#ifdef SNOW
		float3 side = normalize(cross(billboardAxis, -particle.dir));
		float3 up = normalize(cross(billboardAxis, side));
		
		float4 posW_ = float4(particle.pos, 1);
		float4 posV_ = mul(posW_, ksView);
		posV_.xy /= abs(posV_.z);
		stretchedK *= saturate(length(posV_.xy));

		float heightMult = lerp(1, 1 + length(particle.dir) / 10, stretchedK);
		float3 offset = (up * quadPos.y * heightMult + side * quadPos.x) * lerp(0.01, 0.02, abs(gHitsIntensity));
	#else
		float distantFix = 1 + distance * 0.08 / max(extCameraTangent, 1);

		float3 side = normalize(cross(billboardAxis, -particle.dir));
		float3 up = normalize(cross(billboardAxis, side));
		// float3 side = normalize(cross(billboardAxis, -particle.dir));

		float widthMult = lerp(0.03, 0.04 * distantFix, stretchedK);
		float heightMult = lerp(0.03, 1, stretchedK);

		// widthMult *= lerp(1, 1.2, extSceneRain);
		heightMult *= lerp(1, 1.8, extSceneRain);
		heightMult = lerp(widthMult, heightMult, abs(dot(up, normalize(particle.dir))));

		float3 offset = (up * quadPos.y * heightMult + side * quadPos.x * widthMult) * particle.size;
	#endif


	float4 posW = float4(particle.pos + offset, 1);
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

	PS_IN vout;
	vout.PosH = posH;

	#ifndef MODE_GBUFFER
		vout.Tex = quadPos.xy;
		vout.Opacity = particle.fade;
		#ifdef SNOW
			vout.NoiseOffset = particle.size;
		#else
			vout.NoiseOffset = dot(particle.pos, 0.1);
		#endif

		vout.DirUp = up;
		vout.DirSide = side;

		#ifndef MODE_SHADOW
			vout.PosC = posW.xyz - ksCameraPosition.xyz;
			#ifdef MODE_GBUFFER
				GENERIC_PIECE_VELOCITY(posW, particle.velocity * gDeltaTime);
			#else
				vout.Fog = calculateFog(posV);
				float3 toCamera = vout.PosC;
				CALCULATE_LIGHTING(vout);
				// vout.LightVal = 100;

				#ifdef SNOW
					// vout.LightVal += ksLightColor.rgb * getShadowBiasMult(vout.PosC, 0, float3(0, 1, 0), mul(float4(posW.xyz, 1), ksShadowMatrix0), 0, 1);
				#endif
			#endif
		#endif
	#endif

	return vout;
}