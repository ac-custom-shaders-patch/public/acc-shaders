#include "include/common.hlsl"

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  // return float4(pin.Tex, 0, 1);
  return float4(0, 0, 0, txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0) * 0.1);
  // return 0;
}