#define SPS_NO_POSC
#include "accRainWindscreen.hlsl"

cbuffer cbCamera : register(b0) {
  float4x4 ksView_base;
  float4x4 ksProjection_base;
}

struct PS_IN_wipers {
  float4 PosH : SV_POSITION;
};

#include "../../recreated/include_new/base/sps_utils.fx"

PS_IN_wipers main(VS_IN_ac vin SPS_VS_ARG) {
  PS_IN_wipers vout;
  float4 posW = mul(vin.PosL, gWorld);
  float4 posV = mul(posW, ksView);
  vout.PosH = mul(posV, ksProjection);
  SPS_RET(vout);
}
