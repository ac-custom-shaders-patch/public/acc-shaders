#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#define R 1
float4 sampleBlur(float2 uv){
  float4 currentShot = 0;
  float totalWeight = 0;
  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    float weight = 1;
    currentShot += txDiffuse.SampleLevel(samLinearSimple, uv, 1, int2(x, y) * 2) * weight;
    totalWeight += weight;
  }
  return currentShot / totalWeight;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float4 prev = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);
  float4 prevB = sampleBlur(pin.Tex);
  float thick = prev.x;
  float running = prevB.y;

  if (thick > 1.2 || thick > 0 && running > 0.05){
    running = max(pow(thick * 2, 2), running);
    thick = 0;
  }

  return float4(saturate(thick - 0.0001*0), saturate(running - 0.02), (prevB.y - prev.y) * 10, 1);
}