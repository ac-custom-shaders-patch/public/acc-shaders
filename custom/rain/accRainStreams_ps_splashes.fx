#define STREAMS
#define WINDSCREEN_SPLASHES
#include "accRainDrops.hlsl"

float4 main(PS_IN pin) : SV_TARGET {
  float length = dot2(pin.Tex);
  clip(1 - length);
  return float4(pin.Velocity, saturate(remap(length, 0.2, 1, 1, 0)));
}