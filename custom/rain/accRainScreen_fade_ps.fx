#include "include/common.hlsl"

Texture2D<float> txDiffuse : register(t0);

cbuffer cbData : register(b10) {
	float gAspectRatio;
	float gFade;
  float2 gScreenSizeInv;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

// cbuffer cbData : register(b10) {
//   float gOffset;
//   float3 gPad;
// }

float main(VS_Copy pin) : SV_TARGET {
  // return 0;
  float v = txDiffuse.Load(int3(pin.PosH.xy, 0));
  v = min(v, 0.8) - gFade * 2;
  // v = min(v, 0.8) - gFade * 0;
  return v;
}