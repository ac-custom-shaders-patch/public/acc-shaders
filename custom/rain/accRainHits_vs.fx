#define SPLASHES
#include "accRainDrops.hlsl"
#include "include_new/base/_include_vs.fx"
#include "include_new/ext_shadows/_include_vs.fx"

static const float3 BILLBOARD[] = {
  float3(-1, -1, 0),
  float3(1, -1, 0),
  float3(-1, 1, 0),
  float3(-1, 1, 0),
  float3(1, -1, 0),
  float3(1, 1, 0),
};

StructuredBuffer<RainDrop> particleBuffer : register(t2);

#ifndef MODE_MAIN_NOFX
	#include "include_new/base/common_ps.fx"
	#include "include_new/ext_lightingfx/_include_ps.fx"
#endif

PS_IN main(uint fakeIndex : SV_VERTEXID) {
  uint vertexID = fakeIndex % 6;
  uint instanceID = fakeIndex / 6;
  RainDrop particle = particleBuffer[instanceID];
  float3 quadPos = BILLBOARD[vertexID];

	float3 billboardAxis = normalize(particle.pos - ksCameraPosition.xyz);
	float distance = length((particle.pos - ksCameraPosition.xyz).xz);
	#ifdef MODE_SHADOW
		billboardAxis = ksLightDirection.xyz;
	#endif

	#ifdef SNOW
		billboardAxis = float3(0, -1, 0);
		float3 side = float3(0, 0, 1);
		// particle.size = 1;
		// float3 side = normalize(cross(billboardAxis, float3(0, -1, 0)));
		float3 up = normalize(cross(billboardAxis, side));
		float3 offset = (up * quadPos.y + side * quadPos.x) * particle.size;
  	// particle.pos += normalize(ksCameraPosition.xyz - particle.pos) * 0.2;
	#else
		float3 side = normalize(cross(billboardAxis, float3(0, -1, 0)));
		float3 up = normalize(cross(billboardAxis, side));
		float3 offset = (up * max(0, quadPos.y) + side * quadPos.x) * particle.size;
		particle.pos -= billboardAxis * particle.size;
	#endif

	float4 posW = float4(particle.pos + offset, 1);
	float4 posV = mul(posW, ksView);
	#ifdef SNOW
		posV.xyz *= max(0.5, 1 - 0.1 / length(posV.xyz));
	#endif
	float4 posH = mul(posV, ksProjection);

	PS_IN vout;
	vout.PosH = posH;

	#ifndef MODE_GBUFFER
		vout.Tex = quadPos.xy;
		vout.Opacity = particle.fade;
		vout.NoiseOffset = particle.dir.x;

		vout.DirUp = up;
		vout.DirSide = side;

		#ifndef MODE_SHADOW
			vout.PosC = posW.xyz - ksCameraPosition.xyz;
			#ifdef MODE_GBUFFER
				GENERIC_PIECE_VELOCITY(posW, particle.velocity * gDeltaTime);
			#else
				vout.Fog = calculateFog(posV);
		
				#ifndef MODE_MAIN_NOFX
					float4 txDiffuseValue = 1;
					float3 posCPerVertexLighting = vout.PosC;
					LFX_MainLight mainLight;
					float3 lightingFX = 0;					
					float3 toCamera = vout.PosC;
					LIGHTINGFX(lightingFX);
					#ifndef SHADER_MIRROR
						vout.LightFocus = saturate(1.4 * mainLight.power / max(dot(lightingFX, 1), 1));
						vout.LightDir = mainLight.dir;
					#endif
					vout.LightVal = lightingFX;
					vout.Shadow = getShadowBiasMult(vout.PosC, 0, float3(0, 1, 0), mul(float4(posW.xyz, 1), ksShadowMatrix0), 0, 1);
				#endif
			#endif
		#endif
	#endif

	return vout;
}