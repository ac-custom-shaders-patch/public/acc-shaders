RWTexture2D<float> rwWiperDirt : register(u7);

[numthreads(16, 16, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  uint2 p = uint2(threadID.y * 16 + threadID.x, 0);
  rwWiperDirt[p] = max(0, rwWiperDirt[p] * 0.95 - 0.05);
}