#include "include/common.hlsl"

#if defined(WITH_ANIMATED_WIPERS) || defined(VERTEXLESS)
  cbuffer cbDataObject : register(b11) {
    float4x4 gWorldNow;
    float gPosMult;
    float gPosAdd;
    float gPrevBoost;
  }
#else
  cbuffer cbDataObject : register(b11) {
    float4x4 gWorldNow;
    float4x4 gWorldPrev;
  }
#endif

#ifdef USE_EXTRA_RUBBERS
  struct ExtraRubber {
    float3 rubberFrom;
    float rubberOffsetPos;
    float3 rubberTo;
    float rubberOffsetAmount;
    float rubberOffsetExp;
    uint rubberPartIndex;
    float2 pad;

    float3 rubberLocalPos(float rubberPos, float3 offsetAxis){
      float offsetAmount = rubberPos > rubberOffsetPos
        ? remap(rubberPos, 1, rubberOffsetPos, 0, 1)
        : remap(rubberPos, 0, rubberOffsetPos, 0, 1);
      return lerp(rubberFrom.xyz, rubberTo.xyz, rubberPos)
        + offsetAxis * (pow(saturate(offsetAmount), rubberOffsetExp) * rubberOffsetAmount);
    }
  };

  cbuffer cbExtraRubbers : register(b12) {
    ExtraRubber gExtraRubbers[4];
    uint gWiperID;
    float3 gPad;
  }
#endif

#include "../../custom_objects/common/bending.hlsl"
#include "../../custom_objects/common/animatedWipers.hlsl"
#include "accRainWindscreen.hlsl"

BENDING_FX_IMPL

cbuffer cbCamera : register(b0) {
  float4x4 ksView;
  float4x4 ksProjection;
  float4x4 ksMVPInverse;
}

cbuffer cbData : register(CB_DATA) {
  float gInstanceMult;
  float gIntensityMult;
  float gResolutionStep;
  float gVelocityMult;
}

struct PS_IN_wipers {
  noperspective float4 PosH : SV_POSITION;
  noperspective float3 PosL : TEXCOORD0;
  noperspective float3 NormalL : TEXCOORD1;
  #ifdef WITH_ANIMATED_WIPERS
    noperspective float WiperRubberPos : TEXCOORD2;
    noperspective float WiperRubberDistance : TEXCOORD3;
    noperspective float WiperMapPos : TEXCOORD4;
  #endif
};

static const float2 BILLBOARD[] = {
  float2(0, 0),
  float2(1, 0),
  float2(0, 1),
  float2(0, 1),
  float2(1, 0),
  float2(1, 1),
};

PS_IN_wipers main(
#ifdef VERTEXLESS
  uint fakeIndex : SV_VertexID
#else
  VS_IN_ac vin, uint instanceID : SV_InstanceID
#endif
  ) {

  #ifdef VERTEXLESS

    const uint wiperSamples = 10;
    const uint timeSamples = 2;
    uint vertexID = fakeIndex % 6;
    uint wiperPosID = fakeIndex / 6 % wiperSamples;
    uint timeInterpolationID = fakeIndex / 6 / wiperSamples % timeSamples;

    float2 vertexPos = BILLBOARD[vertexID];
    float rubberPos = ((float)wiperPosID + vertexPos.x) / (float)wiperSamples;

    uint partIndex = 2; /* 2 for turning */
    bool withLagging = true;

    #ifdef USE_EXTRA_RUBBERS
      uint rubberIndex = fakeIndex / 6 / wiperSamples / timeSamples;
      uint wiperIndex = gWiperID;
      float3 posLBase = gExtraRubbers[rubberIndex].rubberLocalPos(rubberPos, gWiperParams[wiperIndex].offsetAxis);
      partIndex = gExtraRubbers[rubberIndex].rubberPartIndex;
      withLagging = false;
    #else
      uint wiperIndex = fakeIndex / 6 / wiperSamples / timeSamples;
      float3 posLBase = gWiperParams[wiperIndex].rubberLocalPos(rubberPos);
    #endif

    float3 posL0 = gWiperParams[wiperIndex].calculateRubber(posLBase, wiperIndex, false, partIndex, withLagging);
    float3 posL1 = gWiperParams[wiperIndex].calculateRubber(posLBase, wiperIndex, true, partIndex, withLagging);
    float3 posW0 = mul(float4(posL0, 1), gWorldNow).xyz;
    float3 posW1 = mul(float4(posL1, 1), gWorldNow).xyz;
    float3 velocityW = posW0.xyz - posW1.xyz;

    float3 posL = 0;
    float3 posW = lerp(posW0, posW1, ((float)timeInterpolationID + vertexPos.y) / (float)timeSamples);
    float2 rubberPosDistance = float2(rubberPos, 0);
    float rubberClipThreshold = 1;

  #else

    #ifdef CREATE_MASK
      float mixValue = 0;
    #else
      float mixValue = (float)instanceID * gInstanceMult - 0.5;
    #endif

    #ifdef WITH_ANIMATED_WIPERS
      float2 rubberPosDistance;
      float rubberClipThreshold, wiperIndex = -1;
      float3 posL0 = animatedWipersCalculate(vin.PosL.xyz, vsLoadMat0b(vin.TangentPacked), false, rubberPosDistance, rubberClipThreshold, wiperIndex);
      if (wiperIndex == -1){
        DISCARD_VERTEX(PS_IN_wipers);
      }

      float3 posL1 = animatedWipersCalculate(vin.PosL.xyz, vsLoadMat0b(vin.TangentPacked), true, rubberPosDistance, rubberClipThreshold, wiperIndex);

      float3 posW0 = mul(float4(posL0, 1), gWorldNow).xyz;
      float3 posW1 = mul(float4(posL1, 1), gWorldNow).xyz;
    #else

      float3 posW0 = mul(vin.PosL, gWorldNow).xyz;
      #ifdef WITH_BENDING
        alterPosBending(vin.PosL.xyz, false, posW0.xyz);
      #endif

      float3 posW1 = mul(vin.PosL, gWorldPrev).xyz;
      #ifdef WITH_BENDING
        alterPosBending(vin.PosL.xyz, true, posW0.xyz);
      #endif

    #endif

    float3 posL = vin.PosL.xyz;
    float3 posW = lerp(posW0, posW1, mixValue);
    float3 velocityW = posW0.xyz - posW1.xyz;

  #endif
  
  PS_IN_wipers vout;
  float3 velocity;

  #ifdef CREATE_MASK
    velocity = 0;
    vout.PosH = mul(float4(posW, 1), ksMVPInverse);
  #else
    float4 posV = mul(float4(posW, 1), ksView);
    velocity = mul(mul(velocityW, (float3x3)ksView), (float3x3)ksProjection) * gVelocityMult;
    vout.PosH = mul(posV, ksProjection);
  #endif

  vout.PosL = posL;
  vout.NormalL = velocity; // TODO: car transform?
  #ifdef WITH_ANIMATED_WIPERS
    vout.WiperRubberPos = rubberPosDistance.x;
    vout.WiperRubberDistance = rubberClipThreshold - rubberPosDistance.y;
    vout.WiperMapPos = (rubberPosDistance.x + wiperIndex) * gPosMult + gPosAdd;
  #endif
  return vout;
}
