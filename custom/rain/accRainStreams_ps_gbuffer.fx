#define CREATE_MOTION_BUFFER

struct PS_IN_gbuffer {
  float4 PosH : SV_POSITION;
};

struct PS_OUT_gbuffer {  
  float4 normal : SV_Target;
  #ifdef CREATE_REFLECTION_BUFFER
    float4 baseReflection : SV_Target1;
    float4 reflectionColorBlur : SV_Target2;
    #ifdef CREATE_MOTION_BUFFER
      float2 motion : SV_Target3;
      float stencil : SV_Target4;
    #endif
  #else
    #ifdef CREATE_MOTION_BUFFER
      float2 motion : SV_Target1;
      float stencil : SV_Target2;
    #endif
  #endif
};

PS_OUT_gbuffer main(PS_IN_gbuffer pin) {
  PS_OUT_gbuffer ret = (PS_OUT_gbuffer)0;
  ret.stencil = 1;
  return ret;
}