#include "accRainWindscreen.hlsl"

// Texture2D txPrevious : register(t1);
// Texture2D txWipers : register(t2);

// #define R 2
// float main(VS_Copy pin) : SV_TARGET {
//   float4 value = txInput.SampleLevel(samLinear, pin.Tex, 0);
//   return value.a;
// }

float get(float2 uv, float level){
  float4 v = txInput.SampleLevel(samLinearClamp, uv, level);
  // return v.z;
  return max(v.z, v.w);
}

float2 getN(float2 uv, float radius, float level, float dir = -1){
  float tc = get(uv, level);
  float tl = get(uv + float2(radius * dir, 0), level);
  float tt = get(uv + float2(0, radius * dir), level);
  return float2(tl - tc, tt - tc) * dir;
}

float2 getNF(float tb, float2 uv, float radius, float level){
  float tl = get(uv - float2(radius, 0), level);
  float tt = get(uv - float2(0, radius), level);
  return -float2(tl - tb, tt - tb);
}

#include "include/poisson.hlsl"
float4 main(VS_Copy pin) : SV_TARGET {
  POISSON_AVG(float4, poisson, txInput, samLinearClamp, pin.Tex, 0.0008, 6);

  float4 result = txInput.SampleLevel(samLinearClamp, pin.Tex, 0);
  float4 blurred = txInput.SampleLevel(samLinearClamp, pin.Tex, 1.5);
  float2 normal = (getN(pin.Tex, 0.0005, 1.5) + getN(pin.Tex, 0.0005, 1.5, 1)) * 2.2;
  float2 normalTex = normal * float2(-0.5, -0.5) + 0.5;

  return float4(
    normalTex, 
    result.z, 
    result.a);
    // txInput.SampleLevel(samLinearClamp, pin.Tex, 1).a);
    // poisson.a);
}
