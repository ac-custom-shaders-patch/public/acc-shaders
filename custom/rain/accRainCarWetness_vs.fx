cbuffer cbData : register(b10) {
  float4 gPad0;
  float4 gPad1;
  float4x4 gTransform;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float3 ShadowTex : TEXCOORD1;
};

VS_Copy main(uint id: SV_VertexID) {
  VS_Copy vout;
  
  float2 offset = float2(id & 1, id >> 1);
  vout.PosH = float4((offset * 2 - 1) * float2(1, -1), 0, 1);
  vout.Tex = offset;

  float3 posW = lerp(float3(0.5, 0, 0.5), float3(-0.5, 0, -0.5), float3(offset.x, 0, offset.y));
  vout.ShadowTex = mul(float4(posW, 1), gTransform).xyz;
  return vout;
}
