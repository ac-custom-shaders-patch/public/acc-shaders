#include "accRainWindscreen.hlsl"

struct PS_IN_wipers {
  noperspective float4 PosH : SV_POSITION;
  noperspective float3 PosL : TEXCOORD0;
  noperspective float3 NormalL : TEXCOORD1;
};

cbuffer cbData : register(CB_DATA) {
  float gInstanceMult;
  float gDistanceThreshold;
  float gResolutionStep;
  float gPad0;
}

cbuffer cbMeshData : register(b11) {
  float3 gPosMin;
  float gMult;
  float gOffset;
}

Texture2D<float> txSurfaceDepth : register(t0);

float4 main(PS_IN_wipers pin) : SV_TARGET {
  float d = abs(txSurfaceDepth.SampleLevel(samLinearSimple, pin.PosH.xy * gResolutionStep, 0) - pin.PosH.z);
  return float4(pin.NormalL.xy * 0.5 + 0.5, gOffset + length(pin.PosL.xyz - gPosMin) * gMult, 1);
}