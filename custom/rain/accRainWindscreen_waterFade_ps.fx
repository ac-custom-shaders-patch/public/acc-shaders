#include "accRainWindscreen.hlsl"

Texture2D txPrevious : register(t0);
Texture2D txWipers : register(t2);

float4 main(VS_Copy pin) : SV_TARGET {
  float4 noise = txNoise.SampleLevel(samLinearSimple, pin.Tex * 2.2, 0);
  float4 noiseAdjusted = saturate(noise * 30 - 10);

  float4 wipers = txWipers.SampleLevel(samLinearSimple, pin.Tex, 0);
  float4 previous = txPrevious.Load(int3(pin.PosH.xy, 0));
  previous.z = max(previous.z - lerp(gFadeNormal, gFadeFast, noiseAdjusted.x), 0) * (1 - wipers.a);
  previous.w = 0;
  previous.xy = 0;
  // previous.z *= noiseAdjusted.y;


  previous.z = min(previous.z, 1 - gFading);
  return previous;
}