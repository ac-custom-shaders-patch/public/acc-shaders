#define USE_RAIN_DIR_FIXED
#include "accRainDrops.hlsl"

// float3 groundPos(float3 rainPos){
//   float4 posH = mul(float4(rainPos, 1), gRainShadowTransform);
//   float2 uv = posH.xy * float2(0.5, -0.5) + float2(0.5, 0.5);
//   float shadowValue = txRainShadow.SampleLevel(samLinearBorder0, float3(uv.xy, 1), 0);
//   return mul(float4(posH.xy, shadowValue, 1), gRainShadowTransformInv).xyz;
// }

void spawnBubble(float water, float3 pos, float4 rand4){
  if (water < 0.95 || rand4.x > 0.25) return;
 
  RainDrop drop;
  drop.pos = pos;
  drop.fade = 0;
  drop.dir = float3(0, 0, 0);
  drop.size = 0.015 * lerp(0.5, 1, rand4.x);
  buResultSplashes.Append(drop);
}

void spawnSplashPiece(RainDrop drop, float time, float2 dirMult, float heightK, float sizeMult){
  float3 dirK = normalize(float3(dirMult.x, heightK, dirMult.y));
  drop.dir.y = (-pow(time * lerp(1.5, 0.7, saturate(heightK * 2)) * 2.8 - 1, 2) + 1) * drop.dir.y;
  drop.pos += float3(drop.dir.x * time, drop.dir.y * saturate(heightK * 2), drop.dir.z * time) * dirK;
  drop.dir.xz *= dirK.xz;
  drop.dir.y = 0.1;
  drop.size *= saturate(1 - time * lerp(1, 0.7, sizeMult)) * sizeMult;
  buResultSplashes.Append(drop);
}

#include "include/poisson.hlsl"

void spawnSplash(float water, float3 pos, float3 posBase, float4 rand4){
  if (water < 0.9) return;

  float randK = rand4.x;
  float rand0 = rand4.y;
  float2 randV = float2(rand4.z, rand4.w) - 0.5;
  pos.y -= 0.01;

  float passTime = abs(posBase.y - pos.y) / RAIN_SPEED_BASE * 4;

  RainDrop drop;
  drop.pos = pos;
  drop.dir = 0;

  float yMult = 0.05 * (1 + randK) * saturate(remap(water, 0.5, 0.9, 0, 1));
  float2 horVel = randV * 0.1 * (0.5 + randK);
  float baseSize = 0.008 * lerp(0.5, 1, randK) * saturate(remap(water, 0.5, 0.9, 0, 1));
  drop.size = baseSize;
  drop.dir = float3(horVel.x, yMult, horVel.y);
  drop.fade = rand0 + passTime * 0.05;

  if (water > 0.95){
    spawnSplashPiece(drop, passTime, 0, 0.1, 1);
    spawnSplashPiece(drop, passTime, 0, 0.2, 1);
    // spawnSplashPiece(drop, passTime, 0, 0.125, 0.75);    
  }

  #define RAIN_DISK_SIZE 6
  float heightK = 1;
  float sizeK = 0.5;
  for (uint i = 0; i < RAIN_DISK_SIZE; ++i) {
    rand0 = frac(rand0 * 1951.561);
    drop.pos = pos;
    drop.size = baseSize;
    drop.fade += rand0;

    spawnSplashPiece(drop, passTime, SAMPLE_POISSON(RAIN_DISK_SIZE, i), heightK, 
      lerp(0.5, 1, SAMPLE_POISSON_LENGTH(RAIN_DISK_SIZE, i) * rand0));

    drop.pos += 0.01;
    drop.size *= 0.25;
    spawnSplashPiece(drop, passTime, SAMPLE_POISSON(RAIN_DISK_SIZE, i), heightK, 
      lerp(0.5, 1, SAMPLE_POISSON_LENGTH(RAIN_DISK_SIZE, i) * rand0));

    drop.pos.xz -= 0.01;
    spawnSplashPiece(drop, passTime, SAMPLE_POISSON(RAIN_DISK_SIZE, i), heightK, 
      lerp(0.5, 1, SAMPLE_POISSON_LENGTH(RAIN_DISK_SIZE, i) * rand0));

    heightK *= 0.8;
    sizeK = lerp(sizeK, 1, 0.2);    
  }
}

[numthreads(32, 32, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  float2 basePosXZ = gSpawnPointA + float2(float(threadID.x), float(threadID.y)) * gMapStep;
  RAND_INIT(gSpawnPointAWorld + float2(float(threadID.x), float(threadID.y)) * gMapStep);
  if (gDensity * gSplashesDensity < RAND) return;
  // if (RAND > 0.05) return;

  float verticalRandom = RAND + RAND * 0.1;
  float mixParam = RAND;
  float3 randomDir = rainDropRandomDir(float2(RAND, RAND));
  float3 dir = rainDropSummaryDir(mixParam, randomDir);
  float3 pos = rainDropPos(mixParam, basePosXZ, randomDir, verticalRandom);
  float alterDropPosRand = RAND;
  float3 posBase = pos;
  float3 ground = groundPosNoCar(pos);
  float4 rand4 = float4(RAND, RAND, RAND, RAND);

  bool groundOccludedByCar;
  if (pos.y > ground.y) return;

  pos += dir * (ground.y - pos.y) / dir.y;
  pos += dir * (groundPosNoCar(pos, groundOccludedByCar).y - pos.y) / dir.y;
  pos = lerp(pos, ground, 0.05);
  if (!isVisible(pos) || groundOccludedByCar) return;

  bool isWaterSurface;
  float water = waterThickness(pos, isWaterSurface);
  water = saturate(water * 1.6);
  if (!isWaterSurface) spawnBubble(water, pos, rand4);
  spawnSplash(water, pos, posBase, rand4);
}