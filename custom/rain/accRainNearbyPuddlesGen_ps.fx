

#define ALLOW_RAINFX
#define RAINFX_USE_PUDDLES_MASK
#define MODE_PUDDLES
#define CBCAMERA_DEFINED

cbuffer cbData : register(b10) {
  float4x4 gViewProj;
  float3 ksCameraPosition;
  float lPad1;
}

// #define ksCameraPosition float3(24.00, -8.00, -88.00)

#include "include_new/base/_include_ps.fx"

struct PS_IN_puddlesGen {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float3 PosW : TEXCOOR1;
  float3 NormalW : TEXCOOR2;
  float Ao : TEXCOORD3;
};

    // float4 posH;
    // float3 posC;
    // float3 posW;
    // float3 posG;
    // float3 normalW;
    // float3 posL;
    // float3 normalL;
    // float3 toCamera;
    // #ifdef RAINFX_USE_PUDDLES_MASK
    //   float wetSkidmarks;
    // #endif

float4 main(PS_IN_puddlesGen pin) : SV_TARGET {
  float4 txDiffuseValue = 0;

  RainSurfaceInput RP_input;
  RP_input.posH = 0;
  RP_input.posC = pin.PosW - ksCameraPosition;
  RP_input.normalW = normalize(pin.NormalW);
  RP_input.toCamera = float3(0, 1, 0);
  RP_input.posL = pin.PosW;
  RP_input.normalL = normalize(pin.NormalW);
  RP_input.wetSkidmarks = 0;
    
  RainParams RP = _rainFx_rainMap(RP_input, txDiffuseValue, pin.Ao);

  // R: puddleValue, G: reliefValue, B: racingLineOffset (0…1)
  return float4(ENCODE_NZN_UINT8(RP.water), ENCODE_NZN_UINT8(RP.wetness), ENCODE_NZN_UINT8(RP.damp * 0.5 + 0.5), 0);
}

