#define SPS_NO_POSC
#include "../../custom_objects/common/bending.hlsl"
#include "../../custom_objects/common/animatedWipers.hlsl"
#include "accRainWindscreen.hlsl"

BENDING_FX_IMPL

cbuffer cbCamera : register(b0) {
  float4x4 ksView_base;
  float4x4 ksProjection_base;
}

cbuffer cbDataObject : register(b11) {
  float4x4 gWorld0;
}

struct PS_IN {
  float4 PosH : SV_POSITION;
};

#include "../../recreated/include_new/base/sps_utils.fx"

PS_IN main(VS_IN_ac vin SPS_VS_ARG) {
  #ifdef WITH_ANIMATED_WIPERS
    for (int i = 0; i < ENTRIES_COUNT; ++i){
      if (gWiperParams[i].calculate(vin.PosL.xyz, vin.NormalL.xyz, vsLoadMat0b(vin.TangentPacked), i, false)) break;
    }
  #endif

  PS_IN vout;
  float4 posW = mul(vin.PosL, gWorld0);
  #ifdef WITH_BENDING
    alterPosBending(vin.PosL.xyz, false, posW.xyz);
  #endif

  vout.PosH = mul(mul(posW, ksView), ksProjection);
  SPS_RET(vout);
}
