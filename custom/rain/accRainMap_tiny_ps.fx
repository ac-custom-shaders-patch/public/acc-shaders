#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);
#define PADDING_MULT 1

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float Random : TEXCOORD1;
};

float2 remapNormal(float2 input){
  input *= 0.8;
  // return pow(length(input), 2) * normalize(input);
  // return normalize(input);
  return input;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float dist = length(pin.Tex);
  clip(1 - dist);

  if (dist >= 0.95){
    return float4(0, 0, 1, 0);
  }

  if (dist >= 0.8){
    return 0;
  }

  #if PADDING_MULT == 1
    return float4(float2(1, -1) * remapNormal(pin.Tex), 0, pin.Random);
  #else
    return float4(float2(1, -1) * remapNormal(dist > PADDING_MULT ? 0 : (pin.Tex) / PADDING_MULT), 0, dist > PADDING_MULT ? 0 : pin.Random);
  #endif
}