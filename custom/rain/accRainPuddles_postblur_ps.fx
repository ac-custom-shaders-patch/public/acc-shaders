#include "include/common.hlsl"
#include "include/poisson.hlsl"

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float main(VS_Copy pin) : SV_TARGET {
  POISSON_AVG(float, value, txDiffuse, samLinearClamp, pin.Tex, 0.0008, 16);
  return value * 2 - 1;
}