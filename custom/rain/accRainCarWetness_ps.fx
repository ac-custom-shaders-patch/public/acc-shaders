#include "include/common.hlsl"
#include "include/poisson.hlsl"

cbuffer cbData : register(b10) {
  float gFade0;
  float gFade2;
  float gRainIntensity;
  float gFallbackAO;
  float gFading;
  float3 gPad0;
  float4x4 gTransform;
}

Texture2DArray<float> txRainShadow : register(t0);
Texture2D txSplashes : register(t1);
Texture2D<float> txPrev : register(t2);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float3 ShadowTex : TEXCOORD1;
};

float sampleShadow(float2 uv, float comparison){
  #ifdef SIMPLIFIED_MODE
    return txRainShadow.SampleCmpLevelZero(samShadow, float3(uv, 0), comparison);
  #else
    float sum = 0;
    float sampleScale = 0.003;

    #define RAIN_DISK_SIZE 16
    if (comparison >= 1) return 1; 

    for (uint i = 0; i < RAIN_DISK_SIZE; ++i) {
      float2 sampleUV = uv + SAMPLE_POISSON(RAIN_DISK_SIZE, i) * sampleScale;
      sum += txRainShadow.SampleCmpLevelZero(samShadow, float3(saturate(sampleUV), 0), comparison);
    }
    return sum / RAIN_DISK_SIZE;
  #endif
}

float samplePrevBlurred(float2 uv){
  POISSON_AVG(float, result, txPrev, samLinearClamp, uv, 0.02, 8);
  return result;
}

float sampleSplashes(float2 uv){
  POISSON_AVG(float4, result, txSplashes, samLinearClamp, uv, 0.02, 8);
  return result.a;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float prev = txPrev.Load(int3(pin.PosH.xy, 0));
  // prev += saturate((samplePrevBlurred(pin.Tex) - prev) * 200) * gFade1;

  float shadow = (all(abs(pin.ShadowTex.xyz - 0.5) < 0.5) 
    ? sampleShadow(pin.ShadowTex.xy, pin.ShadowTex.z) : gFallbackAO) * saturate(gRainIntensity * 10) * 0.8;

  float ret = prev - lerp(gFade0, gFade2 * 0.2, saturate(prev * 20 - 10));
  ret += saturate((shadow - ret) * 200) * gFade2;

  #ifndef SIMPLIFIED_MODE
    // ret = max(ret, sampleSplashes(pin.Tex) * 2);
    ret += sampleSplashes(pin.Tex);
  #endif

  ret = min(ret, 1 - gFading);
  // ret = gRainIntensity;
  // ret = all(abs(pin.ShadowTex.xyz - 0.5) < 0.5);
  // ret = 0;
  // ret = 1;
  return ret;
}