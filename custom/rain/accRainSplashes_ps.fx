#define SPLASHES
#define PARAM_BLUR 0
#define PARAM_USE_COLOR gUseColor
#include "accRainDrops.hlsl"

#define ALLOW_EXTRA_FEATURES
#include "refraction.hlsl"

PS_OUT main(PS_IN pin) {
  INTERIOR_TEST;
  READ_VECTOR_TOCAMERA
  float softK = calculateSoft(pin.PosH, pin.PosC, PARAM_BLUR);

  if (pin.NoiseOffset){
    float4 noise = txNoise.SampleLevel(samLinearSimple, pin.Tex * 0.03 + pin.NoiseOffset, 0);
    // pin.Tex *= 1 + noise.xy * 0.25;
    pin.Tex *= 1 + max(noise.xy + noise.zw - 1, 0) * 0.5;
  } else {
    if (pin.Tex.y < 0){
      clip(abs(toCamera.y) * sqrt(1 - pow(pin.Tex.x, 2)) - -pin.Tex.y);
    }
  }

  float sharpness = 0.95;
  float facingShare = sqrt(saturate(1 - dot2(pin.Tex)));
  // if (pin.Tex.y < 0) pin.Tex.y *= lerp(1, 1.4, abs(pin.Tex.y));
  float3 normal = normalize(pin.DirUp * pin.Tex.y * lerp(1, 4, gStretching)
    + pin.DirSide * pin.Tex.x
    + -toCamera * facingShare);

  float fresnelBase = saturate(0.05 + pow((1 + dot(toCamera, normal)) * lerp(1, 2, gStretching), 3));
  float fresnel = max(fresnelBase, saturate(abs(pin.Tex.y) * 1.3) * gStretching);

  float alpha = saturate(remap(length(pin.Tex), sharpness, 1, 1, 0));// * softK;

  // float4 noiseSplash = txNoise.SampleLevel(samLinearSimple, pin.Tex * 0.1 + pin.NoiseOffset, 0);
  // alpha *= noiseSplash.x;

  float3 refraction = lerp(toCamera, -normal, 0.5);
  float3 reflection = reflect(toCamera, normal);
  float3 reflectionColor = sampleEnv(reflection, 3, 0);
  float3 refractionColor = sampleEnv(refraction, 3, 0);

  if (1){
    refractionColor = calculateRefraction(pin.PosH, pin.PosC, toCamera, normal).rgb;
  }

  clip(alpha - 0.05);

  float3 lighting;
  if (pin.NoiseOffset && gUseColor){
    lighting = lerp(refractionColor, reflectionColor, fresnel);
    lighting += pin.LightVal * max(fresnel * pow(saturate(dot(pin.LightDir, toCamera)), 20) * 0.4,
      lerp(0.02, simpleReflectanceModel(-toCamera, pin.LightDir, normal), pin.LightFocus));
    alpha = 1;
  } else {
    alpha = 0.1;
    lighting = fresnel * reflectionColor / alpha;
    lighting += pin.LightVal * max(fresnel * pow(saturate(dot(pin.LightDir, toCamera)), 20),
      lerp(0.05, 0.1 * pow(saturate(dot(normal, pin.LightDir)), 40), pin.LightFocus)) / alpha;
  }

  // lighting = float3(3, 0, 0);
  // alpha = 1;
  
  lighting += simpleReflectanceModel(-toCamera, -ksLightDirection.xyz, normal) * ksLightColor.rgb * pin.Shadow;
  alpha *= interiorMult;

  RETURN_BASE(lighting, alpha);
}