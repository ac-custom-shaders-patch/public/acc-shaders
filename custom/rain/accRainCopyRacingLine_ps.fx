#include "include/common.hlsl"

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

// float4 main(VS_Copy pin) : SV_TARGET {
//   // if (any(pin.Tex > 0.95 || pin.Tex < 0.05)) return 0.1;

//   float paddingAdj = 0.54;
//   pin.Tex = (pin.Tex * 2 - 1) * paddingAdj + 0.5;
//   return txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0) + (any(pin.Tex > 0.99 || pin.Tex < 0.01) ? 0.1 : 0);
// }

#include "include/poisson.hlsl"
float4 main(VS_Copy pin) : SV_TARGET {
  float paddingAdj = 0.54;
  pin.Tex = (pin.Tex * 2 - 1) * paddingAdj + 0.5;

  float currentShot = 0;
  float radius = 0.0016;

  #define DISK_SIZE 16
  for (uint i = 0; i < DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(DISK_SIZE, i);
    currentShot += txDiffuse.SampleLevel(samLinearClamp, pin.Tex + offset * radius, 0);
  }

  return currentShot / DISK_SIZE;
}