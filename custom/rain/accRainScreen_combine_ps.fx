#include "include/common.hlsl"

Texture2D txBlurred : register(t0);
Texture2D<float> txMask : register(t1);

cbuffer cbData : register(b10) {
	float gAspectRatio;
	float gFade;
  float2 gScreenSizeInv;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#include "include/poisson.hlsl"
float2 estimateDir(float2 uv){
  float2 ret = 0;

  #define PUDDLES_DISK_SIZE 16
  [unroll]
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.005 * float2(1, gAspectRatio);
    float v = txMask.SampleLevel(samLinearSimple, uv + offset, 0.5);
    ret += float2(ddx(v), ddy(v));
  }
  return saturate(ret / PUDDLES_DISK_SIZE);
}

float4 main(VS_Copy pin) : SV_TARGET {
  float mask = txMask.SampleLevel(samLinearSimple, pin.Tex, 0) * 3;

  [branch]
  if (mask < 0.005){
    return 0;
  }

  float4 color = 0;
  float2 dir = estimateDir(pin.Tex);
  #ifdef DELAYED_RESOLVE
    color.rg = -dir * mask * 3.5;
    color.b = 2 * mask;
  #else
    color = txBlurred.SampleLevel(samLinearClamp, pin.Tex - dir * mask * 3.5, 2 * mask);
    if (!dot(color, 1)) discard;
  #endif
  color.a = saturate(mask * 2);
  return color;

  // float v = txDiffuse.Load(int3(pin.PosH.xy, 0));
  // v = min(v, 0.8) - gFade * 2;
  // return v;
}