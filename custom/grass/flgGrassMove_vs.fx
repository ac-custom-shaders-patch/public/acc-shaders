#include "include_new/base/cbuffers_vs.fx"
#include "flgGrassPiece.hlsl"

VS_GrassMove main(uint id: SV_VertexID) {
  VS_GrassMove vout;
  float2 ver = BILLBOARD[id];
  float4 posW = float4(ver.xy, 0, 1);
  vout.PosH = mul(posW, gViewProj);
  // vout.PosH = posW;
  vout.Tex = ver * 0.5 + 0.5;
  vout.Tex.y = 1 - vout.Tex.y;
  vout.ReduceBy = gFrameTime * 0.2;

  float2 v = float2(id % 2, id / 2) * 2;
  vout.Tex = float2(v.x, 1 - v.y);
  vout.TexPrev = mul(float4(v, 0, 1), gViewProj).xy * float2(1, -1) + float2(0, 1);
  vout.PosH = float4(v * 2 - 1, 0, 1);

  return vout;
}
