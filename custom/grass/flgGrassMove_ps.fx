#include "flgGrassPiece.hlsl"

Texture2D<float2> txPrevious : register(t0);
Texture2D<float2> txNow : register(t1);

float2 main(VS_GrassMove pin) : SV_TARGET {
  float2 v = txPrevious.Sample(samPointBorder0, pin.TexPrev);
  float2 n = txNow.Sample(samPointBorder0, pin.Tex);
  v.y = n.y; // occlusion

  float m = max(n.x, v.x);
  if (m > 0.96){
    v.x = m;
  } else if (n.x > v.x) {
    v.x = min(n.x, v.x + pin.ReduceBy * 10);
  } else if (n.x < v.x * 0.8) {
    v.x = saturate(v.x - pin.ReduceBy);
  }
  return v;
}