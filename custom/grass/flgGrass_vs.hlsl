StructuredBuffer<FoliageFinVertex> particleBuffer : register(t0);
Texture2D<float2> txDeformation : register(t1);

// #define MAX_WIND_SPEED_2 4

// float2 windOffset2(float3 pos, float windPower, float freqMult){
//   #ifdef NO_WIND
//     return 0;
//   #else
//     float waveOffset = saturate(0.1 + extWindSpeed / MAX_WIND_SPEED_2);
// 		float windIntensity = saturate(extWindSpeed / MAX_WIND_SPEED_2);
// 		float randOffset = lerp(frac(dot(pos.xz, 193.193)) * 3.14, 0, windIntensity);
//     float wave = sin(extWindWave * 1.773 * freqMult + pos.z * freqMult * WIND_COORDS_MULT / 17 + randOffset) 
//       * sin(extWindWave * freqMult + pos.x * freqMult * WIND_COORDS_MULT / 13 + randOffset);
//     return extWindVel * windPower * (1 + wave + waveOffset) / (20 + extWindSpeed);
//   #endif
// }

// float2 windOffsetPrev2(float3 pos, float windPower, float freqMult){
//   #ifdef NO_WIND
//     return 0;
//   #else
//     float waveOffset = saturate(0.1 + extWindSpeedPrev / MAX_WIND_SPEED_2);
// 		float windIntensity = saturate(extWindSpeed / MAX_WIND_SPEED_2);
// 		float randOffset = lerp(frac(dot(pos.xz, 193.193)) * 3.14, 0, windIntensity);
//     float wave = sin(extWindWavePrev * 1.773 * freqMult + pos.z * freqMult * WIND_COORDS_MULT / 17 + randOffset) 
//       * sin(extWindWavePrev * freqMult + pos.x * freqMult * WIND_COORDS_MULT / 13 + randOffset);
//     return extWindVelPrev * windPower * (1 + wave + waveOffset) / (20 + extWindSpeed);
//   #endif
// }

Texture2D<float> txWindOffsets : register(t21);
Texture2D<float> txWindOffsetsPrev : register(t19);

// void applyWind_(inout float3 pos, float3 up, float heightHalf, bool prev, float phaseOffset){
//   float3 posIn = pos - extSceneOffset;
// 	float totalHeight = heightHalf * 2;
// 	float2 windXZ = (prev ? windOffsetPrev2(posIn.xyz + phaseOffset, 1, 2.2) : windOffset2(posIn.xyz + phaseOffset, 1, 2.2)) * 0.5;
// 	float y = saturate(length(windXZ));
// 	float verticalOffset = (1 - sqrt(1 - y * y)) * totalHeight;
// 	float3 wind = float3(windXZ.x, 0, windXZ.y) * totalHeight;
// 	pos += wind - up * (dot(wind, up) - verticalOffset);
// }

void applyWind(float phaseOffset, inout float3 pos, float3 up, float affectedByWind, float fade, float height, bool prev, inout float specular){
  Texture2D<float> tx = prev ? txWindOffsetsPrev : txWindOffsets;
  float windMult = affectedByWind 
    * (tx.SampleLevel(samLinearSimple, (pos.xyz - extSceneOffset).xz / 80 + phaseOffset * 0.05, 0) 
      + tx.SampleLevel(samLinearSimple, (pos.xyz - extSceneOffset).xz / 21.71 + phaseOffset * 0.05, 0) * 0.471)
    * -up.y;
  float2 windOffset = windMult * (prev ? extWindVelPrev : extWindVel);
  windOffset = windOffset / (1 + abs(windOffset));
  pos.xz += windOffset * height;
  pos.y -= min(0.5, 1 - sqrt(1 - dot2(windOffset))) * (0.5 + frac(558.476 * phaseOffset)) * height;
  specular += affectedByWind * lerp(0.1, 0.3, saturate(-ksLightDirection.y)) 
    * fade * saturate((prev ? extWindSpeedPrev : extWindSpeed) / 10) * windMult;
}

float getBentK(float Py, float bentPow, float bentStrength){
	return pow(abs(Py), bentPow) * bentStrength;
}