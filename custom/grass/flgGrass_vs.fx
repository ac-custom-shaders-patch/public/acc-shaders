#ifndef MODE_MAIN_NOFX
	#define MODE_MAIN_FX
#endif

#define SHADOWS_FILTER_SIZE 1
#define NO_SHADOWS_CASCADES_TRANSITION

// #ifdef GAMMA_FIX
// 	#define SHADOW_POISSON_DISK_SIZE 12
// 	#define SHADOW_POISSON_DISK_SCALE 1
// 	#define FORCE_DISC_SHADOWS
// 	#define USE_SCALE_PARAM
// #endif

#include "include_new/base/_include_vs.fx"
#include "include_new/ext_shadows/_include_vs.fx"

// #define LIGHTING_SAFE
#ifndef MODE_MAIN_NOFX
	#define AO_EXTRA_LIGHTING 1
	#define GI_LIGHTING 0
	// #define LIGHTINGFX_NOSPECULAR
	#define LIGHTINGFX_SPECULAR_EXP 24
	#define LIGHTINGFX_SPECULAR_COLOR bitLightingFade * 0.5
	#define LIGHTINGFX_KSDIFFUSE 1
	#define GAMMA_LIGHTINGFX_KSDIFFUSE_ONE
	// #define LIGHTINGFX_SIMPLEST
	#define LIGHTINGFX_SIMPLEST_NORMAL
	#define LIGHTINGFX_SHADOW_GRASS_OFFSET
	// #define NO_EXTSPECULAR
	#ifndef SHADER_MIRROR
		#define LIGHTINGFX_FIND_MAIN
		// #define LIGHTINGFX_TWO_NORMALS
	#endif
	#define POS_CAMERA posCPerVertexLighting
	#define POS_TOCAMERA normalize(POS_CAMERA)
	#include "include_new/ext_lightingfx/_include_ps.fx"
#endif

#include "include_new/base/common_ps.fx"
#include "include_new/ext_functions/wind.fx"
#include "include/normal_encode.hlsl"
#include "include/ssgi_vs.hlsl"
#include "flgGrass.hlsl"
#include "flgGrass_vs.hlsl"

float getDeformationAO(float2 posXZ){
  float2 deformationUV = (posXZ - gDeformationPointB) / (gDeformationPointA - gDeformationPointB);
  float2 deformationEdges = saturate((0.5 - abs(deformationUV - 0.5)) * 10);
  float deformationEdge = deformationEdges.x * deformationEdges.y;
  int R = 2;
  float2 a = txDeformation.SampleLevel(samLinearBorder0, deformationUV, 0, int2(-R, -R));
  float2 b = txDeformation.SampleLevel(samLinearBorder0, deformationUV, 0, int2(-R, R));
  float2 c = txDeformation.SampleLevel(samLinearBorder0, deformationUV, 0, int2(R, -R));
  float2 d = txDeformation.SampleLevel(samLinearBorder0, deformationUV, 0, int2(R, R));
  float2 av = saturate((a + b + c + d) / 4 * 1.6 - (gUseDeformationOcclusion ? 0.6 : 0.8));
	// return gDeformationPointB.x == 0 ? 100 : 0;
  return av.y * deformationEdge;
}

static const float2 BILLBOARD[] = {
	float2(-1, 0),
	float2(1, 0),
	float2(-1, 1),
	float2(-1, 1),
	float2(1, 0),
	float2(1, 1),
};

// float cascadeShadowStep_single(float deltaZ, float4 uv, float tx) {
//   return uv.z >= 1 ? 1 : txShadowArray.SampleCmpLevelZero(samShadow, float3(uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5), tx), uv.z);
// }

#if !defined(MODE_KUNOS) && !defined(NO_SHADOWS) && !( defined(SUPPORTS_BLUR_SHADOWS) && defined(BLUR_SHADOWS) )
#include "include/poisson.hlsl"
float getShadowBiasMult(float3 posC, float4 shadowTex0, float3 offset0, float3 offset1) {
  #ifdef NO_CLOUD_SHADOW
    float cloudShadowsMult = 1;
  #else
    float cloudShadowsMult = getCloudShadow(posC);
  #endif

  float3 shadowTex1 = shadowTex0.xyz * shadowsMatrixModifier1M + shadowsMatrixModifier1A;
  float3 shadowTex2 = shadowTex0.xyz * shadowsMatrixModifier2M + shadowsMatrixModifier2A;
  float3 shadowTex3 = shadowTex0.xyz * shadowsMatrixModifier3M + shadowsMatrixModifier3A;

	int cascade;
	float3 uv;
	float3 uvMult, uvAdd;
	if (checkCascade(shadowTex0)){
		cascade = 0;
		uv = shadowTex0.xyz;
		uvMult = 1;
		uvAdd = 0;
	} else if (checkCascade(float4(shadowTex1, 1))){
		cascade = 1;
		uv = shadowTex1;
		uvMult = shadowsMatrixModifier1M;
		uvAdd = shadowsMatrixModifier1A;
	} else if (checkCascade(float4(shadowTex2, 1))){
		cascade = 2;
		uv = shadowTex2;
		uvMult = shadowsMatrixModifier2M;
		uvAdd = shadowsMatrixModifier2A;
	} else if (checkCascade(float4(shadowTex3, 1))){
		cascade = 3;
		uv = shadowTex3;
		uvMult = shadowsMatrixModifier3M;
		uvAdd = shadowsMatrixModifier3A;
	} else {
		uv = 1;
	}

	float mult = 1;
	if (uv.z < 1) {
		offset0 *= uvMult * float3(0.5, -0.5, 1);
		offset1 *= uvMult * float3(0.5, -0.5, 1);
		uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

		#define GRASS_SHADOW_TAPS 16
    float ret = 1e-10;
		float count = 1e-10;
    for (uint i = 0; i < GRASS_SHADOW_TAPS; ++i) {
      float2 sampleDir = SAMPLE_POISSON(GRASS_SHADOW_TAPS, i);
			float3 tapUV = uv + sampleDir.x * offset0 + sampleDir.y * offset1;
			float contibution = all(abs(tapUV * 2 - 1) < 1);
			ret += txShadowArray.SampleCmpLevelZero(samShadow, float3((tapUV.xy), cascade), tapUV.z) * contibution;
			count += contibution;
    }
		mult = ret / count;
	}

	return cloudShadowsMult * mult;
}
#endif

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	PS_IN vout;

	// #ifdef SIMPLE_SHAPE
	// 	uint vertexID = fakeIndex % 3;
	// 	uint instanceID = fakeIndex / 3;
	// #else
	// 	uint vertexID = fakeIndex % 15;
	// 	uint instanceID = fakeIndex / 15;
	// #endif

	uint vertexID = fakeIndex % 6;
	uint instanceID = fakeIndex / 6;

	FoliageFinVertex piece = particleBuffer[instanceID];

	// UNPACK
	float2 type_repeatk = FLG_unpack(piece.type_flip);
	float4 color_fade = FLG_unpack(piece.color_fade);
	float4 rotation_width_height = FLG_unpack(piece.rotation_width_height);
	float4 normal_deform_ao = FLG_unpack(piece.normal_deform_ao);
	float4 up_singleness = FLG_unpack(piece.up_singleness);
	float4 cut_passid_dif_spec = FLG_unpack(piece.shadowmult_passid_dif_spec);
	float3 in_normal = normalDecode(normal_deform_ao.xy);

	// PIECE_VERTICAL STUFF
	float3 up = up_singleness.xyz;
	// up = float3(0, -1, 0);
	float2 sideXZ = rotation_width_height.xy;
	float3 side = normalize(cross(float3(sideXZ.x, 0, sideXZ.y), in_normal));
	float3 bent = normalize(cross(side, in_normal));

	float2 P = BILLBOARD[vertexID];
	// float squashedMult = saturate(1 - type_repeatk.x * type_repeatk.x);
	// squashedMult = 0;

	// float bentHeight = rotation_width_height.w * sqrt(squashedMult);
	// float cutK = saturate(bentHeight * cut_passid_dif_spec.x - 2);
	// if (P.y == 1 && cutK > 0) {
	// 	P.y = lerp(P.y, 0.67, cutK);
	// 	P.x = lerp(P.x, 0.8, cutK);
	// }

	float affectedByWind = P.y * normal_deform_ao.w * 0.5;
	float windPhaseOffset = type_repeatk.x / 2;

	// float affectedByWind = P.y * (0.5 + abs(type_repeatk.x)) * squashedMult;
	// float bentK = getBentK(P.y, type_flip.y, type_repeatk.x);
	rotation_width_height.z *= type_repeatk.y;

	float3 origPosition = piece.position;
	piece.position -= rotation_width_height.w * up * P.y;
	piece.position += side * rotation_width_height.z * P.x;
	// piece.position += bent * bentK * rotation_width_height.w;

	float3 posC = origPosition - ksCameraPosition.xyz;
	float distance = length(posC);
	float3 toCamera = posC / distance;
	// piece.position.y += P.x * saturate(abs(dot(side, toCamera)) * 4 - 3) * rotation_width_height.z / 2 * sign(toCamera.y);

	#ifdef CREATE_SHADOW_BUFFER
		[branch]
		if (distance > 30){
			vout = (PS_IN)0;
			vout.PosH = -1;
			return vout;
		}
	#endif
	
	#ifdef CREATE_SHADOW_BUFFER
		piece.position.y -= rotation_width_height.z * saturate(distance / 15 - 1);
	#endif
	float4 posW = float4(piece.position, 1);

	float2 baseTex = float2(0.5 + 0.5 * P.x, 1 - P.y);
	int pieceID = int(round(cut_passid_dif_spec.y));
	if (pieceID < 0){
		vout.Tex = float2((baseTex.x * type_repeatk.y + type_repeatk.x) * gTexGridInv.x, baseTex.y);
		vout.Tex.y = lerp(vout.Tex.y, 1, cut_passid_dif_spec.x * frac(type_repeatk.x  * gTexGridInv.x + vout.Tex.x * 0.3)) * gTexGridInv.y;
	} else {
		CustomTexVSPiece texPiece = gTexPieces[pieceID];
		float4 start_size = FLG_unpack(texPiece.start_size);
		vout.Tex = start_size.xy + start_size.zw * baseTex;
		// float stableK = 1 / max(1, texPiece.sizeMult.x * 0.25 + pow(texPiece.sizeMult.y, 2));
		cut_passid_dif_spec.w *= texPiece.thickness;
		affectedByWind *= texPiece.thickness;
	}

	vout.Deformation = normal_deform_ao.z;
	
	#ifndef CLOSE_MODE
		float4 origPosW = posW;
		{
			// applyWind(posW.xyz, up, saturate(rotation_width_height.w * -up.y - 0.05) * affectedByWind, false, windPhaseOffset);
			applyWind(windPhaseOffset, posW.xyz, up, affectedByWind, saturate(color_fade.w), rotation_width_height.w, false, cut_passid_dif_spec.w);
		}
	#endif

	float4 posV = mul(posW, ksView);
	vout.PosH = mul(posV, ksProjection);


	// if (1){
	// 	vout = (PS_IN)0;
	// 	vout.PosH = -1;
	// 	return vout;
	// }

	#ifndef CREATE_SHADOW_BUFFER
		// vout.PosC = posC;
		vout.PosC = piece.position - ksCameraPosition.xyz;
		vout.Fog = calculateFog(posV);

		#ifndef MODE_GBUFFER
			vout.Color = color_fade.xyz;

			float4 posWPerVertexLighting = float4((posW.xyz + origPosition) / 2 + float3(0, 0.02 + rotation_width_height.w, 0), 1);
			float4 posWShadow = posWPerVertexLighting;
			#ifndef CLOSE_MODE
				posWShadow.xyz += ksLightDirection.xyz * -0.05;
			#endif
			// posWShadow.xyz -= ksLightDirection.xyz * lerp(2.5, 0.25, saturate(0.1 - ksLightDirection.y));
			// posWShadow.xyz -= ksLightDirection.xyz * 0.1;
			float hw = rotation_width_height.z / 2;
			float vout_Shadow = getShadowBiasMult(posC, mul(posWShadow, ksShadowMatrix0), 
				mul(float3(hw, 0, 0), (float3x3)ksShadowMatrix0), mul(float3(0, 0, hw), (float3x3)ksShadowMatrix0));
			// vout_Shadow = getShadowBiasMult(posC, 0, float3(0, 1, 0), mul(posWShadow, ksShadowMatrix0), 0, 1);
			// vout_Shadow = P.y;
			// vout_Shadow = min(vout_Shadow, cut_passid_dif_spec.x);

			// float selfShadowK = saturate(type_repeatk.y);
			// float shadowAdj = affectedByWind * 0.5 + 0.5;
			// shadowAdj = lerp(shadowAdj, 1, saturate(distance / 50 + pow(abs(toCamera.y), 3)));
			// float aoValue = normal_ao.w * lerp(shadowAdj, 1, selfShadowK);
			// vout_Shadow *= lerp(shadowAdj, 1, selfShadowK);

			float aoValue = normal_deform_ao.w;
      float wetK = saturate(extSceneWetness * 100) * aoValue * 0.5;

			[branch]
			if (gUseDeformationOcclusion) {
				float deformationAO = getDeformationAO(posW.xz);
				aoValue *= 1 - deformationAO;
				vout_Shadow = min(vout_Shadow, 1 - deformationAO * P.y);
			}

			// float deformationAO = getDeformationAO(posW.xz);
			// P.y = lerp(1, P.y, vout.Deformation);
			// aoValue *= 1 - deformationAO;
			// vout_Shadow = min(vout_Shadow, 1 - deformationAO * P.y);

			float3 bitNormal = bent;
			if (dot(bitNormal, toCamera) > 0) bitNormal = -bitNormal;

			float bitLightingFade = saturate(color_fade.w);// * (1 - 0.8 * saturate(cut_passid_dif_spec.y / 4));
			float advFade = lerp(pow(saturate(1 - distance / 30), 1.4), 0, up_singleness.w);
			float advFadeFurther = lerp(saturate(1.2 - distance / 60), 0, up_singleness.w);
			float bitShadow1 = bitLightingFade * vout_Shadow * saturate(0.5 + P.y * 3);
			float bitShadow2 = bitLightingFade * vout_Shadow * saturate(P.y * 3);

			float3 backlitColor = max(lerp(dot(vout.Color, 0.25), vout.Color, GAMMA_OR(2, 3)), 0);
			float directionMult = abs(dot(ksLightDirection.xyz, bent));
			vout.SpecIntensity = cut_passid_dif_spec.w * bitShadow1 * lerp(1, directionMult * lerp(1, 2, wetK), advFadeFurther);
			// vout.SpecIntensity = cut_passid_dif_spec.w;
			vout.Backlit = extSpecularColor.rgb 
				* advFadeFurther
				// * pow(saturate(dot(-toCamera, ksLightDirection.xyz)), 12) 
				* saturate(dot(-toCamera, ksLightDirection.xyz)) 
				* bitShadow2 
				* backlitColor 
				* 0.15
				* GAMMA_LINEAR_SIMPLE(pow(directionMult, 4) + pow(saturate(dot(-toCamera, ksLightDirection.xyz)), 32));

			// vout.SpecIntensity = 0;
			// vout.Backlit = 0;
			// vout_Shadow *= cut_passid_dif_spec.z;

			#ifdef GAMMA_FIX
				// vout_Shadow = 1;
			#endif

			// float bitLdotH = saturate(dot(ksLightDirection.xyz, -bitNormal));
			float planeLdotH = LAMBERT(ksLightDirection.xyz, -in_normal);
			vout.Lighting = vout_Shadow * ksLightColor.rgb * planeLdotH;
			vout.ShadowDetails = 1 - pow(planeLdotH, 2);
			// vout.ShadowDetails = 1;
			vout.Fade = color_fade.w * advFadeFurther;
			vout.TexDimming = pow(vout.Fade, 2) * lerp(0.5, 0, up_singleness.w) * saturate(rotation_width_height.w * 2);
			// vout.TexDimming = up_singleness.w;
			vout.LocalSpecNormal = bitNormal + normalize(piece.position - origPosition);// + in_normal * (P.y - 0.5) * 0.5 + side * (P.x - 0.5) * 1;
			vout.LocalSpecShadow = bitShadow2 * frac(abs(rotation_width_height.x) * 9217);
			vout.PosY = P.y;
			// aoValue *= lerp(1, saturate(P.y * 2), bitLightingFade * 0.5);

			vout.Ambient = getAmbientBaseAt(posC, in_normal, GAMMA_LINEAR(aoValue));
			vout.Color = lerp(dot(vout.Color, 0.25), vout.Color, 1 + P.y * advFade);
			vout.Occlusion = aoValue;
			// vout.Color = lerp(vout.Color, float3(0.04, 0.08, 0.02), P.y * advFade);

			#ifndef MODE_MAIN_NOFX
				float4 txDiffuseValue = 1;
				float3 normalW = in_normal;
				// float3 normalW0 = in_normal;
				// float3 normalW1 = bitNormal;
				float normalMix = bitLightingFade * 0.3;
				float3 posCPerVertexLighting = posWPerVertexLighting.xyz - ksCameraPosition.xyz;
				LFX_MainLight mainLight;
				float3 lightingFX = 0;
				LIGHTINGFX(lightingFX);
				#ifndef SHADER_MIRROR
					float lightingFocus = saturate(mainLight.power / max(dot(vout.Lighting, 1), 1));
					// vout.Backlit += 4 * lightingFocus * backlitColor * pow(directionMult, 4) * vout.Lighting 
					directionMult = abs(dot(mainLight.dir, bent));
					vout.Backlit += lightingFocus * backlitColor * lightingFX * 0.25
						* saturate(dot(toCamera, mainLight.dir)) * advFade * aoValue * bitShadow2
						* pow(directionMult, 4);
				#endif

				vout.Lighting += lightingFX;// * lerp(1, P.y, advFade);
				vout.ShadowDetails = lerp(vout.ShadowDetails, 1, saturate(dot(lightingFX, 1) / max(1e-10, dot(vout.Lighting, 1))));
				// vout.Lighting += 10;// * lerp(1, P.y, advFade);
				// vout.Lighting += getSSGI(vout.PosH);
			#endif

			vout.ShadowDetails = lerp(vout.ShadowDetails, 0, 0.22 + saturate(vout.PosY) * 0.07);		
			if (!gUseShadowDetails) vout.ShadowDetails = 0;
			vout.Lighting *= cut_passid_dif_spec.z;// * lerp(1, P.y, advFade * 0.5);
		#else
			if (color_fade.w < 0.5 || distance > 30){
				vout = (PS_IN)0;
				vout.PosH = -1;
				return vout;
			}

			vout.NormalW = in_normal;
			posW = origPosW;
			// applyWind(posW.xyz, up, saturate(rotation_width_height.w * -up.y - 0.05) * affectedByWind, true, windPhaseOffset);
			// posW.xyz += posWWindOffset;
			applyWind(windPhaseOffset, posW.xyz, up, affectedByWind, saturate(color_fade.w), rotation_width_height.w, true, cut_passid_dif_spec.w);
			GENERIC_PIECE_WORLD(posW);
		#endif
	#endif

	#if 0
	{
		bool prev = false;
		float3 posIn = posW.xyz - extSceneOffset;
		float phaseOffset = windPhaseOffset * 0;
		float2 extWindVel = float2(20, 20);
		extWindVel = rotate2d(extWindVel, ksGameTime / 3e4);
		// float extWindWave = 1;

		float ang = atan2(extWindVel.x, -extWindVel.y);
		float2 posInR = rotate2d(posIn.xz, ang) * float2(0.2, 1);

		// float totalHeight = heightHalf * 2;
		// float2 windXZ = (prev ? windOffsetPrev2(posIn.xyz + phaseOffset, 1, 2.2) : windOffset2(posIn.xyz + phaseOffset, 1, 2.2)) * 0.5;
		// float2 windXZ = (rotate2d(posInR, 0) + extWindVel * extWindWave * 0) * 0.02;

		// float3 noiseUV = float3(windXZ, dot(windXZ, 0.371)) * 0.5;
		// float4 noise = txNoise3D.SampleLevel(samLinearSimple, noiseUV, 0) * 0.6
		// 	+ txNoise3D.SampleLevel(samLinearSimple, noiseUV * 2.07 + 1.71, 0) * 0.3
		// 	+ txNoise3D.SampleLevel(samLinearSimple, noiseUV * 4.19 + 0.91, 0) * 0.1;

		// float windOffset = txWindOffsets.SampleLevel(samLinearSimple, posIn.xz / 80, 0);
		// vout.Color *= float3(windOffset, 1 - windOffset, 0);
		// vout.SpecIntensity += 0.1 * windOffset;
		// pos.xz += noise.xy * heightHalf;

		// float y = saturate(length(windXZ));
		// float verticalOffset = (1 - sqrt(1 - y * y)) * totalHeight;
		// float3 wind = float3(windXZ.x, 0, windXZ.y) * totalHeight;
		// pos += wind - up * (dot(wind, up) - verticalOffset);
	}
	#endif

	return vout;
}