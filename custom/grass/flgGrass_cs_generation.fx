#include "flgGrass.hlsl"
#include "flgGrass_generation.hlsl"
#include "include_new/base/_gamma.fx"

#define DEFAULT_THRESHOLD_PARAMS float4(0.5, 0.05, 0.02, 0.35)
// #define DEBUG_COLOR_TEST
// #define DEBUG_FADEK
// #define FORGE_SIZE_MULT 1

[numthreads(32, 32, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  float seed = 0.12345;

  float2 basePosOffset = float2(float(threadID.x), float(threadID.y)) * gMapStep;
  float2 basePosXZ = gSpawnPointA + basePosOffset;

  // float rand1 = frac((gSpawnPointAWorld.x + basePosOffset.x) / 16.98696) + frac((gSpawnPointAWorld.y + basePosOffset.y) / 198.17822)
  //   + frac(gPassID / 143.625);
  // #define RAND (rand1 = frac(rand1 * 43758.5453))

  // RAND_INIT(gSpawnPointAWorld + basePosOffset + float2(gPassID * 37, gMapStep * 94));
  // #define RAND RAND_CORE

  RAND_INIT((gSpawnPointAWorld + basePosOffset) * 0.01 + float2(gPassID * 37, gMapStep * 94));
  // #define RAND RAND_CORE

  // RAND_INIT(gSpawnPointAWorld + basePosOffset + float2(gPassID * 37, gMapStep * 94));
  float rand1 = RAND_CORE;
  // #define RAND (rand1 = frac(rand1 * 43758.5453))
  // #define RAND (rand1 = frac(rand1 * (__LINE__ + __LINE__ / 397. + __LINE__ / 3973.)))
  #define RAND (rand1 = frac(rand1 * (__LINE__ + __LINE__ / 397. + __LINE__ / 3973.)))

  // SHUFFLE A BIT
  float2 posXZ = basePosXZ;
  posXZ += (float2(RAND, RAND) - 0.5) * gMapStep;

  // UV & FACTORS FOR USING GATHER
  SamplingParams S = getSamplingParams(posXZ);
  if (S.isOutside) return;

  // POSITION
  float3 normal;
  float ao;
  float3 pos = float3(posXZ.x, calculateDepth(S, false, normal, ao), posXZ.y);
  if (abs(normal.y) < 0.4 || !isVisible(pos + float3(0, 0.3, 0), 2)) return;
  
  // PARAMS
  GrassParams GP = getGrassParams(S);
  
  // COLOR
  float4 color = txColor.SampleLevel(samLinearClamp, S.mapUV, 0);
  float colorTest = colorThreshold(color.rgb, GP.mask_mainThreshold, GP.mask_redThreshold, GP.mask_minLuminance, GP.mask_maxLuminance);
  if (color.a < 0.7 || colorTest < 0.2 + RAND * 0.3) return;

  float blurryMap = txColor.SampleLevel(samLinearClamp, S.mapUV, 2).a;;
  float edgeK = pow(blurryMap, 2);
  // float singleness = sqrt(1 - blurryMap); // pre 0.2.3
  float singleness = (1 - blurryMap);
  colorTest *= edgeK;

  // LOW FREQ NOISE
  float4 lfreq = saturate(txNoise.SampleLevel(samLinearWrap, (gSpawnPointAWorld + basePosOffset) / 40, 0));
  const float RAND_LF0 = lfreq.x;
  const float RAND_LF1 = lfreq.y;
  const float RAND_LF2 = lfreq.z;
  const float RAND_LF3 = lfreq.w;

  // RELATION TO CAMERA
	float3 posC = pos - gCameraPos.xyz;
  float distanceReal = length(posC);
	float3 toCamera = posC / distanceReal;

  // LOD DISAPPEARANCE
  float distanceAdj = length(pos - float3(gMapOrigin.x, gCameraPos.y, gMapOrigin.y));
  // float distanceHor = lerp(distanceReal, length(posC.xy), saturate(gPassID));
  float distanceHor = distanceAdj;
  float fadeK = saturate(1.6 - distanceHor * gMaxDistanceInv * 2.0);
  // float lodFix = saturate(2 - distanceHor * gMaxDistanceInv * 2.4);
  float lodFix = fadeK;
  if (fadeK <= 0.01) return;
 
  // DEFORMATION
  float deformationValue = saturate(getDeformation(pos.xz)).x;
  float maxHeight = deformationValue == 0 ? 1e6 : saturate(1 - deformationValue / 0.99) * 0.5;
  bool cutBit = deformationValue == 1;
  if (cutBit) {
    GP.shape_cut = 1;
    GP.shape_tidy = 1;
    GP.shape_size = 0.3;
    maxHeight = 1e6;
  }

  // ORIENTATION
	float3 up = normalize(-normal + float3(0, -2, 0));
  float2 rotation = normalize(float2(RAND - 0.5, RAND - 0.5));

  // CUT GRASS
  [branch]
  if (gDebugPasses) {
    float colorID = gFarPass ? 5 - gPassID : gPassID;
    if (colorID == 0) color.rgb = float3(1, 0, 0);
    else if (colorID == 1) color.rgb = float3(1, 1, 1);
    else if (colorID == 2) color.rgb = float3(0, 1, 0);
    else if (colorID == 3) color.rgb = float3(0, 0, 1);
    else if (colorID == 4) color.rgb = float3(0, 1, 1);
    else if (colorID == 5) color.rgb = float3(0.6, 0, 0.6);
    color.rgb /= 4;
  }

  #ifdef DEBUG_FADEK
    color.rgb = float3(fadeK, 1 - fadeK, 0) * 0.25;
  #endif

  // MATERIAL PARAMS
  float4 materialParams = txMaterialParams.SampleLevel(samLinearClamp, S.mapUV, 0);
  float diffuseK = materialParams.x / max(materialParams.y, 0.01);
  float specFade = saturate(1.2 - distanceReal / 40);
  float specEXP = max(255 * materialParams.w, 1);
  float specC = materialParams.z;
  float specK = calculateSpecular(normal, -toCamera, specEXP) * specC;

  // color.rgb = max(0, color.rgb / materialParams.y) * materialParams.y;
  color.rgb = applyGamma(color.rgb, 1);

  // CAR’S WIND
  float3 air = getAir(pos.xz);
  air -= sign(air) * min(pow(RAND, 2) * 0.05, abs(air));
  air = sign(air) * pow(air, 2);

  // SIZE
  #ifdef DEBUG_COLOR_TEST
    color.rgb = float3(colorTest, 1 - colorTest, 0) / 4;
  #endif

  float finHeight = 0.2 * lerp(0.9 + pow(frac(rotation.x * 239.165), 40) * 0.4 - 1 * pow(RAND_LF0 * RAND_LF1, 2), 0.75, GP.shape_tidy);
  float finWidth = finHeight * GP.shape_width;
  float tiltMult = lerp(0.5, 0.1, GP.shape_tidy);
	// finHeight *= 1 + distanceReal / 120;
	// finHeight *= 0.5 + 0.5 * saturate(RAND_LF0 + RAND_LF1);

  if (finHeight < 0.02) return;

  int texGroupID = gPassID <= 2 ? -1 : getGroup(GP.groupChances, RAND);
  int texPieceID = -1;
  if (texGroupID != -1){
    uint count = gTexGroupsCount[texGroupID];
    float randomValue = RAND * gTexGroupsChanceTotal[texGroupID];
    int texPieceIDInGroup = 0;
    for (uint i = 0; i < count; i++){
      if ((randomValue -= gTexGroups[texGroupID].pieces[i].chance) <= 0){
        texPieceIDInGroup = i;
        break;
      }
    }
    CustomTexCSPiece TP = gTexGroups[texGroupID].pieces[texPieceIDInGroup];
    if (TP.sizeMult.x != 0){
      texPieceID = texGroupID * 8 + texPieceIDInGroup;
      finWidth = finHeight * TP.sizeMult.x * 0.5;
      finHeight *= TP.sizeMult.y;

      tiltMult = 0;
      lodFix = 1;
    }
  }

  // int texGroupID = -1;
  // int texPieceID = -1;

  float sizeMult = GP.shape_size;
  #ifdef FORGE_SIZE_MULT
    sizeMult = FORGE_SIZE_MULT;
  #endif
  if (RAND > (gPassID < 1 ? 1 : 2) / sizeMult){
    return;
  }
  if (gPassID == 0) sizeMult = sizeMult * 0.5;
  if (gPassID == 1) sizeMult = sizeMult * 0.75;
  // sizeMult = 1;
  // if (gPassID <= 1) sizeMult = min(sizeMult, gPassID == 0 ? 2 : (sizeMult + 1) / 2);
  sizeMult *= colorTest;
  // sizeMult *= saturate(fadeK * 3);
  finHeight *= sizeMult;
  finWidth *= sizeMult;
  // color.rgb = float3(fadeK, 1 - fadeK, 0) * 0.25;

  float repeatBase = (gPassID == 0 && sizeMult < 0.5 ? 0.2 : 0.1) * (1 + RAND);
  float repeatK = round(clamp(edgeK * repeatBase / finWidth, 1, 4));
  if (gPassID >= 4) repeatK = 2;
  // repeatK = 1;

  float deformation = 1 - saturate(maxHeight / finHeight);
  float deformSquash = lerpInvSat(deformation, 0, 0.7) * lerpInvSat(deformation, 1, 0.7);
  float deformationExp = 1 / (1 + 2 * deformSquash);
  up = normalize(up - air * 6 + normalize(float3(RAND - 0.5, 0, RAND - 0.5)) * (tiltMult + lerpInvSat(deformation, 0.7, 1) * 50));
  color.rgb *= 1 - pow(deformation, 4) * 0.1;
  // color.rgb = float3(1, 0, 0);
  // color.rgb = float3(lodFix, 1 - lodFix, 0);

  // color *= RAND_LF2;
 
  float finalSizeMult = (1 + 0.1 * pow(deformation, 4)) * lodFix;
  finHeight *= finalSizeMult * lerp(1, 0.2, deformSquash);
  finWidth *= finalSizeMult;

  if (!isVisible(pos + float3(0, finHeight / 2, 0), max(0.5, finHeight))){
    return;
  }

  // color.rgb = normal * 0.25 + 0.25;
  // color.rgb = float3(0.04, 0.08, 0.02);
  // color.rgb = lerp(color.rgb, float3(0.04, 0.08, 0.02), 0.1);
  // color.rgb = float3(RAND, RAND, RAND) / 2;
  // return;

  // PACKING
  FoliageFinVertex fin = (FoliageFinVertex)0;
  fin.position = pos + float3(0, deformation * lerp(0.01, 0.02, RAND), 0);
  FLG_pack(fin.type_flip, float2(floor(RAND * gTexGrid.x), repeatK));
  FLG_pack(fin.color_fade, float4(color.rgb, fadeK * (1 - deformation * 0.5)));
  FLG_pack(fin.rotation_width_height, float4(rotation, finWidth, finHeight));
  FLG_pack(fin.normal_deform_ao, float4(normalEncode(normal, true), deformationExp, ao));
  FLG_pack(fin.up_singleness, float4(up, singleness));
  FLG_pack(fin.shadowmult_passid_dif_spec, float4(lerp(0.4, 0.6, RAND) * GP.shape_cut, texPieceID, diffuseK, specK));
  buResult.Append(fin);
}