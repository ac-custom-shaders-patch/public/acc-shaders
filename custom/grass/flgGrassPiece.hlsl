#include "include/common.hlsl"
#include "include/samplers.hlsl"

cbuffer cbPiece : register(b11) {
  float4x4 gViewProj;
  float2 gMapPointA;
  float2 gMapPointB;
  float gFrameTime;
  float gMapOriginY;
  float gMapDepthSize;
  float gOpacity;
  float4x4 gTransform;
  float3 gVelocityW;
  float gShadowOpacity;
};

struct VS_IN {
  float3 PosL : POSITION;
  uint NormalL : NORMAL_ENCODED;
};

struct VS_Deform {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float Deformation : TEXCOORD1;
  float ShadowMult : TEXCOORD2;
};

struct VS_Air {
  float4 PosH : SV_POSITION;
};

struct VS_GrassMove {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float2 TexPrev : TEXCOORD2;
  float ReduceBy : TEXCOORD3;
};

struct VS_Move {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float ReduceBy : TEXCOORD1;
};

static const float2 BILLBOARD[] = {
	float2(-1, -1),
	float2(1, -1),
	float2(-1, 1),
	float2(-1, 1),
	float2(1, -1),
	float2(1, 1),
};

Texture2D<float> txDepthGrass : register(t0);

float getY(float2 posXZ){
  float2 uv = (posXZ - gMapPointB) / (gMapPointA - gMapPointB);
  float r = txDepthGrass.SampleLevel(samLinearBorder0, uv, 0);
  return r == 0 ? 0 : gMapOriginY - r * gMapDepthSize;
}

float getDeformation(float3 posW){  
  float distance = posW.y - getY(posW.xz);
  return saturate(1 - distance * 2);
}

float getShadowMult(float3 posW){  
  float distance = posW.y - getY(posW.xz);
  return lerpInvSat(distance, 0.9, 0.2);
}

