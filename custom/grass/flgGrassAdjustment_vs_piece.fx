#include "include_new/base/cbuffers_vs.fx"
#include "flgGrassAdjustment.hlsl"

static const float2 BILLBOARD[] = {
	float2(-1, -1),
	float2(1, -1),
	float2(-1, 1),
	float2(-1, 1),
	float2(1, -1),
	float2(1, 1),
};

PS_IN_Solid main(uint id: SV_VertexID) {
  float2 ver = BILLBOARD[id];
  PS_IN_Solid vout;
  float3 posW = mul(float4(-ver.x, 0, ver.y, 1), gWorld).xyz;
  vout.PosH = mul(float4(posW, 1), gViewProj);
  vout.PosW = posW.xz; 
  vout.Tex = (ver.xy * 0.5 + 0.5) * gTexScale + gTexOffset;
  return vout;
}
