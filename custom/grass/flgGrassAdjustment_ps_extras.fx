#include "flgGrassAdjustment.hlsl"

float4 main(PS_IN_Solid pin) : SV_TARGET {
  return applyExtras(gColor, pin.PosW);
}