cbuffer PerDrapeBuffer : register(b11) {
	uint gCount0;
	uint gCount1;
	float2 _pad0;
};

RWByteAddressBuffer indirectBuffers : register(u0);

[numthreads(1, 1, 1)]
void main(uint3 id : SV_DispatchThreadID) {
	// indirectBuffers.Store4(0, uint4(gCount0 * 15, 1, 0, 0));
	// indirectBuffers.Store4(16, uint4(gCount1 * 3, 1, 0, 0));
	indirectBuffers.Store4(0, uint4(gCount0 * 6, 1, 0, 0));
	indirectBuffers.Store4(16, uint4(0, 1, 0, 0));
}