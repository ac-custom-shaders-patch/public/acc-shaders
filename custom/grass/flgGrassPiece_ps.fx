#include "flgGrassPiece.hlsl"

float2 main(VS_Deform pin) : SV_TARGET {
  float v = max(abs(pin.Tex.x), abs(pin.Tex.y));
  float d = saturate((1 - v) * 10);
  float a = sqrt(d);

  if (pin.Deformation == 2) {
    return float2(1, a);
  }

  return float2(pin.Deformation, a * pin.ShadowMult);

  // float v = max(abs(pin.Tex.x), abs(pin.Tex.y));
  // float d = saturate((1 - v) * 10);
  // float a = sqrt(d * (0.5 + 0.5 * saturate(pin.Opacity)));
  // if (pin.Opacity == 2) {
  //   return float2(1, a);
  // }

  // return float2(saturate(d * pin.Opacity) * 0.98, a);
}