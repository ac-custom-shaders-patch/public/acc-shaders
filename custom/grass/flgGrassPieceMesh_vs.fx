#include "include_new/base/cbuffers_vs.fx"
#include "flgGrassPiece.hlsl"

VS_Deform main(VS_IN vin) {
  VS_Deform vout;

  float4 posW = mul(float4(vin.PosL, 1), gTransform);
  float4 posH = mul(posW, gViewProj);

  vout.PosH = posH;
  vout.Tex = 0;
  vout.Deformation = min(getDeformation(posW.xyz), gOpacity);
  vout.ShadowMult = getShadowMult(posW.xyz) * gShadowOpacity;
  return vout;
}
