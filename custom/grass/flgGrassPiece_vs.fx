#include "include_new/base/cbuffers_vs.fx"
#include "flgGrassPiece.hlsl"

VS_Deform main(uint id: SV_VertexID) {
  VS_Deform vout;

  float2 ver = BILLBOARD[id];
  float4 posW = mul(float4(-ver.x * 1.2, -1, ver.y * 1.2, 1), gTransform);
  float4 posH = mul(posW, gViewProj);
  
  vout.PosH = posH;
  vout.Tex = ver;
  vout.Deformation = min(getDeformation(posW.xyz), gOpacity);
  vout.ShadowMult = getShadowMult(posW.xyz) * gShadowOpacity;
  return vout;
}
