#define ALPHATEST_THRESHOLD 0.8
#define A2C_SHARPENED_SIMPLE

#ifndef MODE_GBUFFER
  #define MODE_MAIN_FX
#endif

#define INPUT_DIFFUSE_K 1
#define INPUT_AMBIENT_K 1
#define INPUT_SPECULAR_K 0.14
#define INPUT_SPECULAR_EXP 3
#define INPUT_EMISSIVE3 0
#define NO_AMBIENT_MULT
#define IS_TRACK_MATERIAL 1

#define NO_CARPAINT
// #define NO_GBUFFER
#define NO_SSAO
#define NO_EXTAMBIENT
// #define INCLUDE_GRASS_CB
#define NO_EXTSPECULAR
#define LIGHTINGFX_GRASS
#define GBUFF_MASKING_MODE 0

#include "include_new/base/_include_ps.fx"
#include "flgGrass.hlsl"

Texture2D txGrass : register(t0);

float calculateSpecular(float3 normalW, float3 toCamera, float specularEXP){
  float specularBase = saturate(dot(normalize(toCamera - ksLightDirection.xyz), normalW));
  return pow(specularBase, specularEXP);
}

#if defined(CREATE_SHADOW_BUFFER) || defined(MODE_GBUFFER_)
float main(PS_IN pin) : SV_TARGET {
#else
PS_OUT main(PS_IN pin) {
#endif

  #if !defined(MODE_GBUFFER) && !defined(CREATE_SHADOW_BUFFER)
    // if (GAMMA_FIX_ACTIVE) pin.Backlit *= pow(1 - pin.Tex.y, 2);
  #endif
  pin.Tex.y = pow(saturate(pin.Tex.y), pin.Deformation);

  float4 texValue = txGrass.Sample(samLinear, pin.Tex);
  // texValue = 1;

  #if defined(CREATE_SHADOW_BUFFER)
    uint2 pos = uint2(pin.PosH.xy);
    uint2 xy2 = pos.xy % 2;
    if (texValue.a < 0.25 || dot(xy2, 1) == 1) discard;
    return 1;
  #else
    #ifndef MODE_GBUFFER
      texValue.a = saturate((texValue.a - 0.4) / max(fwidth(texValue.a), 0.001) + 0.5);
    #endif
    clip(min(texValue.a - 0.25, dot2(pin.PosC) - gInteriorClip));
    float4 txDiffuseValue = 1;
    #ifdef MODE_GBUFFER
      READ_VECTORS
      float3 lighting = 0;
      texValue.a = 1;
    #else
      float wetK = saturate(gWetK) * pin.Occlusion;
      // float wetK = 1 * pin.Occlusion;
      // pin.Fade = 1;
      // float texAO = lerp(1, texValue.g * (0.85 + 0.15 * pow(pin.PosY, 1)), pin.TexDimming);
      float texAO = lerp(1, (0.5 + 0.5 * texValue.g) * pow(pin.PosY, 0.5), pin.TexDimming);
      float texBacklit = saturate(texValue.g * 5 - 3);
      float texSpec = saturate(texValue.g * 10 - 9);
      float texSat = saturate((texValue.g - max(texValue.r, texValue.b)) * 2);
      // float texDot = saturate(1 - dot(texValue.rgb, 10));
      float3 baseColor = lerp(pin.Color, applyGamma(texValue.rgb * GAMMA_OR(1, gTextureBrightness), gTextureBrightness), (1 - texSat) * pin.Fade);

      // texAO = lerp(texAO, 1, texDot);
      // baseColor = lerp(baseColor, float3(1, 1, 0), texDot);

      baseColor *= lerp(1, GAMMA_LINEAR(0.65), wetK);
      // baseColor = float3(pin.PosY, 1 - pin.PosY, 0);
      // baseColor = float3(pin.ShadowDetails, 1 - pin.ShadowDetails, 0);

      baseColor = lerp(baseColor, 1, pow(pin.PosY, 2) * saturate(-gWetK));

      // clip(0.01 - texDot);

			// float4 tex0 = mul(float4(pin.PosC + ksRealCameraPosition, 1), ksShadowMatrix0);
			// float vout_Shadow = getShadowBiasMult(pin.PosC, 0, float3(0, 1, 0), tex0, 0, 1);
      // pin.Lighting *= vout_Shadow;

      float fakeShadow = lerp(1, saturate(texAO * 4.5 - 3.25), pin.ShadowDetails * texSat);
      pin.Lighting *= fakeShadow;

      // baseColor = float3(saturate(texValue.g * 4.5 - 2.5), 1 - saturate(texValue.g * 4.5 - 2.5), 0);

      // float texShad = lerp(1, pow(pin.PosY, 2), pin.TexDimming);
      // pin.Ambient *= texShad;
      // pin.Lighting *= texShad;

      READ_VECTOR_TOCAMERA
      #ifdef NO_SS_EFFECTS
        float3 lighting = baseColor * (pin.Ambient
          + pin.Lighting) * texAO
          + pin.Backlit * texBacklit * pin.PosY;
      #else
        float3 lighting = baseColor * (pin.Ambient * AO_LIGHTING.w
          + pin.Lighting * AO_EXTRA_LIGHTING + GI_LIGHTING * 0.33) * texAO
          + pin.Backlit * texBacklit * pin.PosY;
      #endif
      lighting += extSpecularColor.rgb * (pin.SpecIntensity * (0.3 + texSpec * 2) * fakeShadow);

      float3 localNormal = normalize(pin.LocalSpecNormal);
      lighting += extSpecularColor.rgb * (calculateSpecular(localNormal, -toCamera, 40) * gSpecularIntensity * pin.LocalSpecShadow * fakeShadow);

      float fresnel = gReflectivity * pow(pin.PosY, 2) * pow(1 - abs(toCamera.y), 2) * pin.Fade;
      float3 reflDir = toCamera * float3(-1 /* fix for X inverse */, -1, 1) + float3(0, 0.25, 0);
      float3 reflColor = txCube.SampleLevel(samLinearSimple, reflDir, 5.5).rgb;
      lighting += fresnel * reflColor;      
    #endif

    #ifdef EARLYZ
      PS_OUT ret;
      lighting = 0;
      ret.result = texValue.a;
      return ret;
    #endif

    RETURN_BASE(lighting, texValue.a);
  #endif
}