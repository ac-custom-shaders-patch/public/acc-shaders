#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float gMargin;
  float gScaleAngle;
  // float gAspectRatio;
  // float gScaleLinear;
  float2 gDMult;
  uint gMode;
  float3 gPad;
}

float4 samplePiece(float2 xy, float z, float sx, float sy){
  if (z < 0) return 0;
  float2 xya = xy / z * gMargin;
  float2 u = (0.5 - xya * 0.5 + float2(sx, sy)) / float2(3, 2);
  float d = max(abs(xya.x), abs(xya.y));
  // return float4(txDiffuse.SampleLevel(samLinearSimple, u, 0).rgb, 1) * pow(saturate(remap(d, gMargin, 1, 1, 0)), 2);
  return float4(txDiffuse.SampleBias(samLinear, u, -1).rgb, 1) * pow(saturate(remap(d, gMargin, 1, 1, 0)), 2);
}

float4 sampleColor(float3 dir){
  if (dir.z < -0.57) return 0;

  float4 v = 0;
  v += samplePiece(dir.xy * float2(-1, 1), dir.z, 1, 0);
  v += samplePiece(dir.yx * float2(1, -1), -dir.z, 1, 1);
  v += samplePiece(dir.zy, dir.x, 2, 0);
  v += samplePiece(dir.zy * float2(-1, 1), -dir.x, 0, 0);
  v += samplePiece(dir.zx * float2(1, -1), dir.y, 0, 1);
  v += samplePiece(-dir.zx, -dir.y, 2, 1);
  return saturate(remap(-dir.z, 0.55, 0.57, 1, 0)) * v / v.w;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float2 d = (pin.Tex * 2 - 1) * gDMult; //float2(gAspectRatio, 1) * gScaleLinear;
  float2 w;
  float m = 1;
  if (gMode == 0){ 
    // Orthographic projection:
    // d = f * sin(w) → w = arcsin(d)
    w = asin(length(d)) * normalize(d);
    m = saturate(remap(length(d), 0.995, 1, 1, 0));
  } else if (gMode == 1){ 
    // Equisolid angle:
    // d = 2f * sin(w/2) → w = 2 * arcsin(d / 2)
    w = 2 * asin(length(d) / 2) * normalize(d);
  } else if (gMode == 2){ 
    // Equidistant angle:
    // d = f * w → w = d
    w = d;
  } else {
    // Stereographic angle:
    // d = 2f * tan(w/2) → w = 2 * arctan(d / 2)
    w = 2.2 * atan(length(d) / 2) * normalize(d);
  }

  w *= gScaleAngle;
  float xs = sin(w.x);
  float xc = cos(w.x);
  float ys = sin(w.y);
  float yc = cos(w.y);
  return m * sampleColor(float3(xs * yc, -ys, xc * yc));

  
  // float2 angle = float2((pin.Tex.x + 0.5) * M_PI * 2, (pin.Tex.y - 0.5) * M_PI);
  // float dirX = (pin.Tex.x * 2 - 1) * 1920./1080.;
  // float dirY = (pin.Tex.y * -2 + 1);
  // return sampleColor(normalize(float3(dirX, dirY, sqrt(1 - pow(dirX, 2) - pow(dirY, 2)))));
}