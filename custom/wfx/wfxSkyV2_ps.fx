#define USE_PS_FOG
#define USE_PS_FOG_RECOMPUTE
#define USE_BACKLIT_FOG
#define FOG_NEW_FORMULA cFogNewFormula

#ifdef COVERS_FG
  #define FOG_MULT_MODE
#endif

#include "wfx_common_sky_v2.hlsl"
#include "include/bicubic.hlsl"
#include "../../recreated/include_new/base/_include_ps.fx"

Texture2D txStarMap : register(TX_SLOT_MAT_0);
Texture2D<float> txFeaturesMask : register(TX_SLOT_MAT_1);
Texture2D txEarthMap : register(TX_SLOT_MAT_2);

#ifdef EARTH_SPACE_MODE
  cbuffer cbExtEarth : register(b8) {
    float3 moonDirection;
    float _pad;
  };
#endif

float4 sampleNoise(float2 uv, bool fixUv = false, float level = 0){
  if (fixUv){
    float textureResolution = 32;
    uv = uv * textureResolution + 0.5;
    float2 i = floor(uv);
    float2 f = frac(uv);
    uv = i + f * f * (3 - 2 * f);
    uv = (uv - 0.5) / textureResolution;
  }
	return txNoise.SampleLevel(samLinearSimple, uv, level);
}

float3 sampleStars(float2 uv){
  #ifdef HIGH_QUALITY_STARS
    float2 textureResolution = float2(8192, 4096);
    float mip0 = txStarMap.CalculateLevelOfDetail(samLinear, uv.y) - 0.5;
    return sampleBicubic(txStarMap, uv, mip0).xyz;
  #else
    return txStarMap.Sample(samLinear, uv).xyz;
  #endif
}

float3 getAtmosphericScattering(float3 dir, out float3 extras){
  float3 sky0, sun0, fex0, sky1, sun1, fex1;
  gSkySun.calculate(dir, sky0, sun0, fex0);
  gSkyOpposite.calculate(dir, sky1, sun1, fex1);

  float sunSideBase = saturate(dot(dir, cGradientDirection) * 0.5 + 0.5);
  float sunSide = sunSideBase;
  float3 totalSky = lerp(sky1, sky0, sunSide);
  float3 totalSun = lerp(sun1, sun0, sunSide);
  float3 totalFex = lerp(fex1, fex0, sunSide);
  applyExtraGradients(dir, totalSky);

  #ifdef COVERS_BG
    extras = 0;
    return totalSky * cBrightnessMult;
  #endif

  #ifdef COVERS_FG
    totalSky = 0;
  #endif

  float skyEdge = getSkyEdge(dir);
  float sunDotV = dot(dir, cSunDirection);
  float sunShape = getSunPoint(sunDotV);
  float moonDotV = saturate(dot(dir, cMoonDirection));
  float moonShapeInv = saturate(1.0 - getMoonPoint(moonDotV) * 100);
  float moonMie = pow(moonDotV, cMoonMieExp);
  extras = 0;
  extras += moonMie * cMoonMieColor * skyEdge;
	extras += totalSun * sunShape * moonShapeInv;
  extras *= cBrightnessMult;

  [branch]
  if (cStarsBrightness > GAMMA_OR(1e-5 * cBrightnessMult, 0.001)){
    float3 dirRotated = mul(dir, (float3x3)cStarMapRotation);
    float u = atan2(-dirRotated.x, dirRotated.z) / (2 * M_PI) + 0.5;
    float v = 0.5 - asin(dirRotated.y) / (M_PI / 2) * 0.5;
    float3 txStarsValue = sampleStars(float2(u, v)) * (1 - sunShape) * moonShapeInv * totalFex * cStarsColor;
    float3 starsBlend = lerp(luminance(txStarsValue), txStarsValue, cStarsSaturation);
    extras += pow(abs(starsBlend), cStarsExponent) * cStarsBrightness;
  }

  if (sunShape) {
    // return 0;
    // extras = 1e9;
  }

  return totalSky * cBrightnessMult;
}

float iSphere(in float3 ro, in float3 rd, in float4 sph) {
  float3 oc = ro - sph.xyz;
  float b = dot(oc, rd);
  float c = dot(oc, oc) - sph.w * sph.w;
  float h = b * b - c;
  if (h < 0.0) return -1.0;
  h = sqrt(h);
  return -b - h;
}

float3x3 rotate3d(float s, float c, float3 axis) {
	float t = 1 - c;
	float x = axis.x;
	float y = axis.y;
	float z = axis.z;
	return float3x3(
		t * x * x + c,      t * x * y - s * z,  t * x * z + s * y,
		t * x * y + s * z,  t * y * y + c,      t * y * z - s * x,
		t * x * z - s * y,  t * y * z + s * x,  t * z * z + c);
}

float2 spiralUV(float2 uv, float texBlurry) {
  // From https://www.shadertoy.com/view/XsjGRd
	float reps = 2.0;
	float2 uv2 = frac(uv*reps);
	float2 center = floor(frac(uv*reps)) + 0.5;
	float2 delta = uv2 - center;
	float dist = length(delta);
	float angle = atan2(delta.y, delta.x);
	float nudge = dist * 4.0;
	float2 offset = float2(delta.y, -delta.x);
	float blend = max(abs(delta.x), abs(delta.y))* 2.0;
	blend = saturate((0.5 - dist) * 2.0);
	blend = pow(blend, 1.5);
	offset *= blend;
	return uv + offset * 1.1 * texBlurry;
}

float3 cloudMap(float2 uv){  
  float ownPos = length(loadVector(asuint(cStarMapRotation[3][0])) - frac(uv));
  float ownClouds = pow(lerpInvSat(ownPos, 0.05, 0.005), 2);
  float randomSeed = loadVector(asuint(cStarMapRotation[3][1])).x;
  float ownCloudsCoverage = loadVector(asuint(cStarMapRotation[3][1])).y;
  float curTime = frac(cStarMapRotation[3][2]) * 50;

  float2 nuv = spiralUV(uv * float2(2, 1.5) + randomSeed, 0.5);
  nuv += curTime + frac(randomSeed * 234.945);
  float cloudsL = min(txNoise.CalculateLevelOfDetail(samLinearSimple, nuv * 4) - 4, -2);
  float4 cloudsB = pow((sampleNoise(uv / float2(1, 2) + curTime * 0.2, true, cloudsL)
    + sampleNoise(uv / float2(1, 2) - curTime * 0.2 + 0.17, true, cloudsL)
    + sampleNoise(uv / float2(1, 2) - curTime * 0.2 + 0.29, true, cloudsL)) / 2.8, 2);
  // float4 cloudsB = pow(saturate(txStarMap.SampleLevel(samLinearSimple, (uv / 2 + curTime) * 0.1, 4) * 100), 0.1);
  float4 clouds0 = sampleNoise(nuv, true, cloudsL);
  float m = min(1, 7 - abs(uv.y * 2 - 1) * 8) * smoothstep(0, 1, saturate(cloudsB.w * 1.4));
  clouds0.r = lerp(clouds0.r, ownCloudsCoverage, ownClouds) * m;
  nuv += frac(randomSeed * 776.669);
  float4 clouds1 = sampleNoise(spiralUV(nuv * 2 + 0.688 + curTime, 1), true, cloudsL + 1) * m;
  float4 clouds2 = sampleNoise(spiralUV(nuv * 4 + 0.917 - curTime, 0), false, cloudsL + 2) * m;
  nuv.y /= 1.33;
  nuv += frac(randomSeed * 483.543);
  float4 clouds3 = sampleNoise(spiralUV(nuv * 8 + 0.674 + curTime, 0), false, cloudsL + 3) * m;
  float4 clouds4 = sampleNoise(spiralUV(nuv * 16 + 0.763 - curTime, 0), false, cloudsL + 4) * m;
  float4 clouds5 = sampleNoise(nuv * 32 + 0.678 + curTime, false, cloudsL + 5);
  float4 clouds6 = sampleNoise(nuv * 64 + 0.791 - curTime, false, cloudsL + 6);
  float cloudsAvg = clouds0.r * 1.6 + clouds1.r * 1.2 + clouds2.r * 0.4 + clouds3.r * pow(clouds0.w, 2)
    + clouds4.r * pow(clouds0.b, 8) + saturate(clouds5.r * 0.2 + clouds6.g * 0.8) * lerp(0, 1, pow(clouds0.g, 4)) + clouds6.r * 0.4;
  float clouds = pow(smoothstep(0, 1, saturate(cloudsAvg / 2.4 - 0.2)), 3);
  return float3(clouds, clouds3.w, clouds2.w);
}

#include "include/poisson.hlsl"

float4 main(PS_IN_Sky pin
  #ifdef EARTH_SPACE_MODE
  , out float outDepth : SV_Depth
  #endif
  ) : SV_TARGET {

// discard;

  float3 toCamera = normalize(pin.PosW), extras;
  float refraction = lerpInvSat(toCamera.y, 0.01, -0.01);
  if (refraction) {
    toCamera += normalize(cross(toCamera, extDirUp)) * (
      sin(ksGameTime / -5e2 + toCamera.y * 3e3 - toCamera.x * 100)
      + 0.5 * sin((ksGameTime / -4e2 + toCamera.y * 3e3) * 2.17 + toCamera.x * 100 + 0.116)) / 8e3 * refraction;
  }
  float3 color = getAtmosphericScattering(toCamera, extras);
  processRainbow(color, toCamera);

  float fogMult = cFogMult + cFogMult2 * pow(saturate(cFogRange - toCamera.y * cFogRange), cFogMultExp);
  if (!FOG_NEW_FORMULA){
    pin.Tex.x = 1;
  }

  #ifndef COVERS_FG
  #ifdef EARTH_SPACE_MODE
  // color = 0;
  fogMult = 0;
  outDepth = 0.9999;
  #endif

  [branch]
  if (toCamera.y < 0 && cStarMapRotation[3][3]) {
    float altitude = cStarMapRotation[3][3];
    float3 ownPos = float3(0, 6371e3 + altitude, 0);
    float sph = iSphere(ownPos, toCamera, float4(0, 0, 0, 6371e3));
    #ifndef EARTH_SPACE_MODE
      float spaceLook = 0;
    #else
      float spaceLook = saturate(altitude / 5e4 - 1);
    #endif
    if (sph > 0) {
      float3 pos = ownPos + sph * toCamera;
      float3 nm = normalize(pos);
      float3 nmUV = mul(
        mul(nm, rotate3d(sin(cStarMapRotation[2][3]), cos(cStarMapRotation[2][3]), float3(0, 1, 0))), 
        rotate3d(sin(cStarMapRotation[1][3]), cos(cStarMapRotation[1][3]), float3(1, 0, 0)));
      float2 uv = float2(-atan2(nmUV.x, nmUV.y) / (2 * M_PI) - cStarMapRotation[0][3], 0.5 - asin(clamp(nmUV.z, -1, 1)) / M_PI);        
      float mip0 = txEarthMap.CalculateLevelOfDetail(samLinearSimple, uv.y) - 0.5;
      float mip1 = txEarthMap.CalculateLevelOfDetail(samLinear, uv.y) - 0.5;
      float2 uvClamped = uv.y < (frac(uv.x) > 0.995 || frac(uv.x) < 0.005 ? 0.04 : 0.007) ? float2(0.5, 0.002) : uv.y > 0.99 ? float2(0.5, 0.998) : uv;
      float mipClamped = clamp(mip1, 0, 5);
      float4 map;
      [branch]
      if (mipClamped == 0) {
        map = sampleBicubic(txEarthMap, uvClamped, mipClamped);
      } else {
        map = txEarthMap.SampleLevel(samLinear, uvClamped, mipClamped);
      }
      float waterMask = lerpInvSat(map.w, 0.1, 0.05);
      float nightMask = lerpInvSat(map.w, 0.18, 1);
      float3 nmFlat = nm;
      float nmDdx = ddx(dot(map.rgb, 1));
      float nmDdy = ddy(dot(map.rgb, 1));
      #ifdef EARTH_SPACE_MODE
        float4 ret = 0;
        float tot = 0;
        #define DISK_SIZE 7
        [unroll]
        for (uint i = 0; i < DISK_SIZE; ++i) {
          float2 offset = SAMPLE_POISSON(DISK_SIZE, i);
          float4 map = txEarthMap.SampleLevel(samLinear, uvClamped + offset * (float2(0.25, 1) * 0.0015), mipClamped);
          nmDdx += ddx(dot(map.rgb, 1));
          nmDdy += ddy(dot(map.rgb, 1));
        }
        nmDdx /= 8;
        nmDdy /= 8;
      #endif
      nm = normalize(nm + (nmDdy * extDirUp + nmDdx * normalize(cross(extDirUp, extDirLook))) * spaceLook * (1 - waterMask) * 0.3 / (1 + (mip0 + 0.5) * 2));

      float occ = max(waterMask, saturate(luminance(map.rgb) * 40));
      float shadow = saturate(dot(nm, -ksLightDirection.rgb));
      // TODO: Moon shadow
      float3 lightColor = ksLightColor.rgb * shadow;
      float3 ground = lightColor + getAmbientBaseAt((sph / 2) * toCamera, float3(0, 1, 0), 1);
      float3 diffColor = lerp(pow(max(map.rgb, 0), GAMMA_OR(2, 1)), map.rgb, waterMask * 0.1) * lerp(3 * float3(0.5, 1, 1.5), 1, spaceLook);
      ground *= diffColor;
      ground += extSpecularColor.rgb * lerp(0.02, 0.04, waterMask) 
        * reflectanceModel(-toCamera, -ksLightDirection.rgb, nm, lerp(50, 100, waterMask)) * occ * lerp(map.rgb, 1, waterMask);
      ground += pow(nightMask, 3) * float3(10, 9, 7) * saturate(-2 * dot(nm, -ksLightDirection.rgb)) * getEmissiveMult();
      ground *= 1 - waterMask * (1 - spaceLook);

      // float3 reflDir = fixReflDir(GET_REFL_DIR(toCamera, nmFlat), sph * toCamera, nmFlat, 1);
      // float3 reflColor = getReflectionAt(reflDir, -toCamera, 1, true, 0);
      // float3 reflColor = ksZenithColor.rgb;
      float3 reflColor = lerp(ksHorizonColor.rgb, ksZenithColor.rgb, 1 / rsqrt(-toCamera.y));
      ground += lerp(reflColor, lightColor * 0.05 * float3(0, 0.3, 1), spaceLook) * waterMask * occ * lerp(0.04, 1, pow(1 - abs(dot(nm, toCamera)), 5));

      {
        float sunAmount = saturate(dot(-toCamera, ksLightDirection.xyz));
        float3 fogColor = ksFogColor.rgb + ksLightColor.xyz * pow(sunAmount, extFogBacklitExp) * extFogBacklitMult;
        // ground = lerp(ground, fogColor, (1 - spaceLook) * pow(1 - exp(-sph / lerp(2e4, 2e5, saturate(altitude / 5e3 - 1))), 4));
        ground = withFogImpl(ground, (sph / 2) * toCamera, toCamera, pin.Tex.x, 1, 1).rgb;
        ground = lerp(ground, color, (1 - spaceLook) * pow(1 - exp(-sph / lerp(5e4, 5e5, saturate(altitude / 5e3 - 1))), 4));
      }

      #ifdef EARTH_SPACE_MODE
      if (spaceLook) {
        // if (iSphere(nmFlat * 6371e3, cSunDirection, float4(moonDirection * 384e6, 1737e3)) > 0) {
        //   ground *= 0.01;
        //   lightColor *= 0.01;
        // }

        float moonLight = cBrightnessMult * 0.001;
        ground += saturate(dot(nm, moonDirection)) * moonLight * diffColor * spaceLook;
        ground += lightColor * waterMask * float3(0, 0.5, 1) * 0.01 * pow(saturate(-dot(toCamera, nmFlat)), 8) * spaceLook;
        lightColor = ksLightColor.rgb * pow(saturate(dot(nmFlat, -ksLightDirection.rgb) + 0.1), 2);
       
        {  
          float3 pos = ownPos + (sph + 5e4) * toCamera;
          float3 nm = normalize(pos);
          float3 nmUV = mul(
            mul(nm, rotate3d(sin(cStarMapRotation[2][3]), cos(cStarMapRotation[2][3]), float3(0, 1, 0))), 
            rotate3d(sin(cStarMapRotation[1][3]), cos(cStarMapRotation[1][3]), float3(1, 0, 0)));
          float2 uv = float2(-atan2(nmUV.x, nmUV.y) / (2 * M_PI) - cStarMapRotation[0][3], 0.5 - asin(clamp(nmUV.z, -1, 1)) / M_PI);   
          float3 clouds = cloudMap(uv) * spaceLook;
          ground *= lerp(1, pow(saturate(1 - clouds.x * 4), 8), 0.9 * saturate(4 * abs(dot(nmFlat, -ksLightDirection.rgb))));
        }

        float3 clouds = cloudMap(uv) * spaceLook;

        // ground *= lerp(1, pow(saturate(1 - clouds.x * 2), 8), abs(dot(nmFlat, -ksLightDirection.rgb)));
        ground += clouds.x * (lightColor + saturate(dot(nmFlat, moonDirection)) * moonLight * 0.2) 
          * lerp(1, float3(0, 0.04, 0.2), pow(saturate(1 - abs(dot(nmFlat, -ksLightDirection.rgb))), 4)) * (1 + clouds.y * 0.3 + clouds.z * 0.3);
      }
      #endif

      float groundMix = smoothstep(0, 1, saturate(2 + saturate(altitude / 1e6 - 0.1) * 2 - sph / sqrt(altitude * 3e6)));
      #ifdef EARTH_SPACE_MODE
        return float4(ground, 1);
      #endif
      groundMix = saturate(groundMix);
      
      if (refraction) {
        extras *= lerp(
          pow(1 - groundMix, 2 + 4 * lerpInvSat((1 - dot(normalize(toCamera.xz), normalize(cSunDirection.xz))) * (1 + pow(groundMix, 4)), 0.000002, 0.00001)) * 0.05, 1, pow(1 - groundMix, 8));
      }
      
      groundMix = pow(groundMix, 2);
      color = lerp(color, ground, groundMix);
      // fogMult *= saturate(toCamera.y * 10 + 2);
      // pin.PosW = lerp(pin.PosW, sph * toCamera, groundMix);
      // fogMult *= smoothstep(0, 1, saturate(sph / (altitude * 10) - 1));
      // fogMult *= lerp(1 - exp(-pow(max(0, sph / (altitude * 15) - 0.35), 2)), 1, spaceLook);
      // fogMult = lerp(fogMult, 1 - saturate(altitude / 1e6 - 0.1), 1 /*TODO: ARE YOU SURE? */);
    } 
    #ifdef EARTH_SPACE_MODE
    else if (spaceLook) {
      float radius = min(200e3, altitude * 0.75);
      float sph2 = iSphere(ownPos, toCamera, float4(0, 0, 0, 6371e3 + radius));
      if (sph2 > 0) {
        float3 pos = ownPos + toCamera * dot(toCamera, -ownPos);
        float distToGround = length(pos) - 6371e3;
        float3 nm = normalize(pos);
        color = float3(0, 0.5, 1) * 0.1 * saturate(dot(nm, -ksLightDirection.rgb)) 
          * ksLightColor.rgb * pow(saturate(1 - distToGround / radius), 4) * spaceLook * (1 + pow(saturate(-dot(toCamera, ksLightDirection.rgb)), 80) * 4);
        outDepth = 1;
        return float4(color, 0);
      }
    }
    #endif
  }
  
  #ifdef EARTH_SPACE_MODE
  discard;
  return 0;
  #endif
  #endif

  color += extras;

  #ifdef COVERS_FG
    float mask = 1 - txFeaturesMask.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw, 0);
    clip(mask - 0.001);
    return float4(color * mask * withFogImpl(1, pin.PosW, toCamera, pin.Tex.x, fogMult, 1), 0);
  #else
    float4 ret = withFogImpl(color, pin.PosW, toCamera, pin.Tex.x, fogMult, 1);
    return float4(ret.rgb + dithering(pin.PosH.xy) * cDitherScale, 1);
  #endif
}
