#include "include/common.hlsl"

#define CLOUDS_V2
#ifdef CLOUDS_BATCH

  #ifdef TARGET_PS
    #include "include_new/base/_include_ps.fx"
  #else
    #include "include_new/base/_include_vs.fx"
  #endif

  // StructuredBuffer<Cloud> cloudsBuffer : register(t3);
  #ifdef TARGET_VS
    static const float3 BILLBOARD[] = {
      float3(0, 0, 0),
      float3(1, 0, 0),
      float3(0, 1, 0),
      float3(0, 1, 0),
      float3(1, 0, 0),
      float3(1, 1, 0),
    };

    float3 getPosW(Cloud cloud, uint id, out float4 posH, out float2 tex, float mult = 1){
      float2 offset = BILLBOARD[id].xy;
      float4 width = cloud.sizeWidth;
      float4 height = cloud.sizeHeight;
      float4 posW = float4(cloud.position + offset.x * width.xyz + offset.y * height.xyz, 1);
      if (mult > 1){
        posW.xyz += width.xyz * (mult - 1) * (offset.x ? 1 : -1);
        posW.xyz += height.xyz * (mult - 1) * (offset.y ? 1 : -1);
      }
      float4 posV = mul(posW, ksView);
      posH = mul(posV, ksProjection);
      tex = offset;
      if (width.w == 1) tex.x = 1.0 - tex.x;
      if (height.w == 1) tex.y = 1.0 - tex.y;
      return posW.xyz - ksCameraPosition.xyz;
    }
  #endif

  struct PS_IN_CloudFXBatch {
    float4 PosH : SV_POSITION;
    float3 PosC : TEXCOORD0;
    float Fog : TEXCOORD1;
    float2 Tex : TEXCOORD2;
    MOTION_BUFFER
    nointerpolation uint CloudIndex : INDEX0;
    // float3 ToUp : TEXCOORD3;
    // float3 ToSide : TEXCOORD4;
  };
#else
  #include "wfx_common.hlsl"
#endif

#ifdef TARGET_PS
  Texture2DArray txCloudNoise : register(t2);
  float4 sampleCloud(float3 uv, float bias = 0){
    float slices = 128;
    uv.z = frac(uv.z);
    float sliceIndex = floor(uv.z * slices);
    float sliceTransition = frac(uv.z * slices);
    #ifdef CLOUD_DETAIL_BIAS
      float4 v0 = txCloudNoise.SampleBias(samLinearSimple, float3(uv.xy, sliceIndex), bias + CLOUD_DETAIL_BIAS); 
      float4 v1 = txCloudNoise.SampleBias(samLinearSimple, float3(uv.xy, sliceIndex == (slices - 1) ? 0 : sliceIndex + 1), bias + CLOUD_DETAIL_BIAS); 
    #else
      float4 v0 = txCloudNoise.SampleBias(samLinearSimple, float3(uv.xy, sliceIndex), bias); 
      float4 v1 = txCloudNoise.SampleBias(samLinearSimple, float3(uv.xy, sliceIndex == (slices - 1) ? 0 : sliceIndex + 1), bias); 
    #endif
    #ifdef FOR_REFLECTIONS
      return v0;
    #endif
    return lerp(v0, v1, sliceTransition);
  }

  #include "include/perlinworley.hlsl"
#endif