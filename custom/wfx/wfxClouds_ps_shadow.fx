#define PS
#define USE_PS_FOG
#define USE_PS_FOG_RECOMPUTE
#define USE_BACKLIT_FOG
#define CLOUDS
#include "wfx_common.hlsl"

// #define OPTIMIZE_SKY

float3 calculateLight(float3 toCloud, float3 normal, float4 txDiffuseValue, float3 lightDir, float3 lightColor, bool includeSpecular){
  float frontlitDot = dot(normal, lightDir);
  float frontlitK = saturate(lerp(1, frontlitDot, frontlitDiffConcentration));
  float3 result = frontlitK * frontlitMult * colorOpacity.rgb;

  float backlitDot = saturate(dot(toCloud, lightDir));
  float backlitK = pow(backlitDot, abs(backlitExp) * (2 - txDiffuseValue.a));
  result += backlitK * backlitMult 
    * (1 - pow(saturate(txDiffuseValue.a), backlitOpacityExp) * backlitOpacityMult);

  if (includeSpecular){
    float specularBase = saturate(dot(normalize(lightDir - toCloud), normal)) 
      * backlitDot;
    result += pow(specularBase, specularExp) * specularPower;
  }

  return result * lightColor;
}

float4 main(PS_IN_CloudFX pin) : SV_TARGET {
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);

  float3 toCamera = normalize(pin.PosC);
  float2 txNm = txDiffuseValue.xy * 2 - 1;
  // float3 toSide = normalize(cross(-toCamera, float3(0, 1, 0)));
  // float3 toUp = normalize(cross(toCamera, toSide));
  float3 toSide = normalize(pin.ToSide);
  float3 toUp = normalize(pin.ToUp);

  float edge = saturate(length(txNm));
  float notEdge = sqrt(1 - edge * edge);
  float3 normal = -toCamera * notEdge + toUp * txNm.y + toSide * txNm.x;

  // float3 light = calculateLight(toCamera, normal, txDiffuseValue, -ksLightDirection.xyz, ksLightColor.xyz, true);
  float3 light = calculateLight(toCamera, normal, txDiffuseValue, lightDirection, lightColor, true);

  float noiseMult = edge * edge;
  noiseMult *= noiseMult;
  // noiseMult *= noiseMult;

  float alpha = (txDiffuseValue.a - cutoff) * colorOpacity.a;
  [branch]
  if (useNoise){
    alpha = saturate(alpha 
      - txVariation.Sample(samLinearSimple, pin.Tex + noiseOffset).x * cloudNoiseMult);
  } else {
    alpha = saturate(alpha);
  }

  clip(alpha - 0.1);

  uint2 pos = uint2(pin.PosH.xy);
  // pos += pin.Offset2;
  uint2 xy2 = pos.xy % 2;
  uint2 xy3 = pos.xy % 3;

  // alpha *= 0.67;
  // if (alpha < 0.75 && dot(xy2, 1) == 0) discard;
  // if (alpha < 0.5 && xy2.x == 0) discard;
  // if (alpha < 0.25 && xy2.y == 0) discard;
  
  alpha *= 0.85;
  // alpha -= 0.4;
  if (alpha < 0.9 && xy3.x == 0 && xy3.y == 0) discard;
  if (alpha < 0.8 && xy3.x == 2 && xy3.y == 1) discard;
  if (alpha < 0.7 && xy3.x == 1 && xy3.y == 2) discard;
  if (alpha < 0.6 && xy3.x == 1 && xy3.y == 0) discard;
  if (alpha < 0.5 && xy3.x == 1 && xy3.y == 1) discard;
  if (alpha < 0.4 && xy3.x == 0 && xy3.y == 2) discard;
  if (alpha < 0.3 && xy3.x == 2 && xy3.y == 0) discard;
  if (alpha < 0.2 && xy3.x == 0 && xy3.y == 1) discard;

  return 1;
}
