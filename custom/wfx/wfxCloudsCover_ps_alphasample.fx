#define USE_PS_FOG
#define USE_PS_FOG_RECOMPUTE
#define USE_BACKLIT_FOG
#define FOG_NEW_FORMULA_NO_BRANCH
#define FOG_NEW_FORMULA 1
#include "wfx_common_clouds_cover.hlsl"
#include "wfx_common.hlsl"

Texture2D txCloudsCover : register(TX_SLOT_MAT_0);
Texture2D<float> txFeaturesMask : register(TX_SLOT_MAT_1);

void main(PS_IN_CloudCover pin,
    out float4 out0 : SV_Target0
) {
  float3 toCamera = normalize(pin.PosW);
  pin.Tex = float2(-cTexScaleX * atan2(toCamera.x, toCamera.z) / (2 * M_PI) + cTexOffsetX, 1 - asin(toCamera.y) / (M_PI / 2));
  pin.Tex.x = frac(pin.Tex.x);
  pin.Tex.y = remap(pin.Tex.y, 0, cTexRemapY, 0, 1);

  float level = txCloudsCover.CalculateLevelOfDetail(samLinear, pin.Tex.y);
  float4 txValue = txCloudsCover.SampleLevel(samLinearClamp, pin.Tex, level);
  float txMaskValue = txFeaturesMask.SampleLevel(samLinearClamp, pin.Tex, 0);
  // txValue.a *= lerp(cOpacityFade, 1, smoothstep(0, 1, saturate(remap(pin.Tex.y, 1.02, 1, 0, 1))));
  txValue.a = saturate(remap(txValue.a, cOpacityCutoff, 1, 0, 1));
  txValue.a = pow(txValue.a, cOpacityExp);
  txValue.a *= cOpacityMult * txMaskValue;
  out0 = saturate(txValue.a * cShadowOpacityMult);
  // out0 = 1;
}
