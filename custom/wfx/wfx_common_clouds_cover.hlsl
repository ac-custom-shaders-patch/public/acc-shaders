#include "include/common.hlsl"

#ifndef MOTION_BUFFER
  #define MOTION_BUFFER 
#endif

cbuffer cbCloudsCover : register(b10) {
  float3 cColorMult;
  float cOpacityMult;

  float3 cColorExp;
  float cOpacityExp;

  float cOpacityCutoff;
  float cOpacityFade;
  float cTexOffsetX;
  float cTexRemapY;

  float cFogMultZenith;
  float cFogMultDelta;
  float cFogRangeInv;
  float cFogMultExp2;

  float cShadowRadius;
  float cShadowOpacityMult;
  float cMaskOpacityMult;
  float cTexScaleX;
}

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct PS_IN_CloudCover {
  float4 PosH : SV_POSITION;
  float3 PosW : TEXCOORD2;
  float2 Tex : TEXCOORD0;
  MOTION_BUFFER
  float Fog : TEXCOORD4;
};