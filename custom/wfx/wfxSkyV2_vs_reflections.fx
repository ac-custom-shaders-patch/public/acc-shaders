#include "wfx_common_sky_v2.hlsl"
#include "../../recreated/include_new/base/_include_vs.fx"

float3 getAtmosphericScattering(float3 dir){
  float3 sky0, sun0, fex0, sky1, sun1, fex1;
  gSkySun.calculate(dir, sky0, sun0, fex0);
  gSkyOpposite.calculate(dir, sky1, sun1, fex1);

  float sunSide = saturate(dot(dir, cGradientDirection) * 0.5 + 0.5);
  float3 totalSky = lerp(sky1, sky0, sunSide);
  float3 totalSun = lerp(sky1, sky0, sunSide);
  float3 totalFex = lerp(fex1, fex0, sunSide);
  applyExtraGradients(dir, totalSky);
  return totalSky * cBrightnessMult;
}

PS_IN_Sky main(VS_IN vin) {
  PS_IN_Sky vout;
  float3 posN = normalize(vin.PosL.xyz);
  float3 posC = posN * ksFarPlane * 0.99;
  float3 posW = posC + ksCameraPosition.xyz;
  float4 posV = mul(float4(posW, 1), ksView);
  vout.PosH = mul(posV, ksProjection);
  vout.PosH.z = vout.PosH.w;
  vout.PosW = posN * 30000;
  vout.NormalL = 0;
  vout.Tex = 0;
  vout.LightH = getAtmosphericScattering(posN);
  vout.Tex.x = calculateFogNew(vout.PosW - float3(0, 0.001, 0));
  vout.Tex.y = cFogMult + cFogMult2 * pow(saturate(cFogRange - posN.y * cFogRange), cFogMultExp);
  vout.PosH.z = vout.PosH.w;
  return vout;
}
