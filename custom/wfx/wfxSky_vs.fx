#define USE_PS_FOG
#define USE_PS_FOG_RECOMPUTE
#define SPS_NO_POSC
#include "../../recreated/include_new/base/_include_vs.fx"
#include "wfx_common_sky.hlsl"

PS_IN_Sky main(VS_IN vin SPS_VS_ARG) {
  PS_IN_Sky vout;
  float3 posN = applySkyVertexCorrection(vin.PosL);
  float3 posC = posN * ksFarPlane * 0.99;
  float3 posW = posC + ksCameraPosition.xyz;
  float4 posV = mul(float4(posW, 1), ksView);
  vout.PosH = mul(posV, ksProjection);
  vout.PosW = posN * 30000;
  vout.NormalL = 0;
  vout.Tex = 0;
  vout.LightH = 0;
  vout.PosH.z = vout.PosH.w;
  SPS_RET(vout);
}
