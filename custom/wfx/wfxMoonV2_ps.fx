#define PS
#include "wfx_common_sky_v2.hlsl"
#include "wfx_common.hlsl"

cbuffer cbExtMoon : register(b8) {
  float3 moonToSunDirection;
  float moonBrightness;

  float moonOpacity;
  float moonFogOpacity;
  float moonUseGradient;
  float moonClipThreshold;

  float3 moonColor;
  float __padding_moon_1;

  float3 shadowDir;
  float __padding_moon_2;
};

Texture2D<float> txFeaturesMask : register(TX_SLOT_MAT_1);

float4 main(PS_IN_Sky pin) : SV_TARGET {
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  clip(txDiffuseValue.a - moonClipThreshold);

  txDiffuseValue.a *= 1 - txFeaturesMask.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw, 0);

  // txDiffuseValue.xy = lerp(txDiffuseValue.xy, 0.5, 0.9);

  float2 txNm = txDiffuseValue.xy * 2 - 1;
  float2 offsetBase = pin.Tex * 2 - 1;
  float2 offset = clamp(offsetBase - txNm * (1 - dot(offsetBase, offsetBase)), -1, 1);
  float3 normal = float3(offset.x, -offset.y, sqrt(1 - saturate(dot(offset, offset))));
  float litUp = saturate(dot(normal, moonToSunDirection) * 3) * txDiffuseValue.z;

  float3 nm = txDiffuseValue.xyz * 2 - 1;
  nm.z = sqrt(saturate(1 - dot2(nm.xy)));
  litUp = LAMBERT(nm, moonToSunDirection);

  // float3 toCamera = normalize(pin.PosW);

  float3 toCamera = normalize(pin.PosW);
  float3 sky0, sun0, fex0, sky1, sun1, fex1;
  gSkySun.calculate(toCamera, sky0, sun0, fex0);
  gSkyOpposite.calculate(toCamera, sky1, sun1, fex1);

  float sunSideBase = saturate(dot(toCamera, cSunDirection) * 0.5 + 0.5);
  float sunSide = sunSideBase;
  float3 totalSky = lerp(sky1, sky0, sunSide);
  float3 totalSun = lerp(sky1, sky0, sunSide);
  float3 totalFex = lerp(fex1, fex0, sunSide);
  float skyEdge = getSkyEdge(toCamera);

  float3 color = moonColor * lerp(moonUseGradient, 1, txDiffuseValue.z) * moonBrightness * totalFex * skyEdge * cStarsColor;
  float3 starsBlend = lerp(luminance(color), color, cStarsSaturation);

  float eclipseUmbra = pow(lerpInvSat(dot(toCamera, shadowDir), 0.99992, 0.99989), 2);
  float eclipsePenumbra = pow(lerpInvSat(dot(toCamera, shadowDir), 0.99977, 0.99973), 2);
  starsBlend *= lerp(GAMMA_OR(0.4, 0.8), 1, eclipsePenumbra);
  starsBlend *= lerp(float3(GAMMA_OR(0.03, 0.05), 0, 0), 1, eclipseUmbra);
    
  float alpha = litUp * saturate(moonOpacity) * lerp(0.5, 2, txDiffuseValue.a);
  return withFogImpl(starsBlend, toCamera * ksFarPlane, toCamera, pin.LightH.x, moonFogOpacity, alpha);
}
