#include "include/samplers.hlsl"
#include "include/perlinworley.hlsl"

Texture2D txDiffuse : register(t0);

cbuffer cbData : register(b10) {
  float gSlice;
  float gPerlinFrequency;
  uint gPerlinOctaves;
  float gWorleyFrequency;
  float gShapeMult;
  float gShapeExp;
  float gShape0Mip;
  float gShape0Contribution;
  float gShape1Mip;
  float gShape1Contribution;
  float gShape2Mip;
  float gShape2Contribution;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float getCloud(float2 uv, float mip = 0, int2 offset = int2(0, 0)){
  // return txDiffuse.SampleLevel(samLinearSimple, uv, mip, offset).x * 1;
  float perlinWorley = txDiffuse.SampleLevel(samLinearSimple, uv, mip, offset).x;  
  float3 worley = txDiffuse.SampleLevel(samLinearSimple, uv * 2, mip, offset).yzw;
  float wfbm = worley.x * 0.625 + worley.y * 0.125 + worley.z * 0.25;   
  return remap(perlinWorley, wfbm - 1, 1, 0, 1);
}

float2 getShape(float2 uv, float mip = 0, int2 offset = int2(0, 0)){
  float cloudBlurred = getCloud(uv, mip, offset);
  float2 d = float2(ddx(cloudBlurred), ddy(cloudBlurred)) * gShapeMult;
  return pow(saturate(abs(d)), gShapeExp) * sign(d);
}

float4 main(VS_Copy pin) : SV_TARGET {
  // float perlinWorley = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0).x;  
  // float3 worley = txDiffuse.SampleLevel(samLinearSimple, pin.Tex * 2, 0).yzw;
  // float wfbm = worley.x * 0.625 + worley.y * 0.125 + worley.z * 0.25;   
  float cloud = getCloud(pin.Tex, 0);

  float2 d = 0;
  float w = 0;
  [unroll] for (int x = -2; x <= 2; x++)
  [unroll] for (int y = -2; y <= 2; y++){
    if (abs(x) + abs(y) == 4) continue;
    // d += getShape(pin.Tex, 0, int2(x, y) * 2);
    d += getShape(pin.Tex, gShape0Mip, int2(x, y)) * gShape0Contribution;
    d += getShape(pin.Tex, gShape1Mip, int2(x, y)) * gShape1Contribution;
    d += getShape(pin.Tex, gShape2Mip, int2(x, y)) * gShape2Contribution;
    w += 1;
  }
  d /= w;
  // float2 d = getShape(pin.Tex);

  float backlit = cloud * (d.y * 0.35 + 0.65);
  return float4(d * 0.5 + 0.5, backlit, cloud);
}
