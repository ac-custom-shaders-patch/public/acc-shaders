#define VS
#define SPS_NO_POSC
#include "wfx_common.hlsl"

PS_IN_Sky main(uint id: SV_VertexID SPS_VS_ARG) {
  PS_IN_Sky vout;
  vout.PosW = getPosW(id, vout.PosH, vout.Tex, 1 SPS_VS_TOSS_PASS);
  vout.NormalL = 0;
  vout.LightH = 0;
  vout.LightH.x = calculateFogNew(vout.PosW);
  SPS_RET(vout);
}
