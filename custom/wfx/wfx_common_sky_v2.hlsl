#include "include/common.hlsl"
#include "include_new/base/cbuffers_common.fx"
#define MAX_EXTRA_GRADIENTS_NUM 16

struct ExtraGradient {
  float3 color;
  uint isAdditive_expValue;

  float3 directionX; // direction * X; X = 1 / (g.sizeFrom - g.sizeTo)
  float sizeY; // Y = -g.sizeTo * X
};

struct SkyParams {
  float3 gBetaM;
  float gMieV;

  float3 gCacheA;
  uint gSunShapeMult_Saturation;

  float gMieDirectionalG;
  float gMieZenithLength;
  float gRayleigh;
  float gRayleighZenithLength;

  float gSunIntensityFactor;
  float gSunIntensityFalloffSteepness;
  float gYScale;
  float gYOffset;

  float gLuminance;
  float gBackgroundLight;
  float gGammaInv;
  float gSaturation;

  float henyeyGreensteinPhase(float cosTheta) {
    float g2 = pow(gMieDirectionalG, 2);
    return (0.25 / M_PI) * (1 - g2) / pow(saturate(1 - 2 * gMieDirectionalG * cosTheta + g2), 1.5);
  }

  float sunIntensity(float zenithAngleCos) {
    float cutoffAngle = M_PI / 1.95; // Earth shadow hack
    return gSunIntensityFactor * saturate(1 - exp(-((cutoffAngle - acos(zenithAngleCos)) / gSunIntensityFalloffSteepness)));
  }

  float3 getBetaR(float3 dir){
    float sunfade = 1 - saturate(1 - exp(dir.y));
    float rayleighCoefficient = gRayleigh - (1 - sunfade);
    return gCacheA * rayleighCoefficient;
  }

  float getDenom(float dirY){
    float zenithAngle = acos(dirY);
    return dirY + 0.15 * pow(93.885 - zenithAngle * (180.0 / M_PI), -1.253);
  }

  void calculate(float3 dir, out float3 sky, out float3 sun, out float3 fex);
};

cbuffer cbExtSky : register(b6) {
  // Generic part:
  float cSunSize;
  float3 cSunDirection;

  float cMoonSize;
  float cMoonMieExp;
  float cFogMult;
  float cBrightnessMult;

  float3 cMoonMieColor;
  float cStarsSaturation;

  float3 cStarsColor;
  float cStarsBrightness;

  float3 cMoonDirection;
  float cStarsExponent;

  float cFogMult2;
  float cFogMultExp;
  float cFogRange;
  uint cExtraGradientsCount;

  float4x4 cStarMapRotation;

  // V2 part:
  SkyParams gSkySun;
  SkyParams gSkyOpposite;

  float cRainbowBrightness;
  float cRainbowSecondaryBrightness;
  float cRainbowInBetweenDarkening;
  float cFogNewFormula;

  float3 cGradientDirection;
  float cDitherScale;

  ExtraGradient extraGradients[MAX_EXTRA_GRADIENTS_NUM]; 
}; 

float3 applySkyVertexCorrection(inout float4 posL){
  return posL.xyz;
}

void applyExtraGradient(ExtraGradient g, float3 dir, inout float3 color){
  float dotV = dot(g.directionX, dir);
  float dotT = saturate(dotV + g.sizeY);
  float smooth = pow(dotT * dotT * (3 - 2 * dotT), f16tof32(g.isAdditive_expValue >> 16));
  float3 colorSmoothed = g.color * smooth;
  // color *= 1 - smooth * (g.isAdditive_expValue & 4) + colorSmoothed * (g.isAdditive_expValue & 2);
  // color += colorSmoothed * (g.isAdditive_expValue & 1);
  if (g.isAdditive_expValue & 1){
    color += colorSmoothed;
  } else {
    color *= 1 - smooth + colorSmoothed;
  }
}

void applyExtraGradients(float3 dir, inout float3 color){
  for (uint i = 0; i < cExtraGradientsCount; i++){
    applyExtraGradient(extraGradients[i], dir, color);
  }
}

float rayleighPhase(float cosTheta) {
	return (3 / (16 * M_PI)) * (1 + pow(cosTheta, 2));
}

float getSunPoint(float dotV){
  return saturate(dotV * cSunSize - 100000);
}

float getMoonPoint(float dotV){
  return saturate(dotV * cMoonSize - 100000);
}

float getSkyEdge(float3 dir){
  return 1;
}

void SkyParams::calculate(float3 dir, out float3 sky, out float3 sun, out float3 fex){
  // Rayleigh coefficient
  float3 betaR = getBetaR(cSunDirection);
  
  // Mie coefficient
  float3 betaM = gBetaM;
  
  // Adjusted Y components of directions
  float dirY = saturate(dir.y * gYScale + gYOffset);
  float sunY = saturate(cSunDirection.y * gYScale + gYOffset);

  // Optical length, cutoff angle at 90 to avoid singularity
  float denom = getDenom(dirY);
  float sR = gRayleighZenithLength / denom;
  float sM = gMieZenithLength / denom;
  
  // Combined extinction factor
  fex = exp(-(betaR * sR + betaM * sM));
  
  // In-scattering
  float sunDotV = dot(dir, cSunDirection);
  float3 betaRTheta = betaR * rayleighPhase(sunDotV * 0.5 + 0.5);
  float3 betaMTheta = betaM * henyeyGreensteinPhase(sunDotV);
  float sunE = sunIntensity(sunY);

  float3 Lbase = (betaRTheta + betaMTheta) / (betaR + betaM);
  float3 Lin = pow(max(sunE * Lbase * (1 - fex), 0), 1.5);
  Lin *= lerp(1, pow(max(sunE * Lbase * fex, 0), 0.5), pow(1 - sunY, 5));
  
  // Composition + solar disc
  sky = Lin + gBackgroundLight * fex;
  sky *= gLuminance;

  float sat = 1 - min(sky.r, min(sky.g, sky.b)) / (max(sky.r, max(sky.g, sky.b)) + 0.01);
  sky = max(0, lerp(luminance(sky), sky, lerp(gSaturation, min(gSaturation, 1), pow(sat, 8))));
  sky = pow(max(sky, 0), gGammaInv);

  float2 sunMult_Saturation = float2(f16tof32(gSunShapeMult_Saturation), f16tof32(gSunShapeMult_Saturation >> 16));
	sun = sunE * max(0, lerp(luminance(fex), fex, sunMult_Saturation.y)) * sunMult_Saturation.x;
}

// Based on GPU Gems
// Optimised by Alan Zucconi
float3 bump3y(float3 x, float3 yOffset) {
  return saturate(1 - x * x - yOffset);
}

// Spektre
float3 spectralSpektre(float l) {
 float r = 0.0, g = 0.0, b = 0.0;
 l = lerp(400, 700, l);
 if (l >= 400. && l < 410.0) { float t = (l - 400) / 10; r = 0.33 * t - 0.2 * t * t; }
 else if (l >= 410. && l < 475.0) { float t = (l - 410) / 65; r =0.14 - 0.13 * t * t; }
 else if (l >= 545. && l < 595.0) { float t = (l - 545) / 50; r = 1.98 * t - t * t; }
 else if (l >= 595. && l < 650.0) { float t = (l - 595) / 55; r =0.98 + (0.06 * t) - 0.4 * t * t; }
 else if (l >= 650. && l < 700.0) { float t = (l - 650) / 50; r =0.65 - (0.84 * t) + 0.2 * t * t; }
 if (l >= 415. && l < 475.0) { float t = (l - 415) / 60; g = 0.8 * t * t; }
 else if (l >= 475. && l < 590.0) { float t = (l - 475) / 115; g = 0.8 + 0.76 * t - 0.8 * t * t; }
 else if (l >= 585. && l < 639.0) { float t = (l - 585) / 54; g = 0.82 - 0.8 * t; }
 if (l >= 400. && l < 475.0) { float t = (l - 400) / 75; b = 2.2 * t - 1.5 * t * t; }
 else if (l >= 475. && l < 560.0) { float t = (l - 475) / 85; b = 0.7 - t + 0.3 * t * t; }
 return float3(r,g,b);
}

#define RAINBOW_LEAKAGE_RED 1.4
#define RAINBOW_LEAKAGE_VIOLET -0.25

// 0 for 400, 1 for 700
float3 spectralZucconi(float x) {
  // return rainbow(x);
  // x = smoothstep(-0.2, 1.2, x);

  // float3 ret = spectralSpektre(x);
  // return spectralSpektre(x);

  const float3 cs = float3(3.54541723, 2.86670055, 2.29421995);
  const float3 xs = float3(0.69548916, 0.49416934, 0.28269708);
  const float3 ys = float3(0.02320775, 0.15936245, 0.53520021);
  // float3 ret = bump3y(cs * (x - xs), ys);

  const float3 c1 = float3(3.54585104, 2.93225262, 2.41593945);
  const float3 x1 = float3(0.69549072, 0.49228336, 0.27699880);
  const float3 y1 = float3(0.02312639, 0.15225084, 0.52607955);
  const float3 c2 = float3(3.90307140, 3.21182957, 3.96587128);
  const float3 x2 = float3(0.11748627, 0.86755042, 0.66077860);
  const float3 y2 = float3(0.84897130, 0.88445281, 0.73949448);
  float3 ret = bump3y(c1 * (x - x1), y1) + bump3y(c2 * (x - x2), y2);

  // return ret;

  float redFix = saturate(x * 2 - 1) * saturate(1 - pow(ret.r, 2));
  ret = lerp(ret, float3(1, 0, 0) * pow(saturate(remap(x, RAINBOW_LEAKAGE_RED, 0.85, 0, 1)), 2), redFix);

  // float violetFix = saturate(remap(x, 0.4, 0.15, 0, 1));
  float violetFix = saturate(1 - x * 2) * saturate(1 - pow(ret.g, 2));
  ret = lerp(ret, float3(0.5, 0, 1) * 0.5 * pow(saturate(remap(x, RAINBOW_LEAKAGE_VIOLET, 0.15, 0, 1)), 2), violetFix);

  return ret;
}

void processRainbow(inout float3 color, float3 toCamera) {
  /* 
  Source: 
    http://www.philiplaven.com/p20.html

  Calculated via:
    function rainbow1(ior) { let a = Math.acos(Math.sqrt((ior * ior - 1) / 3)); return (-2 * a + 4 * Math.asin(Math.sin(a) / ior)) * 180 / Math.PI; }
    function rainbow2(ior) { let a = Math.acos(Math.sqrt((ior * ior - 1) / 8)); return (2 * a - 6 * Math.asin(Math.sin(a) / ior) + Math.PI) * 180 / Math.PI; }
    [ rainbow1(1.33141), rainbow2(1.33141) ]

  Based on:
    https://plus.maths.org/content/rainbows
    https://scientificsentence.net/Optics/descartes_rainbow.php
  */
  
  [branch]
  if (cRainbowBrightness == 0) return;

  const float rainbow1_400 = cos(M_PI / 180 * 40.43337);
  const float rainbow1_700 = cos(M_PI / 180 * 42.30989);
  const float rainbow2_400 = cos(M_PI / 180 * 53.86255);
  const float rainbow2_700 = cos(M_PI / 180 * 50.47310);

  float rainbowMult = saturate(remap(toCamera.y, 0.3 - abs(cSunDirection.y), 0.75 - abs(cSunDirection.y), 1, 0));
  // float rainbowMult = saturate(remap(toCamera.y, 0.3 - abs(cSunDirection.y), 0.31 - abs(cSunDirection.y), 1, 0));
  #ifdef REFLECTIONS_MODE
    rainbowMult *= 0.5;
  #endif

  float LdotV = dot(toCamera, -cSunDirection);
  float rainbow1Value = remap(LdotV, rainbow1_400, rainbow1_700, 0, 1);
  float rainbow2Value = remap(LdotV, rainbow2_400, rainbow2_700, 0, 1);
  color *= lerp(1, lerp(rainbow1Value < RAINBOW_LEAKAGE_RED ? 1 : lerp(cRainbowInBetweenDarkening, 1, cRainbowSecondaryBrightness), 
    cRainbowInBetweenDarkening, saturate(min(rainbow1Value, rainbow2Value))), rainbowMult);

  float rainbowValue = rainbow1Value < RAINBOW_LEAKAGE_RED ? rainbow1Value : rainbow2Value;
  float rainbowIntensity = cRainbowBrightness * (rainbow1Value < RAINBOW_LEAKAGE_RED ? 1 : cRainbowSecondaryBrightness); 

  [branch] 
  if (rainbowValue > RAINBOW_LEAKAGE_VIOLET && rainbowValue < RAINBOW_LEAKAGE_RED){
    color += GAMMA_LINEAR_SIMPLE(spectralZucconi(rainbowValue)) 
      * GAMMA_ALBEDO_BOOST
      // * GAMMA_OR(max(0, 1 - abs(rainbowValue * 2 - 1) * 0.5), 1)
      * (luminance(color) * rainbowMult * rainbowIntensity);
  }
}