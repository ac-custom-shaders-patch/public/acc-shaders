#define VS
#define CLOUDS
#define CLOUDS_REFLECTIONS
#include "wfx_common.hlsl"

PS_IN_CloudFX main(uint id: SV_VertexID) {
  PS_IN_CloudFX vout;
  vout.PosC = getPosW(id, vout.PosH, vout.Tex, 1);
  vout.Fog = calculateFogNew(vout.PosC);
  vout.ToSide = cross(-vout.PosC, float3(0, 1, 0));
  vout.ToUp = cross(vout.PosC, vout.ToSide);
  return vout;
}
