#define MAX_EXTRA_GRADIENTS_NUM 32
#define M_PI 3.14159265359

struct ExtraGradient {
  float3 color;
  float expValue;

  float3 directionX; // direction * X; X = 1 / (g.sizeFrom - g.sizeTo)
  float sizeY; // Y = -g.sizeTo * X

  uint isAdditive;
  float3 padding;
};

cbuffer cbExtSky : register(b6) {
  // Generic part:
  float cSunSize;
  float3 cSunDirection;

  float cMoonSize;
  float cMoonMieExp;
  float cFogMult;
  float cBrightnessMult;

  float3 cMoonMieColor;
  float cStarsSaturation;

  float3 cStarsColor;
  float cStarsBrightness;

  float3 cMoonDirection;
  float cStarsExponent;

  float cFogMult2;
  float cFogMultExp;
  float cFogRange;
  uint cExtraGradientsCount;

  float4x4 cStarMapRotation;

  // V1 part:
  float cZenithOffset;
  float cDensity;
  float cSunMieExp;
  float cSunBrightness;

  float3 cSkyColor;
  float cZenithDensityExp;

  float3 cSunColor;
  float _pad;

  float3 cacheSunAbsorption;   // cache
  float sunPointDistMult; // cache

  float3 cacheSunAbsorption2; // cache
  float cInputYOffset;

  ExtraGradient extraGradients[MAX_EXTRA_GRADIENTS_NUM]; 
};

float3 applySkyVertexCorrection(inout float4 posL){
  return posL.xyz;
}

void applyExtraGradient(ExtraGradient g, float3 dir, inout float3 color){
  float dotV = dot(g.directionX, dir);
  float dotT = saturate(dotV + g.sizeY);
  float smooth = pow(dotT * dotT * (3.0 - 2.0 * dotT), g.expValue);
  float3 colorSmoothed = g.color * smooth;
  if (g.isAdditive){
    color += colorSmoothed;
  } else {
    color *= 1 - smooth + colorSmoothed;
  }
}

void applyExtraGradients(float3 dir, inout float3 color){
  for (uint i = 0; i < cExtraGradientsCount; i++){
    applyExtraGradient(extraGradients[i], dir, color);
  }
}

float getSkyEdge(float dirHeight){
  return saturate((dirHeight - cZenithOffset) * 100.0);
}

float getZenithDensity(float x){
  return cDensity * pow(max(x - cZenithOffset, 0.35e-2), cZenithDensityExp);
}

float3 getSkyAbsorption(float y){
  return exp2(cSkyColor * -y) * 2.0;
}

float3 getRayleigMultiplier(float dotV){
  return dotV * dotV * (M_PI * 0.5) * cSunColor;
}

float getSunPoint(float dotV){
  return saturate(dotV * cSunSize - 100000);
}

float getMoonPoint(float dotV){
  return saturate(dotV * cMoonSize - 100000);
}

float getHeight(float3 v){
  return saturate(v.y + cInputYOffset);
}