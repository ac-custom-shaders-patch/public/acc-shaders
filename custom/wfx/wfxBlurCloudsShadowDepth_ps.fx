#include "include/common.hlsl"

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#include "include/poisson.hlsl"

float main(VS_Copy pin) : SV_TARGET {
  float R = 0.02;
  float ret = 0;
  #define DISK_SIZE 32
  for (uint i = 0; i < DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(DISK_SIZE, i);
    float v = txDiffuse.SampleLevel(samLinearClamp, pin.Tex + offset * R, 0);
    ret += exp(1 - v);
  }
  return ret / DISK_SIZE;
}