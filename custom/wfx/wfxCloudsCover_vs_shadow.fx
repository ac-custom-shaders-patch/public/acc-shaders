#define USE_PS_FOG
#define USE_PS_FOG_RECOMPUTE
#define USE_ALT_CBUFFER
#include "wfx_common_clouds_cover.hlsl"
#include "wfx_common.hlsl"

PS_IN_CloudCover main(VS_IN_ac vin) {
  PS_IN_CloudCover vout;
  float3 posN = vin.PosL.xyz;
  float3 posC = posN * cShadowRadius;
  float3 posW = posC + ksCameraPosition.xyz;
  float4 posV = mul(float4(posW, 1), ksView);
  vout.PosH = mul(posV, ksProjection);
  vout.PosW = posN * 30000;
  vout.Tex = vin.Tex;
  vout.Fog = 0;
  return vout;
}
