#define PS
#include "wfx_common.hlsl"

Texture2D<float> txFeaturesMask : register(TX_SLOT_MAT_1);

float3 calculateLight(float3 toCloud, float3 normal, float3 lightDir, float3 lightColor){
  float frontlitDot = dot(normal, lightDir);
  float frontlitK = saturate(lerp(1, frontlitDot, 0.6));
  float3 result = frontlitK * 0.5;

  float specularBase = saturate(dot(normalize(lightDir - toCloud), normal));
  result += pow(specularBase, 10.0);

  return result * lightColor;
}

float4 main(PS_IN_Sky pin) : SV_TARGET {
  // return float4(4, 4, 0, 1);
  float2 offset = pin.Tex * 2 - 1;
  offset *= 1 + ringsFlag;
  float3 toCamera = normalize(pin.PosW);
  float litUp = 1;
  float3 color = colorOpacity.rgb;

  float alpha = saturate((1 - length(offset)) * 100);

  float r = dot2(offset);
  if (abs(offset.y) < 0.2) {
    float alt = dot2(offset * float2(1 / (1 + ringsFlag), 4));
    if (r > 1) alpha = (1 - abs(offset.y) / 0.2) * 0.1 * ringsFlag;
    r = min(r, alt);
  }

  alpha *= saturate(colorOpacity.a * litUp);
  alpha *= 1 - txFeaturesMask.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw, 0);
  clip(1 - r);

  // float u = sqrt(saturate(1 - dot2(offset)));
  // float3 nm = normalize(float3(offset.x, u, offset.y));
  // float3 dirSide = normalize(cross(toCamera, extDirUp));
  // float3 dirUp = normalize(cross(toCamera, dirSide));
  // nm = nm.x * dirSide + nm.y * -toCamera + nm.z * dirUp;
  // color *= saturate(dot(nm, sunDirection));
  color *= saturate(dot(-toCamera, sunDirection) + 1);
  color *= alpha; // blends in max mode

  // color = 1e3;
  // return float4(color, alpha);

  // return withFogImpl(color, toCamera * ksFarPlane, toCamera, pin.LightH.x, 1, alpha);
  return withFogImpl(color, toCamera * ksFarPlane, toCamera, pin.LightH.x, fogOpacity, alpha);
}
