#define NO_CARPAINT
#define NO_SHADOWS
#define USE_LAMBERT_GAMMA
#include "include/common.hlsl"
#include "include/samplers.hlsl"
#include "../../recreated/include_new/base/sps_utils.fx"

#ifdef USE_ALT_CBUFFER
  cbuffer cbExtAlt : register(b4) {
    float4x4 ksView;
    float4x4 ksProjection;
    float3 ksCameraPosition;
    float pad;
  }

  struct PS_IN_CloudFX {
    float4 PosH : SV_POSITION;
    float3 PosC : TEXCOORD0;
    float2 Tex : TEXCOORD1;
  };

  Texture2D txDiffuse : register(t0);
  Texture2D txVariation : register(t1);
#elif defined(TARGET_PS)
  #include "../../recreated/include_new/base/_include_ps.fx"
#else
  #include "../../recreated/include_new/base/_include_vs.fx"
#endif

#ifndef CLOUDS

cbuffer cbExtSprite : register(b5) {
  float4 colorOpacity;

  float cutoff;
  float3 position;

  // uint2 sizeWidth;
  // uint2 sizeHeight;
  float4 sizeWidth;
  float4 sizeHeight;

  uint2 sunDirection_ringsFlag;
  float fogOpacity;
};

#define sunDirection loadVector(sunDirection_ringsFlag).xyz
#define ringsFlag loadVector(sunDirection_ringsFlag).w

#else

#define cloudNoiseMult 1
#define ringsFlag 0

cbuffer cbExtCloud : register(b5) {
  float4 colorOpacity;

  float cutoff;
  float3 position;

  // uint2 sizeWidth;
  // uint2 sizeHeight;
  float4 sizeWidth;
  float4 sizeHeight;

  float3 ambientColor;
  float frontlitMult;

  float frontlitDiffConcentration;
  float backlitExp; // 20
  float backlitOpacityExp; // 2
  float backlitOpacityMult; // 0.8

  float backlitMult;
  float specularPower;
  float specularExp;
  float fogOpacity;

  float2 noiseOffset;
  #ifdef CLOUDS_V2
    float contourExp;
  #else
    uint useNoise;
  #endif
  float procSharpnessMult;

  float3 lightDirection;
  float brightnessMult;

  float3 lightColor;
  float shadowOpacity; // aka countour lighting intensity

  float2 mapProc; // 0.65, 0.75
  float2 scaleProc;

  float2 normalScaleProc; // 0.5, 0.5
  float shapeShiftingProc;
  float ambientConcentration;

  float2 texStart;
  float2 texSize;

  float extraFidelity;
  float arcMultiplier; // 0.3
  float distance;
  float shadowsOpacity;

  float alphaSmoothstep;
  float facingNormalExp; // 0.5
  float normalYExp;
  float topFogBoost;
};

#endif

float3 getPosW(uint id, out float4 posH, out float2 tex, float mult /* = 1 */ SPS_VS_TOSS_ARG){
  float2 offset = float2(id & 1, id >> 1);
  float4 width = loadVector(sizeWidth);
  float4 height = loadVector(sizeHeight);
  if (ringsFlag) {
    mult *= 1 + ringsFlag;
  }
  float4 posW = float4(position + offset.x * width.xyz + offset.y * height.xyz, 1);
  if (mult > 1){
    posW.xyz += width.xyz * (mult - 1) * (offset.x ? 1 : -1);
    posW.xyz += height.xyz * (mult - 1) * (offset.y ? 1 : -1);
  }
  float3 posC = posW.xyz - ksCameraPosition.xyz;
  #ifdef CLOUDS_REFLECTIONS
    float4 posV = mul(float4(normalize(posC) * 100 + ksCameraPosition.xyz, 1), ksView);
  #else
    float4 posV = mul(float4(posW.xyz, 1), ksView);
  #endif
  posH = mul(posV, ksProjection);
  tex = offset;
  if (width.w == 1) tex.x = 1.0 - tex.x;
  if (height.w == 1) tex.y = 1.0 - tex.y;
  return posC;
}