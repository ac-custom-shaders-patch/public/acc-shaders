#define PS
#define CLOUDS
#include "wfx_common.hlsl"

float3 calculateLight(float3 toCloud, float3 normal, float4 txDiffuseValue, float3 lightDir, float3 lightColor, bool includeSpecular){
  float frontlitDot = dot(normal, lightDir);
  float frontlitK = saturate(lerp(1, frontlitDot, frontlitDiffConcentration));
  float3 result = frontlitK * frontlitMult * colorOpacity.rgb;

  float backlitDot = saturate(dot(toCloud, lightDir));
  float backlitK = pow(backlitDot, abs(backlitExp));
  result += backlitK * backlitMult;

  return result * lightColor;
}

float4 main(PS_IN_CloudFX pin) : SV_TARGET {
  float4 txDiffuseValue = txDiffuse.SampleBias(samLinear, pin.Tex, 2);

  float3 toCamera = normalize(pin.PosC);
  float2 txNm = txDiffuseValue.xy * 2 - 1;
  float3 toSide = normalize(pin.ToSide);
  float3 toUp = normalize(pin.ToUp);

  float notEdge = sqrt(saturate(1 - dot(txNm, txNm)));
  float3 normal = -toCamera * notEdge + toUp * txNm.y + toSide * txNm.x;
  // float3 light = calculateLight(toCamera, normal, txDiffuseValue, -ksLightDirection.xyz, ksLightColor.xyz, true)
  //   /*+ calculateLight(toCamera, normal, txDiffuseValue, extMoonDirection, extMoonColor, false)*/;
  // normal.y *= -1;

  // TODO: FIX USING WATER TO TEST THINGS
  float3 light = calculateLight(toCamera, normal, txDiffuseValue, lightDirection, lightColor/5, true);
  float alpha = (txDiffuseValue.a - cutoff) * colorOpacity.a;

  clip(alpha - 0.0001);
  return withFogImpl((ambientColor + txDiffuseValue.z * colorOpacity.rgb + light) * brightnessMult, 
    toCamera * ksFarPlane, toCamera, pin.Fog, fogOpacity, alpha);
}
