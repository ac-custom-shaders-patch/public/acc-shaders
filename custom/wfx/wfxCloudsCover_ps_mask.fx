#define USE_PS_FOG
#define USE_PS_FOG_RECOMPUTE
#define USE_BACKLIT_FOG
#define FOG_NEW_FORMULA_NO_BRANCH
#define FOG_NEW_FORMULA 1
#include "wfx_common_clouds_cover.hlsl"
#include "../../recreated/include_new/base/_include_ps.fx"

Texture2D txCloudsCover : register(TX_SLOT_MAT_0);

float4 main(PS_IN_CloudCover pin) : SV_TARGET {
  float3 toCamera = normalize(pin.PosW);
  pin.Tex = float2(-cTexScaleX * atan2(toCamera.x, toCamera.z) / (2 * M_PI) + cTexOffsetX, 1 - asin(toCamera.y) / (M_PI / 2));
  pin.Tex.x = frac(pin.Tex.x);
  pin.Tex.y = remap(pin.Tex.y, 0, cTexRemapY, 0, 1);

  float level = txCloudsCover.CalculateLevelOfDetail(samLinear, pin.Tex.y);
  return cMaskOpacityMult * txCloudsCover.SampleLevel(samLinearClamp, pin.Tex, level).x;
}
