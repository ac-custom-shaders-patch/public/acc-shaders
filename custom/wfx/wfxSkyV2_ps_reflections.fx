#define REFLECTIONS_MODE
#include "wfx_common_sky_v2.hlsl"
#include "../../recreated/include_new/base/_include_ps.fx"

float4 main(PS_IN_Sky pin) : SV_TARGET {
  float3 dir = normalize(pin.PosW);
  processRainbow(pin.LightH, dir);

  float sunDotV = dot(dir, cSunDirection);
  float sunShape = getSunPoint(sunDotV);
  // if (sunDotV > 0.9999) {
  if (sunShape) {
    float sunSideBase = saturate(dot(dir, cGradientDirection) * 0.5 + 0.5);
    float sunSide = sunSideBase;
    
    float3 sky0, sun0, fex0, sky1, sun1, fex1;
    gSkySun.calculate(dir, sky0, sun0, fex0);
    gSkyOpposite.calculate(dir, sky1, sun1, fex1);
    float3 totalSun = lerp(sun1, sun0, sunSide);
  
    return float4(totalSun, 1);
    return 0;
  }

  return withFogImpl(pin.LightH, pin.PosW, dir, pin.Tex.x, pin.Tex.y, 1);
}
