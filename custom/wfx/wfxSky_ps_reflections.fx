#include "../../recreated/include_new/base/_include_ps.fx"
#include "wfx_common_sky.hlsl"

float4 main(PS_IN_Sky pin) : SV_TARGET {
  // return 0;
  return withFogImpl(pin.LightH, pin.PosW, normalize(pin.PosW), pin.Tex.x, pin.Tex.y, 1);
}
