#define USE_PS_FOG
#define USE_PS_FOG_RECOMPUTE
#define USE_BACKLIT_FOG
#define CLOUDS

#ifndef COVER_SUN
  #define FOG_NEW_FORMULA (!cFogMultExp)
  cbuffer cbExtSky : register(b6) {
    float cFogMult;
    float cFogMult2;
    float cFogMultExp;
    float cFogRange;
  }
#endif

#include "wfx_common_clouds_v2.hlsl"

float3 calculateLight(float3 toCloud, float3 normal, float edgeTilt,
    float alpha, float3 lightDir, float3 lightColor, float shadow, bool includeSpecular){
  float frontlitDot = dot(normal, lightDir);
  float frontlitK = saturate(lerp(1, frontlitDot, frontlitDiffConcentration * lerp(0.4, 1, alpha)));
  float3 result = frontlitK * frontlitMult;

  float alpha2 = alpha * alpha;
  float backlitDot = saturate(dot(toCloud, lightDir));
  float backlitK = pow(backlitDot, abs(backlitExp) * (2 - alpha2 * 1.5));
  result += backlitK * backlitMult 
    * saturate(pow(saturate(1 - alpha), backlitOpacityExp) + 1 - backlitOpacityMult);

  float contourK = pow(saturate(edgeTilt), contourExp * alpha) * saturate(-normal.y);
  result += contourK * ambientColor * shadowOpacity;

  if (includeSpecular){
    float specularBase = saturate(dot(normalize(lightDir - toCloud), normal)) 
      * backlitDot;
    result += pow(specularBase, specularExp) * specularPower;
  }

  return result * lightColor * shadow;
}

#define ENCODE_CLOUD_SHADOW (pin.PosH.z / pin.PosH.w)

Texture2D<float> txCloudShadowDepth : register(TX_SLOT_MAT_12);

float getCloudOnCloudShadow(float3 posC){
  #if defined(COVER_SUN) || defined(FOR_REFLECTIONS)
    return 1;
  #else
    float4 shadowMapUV = mul(float4(posC, 1), extCloudShadowMatrix);
    float cloudDepth = 1 - (shadowMapUV.z / shadowMapUV.w + 0.01);
    float occluder = txCloudShadowDepth.SampleLevel(samLinearClamp, shadowMapUV.xy, 0);
    float shadowBase = saturate(occluder * exp(-cloudDepth) * 6 - 4.8);
    return GAMMA_LINEAR_SIMPLE(lerp(
      saturate(1 - txCloudShadow.SampleLevel(samLinearClamp, shadowMapUV.xy, 0) * shadowsOpacity),
      1, shadowBase));
  #endif
}

void main(PS_IN_CloudFX pin
  #ifdef BUILD_SHADOWMAP
    , out float out0 : SV_Target0
    , out float out1 : SV_Target1
  #else
    , out float4 out0 : SV_Target0
  #endif
) {
  #ifdef FOR_REFLECTIONS
    // out0 = 1;
    // return;
  #endif

  // if (pin.Tex.y < 0.005 || pin.Tex.y > 0.995
  //     || pin.Tex.x < 0.005 || pin.Tex.x > 0.995){
  //   out0 = float4(3, 0, 0, 1);
  //   return;
  // }

  bool isHorizontal = backlitExp < 0;
  float3 toCamera = normalize(pin.PosC);
  #ifndef USE_ALT_CBUFFER
    float3 toSide = normalize(pin.ToSide);
    float3 toUp = normalize(pin.ToUp);
    float3 toForward = -toCamera;
  #endif

  float2 uv = noiseOffset;
  if (!isHorizontal) {
    pin.Tex.y += pow(abs(pin.Tex.x * 2 - 1), 2) * arcMultiplier * abs(toCamera.y) * (1 - pow(abs(pin.Tex.y * 2 - 1), 2));
    float2 angleV = normalize(position.xz - ksCameraPosition.xz);
    float angle = atan2(angleV.y, angleV.x) / (2 * M_PI);
    uv.x -= 3 * angle / scaleProc.x;
  } else {
    #ifndef USE_ALT_CBUFFER
      toSide = -normalize(sizeWidth.xyz);
      toUp = -normalize(sizeHeight.xyz);
      toForward = normalize(cross(toSide, toUp));
    #endif
  }

  if (sizeWidth.w) uv.x *= -1;
  uv += pin.Tex - 0.5;

  // float2 maskUv = pin.Tex;
  // maskUv.y = pow(maskUv.y, 0.7);

  float4 txMaskValue = txDiffuse.Sample(samLinear, texStart + texSize * pin.Tex);
  txMaskValue.xy = txMaskValue.xy * 2 - 1;
  // if (pin.Tex.y > 0.5 + pow(abs(pin.Tex.x - 0.5), 2)){
  //   txMaskValue.y = -1;
  // }
  float sharpness = saturate(txMaskValue.z * procSharpnessMult);
  float bias = max(-extraFidelity, 0);
  
  float4 txCloudValue = sampleCloud(float3(uv * scaleProc, shapeShiftingProc), bias); 
  txCloudValue.xy = txCloudValue.xy * 2 - 1;

  float4 txDetailValue = sampleCloud(float3(uv * scaleProc * 5.3, 0.5 + shapeShiftingProc), bias); 
  txDetailValue.xy = txDetailValue.xy * 2 - 1;

  #ifdef EXTRA_FIDELITY
    [branch]
    if (extraFidelity > 0) {
      float detailMult = extraFidelity * saturate(txMaskValue.a * 2) * saturate(2 - txMaskValue.a * 2);
      txCloudValue.xy = lerp(txCloudValue.xy, txDetailValue.xy, detailMult);
      txCloudValue.zw = saturate(txCloudValue.zw * lerp(1, 1.4 * txDetailValue.zw, detailMult));
    }
  #endif
  
  float4 txCombinedValue = txCloudValue * txMaskValue;
  float th0 = lerp(1, mapProc.x - sharpness * 0.1, txMaskValue.a * (1 - cutoff));
  float th1 = lerp(1.05, lerp(mapProc.y, mapProc.x, sharpness) - sharpness * 0.1, txMaskValue.a * (1 - cutoff));
  txCombinedValue.a = saturate(remap(txCloudValue.a, th0, th1, 0, 1));

  float2 txNm = txCloudValue.xy * normalScaleProc.x + txMaskValue.xy * normalScaleProc.y;
  txNm.y = pow(saturate(txNm.y * 0.5 + 0.5), normalYExp) * 2 - 1;

  float edgeTilt = length(txNm);
  float notEdge = pow(saturate(1 - edgeTilt * edgeTilt), facingNormalExp);
  if (sizeWidth.w) txNm.x *= -1;

  #ifndef USE_ALT_CBUFFER
    float3 normal = toForward * notEdge + toUp * txNm.y + toSide * txNm.x;
    if (normalYExp > 1){
      normal -= toForward * saturate(saturate(txCombinedValue.y * 1.4 - 0.1) * saturate(-txNm.y * 2) * (normalYExp - 1) * 5);
    }
    normal = normalize(normal);
  #endif

  float MASK = txDiffuse.Sample(samLinear, pin.Tex).a;
  float CLOUD = saturate(txCombinedValue.a) * colorOpacity.a;
  CLOUD = saturate(lerp(CLOUD, smoothstep(0, 1, CLOUD), alphaSmoothstep));
  clip(CLOUD - 0.001);
  
  #ifndef USE_ALT_CBUFFER
    float cloudShadow = 1;

    [branch]
    if (shadowsOpacity){
      float3 posW = toCamera * distance;
      cloudShadow = getCloudOnCloudShadow(posW);
    }

    float3 light = calculateLight(toCamera, normal, edgeTilt, CLOUD, lightDirection, lightColor, cloudShadow, true);
    float3 finalColor = (ambientColor * saturate(lerp(1, normal.y, ambientConcentration)) 
      + colorOpacity.rgb * saturate(-normal.y)
      + light) * brightnessMult;

    // finalColor = cloudShadow;
  #else
    float3 finalColor = 0;
  #endif

  // finalColor = float3(txNm + 1, 0);
  // finalColor = normal + 1;
  // finalColor = float3(pin.Tex.y, 1 - pin.Tex.y, 0);

  #ifdef COVER_SUN
    finalColor = 0;
    #ifdef USE_SHADOW_OPACITY
      CLOUD = saturate(sqrt(CLOUD) * shadowOpacity);
    #else
      CLOUD = saturate(CLOUD * 3 - 1);
    #endif

    #ifdef BUILD_SHADOWMAP
      out0 = CLOUD;
      out1 = ENCODE_CLOUD_SHADOW;
    #elif defined(RETURN_ALPHA_AS_COLOR)
      out0 = float4(CLOUD.xxx, 1);
    #else
      out0 = float4(0, 0, 0, CLOUD);
    #endif
  #else
    #ifndef FOR_REFLECTIONS
      float backlitDot = saturate(dot(toCamera, lightDirection));
      float backlitK = saturate(pow(backlitDot, CLOUD * 800 + 80) * 1.1 - 0.05);
      backlitK = smoothstep(0, 1, backlitK) * 0.15;
      finalColor += lightColor * backlitK * lerp(0.5, 0, CLOUD);
      CLOUD = saturate(CLOUD * (1 + CLOUD * backlitK));
    #endif

    float fogMult = fogOpacity;
    if (!FOG_NEW_FORMULA || GAMMA_FIX_ACTIVE){
      fogMult *= cFogMult + cFogMult2 * pow(saturate(cFogRange - toCamera.y * cFogRange), cFogMultExp);
      pin.Fog = 1;
    }

    fogMult += lerp(0, topFogBoost, 1 - pin.Tex.y);
    out0 = withFogImpl(finalColor, 
      toCamera * ksFarPlane, toCamera, pin.Fog, fogMult, CLOUD);
  #endif
}
