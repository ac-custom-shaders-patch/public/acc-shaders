// based on https://www.shadertoy.com/view/Ml2cWG by robobo1221

#include "../../recreated/include_new/base/_include_vs.fx"
#include "wfx_common_sky.hlsl"

float3 getAtmosphericScattering(float3 dir){
  float dirHeight = getHeight(dir);
  float zenithDensity = getZenithDensity(dirHeight);
  float3 absorption = getSkyAbsorption(zenithDensity);
  float dotV = saturate(dot(dir, cSunDirection));
  float3 rayleighMult = 1.0 + getRayleigMultiplier(dotV);
  float3 sky = cSkyColor * zenithDensity * rayleighMult;  
  float3 totalSky = lerp(sky * absorption, sky / (sky + 0.5), sunPointDistMult);
  totalSky *= cacheSunAbsorption2;
  applyExtraGradients(dir, totalSky);
  return max(totalSky * cBrightnessMult, 0);
}

PS_IN_Sky main(VS_IN vin) {
  PS_IN_Sky vout;
  float3 posN = normalize(vin.PosL.xyz);
  float3 posC = posN * ksFarPlane * 0.99;
  // float3 posC = posN * 1e12f;
  float3 posW = posC + ksCameraPosition.xyz;
  float4 posV = mul(float4(posW, 1), ksView);
  vout.PosH = mul(posV, ksProjection);
  vout.PosH.z = vout.PosH.w;
  vout.PosW = posN * 30000;
  vout.NormalL = 0;
  vout.Tex = 0;
  vout.LightH = getAtmosphericScattering(posN);
  vout.Tex.x = calculateFogNew(vout.PosW - float3(0, 0.001, 0));
  vout.Tex.y = cFogMult + cFogMult2 * pow(saturate(cFogRange - posN.y * cFogRange), cFogMultExp);
  vout.PosH.z = vout.PosH.w;
  return vout;
}
