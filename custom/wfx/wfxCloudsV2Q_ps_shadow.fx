#define COVER_SUN
#define USE_SHADOW_OPACITY
#define USE_ALT_CBUFFER
#define CLOUD_DETAIL_BIAS 2
#define EXTRA_FIDELITY
#define BUILD_SHADOWMAP
#include "wfxCloudsV2_ps.fx"
