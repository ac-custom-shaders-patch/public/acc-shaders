#define USE_ALT_CBUFFER
// #define PS
#define CLOUDS
#include "wfx_common.hlsl"

float4 main(PS_IN_CloudFX pin) : SV_TARGET {
  float4 txDiffuseValue = txDiffuse.SampleLevel(samLinear, pin.Tex, 0);
  // return 1;
  // return (float4)saturate(txDiffuseValue.a * 1.2);

  float alpha = (txDiffuseValue.a - cutoff) * colorOpacity.a;
  [branch]
  if (useNoise){
    alpha = saturate(alpha 
      - txVariation.SampleLevel(samLinearSimple, pin.Tex + noiseOffset, 0).x * cloudNoiseMult);
  } else {
    alpha = saturate(alpha);
  }

  // return alpha;
  // return float4(1, 1, 1, 1);
  return float4((float3)saturate(alpha * shadowOpacity), 1);
}
