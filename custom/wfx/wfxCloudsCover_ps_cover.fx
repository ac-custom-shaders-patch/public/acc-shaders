#define USE_PS_FOG
#define USE_PS_FOG_RECOMPUTE
#define USE_BACKLIT_FOG
#include "../../recreated/include_new/base/_include_ps.fx"
#include "wfx_common_clouds_cover.hlsl"

Texture2D<float> txFeaturesMask : register(TX_SLOT_MAT_1);

float main(PS_IN_CloudCover pin) : SV_TARGET {
  float mask = txFeaturesMask.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw, 0);
  clip(mask - 0.5);
  return 1;
}
