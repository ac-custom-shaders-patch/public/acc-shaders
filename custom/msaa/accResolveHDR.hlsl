#include "include/common.hlsl"

#define GAMMA_FIX_CONDITION true
#include "include_new/base/_gamma.fx"

cbuffer cbData : register(b10) {
  float gWeightBaseK;
}

Texture2DMS<float4> txDiffuse : register(t0); 

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#define WEIGHT_BASE_K GAMMA_OR(gWeightBaseK, 1.)

#if CUSTOM_KERNEL == 1

  #if SAMPLE_COUNT == 8
    static const float2 SubSampleOffsets[8] = {
        float2( 0.0625f, -0.1875f),
        float2(-0.0625f,  0.1875f),
        float2( 0.3125f,  0.0625f),
        float2(-0.1875f, -0.3125f),
        float2(-0.3125f,  0.3125f),
        float2(-0.4375f, -0.0625f),
        float2( 0.1875f,  0.4375f),
        float2( 0.4375f, -0.4375f),
    };
  #elif SAMPLE_COUNT == 4
    static const float2 SubSampleOffsets[4] = {
        float2(-0.125f, -0.375f),
        float2( 0.375f, -0.125f),
        float2(-0.375f,  0.125f),
        float2( 0.125f,  0.375f),
    };
  #elif SAMPLE_COUNT == 2
    static const float2 SubSampleOffsets[2] = {
        float2( 0.25f,  0.25f),
        float2(-0.25f, -0.25f),
    };
  #endif

  float filterSmoothstep(float x){ return 1 - smoothstep(0, 1, x); }

  float4 main(VS_Copy pin) : SV_TARGET {
    float4 resValue = 0;
    
    const float gResolveFilterDiameter = 2;
    const int gSampleRadius = (int)((gResolveFilterDiameter / 2.0f) + 0.499f);

    float4 sum = 0;
    const bool InverseLuminanceFiltering = true;
    for (int y = -gSampleRadius; y <= gSampleRadius; ++y) {
      for (int x = -gSampleRadius; x <= gSampleRadius; ++x) {
        float2 samplePos = pin.PosH.xy + float2(x, y);
        [unroll]
        for(uint subSampleIdx = 0; subSampleIdx < SAMPLE_COUNT; ++subSampleIdx) {
          float2 sampleDist = abs(float2(x, y) + SubSampleOffsets[subSampleIdx].xy) / (gResolveFilterDiameter / 2.0f);
          bool useSample = all(sampleDist <= 1);
          if (useSample) {
            float3 color = max(0, txDiffuse.Load(uint2(samplePos), subSampleIdx).xyz);
            float weight = filterSmoothstep(sampleDist.x) * filterSmoothstep(sampleDist.y);
            sum += float4(color, 1) * weight / (WEIGHT_BASE_K + luminance(color));
          }
        }
      }
    }
    return float4(max(0, sum.rgb / sum.a), 1);
  }

#else

  float3 tonemap(float3 x){ return x / (x + WEIGHT_BASE_K); }
  float3 inverseTonemap(float3 x){ return WEIGHT_BASE_K * x / max(1 - x, 0.00001); }
  // float3 tonemap(float3 x){ return x; }
  // float3 inverseTonemap(float3 x){ return x; }
  float4 tonemap(float4 x){ return float4(tonemap(x.rgb), x.a); }
  float4 inverseTonemap(float4 x){ return float4(inverseTonemap(x.rgb), x.a); }

  float4 main(VS_Copy pin) : SV_TARGET {
    float4 resValue = 0;
    [unroll]
    for (uint i = 0; i < SAMPLE_COUNT; ++i){
      resValue += tonemap(max(0, txDiffuse.Load(pin.PosH.xy, i)));
    }
    return inverseTonemap(resValue / SAMPLE_COUNT);
  }

#endif