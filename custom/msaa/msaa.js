Object.entries({
  msaa2: 2,
  msaa4: 4,
  msaa8: 8
}).map(([msaaKey, msaaValue]) => Object.entries({
  _custom: 1,
  ['']: 0
}).map(([typePostfix, typeValue]) => ({
  source: `accResolveHDR.hlsl`,
  target: FX.Target.PS,
  saveAs: `accResolveHDR_${msaaKey}${typePostfix}_ps.fxo`,
  defines: {
    'SAMPLE_COUNT': msaaValue,
    'CUSTOM_KERNEL': typeValue
  }
}))).flat(Infinity)