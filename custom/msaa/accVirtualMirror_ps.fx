#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float4 gColorMult;
}

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  // return txDiffuse.SampleLevel(samPoint, pin.Tex, 0);
  // return pow(txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0) * gColorMult, 0.4545);// + dithering(pin.PosH.xy) * 0;
  return txDiffuse.Sample(samLinearClamp, pin.Tex) * gColorMult;
}