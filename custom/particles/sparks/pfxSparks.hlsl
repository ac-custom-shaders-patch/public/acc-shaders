#define PARTICLES_PACKING
#define PARTICLES_WITHOUT_NORMALS 
#include "particles/common/include_decl.hlsl"

#define FLAG_NO_CAR_CLIPPING 1

void __pack(out uint2 value, float3 vec, uint w){ __pack(value, float4(vec, 0)); value.y |= w; }
void __unpk(uint2 value, out float3 vec, out uint w){ float4 R; __unpk(value, R); vec = R.xyz; w = value.y & 0xffff; }

struct Particle {
	float3 pos;
	float life;

	float3 velocity;
	float randomValue;

	float size;
	uint carIndex;
	float brightness;
	float lifePassedOutside;

	float4 collisionPlane1;
	float4 collisionPlane2;

	// float2 lastScreenPos;
	// float2 currentScreenPos;
	float3 carVelocity;
	float motionMult;

	float3 _pad4;
	uint color;

	float lightChance;
	float lightActive;
	float spawnDensity;
	float pad2;

	float3 randomMotion;
	uint flags;
};

struct Particle_packed {
	float3 pos;
	float life;

	float3 velocity;
	float randomValue;

	float size;
	uint carIndex;
	float brightness;
	float lifePassedOutside;

	uint2 collisionPlane1xyz_pad2;
	uint2 collisionPlane2xyz_lightActive;

	float collisionPlane1w;
	float collisionPlane2w;
	float spawnDensity;
	float lightChance;

	uint2 carVelocity_motionMult;
	uint2 randomMotion_flags;

	float3 _pad4;
	uint color;
};

#define PARTICLES_OFFSET_ORIGIN(P, OFFSET) \
	P.pos.xyz += OFFSET;\
	P.collisionPlane1.w += dot(P.collisionPlane1.xyz, OFFSET);\
	P.collisionPlane2.w += dot(P.collisionPlane2.xyz, OFFSET);

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.pos, _U.pos);\
	FN(_P.life, _U.life);\
	FN(_P.velocity, _U.velocity);\
	FN(_P.randomValue, _U.randomValue);\
	FN(_P.size, _U.size);\
	FN(_P.carIndex, _U.carIndex);\
	FN(_P.brightness, _U.brightness);\
	FN(_P.lifePassedOutside, _U.lifePassedOutside);\
	FN(_P.carVelocity_motionMult, _U.carVelocity, _U.motionMult);\
	FN(_P.randomMotion_flags, _U.randomMotion, _U.flags);\
	FN(_P._pad4, _U._pad4);\
	FN(_P.color, _U.color);\
	FN(_P.spawnDensity, _U.spawnDensity);\
	FN(_P.lightChance, _U.lightChance);\
	FN(_P.collisionPlane1w, _U.collisionPlane1.w);\
	FN(_P.collisionPlane2w, _U.collisionPlane2.w);\
	FN(_P.collisionPlane1xyz_pad2, _U.collisionPlane1.xyz, _U.pad2);\
	FN(_P.collisionPlane2xyz_lightActive, _U.collisionPlane2.xyz, _U.lightActive);

struct CarFloor {
	float3 normal;
	float distance;

	float3 center;
	float speed;

	float3 vec_h;
	float wheelsForce;

	float3 vec_v;
	float upwardsForce; // 500 for fast moving opewnwheelers

	float3 velocity;
	float _lightStrength;

	float4 wheels[4];
};

#if defined(TARGET_CS)
	StructuredBuffer<CarFloor> carFloors : register(TX_SLOT_NOISE);

	struct EmittingSpot {
		uint emitCount;
		float3 emitterPosBase;

		float3 emitterPosOffset0;
		float emitterSpeed;

		float3 emitterPosOffset1;
		uint carIndex;

		float3 emitterDirection0;
		float xParticleLifeSpan;

		float emitterSpreadMult2;
		float pad0;
		uint emitterFlags;
		float xParticleLifeSpanRandomness;

		float4 collisionPlane1;
		float4 collisionPlane2;

		uint color;
		float emitterSpeedMin;
		float emitterSpreadMult;
		float carVelocityMult;
	};

	#define FORCE_FIELDS 16

	struct ForceField {
		float3 pos;
		float radius_sqr;
		float3 force;
		float push_away;
	};

	struct CarTransform {
    float4x4 toUV;
    float4x4 toWorld;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		uint2 _xPad_;
		float xRatio;
		float xGravity;

		float2 extWindVel;
		float extWindSpeed;
		float extWindWave;

		float xZFarP;
		float xZNearP;
		float xFOV;
		uint gCarCollision;

    CarTransform gCars[2];
		ForceField xForceFields[FORCE_FIELDS];
	};

	Texture2D<float4> txCar0 : register(t2);
	Texture2D<float4> txCar1 : register(t3);
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		PFX_PS_IN
		float Ratio : TEXCOORD4;
		#ifndef MODE_SHADOW
			float Brightness : TEXCOORD2;
			float GlowExtra : MISC0;
			uint Color : TEXCOORD3;
		#endif
	};

	cbuffer cbSparks : register(CBUFFER_DRAW_SLOT) {
		float brightnessBase;
		float brightnessExtra;
		float sizeMultiplier;
		float velocityMultiplier;
		
		float fadingBase;
		float fadingLifeLeftK;
		float motionBlurExtra;
		float gCarCollisions;

		float4x4 gCarCollisionTransform;
	}
#endif

#include "particles/common/include_impl.hlsl"
