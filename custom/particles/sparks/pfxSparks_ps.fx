
// #define GBUFFER_NORMALS_REFLECTION
// #define CREATE_MOTION_BUFFER
// #define USE_ALPHATEST

#ifdef MODE_LIGHTS
  #define MIPMAPPED_GBUFFER 3
  #define MIPMAPPED_GBUFFER_FIXED
  #include "general/accSS.hlsl"
#endif

#include "pfxSparks.hlsl"

Texture2D<float> txCarCollision : register(t1);

float checkCarCollision(float3 pos){
  float4 uv = mul(float4(pos, 1), gCarCollisionTransform);
  float v = txCarCollision.SampleLevel(samLinear, uv.xy, 0);
  // if (v == 1) return 1;
  // if (uv.y > 0.98) return 1;
  return !gCarCollisions || any(abs(uv.xyz - 0.5) > float3(0.5, 0.5, 0.48)) ? 1 : saturate((txCarCollision.SampleLevel(samLinear, uv.xy, 0) - uv.z) * 50);
}

PS_OUT main(PS_IN pin) {
	#if !defined(MODE_SHADOW) && !defined(MODE_LIGHTS) && !defined(MODE_GBUFFER)
    float interiorMult = pin.Ratio > 0 ? checkCarCollision(pin.PosC + ksCameraPosition.xyz) : 1;
    if (interiorMult == 0){ clip(-1); return (PS_OUT)0; }
  #endif

  pin.Ratio = abs(pin.Ratio);
  float full = lerp(pin.Ratio, 1, pow(saturate((0.5 - 0.5 * pin.Tex.y) * 1.1), 10));
	#ifdef MODE_SHADOW
    float3 lighting = 0;
  #else
    float3 toCamera = normalize(pin.PosC);
    float4 inputColor = unpackColor(pin.Color);
    // float3 lighting = inputColor.a * pow(inputColor.rgb, lerp(1.6, 1, pin.Ratio)) * pin.Brightness * 1.6;
    float3 lighting = inputColor.a * GAMMA_LINEAR(inputColor.rgb) * (pin.Brightness * GAMMA_OR(2, 1));
    // lighting *= 0.3;
    #ifndef MODE_LIGHTS
      lighting *= 4;
      // lighting *= 1.4 * (pin.GlowExtra + 0.4 * lerp(0, 1 - pin.Ratio, pow(saturate((0.5 - 0.5 * pin.Tex.y) * 1.3), 4)));
    #endif
  #endif

  #ifdef MODE_LIGHTS
    lighting *= gLightsSceneMult * GAMMA_SCREENLIGHT_MULT;
     
    float2 ssUV = pin.PosH.xy * gSize.zw;
    float depth = ssGetDepth(ssUV);
    float3 origin = ssGetPos(ssUV, depth);
    float3 normal = ssGetNormal(ssUV);
    float3 toLight = origin - pin.PosC;
    float alpha = pow(saturate(1 - length(toLight) / pin.GlowExtra), 4) * sqrt(saturate(-dot(toLight, normal)));

    // alpha = 1;

    // float alpha = 1;

    // lighting = 1;
    // alpha = saturate(1 - length(toLight));
    // alpha = frac(origin.y);
    // alpha = frac(depth * 100);
  #else
    pin.Tex.y = 1 - 2 * pow(saturate(-pin.Tex.y * 0.5 + 0.5), lerp(1 + abs(pin.Tex.x) * 2, 1, pin.Ratio));
    float alpha = saturate((1 - dot2(pin.Tex)) * 10);
  #endif

  #ifdef MODE_GBUFFER
    PS_OUT ret = (PS_OUT)0;
    if (extFrameTime != 0){
      ret.stencil = 0.055;
    } else {
      ret.stencil = 0.5;
    }
    return ret;
  #else
    clip(alpha - 0.005);

    #ifdef OBJECT_SHADER
      wtf dude
    #endif
    
    if (GAMMA_FIX_ACTIVE) {
      lighting = pow(max(lighting, 0), 1.5) * 6 * getEmissiveMult();
      #ifdef MODE_LIGHTS
        alpha = pow(alpha, 2) * 0.2;
      #endif
    } else {
      #ifndef MODE_LIGHTS
        lighting *= 5;
      #endif
    }

    // lighting = GAMMA_OR(GAMMA_LINEAR(lighting) * 0.8, lighting);

    // alpha = saturate(alpha);
    // lighting *= alpha / 0.001;
    // alpha = 0.001;

    RETURN_BASE(lighting, alpha);
  #endif
}