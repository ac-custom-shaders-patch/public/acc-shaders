#define PASS_ALIVE_COUNT
#include "pfxSparks.hlsl"

// #ifndef DEPTH_COLLISIONS
// #define de_VP xVP
// #endif

float rand(float2 co){
  return frac(sin(dot(co.xy, float2(12.9898, 78.233))) * 43758.5453);
}

void carCollisionCheck(inout Particle particle, CarTransform car, Texture2D tx, float dt){
  float4 uv = mul(float4(particle.pos, 1), car.toUV);
	bool upperHalfOnly = true;
  if (upperHalfOnly ? uv.z > 0.5 : uv.z > 0.9 || any(abs(uv.xyz - 0.5) > 0.5)) return;
  float4 map = tx.SampleLevel(samLinearClamp, uv.xy, 0);
	if (map.w < uv.z){
		// particle.velocity.y = 1;
    float3 dirNm = mul(map.xyz, (float3x3)car.toWorld);
		if (dot(particle.velocity, dirNm) < 0){
			particle.pos -= particle.velocity * dt;
      particle.velocity = reflect(particle.velocity, dirNm) * 0.7;
			particle.motionMult = 0;
			// particle.life += max(0, length(particle.velocity) / 5 - 0.1);
		}
	}
}

#define MAX_WIND_SPEED 20
#define WIND_COORDS_MULT 1
float2 windOffset(float3 pos, float windPower, float freqMult){
  #ifdef NO_WIND
    return 0;
  #else
    float waveOffset = saturate(0.1 + extWindSpeed / MAX_WIND_SPEED);
    float wave = sin(extWindWave * 1.773 * freqMult + pos.z * freqMult * WIND_COORDS_MULT / 17) 
      * sin(extWindWave * freqMult + pos.x * freqMult * WIND_COORDS_MULT / 13);
    return extWindVel * windPower * (1 + (wave + waveOffset) * 0.5);
  #endif
}

void process(inout Particle particle, uint3 DTid, uint aliveCount) {
	// float4 gSize = float4(1920, 1080, 1.0/1920.0, 1.0/1080.0);

	// float pixelRadius = mul(float4(particle.pos, 1), de_VP).w * max(gSize.z, gSize.w) * 10;
	// float radius = max(length(vin.NormalL), pixelRadius);
	// fade = length(vin.NormalL) / radius;
	// vin.PosL.xyz = vin.PosL.xyz - vin.NormalL + normalize(vin.NormalL) * radius;
	// float4 pos2D0 = mul(float4(particle.pos, 1), de_VP);
	// float4 pos2D1 = mul(float4(particle.pos + 0.01, 1), de_VP);
	// pos2D0.xyz /= pos2D0.w;
	// pos2D1.xyz /= pos2D1.w;
	// float len = length(pos2D1.xy / pos2D1.z - pos2D0.xy / pos2D0.z) * 1000;

	// particle.life -= 0.001;
	// return;

	float toCamera = length(particle.pos - gCameraPosition);
	particle.size = (1 + particle.brightness * 0.2) * clamp(0.001 + toCamera / 1000, 0.005, 0.015);
	particle.carVelocity *= xRatio;
	particle.lightActive = particle.lightChance < min(0.8, 800 / (float)aliveCount);

	if (gFrameTime) {
		// float3 force = float3(0, -10, 0);
		float3 force = float3(0, xGravity, 0) + particle.randomMotion;
		// float3 forceRandom = force * 0.5 * (particle.randomValue + saturate(-particle.randomMotion.y * 4)) 
		// 	+ particle.randomMotion;
		// force = lerp(forceRandom, force, saturate(particle.life - 1));

		for (uint i = 0; i < FORCE_FIELDS; i++) {
			float3 dist = particle.pos - xForceFields[i].pos;
			force += (xForceFields[i].force + normalize(dist) * xForceFields[i].push_away) 
				* saturate(1 - dot(dist, dist) * xForceFields[i].radius_sqr) * 3; // TODO: move to cbuffer
		}

		// integrate:
		particle.pos += particle.velocity * gFrameTime;

		// checking for car floor
		if (particle.carIndex) {
			CarFloor carFloor = carFloors[particle.carIndex];
			float3 fl_d = particle.pos - carFloor.center;
			float fl_t = dot(fl_d, carFloor.normal);
			float sz_m = 0.95 + particle.randomValue * 0.1;
			float fl_h = abs(dot(fl_d, carFloor.vec_h));
			float fl_v = abs(dot(fl_d, carFloor.vec_v));

			for (uint i = 0; i < 4; i++) {
				float3 dist = particle.pos - carFloor.wheels[i].xyz;
				dist -= carFloor.normal * dot(dist, carFloor.normal);
				float len = length(dist);
				float3 dir = dist / len;
				float K = len * carFloor.wheels[i].w;
				if (K < 0.6) particle.life = -1;
				force += dir * carFloor.wheelsForce * saturate(1 - K);
			}

			if (fl_h > sz_m || fl_v > 1) {
				float boost = pow(frac(particle.randomValue * 524.668), 4);
				particle.velocity *= lerp(1, 0.2, boost);
				particle.velocity += float3(0, 1, 0) * lerp(0.2, 1, boost) 
					* carFloor.upwardsForce * 0.04; // TODO: move to cbuffer
				particle.carIndex = 0;
			} else if (fl_t > 0) {
				particle.pos -= carFloor.normal * distanceToPlane(carFloor.normal, carFloor.distance, particle.pos);
			}
		}

		float planeDistance1 = distanceToPlane(particle.collisionPlane1, particle.pos);
		float randomMotionLimit = saturate(planeDistance1 * 2 - 0.2);

		if (!particle.carIndex) {
			particle.velocity = lerp(float3(windOffset(particle.pos, randomMotionLimit, 2.5), 0).xzy, particle.velocity, xRatio);
			particle.velocity += force * (gFrameTime * lerp(0.8, 1.2, frac(particle.randomValue * 114.671)));
			particle.lifePassedOutside += gFrameTime;
			
			if (gCarCollision & 1) carCollisionCheck(particle, gCars[0], txCar0, gFrameTime);
			if (gCarCollision & 2) carCollisionCheck(particle, gCars[1], txCar1, gFrameTime);
		}

		particle.motionMult = saturate(particle.motionMult + gFrameTime * 30);

		// if (particle.lifePassedOutside > 0.05 && gCarCollision && carCollisionCheck(particle.pos, true)){
		// 	particle.velocity = float3(-particle.velocity.x, abs(particle.velocity.y), -particle.velocity.z);
		// 	particle.pad2 = 1;
		// } else {
		// 	particle.pad2 = 0;
		// }

		bool collided = false;
		float bounceV = frac(particle.randomValue * 387.278) * 0.005 + 0.001;
		if (planeDistance1 < 0) {
			particle.velocity = reflect(particle.velocity, particle.collisionPlane1.xyz) * 0.96;
			particle.pos += particle.collisionPlane1.xyz * (-planeDistance1 + bounceV);
			particle.motionMult = 0;
			collided = true;
		}

		float planeDistance2 = distanceToPlane(particle.collisionPlane2, particle.pos);		
		if (!collided && planeDistance2 < 0) {
			particle.velocity = reflect(particle.velocity, particle.collisionPlane2.xyz) * 0.96;
			particle.pos += particle.collisionPlane2.xyz * (-planeDistance2 + bounceV);
			particle.motionMult = 0;
			collided = true;
		}

		float3 randomVal = frac((frac(particle.pos + particle.velocity) + particle.life) * 882.2119);
		particle.randomMotion += (randomVal - 0.5) * gFrameTime;
		particle.randomMotion = clamp(particle.randomMotion, -randomMotionLimit, randomMotionLimit);

		#ifdef DEPTH_COLLISIONS
			float3 posC = particle.pos - de_CameraPosition;
			float depthCalc = length(posC);
			float2 ssUV = ssGetUV(posC).xy;

			[branch]
			if (!collided && particle.lifePassedOutside > 0.2 && depthCalc < 350 && ssUV.x > 0 && ssUV.x < 1 && ssUV.y > 0 && ssUV.y < 1) {
				float3 p0 = ssGetPos(ssUV, txDepth.SampleLevel(samPointClamp, ssUV, 0));
				float3 surfaceNormal = ssNormalDecode(txNormals.SampleLevel(samPointClamp, ssUV, 0).xyz);
				float distanceDepth = dot(posC - p0, surfaceNormal);
				bool isNotFar = distanceDepth > -0.1 - lerp(0, toCamera / 200, saturate(surfaceNormal.y * 2 - 0.5));

				[branch]
				if (surfaceNormal.y > 0.35 && distanceDepth < 0.02 && isNotFar) {
					particle.velocity -= surfaceNormal * dot(particle.velocity, surfaceNormal) * 1.2;
					particle.pos += surfaceNormal * (-distanceDepth + 0.1);
					particle.motionMult = 0;
					collided = true;
				}
			}
  	#endif

		if (collided) particle.life = min(particle.life, 0.5 + frac(particle.randomValue * 953.748));
		particle.life -= gFrameTime * (collided ? 10 : 1);
	}
}
