#include "pfxSparks.hlsl"

void emit(inout Particle particle, EmittingSpot E, RAND_DECL) {	
	particle.pos = E.emitterPosBase;
	{
		float offset = 1 - pow(RAND, 0.35);
		float angle = RAND * M_TAU;
		particle.pos += E.emitterPosOffset0 * sin(angle) * offset;
		particle.pos += E.emitterPosOffset1 * cos(angle) * offset;
	}

	particle.color = packColor(saturate(unpackColor(E.color) + float4((float3(RAND, RAND, RAND) - 0.5) * 0.1, 0)));
	particle.randomValue = RAND;
	particle.lightChance = RAND;
	particle.randomMotion = 0;
	particle.spawnDensity = 0;
	particle.flags = 0;

	particle.velocity = E.emitterDirection0 * lerp(E.emitterSpeedMin, E.emitterSpeed, RAND);
	{
		float offset = (1 - pow(RAND, 0.35)) * min(E.emitterSpeed, 20) * E.emitterSpreadMult;
		float angle = RAND * M_TAU;
		particle.velocity += normalize(E.emitterPosOffset0) * sin(angle) * offset;
		particle.velocity += normalize(cross(E.emitterPosOffset0, E.emitterDirection0)) * cos(angle) * offset * E.emitterSpreadMult2;
	}

	// particle.velocity += normalize(E.emitterPosOffset0) * (RAND - 0.5) * lerp(E.emitterSpeedMin, E.emitterSpeed, RAND) * RAND * E.emitterSpreadMult;
	// particle.velocity += normalize(cross(E.emitterPosOffset0, E.emitterDirection0)) * (RAND - 0.5) * lerp(E.emitterSpeedMin, E.emitterSpeed, RAND) * RAND * E.emitterSpreadMult * 0.1;

	float power = pow(RAND, 4);
	particle.life = E.xParticleLifeSpan + power * E.xParticleLifeSpanRandomness * (0.5 + pow(RAND, 10) + 10 * pow(RAND, 20));
	// particle.life = 4;
	particle.brightness = power;
	particle.size = 1;
	particle.collisionPlane1 = E.collisionPlane1;
	particle.collisionPlane2 = E.collisionPlane2;

	// particle.pos += E.collisionPlane2.xyz * -0.2;

	if (E.carIndex && E.carIndex < 256) {
		particle.carIndex = E.carIndex;
		particle.carVelocity = carFloors[particle.carIndex].velocity;

		if (E.carVelocityMult > 0.5) {
			particle.velocity += carFloors[particle.carIndex].velocity * E.carVelocityMult;
			particle.carIndex = 0;
			particle.lightChance = 1;
		}
	} else {
		particle.carIndex = 0;
		particle.carVelocity = 0;

		if (E.carIndex == 256 /* tyre blown */ || E.carIndex == 257 /* worn tyre or spikes */){
			particle.velocity += E.emitterPosOffset0;
			particle.pos = E.emitterPosBase 
				+ E.emitterPosOffset1 * (E.carIndex == 256 ? (RAND > 0.5 ? 1 : -1) : lerp(-1, 1, RAND)) 
				+ (float3(RAND, RAND, RAND) - 0.5) * 0.01 
				- particle.velocity * RAND * gFrameTime;
		}
	}

	particle.flags = E.emitterFlags;
}
