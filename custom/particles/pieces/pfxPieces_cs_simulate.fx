// #define PARTICLES_CUSTOM_MAIN
#include "pfxPieces.hlsl"
#include "include/samplers.hlsl"

#define ADD_PARTICLE_SPIN(_P) (_P.spin = frac(_P.pos.y * 948.722) * 20 - 10)

void forceFields(float3 pos, inout float3 velocity) {
  for (uint i = 0; i < FORCE_FIELDS; i++) {
    float3 diff = pos - gForceFields[i].pos;
    float dist = dot(diff, diff);
    float distK = saturate(1 - dist * gForceFields[i].radiusSqrInv);
    float3 forcePushAway = normalize(diff) * gForceFields[i].forceMult * 180;
    float3 forceVelSync = (gForceFields[i].velocity - velocity) * abs(gForceFields[i].velocity) * 0.5;
    velocity += distK * distK * (forcePushAway + forceVelSync) * gFrameTime;
  }
}

float sqr(float v){
  return v * v;
}

bool carCollisionProcess(int contactStage, inout Particle particle, CarCollisionData car, Texture2D tx, int index, float bounce, float sticky, inout float3 normalW){  
  if (contactStage == index){
    if (!(gPiecesFlags & (1 << (index - 1)))){
      SET_CONTACT_STAGE(particle, 0);
      return true;
    }
    particle.carContactPos = (particle.carContactPos * 2 - 1) * car.aabbMult.xzy * 0.5 + 0.5;
    float3 newPosXY = particle.carContactPos + particle.velocity * gFrameTime;
    float4 map = tx.SampleLevel(samLinearClamp, newPosXY.xy, 0);
    if (map.w > newPosXY.z + 0.01 || map.w > 0.95 || map.z > -0.5) {
      SET_CONTACT_STAGE(particle, 0);
      ADD_PARTICLE_SPIN(particle);
      float3 newPos = mul(float4(newPosXY, 1), car.toWorld).xyz;
      particle.velocity = (newPos - particle.pos) / gFrameTime;
      particle.pos = newPos;
    } else {
      float dz = map.w - newPosXY.z;
      particle.carContactPos = float3(newPosXY.xy, map.w);
      particle.pos = mul(float4(particle.carContactPos, 1), car.toWorld).xyz;
	    particle.dirSide_contact = (particle.dirSide_contact & ~0xffff) | normalEncode_u(mul(float3(0, 0, 1), (float3x3)car.toWorld));

      normalW = mul(map.xyz, (float3x3)car.toWorld);
      float gravityForce = length(normalW.xz);

      float3 finalForce = (gravityForce * float3(map.xy, 0) * (-gGravity * GRAVITY_MULT(particle)) + car.force.xzy) * lerp(4, 8, frac(particle.halfSize * 612.557));
      if (dot2(finalForce) > sticky * car.sticky || dot2(particle.velocity) > 0.01) {
        particle.velocity += finalForce;

        float speed = length(particle.velocity);
        particle.velocity.z = dz / gFrameTime;
        particle.velocity = normalize(particle.velocity) * speed;
      }

      if (index == 1 && (gPiecesFlags & 16) != 0){
        float4 wipersUV = mul(float4(particle.pos, 1), gWipersT0);
        float4 wiperTex = txWipersMask0.SampleLevel(samLinearBorder1, wipersUV.xy, 0);
        bool interiorMode = gPiecesFlags & 64;
        if (wiperTex.r && (interiorMode || wiperTex.r < 0.01)) {
          if (interiorMode) {
            particle.velocity.xy += -(wiperTex.xy * 2 - 1);
          } else {
            float wiperTexX0 = txWipersMask0.SampleLevel(samLinearBorder1, wipersUV.xy - float2(0.1, 0), 0).r;
            float wiperTexX1 = txWipersMask0.SampleLevel(samLinearBorder1, wipersUV.xy + float2(0.1, 0), 0).r;
            float wiperTexY0 = txWipersMask0.SampleLevel(samLinearBorder1, wipersUV.xy - float2(0, 0.1), 0).r;
            float wiperTexY1 = txWipersMask0.SampleLevel(samLinearBorder1, wipersUV.xy + float2(0, 0.1), 0).r;
            particle.velocity.xy += sign(float2(wiperTexX0 - wiperTexX1, wiperTexY0 - wiperTexY1)) * float2(0.8, -0.4);
          }
        }
      }
    }
    return true;
  } else {
    float4 uv = mul(float4(particle.pos, 1), car.toUV);
    if (any(abs(uv.xyz - 0.5) > 0.5) || uv.z > 0.95) {
      return false;
    } else {
      float4 map = tx.SampleLevel(samLinearClamp, uv.xy, 0);
      if (uv.z < map.w) {
        return false;
      } else {
        if (index == 1){
          for (int i = 0; i < 4; ++i){
            float3 dpos = particle.pos - gWheels[i].xyz;
            float distSqr = dot2(dpos);
            if (distSqr < sqr(gWheels[i].w + 0.2)){
              if (distSqr < sqr(gWheels[i].w)){
                particle.pos = gWheels[i].xyz + normalize(dpos) * (gWheels[i].w + particle.halfSize);
                particle.velocity *= -0.1;
              }
              return false;
            }
          }
        }

        if (uv.z > map.w + 0.1) {
          particle.life = 0;
          return false;
        } else if (particle.velocity.y > 0 || map.z > -0.5){
          normalW = mul(map.xyz, (float3x3)car.toWorld);
          float depth = abs(uv.z - map.w) * car.aabbY;
          particle.velocity *= 0.5;
          particle.velocity.y += gGravity * GRAVITY_MULT(particle);
          particle.velocity += normalize(normalW) * min(depth, 0.05) * gFrameTime * 0.1;
          float relativeSpeed = dot(particle.velocity, normalW);
          if (relativeSpeed < 0) {         
            if (relativeSpeed > -2){
              particle.velocity -= normalW * relativeSpeed;
            } else {
              particle.velocity = reflect(particle.velocity, normalW);
            }
          }
          particle.pos += normalW * lerp(min(depth, 0.05), depth, abs(normalW.y));
          ADD_PARTICLE_SPIN(particle);
          return true;
        } else {
          particle.carContactPos = float3(uv.xy, map.w);
          SET_CONTACT_STAGE(particle, index);       
          particle.velocity = (mul(float4(particle.pos + particle.velocity, 1), car.toUV).xyz - uv.xyz) * float3(1, 1, -0.5 * frac(particle.halfSize * 627.889));
          if (abs(particle.velocity.z) < 1) particle.velocity.z = 0;
          particle.pos = mul(float4(particle.carContactPos, 1), car.toWorld).xyz;
          return true;
        }
      }
    }
  }
}

void collisionProcess(int contactStage, inout Particle particle, inout float3 normalW){
  float bounce = (IS_SHARD(particle) ? 0.25 : 0.05) * (1 + frac(particle.halfSize * 943.981));
  float sticky = pow(gFrameTime, 2) * (IS_GRASS(particle) ? 6 : IS_SHARD(particle) ? 1 : 2);

  [branch]
  if ((gPiecesFlags & 1) != 0){
    if (carCollisionProcess(contactStage, particle, gCars[0], txCar0, 1, bounce, sticky, normalW)) {
      return;
    }
  }

  [branch]
  if ((gPiecesFlags & 2) != 0){
    if (carCollisionProcess(contactStage, particle, gCars[1], txCar1, 2, bounce, sticky, normalW)) {
      return;
    }
  }

  {
    forceFields(particle.pos, particle.velocity);

    bool goingSlow = dot2(particle.velocity) < 1;
    float bounceK = goingSlow ? 1 : 1 + bounce;

    float4 areaUV = mul(float4(particle.pos, 1), gAreaHeightmapTransform);
    if (all(abs(areaUV.xyz - 0.5) < 0.5)) {
      float depth = txAreaDepth.SampleLevel(samLinearBorder1, areaUV.xy, 0);
      if (depth && depth < areaUV.z + 0.000001){
        float contactDepth = (areaUV.z - depth) * 300;
        float dx0 = txAreaDepth.SampleLevel(samLinearClamp, areaUV.xy + float2(0.005, 0), 0);
        float dy0 = txAreaDepth.SampleLevel(samLinearClamp, areaUV.xy + float2(0, 0.005), 0);
        float dx1 = txAreaDepth.SampleLevel(samLinearClamp, areaUV.xy - float2(0.005, 0), 0);
        float dy1 = txAreaDepth.SampleLevel(samLinearClamp, areaUV.xy - float2(0, 0.005), 0);
        normalW = normalize(float3(dx1 - dx0, 2 * 100 * 0.01 / 300., dy1 - dy0));
        particle.velocity -= normalW * dot(particle.velocity, normalW) * bounceK;
        if (contactDepth < 0.1 || normalW.y > 0.9) particle.pos.y += contactDepth;
        else particle.pos.xz += normalW.xz * 0.01;
        particle.collisionPlane1 = float4(normalW, dot(normalW, particle.pos));
        SET_CONTACT_STAGE(particle, -2);
      } else {
        particle.velocity.y += gGravity * GRAVITY_MULT(particle);
        SET_CONTACT_STAGE(particle, 0);
      }
      // SET_CONTACT_STAGE(particle, depth && depth < areaUV.z + 0.001 ? -1 : 0);
    } else {
      float groundHit = collisionCheck(particle.pos, particle.velocity, particle.collisionPlane1, 0, bounceK, 1);
      // particle.pos -= particle.collisionPlane1.xyz * groundHit;
      if (groundHit > 0.01) {
        particle.velocity.y += gGravity * GRAVITY_MULT(particle);
        SET_CONTACT_STAGE(particle, 0);
      } else {
        SET_CONTACT_STAGE(particle, -1);
        normalW = particle.collisionPlane1.xyz;
      }
    }
  }
}

void process(inout Particle particle, uint3 DTid) {
  if (gFrameTime == 0) return;

  //  particle.life -= gFrameTime;
  //  return;

  int contactStage = GET_CONTACT_STAGE(particle);
  float drag = 1 / (1 + gFrameTime * lerp(2.5 + 5 * frac(particle.halfSize * 589.623), 1.5, PIECE_WEIGHT_NORMALIZED(particle)) * (contactStage ? 8 : 1));
  particle.life -= gFadeBoost * (1 + frac(particle.halfSize * 227.211)) + gFrameTime
    * (contactStage > 0 ? 0.02 : contactStage < 0 ? 0.2 : 0.1)
    * (particle.life < 0.025 || particle.life > 0.975 ? 1 : DTid.x > gALS2 ? 100 : DTid.x > gALS1 ? 5 : 0);
  // if (particle.color == 1) {
  //   particle.pos += particle.velocity * gFrameTime;
  //   // particle.halfSize = 0.1;
  //   return;
  // }
  particle.velocity *= drag;
  particle.spin *= contactStage ? drag : lerp(drag, 1, 0.9);
  if (contactStage <= 0){
    particle.pos += particle.velocity * gFrameTime;
  }

  float3 normalW = float3(0, 0.01, 0);
  collisionProcess(contactStage, particle, normalW);

  particle.angle += gFrameTime * particle.spin;    
  particle.landNormal = lerp(normalW, particle.landNormal, 0.8);
}

// [numthreads(THREADCOUNT_SIMULATION, 1, 1)]
// void main(uint3 DTid : SV_DispatchThreadID, uint Gid : SV_GroupIndex) {
//   uint aliveCount = counterBuffer.Load(PARTICLECOUNTER_OFFSET_ALIVECOUNT);
//   if (DTid.x >= aliveCount) return;
//   uint particleIndex = aliveBuffer_CURRENT[DTid.x];
//   Particle particle = PFX_unpack(particleBuffer[particleIndex]);
//   PARTICLES_OFFSET_ORIGIN(particle, gOriginOffset);
//   process(particle, DTid);
//   if (particle.PARTICLES_FIELD_LIFE > 0) {
//     particleBuffer[particleIndex] = PFX_pack(particle);
//     uint newAliveIndex;
//     counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_ALIVECOUNT_AFTERSIMULATION, 1, newAliveIndex);
//     aliveBuffer_NEW[newAliveIndex] = particleIndex;
//   } else {
//     uint deadIndex;
//     counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_DEADCOUNT, 1, deadIndex);
//     deadBuffer[deadIndex] = particleIndex;
//   }
// }
