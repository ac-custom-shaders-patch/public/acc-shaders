#include "pfxPieces.hlsl"

Texture2D txColor1 : register(t4);
Texture2D txColor2 : register(t5);
Texture2D txColor3 : register(t6);
Texture2D txColor4 : register(t7);

ByteAddressBuffer meshVertices : register(t8);
ByteAddressBuffer meshIndices : register(t9);
// globallycoherent RWByteAddressBuffer collectedGrassColors : register(u5);
Texture2D collectedGrassColorsPrev : register(t10);
RWTexture2D<float4> collectedGrassColors : register(u5);

bool alignWithMesh(inout float3 pos, inout float3 velocity, EmittingSpot E, ByteAddressBuffer vertices, ByteAddressBuffer indices, RAND_DECL) {
	MeshPoint M = randomMeshPoint(vertices, indices, E.meshIndexCount, E.transform, RAND_ARGS);
	if (E.type == SRCTYPE_MESH_TEST) {
		pos = M.pos;
		velocity = lerp(M.normal, float3(0, 1, 0), E.emitterVel1.y) * E.emitterVel1.x;
		return true;
	}
	if (dot2(M.pos - pos) < E.meshThresholdSqr && dot(M.normal, normalize(velocity)) > 0) {
		pos = M.pos;
		velocity = normalize(M.normal + normalize(velocity) * 2) * max(length(velocity), 1);
		return true;
	}
	return false;
}

void alignWithMesh(inout float3 pos, inout float3 velocity, EmittingSpot E, RAND_DECL) {
	[branch]
	if (E.meshID == 1) {
		alignWithMesh(pos, velocity, E, meshVertices, meshIndices, RAND_ARGS)
		|| alignWithMesh(pos, velocity, E, meshVertices, meshIndices, RAND_ARGS);
	}
}

void syncColor(inout float3 color, EmittingSpot E, RAND_DECL) {
	float2 uv = float2(0.25 + RAND * 0.5, 0.25 + RAND * 0.5);
	if (E.textureID == 1) {
		color = txColor1.SampleLevel(samLinearClamp, uv, 4).rgb; 
	} else if (E.textureID == 2) {
		color = txColor2.SampleLevel(samLinearClamp, uv, 4).rgb; 
	} else if (E.textureID == 3) {
		color = txColor3.SampleLevel(samLinearClamp, uv, 4).rgb; 
	} else if (E.textureID == 4) {
		color = txColor4.SampleLevel(samLinearClamp, uv, 4).rgb; 
	} else {
		color = 0.04;
	}
	color = clamp(color, 0, 0.4);
	if (RAND > 0.7 || dot(color, 1) == 0) {
		color = 0.04;
	}
	color += float3(RAND, RAND, RAND) * 0.03;
	color += RAND * 0.1;
}

float4 shufflePlane(float4 plane, float4 randVec, float3 turnPoint) {
	const float t = distanceToPlane(plane, turnPoint);
	float3 p = turnPoint - t * plane.xyz;
	float3 d = normalize(normalize(plane.xyz) + randVec.xyz * 0.005 / (1 + t));
	return float4(d, dot(d, p) + randVec.w * 0.01);
}

#include "include/track_area_heightmap.hlsl"

float3 getDirtColor(float3 posG, float randomValue) {
	float4 color = TA_getSurfaceColor(posG);
	return lerp(lerp(unpackColor(gDustColorA).rgb, unpackColor(gDustColorB).rgb, randomValue) * 0.3, color.rgb, color.w * 0.8);
}

float3 getSoilColor(float3 posG, float3 fallbackColor) {
	float4 color = TA_getSurfaceColor(posG);
	return lerp(fallbackColor * 0.3, color.rgb, color.w * 0.6);
}

void emit(inout Particle particle, EmittingSpot E, RAND_DECL) {	
	float3 emitterPos = E.emitterPos1;
	float3 emitterVel = E.emitterVel1;

	if (E.emitterExtraPoints >= 1 && RAND > 0.5) {
		emitterPos = E.emitterPos2;
		emitterVel = E.emitterVel2;
	}

	if (E.emitterExtraPoints >= 2 && RAND > 0.667) {
		emitterPos = E.emitterPos3;
		emitterVel = E.emitterVel3;
	}

	particle.pos = emitterPos;
	particle.velocity = emitterVel * (0.5 + RAND * 0.25) 
		+ normalize((float3(RAND, RAND, RAND) - 0.5)) * RAND * length(emitterVel) * E.randomMult;

	float3 color = float3(0.03, 0.03, 0.03);
	uint type = TYPE_CHUNK;
	float sizeMult = 1;

	if (E.type == SRCTYPE_BLOWN_TYRE) {
		color = float3(0.02, 0.02, 0.02) * (1 + RAND * 0.5);
		particle.halfSize = 0.01 + pow(RAND, 4) * 0.03;
		// particle.color = piece_packColor(float3(1, 0, 0), TYPE_CHUNK);
		particle.spin = RAND * 20 - 10;

		float angleSin, angleCos;
		sincos(RAND * M_PI * 2, angleSin, angleCos);
		particle.velocity = (E.emitterPos2 * angleSin + E.emitterPos3 * angleCos) * (4 + E.typeGrassChance * 0.1);
		particle.pos += E.emitterPos2 * angleSin;
		particle.pos += E.emitterPos3 * angleCos;
		particle.pos += normalize(cross(E.emitterPos2, E.emitterPos3)) * E.posSpread * (RAND * 2 - 1);

		// particle.angle = RAND * 3.14;
		// particle.life = 1;
		// particle.collisionPlane1 = E.emitterPlane1;
		// particle.halfSize *= sizeMult;
		// particle.dirSide_contact = normalEncode_u(float3(0, 0, 1));
		// SET_CONTACT_STAGE(particle, 0);
	} else {
		float3 grassColor = float3(0.2, 0.6, 0.4);
		bool dustPiece = false;
		bool noGrass = gMapPointA.x == gMapPointB.x;
		if (E.type == SRCTYPE_OUTSIDE) {
			float2 mapUV = (emitterPos.xz - gMapPointB) / (gMapPointA - gMapPointB);
			float4 grass = noGrass 
				? float4(E.grassFallbackColor / 6 + float3(RAND, RAND, RAND) / 35, E.isSurfaceGrassFallback) 
				: txGrass.SampleLevel(samLinearClamp, mapUV, 0);
			if (grass.a > 0.7) {
				type = GEN_OUTSIDE_ADJ(E.typeGrassChance * grass.a);
				grassColor = grass.rgb / grass.a;
			} else {
				type = GEN_OUTSIDE_NOGRASS;
			}
			dustPiece = true;
		} else if (E.type == SRCTYPE_COLLISION_CAR) {
			type = TYPE_SHARD;
		} else if (E.type == SRCTYPE_COLLISION_TRACK) {
			type = TYPE_CHUNK;
		} else if (E.type == SRCTYPE_MESH_TEST) {
			type = TYPE_SHARD;
		} else if (E.type == SRCTYPE_LAWNMOWER) {
			float2 mapUV = (emitterPos.xz - gMapPointB) / (gMapPointA - gMapPointB);
			float4 grass = noGrass 
				? float4(E.grassFallbackColor / 6 + float3(RAND, RAND, RAND) / 35, E.isSurfaceGrassFallback) 
				: txGrass.SampleLevel(samLinearClamp, mapUV, 0);
			if (grass.a > 0.7) {
				type = TYPE_GRASS;
				grassColor = grass.rgb / grass.a;
			} else {
				return;
			}
			sizeMult = 0.8;
			if (RAND > 0.95) {
				uint sampleID = uint(RAND * COLLECTED_GRASS_SAMPLES_SIZE);
				collectedGrassColors[uint2(sampleID, E.meshIndexCount)] = float4(grassColor.rgb, 1);
			}
		} else if (E.type == SRCTYPE_LAWNMOWER_LEAK) {
			type = TYPE_GRASS;
			uint sampleID = uint(RAND * COLLECTED_GRASS_SAMPLES_SIZE);
			float4 sampleColor = collectedGrassColorsPrev[uint2(sampleID, E.meshIndexCount)];
			if (sampleColor.a < 0.9) return;
			grassColor = sampleColor.rgb;
			sizeMult = 0.6;
		} 

		float3 posSpread = (float3(RAND, RAND, RAND) - 0.5) * E.posSpread;
		// if (E.emitterPlane2.w > -1e8) {
		// 	particle.pos += posSpread - E.emitterPlane2.xyz * dot(posSpread, E.emitterPlane2.xyz);
		// }

		particle.color = type;
		if (E.soilColor.x >= 5) {
			color = 0.98 * 0.4;
			particle.halfSize = (0.006 + pow(RAND, 4) * 0.016) * (E.soilColor.x == 6 ? 4 : 1);
			particle.spin = RAND * 80 - 40;
		} else if (IS_CHUNK(particle)) {
			color = dustPiece 
				? getDirtColor(emitterPos, RAND)
				: (0.03 + 0.01 * float3(RAND, RAND, RAND)) * (E.type == SRCTYPE_OUTSIDE ? 1 : 1.4);
			particle.halfSize = (0.006 + pow(RAND, 4) * 0.016) * 1.6;
			particle.spin = RAND * 80 - 40;

			if (E.type == SRCTYPE_COLLISION_TRACK) {
				particle.halfSize *= 1 + RAND * 10;
				color = 0.3 + 0.03 * float3(RAND, RAND, RAND);
			}
		} else if (IS_GRASS(particle)) {
			particle.halfSize = 0.015 + pow(RAND, 4) * 0.035;
			color = grassColor;
			particle.velocity *= 0.8;
			particle.spin = RAND * 20 - 10;
			if (RAND > 0.95){
				color = color.ggb * float3(0.8, 0.7, 1);
			} else if (RAND > 0.9){
				color = color.ggb * float3(1, 0.9, 0.6);
			} else {
				color += (float3(RAND, RAND, RAND) - 0.5) * 0.005;
				color += (RAND - 0.5) * 0.01;
			}
		} else if (IS_SOIL(particle)) {
			particle.halfSize = 0.015 + pow(RAND, 4) * 0.035;
			color = getSoilColor(emitterPos, E.soilColor);
			particle.spin = RAND * 4 - 2;
		} else /* if (IS_SHARD(particle)) */ {
			particle.halfSize = 0.01 + pow(RAND, 4) * 0.01;
			syncColor(color, E, RAND_ARGS);
			alignWithMesh(particle.pos, particle.velocity, E, RAND_ARGS);
			particle.spin = RAND * 80 - 40;
		}
	}

	particle.angle = RAND * 3.14;
	particle.color = piece_packColor(color, type);
	particle.life = 1;
	// particle.collisionPlane1 = shufflePlane(E.emitterPlane1, float4(RAND_DIR, RAND_COS), particle.pos);
	particle.collisionPlane1 = E.emitterPlane1;
	particle.halfSize *= sizeMult;
	particle.dirSide_contact = normalEncode_u(float3(0, 0, 1));
	SET_CONTACT_STAGE(particle, 0);
}
