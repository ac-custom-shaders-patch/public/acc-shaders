#define LIGHTINGFX_KSDIFFUSE 0.5
#define LIGHTINGFX_KSSPECULAR 0.1
#include "pfxPieces.hlsl"

#ifdef MODE_SIMPLIFIED_FX
  #define SHADOW 1
  #define SPECULAR 0
  #define USE_REFLECTIONS 0
#elif defined(MODE_GBUFFER)
  #define SHADOW 1
  #define SPECULAR 0
  #define USE_REFLECTIONS useReflections
#else
  #define SHADOW pin.Shadow
  #define SPECULAR pin.Specular
  #define USE_REFLECTIONS useReflections
#endif

Texture2D<float> txCarCollision : register(t1);

float checkCarCollision(float3 pos){
  float4 uv = mul(float4(pos, 1), gCarCollisionTransform);
  float v = txCarCollision.SampleLevel(samLinear, uv.xy, 0);
  // if (v == 1) return 1;
  // if (uv.y > 0.98) return 1;
  return !gCarCollisions || any(abs(uv.xyz - 0.5) > float3(0.5, 0.5, 0.45)) || uv.z > 0.85 ? 1 : saturate((txCarCollision.SampleLevel(samLinear, uv.xy, 0) - uv.z) * 50);
}

PS_OUT main(PS_IN pin) {
  float interiorMult = checkCarCollision(pin.PosC + ksCameraPosition.xyz);
  if (interiorMult == 0){ clip(-1); return (PS_OUT)0; }
  float3 toCamera = normalize(pin.PosC);

  float3 albedo = piece_unpackColor(pin.Color);
  uint type = pin.Color & 0xff;
  float alpha;
  float3 normalT = float3(0, 0, 1);
  float backlitIntensity = 0;
  bool useReflections = false;

  {
    float4 txNoiseValue = txNoise.Sample(samLinear, pin.Tex);

    float2 hfTex = pin.TexBase;
    #ifndef MODE_SIMPLIFIED_FX
      // hfTex.x += pin.TexOffsetX * 0.1;
    #endif
    float4 txHfNoiseValue = txNoise.Sample(samLinear, hfTex);

    if (type == TYPE_SHARD) {
      // Piece of a car
      float4 txLfNoiseValue = txNoise.Sample(samLinear, pin.Tex.yx * 2);
      useReflections = true;
      albedo = albedo * 0.5;
      alpha = 1 - pow(dot2(pin.TexBase), 2) - txNoiseValue.x - saturate(txLfNoiseValue.r - 0.7) * 2;
      normalT = normalize(txNoiseValue.rgb);
    } else if (type == TYPE_GRASS) {
      // Grass
      float dirtPart = lerpInvSat(pin.TexBase.y - frac(pin.RandomSeed * 67.447) * 2 - txHfNoiseValue.b * 0.1, 0.4, 0.5);
      pin.TexBase.x += (1 - pow(abs(pin.TexBase.y), 2 + txNoiseValue.g)) * lerp(0.6, 0.5, dirtPart) * pin.RandomSeed;
      float dist = pow(dot2(pin.TexBase * float2(4, 0.5 + txNoiseValue.b * (1 - dirtPart * 0.2))), 2);
      alpha = 0.5 + 0.1 * dirtPart - dist;
      albedo = lerp(0.5, 1.5, lerp(txNoiseValue.b, txNoiseValue.w, txHfNoiseValue.g))
        * lerp(1, 0.1, lerpInvSat(alpha, 0.5 - 0.08 * txNoiseValue.g, 0.4) * abs(pin.TexBase.y)) 
        * albedo;    
      albedo = lerp(albedo, albedo * float3(0.7, 0.5, 0.2), dirtPart);
      backlitIntensity = lerp(1, 0.1, lerpInvSat(alpha, 0.5 - 0.08 * txNoiseValue.g, 0.4) * abs(pin.TexBase.y)) * (1 - dirtPart);
      normalT = normalize(txNoiseValue.rgb);
    } else {
      // Chunk or soil
      bool isSoil = type == TYPE_SOIL;
      float dist = pow(dot2(pin.TexBase * (1 + txNoiseValue.rg * (isSoil ? 1.4 : 1))), 2) - txHfNoiseValue.g * (isSoil ? 0.15 : 0.05);
      alpha = (isSoil ? 0.6 : 0.5) - dist;
      albedo = albedo * (0.7 + txHfNoiseValue.b * 0.2);

      float2 nmTex = pin.TexBase * (isSoil ? 1.6 : 2);
      normalT = float3(-nmTex, 1 - length(nmTex)) + float3(txHfNoiseValue.bg * 0.5 - 0.25, 0) * (isSoil ? 2 : 1);
      backlitIntensity = lerpInvSat(alpha, 0.3, 0.1);
    }

	  #if defined(MODE_SIMPLIFIED_FX) || defined(MODE_GBUFFER)
      clip(alpha - 0.4);
      alpha = 1;
    #else
      alpha = saturate((alpha - 0.4) / max(fwidth(alpha), 0.001) + 0.5);
      clip(alpha - 0.01);
    #endif
  }

	#ifndef MODE_SIMPLIFIED_FX
    normalT.xy = mul(rotate2d(pin.SinCos.x, pin.SinCos.y), normalT.xy);

    float3 normalW = normalize(normalT.x * pin.TangentW + normalT.y * pin.BitangentW + normalT.z * pin.NormalW);
  #else
    float3 normalW = pin.NormalW;
    pin.Color *= 0.7 + 0.3 * normalT.y;
  #endif

  // {
  //   ReflParams R = (ReflParams)0;
  //   R.resultPower = 0;
  //   R.resultColor = 0;
  //   float3 lighting = piece_unpackColor(pin.Color) * 1e6;
  //   // lighting = pin.NormalW.y;
  //   // lighting = saturate(dot(normalW, -ksLightDirection.xyz));
  //   float alpha = 1;
  //   RETURN(lighting, alpha);
  // }

  float fresnel = 0;
  float3 reflColor = 0;

  [branch]
  if (USE_REFLECTIONS){
    float facingToCamera = dot(normalW, -toCamera);
    fresnel = 0.05 + 0.4 * pow(saturate(1 - facingToCamera), 5);
    float3 reflDir = fixReflDir(normalize(reflect(toCamera, normalW)), pin.PosC, normalW, 0);
    reflColor = fresnel * getReflectionAt(reflDir, -toCamera, 3, false /* can’t use bias in a branch */, 0);
  }

  albedo = applyGamma(albedo, 1);

	#ifdef MODE_GBUFFER
    float3 lighting = 0;
  #else    
    float3 ambient = getAmbientBaseAt(pin.PosC, normalW, AO_LIGHTING.w);
    float litFront = LAMBERT(normalW, -ksLightDirection.xyz);
    float litBack = pow(saturate(-dot(normalW, -ksLightDirection.xyz)), 4) * backlitIntensity;

		#ifndef MODE_SIMPLIFIED_FX
      float extraLightDot = lerp(1, saturate(dot(normalW, pin.LightDir)), dot2(pin.LightDir));
    #else
      float extraLightDot = 1;
    #endif

    float3 diffuse = AO_LIGHTING.w * (ksLightColor.rgb * SHADOW * (litFront + litBack) + pin.Lighting * extraLightDot);
    float3 lighting = albedo * (ambient + diffuse + GI_LIGHTING);
    lighting += txCube.SampleLevel(samLinearSimple, toCamera * float3(-1, 1, 1), 4).rgb * (saturate(toCamera.y * 4) * albedo * backlitIntensity * 0.25);
    lighting += reflColor;
    // lighting = float3(1 - litFront, litFront, 0);
  #endif

  #ifdef MODE_GBUFFER
    ReflParams R = (ReflParams)0;
    R.resultPower = fresnel;
    R.resultColor = reflColor;
  #endif

  // lighting.r += 10;

  RETURN(lighting, alpha);
}