#include "pfxPieces.hlsl"

PS_IN_shadow main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD

	// float3 velocity = mul(particle.velocity, (float3x3)ksView);
	// float speed = length(velocity) + 0.01;
	// float quadDot = dot(quadPos, velocity);
	// quadPos += velocity * quadDot / speed / 2;

	float adjustedSize = particle.halfSize * saturate(particle.life * 40) * saturate((1 - particle.life) * 40);
  int contactStage = (uint)((particle.dirSide_contact >> 16) & 0xff) - 127;

	[branch]
	if (contactStage < 0){
		DISCARD_VERTEX(PS_IN_shadow);
	}

	float angle = particle.angle;
	float angleSin = sin(angle);
	float angleCos = cos(angle);
	quadPos.xy = mul(rotate2d(angleSin, angleCos), quadPos.xy);
	
	quadPos *= adjustedSize;
	particle.pos += ksLightDirection.xyz * adjustedSize;

	PS_IN_shadow vout;
	vout.PosH = float4(particle.pos, 1);

  float3 side = normalize(cross(ksLightDirection.xyz, float3(0, 1, 0)));
  float3 up = normalize(cross(ksLightDirection.xyz, side));

	float3 sideFlat = normalize(float3(side.x, 0, side.z));
	float3 upFlat = normalize(float3(up.x, 0, up.z));
	sideFlat = normalize(cross(float3(0, 1, 0), upFlat));

  side = lerp(side, sideFlat, saturate(-ksLightDirection.y * 2));
  up = lerp(up, upFlat, saturate(-ksLightDirection.y * 2));
  side = normalize(side);
  up = normalize(up);

	vout.PosH.xyz += side * quadPos.x;
	vout.PosH.xyz += up * quadPos.y;

	// vout.PosY = vout.PosH.y;
	// vout.GroundY = particle.groundY;
	vout.PosH = mul(vout.PosH, ksView);
	vout.PosH = mul(vout.PosH, ksProjection);
	vout.Opacity = 1; // TODO: REMOVE

	// TODO: UPDATE
	if (IS_CHUNK(particle)){
		vout.UseTexture = 0;
		vout.Tex = BILLBOARD[vertexID].xy;
		vout.Color = particle.color;
	} else {
		vout.UseTexture = 1;
		vout.Tex = BILLBOARD[vertexID].xy * 0.5 + 0.5;
		vout.Color = 0;
	}
	
	return vout;
}