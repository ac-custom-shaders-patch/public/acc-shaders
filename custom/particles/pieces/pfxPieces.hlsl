#define PARTICLES_PACKING
#define PARTICLES_MESH_SPAWN
#define STENCIL_VALUE 0.2
#include "particles/common/include_decl.hlsl"
#include "include/normal_encode_uint.hlsl"

// Ordered by weight
#define TYPE_GRASS 0
#define TYPE_SOIL 1
#define TYPE_CHUNK 2
#define TYPE_SHARD 3

#define PIECE_TYPE(_P) ((_P).color & 0xff)
#define PIECE_WEIGHT_NORMALIZED(_P) ((float)PIECE_TYPE(_P) / 3)
#define IS_GRASS(_P) (PIECE_TYPE(_P) == TYPE_GRASS)
#define IS_SOIL(_P) (PIECE_TYPE(_P) == TYPE_SOIL)
#define IS_CHUNK(_P) (PIECE_TYPE(_P) == TYPE_CHUNK)
#define IS_SHARD(_P) (PIECE_TYPE(_P) == TYPE_SHARD)

#define GRAVITY_MULT(_P) lerp(0.7, 1, PIECE_WEIGHT_NORMALIZED(_P))
#define GEN_OUTSIDE (RAND > 0.05 ? TYPE_CHUNK : RAND > 0.2 ? TYPE_GRASS : TYPE_SOIL)
#define GEN_OUTSIDE_NOGRASS (RAND > 0.05 ? TYPE_CHUNK : TYPE_SOIL)
#define GEN_OUTSIDE_ADJ(_GRASS_CHANCE) ((RAND * 0.1) < (_GRASS_CHANCE) ? TYPE_GRASS : GEN_OUTSIDE_NOGRASS)
#define SET_CONTACT_STAGE(particle, stage) particle.dirSide_contact = (particle.dirSide_contact & ~0xff0000) | ((stage + 127) << 16)
#define GET_CONTACT_STAGE(particle) ((uint)((particle.dirSide_contact >> 16) & 0xff) - 127)
#define COLLECTED_GRASS_SAMPLES_SIZE 32

#define SRCTYPE_OUTSIDE 1
#define SRCTYPE_COLLISION_CAR 2
#define SRCTYPE_COLLISION_TRACK 3
#define SRCTYPE_MESH_TEST 4
#define SRCTYPE_LAWNMOWER 5
#define SRCTYPE_LAWNMOWER_LEAK 6
#define SRCTYPE_BLOWN_TYRE 7

#define PARTICLES_OFFSET_ORIGIN(P, OFFSET) \
	P.pos.xyz += OFFSET;\
	P.collisionPlane1.w += dot(P.collisionPlane1.xyz, OFFSET);

struct Particle {
	float3 pos;
	float life;

	float4 collisionPlane1;

	float3 velocity;
	float spin;

	float angle;
	uint color;
	uint dirSide_contact;
	float halfSize;

	float3 carContactPos;
	float3 landNormal;
};

struct Particle_packed {
	float3 pos;
	float life;

	float4 collisionPlane1;

	uint2 velocity_spin;
	uint color;
	uint dirSide_contact;

	uint2 landNormal_halfSize;
	uint2 carContactPos_angle;
};

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.pos, _U.pos);\
	FN(_P.life, _U.life);\
	FN(_P.collisionPlane1, _U.collisionPlane1);\
	FN(_P.velocity_spin, _U.velocity, _U.spin);\
	FN(_P.color, _U.color);\
	FN(_P.dirSide_contact, _U.dirSide_contact);\
	FN(_P.landNormal_halfSize, _U.landNormal, _U.halfSize);\
	FN(_P.carContactPos_angle, _U.carContactPos, _U.angle);

uint piece_packColor(float3 color, uint type) {
	uint3 cu = uint3(saturate(color) * 255);
	return (cu.x << 8) | (cu.y << 16) | (cu.z << 24) | type;
}

float3 piece_unpackColor(uint v) {  
  float3 ret;
  ret.r = (v >> 8) & 0xff;
  ret.g = (v >> 16) & 0xff;
  ret.b = (v >> 24) & 0xff;
  return ret / 255.0;
}

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		float3 emitterPos1;

		uint type;
		float3 emitterVel1;

		uint emitterExtraPoints;
		float3 emitterPos2;

		float meshThresholdSqr;
		float3 emitterVel2;

		float typeGrassChance;
		float3 emitterPos3;
		
		float randomMult;
		float3 emitterVel3;

		float4 emitterPlane1;

		uint textureID;
		uint meshID;
		uint meshIndexCount;  // also sets row for SRCTYPE_LAWNMOWER/SRCTYPE_LAWNMOWER_LEAK
		float posSpread;

		float isSurfaceGrassFallback;
		float3 grassFallbackColor;

		float3 soilColor;
		float pad0;

		float4x4 transform;
	};

	#define FORCE_FIELDS 4

	struct ForceField {
		float3 pos;
		float radiusSqrInv;
		float3 velocity;
		float forceMult;
	};

	struct CarCollisionData {
		float3 force;
		float aabbY;
		float3 aabbMult;
		float sticky;
		float4x4 toUV;
		float4x4 toWorld;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		float2 gMapPointA;
		float2 gMapPointB;

		uint gALS1;
		uint gALS2;
		uint gDustColorA;
		uint gDustColorB;

		float gGravity;
		uint gPiecesFlags;
		float gFadeBoost;
		float _gPad;

		float4x4 gAreaHeightmapTransform;
		float4x4 gWipersT0;
		float4x4 gWipersT1;

		float4 gWheels[4];

		ForceField gForceFields[FORCE_FIELDS];
		CarCollisionData gCars[2];
	}

	Texture2D<float4> txCar0 : register(t11);
	Texture2D<float4> txCar1 : register(t12);
	Texture2D<float4> txWipersMask0 : register(t13);
	Texture2D<float4> txWipersMask1 : register(t14);
	Texture2D txGrass : register(t21);
	Texture2D<float> txAreaDepth : register(t22);
	Texture2D txAreaColor : register(t23);
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		PFX_PS_IN
		
		float2 TexBase : TEXCOORD11;
		nointerpolation uint Color : COLOR10;
		nointerpolation float RandomSeed : COLOR11;

		#ifndef MODE_SIMPLIFIED_FX
			float2 SinCos : TEXCOORD4;
			float3 TangentW : TEXCOORD8;
			float3 BitangentW : TEXCOORD9;
			// float TexOffsetX : TEXCOORD20;

			#ifndef MODE_GBUFFER
				float3 LightDir : TEXCOORD23;
			#endif
		#endif

		#ifndef MODE_GBUFFER
			#ifndef MODE_SIMPLIFIED_FX
				float Shadow : COLOR1;
				float Specular : TEXCOORD7;
			#endif
			float3 Lighting : COLOR2;
		#endif
	};

	struct PS_IN_shadow {
		float4 PosH : SV_POSITION;
		float2 Tex : TEXCOORD1;
		float UseTexture : TEXCOORD2;
		float3 Color : COLOR0;
		float Opacity : COLOR1;
	};

	cbuffer cbPieces : register(CBUFFER_DRAW_SLOT) {
		float gCarCollisions;
		float3 _gpad0;
		float4x4 gCarCollisionTransform;
	}
#endif

#include "particles/common/include_impl.hlsl"
