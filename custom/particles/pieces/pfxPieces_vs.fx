#define AO_EXTRA_LIGHTING 1
#define GI_LIGHTING 0
#define LIGHTINGFX_NOSPECULAR
#define LIGHTINGFX_KSDIFFUSE 0.4
#define LIGHTINGFX_SIMPLEST

#ifndef MODE_SIMPLIFIED_FX
	#define LIGHTINGFX_FIND_MAIN
#endif

#include "pfxPieces.hlsl"
#include "include_new/ext_shadows/_include_vs.fx"

// #ifndef SHADER_MIRROR
// 	#define LIGHTINGFX_FIND_MAIN
// #endif
#define POS_CAMERA posCPerVertexLighting
#define POS_TOCAMERA normalize(POS_CAMERA)
#include "include_new/base/common_ps.fx"
#include "include_new/ext_lightingfx/_include_ps.fx"
#include "include/ssgi_vs.hlsl"

#ifdef MODE_SIMPLIFIED_FX
	cbuffer cbPiecesMirror : register(b10) {
		float gSimpleMirrorMode;
	}
#endif

float3x3 rotate3d(float s, float c, float3 axis) {
	float t = 1 - c;
	float x = axis.x;
	float y = axis.y;
	float z = axis.z;
	return float3x3(
		t * x * x + c,      t * x * y - s * z,  t * x * z + s * y,
		t * x * y + s * z,  t * y * y + c,      t * y * z - s * x,
		t * x * z - s * y,  t * y * z + s * x,  t * z * z + c);
}

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD
	
	float adjustedSize = particle.halfSize * saturate(particle.life * 40) * saturate((1 - particle.life) * 40);
	float angleSin, angleCos;
	// adjustedSize = 0.1;
	sincos(particle.angle, angleSin, angleCos);
	quadPos.xy = mul(rotate2d(angleSin, angleCos), quadPos.xy);
	quadPos *= adjustedSize;

	float3 posC = particle.pos - ksCameraPosition.xyz;
	float toCameraDistance = length(posC);
	float3 toCamera = posC / toCameraDistance;
	float3 alongSurface = cross(float3(0, 0.2, 0) + particle.landNormal, normalDecode_u(particle.dirSide_contact));
	float3 adjNormal = particle.landNormal + toCamera * -0.2;// * sign(dot(toCamera, particle.landNormal));
  float3 side = normalize(cross(adjNormal, alongSurface));
  float3 up = normalize(cross(adjNormal, side));
	particle.pos += particle.landNormal * adjustedSize * 0.5;

	float3 offset = side * quadPos.x;
	offset -= up * quadPos.y;
	float3 velocity = particle.velocity;
	float speed = length(velocity);
	if (speed > 0.01){
		particle.pos += normalize(velocity) * speed / (1 + speed) * dot(normalize(velocity), normalize(offset)) * 0.02;
	}

	#ifdef MODE_SIMPLIFIED_FX
		if (gSimpleMirrorMode && GET_CONTACT_STAGE(particle) > 0) {
			DISCARD_VERTEX(PS_IN);
		}
	#endif

// particle.pos.y -= 0.01;
	float4 posW = float4(particle.pos + offset, 1);
	float4 posV = mul(posW, ksView);
	// posV.z -= particle.halfSize * 2;
	// posV.z += 0.2;

	// x = y + 0.2
	// x = y + y * 0.2 / y
	// x = y * (1 + 0.2 / y)
  int contactStage = GET_CONTACT_STAGE(particle);
	if (contactStage == -2) {
		// posV.xyz *= 1 - min(0.1, 0.02 / max(0.1, abs(toCamera.y))) / max(1, -posV.z);
	}

	PS_IN vout;
	vout.PosH = mul(posV, ksProjection);
	#ifdef MODE_GBUFFER
		GENERIC_PIECE_VELOCITY(posW, particle.velocity * extFrameTime);
	#endif
	vout.PosC = particle.pos - ksCameraPosition.xyz;
	vout.TexBase = BILLBOARD[vertexID].xy;
	vout.RandomSeed = frac(particle.halfSize * 831.145);
	vout.Color = particle.color;
	if (IS_CHUNK(particle)){
		vout.Tex = BILLBOARD[vertexID].xy * 0.1;

		// vout.TexBase.x += atan2(toCamera.x, toCamera.z) * 1e5;
	} else if (IS_GRASS(particle)) {
		vout.Tex = BILLBOARD[vertexID].xy * float2(1, 0.03);
	} else if (IS_SOIL(particle)) {
		vout.Tex = BILLBOARD[vertexID].xy * 0.2;
		// vout.TexBase.x += atan2(toCamera.x, toCamera.z) * 1e5;
	} else {
		vout.Tex = BILLBOARD[vertexID].xy * float2(0.08, 0.06);
	}
	vout.Tex += float2(frac(particle.halfSize * 769.771), frac(particle.halfSize * 299.261));

	// float3x3 rot = rotate3d(angleSin, angleCos, toCamera);
	#ifndef MODE_SIMPLIFIED_FX
		// vout.TexOffsetX = atan2(toCamera.x, toCamera.z);
		// vout.TangentW = mul(side, rot);
		// vout.BitangentW = mul(up, rot);
		// vout.NormalW = normalize(cross(vout.TangentW, vout.BitangentW));
		// vout.TangentW *= float3(1, -1, 1);
		// vout.BitangentW *= float3(1, -1, 1);
		vout.NormalW = normalize(cross(side, up));
		vout.BitangentW = normalize(up);
		vout.TangentW = -normalize(side);
		vout.SinCos.x = angleSin;
		vout.SinCos.y = angleCos;
	#else
		vout.NormalW = normalize(cross(side, up));
	#endif
	vout.Fog = calculateFog(posV);

	#ifndef MODE_GBUFFER
		float4 posWPerVertexLighting = posW;
		posWPerVertexLighting -= ksLightDirection * particle.halfSize;

		#ifndef MODE_SIMPLIFIED_FX
			float4 tex0 = mul(posWPerVertexLighting, ksShadowMatrix0);
			vout.Shadow = getShadowBiasMult(vout.PosC, 0, float3(0, 1, 0), tex0, 0, 1);
			vout.Specular = IS_SHARD(particle) ? 1 : 0;
		#endif

		float3 posCPerVertexLighting = posWPerVertexLighting.xyz - ksCameraPosition.xyz;
		vout.Lighting = 0;
		LFX_MainLight mainLight;
		LIGHTINGFX(vout.Lighting);
		vout.Lighting += getSSGI(vout.PosH);

		#ifndef MODE_SIMPLIFIED_FX
			float lightingFocus = saturate(2 * mainLight.power / max(dot(vout.Lighting, 1), 1));
			vout.LightDir = mainLight.dir * lightingFocus;
		#endif
	#endif

	return vout;
}