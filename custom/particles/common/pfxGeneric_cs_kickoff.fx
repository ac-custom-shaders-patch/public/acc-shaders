#define USE_GENERIC
#include "include_decl.hlsl"

RWByteAddressBuffer counterBuffer : register(u4);
RWByteAddressBuffer indirectBuffers : register(u5);

[numthreads(1, 1, 1)]
void main(uint3 DTid : SV_DispatchThreadID) {
	uint deadCount = counterBuffer.Load(PARTICLECOUNTER_OFFSET_DEADCOUNT);
	uint aliveCount_NEW = counterBuffer.Load(PARTICLECOUNTER_OFFSET_ALIVECOUNT_AFTERSIMULATION);

	uint realEmitCountTotal = gEmitCountTotal;
	uint maxEmitPerGroup = gEmittersCount <= 0 ? 0 : gMaxEmitCountPerGroup;
	if (realEmitCountTotal > deadCount) {
		maxEmitPerGroup = gEmittersCount <= 0 ? 0 : uint(floor((float)deadCount / (float)gEmittersCount));
		realEmitCountTotal = maxEmitPerGroup * gEmittersCount;
	}

	indirectBuffers.Store3(ARGUMENTBUFFER_OFFSET_DISPATCHEMIT, uint3(ceil((float)maxEmitPerGroup / (float)THREADCOUNT_EMIT), gEmittersCount, 1));
	indirectBuffers.Store3(ARGUMENTBUFFER_OFFSET_DISPATCHSIMULATION, uint3(ceil((float)(aliveCount_NEW + realEmitCountTotal) / (float)THREADCOUNT_SIMULATION), 1, 1));
	counterBuffer.Store(PARTICLECOUNTER_OFFSET_ALIVECOUNT, aliveCount_NEW);
	counterBuffer.Store(PARTICLECOUNTER_OFFSET_ALIVECOUNT_AFTERSIMULATION, 0);
	counterBuffer.Store(PARTICLECOUNTER_OFFSET_REALEMITCOUNT, maxEmitPerGroup);
}
