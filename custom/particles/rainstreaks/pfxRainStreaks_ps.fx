#include "pfxRainStreaks.hlsl"

float main(PS_IN pin) : SV_Target {
  clip(1 - dot2(pin.Tex));
  return 1;
}