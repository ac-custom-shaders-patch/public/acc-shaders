#include "pfxRainStreaks.hlsl"
#include "include/samplers.hlsl"

Texture2D<float> txWater : register(t5);
Texture2D txNoise : register(TX_SLOT_NOISE);

void process(inout Particle particle, uint3 DTid) {
  float4 noise = txNoise.SampleLevel(samLinearWrap, particle.pos * 3, 0);
  float offset = noise.x * 2 - 1;
  offset = pow(abs(offset), 2) * sign(offset) * 2;

  float thickness = txWater.SampleLevel(samLinearWrap, particle.pos, areaMipLevel);
  bool isMoving = dot(particle.velocity, 1);
  if (thickness > rolloffThreshold || isMoving){
    particle.velocity = normalize(float2(offset, 1)) * lerp(0.01, 0.4, particle.speed);
  }

  particle.pos += particle.velocity * gFrameTime;
  particle.life -= isMoving ? gFrameTime : DTid.x > 3800 ? gFrameTime * 10 : gFrameTime * 0.1;
  particle.pos = frac(particle.pos);
}
