// #define PARTICLES_PACKING
#define PARTICLES_WITHOUT_NORMALS 
#define PARTICLES_NO_GRAPHICS_SPACE 
#include "particles/common/include_decl.hlsl"

struct Particle {
	float2 pos;
	float2 velocity;
	float life;
	float size;
	float speed;
	float _pad;
};

struct Particle_packed {
	float3 pos;
	uint life_size;
	float4 pad;
};

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.pos, _U.pos);\
	FN(_P.life_size, _U.life, _U.size);

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		float3 emitterPos;
		float intensity;
		float3 pad0;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN
		float rolloffThreshold;
		float areaMipLevel;
		float2 _pad0;
	};
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		float4 PosH : SV_POSITION;
		float2 Tex : TEXCOORD1;
		float NoiseOffset : TEXCOORD2;
		// float Intensity : TEXCOORD2;
	};
#endif

#include "particles/common/include_impl.hlsl"
