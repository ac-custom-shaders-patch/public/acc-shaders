#include "pfxRainStreaks.hlsl"

void emit(inout Particle particle, EmittingSpot E, RAND_DECL) {
	particle.pos = float2(RAND, RAND) * 0.99 + float2(RAND, RAND) * 0.01;
	particle.life = 1;
	particle.size = RAND;
	particle.speed = RAND;
}
