#include "pfxOilSpill.hlsl"

Texture2D<float> txPuddle : register(t4);

float main(PS_IN pin) : SV_TARGET {
  float dist = length(pin.Tex);
  dist *= 1 + pow(txPuddle.Sample(samLinear, 0.2 + pin.TexNoise * 0.2), 2) * 3;
  clip(1 - dist);
  
  float f = saturate(remap(dist, 0.5, 1, 1, 0));
  float n = txPuddle.Sample(samLinear, pin.TexNoise);
  float p = n * lerp(f, 1, saturate(n * 4 - 2));
  return p * pin.Opacity;
}