#include "pfxOilSpill.hlsl"

void emit(inout Particle particle, EmittingSpot E, RAND_DECL) {
	particle.pos = E.pos;// + float3(RAND - 0.5, 0, RAND - 0.5) * 4;
	particle.dir = E.dir;
	particle.life = 1;
	particle.size = E.intensity * 0.5 + RAND * 0.5;
}
