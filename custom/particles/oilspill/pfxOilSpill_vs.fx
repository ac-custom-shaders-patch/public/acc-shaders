#include "pfxOilSpill.hlsl"

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD

  float3 side = normalize(cross(particle.dir, float3(0, 0, 1)));
  float3 up = normalize(cross(particle.dir, side));
	float3 offset = side * quadPos.x + up * quadPos.y;
	float size = lerp(2, 3, particle.size) * pow(saturate(1 - particle.life), 0.2);

	float4 posW = float4(particle.pos, 1);
	posW.xyz += offset * size;
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

	float2 texOffset = float2(frac(particle.size * 16126.437), frac(particle.size * 61672.161));

	PS_IN vout;
	vout.PosH = posH;
	vout.Tex = BILLBOARD[vertexID].xy;
	vout.TexNoise = vout.Tex * 0.1 * lerp(1, size, 0.8) + texOffset;
	vout.Opacity = 0.6 * saturate(particle.life * 4) * lerp(0.5, 1, particle.size);
	return vout;
}