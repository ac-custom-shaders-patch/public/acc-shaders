#include "include/common.hlsl"

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#include "include/poisson.hlsl"
float main(VS_Copy pin) : SV_TARGET {
  POISSON_AVG(float, result, txDiffuse, samLinearClamp, pin.Tex, 0.004, 8);
  return result;
}
