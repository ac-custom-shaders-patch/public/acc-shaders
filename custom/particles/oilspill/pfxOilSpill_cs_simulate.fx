#include "pfxOilSpill.hlsl"

void process(inout Particle particle, uint3 DTid) {
  particle.life -= DTid.x > gALS2 ? gFrameTime * 10 : DTid.x > gALS1 ? gFrameTime * 0.1 : gFrameTime * 0.01;
}
