#define PARTICLES_WITHOUT_NORMALS 
#include "particles/common/include_decl.hlsl"

struct Particle {
	float3 pos;
	float life;
	float3 dir;
	float size;
};

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		float3 pos;
		float3 dir;
		float intensity;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		uint gALS1;
		uint gALS2;
		uint2 _pad1;
	};
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		float4 PosH : SV_POSITION;
		float2 Tex : TEXCOORD1;
		float2 TexNoise : TEXCOORD2;
		float Opacity : TEXCOORD3;
	};
#endif

#include "particles/common/include_impl.hlsl"
