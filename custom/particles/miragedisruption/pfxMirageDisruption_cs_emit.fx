#include "pfxMirageDisruption.hlsl"

void emit(inout Particle particle, EmittingSpot E, RAND_DECL) {
	particle.pos = E.emitterPos;
	particle.life = 1;
	particle.intensity = E.intensity;
}
