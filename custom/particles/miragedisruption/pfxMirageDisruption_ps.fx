#include "pfxMirageDisruption.hlsl"

float main(PS_IN pin) : SV_Target {
  float s = -0.8;
  if (pin.Tex.y > s) pin.Tex.y = saturate(remap(pin.Tex.y, s, 1, 0, 1));
  else pin.Tex.y = -saturate(-remap(pin.Tex.y, -1, s, -1, 0));
  return saturate(2 - length(pin.Tex.xy) * 2) * pin.Intensity;
}