// #define PARTICLES_PACKING
#define PARTICLES_WITHOUT_NORMALS 
#include "particles/common/include_decl.hlsl"

struct Particle {
	float3 pos;
	float life;
	float3 dpos0;
	float intensity;
	float3 dpos1;
	float pad0;
	float3 dpos2;
	float pad1;
};

struct Particle_packed {
	float3 pos;
	uint life_intensity;
	uint2 dpos0;
	uint2 dpos1;
	uint2 dpos2;
};

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.pos, _U.pos);\
	FN(_P.life_intensity, _U.life, _U.intensity);\
	FN(_P.dpos0, _U.dpos0);\
	FN(_P.dpos1, _U.dpos1);\
	FN(_P.dpos2, _U.dpos2);

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		float3 emitterPos;
		float3 dpos0;
		float intensity;
		float3 dpos1;
		float life;
		float3 dpos2;
		float pad;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		uint gALS1;
		uint gALS2;
		float gFadingInv;
		uint2 _pad;
	};
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		float4 PosH : SV_POSITION;
		float2 TexW : TEXCOORD0;
		float2 Tex : TEXCOORD1;
	};
#endif

#include "particles/common/include_impl.hlsl"
