#include "pfxSkidWet.hlsl"

void process(inout Particle particle, uint3 DTid) {
  // particle.life -= DTid.x > gALS2 ? gFrameTime * 10 : particle.life > 0.5 || DTid.x > gALS1 ? gFrameTime * 0.5 : 0;
  particle.life -= DTid.x > gALS2 ? gFrameTime * 10 : DTid.x > gALS1 * 2 ? gFrameTime * 2 : gFrameTime;
  particle.intensity = min(particle.intensity, gFadingInv);
}
