#include "pfxSkidWet.hlsl"

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD
	// quadPos *= saturate(particle.life * 2) * 200;

	float3 billboardAxis = normalize(particle.pos - ksCameraPosition.xyz);
	#ifdef MODE_SHADOW
		billboardAxis = ksLightDirection.xyz;
	#endif

  float3 side = normalize(cross(billboardAxis, float3(0, -1, 0)));
  float3 up = normalize(cross(billboardAxis, side));
	float3 offset = side * quadPos.x + up * quadPos.y;

	if (vertexID == 1 || vertexID == 4){
		particle.pos += particle.dpos0;
	} else if (vertexID == 2 || vertexID == 3){
		particle.pos += particle.dpos2;
	} else if (vertexID == 5){
		particle.pos += particle.dpos1;
	}

	float4 posW = float4(particle.pos, 1);
	// posW.xyz += offset;
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

	PS_IN vout;
	vout.PosH = posH;
	// vout.Tex = BILLBOARD[vertexID].xy;
	vout.Tex.x = BILLBOARD[vertexID].x;
	vout.Tex.y = saturate(particle.life);
	vout.Tex.y = max(vout.Tex.y, saturate(particle.life * 10 - 9));
	vout.Tex.y *= particle.intensity;
	// vout.Tex.y = particle.intensity;

	vout.TexW = posW.xz * 0.15;
	return vout;
}