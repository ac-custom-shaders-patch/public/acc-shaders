#include "pfxSkidWet.hlsl"

float main(PS_IN pin) : SV_TARGET {
  float4 noise = txNoise.SampleLevel(samLinearSimple, pin.TexW, 0);
  return pin.Tex.y * saturate((1 - abs(pin.Tex.x)) * 4) * 1.2 - noise.x * 0.2;
}