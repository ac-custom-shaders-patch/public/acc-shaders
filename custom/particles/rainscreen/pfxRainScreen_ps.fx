#include "pfxRainScreen.hlsl"

cbuffer cbData : register(b10) {
	float gAspectRatio;
	float gFade;
  float2 gScreenSizeInv;
}

float main(PS_IN pin) : SV_Target {
  float offset = length(pin.Tex);
  clip(1 - offset);
  return saturate(smoothstep(1, 0.2, offset)) * pin.Opacity;
}