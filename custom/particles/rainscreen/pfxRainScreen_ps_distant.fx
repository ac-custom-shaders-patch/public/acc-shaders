#define TYPE_DISTANT
#include "pfxRainScreen.hlsl"

Texture2D txBlurred : register(t0);

cbuffer cbData : register(b10) {
	float gAspectRatio;
	float gFade;
  float2 gScreenSizeInv;

  float3 gCameraUp;
  float gPad0;

  float3 gCameraSide;
  float gPad1;

  float3 gCameraDir;
  float gPad2;
}

float4 main(PS_IN pin) : SV_Target {
  float2 offset = pin.PosH.xy * gScreenSizeInv - pin.Pos;
  float distance = length(offset * float2(gAspectRatio, 1)) / pin.Radius;
  float2 offsetNormalized = offset * float2(gAspectRatio, 1) / pin.Radius;

  float splashK = pow(pin.Splash, 2);
  float4 noise = txNoise.SampleLevel(samLinearSimple, offsetNormalized * lerp(0.1, 0.5, splashK) + pin.NoiseOffset, 0);
  pin.Tex *= lerp(1, 1 + splashK, noise.x);

  float fromCenter = length(pin.Tex);
  clip(1 - fromCenter);


  float4 color;
  #ifdef DELAYED_RESOLVE
    color.rg = -offset * (1 + fromCenter);
    color.b = 2;
    color.w = smoothstep(0, 1, saturate(1 - fromCenter));
  #else
    color = txBlurred.SampleBias(samLinearClamp, pin.Pos - offset * (1 + fromCenter), 2);
    color.w *= smoothstep(0, 1, saturate(1 - fromCenter));
  #endif

  return color;

  // float4 refraction = txBlurred.SampleBias(samLinearClamp, pin.Pos - offsetNormalized * (0.06 / (0.5 + max(0.01, 1 - distance))) * float2(1, gAspectRatio), 2 * pow(fromCenter, 2));
  // float3 normal = float3(offsetNormalized.x, offsetNormalized.y, sqrt(1 - dot2(offsetNormalized)));
  // float3 normalWorld = normalize(normal.x * gCameraSide - normal.y * gCameraUp - normal.z * gCameraDir);
  // float3 reflDir = normalize(reflect(gCameraDir, normalWorld));
  // float3 reflection = txCube.Sample(samLinearSimple, reflDir).xyz;
  // reflection = max(0, reflection / (1 + reflection * 0.1));
  // reflection *= lerp(0.3, 1, saturate(reflDir.y * 3 + 1));

  // refraction.rgb = lerp(reflection.rgb, refraction.rgb, normal.z);
  // return float4(refraction.rgb, pow(saturate((1 - fromCenter) * 4 * pin.Opacity), 2));
}