#include "pfxRainScreen.hlsl"
#include "include/samplers.hlsl"

Texture2D<float> txWater : register(TX_SLOT_NOISE);

void process(inout Particle particle, uint3 DTid) {
  // float4 noise = txNoise.SampleLevel(samLinearWrap, particle.pos * 4, 0);
  // float offset = noise.x * 2 - 1;
  // offset = pow(abs(offset), 2) * sign(offset) * 2;

  float2 grad = 0;
  float density = 0;

  if (gUseNearbyForce) {
    float water00 = txWater.SampleLevel(samLinearBorder0, particle.pos, 3);
    float waterZ0 = txWater.SampleLevel(samLinearBorder0, particle.pos + float2(-0.05, 0), 3);
    float water0Z = txWater.SampleLevel(samLinearBorder0, particle.pos + float2(0, -0.05), 3);
    float waterA0 = txWater.SampleLevel(samLinearBorder0, particle.pos + float2(0.05, 0), 3);
    float water0A = txWater.SampleLevel(samLinearBorder0, particle.pos + float2(0, 0.05), 3);
    float gradX = lerp(waterA0 - water00, water00 - waterZ0, 0.5);
    float gradY = lerp(water0A - water00, water00 - water0Z, 0.5);
    grad = normalize(float2(gradX, gradY));
    density = saturate(water00 * 2 - 1);
  }

  float2 force = gForce.xy * 0.2;
  force.x += (particle.pos.x * 2 - 1) * max(gForce.z, 0);
  force.y -= max(gForce.z * 0.15, 0);
  force *= 1 + density * 3;
  force += grad * density * -2;

  if (particle.pad.x > 0){
    particle.pad.x -= length(force) * gFrameTime + density;
  } else {
    particle.velocity += force * gFrameTime * (gUseNearbyForce ? 1 : 0.4);
  }

  particle.pos += particle.velocity * gFrameTime;
  particle.life -= (particle.pad.x > 0 ? gFrameTime * 0.3 : gFrameTime) * lerp(0.5, 1, saturate(abs(gForce.z) * 0.5));
  // particle.life -= gFrameTime;

  // particle.pos.y = saturate(abs(gForce.z) * 0.5);

  if (any(particle.pos < -0.5 || particle.pos > 1.5)) {
    particle.life = 0;
  }
}
