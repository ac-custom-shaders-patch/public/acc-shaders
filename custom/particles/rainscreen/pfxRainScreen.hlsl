// #define PARTICLES_PACKING
#define PARTICLES_WITHOUT_NORMALS 
#define PARTICLES_NO_GRAPHICS_SPACE 
#define PARTICLES_SORTING 
#define PARTICLES_SORTING_CUSTOM 

#include "particles/common/include_decl.hlsl"

struct Particle {
	float2 pos;
	float2 velocity;
	float life;
	float size;
	float2 pad;
};

struct Particle_packed {
	float3 pos;
	uint life_size;
	float4 pad;
};

float sortingFunc(Particle particle){
	return particle.size;
}

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.pos, _U.pos);\
	FN(_P.life_size, _U.life, _U.size);

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		float3 emitterPos;
		float intensity;
		float3 pad0;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		float3 gForce;
		float gCollisionCheckOnSpawn;

		float3 gCameraPos;
		float gSceneWater_;

		float3 gRainDir;
		float gAdjustedIntensity;

		float3 gCameraDir;
		float gUseNearbyForce;
		
		float4x4 gViewProjInv;
		float4x4 gShadowTransform;
		float4x4 gCarCollisionTransform;
	};
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		float4 PosH : SV_POSITION;
		float2 Tex : TEXCOORD1;
		float NoiseOffset : TEXCOORD2;
		float Opacity : TEXCOORD3;

		#ifdef TYPE_DISTANT
			float2 Pos : TEXCOORD4;
			float Splash : TEXCOORD5;
			float Radius : TEXCOORD6;
		#endif
	};
#endif

#include "particles/common/include_impl.hlsl"
