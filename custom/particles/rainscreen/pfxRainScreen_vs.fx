#include "pfxRainScreen.hlsl"

cbuffer cbData : register(b10) {
	float gAspectRatio;
	float gFade;
  float2 gScreenSizeInv;
}

float4 texToScreen(float2 tex){
  float2 pos = tex;
  pos.y = 1 - pos.y;
  return float4(pos * 2 - 1, 0, 1);
}

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD

	float speed = length(particle.velocity);
	float2 side = speed == 0 ? float2(1, 0) : particle.velocity / speed * float2(1, -1);
	float2 up = float2(side.y, -side.x);

	float2 B = BILLBOARD[vertexID].xy;
	float size = lerp(1, 2, particle.size) * sqrt(saturate(particle.life * 20)) * 0.07 * (1 + pow(saturate(particle.life), 20));
	float2 offset = (size + speed * 0.04) * side * B.x + size * up * B.y;
	
	PS_IN vout;
	vout.PosH = texToScreen(particle.pos) + float4(offset * float2(1, gAspectRatio), 0, 0);
	vout.Tex = BILLBOARD[vertexID].xy;
	vout.NoiseOffset = frac(particle.size * 193.72);
	vout.Opacity = saturate(particle.life * 2) * 0.333;
	return vout;
}