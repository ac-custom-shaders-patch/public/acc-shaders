#include "pfxRainScreen.hlsl"

Texture2D txSplashes : register(t21);
Texture2DArray<float> txRainShadow : register(t22);
Texture2D<float> txCarCollision : register(t23);

bool carCollisionCheck(float3 pos){
  float4 uv = mul(float4(pos, 1), gCarCollisionTransform);
  return all(abs(uv.xyz - 0.5) < 0.5) && txCarCollision.SampleLevel(samLinearClamp, uv.xy, 0) < uv.z;
}

#include "include/poisson.hlsl"
float2 sampleShadow(float2 uv, float comparison){
  float2 sum = 0;
  float sampleScale = 0.0015;
  if (comparison >= 1) return 1; 

  #define RAIN_DISK_SIZE 4
  for (uint i = 0; i < RAIN_DISK_SIZE; ++i) {
    float2 sampleUV = uv.xy + SAMPLE_POISSON(RAIN_DISK_SIZE, i) * sampleScale;
    float comparisonUV = comparison - 0.001 * (1 + SAMPLE_POISSON_LENGTH(RAIN_DISK_SIZE, i));
    sum.x += txRainShadow.SampleCmpLevelZero(samShadow, float3(sampleUV, 0), comparisonUV);
    sum.y += txRainShadow.SampleCmpLevelZero(samShadow, float3(sampleUV, 1), comparisonUV);
  }
  return saturate(float2(sum.x, sum.y) / RAIN_DISK_SIZE);
}

void emit(inout Particle particle, EmittingSpot E, RAND_DECL) {
	particle.pos = float2(RAND, RAND) * 0.99 + float2(RAND, RAND) * 0.01;

  float3 dirW = normalize(mul(float4((particle.pos * 2 - 1) * float2(1, -1), 0.5, 1), gViewProjInv).xyz);
	float3 posW = dirW + gCameraPos - gCameraDir * dot(gCameraDir, dirW);
	float3 posS = mul(float4(posW, 1), gShadowTransform).xyz;
  float shadow = sampleShadow(posS.xy, posS.z).y;
  // float shadow2 = shadow;

  float sideK = abs(particle.pos.x * 2 - 1 + gForce.x * 0.1);
	shadow *= saturate(-dot(gCameraDir, gRainDir) * 2 + 0.2);
	shadow *= lerp(0.1, 1, pow(sideK, 2)) * gAdjustedIntensity;
	shadow *= lerp(0.33, 1, pow(abs(particle.pos.x * 2 - 1), 2));
	// shadow = 0;

	float4 splashes = txSplashes.SampleLevel(samLinearBorder0, particle.pos, 0);
	// particle.life = splashes.a > 0 && splashes.z > 0 || shadow > RAND * 0.99 ? 1 : 0;
	// particle.life = splashes.a > 0 && splashes.z > 0 ? 1 : 0;
	particle.life = splashes.a > 0 && splashes.z > 0 || shadow > RAND * 2 ? 1 : 0;
	// particle.life = shadow2 > 0.9 ? 1 : 0;
	// particle.life = splashes.a;
	particle.size = RAND;
	particle.velocity = 0;
	particle.pad.x = RAND;

  if (gCollisionCheckOnSpawn && carCollisionCheck(lerp(gCameraPos, posW, 0.25))) {
    particle.life = 0;
  } else {
    // particle.life = 0.5;
  }
}
