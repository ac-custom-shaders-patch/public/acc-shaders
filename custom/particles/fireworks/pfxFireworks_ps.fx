#include "pfxFireworks.hlsl"
#include "include_new/ext_functions/depth_map.fx"

Texture2D txSmokeNoise : register(t4);

PS_OUT main(PS_IN pin) {
  float3 toCamera = normalize(pin.PosC);

  // // pin.Tex.y = 1 - 2 * pow(saturate(-pin.Tex.y * 0.5 + 0.5), lerp(1 + abs(pin.Tex.x) * 2, 1, pin.Ratio));
  // float alpha = pow(saturate((1 - dot2(pin.Tex))), 500)
  //   + pow(saturate((1 - dot2(pin.Tex * float2(1, 1 + pin.Ratio * 10)))), 2) * 0.005;
  // // float alpha = saturate((1 - dot2(pin.Tex))) * 0.1;
  // float3 lighting = pin.Color;
  // float headingK = saturate((0.5 - 5 * pin.Tex.y) * 1.2);
  // lighting *= pin.GlowExtra + lerp(0, 1 - pin.Ratio, headingK);
  // lighting = lerp(luminance(lighting), lighting, headingK);

  // // lighting = float3(saturate(pin.Tex.y), saturate(-pin.Tex.y), 0) * 10;
  // // clip(alpha - 0.01);

  // float additiveK = 0.01;
  // alpha *= additiveK;
  // lighting /= additiveK;

  float3 lighting = pin.Color;
  float alpha = saturate((1 - dot2(pin.Tex)) * 100) * saturate(dot(pin.Color, 0.1));
  lighting /= min(dot(lighting, 0.1), 1);

  lighting = GAMMA_KSEMISSIVE(lighting) * getEmissiveMult();

  // float depthZ = txDepth.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw, 0).x;
  // float depthC = pin.PosH.z;
  // if (depthC > depthZ + 0.1) alpha = 0;
  // lighting = float3(txDepth.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw, 0).x, 
  //   1 - txDepth.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw, 0).x, 
  //   0);
  // alpha = 1;

  // lighting = float3(1, 0, 0);
  // alpha = 1;

  RETURN_BASE(lighting, saturate(alpha));
}