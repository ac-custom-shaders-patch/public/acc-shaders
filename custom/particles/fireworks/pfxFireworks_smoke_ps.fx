#include "pfxFireworks_smoke.hlsl"

Texture2D txSparkingGlow : register(t0);
Texture2D txSmokeNoise : register(t4);

PS_OUT main(PS_IN pin) {
  float3 toCamera = normalize(pin.PosC);

  float4 tex = txSmokeNoise.Sample(samLinearSimple, pin.Tex * pin.TexScale + pin.TexOffset);
  float alpha = (saturate(1 - dot2(pin.Tex)) - tex.a) * pin.Opacity;
  float3 lighting = GAMMA_LINEAR(pin.Color) * (saturate(tex.y) * AMBIENT_COLOR_AT(pin.PosC) * 0.4
    + saturate(dot(pin.OffsetW, -ksLightDirection.xyz)) * ksLightColor.rgb * 0.2);

  float4 glow = txSparkingGlow.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw + (tex.xy - 0.5) * 0.3, GAMMA_OR(4.5, 5.5))
    + txSparkingGlow.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw + (tex.xy - 0.5) * 0.1, GAMMA_OR(1.5, 2.5)) * 0.1;
  lighting += glow.xyz * GAMMA_OR(0.1, 1);

  RETURN_BASE(lighting, saturate(alpha * GAMMA_OR(0.15, 1)));
}