#define PASS_INDEX
#include "pfxFireworks_smoke.hlsl"

float3 getRandDir(EmittingSpot E, uint index, RAND_DECL){
	return float3(RAND - 0.5, RAND - 0.5, RAND - 0.5);
}

void emit(inout Particle particle, EmittingSpot E, uint index, RAND_DECL) {
	particle.pos = E.pos + (float3(RAND, RAND, RAND) - 0.5) * E.posSpread;
	particle.color = packColor(lerp(unpackColor(E.colorA), unpackColor(E.colorB), RAND));
	particle.life = E.lifespan * lerp(1, 2 * RAND, E.lifespanSpread);
	particle.size = 0;
	particle.lifePassed = 0;
	particle.lifeSpan = particle.life;
	particle.opacity = E.opacity;
	particle.uvOffset = float2(RAND, RAND);
	particle.targetSize = E.size * lerp(1, 2 * RAND, E.sizeSpread);

	float3 randDir = getRandDir(E, index, RAND_ARGS);
	particle.velocity = E.velocity + normalize(E.velDir 
		+ float3(randDir.x, 0, randDir.z) * E.velDirSpreadXZ 
		+ float3(0, randDir.y, 0) * E.velDirSpreadY) * lerp(E.speedMin, E.speedMax, RAND);
}
