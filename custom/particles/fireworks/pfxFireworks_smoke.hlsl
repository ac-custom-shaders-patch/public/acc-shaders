// #define PARTICLES_PACKING
#define PARTICLES_WITHOUT_NORMALS 
#define PARTICLES_SORTING 
#include "particles/common/include_decl.hlsl"

struct Particle {
	float3 pos;
	float life;

	float3 velocity;
	float size;

	uint color;
	float targetSize;
	float2 uvOffset;

	float lifePassed;
	float opacity;
	float lifeSpan;
	float pad1;
	float4 pad2;
};

// struct Particle_packed {
// 	float3 pos;
// 	float pad0;
// 	uint2 velocity_size;
// 	uint2 color_life;
// };
// #define PARTICLES_PACKING_RULES(FN)\
// 	FN(_P.pos, _U.pos);\
// 	FN(_P.pad0, _U.pad0);\
// 	FN(_P.velocity_size, _U.velocity, _U.size);\
// 	FN(_P.color_life, _U.color, _U.life);

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		float3 pos;

		float posSpread;
		uint spreadFlags;
		float lifespanSpread;
		float sizeSpread;

		float lifespan;
		float3 velocity;

		float3 velDir;
		float velDirSpreadXZ;

		float velDirSpreadY;
		float speedMin;
		float speedMax;
		float size;

		float stretch;
		uint colorA;
		uint colorB;
		float opacity;
	};

	#define FORCE_FIELDS 8

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		float gGravity;
		float gGroundEstimate;
		uint gALS1;
		uint gALS2;

		float2 extWindVel;
		float extWindSpeed;
		float extWindWave;

		float extRainIntensity;
		float3 extPad1;

		float4 extForceFields[FORCE_FIELDS];
	};
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		PFX_PS_IN
		float3 Color : COLOR0;
		float2 TexOffset : TEXCOORD4;
		float TexScale : TEXCOORD5;
		float Opacity : TEXCOORD7;
		float3 OffsetW : TEXCOORD8;
	};

	/* cbuffer cbFireworks : register(CBUFFER_DRAW_SLOT) {
		float brightnessBase;
		float brightnessExtra;
		float sizeMultiplier;
		float velocityMultiplier;
		float fadingBase;
		float fadingLifeLeftK;
		float motionBlurExtra;
		float gDeltaTime;
	} */
#endif

#include "particles/common/include_impl.hlsl"
