#define PASS_INDEX
#include "pfxFireworks.hlsl"

float3 getRandDir(EmittingSpot E, uint index, RAND_DECL){
	if (E.spreadFlags == 1){
		uint partA = uint(ceil(pow(float(E.emitCount), 0.4)));
		uint partB = E.emitCount / partA;
		float vert = index % partA;

		float angle = 2 * M_PI * (index / partA) / partB;
		return float3(sin(angle), float(vert) / partA * 2 - 1, cos(angle))
			+ float3(RAND - 0.5, RAND - 0.5, RAND - 0.5) * 0.1;
	}
	return float3(RAND - 0.5, RAND - 0.5, RAND - 0.5);
}

float getRandColor(RAND_DECL){
	float x = RAND * 2 - 1;
	return sqrt(abs(x)) * sign(x) * 0.5 + 0.5;
}

// RWBuffer<uint> dummyUAV : register(u5);
// cbuffer dummyCB : register(b11) {uint zero;}

void emit(inout Particle particle, EmittingSpot E, uint index, RAND_DECL) {
	// [loop] while (zero == 0) dummyUAV[0] = zero;

	particle.pos = E.pos + (float3(RAND, RAND, RAND) - 0.5) * E.posSpread;
	particle.color = packColor(lerp(unpackColor(E.colorA), unpackColor(E.colorB), getRandColor(RAND_ARGS)));
	particle.life = E.lifespan * lerp(1, 2 * RAND, E.lifespanSpread);
	particle.size = E.size * lerp(1, 2 * RAND, E.sizeSpread);
	particle.stretch = E.stretch;
	particle.opacity = E.opacity;
	particle.lifespan = particle.life;

	float3 up = normalize(E.velocity + float3(RAND - 0.5, 3, RAND - 0.5) * 0.001);
	float3 side = normalize(cross(abs(up.y) > 0.9 ? float3(1, 0, 0) : float3(0, 1, 0), up));
	float3 fwd = normalize(cross(side, up));

	// up = float3(0, 1, 0);
	// side = float3(1, 0, 0);
	// fwd = float3(0, 0, 1);

	float3 randDir = getRandDir(E, index, RAND_ARGS);
	float3 velDir = E.velDir 
		+ randDir.x * E.velDirSpreadXZ * fwd
		+ randDir.y * E.velDirSpreadY * up
		+ randDir.z * E.velDirSpreadXZ * side;
	float velL = length(velDir);
	if (velL > 0) velDir /= velL;
	particle.velocity = E.velocity + velDir * lerp(E.speedMin, E.speedMax, RAND);

	// particle.pos = float3(0, 30, 0);
	// particle.color = packColor(float4(1, 0, 0, 1));
	// particle.life = 1;
	// particle.size = 1;
	// particle.stretch = 1;
	// particle.opacity = 1;
	// particle.velocity = 0;
}
