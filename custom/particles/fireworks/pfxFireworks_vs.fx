#include "pfxFireworks.hlsl"

float2 getPosXY(float3 posW){
	float4 v = mul(mul(float4(posW, 1), ksView), ksProjection);
	return v.xy / v.w;
}

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD

	float3 billboardAxis = normalize(particle.pos - ksCameraPosition.xyz);
	#ifdef MODE_SHADOW
		billboardAxis = ksLightDirection.xyz;
	#endif

	float size = particle.size * saturate(particle.lifespan < 0.0001 ? 1 : particle.life * 3) * 2;
	#ifdef MODE_SIMPLE
		size *= 2;
	#elif defined(MODE_PREPASS)
		size *= 3;
	#else
		float3 ssSide = normalize(cross(billboardAxis, billboardAxis.y > 0.5 ? float3(0, 0, 1) : float3(0, 1, 0)));
		float2 dif = abs(getPosXY(particle.pos) - getPosXY(particle.pos + ssSide * 0.01));
		float pixelRadius = 0.01 / max(dif.x, dif.y) * extScreenSize.z;
		size = max(size, pixelRadius);
	#endif
	#ifdef MODE_GBUFFER
		// size *= 1.2;
	#endif

	quadPos *= size;
  float3 side = normalize(cross(billboardAxis, particle.velocity));
  float3 up = normalize(cross(billboardAxis, side));
	float3 offset = side * quadPos.x + up * quadPos.y;

	float4 posW = float4(particle.pos, 1);
	float3 velocity = particle.velocity;
	float3 velocityDir = normalize(velocity);
	float stretchMult = pow(saturate(length(velocity)), 10) * 0.2;
	posW.xyz += offset;
	posW.xyz += particle.stretch * stretchMult * size * velocity * dot(velocityDir, normalize(offset));

	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

	PS_IN vout;
	vout.PosH = posH;
	vout.Tex = BILLBOARD[vertexID].xy;

	float timePassed = particle.lifespan - particle.life;
	float3 color = unpackColor(particle.color).rgb;
	#ifndef MODE_PREPASS
		color = color + dot(color, 0.3);
	#endif
	vout.Color = color * particle.opacity * 4 / sqrt(1 + timePassed * 5);

	#ifdef MODE_PREPASS
		vout.Color *= lightMult;
	#else
		// vout.Color *= extEmissiveMults.x;
	#endif

	vout.Ratio = 0;
	vout.GlowExtra = 0.7 + stretchMult * saturate(-dot(billboardAxis, velocityDir));

	#ifndef MODE_SHADOW
		vout.PosC = posW.xyz - ksCameraPosition.xyz;
		vout.Fog = calculateFog(posV);
		#ifdef MODE_GBUFFER
			GENERIC_PIECE_STATIC(posW);
		#endif
	#endif

	return vout;
}