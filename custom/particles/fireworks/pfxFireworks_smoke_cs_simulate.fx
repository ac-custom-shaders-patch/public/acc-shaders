#include "pfxFireworks_smoke.hlsl"

#define MAX_WIND_SPEED 20
#define WIND_COORDS_MULT 1

float2 windOffset(float3 pos, float windPower, float freqMult){
  #ifdef NO_WIND
    return 0;
  #else
    float waveOffset = saturate(0.1 + extWindSpeed / MAX_WIND_SPEED);
    float wave = sin(extWindWave * 1.773 * freqMult + pos.z * freqMult * WIND_COORDS_MULT / 17) 
      * sin(extWindWave * freqMult + pos.x * freqMult * WIND_COORDS_MULT / 13);
    return extWindVel * windPower * (1 + (wave + waveOffset) * 0.5);
  #endif
}

void forceFields(inout Particle particle) {
  for (uint i = 0; i < FORCE_FIELDS; i++) {
    float3 diff = particle.pos - extForceFields[i].xyz;
		float dist = length(diff);
		float3 dir = diff / dist;
		float strength = saturate(particle.lifePassed * 2) * extForceFields[i].w / pow(max((dist - particle.size) / 10, 1), 2);
		particle.velocity += 2 * dir * strength;
		particle.size = min(particle.targetSize * 2, particle.size + 0.1 * strength);
  }
}

void process(inout Particle particle, uint3 DTid) {
  float drag = 1 / (1 + 3 * gFrameTime);
  float dragSize = 1 / (1 + 0.5 * gFrameTime);
	float bounceK = 1.2;
	if (length(particle.velocity) > 1){
		particle.pos += particle.velocity * gFrameTime;
	}
	particle.size = lerp(particle.targetSize, particle.size, dragSize);
	particle.life -= gFrameTime;
	particle.lifePassed += gFrameTime;

	float3 neutralSpeed = normalize(particle.velocity) * 2;
	neutralSpeed.xz += windOffset(particle.pos, pow(saturate(particle.lifePassed), 2) * sqrt(clamp((particle.pos.y - gGroundEstimate) / 30, 0, 2) * 0.25), 2.5);
	neutralSpeed.y -= 10 * extRainIntensity;

	// particle.size = 3 * clamp((particle.pos.y - gGroundEstimate) / 30, 0, 3);

	particle.velocity = lerp(neutralSpeed, particle.velocity, drag);
	particle.uvOffset -= float2(length(particle.velocity.xz), particle.velocity.y) * gFrameTime * float2(-0.015, 0.015);
	forceFields(particle);
}
