#include "pfxSmoke.hlsl"
#include "include_new/base/_include_ps.fx"
#include "pfxSmoke_draw.hlsl"

Texture2D txSmokeNoise : register(t4);

float4 main(PS_IN_fakeshadow pin) : SV_TARGET {
  float alpha = GAMMA_OR(pow(saturate(pin.Opacity), 0.4545), pin.Opacity) * smoothstep(1, 0, dot2(pin.Tex));
  // alpha = 1;
  clip(alpha - 0.001);
  float3 nothing = 0;
  return float4(nothing, alpha);
}