#include "pfxSmoke.hlsl"
#include "include/packing_cs.hlsl"

// Input buffers
StructuredBuffer<Wheel> wheels : register(t1);       // description of all wheels, with a fake one at the end for smoke without car
StructuredBuffer<Car> cars : register(t2);           // description of all cars, with a fake one at the end
ByteAddressBuffer countingCellsRead : register(t8);  // number of particles and their maximum weight for different cells from previous frame

// Input/output buffers
RWStructuredBuffer<Particle_packed> particleBuffer : register(u0);  // particles
RWStructuredBuffer<uint> aliveBuffer_CURRENT : register(u1);        // currently alive particle IDs
RWStructuredBuffer<uint> aliveBuffer_NEW : register(u2);            // alive particle IDs for the next frame
RWStructuredBuffer<uint> deadBuffer : register(u3);                 // IDs of dead particles
RWByteAddressBuffer counterBuffer : register(u4);                   // buffer with counters, such as number of alive and dead particles

// Output only buffers
RWStructuredBuffer<float> distanceBuffer : register(u6);  // distances to camera are stored here for sorting
RWByteAddressBuffer countingCellsWrite : register(u7);    // number of particles and their maximum weight for different cells

// Applies force to a particle, either from a collider or from a force field
void pushedParticle(inout Particle P, float3 delta){
  P.pos += delta;
  P.velocity += delta * 3;

  float offset = saturate(length(delta));  
  P.size += offset * 0.5;
  P.targetSize += offset * 0.5;
  P.opacityK *= 1 - offset;
  P.thickBase *= 1 - offset;
}

// Checks collision with a few planes described in constant buffer (for smoke to get stuck in tunnels and such)
void planesCollisionsCheck(float3 pos, inout float3 delta, float size){
  for (uint i = 0; i < COLLISION_PLANES; i++) {
    CollisionPlane plane = xCollisionPlanes[i];
    float3 fwd = float3(plane.fwd_x, plane.fwd_y, plane.fwd_z);
    float3 p = pos - plane.pos;
    float d = dot(p, plane.up);
    delta += plane.up * max(size - abs(d), 0) * sign(d) * all(abs(float2(dot(p, fwd), dot(p, plane.side))) < 1);
  }
}

// Checks collision with a car, each car described by three bounding spheres
void spheresCollisionCheck(float3 pos, inout float3 delta, float size){
  for (uint i = 0; i < COLLISION_SPHERES; i++) {
    float3 sphereDif = pos - xCollisionSpheres[i].xyz;
    float dist = length(sphereDif);
    float sphereDist = dist - size;
    if (sphereDist < xCollisionSpheres[i].w) {
      delta += sphereDif * (xCollisionSpheres[i].w - sphereDist) / dist;
    }
  }
}

// Checks collisions with some scene entities
void collisionsCheck(inout Particle P, float pushAwayMult, float collisionMult, float sizeMult){
  float3 delta = 0;

  spheresCollisionCheck(P.pos, delta, P.size * sizeMult);
  delta *= collisionMult;
  
  planesCollisionsCheck(P.pos, delta, P.size * 0.85);

  float3 forceSum = 0;
  float dtMult = gFrameTime * (P_HAS_FLAG(FLAG_COLLISION_DUST) ? 0.3 : lerpInvSat(P.lifeOut, 0.35, 1.35));
  for (uint i = 0; i < FORCE_FIELDS; i++) {
    float3 diff = P.pos - xForceFields[i].pos;
    float dist = dot2(diff) / 4;
    float distK = saturate(1 - dist * xForceFields[i].radiusSqrInv);
    float3 forcePushAway = normalize(diff) * xForceFields[i].forceMult * 2.5;
    float3 forceVelSync = (xForceFields[i].velocity - P.velocity) * abs(xForceFields[i].velocity) * 0.125;
    forceSum += pow(distK, 2) * (forcePushAway + forceVelSync);
  }

  pushedParticle(P, (delta + forceSum * dtMult) * pushAwayMult);
}

// Calculates wind offset used as a wind force, simple method adding a bit of variance
#define MAX_WIND_SPEED 20
#define WIND_COORDS_MULT 1
float2 windOffset(float3 pos, float windPower, float freqMult){
  #ifdef NO_WIND
    return 0;
  #else
    float waveOffset = saturate(0.1 + extWindSpeed / MAX_WIND_SPEED);
    float wave = sin(extWindWave * 1.773 * freqMult + pos.z * freqMult * WIND_COORDS_MULT / 17) 
      * sin(extWindWave * freqMult + pos.x * freqMult * WIND_COORDS_MULT / 13);
    return extWindVel * windPower * (1 + (wave + waveOffset) * 0.5);
  #endif
}

// Calculates cell weight to find the most important large particle per cell and remove the rest
uint getCellWeight(Particle P){
  return P.longLivingIndex;
  // return floor(min(P.lifeOut * 100e3 + P.random * 50e3 + P.wheelID * 10 + P.phase, 4e9));
}

// Main particle update function happens here
[numthreads(THREADCOUNT_SIMULATION, 1, 1)]
void main(uint3 DTid : SV_DispatchThreadID, uint Gid : SV_GroupIndex) {
  uint aliveCount = counterBuffer.Load(PARTICLECOUNTER_OFFSET_ALIVECOUNT);
  GroupMemoryBarrierWithGroupSync();
  if (DTid.x >= aliveCount) {
    return;
  }

  // Loading particle values
  uint particleIndex = aliveBuffer_CURRENT[DTid.x];
  Particle P = PFX_unpack(particleBuffer[particleIndex]);

  // Removing dead particles
  if (P.life <= 0 || gFrameTime >= 1) {
    uint deadIndex;
    counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_DEADCOUNT, 1, deadIndex);
    deadBuffer[deadIndex] = particleIndex;
    return;
  }

  // Accessing state of car and related wheel
  Wheel wheel = wheels[P.wheelID];
  Car car = cars[P.wheelID / 4];

  // Unpacking some special values
  bool snowParticle = P.colorConsistency > 1.01;
  bool longLived = P.targetSize < 0;   // long-living particles for creating a smoked look
  bool veryDense = P.thickBase > 1.5;  // more material particles, more like a scoop of dirt rather than dust, for example
  bool withWheel = wheel.radius >= 0;  // some particles, such as track particles or exhaust/brakes/smoke particles, don’t spawn from wheels
  uint previousCellWeight = getCellWeight(P);

  // Initializing a few extra random values
  float random1 = frac(P.random * 9744.544);
  float random2 = frac(P.random * 6371.672);
  float random3 = frac(P.random * 8768.936);
  float random4 = frac(P.random * 7535.692);
  float random5 = frac(P.random * 7934.438);

  // Applying scene origin offset (gOriginOffset would be non-zero if origin point has changed in last frame)
  P.pos += gOriginOffset;
  P.groundY += gOriginOffset.y;

  // Updating color
  float4 color = unpackColor(P.colorBase);

  // Resetting opacity
  float targetOpacity = longLived ? 1 : color.a;
  float thickTarget = P.thickBase;

  // Life reduction can differ from default gFrameTime
  float lifeReduction = gFrameTime * (P_HAS_FLAG(FLAG_REGULAR_LIFE_REDUCTION) ? 1 : lerp(0, 3, lerpInvSat(P.size, 0, 5)));
    
  if (P_IS_STUCK) {

    // If particle is stuck to a wheel and spinning around with it
    float wheelSpeed = length(wheel.velocity);
    float3 wheelVelDir = wheel.velocity / wheelSpeed;

    // Calculating new on-wheel position, trying to folllow wheel angular speed, but with some inertia
    float stuckRadius = wheel.radius - (WHEEL_HAS_FLAG(FLAG_OPEN_WHEEL) ? 0.01 : P.size * 0.3);
    applyDrag(P.stuckSpeed, wheel.angularSpeed, 1);
    P.stuckSpeed = min(abs(P.stuckSpeed), wheel.stuckMaxSpeed) * sign(P.stuckSpeed);
    P.stuckPos += P.stuckSpeed * gFrameTime;

    // Particle drifts along X axis relative to a wheel
    P.stuckSide += P.stuckDrift * wheel.stuckDriftDtMult;

    // Use on-wheel position to calculate world position and velocity
    float offset = frac((P.stuckPos - P.random * 0.2) / M_TAU);
    float stuckSin, stuckCos;
    sincos(P.stuckPos, stuckSin, stuckCos);
    float3 posBase = wheel.pos - (wheel.up * stuckCos + wheel.dir * stuckSin) * stuckRadius;
    P.pos = posBase + (P.stuckSide - 0.5) * wheel.width * wheel.outside;
    P.velocity = wheel.outside * (min(P.stuckDrift, 2) * wheel.width) * saturate(offset * 20 - 1) * lerpInvSat(wheelSpeed, KMH(2), KMH(12))
      + min(abs(P.stuckSpeed), KMH(60)) * sign(P.stuckSpeed) * (wheel.up * stuckSin - wheel.dir * stuckCos) * wheel.radius;

    // For wheels partially covered by body panels, particles can’t fly off and are less visible
    bool isBlocked = offset > wheel.blockStart && offset < wheel.blockEnd;
    if (isBlocked){
      targetOpacity *= pow(saturate(10 * abs(0.5 - offset) - 0.5), 1);
    }

    // Reduce opacity for slow particles
    float opacityMult = lerp(0.5, 1, lerpInvSat(wheelSpeed, KMH(20), KMH(40))) * lerpInvSat(abs(P.stuckSpeed), 0.1, 1);
    targetOpacity *= opacityMult;

    // Reduce opacity more to avoid that lonely-particles-on-a-wheel effect
    targetOpacity *= 0.4;

    if (wheelSpeed < KMH(1) && abs(P.stuckSpeed) < 0.1
      || CAR_HAS_FLAG(FLAG_CAR_JUMPED)){
      P.life = 0;
    }

    // Life remains the same
    lifeReduction = 0;

    // If wheel has stopped, massively reducing life of particles crushing into ground
    if (abs(wheel.angularSpeed) < 0.5 && (P.stuckSpeed > 0 ? offset : 1 - offset) > 0.9){
      lifeReduction = gFrameTime;
    }

    // How stationary is wheel
    float stationary = lerpInvSat(wheelSpeed, KMH(10), KMH(3));

    // If wheel is locked, particles are smaller
    P.size = min(abs(P.stuckSpeed / 40) + 0.03, P.size);

    // Flying off if particle drifted from tyre or stopped spinning
    bool flewUnder = offset > wheel.flyoffStart && offset < wheel.flyoffEnd 
        && (abs(P.stuckPos) > lerp(1, 10, random4 * saturate(P.stuckSpeed)) * lerp(1, 0.3, max(P.dustMix, P.splashMix)) * lerp(1, 0.5, stationary))
        || snowParticle;
    bool flewFromSide = (P.stuckSide > 1 || WHEEL_HAS_FLAG(FLAG_OPEN_WHEEL) && P.stuckSide < 0) && !isBlocked;
    if (flewFromSide || flewUnder) {
      P.stuckSide = 0;
      P.stuckDrift = 0;  // has a second meaning

      // Stationary burnouts work a bit differently
      P.growK *= lerp(1, -0.5, stationary);
      P.targetSize *= lerp(1, 0.5, stationary);

      // Adding wheel velocity
      P.velocity *= lerp(1, flewUnder ? 1 : 0.2, stationary);
      P.velocity += wheel.velocity * wheel.flyoffSpeedMult; // To kick smoke back as it flies away, let’s decrease wheel velocity

      // Reducing vertical speed for regular smoke
      P.velocity -= dot(P.velocity, wheel.up) * wheel.up * lerp(0.8 + 0.2 * max(P.dustMix, P.splashMix), 0, stationary);
      
      // Random offset to make sure smoke wouldn’t be arranged evenly
      P.pos -= P.velocity * gFrameTime * random3;

      // For slower particles, adjusting opacity and visual
      float slowMult = lerpInvSat(abs(P.stuckSpeed), 0, 2);
			P.opacityK *= opacityMult * slowMult;
			P.targetSize *= slowMult;
      P.thickBase *= slowMult;

      // If flew from below car, reduce car collisions
      if (flewUnder){
        P._flags |= FLAG_REDUCED_COLLISIONS;
      }

      // For debugging:
      // P.life = 0;
      // P.colorBase = packColor(float4(stationary, 1 - stationary, 0, 1));
    }

  } else if (P.stuckBelow > 0) {

    // If particle is stuck below car, here is simple movement and tracking if it’s still stuck or has left
    P.pos += P.velocity * gFrameTime;
    P.stuckBelow -= gFrameTime;
    P.size += (lerp(0.35, 0.45, random5) - P.size) * gFrameTime * 5;  // simple growth
    P.pos.y = car.shadowCenter.y;                                       // making sure to align particle
    P.groundY = min(P.groundY, car.shadowCenter.y);                   // if car moves down, update ground Y
    applyDrag(P.velocity, 0, 2);

    // Limit velocity relative to car to reduce particles flying off too fast if car suddenly changed its speed
    float3 relativeVelocity = P.velocity - car.carVelocity;
    float currentSpeed = length(relativeVelocity);
    float maxSpeed = lerpInvSat(car.carSpeed, KMH(40), KMH(100)) * 40;
    if (currentSpeed > maxSpeed){
      P.velocity = car.carVelocity + relativeVelocity * maxSpeed / currentSpeed;
    }

    // Limit opacity if car slows down
    P.opacityK = min(P.opacityK, pow(lerpInvSat(car.carSpeed, KMH(40), KMH(100)), 2));
    if (!P.opacityK){
      P.life = 0;
    }

    // Condition to release stuck particle if it leaves boundaries
    float3 relativeToCar = P.pos - car.shadowCenter;
    float2 carOffset = abs(float2(dot(relativeToCar, car.shadowVecH), dot(relativeToCar, car.shadowVecV)));
    if (P.stuckBelow < 0.99 && max(carOffset.x, carOffset.y) > 1){
      P.stuckBelow = 0;
      P.velocity += wheel.up * car.carSpeed * lerp(0.03, 0.12, random4);
      P.velocity += wheel.dir * car.carSpeed * dot(relativeToCar, car.shadowVecV) * 0.5;
    }
    if (CAR_HAS_FLAG(FLAG_CAR_DISALLOW_STUCK)){
      P.stuckBelow = 0;
    }

  } else if (veryDense) {

    // Very dense particles just arc with gravity
    P.pos += P.velocity * gFrameTime;
    P.velocity.y -= gGravity * gFrameTime;
    P.size += saturate(1 - P.size / abs(P.targetSize)) * gFrameTime;
    applyDrag(P.velocity, 0, 0.1);

  } else {

    // Particle is outside and free
    P.lifeOut += gFrameTime;

    // Keep rotating
    P.stuckPos += P.stuckSpeed * gFrameTime;
    applyDrag(P.stuckSpeed, 0, 5);

    // Limiting maximum particle speed
    float speed = length(P.velocity);
    if (speed > KMH(100)) {
      P.velocity *= KMH(100) / speed;
      speed = KMH(100);
    }

    if (!longLived) {
      P.pos += P.velocity * gFrameTime;

      // How far particle is from ground, from 0 to 1
      float hoverK = lerpInvSat(P.pos.y - P.groundY, 0, P.size);
      float targetVelocityY = P._targetVelocityY;
      float windMult = 0.5; /* unfair? */

      if (snowParticle) {
        hoverK = 0.5 + 0.5 * hoverK;
        targetVelocityY = -0.8;
        windMult = 1;
      } else {
        // Making sure particle wouldn’t fall through the ground
        P.pos.y = max(P.pos.y, P.groundY);
      }

      // For debugging:
      // color.rgb = float3(hoverK, 1 - hoverK, 0);

      // Updating particle size for it to slowly grow
      float targetSize = abs(P.targetSize);
      targetSize *= lerp(0.6, 1, hoverK);
      targetSize *= 1 + lerp(0.1, 0, P.dustMix) * P.lifeOut / (1 + 0.1 * P.lifeOut);

      float growSpeed = saturate(1 - P.size / targetSize);
      growSpeed *= lerp(0.5, 1, P.growK);                                 // taking into account grow parameter
      growSpeed *= 1 + 0.5 * pow(lerpInvSat(P.size, 1, 0.2), 2)           // faster speed for smaller particles,
        * saturate(P.lifeOut * 4);                                        //   only if they were outside long enough      
      if (!P.dustMix) {
        growSpeed *= lerp(0.5, 1, max(lerpInvSat(speed, KMH(20), KMH(40)),  // lower grow rate for burnouts in place, regular for fast drifting
          saturate(P.lifeOut * 2)));                                        //   applies to newly released particles only
        growSpeed *= lerp(0.2, 1, lerpInvSat(speed, KMH(0), KMH(5)));       // reduces grow rate for very slow particles
      } 
      growSpeed *= lerp(1, 1.5, pow(random3, 8));                         // randomly boost grow rate for increased variety
      growSpeed *= lerp(1, 2.5, P.splashMix);                             // wet particles grow faster
      P.size += growSpeed * gFrameTime;
      // P.size += 2 * gFrameTime;

      // Adding some drag to particle velocity, reducing it to neutral velocity
      float3 neutralVelocity = P.velocity;                      // take current velocity
      float maxNeutralSpeed = lerp(KMH(0.25), KMH(4), hoverK);  // and limit it depending on distance to ground
      if (length(neutralVelocity) > maxNeutralSpeed) {
        neutralVelocity *= maxNeutralSpeed / length(neutralVelocity);
      }
      // neutralVelocity = 0;
      neutralVelocity.xz += windOffset(P.pos, lerp(0.2, 1, hoverK), 2.5) * windMult * saturate(P.lifeOut / 2);
      neutralVelocity.y = targetVelocityY + lerp(0, 0.5, hoverK * saturate(1 - hoverK));
      applyDrag(P.velocity, neutralVelocity, lerp(4, 2, hoverK));

      // Faster wind means smoke dissipates faster
      lifeReduction *= max(1, length(neutralVelocity) * 0.8);
      
      // Force fields from other cars, collisions with other cars and surfaces
      float pushAwayLife = P.lifeOut * 2;
      float pushAwayMult = (pushAwayLife / (0.5 + pushAwayLife)) * pow(lerpInvSat(P.size, 1.5, 0.3), 2) * gFrameTime * 50;
      float collisionMult = 1;
      float sizeMult = 0.5;
      if (P_HAS_FLAG(FLAG_COLLISION_DUST)){
        sizeMult = -1;
        float planeDistance = distanceToPlane(car.collisionPlane, P.pos) - P.size * 0.5 - random5;
        if (planeDistance < 0){
          float planeMult = lerpInvSat(P.lifeOut * (1 + random2), 1, 0.2);
          P.pos -= planeDistance * car.collisionPlane.xyz * planeMult;
          P.velocity -= planeDistance * car.collisionPlane.xyz * planeMult;
        }
      } else if (P_HAS_FLAG(FLAG_REDUCED_COLLISIONS | FLAG_NO_COLLISIONS)){
        collisionMult = P_HAS_FLAG(FLAG_REDUCED_COLLISIONS) ? 0.01 : 0;
      }
      collisionsCheck(P, pushAwayMult, collisionMult, sizeMult);

      if (withWheel && !snowParticle) {   
        // For particles spawned from cars, adding force sucking particles behind cars and pushing them up
        float3 suckDir = normalize(cross(car.carVelocity, wheel.up));  // to the side of car velocity

        float dragMult = WHEEL_HAS_FLAG(FLAG_OPEN_WHEEL) ? 2 : 1;
        car.drivenDistance *= dragMult;

        float suckSpeedMult = pow(lerpInvSat(car.carSpeed, KMH(0), KMH(200)), 2);
        float offsetWave = sin(car.drivenDistance * 0.21) * sin(car.drivenDistance * 0.13);
        float3 suckPivot = wheel.axisCenter + suckDir * offsetWave * suckSpeedMult * 0.7;

        float3 relPos = P.pos - suckPivot;                      // position relative to axle center
        float3 relAltPos = relPos + P.velocity * 0.1;           // position relative to axle center
        float suck = pow(lerpInvSat(length(relPos), dragMult * 10, 1), 2) // force reducing with distance
          * suckSpeedMult                                       // taking into account speed
          * lerp(1, 0.2, pow(random2, 2)) 
          * lerp(0.2, 1, P.splashMix)
          * dot(normalize(relAltPos), suckDir)
          * (P_HAS_FLAG(FLAG_REDUCED_FORCES) ? 0.1 : 1)
          * (1 - car.angularSpeedK)
          * gFrameTime;
        P.velocity -= suckDir * suck * dragMult * 1600;
        P.velocity += car.carVelocity / car.carSpeed * abs(suck) * 800;
        P.velocity += wheel.up * abs(suck) * (P_HAS_FLAG(FLAG_REDUCED_COLLISIONS) ? 100 : 800) * lerp(0, 1, pow(random5, 40));
      }
    } else {
      // Fully saturated cell would remain like that for 5 minutes in windless conditions
      P.life = saturate(P.life);
      lifeReduction = gFrameTime / (lerp(5, 1, saturate(extWindSpeed / MAX_WIND_SPEED)) * 60);
      
      // Boosting life reduction if there are too many of those particles
      uint longLivingIndex;
      counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_LONGLIVING_TOTAL, 1, longLivingIndex);
      if (longLivingIndex > 600){
        lifeReduction *= 20;
      }

      // Extra UV animation
      P.uvBaseOffset.y += 0.001 * gFrameTime;
    }

    // Whole thing with cells, grid is 64×64 cells
    int cellId = getCellId(P);
    if (isCellIdValid(cellId)) {
      // Each cell is two uints, one for counting total number of particles, another to figuring out which
      // one of long-lived particles is to keep
      const uint keyLongLived = 1024 * 64;
      const float keyLifeMultiplier = 1000;

      uint3 cellValues = countingCellsRead.Load3(cellId);
      float cellCountLarge = (float)(cellValues.x / keyLongLived);
      float cellCountSmall = (float)(cellValues.x % keyLongLived);
      uint longLivedAmount = (uint)floor(keyLifeMultiplier * P.life);

      if (!longLived || P.targetSize == -1) {
        // Incrementing particles counter if it’s a regular one or a long-lived particle without probation
        countingCellsWrite.InterlockedAdd(cellId, longLived ? keyLongLived * longLivedAmount : 1);
      }

      if (longLived) {
        // Long-lived particles need to ensure only a single one per cell will exist
        countingCellsWrite.InterlockedMax(cellId + 4, getCellWeight(P));

        // Write color with life in alpha channel, deliberately non-locking, hoping to get random particle to leave the color
        if (P.life < 0.2 && frac(P.pos.x * 484.8933) > 0.8) {  // only for newly born particles
          countingCellsWrite.Store(cellId + 8, packColor(float4(color.rgb, lerp(0.005, 1, P.life))));
        }

        if (cellValues.y == previousCellWeight) {
          // This is the main particle, adding rest of cells here
          P.targetSize = -1;

          float4 otherColor = unpackColor(cellValues.z);
          if (otherColor.a > 0 && otherColor.a < P.life * 0.8) {
            color.rgb = (color.rgb * P.life + otherColor.rgb * otherColor.a) / (P.life + otherColor.a);
            P.colorBase = packColor(color);
          }
          P.life = max(P.life, cellCountLarge / keyLifeMultiplier);
        } else if (--P.targetSize < -4){
          // Removing extras after a few failed attempts to register as main particle (whole thing with
          // these cells is that for speeding things up data might not always work well, for example if camera
          // has been moved from one cell to another)
          P.life = 0;
        }
      } else {
        // Regular particles use cells to hide if there are too many neighbours
        float distanceToCamera = length(P.pos - gCameraPosition);
        float particlesLimit = lerp(500, 300, P.dustMix)
          * lerp(1, 0.7, lerpInvSat(P.size, 0.3, 1))
          * lerp(1, 2, lerpInvSat(distanceToCamera, 50, 300))  // slightly increasing limit for particles far away
          * lerp(1, 0.2, lerpInvSat(distanceToCamera, 10, 1))  // massively reducing limit for particles nearby
          * gQuantityScale;

        // A bit of a smoothing logic to make sure camera movements wouldn’t cause any jumps
        float previousCellCount = abs(P.cellCount);
        bool minorChange = abs(cellCountSmall - previousCellCount) < 20;
        bool interpolationActive = P.cellCount < 0;
        if (minorChange || !P.cellCount){
          P.cellCount = cellCountSmall;
        } else if (interpolationActive) {
          P.cellCount = -lerp(previousCellCount, cellCountSmall, 0.2);
        } else {
          P.cellCount = -P.cellCount;
        }

        // Calculating how many particles we can display and either hiding or boosting opacity to compensate
        float visibleShare = saturate(particlesLimit / abs(P.cellCount));
        if (random1 > visibleShare){
          targetOpacity *= lerpInvSat(random1, visibleShare + 0.1, visibleShare);
        } else {
          targetOpacity /= lerp(1, visibleShare, lerpInvSat(P.size, 0.3, 0.7) * 0.8); 
        }

        // Also, reducing particle thickness if there are not enough cells nearby
        thickTarget *= lerpInvSat(abs(P.cellCount), 20, 60);
      }
    }
  }

  if (snowParticle) {
    /* snow particle */
    // color.rgb = float3(1, 0, 0);
  } else if (!veryDense && !longLived){
    // Blending color to bright gray if particle is large
    color.rgb = lerp(0.7, color.rgb, 1 / (1 + max(0, P.size - 0.2) * lerp(1, 0, P.colorConsistency)));
    // if (P.colorConsistency > 0.99) color.rgb = float3(1, 0, 0);
    // else color.rgb = float3(0, 1, 0);
  }

  // Updating remaining time
  P.life -= lifeReduction;

  // Smoothly altering thickness
  thickTarget /= max(1, P.size * 2);
  applyDrag(P.thickFinal, thickTarget, 4);

  // Calculating distance to camera and angle for main UV offset
  float3 eyeVector = P.pos - gCameraPosition;
  float distanceToCamera = length(eyeVector);
  float angle = atan2(eyeVector.x, eyeVector.z);

  // Calculating new opacity
  targetOpacity *= saturate(P.life * P.opacityK);

  if (longLived && targetOpacity > abs(P.opacityS)) {
    P.opacityS = abs(P.opacityS);
    applyDrag(P.opacityS, saturate(targetOpacity), 0.4);
  } else {
    if (!longLived) {
      float opacitySizeFactor = 0.5 + P.dustMix;
      targetOpacity *= lerp(1, 2, P.thickFinal);                // increasing opacity for thick particles
      targetOpacity /= 1 + P.size * opacitySizeFactor;          // reducing opacity for large particles
      targetOpacity *= lerpInvSat(distanceToCamera, 1800, 1200);  // fading out with distance

      if (P_HAS_FLAG(FLAG_FADE_IN)) {
        targetOpacity *= saturate(P.lifeOut * (P_HAS_FLAG(FLAG_SLOWER_FADE_IN) ? 1 : 10));
      }

      targetOpacity *= lerp(1, veryDense ? 0.3 : 0.4, P.splashMix);
    }
    P.opacityS = saturate(targetOpacity);
  }

  // Marking frustum culled particle with negative value
  P.opacityS = abs(P.opacityS) * (isVisible(P.pos, P.size) ? 1 : -1);

  // Packing color back
  if (longLived) {
    float3 targetColor = color.rgb;
    color.rgb = unpackColor(P.color).rgb;
    applyDrag(color.rgb, targetColor, 1);
  }
  P.color = packColor(color);

  // Camera occlusion
  float cameraCoverSize = longLived ? P.size : P.size * 1.4;
  [branch]
  if (gCoverCamera && distanceToCamera < cameraCoverSize && cameraCoverSize > 0.5 && P.lifeOut > 0.5) {
    float mult = lerpInvSat(distanceToCamera, cameraCoverSize, cameraCoverSize * lerp(0.3, 0.7, P.thickFinal))
      * lerpInvSat(cameraCoverSize, 0.5, 1) * lerpInvSat(P.lifeOut, 0.5, 1);
    uint4 occlusion = uint4(float4(color.rgb, abs(P.opacityS) * (longLived ? 0.3 : 1) * lerp(1, 0.1, P.splashMix)) * mult * float4(64, 64, 64, 256));
    counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_CAMERA_OCCLUSION_RG, (occlusion.r << 16) | occlusion.g);
    counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_CAMERA_OCCLUSION_BA, (occlusion.b << 16) | occlusion.a);
  }

  // Moving UV offset based on particle speed relative to camera
  float3 screenVelocity = mul(longLived ? float3(extWindVel.x, 0, extWindVel.y) * 0.1 : P.velocity, (float3x3)xVP);
  if (veryDense) screenVelocity *= 2;
  // if (snowParticle) screenVelocity *= 0.1;
  P.uvBaseOffset.xy += screenVelocity.xy * float2(0.2, 0.1) * (2 / (1 + saturate(P.size))) * gFrameTime * gFovTan;
  P.uvBaseOffset.y += gFrameTime * (veryDense ? 1 : 0.02);

  // Simple angles-based offset: billboards don’t look so billboardy if they change when rotating
  P.uvOffset = (longLived ? 1 : 1) * float2(angle / (M_PI * 2), acos(-(eyeVector / distanceToCamera).y) / 4) + P.uvBaseOffset;
  P.uvOffset *= lerp(1, 0.2, P.splashMix);

  // Sorting distance can be altered if needed, for example taking particle size into account
  float sortingDistance = distanceToCamera + random1 * 0.05;
  distanceBuffer[particleIndex] = 1 / sortingDistance; // something to make sure precision issues wouldn’t mess up sorting
  particleBuffer[particleIndex] = PFX_pack(P);

  // Storing particle as a new alive one
  uint newAliveIndex;
  counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_ALIVECOUNT_AFTERSIMULATION, 1, newAliveIndex);
  aliveBuffer_NEW[newAliveIndex] = particleIndex;
}
