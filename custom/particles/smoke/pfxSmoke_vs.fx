#include "pfxSmoke.hlsl"
#include "include/packing_cs.hlsl"
#include "include_new/base/_include_vs.fx"
#include "include_new/ext_shadows/_include_vs.fx"

#define AO_EXTRA_LIGHTING 1
#define GI_LIGHTING 0
#define LIGHTINGFX_KSDIFFUSE 1
#define GAMMA_LIGHTINGFX_KSDIFFUSE_ONE
#define LIGHTINGFX_NOSPECULAR
#define LIGHTINGFX_SIMPLEST
#ifndef SHADER_MIRROR
	#define LIGHTINGFX_FIND_MAIN
#endif
#define POS_CAMERA posCHalf
#define POS_TOCAMERA normalize(POS_CAMERA)
#include "include_new/base/common_ps.fx"
#include "include_new/ext_lightingfx/_include_ps.fx"

#include "pfxSmoke_draw.hlsl"

StructuredBuffer<Particle_packed> particleBuffer : register(t0);
StructuredBuffer<uint> aliveList : register(t1);
StructuredBuffer<Car> cars : register(t2);
Texture2D<float> txTiles : register(t3);

#if defined(SHADER_SPLASHES)
	cbuffer cbSplashesData : register(b3) {
		float4x4 gCarTransformInv;
		float3 gCarVelocity;
		float _pad0;
	}
#else
	float4 getPosH(float3 pos){
		float4 posW = float4(pos, 1);
		float4 posV = mul(posW, ksView);
		float4 posH = mul(posV, ksProjection);
		return posH;
	}

	float3 getPosHDiv(float3 pos){
		float4 posH = getPosH(pos);
		return posH.xyz / abs(posH.w);
	}

	float tileTestScreen(float2 posH, float distanceToCamera){
		float tileValue = txTiles.SampleLevel(samLinearClamp, posH * float2(0.5, -0.5) + float2(0.5, -0.5), 0);
		float decodedDistance = (1 - tileValue) * 600;
		return lerpInvSat(distanceToCamera * lerp(0.6, 1, lerpInvSat(ksFOV, 0, 30)), decodedDistance * 0.7, decodedDistance * 0.3);
	}

	float shadowContribution(float3 pos, float3 posC){	
		#ifdef NO_SHADOWS
			return 1;
		#else
			float4 p = float4(pos, 1);
			return getShadowBiasMult(posC, 0, float3(0, 1, 0), mul(p, ksShadowMatrix0), 0, 1);	
		#endif
	}
#endif

#if defined(SHADER_TILE)
	#define PS_IN_type PS_IN_tile
#elif defined(SHADER_SPLASHES)
	#define PS_IN_type PS_IN_splashes
#else
	#define PS_IN_type PS_IN
#endif

PS_IN_type main(uint fakeIndex : SV_VERTEXID) {
  // Loading particle
	uint vertexID = fakeIndex % 6;
	uint instanceID = fakeIndex / 6;
	Particle P = PFX_unpack(particleBuffer[aliveList[instanceID]]);

	if (extScreenMode == 3) {
		P.opacityS = abs(P.opacityS);
	}

	// Some randomized values (TODO: is it a good method?)
	float random1 = frac(P.random * 4523.114);
	float random2 = frac(P.random * 6371.671);
	float random3 = frac(P.random * 5675.626);
	float random4 = frac(P.random * 1965.766);

	// Reducing size for newly created particles stuck on wheels
	float realSize = P.size;
	if (P_IS_STUCK){
		P.size *= lerp(0.1, 1, saturate(abs(P.stuckPos) * 2));
	}

  // Unpacking some special values
  bool longLived = P.targetSize < 0; // long-living particles for creating a smoked look
	bool veryDense = P.thickBase > 1.5;

	#if defined(GBUFFER_NORMALS_REFLECTION)
		#error not supported
	#endif

	#if defined(SHADER_SPLASHES)
		if (P.splashMix < 0.1 || P.lifeOut < 0.3){
			DISCARD_VERTEX(PS_IN_type);			
		}
	#endif

	// Main vectors and distance to camera
	float3 posC = P.pos - ksCameraPosition.xyz;
	float distanceToCamera = length(posC);
	float3 fromCamera = posC / distanceToCamera;
  float3 side = normalize(cross(fromCamera, float3(0, 1, 0)));
  float3 up = normalize(cross(fromCamera, side));

	if (longLived){
		P.size = min(P.size, sqrt(distanceToCamera));
	}

	// Stretching particle horizontally if looked from side, for more grounded shape
	float horizontalStretch = longLived ? 1.8 : 1.2;
	float lookedFromAbove = abs(fromCamera.y) * saturate(distanceToCamera / 40 - 0.2) * lerpInvSat(P.size, 0.5, 3);

	// Corner offset to make a billboard
	float2 baseOffset = P.size * lerp(float2(horizontalStretch, 1), lerp(horizontalStretch, 1, 0.5), lookedFromAbove);
	float2 signedOffset = BILLBOARD[vertexID] * baseOffset;
	float3 offset = side * signedOffset.x - up * signedOffset.y;

	// Tile-based culling
	#if !defined(SHADER_SPLASHES)
		float3 cornerOffset = side * baseOffset.x + up * baseOffset.y;
		float3 corner1 = P.pos - cornerOffset;
		float3 corner2 = P.pos + cornerOffset;
		float2 posUH1 = getPosHDiv(corner1).xy;
		float2 posUH2 = getPosHDiv(corner2).xy;
		#if defined(SHADER_MIRROR)
			posUH1.xy *= float2(0.24, 0.06);
			posUH2.xy *= float2(0.24, 0.06);
		#endif

		float2 sizeUH = abs(posUH2 - posUH1);
		float2 posCH1 = clamp(posUH1, -1, 1);
		float2 posCH2 = clamp(posUH2, -1, 1);
		float2 sizeCH = abs(posCH2 - posCH1);

		float onScreenArea = sizeCH.x * sizeCH.y;
		float maxOnScreenArea = lerp(1, 1.8, lerpInvSat(ksFOV, 0, 50))
			* gSizeLimitScale
			* lerp(0.6, 0.8, P.opacityS)
			* lerp(1, 4, pow(random3, 40))
			* lerp(0.5, 2, lerpInvSat(abs(P.cellCount), 600, 0));

		#if defined(SHADER_TILE)
			maxOnScreenArea *= 0.6;
		#endif

		if (onScreenArea > maxOnScreenArea){
			P.opacityS *= lerpInvSat(onScreenArea, maxOnScreenArea * 1.4, maxOnScreenArea);
		}

		#if !defined(SHADER_TILE) && !defined(SHADER_MIRROR) 
			if (distanceToCamera < 400) {
				float2 maxPosCH = max(abs(posCH1), abs(posCH2));
				float posHMult = 1 / max(1, max(maxPosCH.x, maxPosCH.y));
				float tileK = tileTestScreen((posUH1 + posUH2) * posHMult / 2, distanceToCamera);
				P.opacityS *= max(tileK, lerpInvSat(distanceToCamera, 350, 400));
			}
		#endif
	#endif

	// Remove nearby particles to save on FPS and avoid clipping glitches
	#if !defined(SHADER_SPLASHES)
		if (useNearbyInteriorClipping){
			P.opacityS *= saturate(remap(distanceToCamera, 0.5, 1, 0, 1));
		}
	#endif

	#if defined(SHADER_MIRROR)
		P.opacityS = abs(P.opacityS); // disabling frustum culling
		P.opacityS *= !longLived;
		P.opacityS *= lerp(1, 0.5, P.splashMix);
	#endif

	// Long-lived particles do not affect tile culling
	#if defined(SHADER_TILE)
		maxOnScreenArea *= 0.6;
		P.opacityS *= !longLived;
	#endif

	// Starting to prepare the output
	PS_IN_type vout;
	vout.Opacity = P.opacityS;

	// Temporary:
	P.opacityS *= !longLived;

	// Additional hiding of particles nearby to help with performance and reduce camera occlusion
	#if !defined(SHADER_SPLASHES) 
		float localReductionBase = longLived ? 20 : P.size * lerp(0.3, 1, max(P.splashMix, P.dustMix * 0.3)) * 4;
		#ifdef SHADER_MIRROR
			localReductionBase *= 0.5;
		#else
			if (random4 < 0.1){
				localReductionBase *= 0.5;
			}
		#endif
		vout.Opacity *= lerpInvSat(distanceToCamera, localReductionBase, localReductionBase * 3);
	#endif

	// Discarding certain particles completely (TODO: wouldn’t clipping be faster?)
	#if defined(SHADER_MIRROR)
		[branch]
		if (vout.Opacity < 0.015){
			DISCARD_VERTEX(PS_IN_type);
		}
	#elif defined(SHADER_SPLASHES)
		[branch]
		if (vout.Opacity < 0.3){
			DISCARD_VERTEX(PS_IN_type);
		}
		vout.Opacity *= 10;
	#else
		[branch]
		if (vout.Opacity < 0.015){
			DISCARD_VERTEX(PS_IN_type);
		}
	#endif

	// Filling out generic properties
	vout.Tex = BILLBOARD[vertexID];

	#if !defined(SHADER_TILE) && !defined(SHADER_SPLASHES)
		float angle = P.stuckPos * ((P.wheelID & 1) ? 0.2 : -0.2);
		float s = sin(angle);
		float c = cos(angle);	
		vout.TexUV = mul(float2x2(c, -s, s, c), vout.Tex) * float2(1, -1);
	#endif

	#if !defined(SHADER_TILE) && !defined(SHADER_SPLASHES)
		vout.Movement = P.uvOffset;
		vout._Size = longLived ? -P.size : P.size;
		vout._DustSplashMix = P.splashMix > 0 ? -P.splashMix : P.dustMix;
	#endif

	#if !defined(SHADER_SPLASHES)
		// Stretching particle based on its velocity
		float3 velocity = P.velocity - gCameraVelocity * 0.5;
		velocity = normalize(velocity) * min(length(velocity), KMH(40));
		if (veryDense) {
			velocity *= 0.4;
			#if !defined(SHADER_TILE)
				vout.Movement = 0;
			#endif
		} else {
			// Moving particle upwards to a bit of its size
			if (P.growK >= 0){
				float yOffset = max(P.size * lerp(0.33, 0.67, random1) - 0.15 * lerpInvSat(P.lifeOut, 0.2, 0), 0);
				P.pos.y += yOffset / (yOffset + 1);
			}

			velocity *= (P.growK < 0 ? 0.5 : 1) * lerp(0.2, 0.1, P.dustMix) * (P_HAS_FLAG(FLAG_REDUCED_MOTION) ? 0.1 : 1);
		}
		// offset *= 1 + clamp(dot(normalize(velocity), normalize(offset)), -1, 1) * 0.5;
		offset += velocity * realSize * dot(normalize(velocity), normalize(offset)) * (veryDense ? 1 : saturate(P.lifeOut * 2) * 0.5);
	#endif

	// Moving particles towards camera for better soft particles blending
	float cameraOffset = -P.size * saturate(0.5 * distanceToCamera / P.size - 1);
	if (P_IS_STUCK) {
		float stuckPos = saturate(-cos(P.stuckPos));
		cameraOffset *= 1 - pow(stuckPos, 2); 
	}

	// Move small particles even closer
	cameraOffset -= 0.08 * lerpInvSat(P.size, 0.4, 0.25);

	if (cars[P.wheelID / 4].carWidth == 0) {
		cameraOffset = -0.02;
	}

	// Calculating on-screen position with a bit of an offset towards camera
	float4 posW = float4(P.pos + offset, 1);	
	// posW.y += frac(unpackColor(P.color).r * 4857.562) * lerp(-0.1, 0.1, P.size); // for debugging
	// float4 posV = mul(posW, ksView) - float4(0, 0, cameraOffset, 0);
	float4 posV = mul(posW + float4(normalize(SPS_CAMERA_POS - posW.xyz) * -cameraOffset, 0), ksView);
	vout.PosH = mul(posV, ksProjection);
	#if defined(SHADER_TILE)
		vout.DistanceToCamera = distanceToCamera;
	#elif !defined(SHADER_SPLASHES)
  	vout.PosC = P.pos + offset - ksCameraPosition.xyz;
	#endif

	// Extra shading values
	#if !defined(SHADER_TILE) && !defined(SHADER_SPLASHES)
		vout.Color = P.color;
 		vout._Fog = calculateFogNew(vout.PosC);

		float hugeK = pow(lerpInvSat(P.size, 1, 6), 2);
		float largeK = lerpInvSat(P.size, 0.5, 1);
		float smallK = lerpInvSat(P.size, 0.5, 0.3);
		float tinyK = lerpInvSat(P.size, 0.3, 0.1);
		float pThickness = longLived ? 0 : veryDense ? 2 : saturate(P.thickFinal + (1 - P.growK) * 0.3);
		// vout.Opacity *= lerp(1, 0.5, P.splashMix * smallK);
		vout._AlphaOffset = lerp(0.4, 0.8, max(smallK, hugeK * pThickness)) * lerp(1, 2, largeK * P.splashMix) + saturate(-P.growK) * tinyK * 0.5;
		vout._AlphaExp = veryDense ? -0.8 : lerp(lerp(1, 0.7, pThickness) * (1 - smallK * 0.4), 1, P.splashMix);
		vout._AlphaExp *= lerp(1, 0.9, P.dustMix * (1 - largeK));
		// vout._AlphaOffset = 0;
		if (P.colorConsistency > 1.1) {
			vout._AlphaOffset = -vout._AlphaOffset * 0.8;
			if (P.colorConsistency < 1.5) vout.Opacity *= 0.4;
			// vout._AlphaExp *= 2;
		}
	#endif

	// Calculating sun shadows and dynamic lighting
	#if !defined(GBUFFER_NORMALS_REFLECTION) && !defined(SHADER_TILE) && !defined(SHADER_SPLASHES)
		// Using a point midway from particle center to its corner for smoother results
		float4 posWHalf = float4(P.pos + offset * 0.5, 1);
		#if defined(USE_SIMPLIFIED_SHADOWS)
			// Smoother shadows done by sampling several points for each corner
			float3 posWOffset = P.pos - fromCamera * P.size;
			vout._Shadow = shadowContribution(posWOffset + offset * 0.8, posC);
		#elif !defined(NO_SHADOWS)
			// Smoother shadows done by sampling several points for each corner
			float3 posWOffset = P.pos - fromCamera * P.size;
			vout._Shadow = (shadowContribution(posWOffset + offset * 0.5, posC)
				+ shadowContribution(posWOffset + offset * 0.6, posC)
				+ shadowContribution(posWOffset + offset * 0.7, posC)
				+ shadowContribution(posWOffset + offset * 0.8, posC)) / 4;
		#else
			vout._Shadow = 0.7;
		#endif
		vout._Shadow *= lerp(1, 0.3, P.splashMix);

		float3 posCHalf = posWHalf.xyz - ksCameraPosition.xyz;
		LFX_MainLight mainLight;
		vout._Lighting = 0;
		LIGHTINGFX(vout._Lighting);
		#ifndef SHADER_MIRROR
			float lightingFocus = saturate(2 * mainLight.power / max(dot(vout._Lighting, 1), 1));
			vout._LightingDir = mainLight.dir * lightingFocus;
		#endif
		if (longLived) {
			vout._Lighting *= 0.15;
		}

		// For debugging:
		// vout.Lighting.rgb = float3(P._pad1, 1 - P._pad1, 0);
	#endif

	#if !defined(SHADER_MIRROR) && !defined(GBUFFER_NORMALS_REFLECTION) && !defined(SHADER_TILE) && !defined(SHADER_SPLASHES)
		Car car = cars[P.wheelID / 4];

		if ( P_HAS_FLAG(FLAG_GROUND_SPAWN)) {
			// Fading ground-spawned particles under car to hide them from interiors
			float3 relativeToCar = P.pos - car.shadowCenter;
			float2 carOffset = abs(float2(dot(relativeToCar, car.shadowVecH), dot(relativeToCar, car.shadowVecV)));
			float relOffset = max(carOffset.x, carOffset.y);
			vout.Opacity *= lerpInvSat(relOffset, 0.9, 1.1);
		}

		float3 relativeToCar = posWHalf.xyz - car.shadowCenter;
		float2 carOffset = abs(float2(dot(relativeToCar, car.shadowVecH), dot(relativeToCar, car.shadowVecV)));
		float occlusion = lerp(car.shadowOpacity, 1, 
			saturate(lerpInvSat(max(carOffset.x, carOffset.y), 0.75, 1.05) + saturate(relativeToCar.y)));
		occlusion = lerp(occlusion, 1, lerpInvSat(posWHalf.y - P.groundY, 0.25, 0.75));

		float stuckPos = 0.5 - 0.5 * cos(P.stuckPos);                 // 1 at the top of the wheel, 0 at the bottom
		float stuckK = lerpInvSat(P.lifeOut, 0.025, 0);               // 1 for particles that are stuck to the wheel
		float stuckOcclusion = lerp(1, occlusion * stuckPos, stuckK * 0.5);  // darkens particles at the bottom of the wheel

		vout._Occlusion = occlusion * stuckOcclusion;  // affects ambient and diffuse
		vout._Lighting *= vout._Occlusion;
		vout._Depth = -posV.z;

		if (P_HAS_FLAG(FLAG_STUCK_SHADOWED)) {
			vout._Shadow *= lerpInvSat(stuckOcclusion, 0.4, 0.8);
		}

		if (useNearbyInteriorClipping) {
			vout._Depth = max(vout._Depth, 2);
		}

		// Debugging
		// vout._Pad.x = frac(abs(P.opacityS) * 10);
		// vout._Pad.x = saturate(P.life);
	#endif
	
  #ifdef SHADER_SOLID
		vout.Offset2 = uint2(P.random * 6, frac(P.life * 100) * 6);
	#endif
	
  #ifdef SHADER_SPLASHES
		vout.PosH.z = lerp(vout.PosH.z, vout.PosH.z * 0.1 + 0.01, lerpInvSat(P.size, 0.3, 1.2));
		vout.Velocity = (mul(P.velocity, (float3x3)gCarTransformInv) + gCarVelocity) * 0.05;
	#endif

	return vout;
}