#include "pfxSmoke.hlsl"
#include "include_new/base/_include_vs.fx"
#include "pfxSmoke_draw.hlsl"

StructuredBuffer<Particle_packed> particleBuffer : register(t0);
StructuredBuffer<uint> aliveList : register(t1);

PS_IN_shadow main(uint fakeIndex : SV_VERTEXID) {
  // Loading particle
	uint vertexID = fakeIndex % 6;
	uint instanceID = fakeIndex / 6;
	Particle P = PFX_unpack(particleBuffer[aliveList[instanceID]]);

	// Calculating adjusted world position
	float3 posW = P.pos;
  posW -= ksLightDirection.xyz * P.size;  // closer to light

	// Preparing a billboard
  float3 side = normalize(cross(ksLightDirection.xyz, float3(0, 1, 0)));
  float3 up = normalize(cross(ksLightDirection.xyz, side));
	float3 sideFlat = normalize(float3(side.x, 0, side.z));  // different values for light from above
	float3 upFlat = normalize(float3(up.x, 0, up.z));
	sideFlat = normalize(cross(float3(0, 1, 0), upFlat));
  side = normalize(lerp(side, sideFlat, saturate(-ksLightDirection.y * 2)));
  up = normalize(lerp(up, upFlat, saturate(-ksLightDirection.y * 2)));
	float2 billboardCorner = BILLBOARD[vertexID] * (1.6 * P.size);
	posW.xyz += side * billboardCorner.x;
	posW.xyz += up * billboardCorner.y;

	// Starting to prepare the output
	PS_IN_shadow vout;
	vout.PosH = mul(mul(float4(posW, 1), ksView), ksProjection);
	vout.Tex = BILLBOARD[vertexID];
	vout.TexMovement = P.uvBaseOffset;

	// Dithering offset for better blending of several particles
	vout.Offset2 = uint2(P.random * 8, frac(P.random * 7473.813) * 8);

	// Calculating adjusted opacity
	vout.Opacity = abs(P.opacityS);                               // skipping frustum check
	vout.Opacity *= lerpInvSat(P.pos.y - P.groundY, -P.size, 0);  // hiding particles below ground
	if (P.thickBase < 1.5){                                       // render very dense with fewer modifiers
		vout.Opacity *= lerpInvSat(P.lifeOut, 0.05, 0.1);           // hiding not released from wheels yet
		vout.Opacity *= lerpInvSat(P.stuckBelow, 0.1, 0);           // hiding stuck below cars
		vout.Opacity *= lerpInvSat(P.size, 0.2, 0.5);               // hiding small particles
		vout.Opacity *= lerpInvSat(P.size - P.thickFinal, 2, 1);    // hiding large particles
		if (P.targetSize < 0) vout.Opacity = 0;                     // hiding long-living
	} else {
		vout.Opacity *= 0.8;
	}
	
	// Hiding particles if there are too many
	float particlesLimit = 40;
	float showChance = saturate(particlesLimit / abs(P.cellCount));
	float showRandom = frac(P.random * 3883.672);
	vout.Opacity *= lerpInvSat(showRandom, showChance + 0.12, showChance);

	[branch]
	if (vout.Opacity < 0.05){
		DISCARD_VERTEX(PS_IN_shadow);
	}

	return vout;
}