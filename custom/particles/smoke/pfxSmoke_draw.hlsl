#ifndef GBUFFER_NORMALS_REFLECTION
	#define USE_BACKLIT_FOG
#endif

static const float2 BILLBOARD[] = {
	float2(-1, -1),
	float2(1, -1),
	float2(-1, 1),
	float2(-1, 1),
	float2(1, -1),
	float2(1, 1),
};

struct PS_IN {
	float4 PosH : SV_POSITION;	// screen coordinates
	float2 Tex : TEXCOORD;      // particle shape, -1 in bottom left corner, 1 in top right corner

	float3 PosC : TEXCOORD1;      // position of particle center relative to camera
	nointerpolation float Opacity : COLOR;        // opacity, from 0 to 1 (particles with value close to zero will be discarded)
	nointerpolation float2 Movement : TEXCOORD2;  // UV offset
	nointerpolation uint Color : COLOR1;        	// encoded color

	#if !defined(SHADER_TILE)
		float2 TexUV : TEXCOORD10;  // particle tex coords, might be rotated

		nointerpolation float4 __sdts : TEXCOORD3;
		#define _Size __sdts.x           // negative if it’s a long-lived particle
		#define _DustSplashMix __sdts.y  // negative if it’s a splash
		#define _AlphaExp __sdts.z       // negative if very dense
		#define _AlphaOffset __sdts.w    // negative for snow
	#endif

	#if defined(USE_SIMPLIFIED_SHADOWS) || defined(NO_SHADOWS)
		float __os : TEXCOORD4;
		#define _Shadow __os
	#else
		float2 __os : TEXCOORD4;
		#define _Occlusion __os.x
		#define _Shadow __os.y
	#endif

	#if !defined(SHADER_MIRROR)
		float4 __ldd : TEXCOORD6;
		#define _LightingDir __ldd.xyz
		#define _Depth __ldd.w
	#endif

	float4 __lf : TEXCOORD7;
	#define _Lighting __lf.xyz
	#define _Fog __lf.w

	#ifdef SHADER_SOLID
		nointerpolation uint2 Offset2 : TEXCOORD8;
	#endif
};

struct PS_IN_shadow {
	float4 PosH : SV_POSITION;
	float2 Tex : TEXCOORD;
	float2 TexMovement : TEXCOORD1;
	nointerpolation float Opacity : COLOR;
	nointerpolation uint2 Offset2 : TEXCOORD2;
};

struct PS_IN_tile {
	float4 PosH : SV_POSITION;
	float2 Tex : TEXCOORD;
	float Opacity : TEXCOORD1;
	float DistanceToCamera : TEXCOORD2;
};

struct PS_IN_splashes {
	float4 PosH : SV_POSITION;
	float2 Tex : TEXCOORD;
	float3 Velocity : TEXCOORD1;
	float Opacity : TEXCOORD2;
};

struct PS_IN_fakeshadow {
	float4 PosH : SV_POSITION;
	float2 Tex : TEXCOORD1;
	float Opacity : TEXCOORD2;
};

cbuffer cbDrawParams : register(b6) {
	float4 gDepthRemap;
	
	float2 gPosHMult;
	float gSizeLimitScale;
	float gBrightnessSmoke;

	float gRainFXDeblur;
	float3 gCameraVelocity;

	// float gCarCollision; 
  // float4x4 gCarCollisionTransform;
}

#define SCREEN_UV(POS_H) lerp(gDepthRemap.xy, gDepthRemap.zw, (POS_H).xy * extScreenSize.zw * gPosHMult)

// #ifdef TARGET_PS
// 	Texture2D<float> txCarCollision : register(TX_SLOT_MAT_2);
// 	float checkCarCollision_(float3 pos){
// 		float4 uv = mul(float4(pos, 1), gCarCollisionTransform);
// 		return !gCarCollision || any(abs(uv.xyz - 0.5) > float3(0.5, 0.5, 0.48));// ? 1 : saturate((txCarCollision.SampleLevel(samLinear, uv.xy, 0) - uv.z) * 50);
// 	}
// #endif
