#include "pfxSmoke.hlsl"
RWByteAddressBuffer countingCellsWrite : register(u7);

[numthreads(32, 32, 1)]
void main(uint3 DTid : SV_DispatchThreadID) {
	countingCellsWrite.Store3((DTid.y * GRID_SIZE + DTid.x) * GRID_ITEM_SIZE, uint3(0, 0, 0));
}
