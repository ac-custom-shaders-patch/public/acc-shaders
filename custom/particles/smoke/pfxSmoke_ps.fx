// #define SHADOWS_FILTER_SIZE 2
#define MICROOPTIMIZATIONS
#define WITH_SHADOWS
#define DEPTH_LR
#define PIN_FOG pin._Fog

#include "pfxSmoke.hlsl"
#include "include_new/base/_include_ps.fx"
#include "pfxSmoke_draw.hlsl"

#ifdef ALLOW_WET
  #define ALLOW_EXTRA_FEATURES
  #include "refraction.hlsl"
  #include "rainUtils.hlsl"
#endif

#ifndef NO_DEPTH_MAP
  #include "include_new/ext_functions/depth_map.fx"  
  Texture2D<float> txLinearDepth : register(TX_SLOT_MAT_1);
  float getDepthLrAccurate(float4 pinPosH){
    return txLinearDepth.SampleLevel(samLinearSimple, SCREEN_UV(pinPosH.xy), 0).x;
  }

  float calculateSoftK(float4 posH, float depthC, float softDepthK, float offset){  
    #ifdef NO_DEPTH_MAP
      return 1;
    #else
      #ifdef DEPTH_LR
        float depthZ = getDepthLrAccurate(posH);
      #else
        float depthZ = getDepthAccurate(posH);
      #endif
      // return lerp(gDepthRemap.x, gDepthRemap.z, posH.x / gResolutionMult);
      // return posH.xy * extScreenSize.zw * 2;
      // return SCREEN_UV(posH.xy).x;
      // return frac(depthZ);
      return (depthZ - depthC) * softDepthK - offset;
    #endif
  }

  float calculateSoftK(float4 posH, float softDepthK, float offset){  
    return calculateSoftK(posH, linearizeAccurate(posH.z), softDepthK, offset);
  }
#else
  float calculateSoftK(float4 posH, float depthC, float softDepthK, float offset){ return 1; }  
  float calculateSoftK(float4 posH, float softDepthK, float offset){ return 1; }
#endif

Texture2D txSmokeNoise : register(TX_SLOT_MAT_4);

#ifndef SHADER_MIRROR
float4 returnBorder(PS_IN pin, float v = 1) : SV_TARGET {
  // float3 color = float3(pin._Pad, 1 - pin._Pad, 0);
  float3 color = unpackColor(pin.Color).rgb;
  // float f = frac(pin.Opacity * 10);
  // float3 color = float3(f, 1 - f, 0);
  color *= 2;
  color = float3(1 * v, 1 * (1 - v), 0);
  return float4(color, 1);
}
#endif

float3 bump3y(float3 x, float3 yOffset) {
  return saturate(1 - x * x - yOffset);
}

#define RAINBOW_LEAKAGE_RED 1.4
#define RAINBOW_LEAKAGE_VIOLET -0.25

// 0 for 400, 1 for 700
float3 spectralZucconi(float x, float blur) {
  // return rainbow(x);
  // x = smoothstep(-0.2, 1.2, x);

  // float3 ret = spectralSpektre(x);
  // return spectralSpektre(x);

  const float3 cs = float3(3.54541723, 2.86670055, 2.29421995);
  const float3 xs = float3(0.69548916, 0.49416934, 0.28269708);
  const float3 ys = float3(0.02320775, 0.15936245, 0.53520021);
  return bump3y(cs * (x - xs) * (1 - blur), ys);

  const float3 c1 = float3(3.54585104, 2.93225262, 2.41593945);
  const float3 x1 = float3(0.69549072, 0.49228336, 0.27699880);
  const float3 y1 = float3(0.02312639, 0.15225084, 0.52607955);
  const float3 c2 = float3(3.90307140, 3.21182957, 3.96587128);
  const float3 x2 = float3(0.11748627, 0.86755042, 0.66077860);
  const float3 y2 = float3(0.84897130, 0.88445281, 0.73949448);
  float3 ret = bump3y(c1 * (x - x1), y1) + bump3y(c2 * (x - x2), y2);
  return ret;
}

void processRainbow(inout float3 color, float3 toCamera, float distance) {
  /* 
  Source: 
    http://www.philiplaven.com/p20.html

  Calculated via:
    function rainbow1(ior) { let a = Math.acos(Math.sqrt((ior * ior - 1) / 3)); return (-2 * a + 4 * Math.asin(Math.sin(a) / ior)) * 180 / Math.PI; }
    function rainbow2(ior) { let a = Math.acos(Math.sqrt((ior * ior - 1) / 8)); return (2 * a - 6 * Math.asin(Math.sin(a) / ior) + Math.PI) * 180 / Math.PI; }
    [ rainbow1(1.33141), rainbow2(1.33141) ]

  Based on:
    https://plus.maths.org/content/rainbows
    https://scientificsentence.net/Optics/descartes_rainbow.php
  */

  float cRainbowBrightness = 1;
  float cRainbowInBetweenDarkening = 1;
  float cRainbowSecondaryBrightness = 0;
  float3 cSunDirection = -ksLightDirection.xyz;
  
  [branch]
  if (cRainbowBrightness == 0) return;

  const float rainbow1_400 = cos(M_PI / 180 * 40.43337);
  const float rainbow1_700 = cos(M_PI / 180 * 42.30989);

  float rainbowMult = saturate(remap(toCamera.y, 0.3 - abs(cSunDirection.y), 0.75 - abs(cSunDirection.y), 1, 0));
  // float rainbowMult = saturate(remap(toCamera.y, 0.3 - abs(cSunDirection.y), 0.31 - abs(cSunDirection.y), 1, 0));
  #ifdef REFLECTIONS_MODE
    rainbowMult *= 0.5;
  #endif

  float LdotV = dot(toCamera, -cSunDirection);
  float rainbow1Value = remap(LdotV, rainbow1_400, rainbow1_700, 0, 1);
  // rainbow1Value = lerp(rainbow1Value, 0.5, 1 - distance * 0.1 / (1 + distance * 0.1));
  float rainbowValue = rainbow1Value;
  float rainbowIntensity = cRainbowBrightness; 

  [branch] 
  if (rainbowValue > -2 && rainbowValue < 3){
    float3 r = spectralZucconi(rainbowValue, 1 / (1 + distance * 0.05));
    // color *= lerp(1, r, min(1, dot(r, 0.5)));
    // color *= lerp(1, r, 1 - pow(abs(1 - 2 * rainbowValue), 2));
    color *= 1 + r * 0.5;
  }
}

RESULT_TYPE main(PS_IN pin) {
  // float t = 1 - 0.005 / abs(pin._Size);
  float t = 0.98;
  // if (max(pin.Tex.x, pin.Tex.y) > t || min(pin.Tex.x, pin.Tex.y) < -t) return returnBorder(pin);
  // if (dot2(pin.Tex) < 0.01) return returnBorder(pin);
  // pin.Opacity *= 0.1;
  float3 toCamera = normalize(pin.PosC);
  float pAlphaExp = abs(pin._AlphaExp);
  float pSize = abs(pin._Size);
  float pDustMix = saturate(pin._DustSplashMix);
  float pAlphaOffset = abs(pin._AlphaOffset);
  bool longLived = pin._Size < 0;
  bool veryDense = pin._AlphaExp < 0;
  bool snowPiece = pin._AlphaOffset < 0;

  #if defined(ALLOW_WET) || defined(SHADER_MIRROR)
    float pSplashMix = saturate(-pin._DustSplashMix);
  #else
    float pSplashMix = 0;
  #endif

  float2 uv = pin.TexUV;
  // uv *= lerp(1, 0.7, pDustMix);
  
  float distanceToCamera = length(pin.PosC);
  float3 fromCamera = pin.PosC / distanceToCamera;
  float backlitDot = saturate(dot(fromCamera, -ksLightDirection.xyz));
  float3 lighting;
  float alpha;

  #if !defined(SHADER_MIRROR)
    if (useNearbyInteriorClipping){
      pin.Opacity *= saturate(remap(distanceToCamera, 1.1, 1.2, 0, 1));
    }
  #endif

  #if defined(SHADER_MIRROR)
    {
  #else
    [branch]
    if (longLived){

      // if (max(pin.Tex.x, pin.Tex.y) > 0.99) return returnBorder(pin);

      // float3 color = unpackColor(pin.Color).rgb;
      // float localFog = 1 - saturate(dot(color, 1));
      // uv *= 1 + localFog;
      // pin._Fog = lerp(pin._Fog, 1, localFog * 0.5);

      // uv *= 1 + pDustMix;

      float4 txSmokeValue1 = txSmokeNoise.Sample(samLinearSimple, uv * 0.24 + pin.Movement);
      float4 txSmokeValue2 = txSmokeNoise.Sample(samLinearSimple, uv * 0.24 * -0.17 + abs(pin.Movement.yx) * -0.27);
      float4 txSmokeValue = txSmokeValue1 * txSmokeValue2;
      float softK = calculateSoftK(pin.PosH, pin._Depth, lerp(0.05, 0.45, abs(fromCamera.y)), pow(pin.Tex.x, 2));

      lighting = 0.16 * getAmbientBaseNonDirectional(pin.PosC) 
        + ksLightColor.xyz * (0.16 * (saturate(-ksLightDirection.y * 3) * pin._Shadow + pow(backlitDot, 20)))
        + pin._Lighting /* scaled in VR shader */;
      lighting *= GAMMA_LINEAR(unpackColor(pin.Color).rgb * GAMMA_NONPBR_MULT);
      // lighting.r = ksLightColor.g;
      alpha = saturate(softK) * smoothstep(0, 1, saturate(1 - length(pin.Tex) - txSmokeValue.a * 1.5)) * pin.Opacity; 
      // lighting = unpackColor(pin.Color).rgb;
      // alpha = 1; 
      clip(alpha - 0.003);

    } else {
  #endif

    uv *= 1 + lerpInvSat(pSize, 1, 3) * 2;
    if (veryDense) uv *= lerp(2, 1.5, pDustMix);
    uv *= lerp(1, 0.5, pSplashMix);
    if (snowPiece) uv *= 1.8;
    
    float4 txSmokeValue1 = txSmokeNoise.Sample(samLinearSimple, uv * 0.16 + pin.Movement);
    float4 txSmokeValue2 = txSmokeNoise.Sample(samLinearSimple, uv * 0.32 + pin.Movement);
    // float4 txSmokeValue = lerp(txSmokeValue1, txSmokeValue2, lerpInvSat(pSize, pSize < 1.2 ? 0 : 2.4, 1.2));
    float pSizeSmall = lerpInvSat(pSize, 2.4, 1.2);
    float4 txSmokeValue = lerp(txSmokeValue1, txSmokeValue2, pSizeSmall);

    #ifdef GBUFFER_NORMALS_REFLECTION
      #error not supported
    #else
      float alphaTex = smoothstep(0, 1, txSmokeValue.a);
      alpha = 1 - length(pin.Tex);
      // alpha = saturate(alpha - alphaTex * pAlphaOffset - veryDense * pSplashMix * max(0, pSize * 8 - 1.5));
      alpha = saturate(alpha - alphaTex * pAlphaOffset);
      alpha = min(saturate(alpha * 5), pow(alpha, pAlphaExp));

      float alphaBase = alpha;
      alpha *= pin.Opacity;
    #endif

    #ifdef NO_DEPTH_MAP
      float softBaseK = 1;
    #else
      float softSide = pow(pin.Tex.x, 4);
      float softDepthK = lerp(2, 1, softSide) / pSize * lerp(0.5, 1, alpha) * lerp(1, 2, pSplashMix) * lerp(1, 2, pDustMix);
      float softOffset = 2 * max(pow(txSmokeValue.a, 2), softSide * 2) * lerpInvSat(pSize, 0.25, 0.5) * saturate(pSize);
      float softBaseK = calculateSoftK(pin.PosH, pin._Depth, softDepthK, softOffset + veryDense * (1 - txSmokeValue1.w) * 2);
    #endif
    float softK = smoothstep(0, 1, saturate(softBaseK));
    // softK = 1;
    // return returnBorder(pin, softBaseK);

    float clipValue;
    #ifdef SHADER_SOLID
      clipValue = alpha - 0.1;
      return returnBorder(pin);
    #else
      clipValue = alpha * softK - 0.003;
    #endif
    clipValue = min(clipValue, 1 - dot2(pin.Tex));
    clip(clipValue);

    // Simple backlit
    float neutralK = lerpInvSat(alpha, 0.2, 0);
    float backlit = pow(backlitDot, lerp(30, 1, neutralK * neutralK)) * lerp(0.5, 1, neutralK) * 4;
  
    #if defined(SHADER_MIRROR) || defined(USE_SIMPLIFIED_SHADING)
      lighting = (getAmbientBaseNonDirectional(pin.PosC) + ksLightColor.xyz * (1 + backlit) + pin._Lighting) 
        * saturate(txSmokeValue.y * 0.4 + 0.6); 
    #else
      // Preparing flat normal value
      float2 txNm = pin.Tex * float2(-1, 1) * 1.4;
      txNm += lerp(-1, 1, txSmokeValue.xy) * 0.8;
      txNm = clamp(txNm, -1, 1);

      // Use soft particles calculation to estimate how close we are to any surface
      float facingDown = pow(lerpInvSat(softBaseK, 4, 1), 2);  // softBaseK is an unclamped version of softK
      facingDown *= lerp(1, 0, pow(fromCamera.y, 2));          // reduce effect if looked at from above
      facingDown *= saturate(alpha * 5) * 0.2;                 // link to alpha for better fading out
      if (veryDense) facingDown = 0;
      txNm.y = lerp(txNm.y, -1, facingDown);
      
      // Turning flat normal value to world-space one
      float3 toSide = normalize(cross(float3(0, 1, 0), fromCamera));
      float3 toUp = normalize(cross(fromCamera, toSide));
      float3 normal = normalize(-fromCamera * (1 - dot2(txNm)) + toUp * txNm.y + toSide * txNm.x 
        + float3(0, veryDense || snowPiece ? 0 : pDustMix * saturate(pSize * 2), 0));

      // Adding an extra component to backlit, some sort of translucency, helps to avoid smoke looking dirty
      backlit = max(backlit, pow(saturate(dot(normal, ksLightDirection.xyz) * 0.5 + 0.5), 8) * 0.25) * (1 - alpha);
      // float ambientBacklit = pow(saturate(-normal.y * 0.5 + 0.5), 8) * 0.25;
      float ambientBacklit = 0;

      // Calculating lighting
      float diffuseBase = lerp(pow(saturate(dot(normal, -ksLightDirection.xyz) * 0.45 + 0.55), 1), 0.8, neutralK);
      float3 diffuseColor = ksLightColor.xyz * (pin._Shadow * (diffuseBase + backlit)); 
      // processRainbow(diffuseColor, toCamera, distanceToCamera);

      float3 ambientColor = (getAmbientBaseAt(pin.PosC, normal, 1) + AMBIENT_COLOR_AT(pin.PosC) * ambientBacklit) * pin._Occlusion;
      lighting = ambientColor + diffuseColor;

      // Dynamic lighting (zero _LightingDir means no main light source is found)
      float extraLightDot = lerp(1, saturate(dot(normal, pin._LightingDir) * 0.45 + 0.55) 
        + pow(saturate(-dot(normal, pin._LightingDir) * 0.5 + 0.5), 8) * 0.25, dot2(pin._LightingDir));
      lighting += lerp(extraLightDot, 0.8, neutralK) * pin._Lighting * lerp(1, 0.25, pSplashMix);

      // Fading out lighting for bits of smoke looking down into the ground 
      lighting *= lerp(1, 0, facingDown);
      if (snowPiece) lighting *= 3;


      // lighting.r = 0;

    #endif // if defined(SHADER_MIRROR)

    // lighting *= 0.2 * lerp(unpackColor(pin.Color).rgb, luminance(dot(unpackColor(pin.Color).rgb, 0.333)), neutralK * 0.5);
    lighting *= lerp(gBrightnessSmoke, 0.2 + pSplashMix * 0.05, max(pDustMix, pSplashMix)) 
      * GAMMA_LINEAR(unpackColor(pin.Color).rgb * GAMMA_NONPBR_MULT); // before: 0.2
    // lighting = 0.5 * unpackColor(pin.Color).rgb; // before: 0.2

    // Wet drops use blurred version of previous frame for some sort of refraction effect
    #if defined(ALLOW_WET) && !defined(SHADER_MIRROR)
      bool isPaused = gRainFXDeblur;
      float behindBlur = pSplashMix * (veryDense ? 1
        : lerpInvSat(alphaBase, 0.1, 0.5) * lerpInvSat(distanceToCamera, 2, 4) * saturate(pSize));
      [branch]
      if (behindBlur > 0.01) {
        float3 toCamera = normalize(pin.PosC);
        float4 behind = txPrevFrame.SampleLevel(samLinearClamp, 
          SCREEN_UV(pin.PosH.xy) 
            - (veryDense ? calculateRefractionOffset(pin.PosC, toCamera, normal, 0.2 * alpha) : 0), 
          veryDense ? (isPaused ? 1.5 : 2.5) : 3);
        lighting = lerp(lighting, behind.rgb, behind.a * lerp(0, 0.97, behindBlur));
        if (veryDense) {
          if (isPaused) alpha = saturate(alpha * 1.6);

          // Reflections? Why?
          // float fresnel = pow(saturate(1 + dot(toCamera, normal) * 2), 5);
          // float3 reflection = reflect(-toCamera, normal);
          // float3 reflectionColor = sampleEnv(reflection, 3, 0);
          // lighting = lerp(lighting, reflectionColor, fresnel * saturate(alpha * 2) * 0.05);
          // lighting = float3(4, 0, 0);
        }
      }
    #endif

    alpha *= softK;
  }

  alpha = saturate(alpha);
  alpha = GAMMA_OR(pow(alpha, 1.4), alpha);
  // alpha = GAMMA_OR(alpha * 0.1, alpha);
  // alpha = GAMMA_LINEAR_SIMPLE(alpha);
  // alpha = 1;
  RETURN_BASE(lighting, alpha);
}