#include "pfxSmoke.hlsl"
#include "include_new/base/_include_ps.fx"
#include "pfxSmoke_draw.hlsl"

Texture2D txSmokeNoise : register(t4);

float4 main(PS_IN_splashes pin) : SV_TARGET {
  float alpha = pin.Opacity * saturate(1 - dot2(pin.Tex)) * 0.6;
  clip(alpha - 0.1);
  return float4(pin.Velocity, alpha);
}