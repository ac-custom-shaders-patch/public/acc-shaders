#include "include/common.hlsl"
#include "pfxSmoke.hlsl"
#include "include_new/base/_include_ps.fx"
#include "pfxSmoke_draw.hlsl"

Texture2D<float> txLinearDepth : register(TX_SLOT_MAT_1);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float4 Color : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float2 uv = SCREEN_UV(pin.PosH.xy);
  float intensity = pin.Color.a;
  float depth = txLinearDepth.SampleLevel(samLinearSimple, uv, 0);
  float alpha = depth / (lerp(0.5, 0.1, intensity) + depth) * intensity;
  if (useNearbyInteriorClipping) alpha *= lerpInvSat(depth, 1.3, 1.5);
  alpha *= saturate(lerp(0.8, 1, dot2(uv * 2 - 1)));
  clip(alpha - 0.001);
  alpha = GAMMA_LINEAR(alpha);
  return float4(pin.Color.rgb, alpha);
}