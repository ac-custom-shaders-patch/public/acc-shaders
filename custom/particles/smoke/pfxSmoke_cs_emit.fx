#include "include/samplers.hlsl"
#include "pfxSmoke.hlsl"
#include "include/mesh_spawn_cs.hlsl"

RWStructuredBuffer<Particle_packed> particleBuffer : register(u0);
RWStructuredBuffer<uint> aliveBuffer_CURRENT : register(u1);
RWStructuredBuffer<uint> aliveBuffer_NEW : register(u2);
RWStructuredBuffer<uint> deadBuffer : register(u3);
RWByteAddressBuffer counterBuffer : register(u4);
StructuredBuffer<EmittingSpot> emittingSpots : register(t19);
StructuredBuffer<Wheel> wheels : register(t1);
StructuredBuffer<Car> cars : register(t2);
Texture2D txGrass : register(t21);
Texture2D txAreaColor : register(t22);

ByteAddressBuffer meshVertices0 : register(t4);
ByteAddressBuffer meshIndices0 : register(t5);
ByteAddressBuffer meshVertices1 : register(t6);
ByteAddressBuffer meshIndices1 : register(t7);

float grassAmount(float3 pos){
	float2 mapUV = (pos.xz - gMapPointB) / (gMapPointA - gMapPointB);
	return txGrass.SampleLevel(samLinearClamp, mapUV, 0).a;
}

bool alignWithMesh(out float3 pos, out float3 localPos, float size, float3 velocity, uint index, RAND_DECL){
	MeshPoint M;
	[branch]
	if (index){
		M = randomMeshPoint(meshVertices1, meshIndices1, meshIndexCount1, gCarCollisionTransform1, RAND_ARGS);
	} else {
		M = randomMeshPoint(meshVertices0, meshIndices0, meshIndexCount0, gCarCollisionTransform0, RAND_ARGS);
	}
	pos = M.pos + M.normal * size;
	localPos = M.localPos;
	return dot(M.normal, normalize(velocity)) > -0.5;
}

#include "include/track_area_heightmap.hlsl"

void emit(uint3 DTid, EmittingSpot E){ 
	// Initializing randomizer
	RAND_INIT(0.12345, float2(E.randomSeed + dot(DTid, 0.01), (float)DTid.x / (float)THREADCOUNT_EMIT));

	float2 intensity_surfaceK = loadVector(E.intensity_surfaceK);
	float E_intensity = intensity_surfaceK.x;
	float E_surfaceK = intensity_surfaceK.y;

	// Loading wheel data
	Wheel wheel = wheels[E.wheelID];

	// New particle, basic common properties
	Particle P = (Particle)0;
	P.pos = E.contactPoint;
	P.wheelID = E.wheelID;
	P.random = RAND;
	P.phase = (float)DTid.x;
	P.uvBaseOffset = float2(RAND, RAND);
	P.colorConsistency = abs(E.colorConsistency);

	bool specialSnowOrIce = P.colorConsistency > 1.01;
	bool specialIce = P.colorConsistency > 1.5;
	bool specialSnow = specialSnowOrIce && !specialIce;

	[branch] 
	if (wheel.radius < 0) {

		// Particle without wheel
		P.growK = E._growK * (E._growK > 0 ? 0.5 + RAND : 1);
		P.thickBase = saturate(E.thickness + (1 - P.growK) * 0.2);

		float speedOffset = pow(RAND, 2) * (RAND > 0.5 ? 1 : -1);
		P.velocity = length(E.velocity * (0.8 + 0.5 * speedOffset)) * normalize(E.velocity + RAND_DIR * (0.5 * E._spreadK));
		P.color = E.color;
		P.size = E.size;
		P.life = E.life * lerp(0.5, 1.5, pow(RAND, 10));
		P.opacityK = E_intensity / P.life;
		P.groundY = E.groundY;
		P.lifeOut = (E._emitterFlags & FLAG_FADE_IN) ? 0 : 1;
		P._targetVelocityY = E.targetYVelocity;
		P._flags = E._emitterFlags | FLAG_REGULAR_LIFE_REDUCTION;

		P.targetSize = 4 + (1 + P.thickBase) * RAND;
		P.targetSize *= lerp(0.2 + 0.4, 1, saturate(P.growK));
		P.targetSize *= lerp(1, 0.2, saturate(-P.growK));
		P.pos -= E.velocity * gFrameTime;
		// P.color = packColor(float4(1, 0, 0, 1));

		// // Long-lasting particle
		// P.targetSize = -1;                       // flag specifies that it’s a long-living particle
		// P.life = lerp(0.01, 0.03, P.thickBase);  // instead of seconds, life stores slowly reducing density from 0 to 1
		// P.life *= lerp(1, 3, E.dustMix);         // boost intensity for dust particles
		// P.life = 0.5;
		// P.opacityK = 0.5;                        // long-living particles can’t be that opaque
		// P.size = GRID_CELL_SIZE * 0.6;           // resize accordingly
		// P.velocity = 0.000001;                   // won’t be applied, just to avoid dividing by zero
		// P.pos.xz += float2(RAND_COS, RAND_COS) * 30;
		// P.pos.xz = lerp(P.pos.xz, (floor(P.pos.xz / GRID_CELL_SIZE) + 0.5) * GRID_CELL_SIZE, 0.2);
		// P.growK = 0;
		// P.dustMix = 1;

		// // Blending color to whiteish and resetting alpha for more consistency
		// // color.rgb = lerp(0.7, color.rgb, 1 / (1 + max(0, P.size - 0.2) * lerp(0.35, 0, P.colorConsistency)));
		// // color.a = 1;
		// P.colorBase = packColor(float4(1, 1, 1, 1));
		// P.color = packColor(float4(1, 1, 1, 1));
		// P.colorConsistency = 100;

		// // Storing current index, incrementing counter
		// counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_LONGLIVING_INDEX, 1, P.longLivingIndex);
		// P.longLivingIndex = 0xffffffff - P.longLivingIndex;

		// For debugging:
		// P.life = 0;

	} else {

		// Particle with wheel, or spawned by car in general, or spawned stuck below car (dense particles are also here)
		Car car = cars[E.wheelID / 4];
		
		// Heavily reducing rate when on grass or on wet dirt
		float grass = grassAmount(E.contactPoint);
		float reducedRate = lerp(grass, 1, E.dustMix * saturate(gRainWater * 4));
		if (RAND > 0.5 && reducedRate > 0.7) return;

		// Filling out general properties
		P.growK = lerpInvSat(max(wheel.angularSpeed * wheel.radius, car.carSpeed), KMH(40), KMH(100));
		P.thickBase = E.thickness * lerp(1, 0, reducedRate);
		P.size = lerp(lerp(wheel.emitSizeA, wheel.emitSizeB, RAND), 0.15, E.dustMix);
		P.groundY = E.contactPoint.y + min(0, wheel.velocity.y * 2);
		P.dustMix = E.dustMix;
		P.splashMix = E.splashMix;

		// Mixing color based on grass value
		float4 color = lerp(unpackColor(E.color), unpackColor(E.soilColor), grass);
		if (E.colorConsistency < 0) {			
      float4 c1 = TA_getSurfaceColor(E.contactPoint);
			if (c1.w > 0.99) color.rgb = lerp(color.rgb, c1.rgb / 0.2, E.dustMix);
		}
		P.colorConsistency = lerp(P.colorConsistency, 1, grass);

		// Particle would grow until hit targetSize
		P.targetSize = lerp(5, 7, P.thickBase);
		P.targetSize *= lerp(0.6, 1, RAND);
		P.targetSize *= lerp(1, 1.8, P.growK);
		P.targetSize *= lerp(lerp(0.8, 0.1, E.dustMix), 1, lerpInvSat(car.carSpeed, 0, KMH(60)));			
		P.targetSize *= lerp(1, 1.5, E.splashMix);  // wet particles grow faster and bigger

		[branch]
		if (E.thickness < -1.5){
			// Collision!
			P.velocity = E.velocity * lerp(0.3, 0.5, RAND);
			P.velocity = length(E.velocity) * lerp(0.1, 0.4, RAND) 
				* (normalize(E.velocity) + RAND_DIR * 0.25);
			P.size = 0.2;
			P.targetSize = 0.4;
			P.life = lerp(0.5, 2, pow(RAND, 40));
			P.opacityK = 0.4 / P.life;
			P.thickBase = 0;
			P.groundY = min(P.groundY, E.groundY);
			P._targetVelocityY = -1;
			P._flags = FLAG_FADE_IN | FLAG_COLLISION_DUST;

			float3 localPos;
			if (!alignWithMesh(P.pos, localPos, 0.4, E.velocity, E.size ? 1 : 0, RAND_ARGS)){
				// If failed to align with model, use particle as one spawned from obstacle
				P.velocity = -P.velocity * 0.001;
				P.pos = E.contactPoint;
				
				float3 offset = float3(RAND_COS, RAND_COS, RAND_COS);
				offset -= car.collisionPlane.xyz * dot(offset, car.collisionPlane.xyz);
				P.pos += offset * 0.5;
				P.opacityK *= 0.5;

			} else {
				// Dim out particles spawned from bottom area of the car, like if there is more dirt or something (but
				// in reality just want to hide lack of proper occlusion)
				color.rgb *= lerp(0.1, 1, lerpInvSat(localPos.y, 0.4, 0.6));
			}

			// color.rgb = float3(0, 1, 0);
		} else if (E.thickness < -1) {
			// Dust raised from ground
			P.thickBase = 0.1 + 0.7 * lerpInvSat(car.carSpeed, 0, KMH(120));
			P.velocity = car.carVelocity * 0.7;// + float3(0, car.carSpeed * 0.1, 0);
			// P.pos = E.contactPoint + wheel.up * 0.5;
			P.pos = car.shadowCenter - car.carVelocity * (2 / car.carSpeed);
			P.size = lerp(0.7, 1, pow(RAND, 4));
			P.targetSize = P.size * lerp(6, 12, pow(RAND, 8)) * lerp(0.6, 1, E_intensity) * lerp(1, 3, lerpInvSat(car.carSpeed, 0, KMH(200)));
			P.life = lerp(4, 8, RAND);
			P.opacityK = 1.6 / P.life;
			P._flags |= FLAG_GROUND_SPAWN;
			// color.rgb = float3(0, 1, 0);
		} else if (E.thickness < -0.5) {
			// Spawned under car as a large less defined particle
			P.thickBase = 0.2;
			P.velocity = car.carVelocity;
			P.pos = car.shadowCenter - car.carVelocity * (2 / car.carSpeed);
			P.size = lerp(0.7, 1, pow(RAND, 4)) * 0.7 * lerp(1, 3, lerpInvSat(car.carSpeed, 0, KMH(300)));
			P.targetSize *= lerp(1, 2, pow(RAND, 4)) * lerp(1, 2, lerpInvSat(car.carSpeed, 0, KMH(300)));

			P.life = lerp(12, 20, RAND) * lerp(1, 0.4, E.splashMix);
			P.opacityK = 1.6 / P.life;
			// color.rgb = float3(1, 0, 0);
		} else if (E.size == -1){
			// Dust flying off dirty wheels
			float s, c;
			sincos(RAND * M_TAU, s, c);
			P.pos = wheel.pos + (wheel.up * c + wheel.dir * s) * (wheel.radius - 0.03);
			P.velocity = wheel.velocity;
			P.size = 0.1 * (1 + pow(RAND, 2));
			P.life = 0.4 + 4 * pow(RAND, 8);
			P.opacityK = 0.3 / P.life;
			P.targetSize = 8;
			P._flags = FLAG_FADE_IN | FLAG_REDUCED_COLLISIONS | FLAG_REDUCED_MOTION | FLAG_REDUCED_FORCES;
		} else if (E.life < 0){
			// Stuck below car
			float dirMult = lerp(0.2, 4, pow(RAND, 4));
			P.velocity = wheel.velocity
				+ normalize(-wheel.outside + wheel.dir * dirMult) * 10;
			P.pos -= wheel.velocity * gFrameTime;
			P.stuckBelow = 1;
			P.size *= 2;

			P.life = 0.8 + pow(RAND, 2) + 6 * pow(RAND, 4) * (RAND > 0.5);
			P.opacityK = lerp(1.4, 2, P.thickBase) / P.life;
		} else if (!specialSnowOrIce /* not a snow particle */ 
				&& RAND < gLongLastingChance && E.splashMix < 0.1 && isCellIdValid(getCellId(P))) {
			// Long-lasting particle
			P.targetSize = -1;                       // flag specifies that it’s a long-living particle
			P.life = lerp(0.01, 0.03, P.thickBase);  // instead of seconds, life stores slowly reducing density from 0 to 1
			P.life *= lerp(1, 3, E.dustMix);         // boost intensity for dust particles
			P.opacityK = 0.5;                        // long-living particles can’t be that opaque
			P.size = GRID_CELL_SIZE * 0.6;           // resize accordingly
			P.velocity = 0.000001;                   // won’t be applied, just to avoid dividing by zero
			P.pos.xz = lerp(P.pos.xz, (floor(P.pos.xz / GRID_CELL_SIZE) + 0.5) * GRID_CELL_SIZE, 0.5);
			P.growK = 0;
			P.dustMix = 0;

			// Blending color to whiteish and resetting alpha for more consistency
			color.rgb = lerp(0.7, color.rgb, 1 / (1 + max(0, P.size - 0.2) * lerp(0.35, 0, P.colorConsistency)));
			color.a = 1;
			P.colorBase = packColor(color);

			// Storing current index, incrementing counter
			counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_LONGLIVING_INDEX, 1, P.longLivingIndex);
			P.longLivingIndex = 0xffffffff - P.longLivingIndex;
		} else {
			// Stuck to a wheel
			P.stuckPos = RAND / 20;
			P.stuckSpeed = wheel.angularSpeed * saturate(abs(wheel.angularSpeed) / 8 - 1);
			P.size = max(P.size, wheel.radius * 0.5);

			if (WHEEL_HAS_FLAG(FLAG_OPEN_WHEEL)){
				float spawnPos = (pow(RAND, 8) * (RAND > 0.5 ? 1 : -1));
				P.stuckSide = pow(0.5 + 0.4 * spawnPos, 1.6);
				P.stuckDrift = sign(spawnPos) * 0.3 * (1 + RAND);
				P.stuckDrift += car.gforceX * (E.wheelID & 1) * lerp(2, 4, RAND);
			} else {
				P.stuckSide = 0.01 + RAND * 0.8;
				P.stuckDrift = lerp(0.6, 2.4, lerpInvSat(car.carSpeed, KMH(10), KMH(60))) * (1 + RAND)
					+ pow(lerpInvSat(wheel.steered, 0.125, 0.375), 2) * 6  // if wheel is turned, fly off much sooner
					+ lerp(2.5, 0,                                         // flying off slower…
						(1 - max(E.dustMix, E.splashMix))                    // …only if it’s not water or dust
						* lerpInvSat(E_intensity, 0, 0.6));                  // …but high-intensity smoke
			}

			P.velocity = -wheel.dir * wheel.angularSpeed * wheel.radius;
			
			float massiveLifeMult = lerpInvSat(car.carSpeed, KMH(100), KMH(300));
			P.life = lerp(0, 1.2, pow(RAND, 6))   // boost for small particles nearby
				+ lerp(30 * pow(RAND, 60),          // main lifespan value, long but rare
					lerp(4, 10, massiveLifeMult) * pow(RAND, 60), E.splashMix); // water doesn’t stay for as long
			P.opacityK = lerp(1.4, 2, P.thickBase) / P.life;			
			P.size *= lerp(1, 0.5, E.dustMix);
		}

		// P.opacityK *= lerp(1, 0.25, P.dustMix);

		// Dense particles (thrown by wheel)
		float wheelDifference = length(wheel.velocity + P.velocity);         // sliding speed
		float dirtIntensity = lerpInvSat(wheelDifference, KMH(3), KMH(40));  // dirt generates dense particles when sliding
		float finalIntensity = lerp(dirtIntensity, 0.6, E.splashMix);        // when wet, always generate dense particles
		float extraThickBase = max(E.dustMix, E.splashMix);                  // generate for either wet or dusty cases
		[branch] 
		if (abs(E.thickness) > 1e-7 && P_IS_STUCK && (finalIntensity && extraThickBase > 0.2             // only for stuck particles
				&& extraThickBase * lerp(0.2, 0.6, finalIntensity) > RAND * (specialSnow ? 0.5 : 1))) {
			// Unstucking them and marking as dense
			P.stuckSide = 0;
			P.stuckPos = 0;
			P.stuckSpeed = 0;
			P.stuckDrift = 0;
			P.thickBase = specialSnow ? 1 : 2;
			P.lifeOut = 1;

			// Preparing few helping coefficients
			float wheelSpinning = lerpInvSat(abs(wheel.angularSpeed) * wheel.radius, KMH(5), KMH(25));  // how fast wheel is spinning
			float carTurning = saturate(abs(car.gforceX) * 0.5);                                        // how much car is turning

			// Offset velocity is applied to throw particles slighly away from car, otherwise they’d go through its body
			float3 offsetVelocity = 0;
			if (E.wheelID % 4 < 2 && E.splashMix < 0.1) {  // only for rear wheels, and no offset for wet particles
				float dirOffsetMult = lerpInvSat(wheel.steered, 0.5, 0.1);
				offsetVelocity = wheel.outside * min(2, wheel.angularSpeed * wheel.radius) * dirOffsetMult * 0.1;
			}

			// Final velocity consists of wheel velocity, spinning velocity and offset velocity
			// float3 parentVelocity = wheel.velocity * 1.1; // slighlty larger: when going sideways, dense particles should appear in front of a wheel
			float wheelSpeed = length(wheel.velocity);
			float3 parentVelocity = (wheel.velocity / wheelSpeed) * min(wheelSpeed + KMH(5), wheelSpeed * 1.1); // slighlty larger: when going sideways, dense particles should appear in front of a wheel
			
			P.velocity = -wheel.dir * wheel.angularSpeed * wheel.radius;
			float3 spinningVelocity = P.velocity * lerp(0.1, 0.2, pow(RAND, 2)) * lerp(1, 3.5, P.dustMix); // most of velocity is lost to whatever

			// Reduce spinning velocity effect even further if final velocity wouldn’t be aligned with wheel direction
			float slidingForward = abs(dot(normalize(spinningVelocity + parentVelocity), wheel.dir));
			spinningVelocity *= slidingForward;
			wheelSpinning *= slidingForward; // if it’s not aligned, it also doesn’t count as spinning

			// Velocity is ready now, with a bit of extra impulse to get particles to go a bit upwards too
			P.velocity = spinningVelocity + parentVelocity + offsetVelocity;
			P.velocity += wheel.up * lerp(1, 2, wheelSpinning) * lerp(lerp(0.2, 1, pow(RAND, 2)), 0.4, E.splashMix);
			
			// Shifting position around a bit
			P.pos -= wheel.velocity * gFrameTime * lerp(1, 0.95, E.splashMix); // syncs particle position with wheel (why? because of simulate step 
				// running next? would particle.velocity work better here?)
			P.pos += wheel.dir * lerp(lerp(-0.2, 0.2, RAND), -0.05 * sign(wheel.angularSpeed), wheelSpinning); // if wheel is not spinning, shuffle
				// particles around to increase overall area, otherwise shift them to the direction of spinning
			P.pos += wheel.outside * wheel.width * lerp(-0.5, 0.5, RAND);     // distribute particles across wheel width
			P.pos += wheel.up * lerp(0.05, -0.05, carTurning) * E.splashMix;  // move wet particles a bit up unless car is turning
			P.pos += wheel.dir * lerp(-0.1, 0.1, RAND) * carTurning;          // add extra variation if car is turning

			// Dense particles are much smaller
			P.targetSize = lerp(0.15, 0.3, pow(RAND, 3)) * lerp(0.5, 1, saturate(dirtIntensity * 2));
			P.size = lerp(0.02, 0.05, RAND) * lerp(0.5, 1, saturate(dirtIntensity * 2));
			// P.size = lerp(0.02, 0.05, RAND) * 0.5;

			// Important distinction: if wheel is spinning, it’s a larger bit thrown far, but if not, it’s more of a wheel digging into ground 
			// and pushing dirt in front of itself
			P.size *= lerp(0.5, 2, wheelSpinning);
			P.targetSize *= lerp(0.25, 2, wheelSpinning);

			// Life is tiny, especially without wheel spinning
			P.life = lerp(0.05, 0.1, wheelSpinning);
			P.opacityK = lerp(0.2, 10, wheelSpinning) / P.life;

			// Color is a bit darker and more saturated
			// color.rgb = pow(saturate(color.rgb), lerp(1, 1.2, RAND) + 0.1 * slidingForward);
			// P.splashMix = 0;
			// E.splashMix = 0;

			// Wet particles are doing their own thing here (TODO: increase size when car is turning?)
			P.size = lerp(P.size, lerp(0.01, 0.1, carTurning), E.splashMix);
			P.targetSize = lerp(P.targetSize, 0.5, E.splashMix);
			// P.life = 0;
		}

		// Particle intensity is a simple opacityK (aka inverted total lifespan) multiplier
		color.a *= lerp(0.1, 1, pow(E_intensity, 2) * lerp(1, 0.1, reducedRate));
		P.color = packColor(color);

		if (WHEEL_HAS_FLAG(FLAG_OPEN_WHEEL) && P.targetSize >= 0){
			P._flags |= FLAG_REDUCED_COLLISIONS | FLAG_STUCK_SHADOWED;
		}
	}

	if (specialSnowOrIce) {
		P.growK *= 0.5;
		P._flags |= FLAG_REDUCED_COLLISIONS;
	}

	// Filling non-conditional state
	P.thickFinal = P.thickBase;
	P.colorBase = P.color;

	// New particle index retrieved from dead list (pop)
	uint deadCount;
	counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_DEADCOUNT, -1, deadCount);
	uint newParticleIndex = deadBuffer[deadCount - 1];

	// Store the new particle
	P.pos -= gOriginOffset;
	particleBuffer[newParticleIndex] = PFX_pack(P);

	// And add index to the alive list (push)
	uint aliveCount;
	counterBuffer.InterlockedAdd(PARTICLECOUNTER_OFFSET_ALIVECOUNT, 1, aliveCount);
	aliveBuffer_CURRENT[aliveCount] = newParticleIndex;
}

[numthreads(THREADCOUNT_EMIT, 1, 1)]
void main(uint3 DTid : SV_DispatchThreadID, uint3 Gid : SV_GroupID) {
	// Loading emitter, making sure we should spawn
	EmittingSpot E = emittingSpots[Gid.y];
	uint emitCount = counterBuffer.Load(PARTICLECOUNTER_OFFSET_REALEMITCOUNT);	
	emitCount = min(emitCount, E.emitCount);

	[branch] 
	if (DTid.x < emitCount) {
		emit(DTid, E);
	}
}
