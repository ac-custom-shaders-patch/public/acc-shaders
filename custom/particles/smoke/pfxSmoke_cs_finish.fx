#include "pfxSmoke.hlsl"
#define VERTICES_PER_PARTICLE 6

ByteAddressBuffer counterBuffer : register(t0);
RWByteAddressBuffer indirectBuffers : register(u0);

[numthreads(1, 1, 1)]
void main(uint3 DTid : SV_DispatchThreadID) {
	uint particleCount = counterBuffer.Load(PARTICLECOUNTER_OFFSET_ALIVECOUNT_AFTERSIMULATION);
  uint cameraOcclusion = counterBuffer.Load(PARTICLECOUNTER_OFFSET_CAMERA_OCCLUSION_BA);
	indirectBuffers.Store4(ARGUMENTBUFFER_OFFSET_DRAWPARTICLES, uint4(particleCount * VERTICES_PER_PARTICLE, 1, 0, 0));
	indirectBuffers.Store4(ARGUMENTBUFFER_OFFSET_DRAWPARTICLES + 16, cameraOcclusion > 0 ? uint4(3, 1, 0, 0) : 0);
}
