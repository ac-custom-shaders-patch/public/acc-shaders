#include "pfxSmoke.hlsl"
#include "include_new/base/_include_ps.fx"
#include "pfxSmoke_draw.hlsl"
#include "include/bayer.hlsl"

Texture2D txSmokeNoise : register(t4);

float4 main(PS_IN_shadow pin, out float depth : SV_Depth) : SV_TARGET {
  float4 txSmokeValue = txSmokeNoise.Sample(samLinearSimple, pin.Tex * 0.4 + pin.TexMovement);

  float alpha = saturate(1 - length(pin.Tex) - pow(txSmokeValue.a, 2) * 0.8) * pin.Opacity;
  float bayerValue = getBayer(pin.PosH, pin.Offset2);
  clip(pow(saturate(alpha), 0.8) * 0.6 + bayerValue);
  depth = pin.PosH.z - 0.0005 * bayerValue * lerp(1, 4, dot2(pin.Tex));

  // uint2 xy3 = pos.xy % 3;
  // if (alpha < 0.9 && xy3.x == 0 && xy3.y == 0
  //   || alpha < 0.8 && xy3.x == 2 && xy3.y == 1
  //   || alpha < 0.7 && xy3.x == 1 && xy3.y == 2
  //   || alpha < 0.6 && xy3.x == 1 && xy3.y == 0
  //   || alpha < 0.5 && xy3.x == 1 && xy3.y == 1
  //   || alpha < 0.4 && xy3.x == 0 && xy3.y == 2
  //   || alpha < 0.3 && xy3.x == 2 && xy3.y == 0
  //   || alpha < 0.2 && xy3.x == 0 && xy3.y == 1
  //   || alpha < 0.1) {
  //   discard;
  // }

  return 1;
}