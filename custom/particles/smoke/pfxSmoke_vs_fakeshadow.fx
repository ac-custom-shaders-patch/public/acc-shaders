#define FOG_VS_COMPUTE_SECOND

#include "pfxSmoke.hlsl"
#include "include_new/base/_include_vs.fx"
#include "pfxSmoke_draw.hlsl"

StructuredBuffer<Particle_packed> particleBuffer : register(t0);
StructuredBuffer<uint> aliveList : register(t1);

PS_IN_fakeshadow main(uint fakeIndex : SV_VERTEXID) {
  // Loading particle
	uint vertexID = fakeIndex % 6;
	uint instanceID = fakeIndex / 6;
	Particle P = PFX_unpack(particleBuffer[aliveList[instanceID]]);

	// Calculating adjusted world position
	float3 posW = P.pos;
	posW.y = P.groundY;

	// Preparing a billboard
	float3 side = float3(-1, 0, 0);
	float3 up = float3(0, 0, -1);
	float2 billboardCorner = BILLBOARD[vertexID] * (1.6 * P.size);
	posW.xyz += side * billboardCorner.x;
	posW.xyz += up * billboardCorner.y;

	// Starting to prepare the output
	PS_IN_fakeshadow vout;
	vout.PosH = mul(mul(float4(posW, 1), ksView), ksProjection);
	vout.PosH.z -= 0.002 * vout.PosH.w;
	vout.Tex = BILLBOARD[vertexID];

	// Calculating adjusted opacity
	float distanceToCamera = length(P.pos - ksCameraPosition);
	float distanceToGround = abs(P.pos.y - P.groundY);
	vout.Opacity = abs(P.opacityS) * lerp(0.2, 0.05, P.dustMix);  // skipping frustum check
	vout.Opacity *= lerpInvSat(P.lifeOut, 0.05, 0.1);             // hiding not released from wheels yet
	vout.Opacity *= lerpInvSat(P.stuckBelow, 0.1, 0);             // hiding stuck below cars
	vout.Opacity *= lerpInvSat(distanceToGround, P.size * 2, 0);  // hiding particles distant from the ground
	vout.Opacity *= lerpInvSat(distanceToCamera, P.size * 2, P.size * 3); // hiding particles distant from the ground
	vout.Opacity *= lerpInvSat(P.size, 0.4, 0.8);                 // hiding small particles
	vout.Opacity *= lerpInvSat(P.size - P.thickFinal, 1.5, 0.5); // hiding large particles

	if (P.targetSize < 0        // hiding long-living
			|| P.thickBase > 1.5){  // hiding very dense
		vout.Opacity = 0;
	}

	// Hiding particles if there are too many
	float particlesLimit = 100;
	float showChance = saturate(particlesLimit / abs(P.cellCount));
	float showRandom = frac(P.random * 3883.672);
	vout.Opacity *= lerpInvSat(showRandom, showChance + 0.12, showChance);
	vout.Opacity *= pow(1 - calculateFogNew(P.pos - ksCameraPosition.xyz, (P.pos - ksCameraPosition.xyz) / distanceToCamera), 2);

	[branch]
	if (vout.Opacity < 0.02){
		DISCARD_VERTEX(PS_IN_fakeshadow);
	}

	return vout;
}