// #define FORCE_DISC_SHADOWS
// #define SHADOW_POISSON_DISK_SIZE 8
// #define SHADOW_POISSON_DISK_SCALE 20
// #define MODE_LIGHTMAP
#define USE_LAST_SHADOWS_CASCADE_ONLY
#include "include/common.hlsl"
#include "pfxSmoke.hlsl"
#include "include_new/base/_include_vs.fx"
#include "pfxSmoke_draw.hlsl"
#include "include_new/ext_shadows/_include_vs.fx"

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float4 Color : TEXCOORD0;
};

ByteAddressBuffer txCounterBuffer : register(TX_SLOT_MAT_0);

float shadowContribution(float3 pos, float3 posC){	
  #ifdef NO_SHADOWS
    return 1;
  #else
    float4 p = float4(pos, 1);
    return getShadowBiasMult(posC, 0, float3(0, 1, 0), mul(p, ksShadowMatrix0), 0, 1);	
  #endif
}

VS_Copy main(uint id: SV_VERTEXID) {
  VS_Copy vout;
  float2 v = float2(id % 2, id / 2) * 2;
  vout.PosH = float4(v * 2 - 1, 0, 1);

  uint2 encodedSmoke = txCounterBuffer.Load2(PARTICLECOUNTER_OFFSET_CAMERA_OCCLUSION_RG);  
  float4 cameraSmoke = float4(uint4(
    (encodedSmoke.x >> 16) & 0xffff, encodedSmoke.x & 0xffff, 
    (encodedSmoke.y >> 16) & 0xffff, encodedSmoke.y & 0xffff)) / float4(64, 64, 64, 256);
  float3 color = cameraSmoke.rgb / max(0.001, max(cameraSmoke.r, max(cameraSmoke.g, cameraSmoke.b)));  

  float3 posC = -ksLightDirection.xyz * 0.5;
  float shadow = shadowContribution(posC + ksCameraPosition.xyz, posC);

  float3 lighting = GAMMA_LINEAR(color * GAMMA_NONPBR_MULT) * (AMBIENT_COLOR_NEARBY + ksLightColor.xyz * shadow * 0.35) * 0.2;
  float intensity = cameraSmoke.w / (0.8 + cameraSmoke.w);
  vout.Color = float4(lighting, intensity);

  return vout;
}
