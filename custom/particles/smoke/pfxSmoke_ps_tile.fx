#include "pfxSmoke.hlsl"
#include "include_new/base/_include_ps.fx"
#include "pfxSmoke_draw.hlsl"

Texture2D txSmokeNoise : register(t4);

RESULT_TYPE main(PS_IN_tile pin) {
  float alpha = pin.Opacity * saturate(1 - dot2(pin.Tex)) * 0.6;
  clip(alpha - 0.1);

  float3 val = saturate(1 - pin.DistanceToCamera / 600);
  RESULT_TYPE ret;
  ret.result = float4(val, alpha);
  return ret;
}