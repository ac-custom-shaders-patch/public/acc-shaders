// Based on article “GPU-based particle simulation” by Turánszki János:
// https://turanszkij.wordpress.com/2017/11/07/gpu-based-particle-simulation/

#include "include/common.hlsl"
#include "include/normal_encode.hlsl"
#include "include/color_encode.hlsl"
#include "include/packing_cs.hlsl"

#define GRID_ITEM_SIZE 16   // sizeof(uint4)
#define GRID_SIZE 512       // radius is GRID_SIZE * GRID_CELL_SIZE / 2, so 5 km
#define GRID_CELL_SIZE 20.  // size of each cell in meters

struct Particle {
	float3 pos;          // position, graphics space
	float life;          // remaining life in seconds

	float3 velocity;     // velocity, m/s
	float size;          // particle radius in meters

	uint wheelID;        // index of a wheel, can be pointing to fake wheel with radius below zero for smoke not linked to cars
	float random;        // random value assigned on spawn
	float phase;         // ID of spawned particle, from 0 to 5 (6 is maximum number of particles per emitter per frame)
	float thickBase;     // thickness from 0 to 1, 1 for more heated and thick smoke, base value; 2 for small dense particles (for dirt and splashes)

	float stuckPos;      // position along wheel circumference for a particle stuck on a wheel
	float stuckSpeed;    // angular speed of a particle rotating around wheel
	float stuckSide;     // position along X axis relative to a wheel
	float stuckDrift;    // speed of a particle moving along X axis relative to a wheel

	float2 uvOffset;      // texture offset for main rendering, takes into account rotation of a billboard
	float2 uvBaseOffset;  // accumulative texture offset calculated from particle speed relative to camera

	float stuckBelow;     // timer for particles stuck below their cars
	float groundY;        // height of ground related to particle (set during its spawn or state change)
	float targetSize;     // negative for long-living particles, stores flag for unnecessary particles removal
	float cellCount;      // number of particles in nearby cell, negative if interpolation is active

	uint color;           // encoded color
	uint colorBase;
	float opacityS;       // general opacity, negative for frustum culled particles
	float opacityK;       // one divided by total life span, for simple life×opacityK opacity multiplier

	float thickFinal;     // applied thickness: reduces if particle doesn’t have enough neighbours
	float growK;          // parameter controlling grow rate
	float colorConsistency;
	float lifeOut;
	float dustMix;
	float splashMix;
	uint longLivingIndex;

	// Alternate meaning:
	#define _targetVelocityY stuckDrift
	#define _flags longLivingIndex
};

#define FLAG_FADE_IN 1
#define FLAG_COLLISION_DUST 2
#define FLAG_REGULAR_LIFE_REDUCTION 4
#define FLAG_REDUCED_COLLISIONS 8
#define FLAG_SLOWER_FADE_IN 16
#define FLAG_REDUCED_MOTION 32
#define FLAG_REDUCED_FORCES 64
#define FLAG_STUCK_SHADOWED 128
#define FLAG_NO_COLLISIONS 256
#define FLAG_GROUND_SPAWN 512
#define P_HAS_FLAG(flag) ((P._flags & (flag)) ? 1 : 0)

struct Particle_packed {
	float3 pos;
	float life;

	float3 velocity;
	float size;

	uint wheelID;
	float random;
	float opacityS;
	float opacityK;

	float2 uvOffset;
	float2 uvBaseOffset;

	uint color;
	uint colorBase;
	float lifeOut;
	float groundY;

	uint longLivingIndex;
	uint dustMix_splashMix;
	uint growK_colorConsistency;
	uint phase_thickBase;

	uint stuckPos_stuckSpeed;
	uint stuckSide_stuckDrift;
	uint stuckBelow_targetSize;
	uint cellCount_thickFinal;
};

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.pos, _U.pos);\
	FN(_P.life, _U.life);\
	FN(_P.velocity, _U.velocity);\
	FN(_P.size, _U.size);\
	FN(_P.random, _U.random);\
	FN(_P.wheelID, _U.wheelID);\
	FN(_P.uvOffset, _U.uvOffset);\
	FN(_P.uvBaseOffset, _U.uvBaseOffset);\
	FN(_P.groundY, _U.groundY);\
	FN(_P.color, _U.color);\
	FN(_P.colorBase, _U.colorBase);\
	FN(_P.opacityS, _U.opacityS);\
	FN(_P.opacityK, _U.opacityK);\
	FN(_P.lifeOut, _U.lifeOut);\
	FN(_P.longLivingIndex, _U.longLivingIndex);\
	FN(_P.dustMix_splashMix, _U.dustMix, _U.splashMix);\
	FN(_P.growK_colorConsistency, _U.growK, _U.colorConsistency);\
	FN(_P.phase_thickBase, _U.phase, _U.thickBase);\
	FN(_P.stuckPos_stuckSpeed, _U.stuckPos, _U.stuckSpeed);\
	FN(_P.stuckSide_stuckDrift, _U.stuckSide, _U.stuckDrift);\
	FN(_P.stuckBelow_targetSize, _U.stuckBelow, _U.targetSize);\
	FN(_P.cellCount_thickFinal, _U.cellCount, _U.thickFinal);

#define P_IS_STUCK (P.stuckSide != 0)

Particle_packed PFX_pack(Particle _U){
	Particle_packed _P;
	PARTICLES_PACKING_RULES(__pack);
	return _P;
}

Particle PFX_unpack(Particle_packed _P){
	Particle _U;
	PARTICLES_PACKING_RULES(__unpk);
	return _U;
}

struct ParticleCounters {
	uint aliveCount;
	uint deadCount;
	uint realEmitCount;
	uint aliveCount_afterSimulation;
};

static const uint PARTICLECOUNTER_OFFSET_ALIVECOUNT = 0;
static const uint PARTICLECOUNTER_OFFSET_DEADCOUNT = 4;
static const uint PARTICLECOUNTER_OFFSET_REALEMITCOUNT = 8;
static const uint PARTICLECOUNTER_OFFSET_ALIVECOUNT_AFTERSIMULATION = 12;

static const uint PARTICLECOUNTER_OFFSET_LONGLIVING_INDEX = 16;
static const uint PARTICLECOUNTER_OFFSET_LONGLIVING_TOTAL = 20;
static const uint PARTICLECOUNTER_OFFSET_CAMERA_OCCLUSION_RG = 24;
static const uint PARTICLECOUNTER_OFFSET_CAMERA_OCCLUSION_BA = 28;

struct Wheel {
	float3 pos;
	float radius;

	float3 dir;
	float width;

	float3 up;
	float angularSpeed;

	float3 outside;
	float steered;

	float3 velocity;
	uint wheelFlags;

	float3 axisCenter;
	float axisRadius;

	uint pad0;
	float stuckOffset; // 1
	float flyoffStart; // 0
	float flyoffEnd; // 0.15

	float flyoffDelay; // 2
	float stuckMaxSpeed; // 8…12
	float blockStart; // 1 (0.4)
	float blockEnd; // 1 (0.6)

	float emitSizeA; // 0.08
	float emitSizeB; // 0.1
	float stuckDriftDtMult;
	float flyoffSpeedMult;
};

#define FLAG_OPEN_WHEEL 1
#define WHEEL_HAS_FLAG(flag) (wheel.wheelFlags & (flag))

struct CollisionPlane {
	float3 pos;
	float fwd_x;
	float3 up;
	float fwd_y;
	float3 side;
	float fwd_z;
};

struct Car {
	float3 carVelocity;
	float carSpeed;

	float3 shadowCenter;
	float shadowOpacity;

	float3 shadowVecH;
	float carWidth;

	float3 shadowVecV;
	uint carFlags;

	// float4 bs[3];
	// CollisionPlane centerPlane;

	float drivenDistance;
	float gforceX;
	float angularSpeedK;
	float _pad;

	float4 collisionPlane;
};

#define FLAG_CAR_DISALLOW_STUCK 1
#define FLAG_CAR_JUMPED 2
#define CAR_HAS_FLAG(flag) (car.carFlags & (flag))

struct EmittingSpot {
	uint emitCount;
	float3 contactPoint;

	uint wheelID;
	float randomSeed;
	uint intensity_surfaceK;
	float thickness;  // negative for wheel particles which should spawn from the middle of the car

	float dustMix;
	float splashMix;
	float life;
	float size;

	float3 velocity;
	float targetYVelocity;

	float groundY;
	float colorConsistency;
	uint color;
	uint soilColor;

	// Alternative meaning for non-wheel particles:
	#define _spreadK dustMix
	#define _growK splashMix
	#define _emitterFlags soilColor
};

#define THREADCOUNT_EMIT 256
#define THREADCOUNT_SIMULATION 256
#define FORCE_FIELDS 8
#define COLLISION_PLANES 4
#define COLLISION_SPHERES 18  // 3 per car, 6 cars

struct ForceField {
	float3 pos;
	float radiusSqrInv;
	float3 velocity;
	float forceMult;
};

#if defined(TARGET_CS)
	#include "include/random_cs.hlsl"

	cbuffer cbSimulationData : register(b8) {
		uint gEmittersCount;
		uint gEmitCountTotal;
		uint gMaxEmitCountPerGroup;
		float gEmitterRandomness;

		float gFrameTime;
		float3 gCameraPosition;

		float3 gOriginOffset;
  	uint gTripleFrustum;

		float4 gViewFrustum1;
		float4 gViewFrustum2;
		float4 gViewFrustum3;
		float4 gViewFrustum4;

		float gFovTan;
		float gQuantityScale;
		float gLongLastingChance;
		float gCoverCamera;

		float2 gMapPointA;
		float2 gMapPointB;

		float4x4 xVP;

		uint meshIndexCount0;
		uint meshIndexCount1;
		float gRainIntensity;
		float gRainWater;

		float2 extWindVel;
		float extWindSpeed;
		float extWindWave;

		ForceField xForceFields[FORCE_FIELDS];
		CollisionPlane xCollisionPlanes[COLLISION_PLANES];
		float4 xCollisionSpheres[COLLISION_SPHERES];

		float4x4 gCarCollisionTransform0;
		float4x4 gCarCollisionTransform1;
		float4x4 gAreaHeightmapTransform;
	};

	#define gGravity 9.81 // TODO: move to cbuffer

	bool isVisible(float3 pos, float halfSize){
		// return true;
		if (gTripleFrustum){
			return gTripleFrustum == 2 
				|| dot(gViewFrustum1.xyz, pos) - gViewFrustum1.w > -halfSize
				|| dot(gViewFrustum2.xyz, pos) - gViewFrustum2.w > -halfSize;
		} else {
			return dot(gViewFrustum1.xyz, pos) - gViewFrustum1.w > -halfSize
				&& dot(gViewFrustum2.xyz, pos) - gViewFrustum2.w > -halfSize
				&& dot(gViewFrustum3.xyz, pos) - gViewFrustum3.w > -halfSize
				&& dot(gViewFrustum4.xyz, pos) - gViewFrustum4.w > -halfSize;
		}
	}

	int getCellId(Particle P){		
    float2 cameraRounded = floor(gCameraPosition.xz / 100) * 100;
    int2 cell = (int2)floor((P.pos.xz - cameraRounded) / GRID_CELL_SIZE) + (GRID_SIZE / 2);
    return (cell.x + cell.y * GRID_SIZE) * GRID_ITEM_SIZE;
	}

	bool isCellIdValid(int cellId){
		return cellId >= 0 && cellId < GRID_SIZE * GRID_SIZE * GRID_ITEM_SIZE;
	}

	#define applyDrag(value, target, speedMultiplier)\
		value = lerp(target, value, 1 / (1 + gFrameTime * speedMultiplier));
#endif

float getPlaneDistance(float3 planeNm, float planeDistance, float3 pos) {
  return dot(planeNm, pos) - planeDistance;
}

static const uint ARGUMENTBUFFER_OFFSET_DISPATCHEMIT = 0;
static const uint ARGUMENTBUFFER_OFFSET_DISPATCHSIMULATION = ARGUMENTBUFFER_OFFSET_DISPATCHEMIT + (3 * 4);
static const uint ARGUMENTBUFFER_OFFSET_DRAWPARTICLES = ARGUMENTBUFFER_OFFSET_DISPATCHSIMULATION + (3 * 4);
