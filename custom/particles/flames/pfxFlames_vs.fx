#include "pfxFlames.hlsl"

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	#ifdef MODE_MAIN
		uint vertexID = fakeIndex % 6;
		uint instanceID = fakeIndex / 12;
		bool facingView = fakeIndex % 12 >= 6;
		Particle particle = PFX_unpack(particleBuffer[aliveList[instanceID]]);
		float3 quadPos = BILLBOARD[vertexID];
	#else
		LOAD_PARTICLE_BILLBOARD
		bool facingView = false;
	#endif

	PS_IN vout;

	float bSize = lerp(0.01, 0.03, abs(particle.size)) 
		* lerp(0.4, 1, particle.size < 0 ? 0 : saturate((1 - particle.life) * 5))
		* lerp(4, 2, particle.life)
		* (particle.limit < 0 ? lerp(2, 1, particle.life) : 1);

	bool extraBig = particle.size > 0 && frac(particle.size * 12665.63) > 0.8;
	if (extraBig){
		bSize *= lerp(3, 1, particle.life);
	}
	float fSize = bSize * particle.initialSize;

	float3 posC = particle.pos - ksCameraPosition.xyz;
	float3 velC = particle.smoothVelocity - particle.carVelocity;
	float3 billboardAxis = normalize(posC);
	#ifdef MODE_SHADOW
		billboardAxis = ksLightDirection.xyz;
	#endif

	float3 velocity = dot(velC, 1) ? velC : float3(0, 1, 0);
	float speed = length(velocity);
	float3 velocityDir = velocity / speed;

	float3 side, up;
	if (facingView){
		side = normalize(cross(billboardAxis, float3(0, 1, 0)));
		up = normalize(cross(billboardAxis, side));
	} else {		
		side = normalize(cross(billboardAxis, velocityDir));
		up = normalize(cross(billboardAxis, side));
	}

	float facingK = abs(dot(velocityDir, billboardAxis)) - frac(particle.size * 86341.61) * 0.2;
	float facingAdjustment = saturate(remap(facingK, 0.7, 0.8, 1, 0));
	#if defined(MODE_MAIN) || defined(MODE_GBUFFER)
		vout.CutOff = facingAdjustment;
		if (!facingView){
			vout.CutOff = saturate(remap(facingK, 0.85, 0.75, 1, 0));
		}
	#endif

	vout.Tex = quadPos.xy;
	vout.Life = particle.life;

	float colorAlpha = unpackColor(particle.color).a;
	float estimateSize = particle.initialSize / 4;
	// colorAlpha *= saturate(length(posC) / estimateSize - 1);
	// colorAlpha = 0;
	// particle.color = particle.color & 0xffffff | ((int)(colorAlpha * 255) << 24);

	#ifdef MODE_MAIN
		vout.SoftKMult = lerp(10, 50, colorAlpha) * lerp(lerp(0.01, 0.1, colorAlpha), 1, pow(saturate(particle.life), 2)) / particle.initialSize;
	#endif
	
	float limitSat = saturate(particle.limit);
	vout.LifeAdj = saturate((particle.life - limitSat) / (1 - limitSat) * lerp(4, 1, limitSat));
	vout.LifeAdj *= saturate(length(posC) / estimateSize - 1);

  #ifndef MODE_GBUFFER
		vout.Color = particle.color;
		vout.TemperatureMult = abs(particle.temperatureMult);
		if (vout.LifeAdj == 0){
			DISCARD_VERTEX(PS_IN);
		}
	#else
		if (colorAlpha < 0.5 || vout.LifeAdj == 0){
			DISCARD_VERTEX(PS_IN);
		}
	#endif

		// if (extraBig){
		// 	DISCARD_VERTEX(PS_IN);
		// }

	#ifdef MODE_LIGHTS
		[branch]
		// if (particle.size < 0 || frac(particle.size * 43261.21) < 0.97){
		if (particle.size < 0 || frac(particle.size * 43261.21) < 0.95 || particle.initialSize > 2 || vout.LifeAdj == 0){
			DISCARD_VERTEX(PS_IN);
		}

		float3 offset = (side * quadPos.x + up * quadPos.y) * 0.8;
		vout.Offset = frac(particle.size * 52447.73);
	#else
		float noiseScale = frac(particle.size * 43261.21);
		vout.TexAlt = quadPos.xy * bSize * (particle.limit < 0 ? 0.25 : 0.8) * lerp(0.5, 1, noiseScale) * (extraBig ? 0.5 : 1)
			+ float2(frac(particle.size * 76574.61), frac(particle.size * 32659.73));
	  #if defined(MODE_MAIN) || defined(MODE_GBUFFER)
			vout.NoiseOffset = float2(dot(particle.pos, side), -dot(particle.pos, up)) * 0.002;
		#endif
		vout.Skew = (1 - abs(dot(velocityDir, billboardAxis))) * lerp(0.01, 0.04, frac(particle.size * 37761.75)) 
			* clamp(1 - particle.life, 0, 0.35) * lerp(0.5, 1, noiseScale) * facingAdjustment;
		vout.Fidelity = saturate(0.2 + sqrt(frac(particle.size * 71175.61))) * saturate(particle.life * 1.4);

		float3 offset = (side * quadPos.x + up * quadPos.y) * fSize;
		float3 stretchOffset = velocity * dot(velocityDir, normalize(offset));
		offset += lerp(0.02, 0.05, saturate((1 - particle.life) * 5 - 0.5)) * stretchOffset * facingAdjustment * saturate(particle.life * 2);
	#endif

	float4 posW = float4(particle.pos + offset, 1);
	float4 posV = mul(posW, ksView);
	vout.PosH = mul(posV, ksProjection);

	#if defined(MODE_LIGHTS)
		vout.PosC = particle.pos - ksCameraPosition.xyz;
	#elif defined(MODE_SHADOW)
		vout.Offset2 = uint2(frac(particle.size * 47134.21) * 2, frac(particle.size * 23973.74) * 2);
	#else
		vout.PosC = posW.xyz - ksCameraPosition.xyz;
		vout.Fog = calculateFog(posV);
		#ifdef MODE_GBUFFER
			GENERIC_PIECE_VELOCITY(posW, extFrameTime);
		#endif
	#endif

	return vout;
}