#include "pfxFlames.hlsl"

static const uint ARGUMENTBUFFER_OFFSET_DRAWPARTICLES_TWICE = ARGUMENTBUFFER_OFFSET_DISPATCHSIMULATION + (3 * 4) + (4 * 4);

ByteAddressBuffer counterBuffer : register(t0);
RWByteAddressBuffer indirectBuffers : register(u0);

[numthreads(1, 1, 1)]
void main(uint3 DTid : SV_DispatchThreadID) {
	uint particleCount = counterBuffer.Load(PARTICLECOUNTER_OFFSET_ALIVECOUNT_AFTERSIMULATION);
	indirectBuffers.Store4(ARGUMENTBUFFER_OFFSET_DRAWPARTICLES, uint4(particleCount * VERTICES_PER_PARTICLE, 1, 0, 0));
	indirectBuffers.Store4(ARGUMENTBUFFER_OFFSET_DRAWPARTICLES_TWICE, uint4(particleCount * VERTICES_PER_PARTICLE * 2, 1, 0, 0));
}
