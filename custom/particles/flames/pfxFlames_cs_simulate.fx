#include "pfxFlames.hlsl"

Texture3D txNoiseVolume : register(t11);
Texture2D txNoise : register(TX_SLOT_NOISE);

void forceFields(float3 pos, inout float3 velocity) {
  for (uint i = 0; i < FORCE_FIELDS; i++) {
    if (gForceFields[i].radiusSqrInv == 0) break;
    float3 diff = pos - gForceFields[i].pos;
    float dist = dot2(diff);
    float distK = saturate((1 - dist * gForceFields[i].radiusSqrInv) * 2);
    float3 forcePushAway = normalize(diff) * gForceFields[i].forceMult;
    velocity += distK * distK * forcePushAway * gFrameTime * 80;

    float3 forceVelSync = gForceFields[i].velocity - velocity;
    velocity += saturate(distK * distK * gFrameTime * 20) * forceVelSync;
  }
}

#define MAX_WIND_SPEED 20
#define WIND_COORDS_MULT 1
float2 windOffset(float3 pos, float windPower, float freqMult){
  #ifdef NO_WIND
    return 0;
  #else
    float waveOffset = saturate(0.1 + extWindSpeed / MAX_WIND_SPEED);
    float wave = sin(extWindWave * 1.773 * freqMult + pos.z * freqMult * WIND_COORDS_MULT / 17) 
      * sin(extWindWave * freqMult + pos.x * freqMult * WIND_COORDS_MULT / 13);
    return extWindVel * windPower * (1 + (wave + waveOffset) * 0.5);
  #endif
}

void process(inout Particle particle, uint3 DTid) {
  RAND_INIT(particle.size + particle.life, particle.velocity.xz);
  bool firstFrame = particle.life == 1;

  float drag = 1.0 / (1.0 + gFrameTime * 0.5);
  float lifeRate = frac(abs(particle.size) * 161672.325);
  particle.life -= gFrameTime * lerp(3, 2, pow(lifeRate, 4)) * (particle.size < 0 ? 2 : 1) * (particle.limit < 0 ? 0.5 : 1);
  if (firstFrame) return;

  if (particle.life < 0.9 && frac(abs(particle.size) * 931886.958) > 0.1 && particle.temperatureMult < 0){
    particle.life = 0;
  }
  
  if (particle.size < 0){

    particle.pos += lerp(particle.carVelocity, particle.velocity, 0.2) * gFrameTime;

  } else {

    float4 noise0 = txNoiseVolume.SampleLevel(samLinearWrap, float3(particle.pos.xz * 0.1, gTime * 0.3), 0);
    float4 noise1 = txNoiseVolume.SampleLevel(samLinearWrap, float3(particle.pos.xz * 2, gTime + lifeRate), 0);
    float3 offset = noise0.xyz * 2 - 1;
    offset = pow(abs(offset), 2) * sign(offset) + (noise1.xyz * 2 - 1) * 1.2;

    particle.pos += particle.velocity * gFrameTime;
    particle.velocity = lerp(float3(windOffset(particle.pos, 0.5, 2.5), 0).xzy, particle.velocity, drag);
    // particle.velocity *= drag;
    particle.velocity.y += gFrameTime * 4;
    particle.velocity += offset * gFrameTime * 10;

    particle.smoothVelocity = lerp(particle.velocity, particle.smoothVelocity, drag);

    // particle.velocity += particle.pad * gFrameTime;
    // particle.pad += float3(RAND - 0.5, RAND - 0.5, RAND - 0.5) * gFrameTime * 30;

    if (particle.life < particle.limit){
      particle.life = 0;
    }
    
  }
}
