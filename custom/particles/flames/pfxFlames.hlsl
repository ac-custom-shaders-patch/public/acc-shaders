#define PARTICLES_PACKING
#define PARTICLES_WITHOUT_NORMALS 
#define PARTICLES_SORTING 
#include "particles/common/include_decl.hlsl"

struct Particle {
	float3 pos;
	float life;
	float3 velocity;
	float size;
	float3 carVelocity;
	float rand;
	uint color;
	float initialSize;
	float temperatureMult;
	float3 smoothVelocity;
	float limit;
};

struct Particle_packed {
	float3 pos;
	float life;
	uint2 velocity_size;
	uint2 carVelocity_rand;
	uint color;
	uint initialSize_temperatureMult;
	uint2 smoothVelocity_limit;
};

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.pos, _U.pos);\
	FN(_P.life, _U.life);\
	FN(_P.velocity_size, _U.velocity, _U.size);\
	FN(_P.carVelocity_rand, _U.carVelocity, _U.rand);\
	FN(_P.smoothVelocity_limit, _U.smoothVelocity, _U.limit);\
	FN(_P.initialSize_temperatureMult, _U.initialSize, _U.temperatureMult);\
	FN(_P.color, _U.color);

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		float3 pos;

		float size;
		float3 velocity;

		uint color;
		float3 carVelocity;

		float flameIntensity;
		float temperatureMult;
		uint spawnFlags;
		float wheelAngularSpeed;

		float3 wheelOutside;
		float wheelRadius;

		float3 wheelUp;
		float wheelWidth;
	};

	#define FLAG_KEEP_CAR_VELOCITY 1

	#define FORCE_FIELDS 4

	struct ForceField {
		float3 pos;
		float radiusSqrInv;
		float3 velocity;
		float forceMult;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		float gGravity;
		float gTime;
		float2 gPad0;  

		float2 extWindVel;
		float extWindSpeed;
		float extWindWave;  

		ForceField gForceFields[FORCE_FIELDS];
    // float4x4 gCarCollisionTransform;
	};
	
	// Texture2D<float> txCarCollision : register(t2);
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		PFX_PS_IN
		float Life : TEXCOORD3;
		float LifeAdj : TEXCOORD18;
		#ifndef MODE_GBUFFER
			nointerpolation uint Color : COLOR;
			nointerpolation float TemperatureMult : COLOR1;
		#endif
		#ifdef MODE_LIGHTS
			float Offset : TEXCOORD14;
		#else
			float2 TexAlt : TEXCOORD12;
			float Skew : TEXCOORD14;
			float Fidelity : TEXCOORD15;

			#if defined(MODE_MAIN) || defined(MODE_GBUFFER)
				float2 NoiseOffset : TEXCOORD16;
				float CutOff : TEXCOORD17;
			#endif
			#ifdef MODE_MAIN
				float SoftKMult : TEXCOORD19;
			#endif
		#endif
		#ifdef MODE_SHADOW
			nointerpolation uint2 Offset2 : EXT9;
		#endif
	};
#endif

#include "particles/common/include_impl.hlsl"
