#ifdef MODE_LIGHTS
  #define MIPMAPPED_GBUFFER 2
  #define MIPMAPPED_GBUFFER_FIXED
  #include "general/accSS.hlsl"
#endif

#include "pfxFlames.hlsl"

#ifdef MODE_MAIN
  #include "include_new/ext_functions/depth_map.fx"
#endif

#include "include/bayer.hlsl"

float4 sampleNoise(float2 uv, bool fixUv = false){
  if (fixUv){
    float textureResolution = 32;
    uv = uv * textureResolution + 0.5;
    float2 i = floor(uv);
    float2 f = frac(uv);
    uv = i + f * f * (3 - 2 * f);
    uv = (uv - 0.5) / textureResolution;
  }
	return txNoise.SampleLevel(samLinearSimple, uv, 0);
}

void flameAdjust(inout float f){
  f += f - f * f * (3 - 2 * f);
}

#ifdef MODE_LIGHTS
  float ssGetDepth2(float2 uv){
    float ret = 0;
    [unroll]
    for (int y = -1; y <= 1; ++y) {
      [unroll]
      for (int x = -1; x <= 1; ++x) {
        if (abs(x) + abs(y) == 2) continue;
        ret = max(ret, txDepth.SampleLevel(samPointClamp, uv, 0, int2(x, y)).x);
      }
    }
    return ret;
  }
#endif

float4 sampleNoiseFlame(float2 uv){
  float textureResolution = 32;
  uv = uv * textureResolution + 0.5;
  float2 i = floor(uv);
  float2 f = frac(uv);
  flameAdjust(f.x);
  flameAdjust(f.y);
  uv = i + f;
  // uv = i + f * f * f * (f * (f * 6 - 15) + 10);
  uv = (uv - 0.5) / textureResolution;
	return txNoise.SampleLevel(samLinearSimple, uv, 0);
}

float3 getHeatColor(float h) {
  float T = h * 5600;
  if (T < 400) return 0;
  float3 f = float3(1, 1.5, 2);
  return f * f * f / (exp(f * 19e3 / T) - 1);
}

#ifdef MODE_LIGHTS
  float4 main(PS_IN pin) : SV_TARGET {
#else
  PS_OUT main(PS_IN pin) {
#endif

  float life = saturate(pin.Life);
  float lifeTime = saturate(1 - pin.Life);
  float temperatureEstimate = lerp(1, 0.1, pow(lifeTime, 1.2));

  #ifdef MODE_MAIN
    float depthZ = getDepthAccurate(pin.PosH);
    float depthC = linearizeAccurate(pin.PosH.z);
    float softK = saturate((depthZ - depthC) * pin.SoftKMult);
  #else
    float softK = 1;
  #endif

  #ifdef MODE_GBUFFER
    float3 heatColor = 0;
  #else
    float3 heatColor = getHeatColor(temperatureEstimate * pin.TemperatureMult);
    heatColor = heatColor / max(1, luminance(heatColor) * 40) * unpackColor(pin.Color).rgb;
  #endif

  float dist = length(pin.Tex);
  #ifdef MODE_LIGHTS
    float2 ssUV = pin.PosH.xy * gSize.zw;
    #ifdef MODE_LIGHTS_EXTRA
      ssUV *= 2;
      float depth = ssGetDepth(ssUV);
    #else
      float depth = ssGetDepth(ssUV);
    #endif

    float3 origin = ssGetPos(ssUV, depth);
    float3 normal = ssGetNormal(ssUV);
    float3 toLight = origin - pin.PosC;
    float alpha = pow(saturate(1 - length(toLight) / 0.8), 2) * sqrt(saturate(-dot(toLight, normal))) * saturate(remap(dist, 0.9, 1, 1, 0));

    #ifdef MODE_LIGHTS_EXTRA
      float3 random = normalize(txNoise.SampleLevel(samPoint, (pin.PosH.xy + extFrameNum) / 32 + pin.Offset, 0).xyz);
      for (int i = 1; i < 10; ++i){
        float3 pos = lerp(origin, pin.PosC + (random.yyz - 0.5) * 0.03, ((float)i + random.x) / 10 * 0.7);
        float3 uv = ssGetUv(pos);
        float actualDepth = ssGetDepth(uv.xy, true);
        if (actualDepth < uv.z - 0.0002){
          alpha = 0; 
        }
      }

      // float3 bayer = getBayer(pin.PosH);
      // for (int i = 1; i < 10; ++i){
      //   float3 pos = lerp(origin, pin.PosC, ((float)i + bayer) / 10 * 0.7);
      //   float3 uv = ssGetUv(pos);
      //   float actualDepth = ssGetDepth(uv.xy, true);
      //   if (actualDepth < uv.z - 0.0002){
      //     alpha = 0; 
      //   }
      // }
    #endif

    float3 lighting = 70 * heatColor * alpha * gLightsSceneMult;
    if (GAMMA_FIX_ACTIVE) {
      lighting = pow(lighting, 2) * 10 * getEmissiveMult() * GAMMA_SCREENLIGHT_MULT;
    }

    // return float4(100 * (depth == 0 ? 1 : 0), (depth == 0 ? 0 : 1), 0, 0);
    // return float4(100 * (depth == 0 ? 1 : 0), (depth == 1 ? 0 : 1), 0, 0);
    // return float4(normal * 0.1, 1);
    // return float4(frac(origin) * 0.1, 0);
    // return float4(saturate(-dot(toLight, normal)), 0, 0, 0);
    // return float4(0, frac(depth * 100) * 0.01, 0, 0);
    // return float4(1 * saturate(1 - length(toLight) / 0.9), 0, 0, 0);
    // return float4(1 * saturate(-dot(toLight, normal)), 0, 0, 0);
    return float4(lighting, 0);
  #else
    #ifndef MODE_SHADOW
      READ_VECTOR_TOCAMERA
    #endif

    float falloff = saturate(remap(dist, 0.5, 1, 0, 1));
    float young = pow(life, 20);
    float noiseScale = lerp(0.05, 0.2, pin.Fidelity) * 0.5;

    pin.TexAlt.y -= pow(abs(pin.Tex.x), 2) * pin.Skew;
    float4 txNoise0 = sampleNoise(pin.TexAlt + lifeTime * 2 * float2(0.02, 0.08), true);
    float4 txNoise1 = sampleNoiseFlame(pin.TexAlt + 0.2 + txNoise0.xy * 0.03 + lifeTime * 2 * float2(-0.02, 0.08));
    float4 txNoiseValue0 = sampleNoise(pin.TexAlt * 2.9 + 0.4 + txNoise1.xy * noiseScale + lifeTime * float2(0.02, 0.08), true);
    float4 txNoiseValue1 = sampleNoise(-pin.TexAlt * 1.9 + 0.1 + txNoise1.xy * noiseScale + lifeTime * float2(-0.02, 0.08), true);

    // float4 txNoiseValue = txNoiseValue0;
    float4 txNoiseValue = 1 - txNoiseValue0 * txNoiseValue1;
    // float4 txNoiseValue = max(txNoiseValue0, txNoiseValue1);

    float detailScale = lerp(0.4, 0, pin.Fidelity);
	  #if defined(MODE_MAIN) || defined(MODE_GBUFFER)
      float2 noiseOffset = pin.NoiseOffset;
      float cutOff = pin.CutOff;
    #else
      float2 noiseOffset = 0;
      float cutOff = 0;
    #endif

    // float4 txNoiseL = sampleNoise(pin.TexAlt * 47 + noiseOffset * 0.2 + lifeTime * float2(0, 1), false);
    // txNoiseValue += (txNoiseL.xyzw - 0.5) * detailScale;
	  // #ifdef MODE_MAIN
    //   float4 txNoiseH = sampleNoise(pin.TexAlt * 81 + noiseOffset + lifeTime * float2(0, 1), true);
    //   float4 txNoiseQ = sampleNoise(pin.TexAlt * 177 + noiseOffset * 5 + lifeTime * float2(0, 1), true);
    //   txNoiseValue += (txNoiseH.xyzw - 0.5) * detailScale * 0.5;
    //   txNoiseValue += (txNoiseQ.xyzw - 0.5) * detailScale * 0.25;
    // #endif

    float flame = pow(txNoiseValue.x, 2);
    flame = saturate(remap(flame, 0.1, 0.9, 0, 1));

    float3 lighting = 10000 * heatColor * lerp(0.5, pow(txNoiseValue.x, 8), pin.Fidelity);
    if (GAMMA_FIX_ACTIVE) {
      lighting = pow(lighting * 0.25, 2) * getEmissiveMult();
    }

    float alpha = pow(saturate(max(flame, young) - falloff - cutOff), 2) * lerp(0.4, 1, young) * pin.LifeAdj * saturate(lifeTime * 10) * softK;
    #ifdef MODE_SHADOW
      float bayerValue = getBayer(pin.PosH, pin.Offset2);
      clip(alpha + bayerValue);
      alpha = 1;
    #elif defined(MODE_GBUFFER)
      clip(alpha - 0.08);
    #else
      alpha *= saturate(alpha * 3);
    #endif

    alpha = GAMMA_ALPHA(alpha);
    RETURN_BASE(lighting, alpha);
  #endif
}