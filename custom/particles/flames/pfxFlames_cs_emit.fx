#include "pfxFlames.hlsl"

float skew(float v, float k){
	return pow(v, k < 0 ? 1 + k : 1 + 1 / (1 - k));
}

void emit(inout Particle particle, EmittingSpot E, RAND_DECL) {
	particle.pos = E.pos;// + float3(RAND - 0.5, RAND - 0.5, RAND - 0.5) * 0.03;
	
	particle.life = 1;
	// particle.life = RAND > 0.8 ? 1 : 0;
	// particle.life = RAND > 0.97 ? 1 : 0;

	if (RAND > 0.2 + abs(sin(gTime * 2.19 + E.pos.x) * sin(gTime * 2.71 + E.pos.z))){
		particle.life = 0;
	}

	// float3 dirOffset = float3(RAND - 0.5, skew(RAND, sin(gTime * 2.19 + E.pos.x) * sin(gTime * 2.71 + E.pos.z) * 0.9) - 0.25, RAND - 0.5) * 2;
	float3 dirOffset = float3(RAND - 0.5, RAND - 0.5, RAND - 0.5) * 2;
	// particle.pos -= E.carVelocity * gFrameTime;// * lerp(0.5, 1.5, RAND);
	particle.velocity = E.carVelocity + 2 * E.velocity + pow(dirOffset, 2) * sign(dirOffset) * lerp(0.1, 0.4, RAND);
	particle.velocity *= E.size;
	particle.smoothVelocity = particle.velocity;
	particle.carVelocity = E.carVelocity;
	particle.color = E.color;
	particle.size = RAND;
	particle.initialSize = E.size;
	particle.temperatureMult = E.temperatureMult;

	if (sin(gTime * 8) < 0.9){
		// particle.life = 0;
	}

	if (E.flameIntensity > 1){
		particle.size += E.flameIntensity - 1;
		particle.limit = lerp(0.5, -0.5, pow(RAND, 4));
		if (!(E.spawnFlags & FLAG_KEEP_CAR_VELOCITY)) particle.velocity = 0;
		particle.pos = E.pos + E.velocity * (RAND - 0.5) * 10 * gFrameTime;
	} else if (RAND > 0.3){
		particle.size = -1 - particle.size;
		particle.limit = 0.9;
	} else {
		float speed = length(E.carVelocity);
		particle.limit = lerp(0.9 - saturate(speed * 3.6 / 200) * lerp(0.1, 0.4, E.flameIntensity), lerp(0.5, 0, E.flameIntensity), pow(RAND, 20));
		// particle.limit = 0.0;
		// particle.life = 0;
	}

	if (E.wheelRadius){
		bool blownWheel = E.wheelRadius < 0;
		float angle = blownWheel ? RAND * -0.1 - M_PI / 2 : RAND * M_PI * 2;
		float3 wheelFwd = normalize(cross(E.wheelOutside, float3(0, E.wheelWidth < 0 ? 1 : -1, 0)));
		particle.pos += sin(angle) * E.wheelUp * abs(E.wheelRadius);
		particle.pos += cos(angle) * wheelFwd * abs(E.wheelRadius);
		particle.pos += blownWheel 
			? E.wheelOutside * (abs(E.wheelWidth) * (RAND > 0.5 ? 0.5 : -0.5))
			: E.wheelOutside * ((RAND - 0.5) * 0.05 + abs(E.wheelWidth) * 0.5);

		float speedBoost = pow(saturate(dot2(E.velocity) / 400), 1);
		particle.velocity = E.velocity * lerp(blownWheel ? lerp(0.4, 0.8, RAND) : 0.8, 0.92, speedBoost) + (sin(angle + M_PI / 2) * E.wheelUp + cos(angle + M_PI / 2) * wheelFwd) * abs(E.wheelRadius) * E.wheelAngularSpeed * 0.05;
		particle.smoothVelocity = particle.velocity;
		particle.carVelocity = E.carVelocity;
		particle.size = E.flameIntensity * lerp(0.5, 1.5, RAND) * lerp(2, 1, speedBoost);
		particle.limit = RAND * lerp(0.2, 0.6, E.flameIntensity);

		// if (RAND > 0.7){
		// 	particle.pos = E.pos - E.wheelUp * E.wheelRadius;
		// 	particle.velocity = 0;
		// 	particle.smoothVelocity = 0;
		// }
	}
}
