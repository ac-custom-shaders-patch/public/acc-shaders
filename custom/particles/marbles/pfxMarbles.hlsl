#define PARTICLES_PACKING
#define PARTICLES_WITHOUT_NORMALS 
#include "particles/common/include_decl.hlsl"

struct Particle {
	float3 pos;
	float life;

	float3 velocity;
	float size;

	float4 collisionPlane1;
};

struct Particle_packed {
	float3 pos;
	float collisionPlane1w;

	uint2 velocity_size;
	uint2 collisionPlane1xyz_life;
};

#define PARTICLES_OFFSET_ORIGIN(P, OFFSET) \
	P.pos.xyz += OFFSET;\
	P.collisionPlane1.w += dot(P.collisionPlane1.xyz, OFFSET);

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.pos, _U.pos);\
	FN(_P.collisionPlane1w, _U.collisionPlane1.w);\
	FN(_P.velocity_size, _U.velocity, _U.size);\
	FN(_P.collisionPlane1xyz_life, _U.collisionPlane1.xyz, _U.life);

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		float3 emitterPos;

		float emitterSizeMult;
		float3 emitterVelocity;

		float4 collisionPlane1;
	};

	#define FORCE_FIELDS 4

	struct ForceField {
		float3 pos;
		float radiusSqrInv;
		float3 velocity;
		float forceMult;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		float gGravity;
		uint gALS1;
		uint gALS2;
		uint _pad1;

		ForceField gForceFields[FORCE_FIELDS];
	};
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		PFX_PS_IN
		float AmbientMult : COLOR0;
		float CenterK : COLOR1;
		float RandomK : COLOR2;
	};

	/* cbuffer cbMarbles : register(CBUFFER_DRAW_SLOT) {
		float brightnessBase;
		float brightnessExtra;
		float sizeMultiplier;
		float velocityMultiplier;
		float fadingBase;
		float fadingLifeLeftK;
		float motionBlurExtra;
		float gDeltaTime;
	} */
#endif

#include "particles/common/include_impl.hlsl"
