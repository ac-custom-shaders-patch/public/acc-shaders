#include "pfxMarbles.hlsl"

PS_OUT main(PS_IN pin) {
  READ_VECTOR_TOCAMERA
  #ifdef MODE_SIMPLE
    float4 txNoiseValue = 0;
  #else
    float4 txNoiseValue = txNoise.Sample(samLinearSimple, pin.Tex / 10 + pin.RandomK);
  #endif
  float alpha = saturate((1 - dot2(pin.Tex) - txNoiseValue.x) * 10);
  float3 lighting = getAmbientBaseNonDirectional(pin.PosC) * pin.AmbientMult * lerp(1, 0.7 + 0.3 * saturate((1 - dot2(pin.Tex))), pin.CenterK);
  clip(alpha - 0.05);
  RETURN_BASE(lighting, alpha);
}