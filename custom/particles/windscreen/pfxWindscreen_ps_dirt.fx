#define DIRT_MODE
#include "pfxWindscreen.hlsl"

float4 main(PS_IN pin) : SV_TARGET {
  clip(1 - dot2(pin.Tex));
  return pin.Value;
}