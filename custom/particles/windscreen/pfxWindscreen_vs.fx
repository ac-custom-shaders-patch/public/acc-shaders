#include "pfxWindscreen.hlsl"

float4 texToScreen(float2 tex){
  float2 pos = frac(tex);
  pos.y = 1 - pos.y;
  return float4(pos * 2 - 1, 0, 1);
}

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD

	bool isMoving = particle.moved > 0.1;
	// isMoving = false;
  if (particle.life <= 0.00001 || particle.squished > 0){
		PS_IN vout = (PS_IN)0;
		vout.PosH = -1;
		return vout;
	}

	float2 B = BILLBOARD[vertexID].xy;
	float speed = length(particle.velocity);
	float2 side = speed == 0 ? float2(1, 0) : particle.velocity / speed * float2(1, -1);
	float2 up = float2(side.y, -side.x);

	// float size = gScale * particle.sizeMult * lerp(0.0008, 0.0038, particle.size) * saturate(particle.life) * (1 + particle.splashMult * 1.6);
	float size = gScale * particle.sizeMult * lerp(0.0006, 0.0028, particle.size) * saturate(particle.life) * (1 + particle.splashMult * 1.6);
	float stretchMult = speed / 65;
	float2 offset = size * (side * B.x + up * B.y);
	float sizeMult = 1;

	particle.pos -= particle.velocity * gScale * 0./65;
	if (!isMoving){
		offset += side * min(stretchMult, size * 2) * sign(B.x);
		sizeMult += saturate(length(particle.velocity) * 5);
	}

	PS_IN vout;
	vout.PosH = texToScreen(particle.pos) + float4(offset * PARTICLE_SCALE * 1.1 * sizeMult, 0, 0);
	vout.Tex = BILLBOARD[vertexID].xy;
	vout.Velocity = particle.velocity;
	vout.Splash = saturate(particle.splashMult / 3 - 0.5);
	vout.NoiseOffset = particle.rand;
	vout.Intensity = isMoving ? 0 : lerp(0.98, 1, particle.size);
	vout.SizeMult = sizeMult;
	return vout;
}