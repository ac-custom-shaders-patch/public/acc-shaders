#include "pfxWindscreen.hlsl"
#include "include/normal_encode.hlsl"
#include "include/samplers.hlsl"

// Texture2D txNormalsPacked : register(t6);
// Texture2D txNormalsPacked : register(t6);
Texture2D txNormalsX : register(t5);
Texture2D txNormalsY : register(t6);
Texture2D txWipers : register(t7);
Texture2D txWater : register(t8);
Texture2D txOcclusion : register(t10);
Texture2D txNoise : register(t11);
RWTexture2D<float> rwWiperDirt : register(u7);

struct FailedOffDrop {
  float2 pos;
  float2 velocity;
  float size;
  float3 pad;
};

RWStructuredBuffer<FailedOffDrop> buFailedOff: register(u5);
RWByteAddressBuffer buFailedOffCounter: register(u6);

float trail(float4 v){
  return saturate(v.z - v.a * 2);
}

void pushParticleFromWiper(inout float2 pos, float2 dir){
  float offset = 0.005 * gScale;
  float wiperL = txWipers.SampleLevel(samLinearSimple, pos - float2(offset, 0), 2.5).a;
  float wiperR = txWipers.SampleLevel(samLinearSimple, pos + float2(offset, 0), 2.5).a;
  float wiperT = txWipers.SampleLevel(samLinearSimple, pos + float2(0, offset), 2.5).a;
  float wiperB = txWipers.SampleLevel(samLinearSimple, pos - float2(0, offset), 2.5).a;
  pos -= float2(wiperR - wiperL, wiperT - wiperB) * offset;
}

void process(inout Particle particle, uint3 DTid) {
  if (gFrameTime == 0) return;

  // float4 normalsPacked = txNormalsPacked.SampleLevel(samLinearSimple, particle.pos, 0);
  // if (dot(normalsPacked, 1) == 0){
  //   particle.life = 0;
  //   return;
  // }
  // float3 dirX = normalDecode(normalsPacked.xy);
  // float3 dirY = normalDecode(normalsPacked.zw);

  const float dragTracing = 1.0 / (1.0 + gFrameTime * 10);

  if (gFadingLimit > frac(particle.rand * 274.31) * 0.98) {
    particle.life -= gFrameTime * gFadingLimit * lerp(50, 1000, gFadingLimit);
  }

  float4 prevState = txWater.SampleLevel(samLinearSimple, particle.pos, 2);
  float4 prevStateBlurred = txWater.SampleLevel(samLinearSimple, particle.pos, 3);
  float4 prevStateVeryBlurred = txWater.SampleLevel(samLinearSimple, particle.pos, 5);
  float2 localVelocity = prevState.xy / 10;
  float localDensity = saturate(prevStateBlurred.a * 2.5 - 1.5);
  // float localDensity = saturate(prevStateBlurred.a * 3.5 - 2.5);

  if (particle.squished == 0) {
    float looseningMult = lerp(0.1, 1.8, particle.size);
    particle.sticky += gFrameTime * -0.02;
    particle.sticky -= saturate(length(localVelocity) * 3 * looseningMult - 0.1);
    particle.sticky -= localDensity * 0.02 * looseningMult;
    particle.sticky = clamp(particle.sticky, -0.2, 1);
  }

  const float dragWiper = 1.0 / (1.0 + gFrameTime * 10);
  const float dragEasy = 1.0 / (1.0 + gFrameTime * 10);
  const float dragWiperHeavy = 1.0 / (1.0 + gFrameTime * 40);

  float2 wipersPos = particle.pos + (dot(particle.velocity, 1) 
    ? normalize(particle.velocity) * particle.wiperLag * gActualScale * 0.008 : 0);
  particle.wiperLag = lerp(particle.moved ? 1 : 0, particle.wiperLag, dragWiperHeavy);

  float4 wipers = txWipers.SampleLevel(samPointWrap, wipersPos, 0);
  float2 wiperVelocity = (wipers.xy * 2 - 1) * 3.6 * gActualScale / 0.877453;

  float4 wipersPoint = txWipers.SampleLevel(samLinearClamp, wipersPos, 0);
  if (wipersPoint.a > 0.1 && frac(particle.rand * 477.252) < 0.01){
    uint2 dirtPoint = uint2(wipersPoint.z * 255, 0);
    rwWiperDirt[dirtPoint] += clamp(dot2(wiperVelocity) - 0.1, 0, 0.25);
  }

  float4 dirX = txNormalsX.SampleLevel(samLinearClamp, particle.pos, 0);
  float4 dirY = txNormalsY.SampleLevel(samLinearClamp, particle.pos, 0);

  if (particle.moved < 0){
    particle.moved = min(0, particle.moved + gFrameTime * 10);
  } else if (wipers.a > 0.5){
    if (particle.squished > 0 || frac(particle.rand * 197) > 0.99 && dot2(wiperVelocity) > MIN_WIPERS_SPEED){
    // if (particle.squished > 0 || frac(particle.rand * 197) > 0.995 && dot2(wiperVelocity) > MIN_WIPERS_SPEED){
    // if (particle.squished > 0 || frac(particle.rand * 197) > 0.98 && dot2(wiperVelocity) > MIN_WIPERS_SPEED){
      if (!particle.squished){
        particle.sizeMult = particle.pos.x;
        particle.sticky = particle.pos.y;
      }
      particle.squished += gFrameTime * 5;
      particle.velocity = lerp(wiperVelocity, particle.velocity, dragWiper);
      particle.pos += wiperVelocity * 0.8 * gFrameTime / gScale;
      particle.life = 1;
      if (dot2(wiperVelocity) < MIN_WIPERS_SPEED){
        particle.life = 0;
      }
    // } else if (frac(particle.rand * 374.421) > gRainIntensity - 0.6) {
    } else {
      // float channelFailure = gRainIntensity - frac(particle.rand * 374.421);
      if (dot2(wiperVelocity) > MIN_WIPERS_SPEED && !(particle.moved > 0 && dot(particle.velocity, wiperVelocity) < 0)){
        // wiperVelocity *= 0.1;
        particle.velocity = lerp(wiperVelocity * lerp(0.01, 0.1, frac(particle.rand * 3773.7982)), particle.velocity, dragWiperHeavy);

        float skipK = 40; // was 20
        if (frac(particle.rand * 4374.4215) * skipK > prevStateVeryBlurred.a - 0.1) {
          particle.pos += wiperVelocity * gFrameTime / gScale;
        }
        
        // if (frac(particle.rand * 374.421) > gRainIntensity - 0.8) particle.pos += wiperVelocity * gFrameTime / gScale;
        particle.sticky = 0;
        particle.moved = 1;

        // If wiper is going to push particle away from glass, remove it before (otherwise, wiper will leave the glass and particle would remain)
        if (txNormalsX.SampleLevel(samLinearSimple, particle.pos + particle.velocity * 0.06, 0).a < 0.5){
          particle.life = 0;
        }
      } else {
        // if (dot2(particle.velocity) < MIN_WIPERS_SPEED) {
        //   particle.velocity = 0;
        //   particle.sticky = 1;
        //   particle.moved = 0;
        //   particle.life -= gFrameTime;
        // } else {
        //   particle.moved = -1;
        // }

        pushParticleFromWiper(particle.pos, float2(dirX.x, dirY.x));
        particle.velocity = 0;
        particle.sticky = 1;
        particle.moved = 0;
        particle.life -= gFrameTime;
      }
    }
  } else {
    particle.moved = saturate(particle.moved - 0.5);
    // particle.moved = 0;
    if (particle.squished > 0) {
      particle.life = min(1, particle.life - gFrameTime * 20);
      // particle.velocity = 0;
    }
  }

  float4 occlusion = txOcclusion.SampleLevel(samLinearSimple, particle.pos, 0);
  if (occlusion.x < lerp(0.1, 0.5, frac(particle.rand * 165167.931))){
    particle.life = 0;
  }

  if (particle.moved > 0){
    return;
  }

  if (particle.squished) {
    const float dragHeavy = 1.0 / (1.0 + gFrameTime * 30);
    // particle.pos += gScale * gFrameTime * particle.velocity;
    // particle.sizeMult = lerp(particle.pos.x, particle.sizeMult, dragHeavy);
    // particle.sticky = lerp(particle.pos.y, particle.sticky, dragHeavy);
    // particle.pos += gScale * gFrameTime * particle.velocity;
    particle.sizeMult = lerp(particle.pos.x, particle.sizeMult, dragHeavy);
    particle.sticky = lerp(particle.pos.y, particle.sticky, dragHeavy);
  } else {
    particle.life -= DTid.x > gALS2 ? gFrameTime * 100 
      : DTid.x > gALS1 ? gFrameTime * 2 : particle.life < 1 ? gFrameTime : gFrameTime * 0.25;
    if (frac(particle.rand * 13539.5169) > 0.3){
      particle.life -= localDensity * gFrameTime * 5;
    }

    float blurredMask = txNormalsX.SampleLevel(samLinearSimple, particle.pos, 4).a;
    float actualEdge = saturate(remap(blurredMask, 0.9, 0.6, 0, 1));
    float edge = saturate(remap(blurredMask, 0.9, 0.7, 0, 1)) * 0.9;

    particle.life -= saturate(edge * 4 - 3) * gFrameTime * 2;
    particle.splashMult *= dragEasy;

    if (gFlyOff){
      edge = 0;
    }

    float3 normal = normalize(cross(dirX.xyz, dirY.xyz));
    float airResistanceK = saturate(dot(normal, gCarVelocity) * lerp(1, -1, saturate(dirY.w)) / 40);
    particle.sizeMult = lerp(lerp(0.75, 0.55, airResistanceK), particle.sizeMult, dragEasy);
    particle.life -= airResistanceK * gFrameTime * 0.5;

    if (gCollisionIntensity > 0 
      && dot(gCollisionDir, normal) > 0.1
      && particle.splashMult < 0.5 
      && gCollisionIntensity > frac(particle.rand * 196173.61) * 0.5) {
      particle.life = 0;
    }

    if (gFlyOff){
      float lessBlurredMask = txNormalsX.SampleLevel(samLinearSimple, particle.pos, 2).a;
      if (lessBlurredMask < 0.9) {
        particle.life = 0;

        float openValue = occlusion.y;
        if (openValue > 0.95) {
          FailedOffDrop fod = (FailedOffDrop)0;
          fod.pos = particle.pos;
          fod.velocity = gScale * particle.velocity;
          fod.size = particle.size * particle.sizeMult;

          uint newAliveIndex;
          buFailedOffCounter.InterlockedAdd(0, 1, newAliveIndex);
          if (newAliveIndex < 64){
            buFailedOff[newAliveIndex] = fod;
          }
        }
      }
    } 
    
    if (particle.moved == 0 && (!gFlyOff || actualEdge < 0.8)) {
      float4 noise = txNoise.SampleLevel(samLinearSimple, particle.pos / gScale * 128 * gScale, 0);
      float2 offsetBase = noise.xy * 2 - 1;
      float2 offset = sign(offsetBase.xy) * saturate(abs(offsetBase.xy) * 5 - 3.5);
      // float3 gVelocity = float3(0, 1, 0);
      float3 velocityW = gVelocity;// * lerp(0.5, 1.5, pow(gRainIntensity, 3)); 
      velocityW += (gVelocitySide0 * offset.x + gVelocitySide1 * offset.y) * (1 - abs(dot(normal, gVelocity))) * min(length(gVelocity) * 2, 2);
      velocityW += float3(0, gCarLift * (occlusion.z * 2 - 1), 0);
      // velocityW = float3(0, 1, 0);
      // velocityW = 0.0001;
      float2 accelU = float2(dot(velocityW, dirX.xyz), dot(velocityW, dirY.xyz));
      float2 accelUBase = accelU;
      float2 accelDir = normalize(accelU);

      // Force pushing raindrops apart
      // float2 nextPos = particle.pos + particle.velocity * 0.0;
      // float4 waterC = txWater.SampleLevel(samLinearSimple, nextPos, 3);
      // float4 waterL = txWater.SampleLevel(samLinearSimple, nextPos + float2(-0.001, 0), 3);
      // float4 waterT = txWater.SampleLevel(samLinearSimple, nextPos + float2(0, -0.001), 3);
      // float2 pushingForce = float2(waterL.a - waterC.a, waterT.a - waterC.a);
      // pushingForce = dot(pushingForce, 1) ? normalize(pushingForce) : 0;
      // pushingForce *= particle.sticky <= 0 && particle.moved == 0 ? saturate(prevStateBlurred.a * 2.5 - 0.5) : 0;
      // particle.velocity += gFrameTime * pushingForce * saturate(dot(normalize(pushingForce), normalize(accelU)) * 10);

      // Force moving raindrops to trails of other raindrops
      if (dot2(accelU) > 0.000001){
        float2 samplingDir = accelDir.yx * float2(1, -1);
        float2 samplingCenter = particle.pos + accelDir * 0.02 * gScale;
        float waterC = trail(txWater.SampleLevel(samLinearSimple, samplingCenter, 2.5));
        float waterL = trail(txWater.SampleLevel(samLinearSimple, samplingCenter + samplingDir * 0.01 * gScale, 2.5));
        float waterR = trail(txWater.SampleLevel(samLinearSimple, samplingCenter - samplingDir * 0.01 * gScale, 2.5));
        accelU += samplingDir * ((waterL - waterC) + (waterC - waterR)) * -0.003;
      }

      particle.splashMult = max(particle.splashMult, clamp(dot(velocityW, normal) * 0.2, 0, 0.6));
      
      if (particle.life > PARTICLE_LIFESPAN){
        particle.life -= gFrameTime;
        particle.velocity += accelU * gFrameTime;
      } else {
        float2 totalAcceleration = accelU * (particle.sticky <= 0 ? lerp(30, 10, edge) : 0);// * saturate(1 - particle.splashMult);
        // float2 totalAcceleration = accelU * 40;// * saturate(1 - particle.splashMult);

        float curSpeed = length(particle.velocity);
        float localBoostedDensity = saturate(prevStateBlurred.a * 2.5 - 0.5);
        const float drag = 1.0 / (1.0 + 20 * localBoostedDensity * gFrameTime * saturate(1 - dot(particle.velocity / curSpeed, accelDir)));
        particle.velocity = lerp(0, particle.velocity, drag);

        const float lvMult = 1000 * dot2(localVelocity);
        if (lvMult > 1) {
          const float drag = 1.0 / (1.0 + gFrameTime * lvMult);
          particle.velocity = lerp(normalize(localVelocity) * curSpeed, particle.velocity, drag);
        }
        particle.velocity += totalAcceleration * gFrameTime;
        // particle.velocity = lerp(localVelocity, particle.velocity, lerp(1, dragEasy, localDensity));
        // particle.velocity = lerp(0, particle.velocity, 1.0 / (1.0 + gFrameTime * pow(noise.z, 8) * 20));
      }

      // particle.velocity = accelU * 80;

      particle.sticky -= max(length(accelU) - 0.001, 0) * 3;
      // particle.sticky -= max(length(particle.velocity) - 0.001, 0) * 5;
      particle.sticky -= max(dot2(localVelocity) - 0.001, 0) * 3;
      // particle.sticky += length(particle.velocity) * lerp(0.2, 2, pow(particle.rand, 2)) * gFrameTime;
      particle.size = max(particle.size - length(particle.velocity) * gFrameTime, 0);
    }

    particle.velocity *= 1 - edge * 0.1;
    float2 movement = gScale * gFrameTime * particle.velocity;
    // float2 movementOrt = float2(movement.y, -movement.x);
    // float offset = noise.x * 2 - 1;
    // offset = pow(abs(offset), 2) * sign(offset);
    // float2 norm = normalize(float2(offset, 1));
    // movement = movement * norm.y + movementOrt * norm.x;

    float movementValue = length(movement);
    if (movementValue > 0){
      particle.pos += particle.moved ? movement : movement * min(0.008, movementValue) / movementValue;
    }
  }

  if (dirX.a == 0 || any(abs(particle.pos - 0.5) > 0.5) || particle.size < 0){
    particle.life = 0;
  }
}
