#define DIRT_MODE
#include "pfxWindscreen.hlsl"

float4 texToScreen(float2 tex){
  float2 pos = frac(tex);
  pos.y = 1 - pos.y;
  return float4(pos * 2 - 1, 0, 1);
}

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD

  if (particle.life <= 0.01 || particle.squished <= 0.1 || frac(particle.rand * 925.52) > 0.04){
		PS_IN vout = (PS_IN)0;
		vout.PosH = -1;
		return vout;
	}

	float2 B = BILLBOARD[vertexID].xy;
	float speed = length(particle.velocity);
	float2 side = speed == 0 ? float2(1, 0) : particle.velocity / speed * float2(1, -1);
	float2 up = float2(side.y, -side.x);

	float size = gScale * lerp(0.004, 0.008, particle.size) * 2 * saturate(particle.life * 10);
	float stretchMult = speed / 65;
	float2 offset = size * (side * B.x + up * B.y) + 2 * side * stretchMult * sign(B.x);

	PS_IN vout;
	vout.PosH = texToScreen(float2(particle.sizeMult, particle.sticky) - particle.velocity * 0.02) + float4(offset * PARTICLE_SCALE, 0, 0);
	// vout.PosH = texToScreen(particle.pos) + float4(offset * PARTICLE_SCALE, 0, 0);
	vout.Tex = BILLBOARD[vertexID].xy;
	vout.Value = saturate(particle.squished) * saturate(particle.life * 20) * lerp(0.5, 1, frac(particle.rand * 126519.663));
	// vout.Value = 1;
	return vout;
}