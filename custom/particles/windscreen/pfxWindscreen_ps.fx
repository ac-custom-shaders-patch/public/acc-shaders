#include "pfxWindscreen.hlsl"

float4 main(PS_IN pin) : SV_TARGET {
  float splashK = pin.Splash;
  float4 noise = txNoise.SampleLevel(samLinearSimple, pin.Tex * lerp(0.1, 0.5, splashK) + pin.NoiseOffset, 0);
  // pin.Tex *= lerp(1, 1.2 + splashK, noise.x);
  pin.Tex *= lerp(0.8, 1.4 + splashK, noise.x);
  clip(1 - dot2(pin.Tex));

  float distance = length(pin.Tex) * pin.SizeMult;
  float split = lerp(0.5, 0.2, splashK);
  // if (distance < 0.5) distance = 0;
  float fade = saturate(remap(distance, 1 - split, 1, 1, 0));
  fade = max(fade, saturate(remap(distance, 1, 2, 1, 0)) * 0.2 * (1 - splashK));
  fade *= lerp(1, saturate(remap(distance, 1 - split, 1 - split * 2, 1, 0)), splashK);
  return float4(pin.Velocity * 10, pin.Intensity, 1) * fade;
}