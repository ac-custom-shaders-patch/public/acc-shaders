#include "pfxWindscreen.hlsl"

Texture2D txNormalsX : register(t5);
Texture2D txNormalsY : register(t6);
Texture2D txSplashes : register(t9);
Texture2D txOcclusion : register(t10);
Texture2D txFinal : register(t11);

void emit(inout Particle particle, EmittingSpot E, RAND_DECL) {
	particle.pos = float2(RAND, RAND) + (float2(RAND, RAND) - 0.5) * 0.01;
	// particle.pos.x = lerp(0.15, 0.87, particle.pos.x);
	// particle.pos.y = lerp(0.45, 0.8, particle.pos.y);
	// particle.pos.x = lerp(0.645, 0.655, particle.pos.x);
	// particle.pos.y = lerp(0.595, 0.605, particle.pos.y);

	float4 nrmX = txNormalsX.SampleLevel(samLinearSimple, particle.pos, 0);
	float4 nrmY = txNormalsY.SampleLevel(samLinearSimple, particle.pos, 0);
	float3 nrm = normalize(cross(nrmX.xyz, nrmY.xyz));
	float occlusion = saturate(txOcclusion.SampleLevel(samLinearSimple, particle.pos, 2).x * 2 - 1);
	float4 splash = txSplashes.SampleLevel(samLinearSimple, particle.pos, 0);

	particle.life = RAND < E.density * saturate(splash.a * 2) ? PARTICLE_LIFESPAN : 0;
	particle.velocity = float2(0, 0);
	particle.size = RAND * lerp(0.9, 1.2, E.rainIntensity);
	particle.rand = lerp(0.0101, 0.9899, RAND) + lerp(-0.01, 0.01, RAND) + lerp(-0.0001, 0.0001, RAND);
	particle.sticky = RAND < E.stickyMult ? lerp(1, 0.2, pow(RAND, 2)) : 0;
	particle.squished = 0;
	particle.moved = 0;
	particle.splashMult = lerp(2, 4, RAND) + pow(E.rainIntensity, 2);
	particle.wiperLag = 0;

	// particle.life = 0;
	// return;

	if (splash.a * 2.2 - 1.2 > RAND * 0.5){
		float3 splashVelocity = (splash.xyz * 2 - 1) * 20;
		float3 splashDirection = normalize(splashVelocity);
		if (dot(nrm, splashDirection) * 1.2 > RAND){
			particle.size += 2 * splash.a;
			particle.life = PARTICLE_LIFESPAN;

			if (RAND > 0.8 && lerp(0.5, 1, RAND) > dot(nrm, splashDirection)){
				particle.velocity = float2(dot(splashVelocity, nrmX.xyz), dot(splashVelocity, nrmY.xyz));
				particle.life += 0.5;
			} else {
				particle.splashMult += lerp(4, 8, pow(RAND, 2)) * saturate(length(splashVelocity) / 20);
			}
		}

		// Possible issue: extra speed density increases effect from splashes as well.
		// TODO: test it, it might be better this way, but if not, rearrange conditions.
	} else {
		particle.splashMult *= saturate(dot(nrm, gRainDir) * 2);
		// particle.life = 0;

		if (E.speedDensity > RAND){
			if (dot(nrm, E.speedDirection) * 1.2 > RAND){
				particle.splashMult += lerp(8, 16, pow(RAND, 2)) * saturate(E.speedDensity * 2) * saturate(dot(nrm, E.speedDirection) * 2);
			} else {
				particle.life = 0;
				return;
			}
		}
	}
	
	float4 trace = txFinal.SampleLevel(samLinearSimple, particle.pos, 0);
	float mask = txNormalsX.SampleLevel(samLinearSimple, particle.pos, 2).a;
	particle.splashMult *= 1 - trace.a;

	if (!(trace.z > 0.1 && trace.z < 0.8 && trace.a < 0.9)){
		if (saturate((1 - occlusion) * 4) > 0.05 + RAND * 0.5){
			particle.life = 0;
		}

		if (dot(nrm, gRainDir) < lerp(0.5, -0.2, pow(RAND, 2))){
			particle.life = 0;
		}

		if (nrmX.a < 0.5 || nrmY.a < 0.5 && mask < 0.8){
			particle.life = 0;
		}
	}
}
