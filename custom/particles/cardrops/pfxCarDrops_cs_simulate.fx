#include "pfxCarDrops.hlsl"

bool carCollisionCheck(float3 pos, bool upperHalfOnly){
  float4 uv = mul(float4(pos, 1), gCarCollisionTransform);
  if (upperHalfOnly && uv.z > 0.5) return false;
  return all(abs(uv.xyz - 0.5) < 0.5) && txCarCollision.SampleLevel(samLinearClamp, uv.xy, 0) < uv.z;
}

void forceFields(float3 pos, inout float3 velocity, float mult) {
  for (uint i = 0; i < FORCE_FIELDS; i++) {
    if (gForceFields[i].radiusSqrInv == 0) break;
    float3 diff = pos - gForceFields[i].pos;
    float dist = dot2(diff);
    float distK = saturate((1 - dist * gForceFields[i].radiusSqrInv) * 2);
    float3 forcePushAway = normalize(diff) * gForceFields[i].forceMult;
    velocity += distK * distK * forcePushAway * gFrameTime * 40 * mult;

    float3 forceVelSync = gForceFields[i].velocity - velocity;
    velocity += saturate(distK * distK * gFrameTime * 10 * mult) * forceVelSync;
  }
}

void process(inout Particle particle, uint3 DTid) {
  bool firstFrame = particle.life == 1;

  particle.life -= PFX_FLAG(FLAG_DIE_FIRST) && DTid.x > gLimitThreshold ? gFrameTime * 20 
    : PFX_FLAG(FLAG_LONGEVITY) ? gFrameTime * 0.5 : gFrameTime * 2;
  if (PFX_FLAG(FLAG_RIPPLE) || particle.life <= 0) return;

  float drag = 1.0 / (1.0 + gFrameTime * 0.5);
  particle.velocity = lerp(float3(gNeutralVelocity.x, 0, gNeutralVelocity.y) * (1 - particle.life), particle.velocity, drag);
  if (!firstFrame) {
    particle.pos += particle.velocity * gFrameTime;
  }

  if (!PFX_FLAG(FLAG_NO_CAR_COLLISIONS_FOR_REAL) && carCollisionCheck(particle.pos, PFX_FLAG(FLAG_NO_CAR_COLLISIONS))){
    particle.life = 0;
  }

  if (particle.collisionPlane.w){
    if (collisionCheck(particle.pos, particle.velocity, particle.collisionPlane, 0.005, 0, 0) < 0.005){
      particle.pos.y += 0.001;
      particle.life = 0.999;
      particle.flags |= FLAG_RIPPLE;
      particle.flags &= ~FLAG_LONGEVITY;
    }
  }

  if (!PFX_FLAG(FLAG_NO_FORCE_FIELDS)) {
    forceFields(particle.pos, particle.velocity, PFX_FLAG(FLAG_INCREASED_FORCE_FIELDS) ? 1 : 0.5);
  }

  for (uint i = 0; i < WHEEL_COLLIDERS; ++i){
    if (dot2(gWheelColliders[i].xyz - particle.pos) < gWheelColliders[i].w){
      particle.life = 0;
    }
  }

  particle.velocity.y += gGravity * gFrameTime;
}
