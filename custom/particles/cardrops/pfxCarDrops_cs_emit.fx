#include "pfxCarDrops.hlsl"

bool carCollisionCheck(float3 pos, bool upperHalfOnly){
  float4 uv = mul(float4(pos, 1), gCarCollisionTransform);
  if (upperHalfOnly && uv.z > 0.5) return false;
  return all(abs(uv.xyz - 0.5) < 0.5) && txCarCollision.SampleLevel(samLinearClamp, uv.xy, 0) < uv.z;
}

struct FailedOffDrop {
  float2 pos;
  float2 velocity;
  float size;
  float3 pad;
};

Texture2D txWindscreenPos : register(t0);
Texture2DArray<float> txRainShadow : register(t1);

ByteAddressBuffer meshVertices0 : register(t4);
ByteAddressBuffer meshIndices0 : register(t5);
ByteAddressBuffer meshVertices1 : register(t6);
ByteAddressBuffer meshIndices1 : register(t7);
ByteAddressBuffer meshVertices2 : register(t8);
ByteAddressBuffer meshIndices2 : register(t9);

StructuredBuffer<FailedOffDrop> buFlewOffDrops : register(t10);
ByteAddressBuffer buFlewOffDropsCounter : register(t11);

bool alignWithMesh(inout float3 pos, EmittingSpot E, ByteAddressBuffer vertices, ByteAddressBuffer indices, RAND_DECL){
	MeshPoint M = randomMeshPoint(vertices, indices, E.meshIndexCount, E.transform, RAND_ARGS);
	pos = M.pos;
	return dot(M.normal, E.direction) > -0.03 && M.localPos.y < 0.8;
}

cbuffer cbCounter : register(b11) {
	uint gCount;
	float3 _pad0;
};

float3 randomDirection(RAND_DECL){
	float3 ret = float3(RAND, RAND, RAND) * 2 - 1;
	return normalize(ret / cos(ret));
}

void emit(inout Particle particle, EmittingSpot E, uint index, RAND_DECL){
	particle.life = 1;
	particle.size = lerp(0.001, 0.002, RAND);
	particle.velocity = lerp(1, lerp(0.25, 1.75, RAND), E.velocityVariance) * E.velocity;
	particle.velocity = lerp(particle.velocity, randomDirection(RAND_ARGS) * length(particle.velocity), 0.1 * E.velocityVariance);
	particle.collisionPlane = 0;
	particle.flags = 0;

	if (E.noCarCollisions){
		// particle.size *= -1;
	}

	bool set;
	bool testCarWetness = false;

	[branch]
	if (E.meshID == 1){
		testCarWetness = true;
		set = alignWithMesh(particle.pos, E, meshVertices0, meshIndices0, RAND_ARGS);
		particle.flags |= FLAG_NO_CAR_COLLISIONS;
		particle.collisionPlane = E.collisionPlane;
	} else if (E.meshID == 2){
		set = alignWithMesh(particle.pos, E, meshVertices1, meshIndices1, RAND_ARGS);
		particle.flags |= FLAG_NO_CAR_COLLISIONS;
		particle.collisionPlane = E.collisionPlane;
	} else if (E.meshID == 3){
		set = alignWithMesh(particle.pos, E, meshVertices2, meshIndices2, RAND_ARGS);
		particle.flags |= FLAG_NO_CAR_COLLISIONS;
		particle.collisionPlane = E.collisionPlane;
	} else if (E.meshID == 4){
		// Drops from car body generated with car height map
		testCarWetness = true;
		float2 uv = float2(RAND, RAND);
		float4 height = txCarCollision.GatherRed(samLinearClamp, uv);

		float heightMax = max(max(height.x, height.y), max(height.z, height.w));
		float heightMin = min(min(height.x, height.y), min(height.z, height.w));
		set = heightMax == 1 && heightMin < 0.99;

		float4 posH = float4(uv, heightMin, 1);
		float4 posW = mul(posH, E.transform);
		particle.pos = posW.xyz;
		particle.flags |= FLAG_NO_CAR_COLLISIONS;
		particle.collisionPlane = E.collisionPlane;
	} else if (E.meshID == 5){
		// Drops from wheel arches
		bool isFront = E.collisionPlane.x;
		float posX = (1 - sqrt(1 - RAND)) * (RAND < 0.5 ? -1 : 1);
		particle.pos = mul(float4(float3(posX * 0.5, RAND - 0.5, -pow(RAND, 4)) * float3(0.25, 0.2, isFront ? 0.5 : 0.1), 1), E.transform).xyz;
		particle.flags |= FLAG_NO_FORCE_FIELDS;
		set = true;
	} else if (E.meshID == 6){
		// Drops from fast spinning wheels
		particle.pos = mul(float4(float3(RAND - 0.5, pow(RAND, 2) * 0.2, 0) * 0.2, 1), E.transform).xyz;
		particle.velocity = E.velocity
			+ (E.direction * float3(1, 0.5 + 0.5 * RAND, 1) + (float3(RAND, RAND, RAND) - 0.5) * 0.1) 
			* E.angularSpeed * lerp(float3(0.5, 1, 0.5), 1, pow(RAND, 4)) * 0.35;
		particle.collisionPlane = E.collisionPlane;
		particle.flags |= FLAG_NO_FORCE_FIELDS;
		particle.flags |= FLAG_DIE_FIRST;
		set = true;
	} else if (E.meshID == 7){
		// Drops going all over the place from driving over puddles
		particle.pos = mul(float4(float3(RAND - 0.5, pow(RAND, 2) * 0.2, RAND - 0.5) * 0.05, 1), E.transform).xyz;

		// float2 dir = normalize(float2(RAND + 0.2, RAND - 0.5));
		float3 dir = normalize(E.direction + float3(0, 0.5, 0));
		dir.xz = rotate2d(dir.xz, M_PI * lerp(-0.4, 0.4, RAND));

		particle.velocity = E.velocity * lerp(0.2, 0.8, pow(RAND, 2))
			+ dir * length(E.velocity) * lerp(0.05, 0.3, pow(RAND, 2));
		particle.pos += dir * float3(1, 0, 1) * 0.2;
		// particle.collisionPlane = E.collisionPlane;
		particle.flags |= FLAG_LONGEVITY;
		particle.flags |= FLAG_RANDOM_SIZE_BOOST;
		particle.flags |= FLAG_NO_FADE_IN;
		particle.flags |= FLAG_DIE_FIRST;
		set = true;
	} else if (E.meshID == 8){
		// Drops flying off spinning wheel
		float angle = (RAND * 0.99 + RAND * 0.01) * M_PI * 2;
		float posX = cos(angle) * E.velocityVariance;
		float posY = sin(angle) * E.velocityVariance;
		particle.pos = mul(float4(float3((RAND * 1.6 - 0.8) * E.pad, posX, posY), 1), E.transform).xyz;

		particle.velocity = E.velocity
			+ mul(float3(0, -posY, posX), (float3x3)E.transform) * E.angularSpeed * 0.25;
		particle.collisionPlane = E.collisionPlane;

		particle.flags |= FLAG_INCREASED_FORCE_FIELDS;
		particle.flags |= FLAG_DIE_FIRST;
		particle.size *= lerp(1, 0.25, saturate(E.angularSpeed / 50));
		set = true;
	} else if (E.meshID == 9){
		// Drops from trees
		float3 offset = float3(RAND - 0.5, 0, RAND - 0.5);
		float len = length(offset);
		if (len > 1) offset /= len;
		else if (len < 0.5) offset *= 2;
		particle.pos = mul(float4(offset * E.pad * 0.5 + float3(0, RAND - 0.5, 0), 1), E.transform).xyz;
		particle.collisionPlane = E.collisionPlane;
		particle.velocity = float3(0, lerp(-5, -10, RAND), 0);
		particle.flags |= FLAG_LONGEVITY;
		particle.flags |= FLAG_RANDOM_SIZE_BOOST;
		// particle.flags |= FLAG_NO_FADE_IN;
		// particle.flags |= FLAG_RIPPLE;
		set = true;
	} else {
		// Drops flying off windscreen
		FailedOffDrop flew = buFlewOffDrops[index];
		set = index < min(buFlewOffDropsCounter.Load(0), 64) && dot(flew.pos, 1) != 0;
		if (set){
			const float velocityEstimationMult = -0.005;
			float2 uvPos0 = flew.pos + flew.velocity * 2 * velocityEstimationMult;
			float2 uvPos1 = flew.pos + flew.velocity * 3 * velocityEstimationMult;
			float4 carPos0 = txWindscreenPos.SampleLevel(samLinearSimple, uvPos0, 0);
			float4 carPos1 = txWindscreenPos.SampleLevel(samLinearSimple, uvPos1, 0);
			if ((carPos1.y - carPos0.y) / velocityEstimationMult < 0.01) set = false;

			particle.pos = mul(E.transform, float4(carPos0.xyz, 1)).xyz;
			float3 velocityOffset = (mul(E.transform, float4(carPos1.xyz, 1)).xyz - particle.pos) / velocityEstimationMult;
			particle.velocity += velocityOffset;
			particle.pos += velocityOffset * abs(velocityEstimationMult) * 2;
			particle.size = 0.2 * lerp(0.002, 0.004, flew.size);
			// particle.life = 0.9;
			particle.collisionPlane = E.collisionPlane;
			particle.flags |= FLAG_NO_CAR_COLLISIONS;
			particle.flags |= FLAG_INCREASED_FORCE_FIELDS;
			particle.flags |= FLAG_WINDSCREEN_DROP;
			particle.flags |= FLAG_NO_FADE_IN;
			particle.flags |= FLAG_NO_CAR_COLLISIONS_FOR_REAL;
		}
	}
	
	if (!set) {
		particle.life = 0;
	}

	if (E.meshID != 7){
		// particle.life = 0;
	}

	if (testCarWetness && gCarCollision == 2){
    float4 posH = mul(float4(particle.pos, 1), gCarWetnessTransform);
		if (RAND > txCarWetness.SampleLevel(samLinearBorder1, posH.xz, 0) * 2){
			particle.life = 0;
		}
	} else if (E.meshID != 0){
    float4 posH = mul(float4(particle.pos, 1), gShadowTransform);
		if (lerp(0.1, 0.9, RAND) > txRainShadow.SampleCmpLevelZero(samShadow, float3(posH.xy, 0), posH.z)){
			particle.life = 0;
		}
	}

	// particle.pos += E.carVelocity * gFrameTime;

	if (E.meshID != 8 && E.meshID > 4){
		particle.life = 0.999999;
	}

	particle.carVelocity = E.carVelocity;
	particle.rand = RAND * 0.99 + RAND * 0.01;
}
