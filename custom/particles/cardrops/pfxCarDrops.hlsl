#define PARTICLES_PACKING
#define PARTICLES_MESH_SPAWN
#define PARTICLES_WITHOUT_NORMALS
#define PARTICLES_SORTING 
#define PASS_INDEX 
#define STENCIL_VALUE 1
#include "particles/common/include_decl.hlsl"

struct Particle {
	float3 pos;
	float life;
	float3 velocity;
	float size;
	float3 carVelocity;
	float rand;
	float4 collisionPlane;
	uint flags;
	uint3 pad;
};

struct Particle_packed {
	float3 pos;
	float life;
	uint2 velocity_size;
	uint2 carVelocity_rand;
	float4 collisionPlane;
	uint flags;
	uint3 pad;
};

#define PARTICLES_OFFSET_ORIGIN(P, OFFSET) \
	P.pos.xyz += OFFSET;\
	if (P.collisionPlane.w) P.collisionPlane.w += dot(P.collisionPlane.xyz, OFFSET);

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.pos, _U.pos);\
	FN(_P.life, _U.life);\
	FN(_P.collisionPlane, _U.collisionPlane);\
	FN(_P.velocity_size, _U.velocity, _U.size);\
	FN(_P.carVelocity_rand, _U.carVelocity, _U.rand);\
	FN(_P.flags, _U.flags);\
	FN(_P.pad, _U.pad);

#define FLAG_LONGEVITY 1
#define FLAG_NO_FORCE_FIELDS 2
#define FLAG_INCREASED_FORCE_FIELDS 4
#define FLAG_NO_CAR_COLLISIONS 8
#define FLAG_NO_CAR_COLLISIONS_FOR_REAL 512
#define FLAG_WINDSCREEN_DROP 16
#define FLAG_RANDOM_SIZE_BOOST 32
#define FLAG_NO_FADE_IN 64
#define FLAG_RIPPLE 128
#define FLAG_DIE_FIRST 256

#define PFX_FLAG(X) ((particle.flags & X) == X)

#if defined(TARGET_CS)
	struct EmittingSpot {
		uint emitCount;
		uint meshID;
		uint meshIndexCount;
		float velocityVariance;

		float3 direction;
		float noCarCollisions;

		float3 velocity;
		float angularSpeed;

		float3 carVelocity;
		float pad;
		
		float4x4 transform;
		float4 collisionPlane;
	};

	#define FORCE_FIELDS 4
	#define WHEEL_COLLIDERS 8

	struct ForceField {
		float3 pos;
		float radiusSqrInv;
		float3 velocity;
		float forceMult;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		float gGravity;
    uint gCarCollision;
		float2 gNeutralVelocity;    

		ForceField gForceFields[FORCE_FIELDS];
		float4 gWheelColliders[WHEEL_COLLIDERS];
		float4x4 gShadowTransform;
    float4x4 gCarCollisionTransform;
    float4x4 gCarWetnessTransform;
	}

	Texture2D<float> txCarCollision : register(t2);
	Texture2D<float> txCarWetness : register(t3);
#elif defined(TARGET_VS) || defined(TARGET_PS)
	struct PS_IN {
		#ifdef MODE_SPLASHES
			float4 PosH : SV_POSITION;
			float2 Tex : TEXCOORD1;
			float3 Velocity : TEXCOORD2;
		#elif defined(MODE_SHADOW)
			float4 PosH : SV_POSITION;
			float2 Tex : TEXCOORD1;
		#else
			PFX_PS_IN

			float3 NormalBase : POSITION1;
			float BlurK : POSITION5;
			float2 NoiseOffset : POSITION6;

			#if !defined(MODE_GBUFFER) && !defined(MODE_SPLASHES)
				float3 LightVal : COLOR5;
				float3 LightDir : POSITION3;
				float LightFocus : POSITION4;
				float Shadow : COLOR6;
			#endif
		#endif
	};

	cbuffer cbPieces : register(CBUFFER_DRAW_SLOT) {
		float gUseColor;
		float gUseCarVelocity;
		float gDeblur;
    float gCarCollision;
    
    float4x4 gCarCollisionTransform;
	}

  #ifdef TARGET_PS
    Texture2D<float> txCarCollision : register(t1);
    float checkCarCollision(float3 pos){
      float4 uv = mul(float4(pos, 1), gCarCollisionTransform);
      return !gCarCollision || any(abs(uv.xyz - 0.5) > float3(0.5, 0.5, 0.48)) ? 1 : saturate((txCarCollision.SampleLevel(samLinear, uv.xy, 0) - uv.z) * 50);
    }
  #endif

  #define INTERIOR_TEST\
    float interiorMult = checkCarCollision(pin.PosC + ksCameraPosition.xyz);\
    if (interiorMult == 0){ clip(-1); return (PS_OUT)0; }
#endif

#include "particles/common/include_impl.hlsl"
#include "rainUtils.hlsl"
