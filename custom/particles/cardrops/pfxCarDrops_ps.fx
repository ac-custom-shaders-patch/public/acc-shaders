#define PARAM_BLUR pin.BlurK
#define PARAM_USE_COLOR gUseColor
// #define GBUFF_MASKING_MODE true
#include "pfxCarDrops.hlsl"

#define ALLOW_EXTRA_FEATURES
#include "refraction.hlsl"

float4 sampleNoise(float2 uv){
	float textureResolution = 32;
	uv = uv * textureResolution + 0.5;
	float2 i = floor(uv);
	float2 f = frac(uv);
	uv = i + f * f * (3 - 2 * f);
	// uv = i + f * f * f * (f * (f * 6 - 15) + 10);
	uv = (uv - 0.5) / textureResolution;
	return txNoise.SampleLevel(samLinearSimple, uv, 0);
}

#if defined(MODE_SPLASHES) || defined(MODE_SHADOW)
  float4 main(PS_IN pin) : SV_TARGET {
#else
  RESULT_TYPE main(PS_IN pin) {
#endif

  #if defined(MODE_RIPPLE)

    float dist = length(pin.Tex);
    clip(1 - dist);
    READ_VECTOR_TOCAMERA
    float3 lighting = getAmbientBaseAt(0, float3(0, 1, 0), 1);
    float alpha = pin.BlurK * saturate(remap(dist, 0.1, 0.7, 0, 1)) * saturate(remap(dist, 1, 0.7, 0, 1)) * 0.1;
    // lighting = float3 (0, 40, 0);
    RETURN_BASE(lighting, saturate(alpha));

  #else

    #if !defined(MODE_SPLASHES) && !defined(MODE_SHADOW)
      bool noInteriorTest = pin.BlurK < 0;
      pin.BlurK = abs(pin.BlurK);

      if (!noInteriorTest) {
        INTERIOR_TEST;
      }

      float4 noise = sampleNoise(pin.Tex * float2(0.1, 0.1 * pin.BlurK) + pin.NoiseOffset);
      float4 noise1 = sampleNoise(pin.Tex * float2(0.03, 0.03 * pin.BlurK) + pin.NoiseOffset);
      pin.Tex.x *= 0.8 + noise.x + noise.y * noise.z * 0.4;
    #endif
    clip(1 - dot2(pin.Tex));

    #if !defined(MODE_SPLASHES) && !defined(MODE_SHADOW)
      READ_VECTOR_TOCAMERA
      float softK = calculateSoft(pin.PosH, pin.PosC, PARAM_BLUR);

      float facingShare = sqrt(saturate(1 - dot2(pin.Tex)));
      float distanceToCamera = dot2(pin.PosC) / 2;
      float3 normal = normalize(pin.NormalBase + -toCamera * facingShare);
      float fresnel = pow(saturate(1 + dot(toCamera, normal)), 2 - pin.BlurK);
      float alpha = lerp(1, saturate(remap(abs(pin.Tex.y), 0.2, 0.8, 1, 0)) * 0.75, pin.BlurK);

      #if !defined(MODE_GBUFFER)
        alpha *= softK;

        float3 refraction = lerp(-normal, toCamera, 0.5);
        float3 reflection = reflect(toCamera, normal);
        float3 reflectionColor = sampleEnv(reflection, 3, 0);
        float3 refractionColor = sampleEnv(refraction, 3, 0);
        float3 refractionBlurred = sampleEnv(-normal, 5, 0);
        float blur = abs(pin.Tex.y) * pin.BlurK;

        float4 prefFrameSampled = gUseColor ? calculateRefraction(pin.PosH, pin.PosC, toCamera, normal, 0.07, pin.BlurK) : 0;
        if (prefFrameSampled.a){
          refractionColor = prefFrameSampled.rgb;
        } else {
          alpha *= 1 - pin.BlurK * 0.7;
          refractionColor = lerp(refractionColor, refractionBlurred, pin.BlurK);
        }

        float3 lighting = lerp(refractionColor, reflectionColor, fresnel * saturate(alpha * 2));
        lighting += simpleReflectanceModel(-toCamera, -ksLightDirection.xyz, normal) * ksLightColor.rgb * pin.Shadow;

        float lightMult = max(fresnel * pow(saturate(dot(pin.LightDir, toCamera)), 20) * 0.4,
          lerp(0.02, simpleReflectanceModel(-toCamera, pin.LightDir, normal), pin.LightFocus * (1 - pin.BlurK * 0.8)));
        lighting += pin.LightVal * lightMult;

        // lighting = float3(4, 0, 0);
        // alpha = 1;
      #else
        float3 lighting = 0;
      #endif
    #endif

    // alpha *= pin.AlphaMult;

    #ifdef MODE_SPLASHES
      return float4(pin.Velocity, 1);
    #elif defined(MODE_SHADOW)
      // uint2 pos = uint2(pin.PosH.xy);
      // uint2 xy2 = pos.xy % 2;
      // if (dot(xy2, 1) == 1) discard;
      return 0;
    #else

      // if (gCarCollision){
      //   lighting = float3(0, 2, 0);
      //   alpha = 1;
      // } else {
      //   lighting = float3(2, 0, 0);
      //   alpha = 1;
      // }

      RETURN_BASE(lighting, alpha);
    #endif

  #endif
}