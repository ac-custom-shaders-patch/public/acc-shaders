#include "pfxCarDrops.hlsl"
#include "include_new/ext_shadows/_include_vs.fx"
#include "include_new/ext_lightingfx/_include_ps.fx"

#define RAIN_BILLBOARD_STRETCH 0.05
#include "../common/rainUtils_impl.hlsl"

#ifdef MODE_SPLASHES
	cbuffer cbSplashesData : register(b3) {
		float4x4 gCarTransformInv;
		float3 gCarVelocity;
  	float gSizeScale;
	}
#endif

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	LOAD_PARTICLE_BILLBOARD

	PS_IN vout;	

	bool isWindscreenDrop = PFX_FLAG(FLAG_WINDSCREEN_DROP);
	float size = particle.size * 4;
	if (PFX_FLAG(FLAG_RANDOM_SIZE_BOOST)){
		size += pow(frac(particle.rand * 47161.2161), 20) * 0.016;
	}

	size *= saturate(particle.life * 10);
	if (!PFX_FLAG(FLAG_NO_FADE_IN)){
		size *= saturate((1 - particle.life) * 20);
	}

	#if defined(MODE_SPLASHES)
		// if (particle.life > 0.5 || PFX_FLAG(FLAG_RIPPLE) || !PFX_FLAG(FLAG_LONGEVITY)) {
		// if (particle.life > 0.99 || PFX_FLAG(FLAG_RIPPLE)) {
		// 	DISCARD_VERTEX(PS_IN);
		// }
		size *= 2;
	#elif defined(MODE_RIPPLE)
		if (!PFX_FLAG(FLAG_RIPPLE)){
			DISCARD_VERTEX(PS_IN);
		}
	#else
		if (PFX_FLAG(FLAG_RIPPLE)){
			DISCARD_VERTEX(PS_IN);
		}
	#endif

	float stretchMult = PFX_FLAG(FLAG_RANDOM_SIZE_BOOST) ? lerp(0.25, 1.75, frac(particle.rand * 51616.2315)) : 0.2;

	float3 velC = particle.velocity;
	// if (gUseCarVelocity) velC -= particle.carVelocity;
	// else velC.x -= 0.001;
	velC -= particle.carVelocity;

	#if defined(MODE_SPLASHES) || defined(MODE_SHADOW)
		float3 offset = rainBillboard(vertexID, particle.pos, velC, size, vout.Tex);

		#if defined(MODE_SPLASHES)
			offset *= gSizeScale;
			vout.Velocity = (mul(particle.velocity, (float3x3)gCarTransformInv) + gCarVelocity) * 0.05;
		#else
			offset *= 0.3;
		#endif

		if (PFX_FLAG(FLAG_RIPPLE)){
			DISCARD_VERTEX(PS_IN);
		}
	#else
    float3 posC = particle.pos - ksCameraPosition.xyz;
    float3 billboardAxis = normalize(posC);
    #ifdef MODE_SHADOW
      billboardAxis = ksLightDirection.xyz;
    #endif

    float3 velocity = dot(velC, 1) ? velC : float3(0, 0.00001, 0);
		if (gDeblur) velocity *= 0.01;
    float speed = length(velocity);
    vout.BlurK = saturate(remap(speed, 2, 3, 0, 1));

		#if defined(MODE_RIPPLE)
			billboardAxis = float3(0, 1, 0);
			size = lerp(0.04, 0, particle.life);
			stretchMult = 0;
			vout.BlurK = particle.life;
		#else
			// size *= 1.4;
			if (PFX_FLAG(FLAG_NO_CAR_COLLISIONS_FOR_REAL)){
				vout.BlurK = -vout.BlurK - 0.001;
			}
		#endif

    float3 velocityDir = velocity / speed;
    float3 side = normalize(cross(billboardAxis, velocityDir));
    float3 up = normalize(cross(billboardAxis, side));

    vout.Tex = quadPos.xy;
    vout.NormalBase = up * quadPos.y + side * quadPos.x;

    float3 offset = (side * quadPos.x + up * quadPos.y) * size / (1 + 0.1 * speed);
    float3 stretchOffset = velocityDir * speed / (1 + 0.04 * speed) * dot(velocityDir, normalize(offset));
    offset += RAIN_BILLBOARD_STRETCH * stretchMult * stretchOffset;

    #ifdef RAIN_WIDE_SPLASH
      float3 splashDir = normalize(cross(velocity, float3(0, 1, 0)));
      float splashOffset = quadPos.x * abs(billboardAxis.y);
      offset += splashDir * clamp(abs(splashOffset), 0, lerp(0.02, 0.05, frac(stretchMult * 1567.263))) * sign(splashOffset);
    #endif

		#ifndef MODE_SHADOW
			vout.Fog = 0;
		#endif
	#endif

	#if defined(MODE_SHADOW)
		vout.PosH = mul(float4(particle.pos + offset, 1), ksMVPInverse);
	#else
		float4 posW = float4(particle.pos + offset, 1);
		float4 posV = mul(posW, ksView);
		vout.PosH = mul(posV, ksProjection);
	#endif

	#if !defined(MODE_SPLASHES) && !defined(MODE_SHADOW)
		float3 toCamera = normalize(posW.xyz - ksCameraPosition.xyz);
		COMMON_SHADING(vout);
		vout.NoiseOffset = float2(frac(particle.rand * 19216.31651), frac(particle.rand * 91126.13561) + particle.life);
	#endif
	return vout;
}