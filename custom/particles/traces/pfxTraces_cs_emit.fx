#include "pfxTraces.hlsl"

void emit(inout Particle particle, EmittingSpot E, RAND_DECL){
	particle.posA = E.emitterPos;
	particle.posB = E.emitterPosPrev;
	particle.posM = (particle.posA + particle.posB) / 2;

	particle.lifeA = 1;
	particle.lifeB = 1;
	particle.glowA = E.glowA;
	particle.glowB = E.glowB;
	particle.thickness = E.thickness;
}
