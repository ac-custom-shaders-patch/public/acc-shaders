#include "pfxTraces.hlsl"
#include "include_new/ext_functions/depth_map.fx"

float main(PS_IN pin) : SV_TARGET {
  #ifdef NO_DEPTH_MAP
    float softK = 1.0;
  #else
    float depthZ = getDepth(pin.PosH);
    float depthC = linearize(pin.PosH.z);
    float softK = saturate((depthZ - depthC) * (1.05 - pin.PosH.z * pin.PosH.z) * 5e5);
    softK *= softK;
  #endif

  float2 tex = pin.Tex;
  tex.x = saturate(abs(tex.x) - pin.RoundK) * sign(tex.x) / max(1 - pin.RoundK, 0.001);
  clip(1 - dot(tex, tex));

  float color = max(pin.Brightness, 0.1);
  return color * softK * saturate(pin.Brightness * 10) * (1 - pin.Fog);
}