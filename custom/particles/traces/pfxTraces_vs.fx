#include "pfxTraces.hlsl"

static const float3 BILLBOARD[] = {
	float3(-1, -1, 0),
	float3(1, -1, 0),
	float3(-1, 1, 0),
	float3(-1, 1, 0),
	float3(1, -1, 0),
	float3(1, 1, 0),
	
	float3(-1, -1, 0),
	float3(1, -1, 0),
	float3(-1, 1, 0),
	float3(-1, 1, 0),
	float3(1, -1, 0),
	float3(1, 1, 0),
};

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	uint subID = fakeIndex % 12;
	uint instanceID = fakeIndex / 12;
	Particle particle = PFX_unpack(particleBuffer[aliveList[instanceID]]);

	uint vertexID = fakeIndex % 6;
	uint planeID = subID < 6;

	float3 quadPos = BILLBOARD[vertexID];

	float4 posA = float4(planeID ? particle.posA : particle.posM, 1);
	float4 posB = float4(planeID ? particle.posM : particle.posB, 1);
	float4 pad = (posA - posB) * xPaddingK;
	posA += pad;
	posB -= pad;
	// posA += saturate(particle.life * 20 - 18) * normalize(pad);

	float4 posW = posA;
	posA = mul(posA, ksView);
	float4 posV = posA;	
	posA = mul(posA, ksProjection);
	posB = mul(posB, ksView);
	posV = (posV + posB) / 2;
	posB = mul(posB, ksProjection);

	float2 posDifB = posA.xy / posA.w - posB.xy / posB.w;
	float2 posDif = normalize(posA.xy / posA.w - posB.xy / posB.w);

	// float glowA = planeID ? particle.glowA : (particle.glowA + particle.glowB) / 2;
	// float glowB = planeID ? (particle.glowA + particle.glowB) / 2 : particle.glowB;


	// float sizeBase = lerp(particle.lifeA, particle.lifeB, quadPos.x < 0 ? (planeID ? 0 : 0.5) : (planeID ? 0.5 : 1));
	// float sizeBase = lerp(particle.lifeA, particle.lifeB, ((quadPos.x > 0) + !planeID) * 0.5);
	// float sizeBase = lerp(particle.lifeB, particle.lifeA, 1 - (quadPos.x > 0 ? 0.5 : 0) - (!planeID ? 0.5 : 0));
	// float sizeBase = lerp(particle.lifeB, particle.lifeA, (0.5 - (quadPos.x > 0 ? 0.5 : 0)) + (0.5 - (!planeID ? 0.5 : 0)) );
	// float sizeBase = lerp(particle.lifeB, particle.lifeA, (quadPos.x < 0 ? 0.5 : 0) + (planeID ? 0.5 : 0) );
	float sizeBase = lerp(particle.lifeB, particle.lifeA, ((quadPos.x < 0) + planeID) * 0.5);
	// float sizeBase = particle.lifeA;

	float sizeA = sqrt(lerp(0.2, 1, saturate(sizeBase * xNarrowingK))) * particle.thickness;
	// float sizeA = sqrt(lerp(0.2, 1, saturate(sizeBase * xNarrowingK))) * particle.thickness;
	if (quadPos.x < 0) {
		posA = posB;
	}

	if (quadPos.y < 0) posA.xy += posDif.yx * float2(sizeA, -sizeA) * xFovK;
	else posA.xy -= posDif.yx * float2(sizeA, -sizeA) * xFovK;

	float glazingFix = saturate(sizeA - length(posDifB));
	if (quadPos.x < 0) posA.xy -= glazingFix * posDif;
	else posA.xy += glazingFix * posDif;

	PS_IN vout;
	vout.PosH = posA;
	vout.PosC = posW.xyz - ksCameraPosition.xyz;
	vout.Tex = BILLBOARD[vertexID].xy;
	vout.Brightness = lerp(particle.glowB, particle.glowA, ((quadPos.x < 0) + planeID) * 0.5) * saturate(sizeBase * xNarrowingK);
	// vout.Brightness = 1;
	vout.RoundK = 0.7 * saturate(1 - glazingFix * 10);
	vout.Fog = calculateFog(posV);

	#ifdef MODE_GBUFFER
		GENERIC_PIECE_STATIC(posW);
	#endif

	return vout;
}