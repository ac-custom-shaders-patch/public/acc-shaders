#include "pfxTraces.hlsl"

void forceFields(inout float3 pos){
  for (uint i = 0; i < FORCE_FIELDS; i++){
    float3 dist = pos - xForceFields[i].xyz;
    pos += xForceFields[i].w * pow(saturate(1 - dot(dist, dist) / 8), 4) * normalize(dist);
  }
}

void process(inout Particle particle, uint3 DTid){
  particle.lifeA -= gFrameTime * xLifespanMultInv;
  particle.lifeB = particle.lifeA - gFrameTime * xLifespanMultInv;
  
  if (gFrameTime){
    forceFields(particle.posA);
    forceFields(particle.posB);
    forceFields(particle.posM);
  }
}
