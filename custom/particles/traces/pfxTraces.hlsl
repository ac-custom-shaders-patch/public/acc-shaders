#define PARTICLES_PACKING
#define PARTICLES_WITHOUT_NORMALS 
#define PARTICLES_FIELD_LIFE lifeA 
#define VERTICES_PER_PARTICLE 12
#include "particles/common/include_decl.hlsl"

struct Particle {
	float3 posA;
	float lifeA;

	float3 posB;
	float lifeB;

	float3 posM;
	float glowA;

	float glowB;
	float thickness;
};

struct Particle_packed {
	float3 posA;
	float lifeA;

	float3 posB;
	uint lifeB_thickness;

	float3 posM;
	uint glowA_glowB;
};

#define PARTICLES_PACKING_RULES(FN)\
	FN(_P.posA, _U.posA);\
	FN(_P.posB, _U.posB);\
	FN(_P.posM, _U.posM);\
	FN(_P.lifeA, _U.lifeA);\
	FN(_P.lifeB_thickness, _U.lifeB, _U.thickness);\
	FN(_P.glowA_glowB, _U.glowA, _U.glowB);

#define FORCE_FIELDS 16

#if defined(TARGET_CS)

	#define PARTICLES_OFFSET_ORIGIN(P, O)\
		P.posA += O;\
		P.posB += O;\
		P.posM += O;

	struct EmittingSpot {
		uint emitCount;
		float3 emitterPos;

		float glowA;
		float3 emitterPosPrev;

		float glowB;
		float thickness;
		float2 pad;
	};

	cbuffer cbSimulate : register(CBUFFER_SIMULATE_SLOT) {
		CBUFFER_SIMULATE_GEN

		float xLifespanMultInv;
		float3 xPad0;

		float4 xForceFields[FORCE_FIELDS];
	}
#elif defined(TARGET_VS) || defined(TARGET_PS)
	cbuffer cbTraces : register(CBUFFER_DRAW_SLOT) {
		float3 xColor;
		float xNarrowingK;
		float xLifespanMultInv;
		float xFovK;
		float xPaddingK;
		float _drawPad0;
	}

	struct PS_IN {
		PFX_PS_IN
		float Brightness : TEXCOORD2;
		float RoundK : TEXCOORD3;
	};
#endif

#include "particles/common/include_impl.hlsl"

