struct Particle {
  float3 pos;
  float pad0;
  float3 dir;
  float pad1;
};

#if defined(TARGET_VS) || defined(TARGET_PS)

  struct PS_IN { 
    float4 PosH : SV_POSITION;
    float3 Color : TEXCOORD2;
  };

  #if !defined(MODE_SHADOW)
    #define MODE_MAIN_FX
  #endif

  #define NO_PERVERTEX_AO

#endif