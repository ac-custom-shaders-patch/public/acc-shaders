#include "include/common.hlsl"
#include "include/samplers.hlsl"
#include "pfxCarFlagCloth.hlsl"

cbuffer cbCSBuffer : register(b10) {
  float4x4 gAreaHeightmapTransform;
  float4x4 gCarTransform;
  float4x4 gCarTransformInv;
  float4x4 gCarTransformPrevInv;

  float3 gAABBMin;
  float gScaleXY;

  float3 gAABBMax;
  uint gStepsCount;

  float2 gWindVelocity;
  float gScaleX;
  float gScaleY;

  float3 gCarVelocity;
  float _gPad0;

  float3 gOrigin;
  float _gPad1;

  float3 gDir;
  float _gPad2;

  float3 gOriginPrev;
  float _gPad3;

  float3 gDirPrev;
  float _gPad4;
}

#define DT 0.001
RWStructuredBuffer<Particle> particleBuffer : register(u0);
RWByteAddressBuffer outBuffer : register(u1);
StructuredBuffer<Particle> prevParticleBuffer : register(t0);
Texture2D<float> txAreaDepth : register(t1);
Texture2D txCarSurface : register(t2);

float3 tie(inout Particle P, Particle N, float restLength, float stiffness){
  float3 displacement = N.pos - P.pos;
  float distance = max(0.00001, length(displacement));

  float3 direction = displacement / distance;
  float springForceMagnitude = 1e4 * (distance - restLength);

  float3 relativeVelocity = P.dir - N.dir;
  float dampingForceMagnitude = 200 * dot(relativeVelocity, direction);

  return direction * (stiffness * (springForceMagnitude - dampingForceMagnitude));
}

float3 getPosRel(float3 posL){
  float3 ret = (posL.xyz - gAABBMin) / (gAABBMax - gAABBMin);
  ret.xz = ret.xz * 2 - 1;
  return ret;
}

groupshared Particle gCache[32 * 32];

Particle getPPrev(uint2 p, int2 offset = 0){
  return prevParticleBuffer[(p.y + offset.y) * 32 + p.x + offset.x];
}

Particle getP(uint2 p, int2 offset = 0){
  // return prevParticleBuffer[(p.y + offset.y) * 32 + p.x + offset.x];
  return gCache[(p.y + offset.y) * 32 + p.x + offset.x];
}

Particle getPNm(uint2 p, int2 offset = 0){
  return gCache[clamp((int)p.y + offset.y, 0, 31) * 32 + clamp((int)p.x + offset.x, 0, 31)];
}

Particle computePStep(Particle P, uint2 threadID, float progress) {
  float drag = 1 / (1 + DT * 0.3);
  float drag1 = 1 / (1 + DT * 20);

  float windMult = 1;
  float3 force = 0;
  force.y -= 1;

  if (threadID.x > 0) {
    // force.x += max(0, sign(P.pos.z) * normalize(P.pos - getP(threadID.xy, int2(-1, 0)).pos).z) * 1000;

    force += tie(P, getP(threadID.xy, int2(-1, 0)), gScaleY, 1);

    if (threadID.x < 31) {
      force += tie(P, getP(threadID.xy, int2(1, 0)), gScaleY, 1);
    }
    if (threadID.y > 0) {
      force += tie(P, getP(threadID.xy, int2(0, -1)), gScaleX, 1);
    }
    if (threadID.y < 31) {
      force += tie(P, getP(threadID.xy, int2(0, 1)), gScaleX, 1);
      if (threadID.y > 0 && threadID.x > 0 && threadID.x < 31) {
        force += tie(P, getP(threadID.xy, int2(-1, 1)), gScaleXY, 0.5);
        force += tie(P, getP(threadID.xy, int2(-1, -1)), gScaleXY, 0.5);
        force += tie(P, getP(threadID.xy, int2(1, -1)), gScaleXY, 0.5);
        force += tie(P, getP(threadID.xy, int2(1, 1)), gScaleXY, 0.5);
      }   
    }
    
    float4 areaUV = mul(float4(P.pos, 1), gAreaHeightmapTransform);
    if (all(abs(areaUV.xyz - 0.5) < 0.5)) {
      float depth = txAreaDepth.SampleLevel(samLinearBorder1, areaUV.xy, 0);
      float contactDepth = (areaUV.z - depth) * 300 + 0.05;
      windMult = saturate(-contactDepth * 40);

      if (contactDepth > 0) {
        force.y *= lerpInvSat(contactDepth, 0.05, 0);
        force.y += contactDepth * 20;
        P.dir *= drag1;
      }
    }

    float3 posL = lerp(mul(float4(P.pos, 1), gCarTransformPrevInv).xyz, mul(float4(P.pos, 1), gCarTransformInv).xyz, progress);
    float3 posRel = getPosRel(posL);
    if (length(posRel) > 5) {
      P.pad0 = 0;
      P.pos = lerp(P.pos, gOrigin, 0.95);
      P.dir = lerp(P.pos, gCarVelocity, 0.95);
    }
    float3 dir = normalize(posRel);
    float2 surfUV = normalize(dir.xz) * float2(-1, -1) * (1 - dir.y) * 0.5 + 0.5;
    float4 surfData = txCarSurface.GatherAlpha(samPointBorder0, surfUV, 0);  
    float surfDepth = max(surfData.x, max(surfData.y, max(surfData.z, surfData.w)));
    float surfError = surfData.w * 2 + 0.01 - length(posRel);
    if (surfError > 0) {
      surfData.xyz = txCarSurface.SampleLevel(samLinearBorder0, surfUV, 0).xyz;
      float3 nm = mul(normalize(surfData.xyz * 2 - 1), (float3x3)gCarTransform);
      force -= dot(force, nm) * nm * lerpInvSat(surfError, 0, 0.005);
      force += nm * surfError * 200;
      P.dir = lerp(gCarVelocity, P.dir, drag1);
    }

    P.dir = lerp(float3(gWindVelocity.x, 0, gWindVelocity.y) * windMult, P.dir, drag);
    P.dir += force * DT;
    P.pos += P.dir * DT;
  } else {
    float3 newPos = lerp(gOriginPrev, gOrigin, progress) + lerp(gDirPrev, gDir, progress) * (threadID.y / 31.);
    if (length(newPos - P.pos) > 0.0001) {
      P.dir = (newPos - P.pos) / DT;
      P.pos = newPos;
    } 
  }
  return P;
}

[numthreads(32, 32, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  Particle P = getPPrev(threadID.xy);

  if (P.pad0 == 0) {
    P.pad0 = 1;
    P.pos = gOrigin + gDir * (threadID.y / 31. + threadID.x / 100.);
    P.dir = frac(threadID.x * 0.0371 + threadID.y * 0.02165) * 0.00001;
  }

  gCache[threadID.y * 32 + threadID.x] = P;
  float3 nm;

  [loop]
  for (uint i = 0U; i < gStepsCount; ++i) {
    GroupMemoryBarrierWithGroupSync();

    if (i == gStepsCount - 1) {
      Particle P0 = getPNm(threadID.xy, int2(-1, 0));
      Particle P2 = getPNm(threadID.xy, int2(1, 0));
      Particle P1 = getPNm(threadID.xy, int2(0, -1));
      Particle P3 = getPNm(threadID.xy, int2(0, 1));
      nm = normalize(cross(P3.pos - P1.pos, P2.pos - P0.pos));  
    }

    P = computePStep(P, threadID.xy, i / (float)(gStepsCount - 1));
    gCache[threadID.y * 32 + threadID.x] = P;
  }

  particleBuffer[threadID.y * 32 + threadID.x] = P;
	outBuffer.Store3((threadID.y * 32 + threadID.x) * 44, asuint(P.pos));
	outBuffer.Store3((threadID.y * 32 + threadID.x) * 44 + 12, asuint(nm));
}