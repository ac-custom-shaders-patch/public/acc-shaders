#define ALLOW_CAR_FLEX
#include "include_new/base/_include_vs.fx"
#include "accFakeCarShadows.hlsl"

void applyPointFlexAlt(inout float3 posW){
  [branch]
  if (flexPivot.x) { 
    // 164 with per-vertex flexOffset, 165 done this way
    float flexAmount = dot(flexAxis, posW - flexPivot);
    float3 origPosW = posW;
    posW = lerp(posW, mul(float4(posW, 1), flexData[flexAmount > 0]).xyz, saturate(abs(flexAmount)) * 0.5);
    

    float3 delta = posW - origPosW;
    posW = origPosW + delta - shadowPlane.xyz * dot(shadowPlane.xyz, delta);
  }
}

float4 toScreenSpaceAlt(float4 posL, out float4 posW, out float4 posV SPS_VS_TOSS_ARG){
  posW = mul(posL, ksWorld);
  applyPointFlexAlt(posW.xyz);
  posV = mul(posW, ksView);
  return mul(posV * float4(posVMult.xxx, 1), ksProjection);
}

PS_IN_FakeCarShadows main(VS_IN vin SPS_VS_ARG) {
  PS_IN_FakeCarShadows vout;
  float4 posW, posV;
  vout.PosH = toScreenSpaceAlt(vin.PosL, posW, posV SPS_VS_TOSS_PASS);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  shadows(posW, SHADOWS_COORDS);
  vout.PosH = toScreenSpaceAlt(vin.PosL, posW, posV SPS_VS_TOSS_PASS);
  SPS_RET(vout);
}
