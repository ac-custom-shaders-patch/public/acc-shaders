cbuffer ShaderConstants : register(b12) {
	float4x4 TransformMatrix : packoffset(c0);
};

struct VSIn {
	float4 Position : POSITION;
	float4 GlyphColor : GLYPHCOLOR;
};

struct VSOut {
	float4 Position : SV_Position;
	float4 GlyphColor : COLOR;
	float2 TexCoord : TEXCOORD;
	// float2 NormTexCoord : TEXCOORD1;
	// float4 ClipDistance : CLIPDISTANCE;
};

VSOut main__(VSIn Input) {
	VSOut Output;	
	Output.Position = mul(TransformMatrix, float4(Input.Position.xy, 0.0f, 1.0f));
	Output.GlyphColor = Input.GlyphColor;// * float4(1, 0, 0, 1);
	Output.TexCoord = Input.Position.zw;	
	// Output.ClipDistance = 1;	
	return Output;
}

struct CharData {
	float2 pos;
	float2 size;
	uint tex_pos0;
	uint tex_pos1;
	uint color;
	float color_mult;
};

cbuffer ShaderConstants : register(b11) {
	CharData gSymbols[128];
};

static const float2 BILLBOARD[] = {
	float2(0, 0),
	float2(1, 0),
	float2(0, 1),
	float2(0, 1),
	float2(1, 0),
	float2(1, 1),
};

#include "include/common.hlsl"

VSOut main(uint id: SV_VertexID) {
	uint instanceID = id / 6;
	uint vertexID = id % 6;

	CharData char = gSymbols[instanceID];
	float2 charPos0 = (float2)int2(char.tex_pos0 & 0xffff, (char.tex_pos0 >> 16) & 0xffff) / 65535.;
	float2 charPos1 = (float2)int2(char.tex_pos1 & 0xffff, (char.tex_pos1 >> 16) & 0xffff) / 65535.;

	VSIn Input;
	Input.Position.xy = char.pos + char.size * BILLBOARD[vertexID] /** 1.2*/;
	Input.Position.zw = lerp(charPos0, charPos1, BILLBOARD[vertexID] /** 1.2*/);
	Input.GlyphColor = unpackColor(char.color);
	Input.GlyphColor.rgb *= char.color_mult;

	VSOut Output;	
	Output.Position = mul(TransformMatrix, float4(Input.Position.xy, 0.0f, 1.0f));
	Output.GlyphColor = Input.GlyphColor;// * float4(1, 0, 0, 1);
	Output.TexCoord = Input.Position.zw;	
	// Output.NormTexCoord = BILLBOARD[vertexID] * 1.2;	
	// Output.ClipDistance = 1;	
	return Output;
}