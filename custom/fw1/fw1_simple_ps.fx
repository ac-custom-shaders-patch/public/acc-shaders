#include "include/samplers.hlsl"
Texture2D<float> tex0 : register(t10);

struct PSIn {
	float4 PosH : SV_Position;
	float4 GlyphColor : COLOR;
	float2 TexCoord : TEXCOORD;
	// float2 NormTexCoord : TEXCOORD1;
	// float4 ClipDistance : CLIPDISTANCE;
};

float4 main(PSIn pin) : SV_Target {
	float a = tex0.Sample(samLinear, pin.TexCoord);	
	if(a == 0.0f)
		discard;	
	float4 ret = float4(pin.GlyphColor.rgb, a * pin.GlyphColor.a);
	#ifdef USE_BLOCKED_PATTERN
		ret *= float4(0.9, 0.5, 0.5, 1);
		ret.x += 0.1 * ((pin.PosH.x + pin.PosH.y) % 16 > 8);
	#endif
	return ret;
}

// #include "include/poisson.hlsl"

// float4 main_(PSIn pin) : SV_Target {
// 	float a = tex0.Sample(samLinear, pin.TexCoord);
// 	if (any(abs(pin.NormTexCoord) > 1))	a = 0.001;

//   float R = 0.0005;
//   float4 ret = 0;
//   float tot = 0.0001;
//   #define DISK_SIZE 16
//   for (uint i = 0; i < DISK_SIZE; ++i) {
//     float2 offset = SAMPLE_POISSON(DISK_SIZE, i);

//     float2 sampleUV = pin.TexCoord + offset * R;
//     float4 v = tex0.SampleLevel(samLinear, sampleUV, 0);
//     float w = 1;
//     ret += v * w;
//     tot += w;
//   }
// 	// ret = tot;
// 	float4 output = float4(pin.GlyphColor.rgb, a * pin.GlyphColor.a);
// 	output.rgb *= output.a;
// 	output.a = max(output.a, (ret / tot).w);
// 	output.rgb /= output.a;
// 	return output;

// 	if(a == 0.0f)
// 		discard;	
// }