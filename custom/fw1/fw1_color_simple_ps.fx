#include "include/samplers.hlsl"
Texture2D tex0 : register(t10);

struct PSIn {
	float4 Position : SV_Position;
	float4 GlyphColor : COLOR;
	float2 TexCoord : TEXCOORD;
};

float4 main(PSIn Input) : SV_Target {	
	float4 a = tex0.Sample(samLinear, Input.TexCoord);	
	if(a.w == 0.0f)
		discard;	
	return float4(a.rgb, a.w * Input.GlyphColor.a);
}