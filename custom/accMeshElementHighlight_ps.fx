struct PS_IN {
  float4 PosH : SV_POSITION;
  float4 Color : TEXCOORD0;
};

float4 main(PS_IN pin) : SV_TARGET {
  return pin.Color;
}
