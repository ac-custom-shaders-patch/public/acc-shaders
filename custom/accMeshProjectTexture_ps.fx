#include "include/common.hlsl"
// #include "include_new/base/textures_ps.fx"

struct PS_IN {
  float4 PosH : SV_POSITION;
  float3 PosG : TEXCOORD0;
  float2 Tex : TEXCOORD1;
};

Texture2D txMain : register(TX_SLOT_MAT_0);

cbuffer cbDataObject : register(b10) {
  float4x4 gPosGToTex;
  float4 gColAdd;
  float4 gColMult;
  float4 gColBg;
  float4 gDesaturate;
}

float4 main(PS_IN pin) : SV_TARGET {
  float4 posH = mul(float4(pin.PosG, 1), gPosGToTex);
  // float4 gColAdd = float4(2, 2, 2, 1);
  // float4 gColMult = float4(-1.8, -1.8, -1.8, 0);
  // float4 gColBg = float4(0.2, 0.2, 0.2, 1);
  // float4 gDesaturate = float4(1, 1, 1, 0);
  float4 inColor = txMain.SampleLevel(samLinearClamp, posH.xy, 0);
  // clip(-1);
  if (dot(gDesaturate, 1)) inColor.rgb = saturate(dot(gDesaturate, inColor));
  return all(posH.xy > 0 && posH.xy < 1) ? gColAdd + gColMult * inColor : gColBg;
}
