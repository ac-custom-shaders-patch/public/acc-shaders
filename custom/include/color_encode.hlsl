float4 decodeColor(float v) {
  float4 encodeMul = float4(1, 255, 65025, 160581375);
  float encodeBit = 1. / 255;
  float4 enc = encodeMul * v;
  enc = frac(enc);
  enc -= enc.yzww * encodeBit;
  return enc;
}

float encodeColor(float4 enc) {
  return dot(clamp(enc, 0, 0.99), float4(1, 1. / 255, 1. / 65025, 1. / 160581375));
}