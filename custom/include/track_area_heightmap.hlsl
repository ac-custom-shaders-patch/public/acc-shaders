#ifndef TRACK_AREA_TRANSFORM
  #define TRACK_AREA_TRANSFORM gAreaHeightmapTransform
#endif

#ifndef TRACK_AREA_TEXTURE
  #define TRACK_AREA_TEXTURE txAreaColor
#endif

float4 TA_getSurfaceColor(float3 posG, bool filtered = true) {
	float4 areaUV = mul(float4(posG, 1), TRACK_AREA_TRANSFORM);
	float4 color = TRACK_AREA_TEXTURE.SampleLevel(samLinearBorder0, areaUV.xy, 1);
  if (filtered) {
    color.g = min(color.r, color.g);
    
    // float s = dot(color, 1. / 3.);
    // if (s > 0.5 || s < 0.01) return 0;
  }
	return color;
}
