#ifndef _SAMPLERSVS_HLSL_
#define _SAMPLERSVS_HLSL_

#define samLinearSimple samLinearWrap

SamplerState samLinearWrap : register(s0) {
  Filter = LINEAR;
  AddressU = CLAMP;
  AddressV = CLAMP;
  AddressW = CLAMP;
};

SamplerState samLinearClamp : register(s1) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
  AddressW = WRAP;
};

SamplerState samLinearBorder0 : register(s2) {
  Filter = LINEAR;
  AddressU = BORDER;
  AddressV = BORDER;
  AddressW = BORDER;
  BorderColor = (float4)0;
};

SamplerState samLinearBorder1 : register(s3) {
  Filter = LINEAR;
  AddressU = BORDER;
  AddressV = BORDER;
  AddressW = BORDER;
  BorderColor = (float4)1;
};

SamplerState samPointWrap : register(s4) {
  Filter = POINT;
  AddressU = WRAP;
  AddressV = WRAP;
  AddressW = WRAP;
};

SamplerState samPointClamp : register(s5) {
  Filter = POINT;
  AddressU = CLAMP;
  AddressV = CLAMP;
  AddressW = CLAMP;
};

SamplerState samPointBorder0 : register(s6) {
  Filter = POINT;
  AddressU = BORDER;
  AddressV = BORDER;
  AddressW = BORDER;
  BorderColor = (float4)0;
};

SamplerState samPointBorder1 : register(s7) {
  Filter = POINT;
  AddressU = BORDER;
  AddressV = BORDER;
  AddressW = BORDER;
  BorderColor = (float4)1;
};

SamplerComparisonState samShadow : register(s8) {
  Filter = COMPARISON_MIN_MAG_MIP_LINEAR;
  AddressU = BORDER;
  AddressV = BORDER;
  AddressW = BORDER;
  BorderColor = 1;
  ComparisonFunc = LESS;
};

SamplerState samPointMirror : register(s9) {
  Filter = POINT;
  AddressU = MIRROR;
  AddressV = MIRROR;
  AddressW = MIRROR;
  BorderColor = (float4)1;
};

#endif