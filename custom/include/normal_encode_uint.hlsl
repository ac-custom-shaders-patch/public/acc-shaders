float2 normalEncode_octWrap(float2 v) {
  return (1 - abs(v.yx)) * (v.xy >= 0 ? 1 : -1);
}
 
uint normalEncode_u(float3 n) {
  n /= abs( n.x) + abs(n.y) + abs(n.z);
  n.xy = n.z >= 0 ? n.xy : normalEncode_octWrap(n.xy);
  n.xy = n.xy * 0.5 + 0.5;
  uint2 u = uint2(n.xy * 255);
  return (u.x << 8) | u.y;
}
 
float3 normalDecode_u(uint e) {
  float2 f = float2((e >> 8) & 0xff, e & 0xff) / 255. * 2.0 - 1.0;
  float3 n = float3(f.x, f.y, 1 - abs(f.x) - abs(f.y));
  float t = saturate(-n.z);
  n.xy += n.xy >= 0 ? -t : t;
  return normalize(n);
}
