#ifndef GAUSSIAN_TYPE
  #define GAUSSIAN_TYPE float4
#endif

#ifndef GAUSSIAN_PREPROCESS
  #define GAUSSIAN_PREPROCESS(X) X
#endif

GAUSSIAN_TYPE GaussianBlur7(Texture2D<GAUSSIAN_TYPE> tex, SamplerState sam, float2 uv, float2 pixelOffset) {
  // kernel width 7 x 7
  const float gWeights[2] = { 0.4490798, 0.0509202 };
  const float gOffsets[2] = { 0.5380487, 2.0627797 };
  GAUSSIAN_TYPE ret = 0;
  [unroll(16)]
  for (int i = 0; i < 2; ++i) {
    float2 offset = gOffsets[i] * pixelOffset;
    ret += gWeights[i] * (GAUSSIAN_PREPROCESS(tex.SampleLevel(sam, uv + offset, 0)) + GAUSSIAN_PREPROCESS(tex.SampleLevel(sam, uv - offset, 0)));
  }
  return ret;
}

GAUSSIAN_TYPE GaussianBlur15(Texture2D<GAUSSIAN_TYPE> tex, SamplerState sam, float2 uv, float2 pixelOffset) {
  // kernel width 15 x 15
  const float gWeights[4] = { 0.2496147, 0.1924633, 0.0514763, 0.0064457 };
  const float gOffsets[4] = { 0.6443417, 2.3788476, 4.2911105, 6.2166071 };
  GAUSSIAN_TYPE ret = 0;
  [unroll(16)]
  for (int i = 0; i < 4; ++i) {
    float2 offset = gOffsets[i] * pixelOffset;
    ret += gWeights[i] * (GAUSSIAN_PREPROCESS(tex.SampleLevel(sam, uv + offset, 0)) + GAUSSIAN_PREPROCESS(tex.SampleLevel(sam, uv - offset, 0)));
  }
  return ret;
}

GAUSSIAN_TYPE GaussianBlur23(Texture2D<GAUSSIAN_TYPE> tex, SamplerState sam, float2 uv, float2 pixelOffset) {
  // kernel width 23 x 23
  const float gWeights[6] = { 0.1650140, 0.1750711, 0.1011206, 0.0426756, 0.0131566, 0.0029622 };
  const float gOffsets[6] = { 0.6577193, 2.4501660, 4.4109597, 6.3728523, 8.3362617, 10.3015347 };
  GAUSSIAN_TYPE ret = 0;
  [unroll(16)]
  for (int i = 0; i < 6; ++i) {
    float2 offset = gOffsets[i] * pixelOffset;
    ret += gWeights[i] * (GAUSSIAN_PREPROCESS(tex.SampleLevel(sam, uv + offset, 0)) + GAUSSIAN_PREPROCESS(tex.SampleLevel(sam, uv - offset, 0)));
  }
  return ret;
}

GAUSSIAN_TYPE GaussianBlur35(Texture2D<GAUSSIAN_TYPE> tex, SamplerState sam, float2 uv, float2 pixelOffset) {
  // kernel width 35 x 35
  const float gWeights[9] = { 0.1085503, 0.1313501, 0.1040560, 0.0721595, 0.0438034, 0.0232758, 0.0108263, 0.0044078, 0.0015709 };
  const float gOffsets[9] = { 0.6629276, 2.4790385, 4.4623189, 6.4456835, 8.4291687, 10.4128103, 12.3966427, 14.3806973, 16.3650055 };
  GAUSSIAN_TYPE ret = 0;
  [unroll(16)]
  for (int i = 0; i < 9; ++i) {
    float2 offset = gOffsets[i] * pixelOffset;
    ret += gWeights[i] * (GAUSSIAN_PREPROCESS(tex.SampleLevel(sam, uv + offset, 0)) + GAUSSIAN_PREPROCESS(tex.SampleLevel(sam, uv - offset, 0)));
  }
  return ret;
}

GAUSSIAN_TYPE GaussianBlur63(Texture2D<GAUSSIAN_TYPE> tex, SamplerState sam, float2 uv, float2 pixelOffset) {
  // kernel width 63 x 63
  const float gWeights[16] = { 0.0599137, 0.0775810, 0.0723185, 0.0647608, 0.0557112, 0.0460406, 0.0365518, 0.0278768, 0.0204242, 0.0143753, 0.0097197, 0.0063134, 0.0039394, 0.0023614, 0.0013598, 0.0007522 };      
  const float gOffsets[16] = { 0.6655480, 2.4937129, 4.4886847, 6.4836583, 8.4786358, 10.4736176, 12.4686041, 14.4635973, 16.4585972, 18.4536076, 20.4486256, 22.4436531, 24.4386940, 26.4337463, 28.4288101, 30.4238892 };
  GAUSSIAN_TYPE ret = 0;
  [unroll(16)]
  for (int i = 0; i < 16; ++i) {
    float2 offset = gOffsets[i] * pixelOffset;
    ret += gWeights[i] * (GAUSSIAN_PREPROCESS(tex.SampleLevel(sam, uv + offset, 0)) + GAUSSIAN_PREPROCESS(tex.SampleLevel(sam, uv - offset, 0)));
  }
  return ret;
}

GAUSSIAN_TYPE GaussianBlur127(Texture2D<GAUSSIAN_TYPE> tex, SamplerState sam, float2 uv, float2 pixelOffset) {
  // kernel width 127 x 127
  const float gWeights[32] = { 0.0295353, 0.0391023, 0.0384428, 0.0374288, 0.0360891, 0.0344607, 0.0325875, 0.0305181, 0.0283036, 0.0259959, 0.0236454, 0.0212994, 0.0190006, 0.0167859, 0.0146859, 0.0127243, 0.0109181, 0.0092777, 0.0078075, 0.0065067, 0.0053702, 0.0043893, 0.0035529, 0.0028480, 0.0022609, 0.0017775, 0.0013839, 0.0010671, 0.0008148, 0.0006161, 0.0004614, 0.0003422 };
  const float gOffsets[32] = { 0.6663964, 2.4984803, 4.4972649, 6.4960489, 8.4948330, 10.4936180, 12.4924021, 14.4911871, 16.4899712, 18.4887562, 20.4875412, 22.4863262, 24.4851112, 26.4838963, 28.4826832, 30.4814682, 32.4802551, 34.4790421, 36.4778290, 38.4766159, 40.4754028, 42.4741898, 44.4729767, 46.4717636, 48.4705505, 50.4693413, 52.4681282, 54.4669189, 56.4657097, 58.4645004, 60.4632912, 62.4620819 };
  GAUSSIAN_TYPE ret = 0;
  [unroll(16)]
  for (int i = 0; i < 32; ++i) {
    float2 offset = gOffsets[i] * pixelOffset;
    ret += gWeights[i] * (GAUSSIAN_PREPROCESS(tex.SampleLevel(sam, uv + offset, 0)) + GAUSSIAN_PREPROCESS(tex.SampleLevel(sam, uv - offset, 0)));
  }
  return ret;
}