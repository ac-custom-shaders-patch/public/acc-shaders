float2 normalEncode(float3 n, bool unwardsOnly = false) {
  if (n.y < -0.999 && !unwardsOnly) {
    n = float3(0.005, -0.995, 0.005);
  }
  n = normalize(n);
  float p = sqrt(n.y * 8 + 8);
  return n.xz / p + 0.5;
}

float3 normalDecode(float2 enc) {
  float2 fenc = enc * 4 - 2;
  float f = dot(fenc, fenc);
  float g = sqrt(1 - f / 4);
  float3 n;
  n.xz = fenc * g;
  n.y = 1 - f / 2;
  return n;
}