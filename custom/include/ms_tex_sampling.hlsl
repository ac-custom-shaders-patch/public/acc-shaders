float4 sampleTex(Texture2D tex, float2 uv){
  return tex.SampleLevel(samPointClamp, uv, 0);
}

float4 sampleTex(Texture2DMS<float4> tex, float2 uv){
  float4 ret = 0;
  [unroll]
  for (uint i = 0; i < MSTEX_SAMPLES; ++i){
    ret += tex.Load(uv * MSTEX_SIZE, i);
  }
  ret /= SAMPLES;
  return ret;
}

float sampleTex(Texture2D<float> tex, float2 uv){
  return tex.SampleLevel(samPointClamp, uv, 0);
}

float sampleTex(Texture2DMS<float> tex, float2 uv){
  float ret = 0;
  [unroll]
  for (uint i = 0; i < MSTEX_SAMPLES; ++i){
    ret += tex.Load(uv * MSTEX_SIZE, i);
  }
  ret /= SAMPLES;
  return ret;
}

