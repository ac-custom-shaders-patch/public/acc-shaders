

#define RATIO 0.5

float remapSat(float x, float a, float b){
  float d = b - a;
  if (!d) d = 1e-20;
  return saturate((x - a) / d);
}

float remapSigned(float x, float a, float b){
  float d = b - a;
  if (!d) d = 1e-20;
  return clamp((x - a) / d * 2 - 1, -1, 1);
}

struct StripeParams {
  float4 stripes[6];

  bool calculate(float2 p){
    [loop] for (int i = 0; i < 6; ++i){
      if (abs(p.x - stripes[i].x) < stripes[i].y || abs(p.x - stripes[i].z) < stripes[i].w) return true;
    }
    return false;
  }
};

struct LineParams {
  float4 curveThickness[4];
  float4 curve[4];

  float2 region;
  float frequency;
  float offset;
  float thicknessBase;
  float thicknessCurveCorrection;
  float thicknessSplit;
  float depth;

  float bend(float x){
    x = saturate(x);
    float ret = 0;
    [loop] for (int i = 0; i < 4; ++i){
      float v = remapSat(x, curve[i].z, curve[i].w);
      ret += (curve[i].x < 0 ? pow(1 - v, 1 - curve[i].x) : pow(v, curve[i].x)) * curve[i].y;
    }
    return ret;
  }

  float thickness(float x){
    float ret = thicknessBase;
    [loop] for (int i = 0; i < 4; ++i){
      ret = lerp(ret, curveThickness[i].x, 
        curveThickness[i].y * remapSat(x, curveThickness[i].z, curveThickness[i].w));
    }
    return ret;
  }

  bool calculate(float2 p){
    p.x = remapSat(p.x, region.x, region.y);
    if (!any(region) || !p.x || p.x == 1) return false;

    float y0 = bend(p.x);
    float dx = 0.001;
    float dy = bend(p.x + dx) - y0;

    float2 dir = normalize(float2(dx, dy));
    float2 per = float2(dir.y, -dir.x);
    
    float pad = thickness(p.x) * lerp(1, 1 / abs(dir.x), thicknessCurveCorrection);
    float dist = frac((p.y + y0) * frequency + offset);
    return dist < pad * thicknessSplit || dist > 1 - pad * (1 - thicknessSplit);
  }
};

struct BitParams {
  float2 frequency;
  float2 offset;
  float2 p0;
  float2 p1;
  
  float2 curve;
  float thickness;
  float depth;

  float2 area;
  float shape;
  float pad;

  float distanceToLine(float2 p, float2 a, float2 b) {
    float2 ba = (b - a) / frequency;
    float2 pa = (p - a) / frequency;
    float2 de = pa - saturate(dot(pa, ba) / max(0.00000001, dot(ba, ba))) * ba;
    return shape ? max(abs(de.x), abs(de.y)) : length(de);
  }

  float calculate(float2 p){
    float2 i = p * frequency + offset;
    float2 l = frac(i);
    float2 s = sign(p1 - p0);
    float2 c = float2(
      remapSigned(l.y, p0.y - thickness * s.y, p1.y + thickness * s.y), 
      remapSigned(l.x, p0.x - thickness * s.x, p1.x + thickness * s.x));
    l += (1 - pow(c, 2)) * curve;
    return p.x > area.x && p.x < area.y && distanceToLine(l, p0, p1) < thickness;
  }
};

cbuffer cbData : register(b10) {
  float margin;
  float depth;
  float shapeBlur;
  float shapeOffset;
  StripeParams gStripes;
  LineParams gLines[8];
  BitParams gBits[20];
  float2 offset;
  uint phase;
  float _pad;
}

float calculateDepth(float2 p){
  p.y = frac(p.y);
  if (p.x < margin || p.x > 1 - margin) return depth;   
  if (gStripes.calculate(p)) return 0;
  { [loop] for (int i = 0; i < 8; ++i){
    if (!gLines[i].frequency) break;
    if (gLines[i].calculate(p)) return gLines[i].depth;
  } }
  { [loop] for (int i = 0; i < 20; ++i){
    if (!gBits[i].frequency.y) break;
    // if (gBits[i].frequency.y == 10) return 0;
    if (gBits[i].calculate(p)) return gBits[i].depth;
  } }
  return 1;
}