#include "include/common.hlsl"

float3 unpackNormal(float v){
  return float3(
    BYTE0(asuint(v)) / 255.f,
    BYTE1(asuint(v)) / 255.f,
    BYTE2(asuint(v)) / 255.f) * 2 - 1;
}

struct MeshPoint {
	float3 localPos;
  float3 pos;
  float3 normal;
};

uint loadIndex(ByteAddressBuffer buffer, uint pos){
	uint val = buffer.Load((pos / 2) * 4);
	return (pos % 2 ? val >> 16 : val) & 0xffff;
}

MeshPoint randomMeshPoint(ByteAddressBuffer vertices, ByteAddressBuffer indices, uint indicesSize, RAND_DECL){
	uint tri = (uint)((indicesSize / 3) * RAND);
	float f = RAND;
	float g = RAND;
	[flatten]
	if (f + g > 1) {
		f = 1 - f;
		g = 1 - g;
	}

	uint i0 = loadIndex(indices, tri * 3);
	uint i1 = loadIndex(indices, tri * 3 + 1);
	uint i2 = loadIndex(indices, tri * 3 + 2);

	float4 v0 = asfloat(vertices.Load4(i0 * 16));
	float4 v1 = asfloat(vertices.Load4(i1 * 16));
	float4 v2 = asfloat(vertices.Load4(i2 * 16));

	float3 n0 = unpackNormal(v0.w);
	float3 n1 = unpackNormal(v1.w);
	float3 n2 = unpackNormal(v2.w);

  MeshPoint ret;
	ret.pos = v0.xyz + f * (v1.xyz - v0.xyz) + g * (v2.xyz - v0.xyz);
	ret.normal = n0 + f * (n1 - n0) + g * (n2 - n0);
	ret.localPos = ret.pos;
  return ret;
}

MeshPoint randomMeshPoint(ByteAddressBuffer vertices, ByteAddressBuffer indices, uint indicesSize, float4x4 transform, RAND_DECL){
	MeshPoint ret = randomMeshPoint(vertices, indices, indicesSize, RAND_ARGS);
  ret.pos = mul(transform, float4(ret.pos, 1)).xyz;
	ret.normal = normalize(mul((float3x3)transform, ret.normal));
  return ret;
}