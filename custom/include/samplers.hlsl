#ifndef _SAMPLERS_HLSL_
#define _SAMPLERS_HLSL_

#if defined(TARGET_VS) || defined(TARGET_CS) || defined(TARGET_DS)
  #include "include/samplers_vs.hlsl"
#elif defined(TARGET_PS)
  #include "include_new/base/samplers_ps.fx"
#endif

#endif