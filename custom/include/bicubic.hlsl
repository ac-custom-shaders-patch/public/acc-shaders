#ifndef BICUBIC_SAMPLER
  #define BICUBIC_SAMPLER samLinearSimple
#endif

float4 cubic(float v){
    float4 n = float4(1, 2, 3, 4) - v;
    float4 s = n * n * n;
    float x = s.x;
    float y = s.y - 4 * s.x;
    float z = s.z - 4 * s.y + 6 * s.x;
    float w = 6 - x - y - z;
    return float4(x, y, z, w);
}

float4 sampleBicubic(Texture2D tex, float2 texCoords, float level, float2 dim){
  texCoords = texCoords * dim - 0.5;  
  float2 fxy = frac(texCoords);
  float4 xcubic = cubic(fxy.x);
  float4 ycubic = cubic(fxy.y);
  float4 c = (texCoords - fxy).xxyy + float2(-0.5, 1.5).xyxy;    
  float4 s = float4(xcubic.xz + xcubic.yw, ycubic.xz + ycubic.yw);
  float4 offset = (c + float4(xcubic.yw, ycubic.yw) / s) / dim.xxyy;  
  float4 sample0 = tex.SampleLevel(BICUBIC_SAMPLER, offset.xz, level);
  float4 sample1 = tex.SampleLevel(BICUBIC_SAMPLER, offset.yz, level);
  float4 sample2 = tex.SampleLevel(BICUBIC_SAMPLER, offset.xw, level);
  float4 sample3 = tex.SampleLevel(BICUBIC_SAMPLER, offset.yw, level);
  float sx = s.x / (s.x + s.y);
  float sy = s.z / (s.z + s.w);
  return lerp(lerp(sample3, sample2, sx), lerp(sample1, sample0, sx), sy);
}

float4 sampleBicubic(Texture2D tex, float2 texCoords, float level){
  uint2 dim;
  uint sampleCount; 
  tex.GetDimensions(level, dim.x, dim.y, sampleCount); 
  return sampleBicubic(tex, texCoords, level, (float2)dim);
}

float4 sampleBicubicFixed(Texture2D tex, float2 texCoords, uint level){
  uint2 dim;
  uint sampleCount; 
  tex.GetDimensions(level, dim.x, dim.y, sampleCount); 
  return sampleBicubic(tex, texCoords, level, (float2)dim);
}