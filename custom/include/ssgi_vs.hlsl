#include "include/samplers.hlsl"

Texture2D txAO : register(TX_SLOT_AO);

float3 getSSGI(float4 posH){
  float2 ssUV = posH.xy / posH.w / 2 + 0.5;
  ssUV.y = 1 - ssUV.y;
  return (txAO.SampleLevel(samLinearClamp, ssUV, 0).yzw);
}