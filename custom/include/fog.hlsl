#ifndef _FOG_HLSL_
#define _FOG_HLSL_

float calculateFogImpl(float4 posV){
  #if defined OPTIMIZE_FOG
    float c = pow(2, posV.z / ksFogLinear * (5.770780 * 2));
    return saturate((1 - c) / (1 + c)) * ksFogBlend;
  #elif defined FOG_NMUV2_VERSION
    float a = -posV.z / ksFogLinear * 5.770780;
    float b = pow(2, a);
    float c = pow(2, -a);
    float d = 1 / (b + c);
    return saturate((b - c) * d) * ksFogBlend;
  #elif defined FOG_PARTICLE_VERSION
    float a = -posV.z / ksFogLinear * 5.770780;
    float b = pow(2, a);
    float c = pow(2, -a);
    float d = b - c;
    float c0 = c + b;
    float c1 = 1 / c0;
    return saturate(d * c1) * ksFogBlend;
  #else
    float a = -posV.z / ksFogLinear * 5.770780;
    float b = pow(2, a);
    float c = pow(2, -a);
    return saturate((b - c) / (b + c)) * ksFogBlend;
  #endif
}

#ifdef MODE_KUNOS
  float calculateFogImpl(float4 posV, float4 posW, float3 posC){
    float c = pow(2, posV.z / ksFogLinear * 5.770780);
    float b = 1 / c;
    return saturate((b - c) / (b + c)) * ksFogBlend;
  }

  float calculateFogNew(float3 posC){
    return 0;
  }

  float calculateFogNew(float3 posC, float3 posN){
    return 0;
  }
#else
  float calculateFogNewFn(float3 posC, float3 posN, bool includeSecondLayer){
    #ifdef USE_PS_FOG
      if (!GAMMA_FIX_ACTIVE) return 0;
    #endif
    float buggyPart;
    float secondaryLayer = 0;
    if (GAMMA_FIX_ACTIVE) {
      buggyPart = 1 - exp(-length(posC) * ksFogLinear);

      #ifdef FOG_VS_COMPUTE_SECOND      
        includeSecondLayer = true;
      #endif

      if (includeSecondLayer) {
        float4 fog2Color_blend = loadVector(p_fog2Color_blend);
        float yK = posN.y / (extFogConstantPiece + sign(extFogConstantPiece) * abs(posN.y));
        float buggyPart = 1 - exp(-max(length(posC) - 2.4, 0) * pow(2, yK * 5) * p_fog2Linear); 
        secondaryLayer = saturate(fog2Color_blend.w * pow(saturate(buggyPart), p_fog2Exp));
      }
    } else {
      if (abs(posN.y) < 0.001) posN.y = 0.001;
      buggyPart = (1 - exp(-length(posC) * posN.y * ksFogLinear)) / posN.y;
      buggyPart *= extFogConstantPiece;
    } 
    return lerp(ksFogBlend * pow(saturate(buggyPart), extFogExp), 1, secondaryLayer);
  }

  float calculateFogNewFn(float3 posC){
    return calculateFogNewFn(posC, normalize(posC), false);
  }

  float calculateFogImpl(float4 posV, float4 posW, float3 posC, bool includeSecondLayer = false){
    #ifndef FOG_NEW_FORMULA_NO_BRANCH
      [branch]
    #endif
    if (extUseNewFog){
      return calculateFogNewFn(posC, normalize(posC), includeSecondLayer);
    } else {
      float c = pow(2, posV.z / ksFogLinear * 5.770780);
      float b = 1 / c;
      return saturate((b - c) / (b + c)) * ksFogBlend;
    }
  }

  float calculateFogNew(float3 posC){
    #ifndef FOG_NEW_FORMULA_NO_BRANCH
      [branch]
    #endif
    if (extUseNewFog){
      return calculateFogNewFn(posC);
    } else {
      return 0;
    }
  }

  float calculateFogNew(float3 posC, float3 posN){
    #ifndef FOG_NEW_FORMULA_NO_BRANCH
      [branch]
    #endif
    if (extUseNewFog){
      return calculateFogNewFn(posC, posN, false);
    } else {
      return 0;
    }
  }
#endif

// posW is actually not needed at all
#define calculateFog(x) calculateFogImpl(x, 0, vout.PosC)

#endif

#define FOG_FAKE_SHADOW_VS_MULT(posV, posC) (calculateFogImpl(posV, 0, posW.xyz - ksCameraPosition.xyz, true))