#ifndef _BAYER_H_
#define _BAYER_H_

#define bayerN 4
static const float bayerMatrix[][bayerN] = {
  { -1, -0.5, -0.875, -0.375 },
  { -0.25, -0.75, -0.125, -0.625 },
  { -0.8125, -0.3125, -0.9375, -0.4375 },
  { -0.0625, -0.5625, -0.1875, -0.6875 },
};

float getBayer(float4 posH, uint2 offset = 0){
  uint2 pos = uint2(posH.xy);
  pos += offset;
  return bayerMatrix[pos.y % bayerN][pos.x % bayerN];
}

#endif