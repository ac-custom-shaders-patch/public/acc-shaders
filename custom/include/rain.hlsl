#ifndef _RAIN_HLSL_
#define _RAIN_HLSL_

#ifdef __cplusplus

#define ARG_OUT float&

#include <math/math.h>

namespace data
{
	using namespace math;
	
	__forceinline float lerpInvSat(float value, float zeroAt, float oneAt){
		return saturatef(remap(value, zeroAt, oneAt, 0.f, 1.f));
	}

	#else
#define ARG_OUT out float
#define DBG_LIVE(X) 
	#endif

	void rainCalculate(float puddleAmount, float sceneRain, float sceneWet, float sceneWater, float aoValue, float puddleValue, float reliefValue, float racingLineOffset,
		ARG_OUT retDamp, ARG_OUT retWetness, ARG_OUT retWater)
	{
		// Offset scene wetness based on relief
		float reliefExp = lerp(1.15f, 0.85f, lerp(0.5f, reliefValue, saturate(sceneWet * 10.f))); // 1.15 at the hills
		sceneWet = pow(saturate(sceneWet), reliefExp);
		sceneWater = pow(saturate(sceneWater), reliefExp);
		
		// Offset scene wetness and puddle map based on racing line
		float wetOffset = -racingLineOffset; // positive wetOffset makes things wetter
		sceneWet = saturate(sceneWet + wetOffset * lerp(0.005f, 1.f, lerpInvSat(sceneWet, 0.012f, 1.2f)));
		puddleValue = saturate(puddleValue + wetOffset * 0.03f);

		// Puddle thickness based on sceneWater
		float puddleOffset = lerp(pow(1.f - sceneWater, 8.f), pow(1.f - sceneWater, 2.f), sceneWater);
		float puddleForce = (puddleValue - puddleOffset) * 2.5f - 0.1f * sceneWater;

		// Tiny variation in puddle map without clipping
		float tinyPuddle = puddleValue * 40.f;
		tinyPuddle = tinyPuddle / (1.f + tinyPuddle);

		// Puddleness outside of puddles 
		float genPuddleness = lerp(tinyPuddle, 1.f, lerp(0.3f, 0.5f, sceneWet));
		float failedToChannel = lerp(tinyPuddle * 0.25f, 1.f, pow(sceneRain, 2.f));
		float thicknessEst = (genPuddleness * 0.5f + failedToChannel * 0.4f) * sceneWet;
		retDamp = lerp(saturate(sceneWet * 100.f) * lerp(1.f, 1.2f, tinyPuddle), 1.f, lerpInvSat(thicknessEst, 0.f, 0.1f));
		retWetness = remap(thicknessEst, 0.f, 0.5f, 0.f, 1.f);
		retWater = remap(thicknessEst, 0.5f, 1.f, 0.f, 1.f);

		// Max against puddles
		retDamp = max(retDamp, puddleForce * 20.f + lerp(2.f, 0.5f, sceneWater) * saturate(sceneWater * 20));
		retWetness = max(retWetness, puddleForce * 20.f + lerp(0.f, -1.f, sceneWater));
		retWater = max(retWater, puddleForce);

		// Make sure values are within 0…1 range
		retDamp = saturate(retDamp);
		retWetness = saturate(retWetness);
		retWater = saturate(retWater);

		// Offset result based on AO
		retDamp *= lerpInvSat(aoValue, 0.1f, 0.2f);
		retWetness *= pow(lerpInvSat(aoValue, 0.2f, 0.5f), 8.f);
		retWater *= lerpInvSat(aoValue, 0.1f, 0.5f);
	}

	#ifdef __cplusplus
}
#endif

#endif
