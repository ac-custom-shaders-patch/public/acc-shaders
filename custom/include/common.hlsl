#ifndef _COMMON_HLSL_
#define _COMMON_HLSL_

#define AC_INPUT_ELEMENTS\
  float4 PosL : POSITION;\
  float3 NormalL : NORMAL;\
  float2 Tex : TEXCOORD;\
  float3 TangentPacked : TANGENT;

#define AC_INPUT_SKINNED_ELEMENTS\
  float4 PosL : POSITION;\
  float3 NormalL : NORMAL;\
  float2 Tex : TEXCOORD;\
  float3 TangentPacked : TANGENT;\
  float4 BoneWeights : TEXCOORD1;\
  float4 BoneIndices : TEXCOORD2;

#define DISCARD_VERTEX(TYPE) { TYPE vout = (TYPE)0; vout.PosH = -10; return vout; }
#define BYTE0(X) (((X) & 0x000000FF) >> 0)
#define BYTE1(X) (((X) & 0x0000FF00) >> 8)
#define BYTE2(X) (((X) & 0x00FF0000) >> 16)
#define BYTE3(X) (((X) & 0xFF000000) >> 24)

float2 vsLoadAltTex(float3 tangentPacked){
  return tangentPacked.xy;
}

float3 vsLoadTangent(float3 tangentPacked){
  uint u = asuint(tangentPacked.x);
  return float3(BYTE0(u) / 255.f, BYTE1(u) / 255.f, BYTE2(u) / 255.f) * 2 - 1;
}

uint vsLoadFlexOffset(float3 tangentPacked){
  return BYTE3(asuint(tangentPacked.x));
}

float vsLoadAo0(float3 tangentPacked){
  return pow(BYTE0(asuint(tangentPacked.z)) / 255., 2);
}

float vsLoadAo1(float3 tangentPacked){
  return pow(BYTE1(asuint(tangentPacked.z)) / 255., 2);
}

float vsLoadWet(float3 tangentPacked){
  return BYTE2(asuint(tangentPacked.z)) / 255.;
}

float vsLoadHeating(float3 tangentPacked){
  return BYTE3(asuint(tangentPacked.z)) / 255.;
}

uint vsLoadMat0b(float3 tangentPacked){
  return BYTE0(asuint(tangentPacked.y));
}

uint vsLoadMat1b(float3 tangentPacked){
  return BYTE1(asuint(tangentPacked.y));
}

uint vsLoadMat2b(float3 tangentPacked){
  return BYTE2(asuint(tangentPacked.y));
}

uint vsLoadMat3b(float3 tangentPacked){
  return BYTE3(asuint(tangentPacked.y));
}

float vsLoadMat0f(float3 tangentPacked){
  return BYTE0(asuint(tangentPacked.y)) / 255.;
}

float vsLoadMat1f(float3 tangentPacked){
  return BYTE1(asuint(tangentPacked.y)) / 255.;
}

float vsLoadMat2f(float3 tangentPacked){
  return BYTE2(asuint(tangentPacked.y)) / 255.;
}

float vsLoadMat3f(float3 tangentPacked){
  return BYTE3(asuint(tangentPacked.y)) / 255.;
}

#if defined(TARGET_PS) || defined(TARGET_VS) || defined(TARGET_CS)
  #include "include_new/base/_texture_slots.fx"
#endif

#ifdef TARGET_PS
  #include "include_new/base/samplers_ps.fx"
#endif

#define M_PI 3.14159265359
#define M_TAU (M_PI * 2)

float2x2 rotate2d(float sinV, float cosV) {
  return float2x2(cosV, -sinV, sinV, cosV);
}

float dot2(float2 v) {
  return dot(v, v);
}

float dot2(float3 v) {
  return dot(v, v);
}

float dot2(float4 v) {
  return dot(v, v);
}

float distanceToPlane(float3 planeNormal, float planeDistance, float3 p) {
  return dot(planeNormal, p) - planeDistance;
}

float distanceToPlane(float4 plane, float3 p) {
  return dot(plane.xyz, p) - plane.w;
}

float3 nearestOnPlane(float4 plane, float3 p) {
	const float d = distanceToPlane(plane, p);
	return p - d * plane.xyz;
}

uint packColor(float4 color) {
	return uint(saturate(color.r) * 255.0) | (uint(saturate(color.g) * 255.0)
		| (uint(saturate(color.b) * 255.0) | uint(saturate(color.w) * 255.0) << 8) << 8) << 8;
}

float4 unpackColor(uint v) {  
  float4 ret;
  ret.r = ((v >> 0) & 0x000000FF) / 255.0;
  ret.g = ((v >> 8) & 0x000000FF) / 255.0;
  ret.b = ((v >> 16) & 0x000000FF) / 255.0;
  ret.a = ((v >> 24) & 0x000000FF) / 255.0;
  return ret;
}

float4 bilinearFactors(float2 uv) {
  float4 vfrac;
  vfrac.xy = frac(uv);
  vfrac.zw = 1 - vfrac.xy;
  return vfrac.zxzx * vfrac.wwyy;
}

float dithering(float2 screenPosPixels){
  return lerp(0.00196, -0.00196, frac(0.25 + dot(screenPosPixels, 0.5)));
}
  
float3 ssNormalEncode(float3 n) {
  return normalize(n);
}

float3 ssNormalDecode(float3 enc) {
  return enc;
}

float luminance(float3 rgb) {
  return dot(rgb, float3(0.2125, 0.7154, 0.0721));
}

float luminance(float v) {
  return v;
}

float remap(float x, float a, float b, float c, float d) {
  return (x - a) / (b - a) * (d - c) + c;
}

float2 remap(float2 x, float2 a, float2 b, float2 c, float2 d) {
  return (x - a) / (b - a) * (d - c) + c;
}

float3 remap(float3 x, float3 a, float3 b, float3 c, float3 d) {
  return (x - a) / (b - a) * (d - c) + c;
}

float4 remap(float4 x, float4 a, float4 b, float4 c, float4 d) {
  return (x - a) / (b - a) * (d - c) + c;
}

#define IN_RANGE(_TYPE, _NAME) _TYPE _NAME##_RangeInv; _TYPE _NAME##_MinDivRange
#define IN_RANGE_GET(_NAME, _VAL) _VAL * _NAME##_RangeInv + _NAME##_MinDivRange
#define IN_RANGE_SAT(_NAME, _VAL) saturate(IN_RANGE_GET(_NAME, _VAL))

#define TEXTURE2D(NAME, SLOT) Texture2D NAME : register(t##SLOT);
#define TEXTURE2D_TYPE(NAME, TYPE, SLOT) Texture2D<TYPE> NAME : register(t##SLOT);

#ifdef MODE_KUNOS
  #define PARAM_CSP(TYPE, NAME)
#else
  #define PARAM_CSP(TYPE, NAME) TYPE NAME
#endif

float3 bump3(float3 x){
  float3 y = 1 - x * x;
  return max(y, 0);
}

float3 rainbow(float x){
  return bump3(float3(4 * (x - 0.75), 4 * (x - 0.5), 4 * (x - 0.25)));
}

float normalize2Colors(float3 rgb1, float3 rgb2){
  // return dot(rgb1, rgb2) / (max(rgb1.x, max(rgb1.y, rgb1.z)) * max(rgb2.x, max(rgb2.y, rgb2.z)) + 0.0001);
  return dot(rgb1, rgb2) / (dot(rgb1, 1) * dot(rgb2, 1) + 0.0001);
}

float3 remapNormal(float2 val, float3 up, float3 side, float3 nm){
  float3 v = float3(val * 2 - 1, 0);
  v.z = sqrt(saturate(1 - dot2(v.xy)));
  return v.x * side + v.y * up + v.z * nm;
}

void mixLayer(inout float3 resultColor, inout float resultAlpha, float3 layerColor, float layerAlpha){
  float newAlpha = lerp(resultAlpha, 1, layerAlpha);
  resultColor = lerp(resultColor * resultAlpha, layerColor, layerAlpha) / max(0.000001, newAlpha);
  resultAlpha = newAlpha;
}

void mixLayerBelow(inout float3 resultColor, inout float resultAlpha, float3 layerColor, float layerAlpha){  
  float3 resultColorPrev = resultColor;
  float resultAlphaPrev = resultAlpha;
  resultColor = layerColor;
  resultAlpha = layerAlpha;
  mixLayer(resultColor, resultAlpha, resultColorPrev, resultAlphaPrev);
}

float lerpInvSat(float value, float zeroAt, float oneAt){
	return saturate(remap(value, zeroAt, oneAt, 0, 1));
}

float KMH(float v){ return v / 3.6; }

float2 loadVector(uint v) {
	return float2(f16tof32(v), f16tof32(v >> 16));
}

float4 loadVector(uint2 vec) {
	return float4(f16tof32(vec.x), f16tof32(vec.x >> 16), f16tof32(vec.y), f16tof32(vec.y >> 16));
}

float2 loadVector(float2 vec) {
	return vec;
}

float4 loadVector(float4 vec) {
	return vec;
}

float smootherstep(float x){
  return x * x * x * (x * (x * 6 - 15) + 10);
}

float2 rotate2d(float2 vec, float angle){
  float sinX = sin(angle);
  float cosX = cos(angle);
  float sinY = sin(angle);
  return mul(float2x2(cosX, -sinX, sinY, cosX), vec);
}

float getTextureLevel(Texture2D tex, float2 uvR){
  float w;
  float h;
  tex.GetDimensions(w, h);
  return max(0, 0.5 * log2(dot2(uvR * float2(w, h))));
}

#endif

// NZN stands for non-zero normalized
#define LOWEST_ENCODABLE_UINT8 (1.001/255)
#define ENCODE_NZN_UINT8(X) lerp(LOWEST_ENCODABLE_UINT8, 1, X)
#define DECODE_NZN_UINT8(X) remap(X, LOWEST_ENCODABLE_UINT8, 1, 0, 1)
