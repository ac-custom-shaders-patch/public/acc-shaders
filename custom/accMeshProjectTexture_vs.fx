#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct PS_IN {
  float4 PosH : SV_POSITION;
  float3 PosG : TEXCOORD0;
  float2 Tex : TEXCOORD1;
};

cbuffer cbDataObject : register(b1) {
  float4x4 gWorld;
}

PS_IN main(VS_IN_ac vin) {
  PS_IN vout;
  float4 posW = mul(vin.PosL, gWorld);
  float4 posV = mul(posW, ksView);
  vout.PosH = mul(posV, ksProjection);
  vout.PosG = posW.xyz;
  vout.Tex = vin.Tex;
  return vout;
}