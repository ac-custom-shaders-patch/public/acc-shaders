#include "include/common.hlsl"

Texture2D txNow : register(t0);
Texture2D txPrev : register(t1);
Texture2D<float2> txMotion : register(t2);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float2 velocity = txMotion.SampleLevel(samLinearClamp, pin.Tex, 0);
  return lerp(
    txPrev.SampleLevel(samLinearClamp, pin.Tex - velocity, 0),
    txNow.SampleLevel(samLinearClamp, pin.Tex, 0) * 4,
    lerp(0.25, 1, saturate(length(velocity) * 1000)));
}