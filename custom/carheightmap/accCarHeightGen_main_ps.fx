#define NO_CARPAINT
#include "include_new/base/_include_ps.fx"

struct PS_IN {
  float4 PosH : SV_POSITION;
  float3 PosW : TEXCOORD;
};

#define DISCARD_AREAS_NUM 2
cbuffer cbDiscardAreas : register(b10) {
  float4 area[DISCARD_AREAS_NUM];
}

float main(PS_IN pin) : SV_TARGET {
  bool d = false;
  [unroll] for (int i = 0; i < DISCARD_AREAS_NUM; i++){
    float3 dif = pin.PosW - area[i].xyz;
    if (dot2(dif) < area[i].w) d = true;
  }
  if (d) discard;
  return 0;
}
