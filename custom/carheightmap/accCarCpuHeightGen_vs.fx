#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"

struct VS_IN_collider {
  float3 PosL : POSITION;
  uint NormalL : NORMAL_ENCODED;
};

cbuffer cbData : register(b10) {
  float4x4 gTransform;
}

float4 main(VS_IN_collider vin) : SV_POSITION {
  return mul(float4(vin.PosL, 1), gTransform);
}

 