#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct PS_IN {
  float4 PosH : SV_POSITION;
  float3 PosW : TEXCOORD;
};

PS_IN main(VS_IN_ac vin) {
  PS_IN vout;

  float4 posW = mul(vin.PosL, ksWorld);
  float4 posH = mul(posW, ksMVPInverse);
  if (posH.z < 0) posH.z = 0;
  vout.PosH = posH;
  vout.PosW = posW.xyz;

  return vout;
}

