#include "include/common.hlsl"

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float3 gAABBSize;
}

float _v(float dy, float dx){
  return sign(dy) / sqrt(1 + pow(dx / dy, 2));
  // return dy / sqrt(1 - dot2(float2(dx, dy)));
}

float computeD(float a, float b){
  if (a == 1 && b == 1) return 0;
  // if (a == 1) return 1;
  // if (b == 1) return -1;
  return a - b;
}

float3 computeNm(float dx, float dy, float resolution = 128) {
  float3 ret = 0;
  ret.x = _v(dx * gAABBSize.y, gAABBSize.x / resolution);
  ret.y = _v(dy * gAABBSize.y, gAABBSize.z / resolution);
  ret.xy *= float2(sqrt(1 - pow(ret.y, 2)), sqrt(1 - pow(ret.x, 2)));
  ret.z = -sqrt(max(0, 1 - dot2(ret.xy)));
  return ret;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float M = txDiffuse.SampleLevel(samPointClamp, pin.Tex, 0);
  float xL = txDiffuse.SampleLevel(samPointClamp, pin.Tex, 0, int2(-1, 0));
  float xR = txDiffuse.SampleLevel(samPointClamp, pin.Tex, 0, int2(1, 0));
  float yL = txDiffuse.SampleLevel(samPointClamp, pin.Tex, 0, int2(0, -1));
  float yR = txDiffuse.SampleLevel(samPointClamp, pin.Tex, 0, int2(0, 1));
  float xL1 = txDiffuse.SampleLevel(samPointClamp, pin.Tex, 0, int2(-2, 0));
  float xR1 = txDiffuse.SampleLevel(samPointClamp, pin.Tex, 0, int2(2, 0));
  float yL1 = txDiffuse.SampleLevel(samPointClamp, pin.Tex, 0, int2(0, -2));
  float yR1 = txDiffuse.SampleLevel(samPointClamp, pin.Tex, 0, int2(0, 2));
  float3 ret = 0;
  float mX = max(max(xL, xR), max(yL, yR));
  if (mX == 1){
    ret = computeNm(xR1 - xL1, yR1 - yL1, 32);
    ret.z = 0;
    ret = normalize(ret);
  } else {
    ret = normalize(computeNm(computeD(xR, M), computeD(yR, M)) + computeNm(computeD(M, xL), computeD(M, yL)));
  }
  // ret.z = -sqrt(1 - pow(ret.x, 2)) * sqrt(1 - pow(ret.y, 2));
  // ret = normalize(ret);
  // ret.z = -1;
  // ret.xyz = normalize(-1);
  // ret.xy /= 2;
  ret /= gAABBSize.xzy;
  // ret = ret * 0.5 + 0.5;
  // ret = 1;
  // return 1 - M;
  return float4(ret, M);
  // return txDiffuse.SampleLevel(samPoint, pin.Tex, 0);
  // return txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);// + dithering(pin.PosH.xy) * 0;
}