#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

cbuffer cbData : register(b10) {
  float4x4 gTransform;
}

float4 main(VS_IN_ac vin) : SV_POSITION {
  return mul(vin.PosL, gTransform);
}

 