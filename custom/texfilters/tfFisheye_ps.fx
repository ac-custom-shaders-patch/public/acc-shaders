#include "tf.hlsl"

cbuffer cbData : register(b10) {
  TfParams gParams;
  float gIntensity;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float2 uv = pin.Tex * 2 - 1;
  uv *= lerp(1, pow(length(uv / sqrt(2)), 4), gIntensity / 2);
  uv = uv * 0.5 + 0.5;
  return loadTexture(uv, gParams) + dithering(pin.PosH.xy);
}