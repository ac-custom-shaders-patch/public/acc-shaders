#include "include/common.hlsl"

struct TfParams {
  float4 _genParams;
  float exposure;
  float brightness;
  float saturation;
  float gamma;
};

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float3 tonemappingBase(float3 color, TfParams params) {
	color *= params.exposure / (1 + color / params.exposure);
	return color;
}

float3 tonemappingReinhardJodie(float3 v, TfParams params) {
  v *= params.exposure;
  float l = luminance(v);
  float3 tv = v / (1 + v);
  return lerp(v / (1 + l), tv, tv);
}

float3 adjustColor(float3 color, TfParams params){
  if (params.exposure != 0){
    color = pow(max(0, color * params._genParams.x), params._genParams.y);
    color = tonemappingReinhardJodie(color, params);
  }
  color = pow(saturate(color), params.gamma);
  color *= params.brightness;
  color = saturate(lerp(luminance(color), color, params.saturation));
  return color;
}

float2 adjustUV(float2 uv, TfParams params){
  return uv;
}

float4 loadTexture(float2 uv, TfParams params){
  uv = adjustUV(uv, params);
  float4 ret = txDiffuse.Sample(samLinear, uv);
  ret.rgb = adjustColor(ret.rgb, params);
  return ret;
}