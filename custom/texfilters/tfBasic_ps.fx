#include "tf.hlsl"

cbuffer cbData : register(b10) {
  TfParams gParams;
}

float4 main(VS_Copy pin) : SV_TARGET {
  return loadTexture(pin.Tex, gParams) + dithering(pin.PosH.xy);
}