#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"

#ifdef WITH_ANIMATED_WIPERS
  #include "../../custom_objects/common/animatedWipers.hlsl"
#endif

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct PS_IN {
  float4 PosH : SV_POSITION;
  float4 Color : TEXCOORD0;
};

cbuffer cbDataObject : register(b1) {
  float4x4 gWorld;
  float4 gColor;
  uint gElementId;
}

PS_IN main(VS_IN_ac vin) {
  #ifdef WITH_ANIMATED_WIPERS
    for (int i = 0; i < ENTRIES_COUNT; ++i){
      if (gWiperParams[i].calculate(vin.PosL.xyz, vin.NormalL.xyz, vsLoadMat0b(vin.TangentPacked), i, false)) break;
    }
  #endif

  PS_IN vout;
  float4 posW = mul(vin.PosL, gWorld);
  float4 posV = mul(posW, ksView);
  vout.PosH = mul(posV, ksProjection);
  vout.Color = gColor;
  uint elementId = vsLoadMat3b(vin.TangentPacked);
  if (gElementId != 0xffffffff && elementId < 256 && elementId != gElementId){
    DISCARD_VERTEX(PS_IN);
  }
  return vout;
}