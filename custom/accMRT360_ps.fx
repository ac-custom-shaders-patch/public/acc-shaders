#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float gMargin;
  float3 _pad;
}

float4 samplePiece(float2 xy, float z, float sx, float sy){
  if (z < 0) return 0;
  float2 xya = xy / z * gMargin;
  float2 u = (0.5 - xya * 0.5 + float2(sx, sy)) / float2(3, 2);
  float d = max(abs(xya.x), abs(xya.y));
  return float4(txDiffuse.SampleBias(samLinear, u, -1).rgb, 1) * pow(saturate(remap(d, gMargin, 1, 1, 0)), 2);
}

float4 sampleColor(float3 dir){
  float4 v = 0;
  v += samplePiece(dir.xy * float2(-1, 1), dir.z, 1, 0);
  v += samplePiece(dir.yx * float2(1, -1), -dir.z, 1, 1);
  v += samplePiece(dir.zy, dir.x, 2, 0);
  v += samplePiece(dir.zy * float2(-1, 1), -dir.x, 0, 0);
  v += samplePiece(dir.zx * float2(1, -1), dir.y, 0, 1);
  v += samplePiece(-dir.zx, -dir.y, 2, 1);
  return v / v.w;
}

float4 main(VS_Copy pin) : SV_TARGET {
  // return txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);

  float2 angle = float2((pin.Tex.x + 0.5) * M_PI * 2, (pin.Tex.y - 0.5) * M_PI);
  float xs = sin(angle.x);
  float xc = cos(angle.x);
  float ys = sin(angle.y);
  float yc = cos(angle.y);
  return sampleColor(float3(xs * yc, -ys, xc * yc));
}