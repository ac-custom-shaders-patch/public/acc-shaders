#define ALLOW_PERVERTEX_AO
#define SUPPORTS_NORMALS_AO

#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"

struct PS_IN_Basic {
  float4 PosH : SV_POSITION;
  float Ao : TEXCOORD1;
};

float4 toScreenSpaceAlt(float4 posL, out float4 posW, out float4 posV){
  posW = mul(posL, ksWorld);
  posV = mul(posW, ksView);
  return mul(posV, ksProjection);
}

PS_IN_Basic main(VS_IN vin) {
  PS_IN_Basic vout;
  float4 posW, posV;
  vout.PosH = toScreenSpaceAlt(vin.PosL, posW, posV);
  PREPARE_AO(vout.Ao);
  return vout;
}
