#define NO_CARPAINT
#include "include_new/base/_include_ps.fx"

struct PS_IN_Basic {
  float4 PosH : SV_POSITION;
  float Ao : TEXCOORD1;
};

float4 main(PS_IN_Basic pin) : SV_TARGET {
  return pin.Ao;
}
