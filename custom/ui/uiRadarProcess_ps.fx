#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float4x4 gTransform;
}

Texture2DMS<float4> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

// #include "include/poisson.hlsl"
// float4 samplePoisson(float2 uv, float level, float radius){
//   float4 ret = 0;
//   #define PUDDLES_DISK_SIZE 32
//   for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
//     float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * radius;
//     ret += txDiffuse.SampleLevel(samLinearSimple, uv + offset, level);
//   }
//   ret /= PUDDLES_DISK_SIZE;
//   return ret;
// }

float4 main(VS_Copy pin) : SV_TARGET {
  float4 resValue = 0;
  [unroll]
  for (uint i = 0; i < 4; ++i){
    resValue += txDiffuse.Load(pin.PosH.xy, i);
  }
  resValue /= 4;
  resValue.rgb /= max(0.1, resValue.a);
  resValue.a *= saturate(remap(length(pin.Tex * 2 - 1), 0.8, 1, 1, 0));
  return resValue;

  // // float4 base = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, lerp(1.5, 2.5, dis));
  // float4 base = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 1);
  // base.rgb /= max(0.1, base.a);
  // base.a = saturate(remap(base.a, 0.25, 0.75, 0, 1));

  // // float4 glow = samplePoisson(pin.Tex, 5.5, 0.05);
  // float4 glow = samplePoisson(pin.Tex, 4.5, 0.05);
  // glow.rgb /= max(0.001, glow.a);
  // // glow.a = pow(glow.a, 2);

  // float4 blurred = samplePoisson(pin.Tex, 0, 0.003);
  // // float contour = saturate((txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 1).a - blurred) * 6);
  // // return saturate(contour * 20);
  // // return glow;

  // // float4 ret = base;// + glow * hit * saturate(dis * 3 - 1) * (1 - base.a);
  // float4 ret = blurred;// + glow * hit * saturate(dis * 3 - 1) * (1 - base.a);
  // // ret.a *= saturate(smoothstep(1, 0.8, dis));
  // // ret.a *= saturate(remap(dis, 0.8, 1, 1, 0));
  // // ret.a += hit * saturate(dis * 3 - 1);
  // // ret.rgb /= ret.a;
  // // ret.a *= ret.a;

  // // float radarEffect = pow(hit, 4) * pow(saturate(dis * 2) * saturate((1 - dis) * 2), 2);
  // // mixLayer(ret.rgb, ret.a, float3(0, 1, 0), radarEffect);
  // return ret;
}