#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_GL pin) : SV_TARGET {
  float4 ret = pin.Color;
  ret *= float4(0.9, 0.5, 0.5, 1);
  ret.x += 0.1 * ((pin.PosH.x + pin.PosH.y) % 16 > 8);
  return ret;
}
