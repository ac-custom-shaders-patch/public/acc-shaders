#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float4x4 gTransform;
  float4 gExtras;
}

cbuffer cbDataPerCar : register(b11) {
  float4x4 gWorld;
  float4 gColor;
}

struct VS_IN_collider {
  float3 PosL : POSITION;
  uint NormalL : NORMAL_ENCODED;
};

struct PS_IN {
  float4 PosH : SV_POSITION;
  float3 NormalW : NORMAL;
  float PosY : TEXCOORD1;
  float2 Tex : TEXCOORD;
};

float4 toScreenSpaceAlt(float4 posL){
  float4 posH = mul(mul(posL, gWorld), gTransform);
  if (posH.z < 0) posH.z = 0;
  return posH;
} 

PS_IN main(VS_IN_collider vin) {
  PS_IN vout;
  vout.PosH = toScreenSpaceAlt(float4(vin.PosL, 1));

  float3 normalL = normalize(float3(
    BYTE0(asuint(vin.NormalL)) / 255.f,
    BYTE1(asuint(vin.NormalL)) / 255.f,
    BYTE2(asuint(vin.NormalL)) / 255.f) * 2 - 1);
  vout.NormalW = mul(normalL, (float3x3)gWorld);
  vout.PosY = vin.PosL.y;
  vout.Tex = 0;

  // vin.PosL.xy =  mul(mul(float4(vin.PosL, 1), gWorld), gTransform).xz * 0.1;
  // vin.PosL.xy =  mul(float4(vin.PosL, 1), gWorld).xz * 0.1;
  // vin.PosL.xy =  vin.PosL.xz * 0.1;
  // vin.PosL.xy = vin.PosL.xz * 0.01;
  // vout.PosH = float4(vin.PosL.xy, 0, 1);
  return vout;
}
