#include "include/common.hlsl"

cbuffer cbDataPerCar : register(b11) {
  float4x4 gWorld;
  float4 gColor;
  float gShape;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float3 NormalW : NORMAL;
  float PosY : TEXCOORD1;
  float2 Tex : TEXCOORD;
};

Texture2D txDiffuse : register(t0);

float4 main(VS_Copy pin) : SV_TARGET {
  float4 m = txDiffuse.Sample(samLinear, pin.Tex) * gColor;

  float shadedParam = saturate(min(normalize(pin.NormalW).y * 0.5 + 0.5, pin.PosY));
  float3 shaded = lerp(saturate(lerp(luminance(m.xyz), m.xyz, 4)) * float3(0.1, 0.15, 0.2), m.xyz, shadedParam);
  m.xyz = lerp(m.xyz, shaded, gShape);

  return m;
}