#include "include/common.hlsl"

#ifdef USE_MSAA
  Texture2DMS<float4> txDiffuse : register(t0);
#else
  Texture2D txDiffuse : register(t0);
#endif

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10){
  float gGamma;
  float gBrightness;
  float2 gPad0;
}

float4 main(VS_Copy pin) : SV_TARGET {
  #ifdef USE_MSAA
    float4 v = 0;
    for (uint i = 0; i < 4; ++i){
      v += txDiffuse.Load(pin.PosH.xy, i);
    }
    v /= 4;
  #else
    float4 v = txDiffuse.Load(int3(pin.PosH.xy, 0));
  #endif

  #ifdef USE_HDR
    v.rgb = pow(max(v.rgb * gBrightness, 0), gGamma);
  #endif
  #ifndef USE_OPAQUE
    v.rgb /= max(v.a, 0.001);
  #endif
  return v;
}