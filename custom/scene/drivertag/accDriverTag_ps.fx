
#define ALLOW_LIGHTINGFX
// #define AO_EXTRA_LIGHTING 1
// #define GI_LIGHTING 0
#define LIGHTINGFX_NOSPECULAR
#define LIGHTINGFX_EXTRA_FOCUSED
#define LIGHTINGFX_KSDIFFUSE 0.5
// #define LIGHTINGFX_SIMPLEST
// #define SIMPLE_MODE

#include "include/common.hlsl"
#include "include_new/base/_include_ps.fx"

cbuffer cbData : register(b10) {
  float4 gPos[32];
  float2 gSize;
  float gRenderDistanceMultInv;
  float gWhiteRefPoint;
}

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
  float Opacity : TEXCOORD3;
  float2 TexRaw : TEXCOORD4;
  uint BgColor : TEXCOORD5;
};

RESULT_TYPE main(PS_IN pin) {
  READ_VECTOR_TOCAMERA
  #ifdef SIMPLE_MODE
    pin.Tex.x = 1 - pin.Tex.x;
  #endif

  float4 txValue = txDiffuse.SampleBias(samLinearClamp, pin.Tex, -1);
  txValue.rgb = GAMMA_LINEAR(txDiffuse.SampleBias(samLinearClamp, pin.Tex, -1).rgb * 8) / 8;
  txValue.rgb *= extWhiteRefPoint;

  float3 lighting = txValue.xyz;
  float alpha = txValue.a;

  #ifndef SIMPLE_MODE
    lighting /= max(0.01, txValue.a);
    float4 bgValue = txPrevFrame.SampleLevel(samLinearClamp, pin.PosH.xy * extScreenSize.zw, 3);
    bgValue.rgb *= 0.4;
    bgValue = lerp(float4(0.2, 0.2, 0.2, 0.8), bgValue, bgValue.a);
    bgValue *= unpackColor(pin.BgColor);

    lighting += bgValue.rgb * 0.5;
    lighting = lerp(bgValue.rgb, lighting, alpha);
    alpha = lerp(alpha, 1, bgValue.a) * pin.Opacity;
  #else
    alpha = lerp(0.9, 1, alpha) * pin.Opacity;
  #endif

  float2 offset = max(0, abs(pin.TexRaw) * float2(8, 1) - float2(7.8, 0.8)) * 10;
  clip(2 - length(offset));

  #ifndef SIMPLE_MODE
    LightingParams L = (LightingParams)0;
    L.txDiffuseValue = 1;
    float3 normalW = toCamera;
    float3 lightingBak = lighting;
    LIGHTINGFX(lighting);
    float3 lightingAdd = lighting - lightingBak;
    lighting = lightingBak + lightingAdd / (1 + dot(lightingAdd, 1) / extWhiteRefPoint);
  #endif

  // #ifndef SIMPLE_MODE
  //   lighting = float3(3, 0, 0);
  //   alpha = 1;
  // #endif

  RETURN_BASE(lighting, alpha);
}