#include "include_new/base/_include_vs.fx"

cbuffer cbData : register(b10) {
  float4 gPos[32];
  float2 gSize;
  float gRenderDistanceMultInv;
  uint gBgColor;
}

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
  float Opacity : TEXCOORD3;
  float2 TexRaw : TEXCOORD4;
  uint BgColor : TEXCOORD5;
};

static const float3 BILLBOARD[] = {
  float3(-1, -1, 0),
  float3(1, -1, 0),
  float3(-1, 1, 0),
  float3(-1, 1, 0),
  float3(1, -1, 0),
  float3(1, 1, 0),
};

PS_IN main(uint fakeIndex: SV_VertexID SPS_VS_ARG_CD) {
  PS_IN vout;

  uint vertexID = fakeIndex % 6;
  #ifdef USE_SPS
    uint bitID = fakeIndex / 12;
    uint instanceID = fakeIndex % 12 < 6 ? 0 : 1;
  #else
    uint bitID = fakeIndex / 6;
  #endif
  float3 quadPos = BILLBOARD[vertexID];

  if (gPos[bitID].w < 0){
    DISCARD_VERTEX(PS_IN);
  }

  float2 offsetSigned = quadPos.xy;
  float3 posBase = gPos[bitID].xyz + float3(0, 0.5, 0);

  float3 look = normalize(posBase - ksCameraPosition.xyz);
  float3 side = normalize(cross(look, float3(0, 1, 0)));
  float3 up = float3(0, 1, 0);
  
  posBase += (side * offsetSigned.x * gSize.x + up * (offsetSigned.y + 1) * 0.95 * gSize.y) / extCameraTangent * 0.001 * lerp(50, length(posBase - ksCameraPosition.xyz), 0.97);
  float4 posW = float4(posBase, 1);
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

  vout.PosH = posH;
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.BgColor = gBgColor;

  float2 offset = offsetSigned * float2(1, 0.95) * 0.5 + 0.5;
  vout.Tex = float2(offset.x, ((1 - offset.y) + gPos[bitID].w) / 32);
  
  vout.TexRaw = offsetSigned;
  vout.Fog = calculateFog(posV);

  float distanceToCamera = length(vout.PosC);
  float fovMult = saturate(min(ksFOV, 50) * 0.0125);
  vout.Opacity = saturate(smoothstep(3, 5, distanceToCamera))
    * saturate(smoothstep(40, lerp(35, 20, fovMult), gRenderDistanceMultInv * distanceToCamera * lerp(0.15, 1, fovMult)));
  SPS_RET(vout);
}
