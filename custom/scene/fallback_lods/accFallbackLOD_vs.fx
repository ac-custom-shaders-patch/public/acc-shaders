#define RAINFX_STATIC_OBJECT
#define MODE_MAIN_FX
#define ALLOW_DYNAMIC_SHADOWS
#define ksAlphaRef 0

#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"

struct VS_IN_collider {
  float3 PosL : POSITION;
  uint NormalL : NORMAL_ENCODED;
};

struct PS_IN_FallbackLOD {
  float4 PosH : SV_POSITION;
  sample float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  // float2 Tex : TEXCOORD2;
  CUSTOM_STRUCT_FIELDS
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  sample float Fog : TEXCOORD8;
  CUSTOM_MODE_REGISTERS
};

cbuffer cbViewport : register(b10) {
  float4x4 gTransform;
  float3 gAABBMin;
  float gAO;
  float3 gAABBMax;
  float gPad1;
}

float3 getPosRel(float3 posL){
  float3 ret = (posL.xyz - gAABBMin) / (gAABBMax - gAABBMin);
  ret.xz = ret.xz * 2 - 1;
  return ret;
}

float4 __toScreenSpace_own(float4 posL, out float4 posW, out float4 posWnoflex, out float4 posV SPS_VS_TOSS_ARG){
  ALTER_POSL(posL.xyz, false);

  posW = mul(posL, gTransform);
  alterPosW(posL.xyz, false, posW);
  posWnoflex = posW;
  APPLY_CFX(posW);

  #ifdef MODE_SHADOWS_ADVANCED
    posV = 0;
    return mul(posW, ksMVPInverse);
  #endif

  posV = mul(posW, ksView);

  float4 posVL = posV;
  return mul(posVL, ksProjection);
}

PS_IN_FallbackLOD main(VS_IN_collider vin SPS_VS_ARG) {
  PS_IN_FallbackLOD vout;
  float3 normalL = normalize(float3(
    BYTE0(asuint(vin.NormalL)) / 255.f,
    BYTE1(asuint(vin.NormalL)) / 255.f,
    BYTE2(asuint(vin.NormalL)) / 255.f) * 2 - 1);
  float4 posW, posWnoflex, posV;
  vin.PosL *= 1.1;
  if (vin.PosL.y < 0.4) vin.PosL.y -= 0.1;
  vout.PosH = __toScreenSpace_own(float4(vin.PosL, 1), posW, posWnoflex, posV SPS_VS_TOSS_PASS);
  OPT_STRUCT_FIELD_POSC(vout.PosC = posW.xyz - ksCameraPosition.xyz);

  // float3 posRel = getPosRel(vin.PosL);
  // float3 dir = normalize(posRel);
  // vout.Tex = normalize(dir.xz) * float2(1, -1) * (1 - dir.y) * 0.5 + 0.5;
  // vout.NormalW = mul(normalL, (float3x3)gTransform);
  vout.NormalW = vin.PosL;

  // vout.Tex = 0;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV))
  GENERIC_PIECE_MOTION(float4(vin.PosL, 1));
  PREPARE_AO(vout.Ao);
  shadows(posW, SHADOWS_COORDS);
  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout); 
}