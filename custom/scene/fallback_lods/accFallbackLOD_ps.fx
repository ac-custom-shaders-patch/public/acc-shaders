#define CARPAINT_NM
#define GETNORMALW_SAMPLER samLinear
// #define SUPPORTS_AO
// #define SUPPORTS_DITHER_FADING

#define INPUT_DIFFUSE_K 1
#define INPUT_AMBIENT_K 1
#define INPUT_SPECULAR_K 1
#define INPUT_SPECULAR_EXP 40
#define INPUT_EMISSIVE3 0

#define MODE_MAIN_FX
#define RAINFX_STATIC_OBJECT
#define ALLOW_DYNAMIC_SHADOWS
#define ksAlphaRef 0

#include "include_new/base/_include_ps.fx"
#include "include/common.hlsl"

struct PS_IN_FallbackLOD {
  float4 PosH : SV_POSITION;
  sample float3 NormalW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  // float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  sample  float Fog : TEXCOORD8;
};

cbuffer cbViewport : register(b10) {
  float4x4 gTransform;
  float3 gAABBMin;
  float gAO;
  float3 gAABBMax;
  float gPad1;
}

float3 getPosRel(float3 posL){
  float3 ret = (posL.xyz - gAABBMin) / (gAABBMax - gAABBMin);
  ret.xz = ret.xz * 2 - 1;
  return ret;
}

#include "include/poisson.hlsl"

PS_OUT main(PS_IN_FallbackLOD pin) {
  float3 toCamera = normalize(pin.PosC);
  float3 dirLocal = mul((float3x3)gTransform, toCamera);
  // float3 dirGlobal = mul(dirLocal, (float3x3)gTransform);
  
  float3 posRel = getPosRel(pin.NormalW);
  float3 dir = normalize(posRel);
  float2 surfUV = normalize(dir.xz) * float2(-1, -1) * (1 - dir.y) * 0.5 + 0.5;

  float surfDepth = txNormal.SampleLevel(samPointBorder0, surfUV, 0).w;  
  float4 debugValue = -1;

  float firstError = length(posRel) - surfDepth * 2;
  float lastError = 0;
  float mult = 1;
  for (int i = 0; i < 32; ++i) {
    float curDistance = length(posRel);
    float actualDistance = surfDepth * 2;

    // float shift = curDistance - actualDistance;
    // debugValue = float4(shift, 1 - shift, 0, 1);

    lastError = surfDepth == 0 ? -0.01 : (curDistance - actualDistance);
    if (surfDepth != 0 && firstError * lastError < 0) mult = 0.1;
    pin.NormalW += dirLocal * (lastError * mult);// * (1 - abs(dot(dirLocal, normalize(posRel))) * 0.9);

    {    
      posRel = getPosRel(pin.NormalW);
      dir = normalize(posRel);
      surfUV = normalize(dir.xz) * float2(-1, -1) * (1 - dir.y) * 0.5 + 0.5;
      surfDepth = txNormal.SampleLevel(samPointBorder0, surfUV, 0).w;
    }
  }

  // if (length(posRel) > surfDepth * 2 + 0.1) discard;

  float4 surfDiffuse = txDiffuse.Sample(samLinearBorder0, surfUV);
  float4 surfMaterial = txMaps.Sample(samLinearBorder0, surfUV);
  float4 surfNormal = txNormal.Sample(samLinearBorder0, surfUV);

  posRel = normalize(posRel) * surfNormal.w * 2;
  posRel.xz = posRel.xz * 0.5 + 0.5;
  pin.NormalW = lerp(gAABBMin, gAABBMax, posRel);

  float3 newPosC = mul(float4(pin.NormalW, 1), gTransform).xyz - ksCameraPosition.xyz;
  // if (dot(normalize(newPosC), toCamera) < 0.9999) debugValue = float4(0, 0, 0, 1);
  // if (dot(normalize(newPosC), toCamera) < 0.999) discard;
  if (!surfDepth || mult == 1 && (dot(normalize(newPosC), toCamera) < 0.999 || abs(lastError) > 0.1)) discard;

  pin.PosC = newPosC;
  toCamera = normalize(newPosC);

  // if (lastError > abs(firstError) * 1.1) debugValue = float4(0, 0, 0, 1);

  // float4 surfDiffuse = 0;
  // float4 surfNormal = 0;
  // float4 surfMaterial = 0;
  // #define PUDDLES_DISK_SIZE 6
  // for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
  //   float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.003;
  //   float2 sampleUV = surfUV + offset;
  //   surfDiffuse += txDiffuse.Sample(samLinear, sampleUV);
  //   surfNormal += txNormal.Sample(samLinear, sampleUV);
  //   surfMaterial += txMaps.Sample(samLinear, sampleUV);
  // }
  // surfDiffuse /= PUDDLES_DISK_SIZE;
  // surfNormal /= PUDDLES_DISK_SIZE;
  // surfMaterial /= PUDDLES_DISK_SIZE;

  float3 normalW = mul(normalize(surfNormal.xyz * 2 - 1), (float3x3)gTransform);
  pin.NormalW = normalW;
  {
    PS_OUT ret;
    ret.result = float4((float3)saturate(normalW.y), 1);
    // return ret;
  }

  // return float4(pin.NormalW + 1, 1);
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW
  extraShadow *= gAO;

  float alpha = 1, directedMult;
  // normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha, directedMult);

  float4 txDiffuseValue = float4(surfDiffuse.xyz, 1);
  // clip(surfDiffuse.w - 0.5);
  // ADJUSTCOLOR(txDiffuseValue);
  APPLY_VRS_FIX;
  // RAINFX_WET(txDiffuseValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING, 1);

  // this.shadow = shadow * directedMult;
  // this.ambientAoMult = ambientAoMult;
  float ksSpecularEXP = lerp(255, 0, surfMaterial.w);
  L.specularValue = surfMaterial.z * 0.5;
  L.specularExp = ksSpecularEXP;
  // #ifdef HAS_SUN_SPECULAR
  //   this.sunSpecularValue = sunSpecular;
  //   this.sunSpecularExp = sunSpecularEXP;
  // #endif

  // L.txSpecularValue = GET_SPEC_COLOR;
  // APPLY_CAO;
  // RAINFX_SHINY(L);

  float3 lighting = L.calculate();
  // BOUNCEBACKFX(lighting, BOUNCEBACKFX_DIFFUSEMASK);
  LIGHTINGFX(lighting);

  ReflParams R = getReflParamsZero();
  R.useBias = true;

    R.fresnelEXP = surfMaterial.y * 10;
    R.ksSpecularEXP = ksSpecularEXP;
    R.finalMult = 1;
    R.metallicFix = 1;
    R.reflectionSampleParam = REFL_SAMPLE_PARAM_DEFAULT;
    R.coloredReflections = 1;
    R.coloredReflectionsColor = 1;
    R.fresnelMaxLevel = surfMaterial.z;
    R.fresnelC = surfMaterial.x; 

  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);
  float4 withReflection = calculateReflection(float4(lighting, alpha), toCamera, pin.PosC, normalW, R);
  if (HAS_FLAG(FLAG_ALPHA_TEST)
  #ifdef MODE_GBUFFER
    || HAS_FLAG(FLAG_GBUFFER_PREMULTIPLIED_ALPHA)
  #endif
  ) withReflection.a = alpha;

  RAINFX_WATER(withReflection);

  // withReflection.rgb = surfMaterial.rgb * 1;

  #ifdef MODE_GBUFFER
    // if (HAS_FLAG(FLAG_GBUFFER_PREMULTIPLIED_ALPHA)) {
    //   R.resultColor = lerp(10, R.resultColor, withReflection.a);
    //   R.resultPower = lerp(1, R.resultPower, withReflection.a);
    //   R.resultBlur = lerp(0, R.resultBlur, withReflection.a);
    //   R.coloredReflections = lerp(1, R.coloredReflections, withReflection.a);
    //   R.coloredReflectionsColor = lerp(0, R.coloredReflectionsColor, withReflection.a);
    //   withReflection.a = 1;
    // }
  #endif

  if (debugValue.x != -1) withReflection = debugValue;
  withReflection.a = 1;

  RETURN(withReflection, withReflection.a);
}