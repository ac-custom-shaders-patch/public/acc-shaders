#define USE_BACKLIT_FOG
#define ALLOW_CAR_FLEX
#define WINDSCREEN_OUTPUT_FADING windscreenFxFading 
#include "include_new/base/_include_vs.fx"

static const float2 BILLBOARD[] = {
	float2(0, 0),
	float2(1, 0),
	float2(0, 1),
	float2(0, 1),
	float2(1, 0),
	float2(1, 1),
};

cbuffer cbData : register(b5) {
  float3 gPos;
  float gBlend;
  float3 gUp;
  uint gColorRG;
  float3 gSide;
  uint gColorBA;
}

struct PS_IN_custom {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
  float TexTransition : TEXCOORD3;
};

PS_IN_custom main(uint id: SV_VertexID SPS_VS_ARG) {
  PS_IN_custom vout;

  float2 B = BILLBOARD[id];
  float4 posW = float4(gPos + gSide * B.x + gUp * B.y, 1);
  APPLY_CFX(posW);
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);
  
  vout.PosH = posH;
  vout.Color = float4(loadVector(gColorRG), loadVector(gColorBA));
  vout.Tex = float2(B.x, 1 - B.y);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
	vout.Fog = calculateFog(posV);
	vout.TexTransition = gBlend;
  float windscreenFxFading = 1;
  VERTEX_POSTPROCESS(vout);
  vout.Color *= windscreenFxFading;
  SPS_RET(vout);
}
