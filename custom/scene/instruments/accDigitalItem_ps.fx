#define USE_BACKLIT_FOG
#include "include_new/base/_include_ps.fx"

struct PS_IN_custom {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
  float TexTransition : TEXCOORD3;
};

Texture2D txBase : register(t0);
Texture2D txTop : register(t1);

RESULT_TYPE main(PS_IN_custom pin) {
  READ_VECTOR_TOCAMERA
  float4 base = GAMMA_LINEAR(txBase.Sample(samLinearClamp, pin.Tex) * pin.Color);
  float3 lighting;
  float alpha;
  [branch]
  if (pin.Tex.x > pin.TexTransition){
    lighting = base.rgb;
    alpha = base.w;
  } else {
    float4 top = txTop.Sample(samLinearClamp, pin.Tex) * pin.Color;
    base.rgb *= base.w;
    top.rgb *= top.w;
    lighting = base.rgb * (1 - top.w) + top.rgb;
    alpha = lerp(base.w, 1, top.w);  
    lighting /= max(alpha, 0.0001);
  }
  RETURN(lighting, alpha);
}