#define USE_BACKLIT_FOG
#define ALLOW_CAR_FLEX
#define WINDSCREEN_OUTPUT_FADING windscreenFxFading 
#include "include_new/base/_include_vs.fx"

static const float2 BILLBOARD[] = {
	float2(0, 0),
	float2(1, 0),
	float2(0, 1),
	float2(0, 1),
	float2(1, 0),
	float2(1, 1),
};

struct Item {
  float posStart;
  float posSize;
  float texStart;
  float texSize;
};

#ifndef ITEMS_COUNT
  #define ITEMS_COUNT 160 // TODO: smaller version!
#endif

cbuffer cbData : register(b5) {
  float3 gColor;
  float gHeight;
  Item gItems[ITEMS_COUNT];
}

struct PS_IN_custom {
  float4 PosH : SV_POSITION;
  float3 Color : COLOR;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
};

PS_IN_custom main(uint id: SV_VertexID SPS_VS_ARG) {
  PS_IN_custom vout;

  #if ITEMS_COUNT == 1
    uint instanceID = 0;
    uint vertexID = id;
  #else
    uint instanceID = id / 6;
    uint vertexID = id % 6;
  #endif

  float3 gPos = ksWorld[3].xyz;
  float3 gSide = -ksWorld[0].xyz;
  float3 gUp = ksWorld[1].xyz * gHeight;

  Item I = gItems[instanceID];
  float2 B = BILLBOARD[vertexID];
  float4 posW = float4(gPos + gSide * (I.posStart + I.posSize * B.x) + gUp * B.y, 1);
  APPLY_CFX(posW);
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);
  
  vout.PosH = posH;
  vout.Color = gColor;
  vout.Tex = float2(I.texStart + I.texSize * B.x, 1 - B.y);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
	vout.Fog = calculateFog(posV);
  float windscreenFxFading = 1;
  VERTEX_POSTPROCESS(vout);
  vout.Color *= windscreenFxFading;
  SPS_RET(vout);
}
