#define USE_BACKLIT_FOG
#include "include_new/base/_include_ps.fx"

struct PS_IN_custom {
  float4 PosH : SV_POSITION;
  float3 Color : COLOR;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
};

Texture2D txBase : register(t0);
Texture2D txTop : register(t1);

RESULT_TYPE main(PS_IN_custom pin) {
  READ_VECTOR_TOCAMERA
  float base = saturate(txBase.Sample(samLinear, pin.Tex).r);
  float3 lighting = GAMMA_KSEMISSIVE(pin.Color.rgb) * extEmissiveMults.y;
  float alpha = GAMMA_ALPHA(base);
  RETURN(lighting, alpha);
}