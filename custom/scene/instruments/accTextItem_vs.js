[
  'accTextItem_1_vs.fx',
  'accTextItem_4_vs.fx',
  'accTextItem_16_vs.fx',
  'accTextItem_vs.fx',
  'accDigitalItem_vs.fx',
].map(x => ({
  source: x,
  target: FX.Target.VS,
  saveAs: x.replace(/_vs\.fx/, '_vs_windscreen.fxo'),
  defines: { 'MODE_WINDSCREEN_FX': 1 }
})).flat()
