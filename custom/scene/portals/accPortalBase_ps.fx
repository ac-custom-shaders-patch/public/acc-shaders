#include "include/common.hlsl"
#include "include_new/base/_include_ps.fx"
#include "include_new/ext_functions/depth_map.fx"

struct PS_IN {
  float4 PosH : SV_POSITION;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  centroid float Fog : TEXCOORD3;
};

cbuffer cbData : register(b10) {
  float3 gPos;
  uint gColor;
}

float4 sampleNoise(float2 uv, bool fixUv = false){
  if (fixUv){
    float textureResolution = 32;
    uv = uv * textureResolution + 0.5;
    float2 i = floor(uv);
    float2 f = frac(uv);
    uv = i + f * f * (3 - 2 * f);
    uv = (uv - 0.5) / textureResolution;
  }
	return txNoise.SampleLevel(samLinearSimple, uv, 0);
}

RESULT_TYPE main(PS_IN pin) {
  float len = length(pin.Tex * 2 - 1);
  
	float3 centerToCamera = normalize(gPos + float3(0, 2 - 0.5, 0) - ksCameraPosition.xyz);
  READ_VECTOR_TOCAMERA
  pin.Tex += 0.01 + float2(atan2(toCamera.x, toCamera.z) / (2 * M_PI), 1 - asin(toCamera.y) / (M_PI / 2));

  float4 color = unpackColor(gColor);
  float rndOffset = dot(gPos - extSceneOffset, float3(3, 2.7, 1.7));

  float4 n0 = sampleNoise(pin.Tex * 0.5, true);
  float4 n1 = sampleNoise(n0.xy * 0.1 + 0.5 * sin(rndOffset + ksGameTime * 0.00017));
  float4 n2 = sampleNoise(n1.xy * 0.1 + 0.5 * sin(rndOffset + ksGameTime * 0.00011));

  float4 k0 = sampleNoise(pin.Tex, true);
  float4 k1 = sampleNoise(n0.xy * 0.1 + 0.5 * sin(rndOffset + ksGameTime * 0.00027));
  float4 k2 = sampleNoise(n1.xy * 0.1 + 0.5 * sin(rndOffset + ksGameTime * 0.00031));

  float edge = saturate(remap(len + n2.x * 0.5, 0.6, 0.8, 0, 1));
  float3 lighting = GAMMA_LINEAR(color.rgb * (1 + 10 * saturate(remap(len + n2.z * 0.5, 0.75, 0.8, 1, 0)))) * getEmissiveMult();
  // lighting *= edge;

  float3 reflDir = float3(-1, 1, -1) * normalize(toCamera + (toCamera - centerToCamera) * 2);
  reflDir += k2.xyz * 0.1;

  lighting = lerp(txCube.Sample(samLinear, reflDir).rgb, lighting, edge);

  #ifdef NO_DEPTH_MAP
    float softK = 1.0;
  #else
    float depthZ = getDepthAccurate(pin.PosH);
    float depthC = linearizeAccurate(pin.PosH.z) + k2.w * 0.5;
    float softK = saturate((depthZ - depthC) * 4);
  #endif

  lighting += pow(softK, 2) * pow(1 - softK, 2) * color.rgb * 2000;

  float alpha = saturate(remap(len + n2.y * 0.5, 0.9, 1, 1, 0)) * lerp(1, 0.5, edge) * color.a * softK;

  RETURN(lighting, alpha);
}
