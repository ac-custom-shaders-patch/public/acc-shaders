#include "include_new/base/_include_vs.fx"

struct PS_IN {
  float4 PosH : SV_POSITION;
  float3 PosC : TEXCOORD1;
  float2 Tex : TEXCOORD2;
  centroid float Fog : TEXCOORD3;
};

cbuffer cbData : register(b10) {
  float3 gPos;
  uint gColor;
  float4 gPos2;
  float4 gPos3;
}

PS_IN main(uint id: SV_VertexID) {
	float3 toCamera = normalize(gPos - ksCameraPosition.xyz);
  float3 toSide = normalize(cross(toCamera, extDirUp));

  float2 offset = float2(id & 1, id >> 1);

  PS_IN vout;
  float4 posW = float4(gPos + extDirUp * (offset.y * 4 - 0.25) + toSide * (offset.x - 0.5) * 4, 1);  
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);
  vout.PosH = posH;
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = offset;
  vout.Fog = calculateFog(posV);
  return vout;
}
