#include "include_new/base/_include_vs.fx"

cbuffer cbViewport : register(b11) {
  float4 _viewport;
}

struct PS_IN_custom {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0; 
  float2 ScreenPos : TEXCOORD1;
  float3 PosC : TEXCOORD2;
  float Fog : TEXCOORD3;
  float3 PosL : TEXCOORD4;       // local mesh position
  float3 NormalW : TEXCOORD5;    // world-space normal
};

PS_IN_custom main(VS_IN vin SPS_VS_ARG) {
  PS_IN_custom vout;

  float4 pos = mul(vin.PosL, ksWorld);  
  float4 posW = float4(pos.xyz, 1);
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);
  
  vout.PosH = posH;
  vout.NormalW = mul(vin.NormalL.xyz, (float3x3)ksWorld);
  vout.PosL = vin.PosL.xyz;
  vout.Tex = vin.Tex;
  vout.ScreenPos = _viewport.xy + _viewport.zw * (posH.xy * float2(0.5, -0.5) + 0.5);
  vout.PosC = pos.xyz - ksCameraPosition.xyz;
	vout.Fog = calculateFog(posV);
  SPS_RET(vout);
}
