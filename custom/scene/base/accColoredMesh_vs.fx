#define USE_BACKLIT_FOG
#include "include_new/base/_include_vs.fx"

cbuffer cbData : register(b1) {
  float4x4 gTransform;
  float4 gExtras;
}

struct VS_IN_collider {
  float3 PosL : POSITION;
  uint Color : COLOR;
};

struct PS_IN_custom {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
};

PS_IN_custom main(VS_IN_collider vin SPS_VS_ARG) {
  PS_IN_custom vout;

  float4 pos = mul(float4(vin.PosL, 1), gTransform);  
  float4 posW = float4(pos.xyz, 1);
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);
  
  vout.PosH = posH;
  vout.Color = unpackColor(vin.Color) * gExtras;
  vout.Tex = 0;
  vout.PosC = pos.xyz - ksCameraPosition.xyz;
	vout.Fog = calculateFog(posV);
  SPS_RET(vout);
}
