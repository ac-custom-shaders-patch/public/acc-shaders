#define USE_BACKLIT_FOG
#include "include_new/base/_include_ps.fx"

cbuffer cbData : register(b10) {
  float4 gColorMult;
  float4 gPos[4];
  float4 gBorderColor;
}

struct PS_IN_custom {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
};

float4 main(PS_IN_custom pin) : SV_TARGET {
  float alpha = min(
    10 * (pin.Tex.y - abs(pin.Tex.x * 2 - 1)),
    20 * (abs(pin.Tex.x * 2 - 1) * 0.25 - pin.Tex.y + 0.75));
  clip(alpha);
  float4 ret = pin.Color 
    * lerp(gBorderColor, gColorMult, saturate(remap(alpha, 1, 2, 0, 1))) 
    * float4(1, 1, 1, saturate(alpha));
  float3 toCamera = normalize(pin.PosC);
  return withFog(ret.rgb, pin.Fog, ret.w);
}