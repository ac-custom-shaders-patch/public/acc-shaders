#include "include_new/base/_include_vs.fx"

cbuffer cbViewport : register(b11) {
  float4 _viewport;
}

struct PS_IN_custom {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0; 
  float2 ScreenPos : TEXCOORD1;
  float3 PosC : TEXCOORD2;
  float Fog : TEXCOORD3;
  float3 PosL : TEXCOORD4;       // local mesh position
  float3 NormalW : TEXCOORD5;    // world-space normal
};

PS_IN_custom main(VS_IN_Skinned vin SPS_VS_ARG) {
  PS_IN_custom vout;

  float4 posW, posWnoflex, posV;
  float3 normalW, tangentW;
  vout.PosH = toScreenSpaceSkinned(vin.PosL, vin.NormalL, vin.TangentPacked, vin.BoneWeights, vin.BoneIndices,
    posW, posWnoflex, normalW, tangentW, posV);

  // float4 pos = mul(vin.PosL, ksWorld);  
  // float4 posW = float4(pos.xyz, 1);
	// float4 posV = mul(posW, ksView);
	// float4 posH = mul(posV, ksProjection);
  
  vout.NormalW = normalW;
  vout.PosL = vin.PosL.xyz;
  vout.Tex = vin.PosL.xz;
  vout.ScreenPos = _viewport.xy + _viewport.zw * (vout.PosH.xy * float2(0.5, -0.5) + 0.5);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
	vout.Fog = calculateFog(posV);
  SPS_RET(vout);
}
