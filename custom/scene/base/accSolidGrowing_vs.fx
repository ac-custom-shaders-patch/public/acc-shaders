#define USE_BACKLIT_FOG
#include "include_new/base/_include_vs.fx"
#include "include/normal_encode.hlsl"

struct VS_IN_custom {
  float3 PosW : POSITION;
  float TrackWidth : COLOR;
  float3 Dir : NORMAL;
  float TrackSide : COLOR1;
};

struct PS_IN_custom {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
};

cbuffer cbData : register(b10) {
  float gOffsetSize;
  float gTrackWidthSize;
  float gOffsetX;
  float gOffsetY;
}

PS_IN_custom main(VS_IN_custom vin SPS_VS_ARG) {
  PS_IN_custom vout;
  
  float4 posW = float4(vin.PosW + extSceneOffset + vin.Dir * (gOffsetSize + vin.TrackWidth * gTrackWidthSize), 1);
  posW.xyz -= vin.Dir * vin.TrackSide * gOffsetX;
  posW.y += gOffsetY;
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);

  vout.PosH = posH;
  vout.Color = 1;
  vout.Tex = 0;
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
	vout.Fog = calculateFog(posV);
  SPS_RET(vout);
}
