#define USE_BACKLIT_FOG
#include "include_new/base/_include_vs.fx"

cbuffer cbData : register(b10) {
  float4 gColorMult;
  float4 gPos[4];
}

struct PS_IN_custom {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
};

PS_IN_custom main(uint id: SV_VertexID SPS_VS_ARG) {
  PS_IN_custom vout;

  float4 pos = gPos[id];  
  float4 posW = float4(pos.xyz, 1);
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);
  
  vout.PosH = posH;
  vout.Color = unpackColor(asuint(pos.w));
  vout.Tex = float2(id & 1, id >> 1);
  vout.PosC = pos.xyz - ksCameraPosition.xyz;
	vout.Fog = calculateFog(posV);
  SPS_RET(vout);
}
