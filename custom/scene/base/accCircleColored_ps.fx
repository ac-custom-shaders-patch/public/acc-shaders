#define USE_BACKLIT_FOG
#include "include_new/base/_include_ps.fx"

cbuffer cbData : register(b10) {
  float4 gColorMult;
  float4 gPos[4];
  float4 gBorderColor;
}

struct PS_IN_custom {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
};

float4 main(PS_IN_custom pin) : SV_TARGET {
  float alpha = remap(dot2(pin.Tex * 2 - 1), 0.8, 1, 1, 0);
  clip(alpha);
  // return float4(pin.Tex, 0, 1);

  // return gBorderColor;

  float4 ret = pin.Color * lerp(gBorderColor, gColorMult, saturate(remap(alpha, 1, 2, 0, 1))) * float4(1, 1, 1, saturate(alpha));
  float3 toCamera = normalize(pin.PosC);
  return withFog(ret.rgb, pin.Fog, ret.w);
}