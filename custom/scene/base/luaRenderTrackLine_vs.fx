#include "include_new/base/_include_vs.fx"
#include "include/normal_encode_uint.hlsl"

cbuffer cbViewport : register(b11) {
  float4 _viewport;
}

struct VS_IN_custom {
  float4 PosL : POSITION;
  float3 NormalL : NORMAL;
  float2 Tex : TEXCOORD;
  float3 TangentPacked : TANGENT;

  #define Dir NormalL
  #define TrackWidth TangentPacked.x
  #define TrackSide TangentPacked.y
  #define NormalPacked TangentPacked.z
};

struct PS_IN_custom {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0; 
  float2 ScreenPos : TEXCOORD1;
  float3 PosC : TEXCOORD2;
  float Fog : TEXCOORD3;
  float3 PosL : TEXCOORD4;       // local mesh position
  float3 NormalW : TEXCOORD5;    // world-space normal
};

cbuffer cbData : register(b10) {
  float gOffsetSize;
  float gTrackWidthSize;
  float gOffsetX;
  float gOffsetY;
}

PS_IN_custom main(VS_IN vin SPS_VS_ARG) {
  PS_IN_custom vout;

  if (vin.TrackWidth == 0){
    vin.TrackWidth = 1 - pow(vin.Tex.y * 2 - 1, 2);
  }

  float4 posW = mul(float4(vin.PosL.xyz + vin.Dir * (gOffsetSize + vin.TrackWidth * gTrackWidthSize), 1), ksWorld);
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);
  
  vout.PosH = posH;
  vout.NormalW = mul(normalDecode_u(asuint(vin.NormalPacked)), (float3x3)ksWorld);
  vout.PosL = vin.PosL.xyz;
  vout.Tex = vin.Tex;
  vout.ScreenPos = _viewport.xy + _viewport.zw * (posH.xy * float2(0.5, -0.5) + 0.5);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
	vout.Fog = calculateFog(posV);
  SPS_RET(vout);
}
