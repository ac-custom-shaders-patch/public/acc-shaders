#define USE_BACKLIT_FOG
#include "include_new/base/_include_ps.fx"

cbuffer cbData : register(b10) {
  float4 gColorMult;
}

struct PS_IN_custom {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
};

float4 main(PS_IN_custom pin) : SV_TARGET {
  float4 ret = pin.Color * gColorMult;
  float3 toCamera = normalize(pin.PosC);
  return withFog(ret.rgb, pin.Fog, ret.w);
}