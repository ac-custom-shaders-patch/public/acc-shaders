#define USE_BACKLIT_FOG
#define FOG_MULT_MODE
#include "include_new/base/_include_ps.fx"
#include "include_new/ext_functions/depth_map.fx"

struct PS_IN_custom {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD0;
  float Fog : TEXCOORD1;
  float3 PosC : TEXCOORD2;
};

float4 main(PS_IN_custom pin) : SV_TARGET {
  float depthZ = getDepthAccurate(pin.PosH);
  float depthC = linearizeAccurate(pin.PosH.z);
  float softK = saturate((depthZ - depthC) * 2);

  float alpha = pow(saturate(1 - length(pin.Tex * 2 - 1)), 20) * 0.5
    + pow(saturate(1 - length(pin.Tex * 2 - 1)), 4) * 0.08;
  alpha *= GAMMA_OR(2.5, 1);
  clip(alpha - 0.00001);
  float4 ret = float4(GAMMA_KSEMISSIVE(pin.Color.rgb) * getEmissiveMult(), 
    GAMMA_KSEMISSIVE(pin.Color.w * saturate(alpha) * softK));
  float3 toCamera = normalize(pin.PosC);
  // ret.gb = 0;
  // ret.w *= withFog(1, pin.Fog, 1);
  // ret.rgb *= 10;
  // ret.w = 1;
  // ret.w = 0;
  return ret;
}