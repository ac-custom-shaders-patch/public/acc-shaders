#define CUSTOM_STRUCT_FIELDS\
  float CornerModifier : EXTRA0;

#include "include_new/base/_include_vs.fx"
#include "include/fog.hlsl"

cbuffer cbData : register(b10) {
  float4 gPos0;
  float4 gPos1;
  float4 gPos2;
  float4 gPos3;
}

static const float2 BILLBOARD[] = {
	float2(-1, -1),
	float2(1, -1),
	float2(-1, 1),
	float2(-1, 1),
	float2(1, -1),
	float2(1, 1),
};

PS_IN_FakeCarShadows main(uint id: SV_VertexID SPS_VS_ARG) {
  PS_IN_FakeCarShadows vout;
  float2 ver = BILLBOARD[id];  

  float4 cornerData = id == 0 ? gPos0 : id == 1 || id == 4 ? gPos1 : id == 2 || id == 3 ? gPos2 : gPos3;
  float4 posW = mul(float4(cornerData.xyz, 1), ksWorld);  
  float3 toCamera = SPS_CAMERA_POS - posW.xyz;
  posW.xyz += toCamera * min(0.05, 0.1 / (0.5 + abs(toCamera.y)));
	float4 posV = mul(posW, ksView);
	float4 posH = mul(posV, ksProjection);
  vout.PosH = posH;
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = ver;
  vout.Fog = pow(saturate(1 - FOG_FAKE_SHADOW_VS_MULT(posV, vout.PosC)), GAMMA_OR(32, 4));
  vout.CornerModifier = cornerData.w;
  shadows(posW, SHADOWS_COORDS);
  SPS_RET(vout);
}
