#define CUSTOM_STRUCT_FIELDS\
  float CornerModifier : EXTRA0;

#define SHADOWS_CASCADES_TRANSITION
#define EXTENDED_PEROBJECT_CB
#ifndef MODE_GBUFFER
  #define USE_BACKLIT_FOG
  #define FOG_MULT_MODE
#endif
#define STENCIL_VALUE 0
#define GBUFF_MASKING_MODE true
// #define GBUFF_MASKING_FALLBACK_ONLY

#include "include_new/base/_flags.fx"
#include "include_new/base/cbuffers_common.fx"
#include "include_new/base/structs_ps.fx"
#include "include_new/base/samplers_ps.fx"
#include "include_new/base/textures_ps.fx"
#include "include_new/base/utils_ps_fog.fx"
#include "include_new/base/utils_ps_gbuff.fx"
#include "include_new/ext_shadows/_include_ps.fx"
#include "include_new/ext_functions/depth_map.fx"
#include "include/fog.hlsl"

#ifdef ALLOW_LIGHTINGFX
	#define AO_EXTRA_LIGHTING 1
	#define LIGHTINGFX_SPECULAR_EXP 1
	#define LIGHTINGFX_SPECULAR_COLOR 0
	#define LIGHTINGFX_KSDIFFUSE 1
	#define GAMMA_LIGHTINGFX_KSDIFFUSE_ONE
  #define LIGHTINGFX_NOSPECULAR
  #define LIGHTINGFX_SIMPLEST
	#define POS_CAMERA posC
	#include "include_new/ext_lightingfx/_include_ps.fx"
#endif

#define SHADOWS_COORDS pin.ShadowTex0
#define AO_LIGHTING 1

cbuffer cbData : register(b10) {
  float2 gSquaredness;
  float gOpacity;
  float gDynamicLightsFactor;
}

#ifdef MODE_GBUFFER
  PS_OUT main(PS_IN_FakeCarShadows pin) {
#else
  float4 main(PS_IN_FakeCarShadows pin) : SV_TARGET {
#endif
  float3 posC = pin.PosC;
  float distanceToCamera = length(pin.PosC);
  float3 toCamera = pin.PosC / distanceToCamera;

  pin.Tex = sign(pin.Tex) * saturate(abs(pin.Tex) * (gSquaredness * pin.CornerModifier) - (gSquaredness * pin.CornerModifier) + 1);  
  float value = saturate(1 - dot2(pin.Tex));

  #ifndef SIMPLE_MODE
    float shadow = getShadow(pin.PosC, pin.PosH, float3(0, 1, 0), SHADOWS_COORDS, 1);
    float ambientValue = max(dot(ksAmbientColor_sky0, 1), 2);
    float lightValue = max(dot(ksLightColor, 1), 0);

    float3 normalW = float3(0, 1, 0);
    float3 lighting = 0;
    #ifdef ALLOW_LIGHTINGFX
      LIGHTINGFX(lighting);
    #endif

    #ifdef MODE_GBUFFER
      #error configuration error
    #endif

    float luminanceMultiplier = lerp(1, ambientValue / max(0.01, shadow * lightValue + dot(lighting, gDynamicLightsFactor /* 0.02 */) + ambientValue), 0.2);
    value = pow(value, 2 - pow(luminanceMultiplier, 4));

    if (useNearbyInteriorClipping){
      value *= saturate(remap(distanceToCamera, 1.1, 1.2, 0, 1));
    }
  #endif

  float result = GAMMA_OR(pow(saturate(gOpacity), 0.4545), gOpacity) * value * pin.Fog;

  #ifdef MODE_GBUFFER
    PS_OUT ret = (PS_OUT)1;
    ret.baseReflection = 1 - result;
    return ret;
  #else
    return float4(0, 0, 0, result);
  #endif
}
