// #if !defined(MODE_MAIN_NOFX) && !defined(MODE_SHADOW)
// 	#define MODE_MAIN_FX
// #endif

#define CUSTOM_STRUCT_FIELDS\
  float3 Rand : EXTRA0;

#include "include_new/base/_include_vs.fx"
#include "include/samplers.hlsl"
#include "include/random_cs.hlsl"

#define VTX_PER_ITEM 3

static const float2 BILLBOARD[] = {
  float2(-1, -1.9),
  float2(1.9, 0),
  float2(-1, 1.9),
};

cbuffer _cbData : register(b10) {
  float3 gPos_;
  float gScale;
  float gOpacity;
  float gTime;
}

float3 computePos(float iA, float iY, float iT, float posRand0, float4 posRand) {
  float bY = iY * lerp(95, 100, posRand0);
  iY = saturate(iY);
  float pR = saturate(iY * 10) + iY * 4 + 40 * pow(iY, 8) + 100 * pow(iY, 20) + 1 * pow(1 - iY, 200);
  float3 gPos = gPos_;
  float sS = (1 + sqrt(iY) * 4);
  gPos.x += sin((iT - iY * 7) * 1.19) * sS;
  gPos.x += sin((iT - iY * 5) * 1.29) * sS;
  gPos.z += sin((iT - iY * 9) * 1.27 + 0.796) * sS;
  gPos.z += sin((iT - iY * 3) * 1.37 + 0.796) * sS;
  // gPos += float3(RAND, RAND, RAND) * 4 - sS;
  pR += (posRand.x + (pow(posRand.y, 8) + pow(posRand.z, 40) + pow(posRand.w, 400)) * (3 + 20 * pow(iY, 1))) * (0.2 + iY) + (sin((iT - iY * 10) * 1.67 + 1.796) + sin((iT - iY * 10) * 1.57 + 1.596) * 0.5) * 0.1;
  gPos += float3(sin(iA), 0, cos(iA)) * pR + float3(0, bY, 0);
  return gPos;
}

PS_IN_PerPixel main(uint fakeIndex : SV_VERTEXID SPS_VS_ARG_CD) {
  uint vertexID = fakeIndex % VTX_PER_ITEM;
  #ifdef USE_SPS
    uint bitID = fakeIndex / (VTX_PER_ITEM * 2);
    uint instanceID = fakeIndex % (VTX_PER_ITEM * 2) < VTX_PER_ITEM ? 0 : 1;
  #else
    uint bitID = fakeIndex / VTX_PER_ITEM;
  #endif

  // uint rowID = bitID / (uint)gCountAcross;
  // uint columnID = bitID % (uint)gCountAcross;
  // float columnX = (columnID + 1) / (gCountAcross + 1);
  // float rowAngle = (rowID + (float)columnID / gCountAcross) / gCountAlong * M_PI * 2;

  bitID = 9999 - bitID;
  
  RAND_INIT(0.12345, float2(bitID / 17.1717, gScale));

  float2 B = BILLBOARD[vertexID];

  // float angle = 

  float iT = gTime * 0.2;
  
  float iY = saturate(bitID / 10000.);

  iY = 1 - pow(1 - iY, 1.5);

  float iTA = iT * (lerp(0.2, 40, pow(1 - iY, 2)) + RAND * pow(1 - iY, 4));
  float iA = bitID * (M_PI / 40.) + iTA;  

  float posRand0 = RAND;
  float4 posRand = float4(RAND, RAND, RAND, RAND);

  float3 gPos0 = computePos(iA, iY, iT, posRand0, posRand);
  float3 gPosU = computePos(iA, (iY + 0.01), iT, posRand0, posRand);
  float3 gDirO = float3(sin(iA), 0, cos(iA));

  float SM = (0.5 + iY * 2) * (1 + RAND) + pow(iY, 4) * 4;
  if (iY < 0.05 && RAND > 0.8) {
    float k = RAND;
    // pR += 1 + k * 10;
    SM += 1 + k * 10;
  }
  float F = dot(float3(sin(iA), 0, cos(iA)), normalize(gPos0 - ksCameraPosition));
  SM *= max(lerpInvSat(F, 0.5, 0.4), lerpInvSat(F, 0.5, 0.6));
  if (F > 0.5) {
    iA += M_PI;
  }


  float gScaleAdj = gScale / 100;
  float3 gPos = lerp(gPos_, gPos0, gScaleAdj);
  SM *= gScaleAdj;

  float3 posC = gPos - ksCameraPosition.xyz;
  float3 toSide = normalize(cross(posC, float3(0, 1, 0)));
  float3 toUp = normalize(cross(posC, toSide));
  
  float4 posW = float4(gPos + (toUp * B.x + toSide * B.y) * SM, 1);
  float4 posV = mul(posW, ksView);
  float4 posH = mul(posV, ksProjection);

  PS_IN_PerPixel vout;
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = B.yx * float2(1, -1); //  * 0.1 + float2(RAND, RAND);
  // vout.NormalW = normalize(float3(sin(iA), -pow(iY, 8) * 4 + (RAND - 0.5) * 2, cos(iA)));

  float3 nm0 = normalize(gPosU - gPos0);
  float3 nm1 = normalize(cross(gDirO, nm0));
  float3 nmF = normalize(cross(nm0, nm1));

  // vout.NormalW = normalize(float3(sin(iA), (length((gPosU - gPos_).xz) - length((gPos - gPos_).xz)) * -10, cos(iA)));
  vout.NormalW = nmF;
  vout.Rand = float3(RAND, RAND, (1 - pow(iY, 4)) * gOpacity * 0.2); 
  vout.PosH = posH;
  shadows(posW, SHADOWS_COORDS);

  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  GENERIC_PIECE_STATIC(posW - float4(gWheelVelocity, 0));
  SPS_RET(vout);
}