#include "insTyreSpikes.hlsl"
#include "include_new/base/_include_vs.fx"
#include "include/samplers.hlsl"
#include "include/random_cs.hlsl"

Texture2D<float> txShape : register(t0);

#if defined(MODE_SHADOW)
  #define VTX_PER_ITEM 6
#else
  #define VTX_PER_ITEM 12
  #include "include_new/ext_tyresfx/_include_vs.fx"
#endif

static const float3 BILLBOARD[] = {
  float3(0, -1, 1),
  float3(1, 0, 0),
  float3(0, 1, 0),
  float3(0, 1, 0),
  float3(-1, 0, 0),
  float3(0, -1, 1),

  #if VTX_PER_ITEM == 12
    float3(-1, -1.9, 0),
    float3(1.9, 0, 0),
    float3(-1, 1.9, 0),
    float3(-1, -1.9, 0.67),
    float3(1.9, 0, 0.67),
    float3(-1, 1.9, 0.67),
  #endif
};

float4 toScreenSpaceAlt(float4 posW){
  float4 posH = mul(posW, ksMVPInverse);
  if (posH.z < 0) posH.z = 0;
  return posH;
} 

PS_STRUCT main(uint fakeIndex : SV_VERTEXID SPS_VS_ARG_CD) {
  uint vertexID = fakeIndex % VTX_PER_ITEM;
  #ifdef USE_SPS
    uint bitID = fakeIndex / (VTX_PER_ITEM * 2);
    uint instanceID = fakeIndex % (VTX_PER_ITEM * 2) < VTX_PER_ITEM ? 0 : 1;
  #else
    uint bitID = fakeIndex / VTX_PER_ITEM;
  #endif

  uint rowID = bitID / (uint)gCountAcross;
  uint columnID = bitID % (uint)gCountAcross;
  float columnX = (columnID + 1) / (gCountAcross + 1);
  float rowAngle = (rowID + (float)columnID / gCountAcross) / gCountAlong * M_PI * 2;
  
  RAND_INIT(0.12345, float2(columnX, rowAngle));
  rowAngle += RAND * M_PI * 0.85 / gCountAlong;
  columnX += (RAND - 0.5) * 0.45 / gCountAcross;

  float rowS = sin(rowAngle), rowC = cos(rowAngle);
  float3 offsetDir = (gWheelUp * rowS + gWheelLook * rowC);
  float3 gWheelSide = normalize(cross(gWheelLook, gWheelUp));
  float actualRadiusBase = (1 - txShape.SampleLevel(samLinearClamp, float2(columnX, 0.5), 0)) * 4;
  if (actualRadiusBase == 0 || actualRadiusBase < gWheelRadius - 0.018) {
    DISCARD_VERTEX(PS_STRUCT);
  }

  float spikeSize = loadVector(pWidth_Height).x;
  float spikeHeight = loadVector(pWidth_Height).y;
  // spikeSize *= 10;
  // spikeHeight *= 10;

  float3 B = BILLBOARD[vertexID];
  if (vertexID >= 9) {
    B.xy *= 0.33;
  }
  float3 posWBase = gWheelPos + offsetDir * (actualRadiusBase + spikeHeight * B.z)
    + gWheelSide * lerp(-1, 1, columnX) * (gTyreWidth / 2);

  float samplePoint = columnX;
  float3 offsetBase;
  if (vertexID < 6) {
    #ifdef MODE_SHADOW
      float3 toCamDir = ksLightDirection.xyz;
      float3 dir0 = normalize(cross(offsetDir, toCamDir));
      float3 dir1 = normalize(cross(offsetDir, dir0));
      offsetBase = (B.x * dir0 + B.y * dir1);
    #else
      float3 toCamDir = normalize(posWBase - ksCameraPosition.xyz);
      float3 dir0 = normalize(cross(offsetDir, toCamDir));
      float3 dir1 = normalize(cross(offsetDir, dir0));
      offsetBase = (B.x * dir0 + B.y * dir1) * lerp(1, 0, B.z);
    #endif
    samplePoint += dot(offsetBase, gWheelSide) * spikeSize / gTyreWidth;
  } else {
    float3 dir0 = normalize(cross(offsetDir, gWheelSide));
    offsetBase = B.x * dir0 + B.y * gWheelSide;
    samplePoint += B.y * spikeSize / gTyreWidth;
  }
  
  #ifdef MODE_SHADOW
    float4 posW = float4(posWBase + offsetBase * spikeSize, 1);
  #else
    float actualRadius = (1 - txShape.SampleLevel(samLinearClamp, float2(samplePoint, 0.5), 0)) * 4;
    if (actualRadius == 0) actualRadius = actualRadiusBase;
    float4 posW = float4(posWBase + offsetDir * (actualRadius - actualRadiusBase), 1);

    float3 normalW = offsetDir;
    squashTyre(0, posW.xyz, normalW);
  #endif
  
  #ifdef MODE_SHADOW
    PS_IN vout;
    // posW.xyz -= ksLightDirection.xyz * 0.1;
    vout.PosH = toScreenSpaceAlt(posW);
    vout.Tex = 0;
    return vout;
  #else
    float3 gStretchDir = normalize(cross(offsetDir, gWheelSide));
    float3 offsetStretched = offsetBase + 4 * gBlurLevel * dot(offsetBase, gStretchDir) * gStretchDir;

    PS_IN_PerPixel vout;
    if (vertexID < 6) {
      vout.PointingFade = saturate((1 - dot(offsetDir, normalize(ksCameraPosition.xyz - posW.xyz))) * lerp(100, 1000, B.z));
      posW.xyz += offsetStretched * spikeSize;
      vout.NormalW = lerp(offsetBase, offsetDir * 0.2, B.z);
      vout.Tex = float2(10, 0) + B.xy;
    } else {
      posW.xyz += offsetStretched * spikeSize;
      vout.NormalW = (offsetBase + (vertexID >= 9 ? offsetDir : 0));
      vout.Tex = BILLBOARD[vertexID].xy;
      vout.PointingFade = spikeHeight;
    }
    vout.PointingPoint = B.z;
    // vout.PointingShift = BILLBOARD[vertexID];

    float4 posV = mul(posW, ksView);
    posV.xyz *= max(0.5, 1 - 0.01 / length(posV.xyz));
    float4 posH = mul(posV, ksProjection);
    vout.PosC = posW.xyz - ksCameraPosition.xyz;
    vout.Ao = 1;
    vout.PosH = posH;
    vout.PointingNm = offsetDir;
    shadows(posW, SHADOWS_COORDS);

    OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));

    GENERIC_PIECE_STATIC(posW - float4(gWheelVelocity, 0));
    // posW = origPosW;
    // shadows(posW, SHADOWS_COORDS);
    SPS_RET(vout);
  #endif  
}