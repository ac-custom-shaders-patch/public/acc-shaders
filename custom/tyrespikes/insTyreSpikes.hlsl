#if !defined(MODE_MAIN_NOFX) && !defined(MODE_SHADOW)
	#define MODE_MAIN_FX
#endif

#ifdef MODE_SHADOW
  struct PS_IN {
    float4 PosH : SV_POSITION;
    float2 Tex : TEXCOORD;
  };
  #define PS_STRUCT PS_IN
#else

  #ifdef MODE_GBUFFER
    #define CREATE_MOTION_BUFFER
    // #define ZERO_GBUFFER
    // #define USE_ALPHATEST
    // #define STENCIL_VALUE 0
  #endif

  #define CUSTOM_STRUCT_FIELDS\
    float3 PointingNm : EXTRA0;\
    float PointingFade : EXTRA1;\
    float PointingPoint : EXTRA2;
  #define PS_STRUCT PS_IN_PerPixel
#endif

cbuffer _cbData : register(b10) {
  float3 gWheelPos;
  float gWheelRadius;

  float3 gWheelUp;
  float gCountAlong;

  float3 gWheelLook;
  float gCountAcross;

  float3 gWheelVelocity;
  float gTyreWidth;

  float extGroundOcclusion;
  float extNeutralReflectionOcclusion;
  float gBlurLevel;
  uint pWidth_Height;
}