add(`insTyreSpikes_vs.fx`, { saveAs: `insTyreSpikes_shadow_vs.fxo`, defines: { 'MODE_SHADOW': 1 } });
add(`insTyreSpikes_ps.fx`, { saveAs: `insTyreSpikes_ps_fs.fxo`, defines: { 'ALLOW_DYNAMIC_SHADOWS': 1 } });
add(`insTyreSpikes_vs.fx`, { saveAs: `insTyreSpikes_vs_gbuffer.fxo`, defines: { 'MODE_GBUFFER': 1 } });
add(`insTyreSpikes_ps.fx`, { saveAs: `insTyreSpikes_ps_gbuffer.fxo`, defines: { 'MODE_GBUFFER': 1 } });
add(`insTyreSpikes_ps.fx`, { saveAs: `insTyreSpikes_ps_gbuffer_refl.fxo`, defines: { 'MODE_GBUFFER': 1, 'CREATE_REFLECTION_BUFFER': 1 } });

