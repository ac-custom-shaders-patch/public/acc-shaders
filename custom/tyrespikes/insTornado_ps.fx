#define NO_CARPAINT
#define SUPPORTS_AO
#define INPUT_DIFFUSE_K 0.1
#define INPUT_AMBIENT_K 0.1
#define INPUT_SPECULAR_K 0.2
#define INPUT_SPECULAR_EXP 10
#define INPUT_EMISSIVE3 0
#define IS_TRACK_MATERIAL 0

#define CUSTOM_STRUCT_FIELDS\
  float3 Rand : EXTRA0;

#include "include/common.hlsl"
#include "include_new/base/_include_ps.fx"

Texture2D txSmokeNoise : register(t0);

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS

  {
    float3 withReflection = float3(pin.NormalW.xyz * 0.1);
    float alpha = 1;

    alpha = lerpInvSat(length(pin.Tex), 1, 0.5);

    float4 tex = txSmokeNoise.Sample(samLinearSimple, pin.Rand.xy + pin.Tex * 0.1);
    float3 nm = float3(pin.Tex.x, pin.Tex.y, 0.1) + tex.xyz;
    float3 toSide = normalize(cross(toCamera, float3(0, 1, 0)));
    float3 toUp = normalize(cross(toSide, toCamera));
    normalW = normalize(normalW * 0.5 + normalize(toSide * nm.x + toUp * nm.y - toCamera * nm.z));
    // float3 nm = tex.y;
    // withReflection = tex.x * 0.1;
    // withReflection = saturate(dot(normalW, -ksLightDirection.xyz)) * 0.1;
    pin.Fog *= 0.25 + 0.25 * saturate(dot(normalW, toCamera) + 1);
    withReflection = (AMBIENT_COLOR_REMOTE * (normalW.y * 0.75 + 0.25)
      + saturate(dot(normalW, -ksLightDirection.xyz) * 0.75 + 0.25) * ksLightColor.xyz) * float3(0.9, 0.95, 1);
    // withReflection = saturate(-ksLightDirection.y) * 0.1;
    // alpha *=
    alpha *= tex.w * pin.Rand.z;
    clip(alpha - 0.01);

    // alpha = 1;
    // withReflection = saturate(pin.NormalW.y);

    RETURN(withReflection, alpha);
  }
}