#include "insTyreSpikes.hlsl"

#define NO_CARPAINT
#define SUPPORTS_AO
#define INPUT_DIFFUSE_K 0.1
#define INPUT_AMBIENT_K 0.1
#define INPUT_SPECULAR_K 0.2
#define INPUT_SPECULAR_EXP 10
#define INPUT_EMISSIVE3 0
#define IS_TRACK_MATERIAL 0
#define DIM_REFLECTTIONS_WITH_SAMPLE_PARAM

#define blurLevel 0
#define ksAmbient 1
#define ksDiffuse 1

struct TyresMaterialParams {
  float aoMult;
  float rainOcclusion;
  float specularValue;
  float specularExp;
  float reflectivity;
  float wetMaskBlend; // how much alpha of txDiffuse is used as a mask for wetness
  float2 uv;
};

#include "include/common.hlsl"
#include "include_new/base/_include_ps.fx"
#include "include_new/ext_tyresfx/_include_ps.fx"

float sphereSoftOcc(float3 r0, float3 rd, float3 s0, float sr) {
  if (dot(rd, s0 - r0) < 0) return 1;
  float3 nearestPointToSphere = r0 + rd * dot(rd, s0 - r0);
  float distanceFromNearestToCenter = length(nearestPointToSphere - s0);
  return pow(lerpInvSat(distanceFromNearestToCenter / sr, 0.8, 0.95), 2);
}

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS

  {
    float3 withReflection = float3(frac(pin.Tex) * 0.05, 0);
    // float3 withReflection = normalW * 0.02;
    float alpha = 1;
    // RETURN(withReflection, alpha);
  }

  float alpha;
  if (pin.Tex.x <= 1.5) {
    // pin.Tex.x -= 10;
    float F = length(pin.Tex);
    // normalW = normalize(normalW * F + pin.PointingNm * (1 - F) * 0.1);
    // pin.PosC += pin.PointingNm * (pin.PointingFade * (1 - F));
    alpha = saturate((1 - dot2(pin.Tex)) * 10);
    // alpha = 1;
  } else {
    alpha = pin.PointingFade;
    if (pin.PointingPoint > 0.67) discard;
    // alpha *= lerpInvSat(pin.PointingPoint, 0.671, 0.67);

    // float progAlong = dot(normalW, pin.PointingNm);
    // pin.PointingShift.xy = pow(pin.PointingShift.xy, 3);
    // pin.PointingShift.xy = pow(abs(pin.PointingShift.xy), 2) * sign(pin.PointingShift.xy);
    // float progAlong = normalize(pin.PointingShift).z;
    // if (progAlong > 0.2 && progAlong < 0.3) {
    //   normalW = pin.PointingNm;
    // }
  }
  if (pin.PointingPoint) {
    alpha *= 1 - gBlurLevel;
  } else {
    normalW = normalize(normalW + pin.PointingNm * gBlurLevel);
    pin.PosC += gBlurLevel * 0.001;
    alpha *= 1 - gBlurLevel * abs(pin.Tex.x);
  }

  alpha -= dithering(pin.PosH.xy) * 100;

  // float specMult = sphereSoftOcc(ksCameraPosition, toCamera, gWheelPos, gWheelRadius);
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float specMult = sphereSoftOcc(ksInPositionWorld, reflect(toCamera, normalW), gWheelPos, gWheelRadius) * extraShadow.x;
  float4 txDiffuseValue = float4(0.15, 0.15, 0.15, 1);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.specularValue *= specMult;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  float groundOcc = lerp(1, sqrt(saturate(pin.PointingNm.y + 1)), extGroundOcclusion);
  lighting *= groundOcc;
  // RETURN(lighting, txDiffuseValue.a);

  ReflParams R = getReflParamsZero();
  R.fresnelEXP = 5;
  R.ksSpecularEXP = 10;
  R.finalMult = 1;
  R.metallicFix = 0;
  R.reflectionSampleParam = REFL_SAMPLE_PARAM_DEFAULT;
  R.coloredReflections = 0;
  R.reflectionSampleParam = (0.04 + 0.96 * specMult) * pow(groundOcc, 2) * reflectionOcclusion(pin.PosC, normalW, toCamera);
  R.fresnelMaxLevel = 1;
  R.fresnelC = 0.2; 

  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);

  // withReflection = saturate(pin.NormalW) * 0.01;
  // withReflection = extraShadow.x * 0.1;
  RETURN(withReflection, alpha);

  // // if (pin.Tex.x <= 1 && length(pin.Tex) > 1) discard;
  // outDepth = pin.PosH.z - 0;// + pow(pin.Tex.y + abs(pin.Tex.x), 2) * 0.005;
  // return float4(getReflectionAt(pin.NormalW, 1, 2, false, 0), alpha);
  // return float4(saturate(pin.NormalW) * 0.01, alpha);
  // return float4(1, 0, 0, alpha);
}