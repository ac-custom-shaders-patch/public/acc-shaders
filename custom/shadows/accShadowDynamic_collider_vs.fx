#include "accShadowDynamic.hlsl"

cbuffer cbData : register(b11) {
  float4x4 gWorld;
}

PS_IN main(VS_IN_collider vin) {
  float3 normalL = float3(
    BYTE0(asuint(vin.NormalL)) / 255.f,
    BYTE1(asuint(vin.NormalL)) / 255.f,
    BYTE2(asuint(vin.NormalL)) / 255.f) * 2 - 1;
  float3 normalW = mul(normalL, (float3x3)gWorld);

  float4 posW = mul(float4(vin.PosL, 1), gWorld);
  float bottomK = saturate(remap(vin.PosL.y, 0.3, 0.4, 1, 0));
  #ifdef HEADLIGHTS_FIX
    posW.y -= bottomK;
    bottomK = 0;
  #endif
  float3 toLight = normalize(gLightPos - posW.xyz);
  float bias = -saturate(-dot(normalW, toLight)) * bottomK * 0.3;
  return voutFill(posW.xyz, 0, bias);
}
