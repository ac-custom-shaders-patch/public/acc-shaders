struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD;
};

cbuffer cbData : register(b1) {
  float4x4 gTransform;
	float gOpacity;
}

float4 main(PS_IN pin) : SV_TARGET {
  float alpha = gOpacity;
  uint2 pos = uint2(pin.PosH.xy);
  uint2 xy3 = pos.xy % 3;
  if (alpha < 0.9 && xy3.x == 0 && xy3.y == 0
    || alpha < 0.8 && xy3.x == 2 && xy3.y == 1
    || alpha < 0.7 && xy3.x == 1 && xy3.y == 2
    || alpha < 0.6 && xy3.x == 1 && xy3.y == 0
    || alpha < 0.5 && xy3.x == 1 && xy3.y == 1
    || alpha < 0.4 && xy3.x == 0 && xy3.y == 2
    || alpha < 0.3 && xy3.x == 2 && xy3.y == 0
    || alpha < 0.2 && xy3.x == 0 && xy3.y == 1
    || alpha < 0.1) {
    discard;
  }
  
  return 0;
}

