#include "include/common.hlsl"

Texture2D<float> txPrev : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float4 gRnd[32];
  float2 gDir;
  float gSpeedMs;
}

float stretch2(float2 uv, float4 rnd) {
  float2 m = frac(rnd.yz / 20) - uv;
  if (m.x > 0.5) m.x -= 1;
  if (m.x < -0.5) m.x += 1;
  if (m.y > 0.5) m.y -= 1;
  if (m.y < -0.5) m.y += 1;
  float r = dot2(m) * 0.7;
  for (int x = -3; x <= 3; ++x){
    r = min(r, dot2(m + x * gDir / 12) * (0.7 + abs(x) * 0.07));
  }
  return r;
}

float boost(float2 uv, float4 rnd) {
  float d = saturate(1 - sqrt(stretch2(uv, rnd)) * 2 * lerp(3, 5, frac(496.411 * rnd.w)) / pow(0.5 - abs(0.5 - rnd.x), 0.5));
  // float4 r = 0;
  // if (gMix1 > 0) r += txNoise.SampleLevel(samLinear, uv + gRnd1.xy, 0) * gMix1;
  // if (gMix1 < 0) r += txNoise.SampleLevel(samLinear, uv + gRnd1.zw, 0) * -gMix1;
  // return d;
  return d * pow(0.5 - abs(0.5 - rnd.x), 2) * lerp(0.2, 0.4, frac(899.886 * rnd.w)) * 0.3;
  // return d * (1 - rnd.x);
}

float boost(float2 uv) {
  float r = 0;
  for (int i = 0; i < 32; ++i) r += boost(uv, gRnd[i]);
  // if (gMix1 > 0) r += txNoise.SampleLevel(samLinear, uv + gRnd1.xy, 0) * gMix1;
  // if (gMix1 < 0) r += txNoise.SampleLevel(samLinear, uv + gRnd1.zw, 0) * -gMix1;
  return r;
}

float sam(float2 uv, float mip) {
  float2 d = gDir / -20;
  return txPrev.SampleLevel(samLinear, uv + d, mip);
  return (txPrev.SampleLevel(samLinear, uv + d, mip)
    + txPrev.SampleLevel(samLinear, uv + d * 0.5, mip)
    + txPrev.SampleLevel(samLinear, uv + d * 1.5, mip)) / 3;
}

float main(VS_Copy pin) : SV_TARGET {
  return clamp(sam(pin.Tex, 0) * lerp(0.99, 0.97, saturate(gSpeedMs / 35)) + boost(pin.Tex), 0, 4);
}