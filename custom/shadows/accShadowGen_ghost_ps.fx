#include "include_new/base/cbuffers_common.fx"
#include "include/bayer.hlsl"

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD;
};

cbuffer cbShadowParams : register(b10) {
  float gAlpha;
  float3 gPad0;
}

float4 main(PS_IN pin) : SV_TARGET {
  if (gAlpha < -getBayer(pin.PosH)) discard;
  // uint2 pos = uint2(pin.PosH.xy);
  // uint2 xy2 = pos.xy % 2;
  // if (dot(xy2, 1)) discard;
  return 0;
}
