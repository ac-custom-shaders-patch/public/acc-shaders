#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct PS_IN {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD1;
};

float4 toScreenSpaceAlt(float4 posL){
  float4 posW = mul(posL, ksWorld);
  APPLY_CFX(posW.xyz);
  float4 posH = mul(posW, ksMVPInverse);
  if (posH.z < 0) posH.z = 0;
  return posH;
} 

PS_IN main(VS_IN_ac vin) {
  PS_IN vout;
  vout.PosH = toScreenSpaceAlt(vin.PosL);
  vout.NormalW = mul(vin.NormalL, (float3x3)ksWorld);
  return vout;
}

