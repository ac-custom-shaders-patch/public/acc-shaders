#include "include/common.hlsl"

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct VS_IN_collider {
  float3 PosL : POSITION;
  uint NormalL : NORMAL_ENCODED;
};

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD1;
  float4 PosC_Bias : TEXCOORD2;
};

cbuffer cbData : register(b10) {
  float4x4 gTransform;
  float3 gLightPos;
  float gLightRangeInv;
  float gLightClip;
  float3 gSceneOrigin;
}

#ifdef TARGET_VS
  #include "include_new/base/_include_vs.fx"

  float4 toScreenSpaceAlt(float4 posL){
    return mul(posL, gTransform);
  } 

  PS_IN voutFill(float3 posW, float2 tex, float bias = 0){
    PS_IN vout;
    vout.PosH = toScreenSpaceAlt(float4(posW, 1));
    vout.Tex = tex;
    vout.PosC_Bias = float4(posW - gLightPos, bias);
    return vout;
  }
#endif