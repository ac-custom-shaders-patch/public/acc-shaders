#define INCLUDE_FLAGS_CB

#include "accShadowDynamic.hlsl"
#include "flagsFX.hlsl"

PS_IN main(VS_IN_ac vin) {
  PS_IN vout;

  float flagWidth = length(vin.NormalL) - 1000.0;
  if (abs(flagWidth) > 20) flagWidth = 0;
  #ifdef FLAG_TENT_MODE
    customWaveTent(vin.PosL.xyz, vin.NormalL, vin.TangentPacked.y, vsLoadAo0(vin.TangentPacked));
  #else
    float2 aoHeight = FFX_loadVector(asuint(vin.TangentPacked.x));
    float3 dir;
    customWave(vin.Tex, vin.PosL.xyz, vin.NormalL, dir, float2(flagWidth, aoHeight.y), vin.TangentPacked.y, vin.TangentPacked.z,
      extWindVel, extWindSpeed, extWindWave);
  #endif
  return voutFill(vin.PosL.xyz, vin.Tex);
}

