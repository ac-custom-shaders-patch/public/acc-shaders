#include "include_new/base/cbuffers_common.fx"

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD;
};

float4 main(PS_IN pin) : SV_TARGET {
  uint2 pos = uint2(pin.PosH.xy);
  uint2 xy2 = pos.xy % 2;
  if (dot(xy2, 1)) discard;
  return 0;
}
