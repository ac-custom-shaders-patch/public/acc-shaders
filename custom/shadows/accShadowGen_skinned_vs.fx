#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"

struct VS_IN_skinned {
  AC_INPUT_SKINNED_ELEMENTS
};

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD;
};

float4 toScreenSpaceAlt(float4 posL, float4 boneWeights, float4 boneIndices){
  float4 posW = 0;
  float totalWeight = 0;
  for (int i = 0; i < 4; i++){
    float weight = boneWeights[i];
    if (SKINNED_WEIGHT_CHECK(weight)){
      uint index = (uint)boneIndices[i];
      float4x4 bone = bones[index];
      posW += mul(bone, posL) * weight;
      totalWeight += weight;
    }
  }

  posW /= totalWeight;
  posW.xyz += ksWorld[3].xyz;
  posW.w = 1;

  // posW.y += 0.001;
  APPLY_CFX(posW.xyz);
  float4 posV = mul(posW, ksView);
  return mul(posV, ksProjection);
} 

PS_IN main(VS_IN_skinned vin) {
  PS_IN vout;
  vout.PosH = toScreenSpaceAlt(vin.PosL, vin.BoneWeights, vin.BoneIndices);
  vout.Tex = vin.Tex;
  return vout;
}

