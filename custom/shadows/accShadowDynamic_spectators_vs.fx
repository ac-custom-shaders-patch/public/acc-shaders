#define INCLUDE_FLAGS_CB
#define OBJECT_SHADER

#include "accShadowDynamic.hlsl"
#include "flagsFX.hlsl"

PS_IN main(VS_IN_ac vin) {
  PS_IN vout;

  float4 posW = vin.PosL;
  posW.xyz += extSceneOffset;

  float3 posC = posW.xyz - ksCameraPosition.xyz;
  #ifdef MODE_SHADOWS_ADVANCED
    float3 dirSide = normalize(cross(ksLightDirection.xyz, float3(0, 1, 0)));
    posW.xyz += ksLightDirection.xyz * abs(vin.NormalL.x) * (vin.NormalL.y > 0 ? 1 : 0.1);
  #else
    float3 dirSide = normalize(cross(posW.xyz - gLightPos.xyz, float3(0, 1, 0)));
  #endif

  // float distSquared = dot2(posC) * 2 / extCameraTangent;
  // if (distSquared > vin.TangentPacked.x / 4){
  //   DISCARD_VERTEX(PS_IN);
  // }

  float3 toCamera = vin.PosL.xyz - ksCameraPosition.xyz;
  posW.y += vin.NormalL.y;
  posW.xz += dirSide.xz * vin.NormalL.x;
  vin.PosL.xyz = posW.xyz;

  return voutFill(vin.PosL.xyz, vin.Tex);
}

