struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD;
};

#define OBJECT_SHADER
#include "include_new/base/_include_ps.fx"
#include "include/common.hlsl"
#include "include/bayer.hlsl"

float4 main(PS_IN pin) : SV_TARGET {
  float alpha = saturate((txDiffuse.Sample(samLinearSimple, pin.Tex).a - ksAlphaRef) / max(1 - ksAlphaRef, 0.01));
  float bayerValue = getBayer(pin.PosH);
  clip(alpha + bayerValue); 

  // uint2 pos = uint2(pin.PosH.xy);
  // uint2 xy3 = pos.xy % 3;
  // if (alpha < 0.9 && xy3.x == 0 && xy3.y == 0
  //   || alpha < 0.8 && xy3.x == 2 && xy3.y == 1
  //   || alpha < 0.7 && xy3.x == 1 && xy3.y == 2
  //   || alpha < 0.6 && xy3.x == 1 && xy3.y == 0
  //   || alpha < 0.5 && xy3.x == 1 && xy3.y == 1
  //   || alpha < 0.4 && xy3.x == 0 && xy3.y == 2
  //   || alpha < 0.3 && xy3.x == 2 && xy3.y == 0
  //   || alpha < 0.2 && xy3.x == 0 && xy3.y == 1
  //   || alpha < 0.1) {
  //   discard;
  // }
  
  return 0;
}

