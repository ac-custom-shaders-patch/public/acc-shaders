struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD1;
  float4 PosC_Bias : TEXCOORD2;
};

cbuffer cbData : register(b10) {
  float4x4 gTransform;
  float3 gLightPos;
  float gLightRangeInv;
  float gLightClip;
  float3 gSceneOrigin;
}

#define OBJECT_SHADER
#include "include_new/base/_include_ps.fx"
#include "include/common.hlsl"

float main(PS_IN pin) : SV_TARGET {
  float alpha = txDiffuse.Sample(samLinearSimple, pin.Tex).a;
  clip(min(alpha - 0.5, length(pin.PosC_Bias.xyz) - gLightClip));
  return (length(pin.PosC_Bias.xyz) + pin.PosC_Bias.w) * gLightRangeInv;
}

