struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD;
};

#define OBJECT_SHADER
#include "include_new/base/_include_ps.fx"
#include "include/common.hlsl"

float4 main(PS_IN pin) : SV_TARGET {
  float alpha = txDiffuse.Sample(samLinearSimple, pin.Tex).a;
  if (alpha <= ksAlphaRef) discard;
  return 0;
}

