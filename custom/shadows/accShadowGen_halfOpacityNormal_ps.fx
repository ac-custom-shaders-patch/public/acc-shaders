#include "include_new/base/cbuffers_common.fx"
#include "include/bayer.hlsl"

struct PS_IN {
  float4 PosH : SV_POSITION;
  float3 NormalW : TEXCOORD1;
};

cbuffer cbShadowParams : register(b5) {
  float alphaFacing;
  float alphaEdge;
  float2 sPad0;
}

float4 main(PS_IN pin) : SV_TARGET {
  float alpha = lerp(alphaEdge, alphaFacing, pow(abs(dot(pin.NormalW, ksLightDirection.xyz)), 2));
  float bayerValue = getBayer(pin.PosH);
  clip(alpha + bayerValue); 
  // uint2 pos = uint2(pin.PosH.xy);
  // uint2 xy3 = pos.xy % 3;
  // if (alpha < 0.9 && xy3.x == 0 && xy3.y == 0
  //   || alpha < 0.8 && xy3.x == 2 && xy3.y == 1
  //   || alpha < 0.7 && xy3.x == 1 && xy3.y == 2
  //   || alpha < 0.6 && xy3.x == 1 && xy3.y == 0
  //   || alpha < 0.5 && xy3.x == 1 && xy3.y == 1
  //   || alpha < 0.4 && xy3.x == 0 && xy3.y == 2
  //   || alpha < 0.3 && xy3.x == 2 && xy3.y == 0
  //   || alpha < 0.2 && xy3.x == 0 && xy3.y == 1
  //   || alpha < 0.1) {
  //   discard;
  // }  
  return 0;
}
