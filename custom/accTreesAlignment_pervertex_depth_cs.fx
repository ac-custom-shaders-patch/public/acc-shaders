#include "include/samplers.hlsl"

cbuffer cbData : register(b10) {
  float4x4 gTransform;
  float4x4 gTransformInv;
}

Texture2D<float4> txNormals : register(t0);
ByteAddressBuffer inVertices : register(t1);
Texture2D<float> txDepth : register(t2);
RWTexture2D<float2> rwOutput : register(u0);
RWTexture2D<float> rwDepthOutput : register(u1);

[numthreads(256, 1, 1)]
void main(uint3 threadID : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint3 Gid : SV_GroupID) {
  float4 v = asfloat(inVertices.Load4(threadID.x * 16));
  float4 texCoord = mul(float4(v.xyz, 1), gTransform);
  float4 texValue = txNormals.SampleLevel(samPointClamp, texCoord.xy, 0);
  if (all(texCoord.xy >= 0 && texCoord.xy <= 1) && texValue.a) {
    rwOutput[uint2(GTid.x, Gid.x)] = normalize(texValue.xyz * 2 - 1).xz * 0.5 + 0.5;

    float4 depth = mul(float4(texCoord.xy, txDepth.SampleLevel(samPointClamp, texCoord.xy, 0), 1), gTransformInv);
    rwDepthOutput[uint2(GTid.x, Gid.x)] = depth.y / depth.w;
    // rwDepthOutput[uint2(GTid.x, Gid.x)] = txDepth.SampleLevel(samLinearClamp, texCoord.xy, 0) * 10000;
    // rwDepthOutput[uint2(GTid.x, Gid.x)] = texCoord.x * 1000000;
  // } else if (all(texCoord.xy >= 0 && texCoord.xy <= 1)) {
  //   rwDepthOutput[uint2(GTid.x, Gid.x)] = txDepth.SampleLevel(samPointClamp, texCoord.xy, 0);
  }
}

