#include "include/common.hlsl"

Texture2D txPrevColor : register(t0);
Texture2D<float> txPrevDepth : register(t1);

cbuffer cbCamera : register(b10) {
  float4x4 gVP;
  float4x4 gVPInv;
  float4x4 gVPPrev;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float d = txPrevDepth.SampleLevel(samLinearSimple, pin.Tex, 0);
  float4 h = float4(pin.Tex, d, 1);
  float4 w = mul(h, gVPInv);
  w /= w.w;
  // return float4(frac(w.xyz / 10), 1);
  float4 uv = mul(w, gVPPrev);
  uv /= uv.w;
  return txPrevColor.SampleLevel(samLinearClamp, uv.xy, 0);
  // return txDiffuse.SampleLevel(samPoint, pin.Tex, 0);
  // return txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);// + dithering(pin.PosH.xy) * 0;
}