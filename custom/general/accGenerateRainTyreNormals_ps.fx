#include "include/common.hlsl"
#include "include/generate_rain_tyres.hlsl"

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#include "include/poisson.hlsl"

float calculateAvgDepth(float2 uv, float radius, float2 offset = 0){
  float d = 0;
  #define DISK_SIZE 16
  [loop] for (uint i = 0; i < DISK_SIZE; ++i){
    d += calculateDepth(uv + (offset + SAMPLE_POISSON(DISK_SIZE, i) * radius) * float2(RATIO, 1));
  }  
  return d / DISK_SIZE;
}

float calculateAreaDepth(float2 uv, float radius, float2 offset = 0){
  float d = 0;
  #define AREA_DISK_SIZE 32
  [loop] for (uint i = 0; i < AREA_DISK_SIZE; ++i){
    d += calculateDepth(uv + (offset + SAMPLE_POISSON(AREA_DISK_SIZE, i) * radius) * float2(RATIO, 1));
  }  
  return d / AREA_DISK_SIZE;
}

float4 main(VS_Copy pin) : SV_TARGET {
  pin.Tex += offset;
  float2 points[5] = {
    float2(-shapeOffset, 0),
    float2(shapeOffset, 0),
    float2(0, -shapeOffset),
    float2(0, shapeOffset),
    float2(0, 0)
  };

  float a[5] = {0,0,0,0,0};
  [loop] for (int i = 0; i < 5; ++i){
    a[i] = calculateAvgDepth(pin.Tex, shapeBlur, points[i]);
  }

  float g = calculateAreaDepth(pin.Tex, 0.02);
  float ao = lerp(0.8, 1, a[4]) * (1 + pow(a[4], 2) - g) * (1 + a[4] - (a[0] + a[1] + a[2] + a[3]) / 4);

  float2 nm = float2(a[0] - a[1], a[2] - a[3]) * 0.5;
  return float4(0.5 - nm * float2(1, -1), sqrt(saturate(1 - dot2(nm))), ao);
}
