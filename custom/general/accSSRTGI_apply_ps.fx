// #define MIPMAPPED_GBUFFER 4
#include "accSS.hlsl"

Texture2D txPreviousNormal : register(TX_SLOT_SS_ARG0);
Texture2D<float3> txPreviousColor : register(TX_SLOT_SS_ARG1);
TextureCube txCube : register(TX_SLOT_REFLECTION_CUBEMAP);

float4 main(VS_Copy pin) : SV_TARGET {
  float4 sceneColor = txColor.SampleLevel(samLinearSimple, pin.Tex, 0);
  float3 prevColor = txPreviousColor.SampleLevel(samLinearSimple, pin.Tex, 0);
  float4 prevNormal = txPreviousNormal.SampleLevel(samLinearSimple, pin.Tex, 0);
  if (prevNormal.a == 0) return sceneColor;
  // return sceneColor;
  // return float4(prevColor, 1);
  return float4(sceneColor.xyz * lerp(0.2, 1, prevNormal.a) + prevColor * 0.5, 1);

  float3 normal = normalize(prevNormal.xyz);
  // normal = ssGetNormal(pin.Tex);

  return txCube.SampleLevel(samLinearSimple, normal, 6);
  return txCube.SampleLevel(samLinearSimple, normal, 6) * prevNormal.a;
}