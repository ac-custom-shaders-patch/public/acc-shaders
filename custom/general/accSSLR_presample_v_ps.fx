#include "accSSLR_flags.hlsl"
#include "accSS_cbuffer.hlsl"
#include "include_new/base/samplers_ps.fx"
#include "include/common.hlsl"
#include "include/bayer.hlsl"

Texture2D txTraced : register(TX_SLOT_SS_ARG4);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float w = 0;
  float4 q = 0;
  [unroll]
  for (int i = -8; i <= 7; ++i){
    float4 b = txTraced.SampleLevel(samPointClamp, pin.Tex, 0, int2(0, i));
    float t = saturate(b.w * 1000) / (abs((float)i) + 1.);
    // float t = saturate(b.w * 1000);
    if (t > w) { q = b; w = t; }
    // if (b.w > q.w) q = b;
  }
  return q;
}