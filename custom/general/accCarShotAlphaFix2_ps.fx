#include "include/common.hlsl"

Texture2D<float> txDepth : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  if (txDepth.SampleLevel(samLinearSimple, pin.Tex, 0) != 1) {
    return float4(0, 0, 0, 1);
  }
  return 0;
}