#include "include_new/base/_include_vs.fx"

#define BX 0.38
#define BY 0.92

static const float2 BILLBOARD[] = {
	float2(-BX, BY), float2(BY, BX), float2(BX, BY),
	float2(-BX, BY), float2(-BY, BX), float2(BY, BX),
	float2(-BY, BX), float2(BY, -BX), float2(BY, BX),
	float2(-BY, BX), float2(-BY, -BX), float2(BY, -BX),
	float2(-BY, -BX), float2(-BX, -BY), float2(BY, -BX),
	float2(-BX, -BY), float2(BX, -BY), float2(BY, -BX),
};

cbuffer cbData : register(b10) {
  float4x4 gWheelTransform;
  float gRimRadius;
  float gRimWidth;
  float gRimOffset;
  float gAngleStep;
}

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

float4 projRimPoint(float2 rimPoint){
  float3 pl = float3(gRimOffset + gRimWidth / 2, rimPoint * gRimRadius * 1.03);
	float4 posW = mul(float4(pl, 1), gWheelTransform);
	float4 posV = mul(posW, ksView_base); // TODO: VSS, combine matrices
  return mul(posV, ksProjection_base);
}

PS_IN main(uint fakeIndex : SV_VERTEXID) {
	uint vertexID = fakeIndex % 18;
	uint instanceID = fakeIndex / 18;
	float2 B = BILLBOARD[vertexID];
	PS_IN vout;
	vout.PosH = projRimPoint(B);
  vout.Tex = B;
  return vout;
}