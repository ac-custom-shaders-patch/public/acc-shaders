#ifndef PB_TYPE
  #define PB_TYPE float
#endif

Texture2D<PB_TYPE> txPreviousMap : register(t0);

cbuffer cbData : register(b10){
  float gRadius;
  float gMult;
  float2 gPad0;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#include "include/common.hlsl"
#include "include/poisson.hlsl"

PB_TYPE main(VS_Copy pin) : SV_TARGET {
  PB_TYPE ret = 0;
  float tot = 0;
  #define PUDDLES_DISK_SIZE 16
  [unroll]
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * gRadius;
    float2 sampleUV = pin.Tex + offset;
    PB_TYPE v = txPreviousMap.SampleLevel(samLinearClamp, sampleUV, 0);
    float w = 1;
    ret += v * w;
    tot += w;
  }
  ret /= tot;   
  return ret * gMult;
}
