cbuffer cbData : register(b10) {
  float2 gDir;
  float gWithAlpha;
}

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 result = txDiffuse.Load(int3(pin.PosH.xy, 0));
  float4 orig = result;

  if (dot(result, 1) > 0.001) return result;
  // return result; 

  float totalW = 0.001;

  // [unroll]
  for (float x = -8; x <= 8; x++){
    if (x == 0) continue;
    float4 read = txDiffuse.Load(int3(pin.PosH.xy + gDir * x, 0));
    float w = dot(read, 1) > 0 ? 1 / abs(x) : 0;
    result += read * w;
    totalW += w;
  }

  result /= totalW;
  if (!gWithAlpha) result.w = orig.w;
  return result;
}