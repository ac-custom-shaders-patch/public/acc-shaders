#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

cbuffer cbData : register(b10) {
  float gAspectRatio;
  float gAspectRatioInv;
  float gBaseAspectRatio;
  float gFlippedScale;
  float gMult;
  float gProgress;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#define BAR_START float2(0.4, 0.95)
#define BAR_END float2(0.6, 0.955)

float2 mapBar(float2 uv){
  return remap(uv, BAR_START, BAR_END, 0, 1);
}

float4 prepareColor(float4 color){
  return color / color.w;
}

float4 sampleColor(float2 uv){
  [branch]
  if (gFlippedScale){
    float2 uvAdj = uv.yx * 2 - 1;
    uvAdj.y *= -1;
    uvAdj *= float2(1 / gBaseAspectRatio, gBaseAspectRatio) * gFlippedScale;
    return prepareColor(txDiffuse.SampleLevel(samLinear, uvAdj * 0.5 + 0.5, 0)) /* * gMult */;
  } else {
    return prepareColor(txDiffuse.SampleLevel(samLinear, uv, 0)) /* * gMult */;
  }
}

float4 calculateBackground(float2 uv){
  if (gAspectRatio){
    float over = gAspectRatio < 1 ? abs(uv.x * 2 - 1) - gAspectRatio : abs(uv.y * 2 - 1) - gAspectRatioInv;
    if (over > 0) return 0.068 * lerp(0.61, 1, saturate(over * 20));
  }

  // clip(-1);
  return sampleColor(uv);
}

float4 main(VS_Copy pin) : SV_TARGET {
  float2 pos = mapBar(pin.Tex);
  if (all(saturate(pos) == pos)){
    float3 col = pos.x > gProgress ? 0.2 : float3(0, 1, 1) * lerp(1, 0.7, pos.y);
    return float4(col, 1);
  }
  
  // float2 posRel = max(abs(mapBar(pin.Tex - float2(0, 0.003)) * 2 - 1) - 0.93, 0) * (BAR_END - BAR_START);
  // float shadow = lerp(0.8, 1, saturate(length(posRel) * 30));

  return calculateBackground(pin.Tex);
}