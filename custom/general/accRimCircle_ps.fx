#include "include_new/base/_include_ps.fx"

cbuffer cbData : register(b10) {
  float4x4 gWheelTransform;
  float gRimRadius;
  float gRimWidth;
  float gRimOffset;
  float gAngleStep;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

float4 projRimPoint(float2 rimPoint){
  float3 pl = float3(gRimOffset + gRimWidth / 2, rimPoint * gRimRadius * 1.03);
	float4 posW = mul(float4(pl, 1), gWheelTransform);
	float4 posV = mul(posW, ksView_base); // TODO: VSS, combine matrices
  return mul(posV, ksProjection_base);
}

float2 projToUV(float4 posH){
  return posH.xy / posH.w * float2(1, -1) * 0.5 + 0.5;
}

float4 main(VS_Copy pin) : SV_TARGET {
  // float2 ssUV = pin.PosH.xy * extScreenSize.zw;
  // float2 pivot = projToUV(projRimPoint(local));

  clip(0.9 - length(pin.Tex));

  // float gAngleStep = 0.5;

  float4 ret = 0;
  float tw = 0.001;
  int st = 2;
  for (int i = 0; i < st; ++i){
    float2 local = rotate2d(pin.Tex, gAngleStep * i / st / 2.1);
    float2 uvN = projToUV(projRimPoint(local));
    float4 s = txDiffuse.SampleLevel(samLinearSimple, uvN, 0);
    float w = 1;
    ret += s * w;
    tw += w;
  }

  // ret /= tw;
  ret /= st;
  return ret;
}