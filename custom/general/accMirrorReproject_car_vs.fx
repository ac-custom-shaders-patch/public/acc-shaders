#include "include/common.hlsl"
#include "include_new/base/_include_vs.fx"

cbuffer cbData : register(b10) {
  float4x4 gWorld;
  float4x4 gWorldPrev;
  float4x4 gVPPrev;
}

struct VS_IN_collider {
  float3 PosL : POSITION;
  uint NormalL : NORMAL_ENCODED;
};

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float2 AbsTex : TEXCOORD1;
};

PS_IN main(VS_IN_collider vin) {
  float3 normalL = normalize(float3(
    BYTE0(asuint(vin.NormalL)) / 255.f,
    BYTE1(asuint(vin.NormalL)) / 255.f,
    BYTE2(asuint(vin.NormalL)) / 255.f) * 2 - 1);

  PS_IN vout;
  float4 posW = mul(float4(vin.PosL + normalL * 0, 1), gWorld);
  float4 posV = mul(posW, ksView);
  vout.PosH = mul(posV, ksProjection);

  float4 posWPrev = mul(float4(vin.PosL, 1), gWorldPrev);
  float4 posHPrev = mul(posWPrev, gVPPrev);
  vout.Tex = posHPrev.xy / posHPrev.w;
  vout.AbsTex = vout.PosH.xy / vout.PosH.w * float2(1, -1) * 0.5 + 0.5;
  return vout;
}
