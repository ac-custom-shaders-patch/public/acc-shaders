#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float4 gRegion;
  int4 gPixelRegion;
}

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  int3 p = int3(pin.PosH.xy, 0);
  p.xy = clamp(p.xy, gPixelRegion.xy, gPixelRegion.zw);
  return txDiffuse.Load(p);
}