#include "include/common.hlsl"
#include "include/samplers.hlsl"

TextureCube txSource : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float mipLevel;
}

float4 calculate(float3 vec){
  return txSource.SampleLevel(samLinearSimple, vec, mipLevel);
}

struct PS_OUT {  
  float4 rt0 : SV_Target;
  float4 rt1 : SV_Target1;
  float4 rt2 : SV_Target2;
  float4 rt3 : SV_Target3;
  float4 rt4 : SV_Target4;
  float4 rt5 : SV_Target5;
};

PS_OUT main(VS_Copy pin) {
  float3 vec = float3(1 - pin.Tex * 2, -1);
  PS_OUT ret;
  ret.rt0 = calculate(vec.zyx * float3(-1, 1, 1));
  ret.rt1 = calculate(vec.zyx * float3(1, 1, -1));
  ret.rt2 = calculate(vec.xzy * float3(-1, -1, -1));
  ret.rt3 = calculate(vec.xzy * float3(-1, 1, 1));
  ret.rt4 = calculate(vec * float3(-1, 1, -1));
  ret.rt5 = calculate(vec);
  return ret;
}