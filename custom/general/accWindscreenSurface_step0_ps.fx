#include "include/common.hlsl"

Texture2D txNormal : register(t0);
Texture2D<float> txDepth : register(t1);

cbuffer _cbExtWindscreenReprojection : register(b10) {
  float4x4 gWorldToCar;
  float3 gDir;
  float _pad;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float3 nm = normalize(txNormal.SampleLevel(samLinearSimple, pin.Tex, 0).xyz * 2 - 1);
  nm = mul(nm, (float3x3)gWorldToCar);
  if (nm.y > 0) nm = -nm;
  float depth = txDepth.SampleLevel(samLinearSimple, pin.Tex, 0);
  nm += gDir * pow(1 - dot(nm, gDir), 4);
  if (depth == 1) return 0;
  return float4(normalize(nm), depth);
}