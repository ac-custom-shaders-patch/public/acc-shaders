#include "accAltTonemapping.hlsl"

cbuffer cbData : register(b1) {
  float W;
  float3 _pad;
}

// float3 applyTonemap(float3 color) {
// 	color = max(0, color - 0.004);
// 	color = (color * (6.2 * color + .5)) / (color * (6.2 * color + 1.7) + 0.06);
//   color = pow(color, 2.8);
// 	return color;
// }

// shoulder strength
const static float A = 0.22;
// linear strength
const static float B = 0.3;
// linear angle
const static float C = 0.1;
// toe strength
const static float D = 0.20;
// toe numerator
const static float E = 0.01;
// toe denominator
const static float F = 0.30;

float filmic_curve(float x) {
	return ((x*(0.22*x+0.1*0.3)+0.2*0.01)/(x*(0.22*x+0.3)+0.2*0.3))-0.01/0.3;
}
// ((1.6*x*(0.22*1.6*x+0.1*0.3)+0.2*0.01)/(1.6*x*(0.22*1.6*x+0.3)+0.2*0.3))-0.01/0.3

float inverse_filmic_curve(float x) {
    float q = B*(F*(C-x) - E);
    float d = A*(F*(x - 1.0) + E);
    return (q -sqrt(q*q - 4.0*D*F*F*x*d)) / (2.0*d);
}
float3 filmic(float3 x) {
    float w = filmic_curve(W);
    return float3(
        filmic_curve(x.r),
        filmic_curve(x.g),
        filmic_curve(x.b)) / w;
}
float3 inverse_filmic(float3 x) {
    x *= filmic_curve(W);
    return float3(
        inverse_filmic_curve(x.r),
        inverse_filmic_curve(x.g),
        inverse_filmic_curve(x.b));
}

float filmic_reinhard2(float x) {
    x *= 1.32;
    float k = 23.0;
	return (exp(-x*k) - 1.0)/k - 1.0/(x + 1.0) + 1.0;
}

float3 filmic_reinhard2(float3 x) {
    float w = filmic_reinhard2(W);
    return float3(
        filmic_reinhard2(x.r),
        filmic_reinhard2(x.g),
        filmic_reinhard2(x.b)) / w;
}

float3 applyTonemap(float3 color) {
	return filmic(color);
	// return filmic_reinhard2(color);
}
