#include "include/common.hlsl"

cbuffer cbCamera : register(b0) {
  float4x4 ksView;
  float4x4 ksProjection;
  float4x4 ksMVPInverse;
  float4 ksCameraPosition;
  float ksNearPlane;
  float ksFarPlane;
  float ksFOV;
  float ksDofFactor;
}

float linearizeAccurate(float depth){
  return 2 * ksNearPlane * ksFarPlane / (ksFarPlane + ksNearPlane - (2 * depth - 1) * (ksFarPlane - ksNearPlane));
}

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
  noperspective float OutputLinear : TEXCOORD1;
};

float main(VS_Copy pin) : SV_TARGET {
  float depth = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);
  float depthLinear = linearizeAccurate(depth);
  if (pin.OutputLinear) return depth == 1 ? 6.5e4 : depthLinear * 0.1;
  return depth == 1 ? 1 : saturate(remap(depthLinear, 300, 600, 0, 1));
}