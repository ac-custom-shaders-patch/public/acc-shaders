#include "include/common.hlsl"

Texture2D txPrevColor : register(t0);
Texture2D<float> txPrevDepth : register(t1);
Texture2D txPrevTrackOnly : register(t2);

cbuffer cbCamera : register(b10) {
  float4x4 gVP;
  float4x4 gVPInv;
  float4x4 gVPPrev;
  float4x4 gCarReprojection;
  float4 gRegion;
  float2 gUVMargin;
  float gDepthThreshold;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
  noperspective float2 AbsTex : TEXCOORD1;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float d = txPrevDepth.SampleLevel(samPointClamp, pin.Tex, 0);
  // return pow(d, 100);
  // if (true) discard;

  // float gDepthThreshold = 0.99;
  // if (d < gDepthThreshold) return float4(0, 3, 0, 1); 
  // if (d < gDepthThreshold) discard; 

  float4 h = float4(pin.AbsTex, d, 1);
  float4 w = mul(h, gVPInv);
  w /= w.w;

  float4 wStatic = w;
  if (d < gDepthThreshold){
    w = mul(w, gCarReprojection);
    w /= w.w;
    // discard;
  }

  // return float4(frac(w.xyz / 10), 1);
  float4 uv = mul(w, gVPPrev);
  uv /= uv.w;

  float4 uvStatic = mul(wStatic, gVPPrev);
  uvStatic /= uvStatic.w;

  // if (any(uv > 1 || uv < 0)) discard; 
  float2 uv2 = gRegion.xy + gRegion.zw * clamp(uv.xy, gUVMargin, 1 - gUVMargin);
  float2 uv2Static = gRegion.xy + gRegion.zw * clamp(uvStatic.xy, gUVMargin, 1 - gUVMargin);
  float d2 = txPrevDepth.SampleLevel(samPointClamp, uv2, 0);
  // if (d2 < gDepthThreshold) return float4(0, 0, 3, 1); 
  // if (d2 < gDepthThreshold) discard;
  if (d2 < gDepthThreshold && d >= gDepthThreshold) discard;

  return txPrevColor.SampleLevel(samPointClamp, uv2, 0);
}