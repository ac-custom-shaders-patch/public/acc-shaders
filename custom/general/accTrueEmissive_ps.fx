#include "include/common.hlsl"
#include "accTrueEmissive.hlsl"

#define MIPMAPPED_GBUFFER 2
#define MIPMAPPED_GBUFFER_FIXED
#include "general/accSS.hlsl"
#include "include_new/base/cbuffers_common.fx"

float smin(float a, float b){
  // return min(a, b);
  float h = saturate(b - abs(a - b));
  return min(a, b) - h * h / max(0.00001, b * 3) * (1 - b);
}

float4 main(VS_Glw pin) : SV_TARGET {
  float2 ssUV = pin.PosH.xy * gSize.zw;
  float3 origin = ssGetPos(ssUV, ssGetDepth(ssUV));
  float3 normal = ssGetNormal(ssUV);

  float maxB = 1 - dot2(pin.Tex);
  clip(maxB);

  // return float4(pin.Color * 2.5, 1);
  float3 toLight = origin - pin.PosC;
  float lambertAdj = saturate(dot(normalize(-toLight), normal));
  lambertAdj = GAMMA_OR(lambertAdj, sqrt(lambertAdj));
  float alpha = 1
    * pow(saturate(1 - length(toLight) * pin.RadiusInv), GAMMA_OR(4, 2)) 
    // * lambertAdj / (1 + 0.5 * lambertAdj)
    * smin(0.5, lambertAdj)
    * lerp(1, smoothstep(0, 1, saturate(dot(normalize(toLight), pin.Normal) * 2)), gDirected)
    ;

  alpha = min(alpha, pow(maxB, 2));
  
  // alpha = 1;
  // pin.Color = GAMMA_LINEAR(1);

  // clip(pin.PosH.x - 1920/2);
  
      // float3 random = normalize(txNoise.SampleLevel(samPoint, (pin.PosH.xy + 0) / 32 + 0.5, 0).xyz);
      // for (int i = 1; i < 10; ++i){
      //   float3 pos = lerp(origin, pin.PosC + (random.yyz - 0.5) * 0.03, ((float)i + random.x) / 10 * 0.7);
      //   float3 uv = ssGetUv(pos);
      //   float actualDepth = ssGetDepth(uv.xy, true);
      //   if (actualDepth < uv.z - 0.0002){
      //     alpha = 0; 
      //   }
      // }

  // [branch]
  // if (dot2(gPos - origin) > 100){
  //   return 0;
  // }

  // clip(pin.)

  // return float4(10, 0, 0, 1);
  // return float4(gDirected, 1 - gDirected, 0, 1);
  // pin.Color.b = 1;
  // return float4(pin.Color * 2.5, 1);
  // return float4(pin.Color * 0.1, 1);
  // return float4(normal, 1);
  // return float4(pin.Color * 0.001, 1);
  return float4(pin.Color * alpha * GAMMA_SCREENLIGHT_MULT, 1);
}