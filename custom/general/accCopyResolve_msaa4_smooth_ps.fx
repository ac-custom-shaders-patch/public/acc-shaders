#include "include/common.hlsl"
#include "include_new/base/cbuffers_common.fx"

Texture2DMS<float4> txDiffuse : register(t0); 

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

static const float2 SubSampleOffsets[4] = {
    float2(-0.125f, -0.375f),
    float2( 0.375f, -0.125f),
    float2(-0.375f,  0.125f),
    float2( 0.125f,  0.375f),
};

float filterSmoothstep(float x){ return 1 - smoothstep(0, 1, x); }

float4 main(VS_Copy pin) : SV_TARGET {
  float4 resValue = 0;
  
  const float gResolveFilterDiameter = 3;
  const int gSampleRadius = (int)((gResolveFilterDiameter / 2.0f) + 0.499f);

  float4 sum = 0;
  const bool InverseLuminanceFiltering = true;
  for (int y = -gSampleRadius; y <= gSampleRadius; ++y) {
    for (int x = -gSampleRadius; x <= gSampleRadius; ++x) {
      float2 samplePos = pin.PosH.xy + float2(x, y);
      [unroll]
      for(uint subSampleIdx = 0; subSampleIdx < 4; ++subSampleIdx) {
        float2 sampleDist = abs(float2(x, y) + SubSampleOffsets[subSampleIdx].xy) / (gResolveFilterDiameter / 2.0f);
        bool useSample = all(sampleDist <= 1);
        if (useSample) {
          float3 color = max(0, txDiffuse.Load(uint2(samplePos), subSampleIdx).xyz);
          float weight = filterSmoothstep(sampleDist.x) * filterSmoothstep(sampleDist.y);
          sum += float4(color, 1) * weight;
        }
      }
    }
  }

  // For color shadowing only: applies gamma correction here
  return float4(max(0, GAMMA_LINEAR(sum.rgb / sum.a)), 1);
}