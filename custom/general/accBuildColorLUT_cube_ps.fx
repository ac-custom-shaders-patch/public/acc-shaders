#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

cbuffer cbData : register(b10) {
  float gZ;
  float gResolution;
  float gMult;
  float gStrength;
  uint2 gSrcSize;
  uint gResolutionU;
  float gResolutionInv;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 neutralColor = float4(pin.Tex, gZ * gResolutionInv, 1);
  uint3 coord = uint3(neutralColor.rgb * gResolution);
  coord.x += coord.z * gResolutionU;
  coord.y += (coord.x / gSrcSize.x) * gResolutionU;
  coord.x %= gSrcSize.x;
  float4 lutColor = txDiffuse.Load(int3(coord.xy, 0));
  return lerp(neutralColor, lutColor, gStrength) * gMult;
}