#include "accIBL.hlsl"
#include "include/poisson.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  float3 normal = normalize(mul(float3(1 - pin.Tex * 2, -1), (float3x3)transform));
  float3 o1 = normalize(cross(normal, float3(0, 1, 0)));
  float3 o2 = normalize(cross(normal, o1));

  float4 avg = 0;
  for (uint i = 0; i < CUBEMAP_BLUR_SIZE; ++i) {
    float4 dvec = CUBEMAP_BLUR[i];
    float3 vecF = normal * dvec.z + o1 * (dvec.x * gBlurRadius) + o2 * (dvec.y * gBlurRadius);
    float4 value = max(0, txFirst.SampleLevel(samLinearSimple, vecF, (float)mipIndex));
    // tweakSample(value);
    float weight = gUseLuminanceWeight ? 1 / (100 + luminance(value.rgb)) : 1;
    if (GAMMA_ACTUAL_VALUE) {
      weight = gammaSampleWeight(value);
    }
    weight *= dvec.w;
    avg += float4(value.rgb, 1) * weight;
  }
  avg /= avg.w;
  // if (!gUseLuminanceWeight) avg.r = 0; 
  // tweakFinal(avg);
  return avg;
}