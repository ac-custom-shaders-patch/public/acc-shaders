#include "include/samplers.hlsl"
#include "include_new/base/cbuffers_common.fx"
#include "include_new/base/cbuffers_ps.fx"
#include "include_new/ext_functions/depth_map.fx"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR;
  float2 Tex : TEXCOORD;
  float3 TangentH : TANGENT;
};

cbuffer _cbData : register(b10) {
  float gOpacity;
}
                                                                                                                                                                                                               
float4 main(VS_Copy pin) : SV_TARGET {
  float depth = linearizeAccurate(txDepth.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw, 0));
  float4 ret = txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0);
  ret = pin.Color * ret;
  float4 xray = float4(ret.rgb * float3(0.5, 0.8, 1), ret.w * saturate(0.2 + fwidth(dot(ret.rgb, 1))) * gOpacity);

  float relative = linearizeAccurate(pin.PosH.z) - depth;
  if (relative > 0){
    ret = lerp(ret, xray, smoothstep(0, 1, saturate(relative * 100)));
  }

  ret.rgb = GAMMA_KSEMISSIVE(ret.rgb);
  ret.a = GAMMA_ALPHA(ret.a);

  // ret.rgb = txDepth.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw, 0);
  // ret.w = 1;

  clip(ret.w - 0.001);
  return ret;
}