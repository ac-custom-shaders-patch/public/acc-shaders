#include "accIBL.hlsl"
#include "include/poisson.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  float3 normal = normalize(mul(float3(1 - pin.Tex * 2, -1), (float3x3)transform));
  float3 o1 = normalize(cross(normal, float3(0, 1, 0)));
  float3 o2 = normalize(cross(normal, o1));

  float4 avg = 0;
  #define PUDDLES_DISK_SIZE 12
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * gBlurRadius;
    float4 value = max(0, txFirst.SampleLevel(samLinearSimple, normal + o1 * offset.x + o2 * offset.y, (float)mipIndex));
    tweakSample(value);
    float weight = gUseLuminanceWeight ? 1 / (100 + luminance(value.rgb)) : 1;
    if (GAMMA_ACTUAL_VALUE) {
      weight = gammaSampleWeight(value);
    }
    avg += float4(value.rgb, 1) * weight;
  }
  avg /= avg.w;
  tweakFinal(avg);
  if (mipIndex == 0) {
    avg = applyColorCorrection(avg);
  }
  return avg;
}