SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2DMS<float> txDiffuse : register(t0); 

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float main(VS_Copy pin) : SV_DEPTH {
  return txDiffuse.Load(pin.PosH.xy, 0);
}