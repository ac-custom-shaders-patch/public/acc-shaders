#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

cbuffer cbData : register(b10) {
  float4 gRegion;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  pin.Tex = pin.Tex * gRegion.zw + gRegion.xy;
  pin.Tex.y = 1 - pin.Tex.y;
  return txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0);
}