#include "accSS.hlsl"

Texture2D<float2> txMotion : register(TX_SLOT_MOTION);

float cone(float T, float l_V){
  return saturate(1.0 - T / l_V);
}

float cylinder(float T, float l_V){
  return 1.0 - smoothstep(0.95 * l_V, 1.05 * l_V, T);
}

float sdc(float a, float b, float t){
  return saturate(1 - (b - a) / t);
}

#define JITTER 4
#define VEL_MULT motionBlurMult
#define MAX_VEL 0.02

#ifndef BLUR_STEPS
  #define BLUR_STEPS 20
#endif

float2 fixVel(float2 vel){  
  if (length(vel) > MAX_VEL) vel *= MAX_VEL / length(vel);
  return vel;
}

float4 main(VS_Copy pin) : SV_TARGET {
  // if (!all(abs(txMotion.SampleLevel(samPoint, pin.Tex, 0).xy) < 1000)) return float4(1, 0, 0, 1);
  float4 base = txColor.SampleLevel(samPoint, pin.Tex, 0);
  float2 v_c = fixVel(txMotion.SampleLevel(samPoint, pin.Tex, 0).xy * VEL_MULT);
  return float4(v_c * 100 + 0.5, saturate(fwidth(base.g) * 2) * 0.2, 1);
}