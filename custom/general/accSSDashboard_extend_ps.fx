#include "accSS_cbuffer.hlsl"
#include "accSSDashboard.hlsl"

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

Texture2D txInput : register(TX_SLOT_SS_ARG0);

#define R 2
float4 main(VS_Copy pin) : SV_TARGET {
  float4 totalValue = 0;
  float totalWeight = 0;

  float4 maxValue = 0;
  float maxWeight = 0;

  float maxR = length(float2(R, R));

  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    
    float4 val = txInput.SampleLevel(samPointClamp, pin.Tex, 0, int2(x, y));
    float cR = length(float2(x, y));
    float weight = saturate(dot(abs(val), 1e4)) / (1 + cR);

    totalWeight += weight;
    totalValue += val * weight;

    if (weight > maxWeight){
      maxWeight = weight;
      maxValue = val;
    }
  }

  // return totalValue / totalWeight;
  // return txInput.SampleLevel(samPointClamp, pin.Tex, 0);
  return maxValue;
}
