cbuffer cbData : register(b1) {
  float2 xy;
  float2 padding;
}

SamplerState samPoint : register(s10) {
  Filter = POINT;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float max_value(float base, float3 color){
  return max(base, max(color.x, max(color.y, color.z)));
}

float4 main(VS_Copy pin) : SV_TARGET {
  float3 result;
  float max_brightness = 0;
  for (float x = -3; x < 3.001; x++)
  for (float y = -3; y < 3.001; y++){
    float3 read = txDiffuse.SampleLevel(samPoint, pin.Tex + xy * float2(x, y), 0).xyz;
    float brightness = dot(read, float3(0.299, 0.587, 0.114));
    if (brightness > max_brightness){
      max_brightness = brightness;
      result = read;
    }
    // max_brightness = max_value(max_brightness, read);
    // result += read;
  }
  return float4(result, 1.0);
  // return float4(max_brightness * result / max_value(1, result), 1.0);
}