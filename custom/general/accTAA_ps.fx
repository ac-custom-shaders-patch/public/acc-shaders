#include "include/common.hlsl"

#define SS_VIEWLESS
#include "accSS_cbuffer.hlsl"
#include "include_new/base/_gamma.fx"

#ifdef TRIPLE_FSR_AWARE
  cbuffer cbTFAData : register(b11){
    float2 gTFAVelocityMult;
  }

  #define VELOCITY_MULT gTFAVelocityMult
#else
  #define VELOCITY_MULT 1
#endif

Texture2D txNow : register(TX_SLOT_COLOR);
Texture2D<float2> txMotion : register(TX_SLOT_MOTION);
Texture2D txHistory : register(TX_SLOT_SS_ARG0);
Texture2D<float2> txMotionOld : register(TX_SLOT_SS_ARG1);
Texture2D<float> txStencil : register(TX_SLOT_SS_ARG2);
Texture2D<float> txStencilOld : register(TX_SLOT_SS_ARG3);
Texture2D<float> txClamping : register(TX_SLOT_SS_ARG4);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float3 tonemap(float3 x){ return x / (x + 1); }
float4 tonemap(float4 x){ return float4(tonemap(x.rgb), x.a); }
// float3 inverseTonemap(float3 x){ return x > 0.9999 ? 0 : x / max(1 - x, 0.00001); } /* black sun bug, why is there a condition? */
float3 inverseTonemap(float3 x){ return x / max(1 - x, 0.00001); }
inline bool isSaturated(float a) { return a == saturate(a); }
inline bool isSaturated(float2 a) { return isSaturated(a.x) && isSaturated(a.y); }

#define MINMAX_ROUNDED

// From "Temporal Reprojection Anti-Aliasing"
// https://github.com/playdeadgames/temporal
float3 clipAABB(float3 aabbMin, float3 aabbMax, float3 prevSample, float3 avg) {
	float3 p_clip = 0.5 * (aabbMax + aabbMin);
	float3 e_clip = 0.5 * (aabbMax - aabbMin);

	float3 v_clip = prevSample - p_clip;
	float3 v_unit = v_clip.rgb / e_clip;
	float3 a_unit = abs(v_unit);
	float ma_unit = max(a_unit.x, max(a_unit.y, a_unit.z));

	if (ma_unit > 1.0) {
		return p_clip + v_clip / ma_unit;
	} else {
		return prevSample; // point inside aabb
	}
}

bool edgeTest(float value, float cmp){
	return value > 0.0039 && value != cmp;
}

bool isEdge(float2 uv, float stencilCenter/* , inout bool isToBeBlurred */){
	// bool ret = false;
	// [unroll] for (int x = -1; x <= 1; x++){
	// 	[unroll] for (int y = -1; y <= 1; y++){
	// 		if (x == 0 && y == 0) continue;
	// 		if (abs(x) + abs(y) == 2) continue;
	// 		float s = txStencil.SampleLevel(samPointClamp, uv, 0, int2(x, y));
	// 		ret = s != stencilCenter || ret;
	// 		// isToBeBlurred = isToBeBlurred || abs(s - 0.48) < 0.005;
	// 	}
	// }
	// return ret;

	// return txStencil.SampleLevel(samLinearClamp, uv + gSize.zw * float2(1.2, 1.2), 0) != stencilCenter
	// 	|| txStencil.SampleLevel(samLinearClamp, uv + gSize.zw * float2(-1.2, 1.2), 0) != stencilCenter
	// 	|| txStencil.SampleLevel(samLinearClamp, uv + gSize.zw * float2(-1.2, -1.2), 0) != stencilCenter
	// 	|| txStencil.SampleLevel(samLinearClamp, uv + gSize.zw * float2(1.2, -1.2), 0) != stencilCenter;

	if (stencilCenter == 0) return false;
	return edgeTest(txStencil.SampleLevel(samLinearClamp, uv + gSize.zw * float2(1.2, 1.2), 0), stencilCenter)
		|| edgeTest(txStencil.SampleLevel(samLinearClamp, uv + gSize.zw * float2(-1.2, 1.2), 0), stencilCenter)
		|| edgeTest(txStencil.SampleLevel(samLinearClamp, uv + gSize.zw * float2(-1.2, -1.2), 0), stencilCenter)
		|| edgeTest(txStencil.SampleLevel(samLinearClamp, uv + gSize.zw * float2(1.2, -1.2), 0), stencilCenter);
}

float rand(float2 co){
  return frac(sin(dot(co.xy, float2(12.9898, 78.233))) * 43758.5453);
}

float3 rgbToYcc(float3 c) {
	return float3(c.x/4.0 + c.y/2.0 + c.z/4.0, c.x/2.0 - c.z/2.0, -c.x/4.0 + c.y/2.0 - c.z/4.0);
}

float3 yccToRgb(float3 c) {
	return saturate(float3(c.x + c.y - c.z, c.x + c.z, c.x - c.y - c.z));
}

static float kernel[9] = {
 0.0625, 0.125, 0.0625,
 0.125, 0.25, 0.125,
 0.0625, 0.125, 0.0625
};

float4 sampleHistory(float2 uv){
  #ifdef SHARPEN_HISTORY
    float4 base = txHistory.SampleLevel(samLinearClamp, uv, 0);
    float4 blurred = 0
      + txHistory.SampleLevel(samLinearClamp, uv + gSize.zw * float2(0.5, 0.5), 0)
      + txHistory.SampleLevel(samLinearClamp, uv + gSize.zw * float2(0.5, -0.5), 0)
      + txHistory.SampleLevel(samLinearClamp, uv + gSize.zw * float2(-0.5, 0.5), 0)
      + txHistory.SampleLevel(samLinearClamp, uv + gSize.zw * float2(-0.5, -0.5), 0);
    float4 delta = base - blurred / 4;
    return base + (0.36 * saturate(dot(float4(delta.xyz, 0.5), 1)) - 0.18) * (gTAAHistorySharpen * GAMMA_OR(0.3, 1));
  #else
		return txHistory.SampleLevel(samPointClamp, uv, 0);
  #endif
}

#ifdef FLICKER_FIX
	struct PS_OUT {
    float4 color : SV_Target;
    float clamping : SV_Target1;
	};

	PS_OUT main(VS_Copy pin) {
#else
	float4 main(VS_Copy pin) : SV_TARGET {
#endif

  float2 velocity = txMotion.Load(int3(pin.PosH.xy, 0)) * VELOCITY_MULT;
	float currentSpeed = length(velocity);
  float2 oldUv = pin.Tex - velocity;

	// Loading stencil

	float stencil = txStencil.SampleLevel(samPointClamp, pin.Tex, 0);
	float stencilOld = txStencilOld.SampleLevel(samPointClamp, oldUv, 0);

	// float stencilL = txStencil.SampleLevel(samLinearClamp, pin.Tex, 0);
	// float stencilLOld = txStencilOld.SampleLevel(samLinearClamp, oldUv, 0);

	// bool blurMe = abs(stencil - 0.48) < 0.005;
	bool edge = isEdge(pin.Tex, stencil/* , blurMe */);

	// bool forceExtraAA = abs(stencil - 0.5); // old version, most likely a mistake
	// bool forceExtraAA = abs(stencil - 0.5) < 0.01; // nothing seems to use it though, so:
	bool forceExtraAA = false;

	// Loading current color
	float4 nbh[9];
	nbh[0] = tonemap(txNow.Load(int3(pin.PosH.xy, 0), int2(-1, -1)));
	nbh[1] = tonemap(txNow.Load(int3(pin.PosH.xy, 0), int2(0, -1)));
	nbh[2] = tonemap(txNow.Load(int3(pin.PosH.xy, 0), int2(1, -1)));
	nbh[3] = tonemap(txNow.Load(int3(pin.PosH.xy, 0), int2(-1, 0)));
	nbh[4] = tonemap(txNow.Load(int3(pin.PosH.xy, 0), int2(0, 0)));
	nbh[5] = tonemap(txNow.Load(int3(pin.PosH.xy, 0), int2(1, 0)));
	nbh[6] = tonemap(txNow.Load(int3(pin.PosH.xy, 0), int2(-1, 1)));
	nbh[7] = tonemap(txNow.Load(int3(pin.PosH.xy, 0), int2(0, 1)));
	nbh[8] = tonemap(txNow.Load(int3(pin.PosH.xy, 0), int2(1, 1)));
	float4 nbhMin = nbh[0];
	float4 nbhMax = nbh[0];
	float4 nbhAvg = 0;
	float4 nbhBlur = 0;

	// return stencil;
	// return txNow.Load(int3(pin.PosH.xy, 0), int2(0, 0));
	
	{
		[unroll]
		for (uint i = 1; i < 9; i++) {
			float4 n = nbh[i];
			nbhMin = min(nbhMin, n);
			nbhMax = max(nbhMax, n);
		}
	}

	{
		[unroll]
		for (uint i = 0; i < 9; i++) {
			float4 n = nbh[i];
			nbhAvg += float4(n.rgb, 1);
			nbhBlur += n * kernel[i];
		}
		nbhAvg /= nbhAvg.w;
	}

	// [branch]
	// if (blurMe){
	// 	#ifdef FLICKER_FIX
	// 		PS_OUT ret;
	// 		ret.color = nbhBlur;
	// 		ret.clamping = 0;
	// 		return ret;
	// 	#else
	// 		return nbhBlur;
	// 	#endif
	// }

	#ifdef MINMAX_ROUNDED
		float4 cmin5 = min(nbh[1], min(nbh[3], min(nbh[4], min(nbh[5], nbh[7]))));
		float4 cmax5 = max(nbh[1], max(nbh[3], max(nbh[4], max(nbh[5], nbh[7]))));
		float4 cavg5 = (nbh[1] + nbh[3] + nbh[4] + nbh[5] + nbh[7]) / 5.0;
		nbhMin = 0.5 * (nbhMin + cmin5);
		nbhMax = 0.5 * (nbhMax + cmax5);
		nbhAvg = 0.5 * (nbhAvg + cavg5);
	#endif

	// Can’t avoid the linear filter here because point sampling could sample irrelevant pixels:
	// float4 history = txHistory.SampleLevel(samLinearClamp, oldUv, 0);
	float4 history = sampleHistory(oldUv);
	history.rgb = tonemap(history.rgb);

	float4 current = nbh[4];

	if (!(dot(history.rgb, 1) > -1e9 && dot(history.rgb, 1) < 1e9)){
		// TODO: Figure out how NaN appears in here
		history.rgb = nbhBlur.rgb;
	}

	// Clamping history
	float3 historyClamped = clamp(history.rgb, nbhMin.rgb, nbhMax.rgb);
	// float clampingBase = forceExtraAA < 0.002 ? 0.1 : stencil > 0.02 ? 0.5 : 1;
	// float clampingBase = max(stencil, 0.5); // stencil above 0.5 is for reduced TAA, with 1 the most reduced
	float clampingBase = stencil == 2./255 ? 0.5 : 1;
	history.rgb = lerp(history.rgb, historyClamped, clampingBase);

	// Simple adjustment for different velocities:
	float oldSpeed = length(txMotionOld.Load(int3(pin.PosH.xy, 0)));
  float blendK = saturate(1.2 - abs(currentSpeed - oldSpeed) * 100);
	// return float4(abs(velocity) * 1000, 1, 1);

	// The linear filtering can cause blurry image, try to account for that:
	float subpixelCorrection = frac(max(abs(velocity.x) * gSize.x, abs(velocity.y) * gSize.y)) * 0.5;

	// Compute a nice blend factor:
	float blendFactor = saturate(lerp(0.05, 0.8, subpixelCorrection));

	// Stencil value >0.5 is for reduced blending rate:
	blendFactor = max(blendFactor, 0.25 * saturate(stencil * 2 - 1));
	if (stencil == (int)(0.055 * 255) / 255.) blendFactor = 1;

	// Stencil-based anti-ghosting:
	float nextAlpha = stencil != stencilOld && min(stencil, stencilOld) > 0.0001 ? 1 : stencil == 0 ? 0.5 : 0;

	// Motion-based anti-ghosting to fix shadows nearby:
	// float motionBasedAntiGhosting = 1;
	float antiGhostingK = 0;
	// if (!edge) 
	{
		antiGhostingK = saturate(2 - length(velocity) * 200);
		// motionBasedAntiGhosting *= antiGhostingK;
		blendK *= antiGhostingK;
		// current = lerp((nbhBlur + current) / 2, current, antiGhostingK); // TODO: re-enable?
	}

	// If information can not be found on the screen, revert to aliased image:
	blendFactor = isSaturated(oldUv) ? blendFactor : 1.0;

	// if (forceExtraAA){
	// 	current.rgb = lerp(nbhBlur.rgb, current.rgb, blendK * (1 - blendFactor));
	// }

	// Do the temporal super sampling by linearly accumulating previous samples with the current one:
	float4 resolved = lerp(current, history, blendK * (1 - blendFactor));

	if (!edge && (stencil != stencilOld || history.a > 0.2)){
		resolved.rgb = lerp(current.rgb, nbhBlur.rgb, gBlurNew);
		// resolved.rgb /= 2;
	}

	// if (edge) resolved = history;

	// resolved = current;
	// resolved = lerp(current, history, 0.99);

	resolved.rgb = inverseTonemap(resolved.rgb);
	resolved.a = nextAlpha;

	// resolved = lerp(txNow.Load(int3(pin.PosH.xy, 0), int2(0, 0)), txHistory.SampleLevel(samPointClamp, oldUv, 0), 0.98);
	
	#ifdef VIEW_STENCIL
		// return stencil > 0.004;
		return stencil == 0 ? 0 : float4(normalize(float2(
			rand(float2(stencil, stencil + 0.2)), 
			rand(float2(stencil, stencil + 0.4)))) * 1,
			// frac(stencil * 5343.4335) * 1,
			saturate(stencil * 2 - 1) * 3,
			1);
	#endif

	// resolved.rg += velocity * 400;
	// resolved.r = resolved.a * 4;
	// resolved.g = edge;
	// resolved.rgb = edge;
	// resolved.r = stencil;
	// resolved.r = antiGhostingK;
	// resolved.rgb = frac(stencil * 728.532);

	#ifdef FLICKER_FIX
		PS_OUT ret;
		ret.color = resolved;
		ret.clamping = clamping;
		return ret;
	#else
		return resolved;
	#endif
}