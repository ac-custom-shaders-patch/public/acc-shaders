#include "accSS_cbuffer.hlsl"

#ifdef FIRST_STEP
  Texture2D<float> txDiffuse : register(TX_SLOT_COLOR);
#else
  Texture2D<float2> txDiffuse : register(TX_SLOT_COLOR);
#endif

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float2 main(VS_Copy pin) : SV_TARGET {
  float2 r = float2(1, 0);
  [unroll]
  for (int i = -8; i <= 7; i++){
    #ifdef FIRST_STEP
      float2 v = txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 1, OFFSET(i));
    #else
      float2 v = txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0, OFFSET(i));
    #endif
    r.x = min(r.x, v.x);
    r.y = max(r.y, v.y);
  }
  return r;
}