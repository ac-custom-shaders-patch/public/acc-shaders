#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
};

float4 main(VS_Copy pin) : SV_TARGET {
  return float4(0, 0, 0, 1);
}