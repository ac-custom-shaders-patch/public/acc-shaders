#include "include/common.hlsl"
#include "include_new/base/cbuffers_common.fx"

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbTraces : register(b6) {
  float3 xColor;
  float xThickness;
  float xNarrowingK;
  float xLifespanMultInv;
  float xFovK;
  float xPaddingK;
}

#include "include/poisson.hlsl"
float4 main(VS_Copy pin) : SV_TARGET {
  POISSON_AVG(float, result, txDiffuse, samLinearClamp, pin.Tex, 0.001, 8);
  return float4(GAMMA_KSEMISSIVE(result * xColor) * getEmissiveMult(), 1);
}
