#include "include/common.hlsl"
#include "include/samplers.hlsl"

TextureCube<float> txFirst : register(t0);
Texture2D txNoise : register(TX_SLOT_NOISE);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

const static float4x4 transformFirst[6] = {
  float4x4(0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1),
  float4x4(0, 0, -1, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 1),
  float4x4(-1, 0, 0, 0, 0, 0, -1, 0, 0, 1, 0, 0, 0, 0, 0, 1),
  float4x4(-1, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1),
  float4x4(-1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
  float4x4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1)
};

cbuffer cbData : register(b10) {
  float screenSize;
  float pixelSize;
}

#include "include/poisson.hlsl"
float sampleCubePoisson(float3 p){
  float radius = pixelSize;
  #define PUDDLES_DISK_SIZE 6
  float2 avg = float2(0, 0.25 * PUDDLES_DISK_SIZE);
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * radius;
    avg += txFirst.SampleLevel(samLinearSimple, p + float3(offset, offset.x), 0);
  }
  // return avg.x / PUDDLES_DISK_SIZE;
  return avg.x / avg.y;
}

float calculate(float3 vec, float4x4 tFirst){
  float3 vecFirst = mul(vec, (float3x3)tFirst);
  return sampleCubePoisson(vecFirst);
}

struct PS_OUT {  
  float rt0 : SV_Target;
  float rt1 : SV_Target1;
  float rt2 : SV_Target2;
  float rt3 : SV_Target3;
  float rt4 : SV_Target4;
  float rt5 : SV_Target5;
};

PS_OUT main(VS_Copy pin) {
  float3 vec = float3(1 - pin.Tex * 2, 1);
  PS_OUT ret;
  ret.rt0 = calculate(vec, transformFirst[0]);
  ret.rt1 = calculate(vec, transformFirst[1]);
  ret.rt2 = calculate(vec, transformFirst[2]);
  ret.rt3 = calculate(vec, transformFirst[3]);
  ret.rt4 = calculate(vec, transformFirst[4]);
  ret.rt5 = calculate(vec, transformFirst[5]);
  return ret;
}