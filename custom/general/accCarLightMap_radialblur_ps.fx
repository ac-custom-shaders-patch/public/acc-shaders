#include "include/common.hlsl"
#include "include/bayer.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float bayer = getBayer(pin.PosH);
  float4 ret = 0;
  for (int i = 0; i < 10; ++i){
    float4 r = txDiffuse.SampleLevel(samLinearSimple, lerp(0.5, pin.Tex, lerp(1, (i + bayer) / 10., 0.2)), 0);
    ret += r * r.w;
  }
  return ret / 10 * lerpInvSat(length(pin.Tex * 2 - 1), 1, 0.8);
}
