#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define CARPAINT_AT
// #define A2C_SHARPENED
// #define GETNORMALW_UV_MULT normalUVMultiplier // Not used, apparently?
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#define SUPPORTS_DITHER_FADING
#define CB_MATERIAL_EXTRA_3\
  float extUseDiffuseAlpha;

#define USE_TXDETAIL
#define USE_ALPHA_FROM_NORMAL_MAP
#include "accTrueEmissive_vs.fx"