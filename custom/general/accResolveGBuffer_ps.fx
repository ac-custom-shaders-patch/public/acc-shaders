#include "accSS_cbuffer.hlsl"

Texture2DMS<float> txDepth : register(TX_SLOT_COLOR); 
Texture2DMS<float4> txNormals : register(TX_SLOT_NORMALS); 

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

struct PS_Out {
  float depth : SV_TARGET0;
  float4 normals : SV_TARGET1;
};

PS_Out main(VS_Copy pin) {
  float retDepth = 1;
  float4 retNormals = 0;
  for (uint i = 0; i < sampleCount; ++i){
    float loaded = txDepth.Load(pin.PosH.xy, i).r;
    [flatten]
    if (loaded < retDepth){
      retDepth = loaded;
      retNormals = txNormals.Load(pin.PosH.xy, i);
    }
  }

  PS_Out result;
  result.depth = retDepth; 
  result.normals = retNormals; 
  return result;
}