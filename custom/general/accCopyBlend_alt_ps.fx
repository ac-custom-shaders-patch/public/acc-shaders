#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float gModalActive;
  float gFade;
}

Texture2D txUITop : register(t0);
Texture2D txUIBase : register(t1);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 uiColor = txUIBase.Sample(samLinearSimple, pin.Tex);
  uiColor.rgb /= max(0.0000001, uiColor.a);
  uiColor.a = lerp(uiColor.a, sqrt(uiColor.a), saturate(uiColor.a * 4 - 0.1));

  [branch]
  if (gModalActive){
    uiColor = lerp(uiColor, float4(0.2, 0.2, 0.2, 1), gModalActive * float4(0.8, 0.8, 0.8, 0.8));
  }

  float4 uiOverColor = txUITop.Sample(samLinearSimple, pin.Tex);
  uiOverColor.rgb /= max(0.0000001, uiOverColor.a);
  // uiOverColor.a = sqrt(uiOverColor.a);
  uiOverColor.a = lerp(uiOverColor.a, sqrt(uiOverColor.a), saturate(uiOverColor.a * 4 - 0.1));
  uiColor.rgb = uiColor.rgb * (1 - uiOverColor.a) + uiOverColor.rgb;
  uiColor.a = 1 - (1 - uiColor.a) * (1 - uiOverColor.a);
  uiColor.a = lerp(uiColor.a, 1, gFade);

  return uiColor;
}