#include "include/common.hlsl"

Texture2D txInput : register(t0);
Texture2D txPrevious : register(t1);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbExtraViewData : register(b10) { 
  float4x4 gCameraToTex; // turns coordinates relative to camera position to UV coordinates
  float4x4 gPreviousCameraToTexSmooth;
  float4x4 gTexToCameraSmooth;
  float gZoom;
  float gFade;
}

#define ZOOM 0.9
#define FADE 0.98

float4 main(VS_Copy pin) : SV_TARGET {
  // return float4(txInput.SampleLevel(samLinearSimple, pin.Tex, 0).bgr, 1);

  // float depthValue = txDepth.SampleLevel(samPointClamp, pin.Tex, 0);
  // float linearDepth = linearizeDepth(depthValue);
  // float t = delinearizeDepth(1.3);
  // if (depthValue < t) return txInput.SampleLevel(samPointClamp, pin.Tex, 0);
  float depthValue = 1;

  pin.Tex = (pin.Tex.xy * 2 - 1) * gZoom * 0.5 + 0.5;

  // float2 texBase = (pin.Tex.xy - gRegion.xy) / gRegion.zw;
  float2 texBase = pin.Tex.xy;
  float4 posW = mul(float4(texBase, depthValue, 1), gTexToCameraSmooth);
  posW.xyz /= posW.w; 

  float4 oldUV = mul(posW, gCameraToTex);
  if (oldUV.w > 0) oldUV /= oldUV.w;
  else oldUV = -1; 

  float4 prevUV = mul(posW, gPreviousCameraToTexSmooth);
  if (prevUV.w > 0) prevUV /= prevUV.w;
  else prevUV = -1;

  prevUV.xy = (prevUV.xy * 2 - 1) / gZoom * 0.5 + 0.5;

  [branch]
  if (all(oldUV.xy > 0 && oldUV.xy < 1)){
    return float4(txInput.SampleLevel(samLinearClamp, oldUV.xy, 0).rgb, 1);
  } else {
    return txPrevious.SampleLevel(samLinearClamp, prevUV.xy, 0) * gFade;
  }
}