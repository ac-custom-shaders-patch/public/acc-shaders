#include "include/common.hlsl"

Texture2DMS<float> txDepth : register(t0); 

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  uint sampleCount;
  float3 _pad0;
}

float main(VS_Copy pin) : SV_TARGET {
  float retDepth = 0;
  // float retDepth = 1;
  for (uint i = 0; i < sampleCount; ++i){
    float loaded = txDepth.Load(pin.PosH.xy, i).r;
    retDepth = max(retDepth, loaded);
    // retDepth = min(retDepth, loaded);
    // retDepth += loaded; // used for soft particles, so averaging seems like the best option
  }
  // return retDepth / (float)sampleCount;
  return retDepth;// / (float)sampleCount;
}