#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

cbuffer cbData : register(b10) {
  uint testPoint; // 16
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  return dot(1, txDiffuse.Load(int3(0, 0, 0)).rgb) < 0.4
    && dot(1, txDiffuse.Load(int3(0, testPoint - 1, 0)).rgb) > 0.4
    && dot(1, txDiffuse.Load(int3(0, testPoint, 0)).rgb) < 0.4;
}