#include "accAltTonemapping.hlsl"

cbuffer cbData : register(b1) {
  float W;
  float3 _pad;
}

float applyTonemap(float x) {
  // Lottes 2016, "Advanced Techniques and Optimization of HDR Color Pipelines"
  const float a = 1.6;
  const float d = 0.977;
  const float hdrMax = max(0, W) + 1;
  const float midIn = 0.18;
  const float midOut = 0.267;

  // Can be precomputed
  const float b =
      (-pow(midIn, a) + pow(hdrMax, a) * midOut) /
      ((pow(hdrMax, a * d) - pow(midIn, a * d)) * midOut);
  const float c =
      (pow(hdrMax, a * d) * pow(midIn, a) - pow(hdrMax, a) * pow(midIn, a * d) * midOut) /
      ((pow(hdrMax, a * d) - pow(midIn, a * d)) * midOut);

  return pow(x, a) / (pow(x, a * d) * b + c);
}

float3 applyTonemap(float3 color){
  return float3(applyTonemap(color.x), applyTonemap(color.y), applyTonemap(color.z));  
}

