#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

cbuffer cbData : register(b10) {
  float4 gTex;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  // return txDiffuse.SampleLevel(samPoint, pin.Tex, 0);
  return txDiffuse.Sample(samLinear, gTex.xy + gTex.zw * pin.Tex);// + dithering(pin.PosH.xy) * 0;
}