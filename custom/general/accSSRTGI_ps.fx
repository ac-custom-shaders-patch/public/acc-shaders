// #define MIPMAPPED_GBUFFER 4
#include "accSS.hlsl"

Texture2D<float2> txMotion : register(TX_SLOT_MOTION);
Texture2D txPreviousNormal : register(TX_SLOT_SS_ARG0);
Texture2D<float3> txPreviousColor : register(TX_SLOT_SS_ARG1);
TextureCube txCube : register(TX_SLOT_REFLECTION_CUBEMAP);

struct RESULT {
  float4 normal : SV_TARGET0;
  float3 color : SV_TARGET1;
};

RESULT main(VS_Copy pin) {
  float depth = ssGetDepth(pin.Tex);
  float intensity = 1 - linearize(depth) / 300;
  if (depth == 1 || intensity <= 0){
    return (RESULT)0;
  }

  float3 origin = ssGetPos(pin.Tex, depth);
  float3 normal = ssGetNormal(pin.Tex);

  float3 random = normalize(txNoise.SampleLevel(samPoint, (pin.PosH.xy + gFrameNum) / 32 + gRandom, 0).xyz);
  float3 randomAlt = frac(random * 28723.3731);
  float radiusMin = 0.05;
  float radiusMax = 0.8;
  float3 giValue = 0;
  float occlusion = 0;

  float3 bentNormal = 0;
  float misses = 0;

  int rays = 12;
  int steps = 8;

  for (int i = 0; i < rays; ++i) {
    float3 rn = reflect(gSamplesKernel[i * 2].xyz, random);
    float3 dirL = rn * sign(dot(rn, normal));
    dirL = normalize(dirL + normal * 0.1);

    bool hit = false;
    float3 hitUV;
    float hitDepth;

    for (int j = 0; j < steps; j++){
      hitUV = ssGetUv(origin + dirL * lerp(radiusMin, radiusMax, pow((float)(j + randomAlt.z) / steps, 2)));
      // if (any(abs(hitUV.xy - 0.5)) > 0.5){
      if (any(hitUV.xy) < 0 || any(hitUV.xy) > 1){
        break;
      }

      hitDepth = ssGetDepth(hitUV.xy, true);

      float collisionDepth = linearize(hitUV.z) - linearize(hitDepth);
      bool collision = collisionDepth > 0 && collisionDepth < 0.5;
      if (collision) {
        hit = true;
        break;
      }
    }

    if (hit){
      float3 hitNormal = ssGetNormal(hitUV.xy);
      giValue += txColor.SampleLevel(samLinearBorder0, hitUV.xy, 0).rgb * saturate(-dot(dirL, hitNormal));
    } else {
      bentNormal += dirL;
      misses += 1;
    }
  }

  bentNormal = bentNormal * intensity + normal * 0.1;
  giValue *= intensity;

  float3 curColor = giValue / rays;
  float4 curNormal = float4(normalize(bentNormal), 0.001 + 0.999 * lerp(1, misses / rays, intensity));
  float2 velocity = txMotion.SampleLevel(samLinearSimple, pin.Tex, 0);
  float speedK = saturate(length(velocity) * 500 - 0.01);
  float3 prevColor = txPreviousColor.SampleLevel(samLinearClamp, pin.Tex - velocity, 0);
  float4 prevNormal = txPreviousNormal.SampleLevel(samLinearClamp, pin.Tex - velocity, 0);
  float3 mixedColor = lerp(prevColor, curColor, lerp(0.1, 1, speedK));
  float4 mixedNormal = lerp(prevNormal, curNormal, lerp(0.1, 1, speedK));

  // mixedColor = curColor;
  mixedNormal = curNormal;

  // mixedNormal.xyz = normal;

  RESULT ret;
  ret.color = mixedColor;
  ret.normal = mixedNormal;
  return ret;
}