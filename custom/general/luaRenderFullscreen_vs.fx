cbuffer cbData : register(b10) {
  float4 gRegion;
  float4 gTex;
  float gEncodedDepth;
  float gDepth;
  float2 _pad;
}

cbuffer cbViewport : register(b11) {
  float4 _viewport;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0; 
  noperspective float2 ScreenPos : TEXCOORD1;
  noperspective float3 PosC : TEXCOORD2;
};

#include "include_new/base/_include_vs.fx"

VS_Copy main(uint id: SV_VertexID) {
  VS_Copy vout;
  
  float2 offset = float2(id & 1, id >> 1);
  vout.Tex = gTex.xy + gTex.zw * offset;
  vout.ScreenPos = _viewport.xy + _viewport.zw * vout.Tex;

  float4 region = gRegion;
  vout.PosH = float4((region.xy + region.zw * offset) * 2 - 1, gEncodedDepth, 1);

  float4 posBase = float4((float2(0, 1) + float2(1, -1) * offset) * 2 - 1, gEncodedDepth, 1);
  float3 dir = mul(posBase, ksMVPInverse).xyz * gDepth;
  vout.PosC = dir;
  
  return vout;
}
