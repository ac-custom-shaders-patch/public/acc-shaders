// #define MIPMAPPED_GBUFFER 4
#include "accSS.hlsl"

#define RADIUS 1
#define VERT_MULTIPLIER 10
#define SAMPLE_THRESHOLD 10.2

Texture2D<float> txSurfaceDepth : register(TX_SLOT_SS_ARG0);

struct SamplingParams {
  float2 mapUV;
  float4 bilinearFactors;
};

SamplingParams getSamplingParams(float2 posXZ){
  SamplingParams ret;
  float2 mapRel = (posXZ - gMapPointB) / (gMapPointA - gMapPointB);
  ret.mapUV = mapRel;
  ret.bilinearFactors = bilinearFactors(ret.mapUV * gMapResolutionInv - 0.5);
  return ret;
}

float4 sampleTrackMap(float2 uv, float closeness){
  float total = 0;
  float4 sum = 0;
  [unroll]
  for (int x = -1; x <= 1; x++)
  [unroll]
  for (int y = -1; y <= 1; y++){
    if (abs(x) + abs(y) == 2) continue;
    // sum += txColor.SampleLevel(samLinearBorder0, uv, 3.5 - 3 * closeness, int2(x, y));
    sum += txColor.SampleLevel(samLinearBorder0, uv, 5 - 4.5 * closeness, int2(x, y));
    total++;
  }
  return sum / total;
  // return txColor.SampleLevel(samLinearBorder0, uv, 5 - 4.5 * closeness);
  // return txColor.SampleLevel(samLinearBorder0, uv, 5);
}

#define DEPTH_FN min

float calculateDepth(SamplingParams S){
  float depthRaw = txSurfaceDepth.SampleLevel(samLinearClamp, S.mapUV, 0);
  return gMapOriginY - depthRaw * gMapDepthSize;
}

float calculateDepth(float2 uv){
  float depthRaw = txSurfaceDepth.SampleLevel(samLinearClamp, uv, 0);
  return gMapOriginY - depthRaw * gMapDepthSize;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float depth = ssGetDepth(pin.Tex);
  float3 origin = ssGetPos(pin.Tex, depth);

  float reflValue;
  float3 normal = ssGetNormal(pin.Tex, reflValue);
  // return depth;
  // return saturate(-normal.y);

  float3 random = normalize(txNoise.Sample(samPoint, pin.PosH.xy / 32).xyz);
  float occlusion = 0.0;
  float radius = min(RADIUS, length(origin) / 8);
  radius = RADIUS;

  float2 offset = normalize(normal.xz);
  SamplingParams S = getSamplingParams(origin.xz + offset * 0.5);
  float lightMapDepth = calculateDepth(S);

  float toGround = origin.y - lightMapDepth;
  float closeness = saturate(1 - toGround / 30);
  float4 lightMap = sampleTrackMap(getSamplingParams(origin.xz + offset * (3 - 2 * closeness)).mapUV, closeness);
  lightMap.xyz = pow(saturate(lightMap.xyz), 0.9);

  // lightMap = sampleTrackMap(getSamplingParams(origin.xz + normal.xz * 0.1).mapUV, closeness);

  // return saturate(1 - (origin.y - lightMapDepth) / 1) * 10;
  // return saturate(1 - toGround / 2);
  // return closeness;

  // return abs(origin.y);
  // return abs(origin.y);
  // return abs(gMapOriginY - txSurfaceDepth.SampleLevel(samLinearClamp, S.mapUV, 0) * gMapDepthSize);

  // return frac(origin.y / 10);
  float2 originMapUV = getSamplingParams(origin.xz).mapUV;
  float depthVal = calculateDepth(originMapUV);
  if (min(originMapUV.x, originMapUV.y) < 0 || max(originMapUV.x, originMapUV.y) > 1) return float4(0, 1, 0, 1);
  // return float4(saturate(abs(depthVal) / 20), saturate(abs(depthVal) / 20 - 10), frac(depthVal / 10), 1);
  return saturate(toGround);
  // return lightMapDepth ;
  // return ;
  // return 0;
  // return lightMap * 100 * (1 - saturate((origin.y - lightMapDepth) / 10));
  // return lightMap * 12;
  return lightMap * saturate(-normal.y * 0.5 + 0.5) * sqrt(closeness * saturate(toGround)) * 2;
  // return lightMap * 20;

  return float4(normal, 1);

  // float4 baseColor = 0;
  // baseColor.rgb += (baseColor.rgb + 0.1) * saturate(giValue / 5) * 5;
  // return baseColor;
  // return (1.0 - saturate(pow(occlusion / SAMPLE_THRESHOLD, 1.8))) * txColor.Sample(samPointClamp, pin.Tex);
}