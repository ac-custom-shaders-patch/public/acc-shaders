#include "include/common.hlsl"

Texture2D<float> txDepth : register(t2);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbCamera : register(b0) {
  float4x4 ksView;
  float4x4 ksProjection;
  float4x4 ksMVPInverse;
  float4 ksCameraPosition;
  float ksNearPlane;
  float ksFarPlane;
  float ksFOV;
  float ksDofFactor;
}

float linearize(float depth){
  return 2.0 * ksNearPlane * ksFarPlane / (ksFarPlane + ksNearPlane - (2.0 * depth - 1.0) * (ksFarPlane - ksNearPlane));
}

#define R 1
float minDepth(float2 uv) {
  float ret = 1;
  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    ret = min(ret, txDepth.SampleLevel(samLinear, uv, 0, int2(x, y) * 2));
  }
  return ret;
}

float main(VS_Copy pin) : SV_TARGET {
  // return 0;
  return linearize(minDepth(pin.Tex));
  // return linearize(txDepth.SampleLevel(samLinearSimple, pin.Tex, 0));
}