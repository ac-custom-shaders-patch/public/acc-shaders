#include "include/common.hlsl"

cbuffer cbCamera : register(b0) {
  float4x4 ksView;
  float4x4 ksProjection;
  float4x4 ksMVPInverse;
  float4 ksCameraPosition;
  float ksNearPlane;
  float ksFarPlane;
  float ksFOV;
  float ksDofFactor;
}

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10){
  float gInputRes;
  float gInputResInv;
  float gCameraY;
  float gBoost;
}

#include "include/poisson.hlsl"
float4 main(VS_Copy pin) : SV_TARGET {
  float ret = 0;
  float tot = 0;

  float radius = gInputResInv * 8;
  float frame = floor(pin.Tex.y * 3) / 3;
  bool depthMode = pin.Tex.y >= 2./3;

  #define DISK_SIZE 52

  float2 frameUv = float2(pin.Tex.x, frac(pin.Tex.y * 3));
  for (uint i = 0; i < DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(DISK_SIZE, i) * radius;
    float2 newUv = frameUv + offset;
    newUv.y = frac(newUv.y + 1) / 3 + frame; 

    float vMax = 0;
    
    [branch]
    if (depthMode){
      float4 vA = abs(txDiffuse.GatherRed(samLinearClamp, newUv, int2(0, 0)));
      vMax = max(max(vA.x, vA.y), max(vA.z, vA.w)); 
    }

    if (vMax < (gCameraY ? 6e4 : 1e6)) {
      ret += txDiffuse.SampleLevel(samPointClamp, newUv, 0);
      tot++;
    }
  }

  ret = tot > 3 ? ret / tot : 1e9;
  if (!depthMode){
    ret = saturate(ret * gBoost);
    ret = saturate(ret * gBoost);
  } else if (gCameraY) {
    ret = gCameraY - ret * 10;
  }  

  return ret;
}
