#include "accAltTonemapping.hlsl"

cbuffer cbData : register(b1) {
  float W;
  float3 _pad;
}

float3 applyTonemap(float3 color) {
	float white = W;
	float luma = dot(color, float3(0.2126, 0.7152, 0.0722));
	float toneMappedLuma = luma * (1. + luma / (white*white)) / (1. + luma);
	color *= toneMappedLuma / luma;
	return color;
}
