#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float3 inverseTonemap(float3 x){ return x > 0.9999 ? 0 : x / max(1 - x, 0.00001); }

cbuffer cbData : register(b10) {
  float gIntensity;
}

float4 main(VS_Copy pin) : SV_TARGET {
  // {
  //   float4 m0 = txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0);
  //   float4 m1 = txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 1);
  //   float4 m2 = txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 2);
  //   float4 m3 = txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 3);
  //   float4 m4 = txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 4);
  //   return (m0 + m1 + m2 + m3) * 0.001 + m4;
  // }

  float4 hdr0 = txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0);
  return float4(hdr0.rgb * gIntensity, 0);
}
