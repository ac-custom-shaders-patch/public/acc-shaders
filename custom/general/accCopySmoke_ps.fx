#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);
Texture2D<float> txDepthLR : register(t1);
Texture2D<float> txDepth : register(TX_SLOT_DEPTH);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float4 gRegion;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float2 uv = pin.Tex * gRegion.zw + gRegion.xy;
  float4 color = txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0);
  // color.r = 0;
  return color;

  // color = 0;
  float depth = 1 / txDepth.SampleLevel(samLinearClamp, uv, 0);
  float closestDepth = 1 / txDepthLR.SampleLevel(samLinearClamp, pin.Tex, 0);

  float sum = 1;

  {
    [unroll] for (int x = -1; x <= 1; x++)
    [unroll] for (int y = -1; y <= 1; y++){
      // if (abs(x) + abs(y) == 4) continue;
      if (x == 0 && y == 0) continue;

      float4 val = txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0, int2(x, y));
      if (val.a > color.a && closestDepth > depth + 0.0001){
        color = val;
        // color = 1;
      }
    }
  }

  // if (closestDepth < depth - 0.003){
  //   color = float4(1, 0, 0, 1);
  // }

  // color.gb = 0;

  return color / sum;
}