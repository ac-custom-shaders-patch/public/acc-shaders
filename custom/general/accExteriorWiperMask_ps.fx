#include "include/common.hlsl"

// Texture2DMS<float> txNow : register(t0);
Texture2D<float> txNow : register(t0);
Texture2D<float> txPrevious : register(t1);
Texture2D<float> txMask : register(t2);

cbuffer cbData : register(b8) {
  float gInstanceMult;
  float gIntensityMult;
  float gResolutionStep;
  float gVelocityMult;
  float gRainIntensity;
  float gWiperProgressDelta;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float sampleNow(float2 uv){
//   float resValue = 0;
//   [unroll]
//   for (uint i = 0; i < 4; ++i){
//     resValue += txNow.Load(uv, i);
//   }
//   return resValue / 4;
}

float samplePrev(float2 uv){
  return txPrevious.SampleLevel(samLinearSimple, uv, 0);
}

float main(VS_Copy pin) : SV_TARGET {
  // float now = sampleNow(pin.PosH.xy);
  float now = txNow.SampleLevel(samLinearSimple, pin.Tex, 0);
  now = lerp(1, now, saturate(gWiperProgressDelta * 100));
  float prev = samplePrev(pin.Tex);
  // if (now == 0 && prev < 0.01) return 1;
  // now = lerp(1, now, gIntensityMult * (1 - txMask.SampleLevel(samLinearSimple, pin.Tex, 0)));
  now = lerp(1, now, lerp(gIntensityMult, 1, 0.8) * (1 - txMask.SampleLevel(samLinearSimple, pin.Tex, 0)));
  float mult = now * saturate(prev + lerp(0.01, 0.06, gRainIntensity));
  return mult;
}