#define SS_VIEWLESS
#define SS_VIEWLESS_ROUGH
#include "accSS.hlsl"

Texture2D<float2> txMotion : register(TX_SLOT_MOTION);
Texture2D<float2> txMotionL : register(TX_SLOT_SS_ARG0);

#ifndef BLUR_STEPS
  #define BLUR_STEPS 24
#endif

#ifdef TRIPLE_FSR_AWARE
  cbuffer cbTFAData : register(b11){
    float4 gTFARegion;
    float2 gTFAVelocityMult;
  }

  float2 clipUV(float2 uv){
    // return uv;
    return clamp(uv, gTFARegion.xy, gTFARegion.zw);
  }

  #define VELOCITY_MULT gTFAVelocityMult
#else
  float2 clipUV(float2 uv){
    return uv;
  }

  #define VELOCITY_MULT 1
#endif

float2 smoothVelocity(float2 velocity){
  return velocity / (1 + length(velocity) * 5);
}

float2 getMotion(float2 uv){
  return VELOCITY_MULT * txMotion.SampleLevel(samPointBorder0, uv, 0);
}

float2 neighbourMax(float2 uv){
  // return txMotionL.SampleLevel(samPointClamp, uv, 0).xy;

  float2 nVel = 0;
  [unroll] for (int x = -1; x <= 1; x++)
  [unroll] for (int y = -1; y <= 1; y++){
    // float2 nVelCand = txMotionL.SampleLevel(samLinearClamp, uv, 0, int2(x, y)).xy;
    float2 nVelCand = txMotionL.SampleLevel(samPointClamp, uv, 0, int2(x, y)).xy;
    if (dot2(nVelCand) > dot2(nVel)) nVel = nVelCand;
  }
  return VELOCITY_MULT * nVel;

  // float2 nVel = 0;
  // [unroll] for (int x = -1; x <= 1; x++)
  // [unroll] for (int y = -1; y <= 1; y++){
  //   nVel += txMotionL.SampleLevel(samLinearClamp, uv, 0, int2(x, y)).xy;
  // }
  // return nVel / 9;
}

float similarity(float2 v0, float2 v1){
  float ls0 = length(v0);
  float ls1 = length(v1);
  return saturate(dot(v0 / ls0, v1 / ls1)) * saturate(remap(abs(ls0 - ls1) / max(ls0, ls1), 0.33, 0.67, 1, 0));
}

float4 main(VS_Copy pin) : SV_TARGET {
  float4 base = txColor.SampleLevel(samPoint, pin.Tex, 0);
  float3 random = txNoise.SampleLevel(samPoint, pin.PosH.xy / 32, 0).xyz;
  // random = 0;

  // return 0.5 + txMotionL.SampleLevel(samPoint, pin.Tex, 0).xyxy * 10;
  // return float4(0.5 + txMotion.SampleLevel(samPoint, pin.Tex, 0).xy * 10, 0, 1);

  float2 localRawVelocity = getMotion(pin.Tex);
  float2 localVelocity = smoothVelocity(localRawVelocity) * motionBlurMult;

  float2 areaRawVelocity = neighbourMax(pin.Tex + random.xy * 0.01);
  if (dot2(localRawVelocity) > dot2(areaRawVelocity) - 0.003) {
    areaRawVelocity = localRawVelocity;
  }

  // return float4(localRawVelocity * 1, 0, 1);
  // return float4(localRawVelocity * 1, 0, 1);

  float2 areaVelocity = smoothVelocity(areaRawVelocity) * motionBlurMult;

  [branch]
  if (dot2(areaVelocity) < pow(0.0007, 2)) return base;

  // return base.bgra;

  float originDepth = linearize(ssGetDepth(pin.Tex));
  
  float t = -1;
  float dt = 2. / BLUR_STEPS;
  t += dt * (random.x - 0.5) * gMotionBlurNoiseMult;

  float4 resultArea = 0;
  float resultAreaWeight = 0;

  float movingAlong = similarity(areaRawVelocity, localRawVelocity);
  areaVelocity = lerp(areaVelocity, localVelocity, pow(movingAlong, 2));
  // areaRawVelocity = lerp(areaRawVelocity, localRawVelocity, pow(movingAlong, 2));

  float blurBoost = 1 - similarity(areaRawVelocity, localRawVelocity);

  // float2 uvN = pin.Tex * 2 - 1;
  // float2 overflow = abs(uvN) + abs(areaVelocity * 2);
  // float overflowV = max(overflow.x, overflow.y);
  // if (overflowV > 1) {
  //   pin.Tex = (pin.Tex * 2 - 1) / overflowV * 0.5 + 0.5;
  // }

  #if !defined(NO_MAX_STEP) && !defined(NO_UNROLL)
    [unroll]
  #endif
  for (int c = 0; c < BLUR_STEPS; c++){
    float2 sampleUV = clipUV(pin.Tex + t * areaVelocity);
    // if (any(sampleUV < 0 || sampleUV > 1)) continue;
    float3 sampleColor = max(0, txColor.SampleLevel(samLinearClamp, sampleUV, 0).rgb);
    // float3 sampleColor = 0.01;
    float2 sampleMotion = getMotion(sampleUV);
    float sampleDepth = linearize(ssGetDepth(sampleUV));
    float weight = max(movingAlong, sampleDepth < originDepth + 0.05); // saturate(remap(originDepth - sampleDepth, -0.05, -0.02, 0, 1));
    float weightInv = sampleDepth > originDepth + 0.05;
    float similar = similarity(sampleMotion, areaRawVelocity);
    float phase = pow(1 - pow(abs(t), 1), 4);

    // weight = 1;
    // weightInv = 1;
    // similar = 1;
    // phase = 1;

    float uvMult = all(abs(sampleUV * 2 - 1) < 1);// * any(sampleColor != 0);
    // weight *= uvMult;
    // weightInv *= uvMult;

    resultArea += float4(sampleColor, 1) * lerp(similar, 1, weightInv ? 1 : blurBoost) * uvMult;
    resultAreaWeight = max(resultAreaWeight, weight * phase * similar);
    t += dt;
  }

  float4 result = float4(base.rgb, 1) * 0.01 + resultArea * resultAreaWeight;
  result /= result.w;

  // if (any(!(result < 1e30)) || any(result == 0)) {
  //   return float4(1, 0, 1, 1);
  // }

  // result.rgb *= 0.05;
  // result += float4(abs(areaRawVelocity), 0, 0);

  return result;
}