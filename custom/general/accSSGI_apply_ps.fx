#include "accSS_cbuffer.hlsl"

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

Texture2D txColor : register(TX_SLOT_COLOR);
Texture2D txSSGI : register(TX_SLOT_SS_ARG0);

float4 getGiValue21(float2 uv){  
  float4 resultBase = 0;
  [unroll] for (int x = -2; x <= 2; x++)
  [unroll] for (int y = -2; y <= 2; y++){
    if (abs(x) + abs(y) == 4) continue;
    resultBase += txSSGI.SampleLevel(samLinearSimple, uv, 2, int2(x, y));
  }
  resultBase /= 21;
  return resultBase;
}

float4 getGiValue9(float2 uv){  
  float4 resultBase = 0;
  [unroll] for (int x = -1; x <= 1; x++)
  [unroll] for (int y = -1; y <= 1; y++){
    resultBase += txSSGI.SampleLevel(samLinearSimple, uv, 2, int2(x, y));
  }
  resultBase /= 9;
  return resultBase;
}

float4 main(VS_Copy pin) : SV_TARGET {
  // return txSSGI.SampleLevel(samLinearSimple, pin.Tex, 0);
  #ifdef DEBUG_VIEW
    return getGiValue9(pin.Tex) * 2;
  #endif
  return txColor.SampleLevel(samLinear, pin.Tex, 0)
    * (1 + pow(max(getGiValue9(pin.Tex), 0), 1.2));
}