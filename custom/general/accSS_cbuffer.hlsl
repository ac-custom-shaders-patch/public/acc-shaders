#ifndef _SS_CBUFFER_
#define _SS_CBUFFER_

#include "include/common.hlsl"

#define TX_SLOT_COLOR TX_SLOT(0)
#define TX_SLOT_MOTION TX_SLOT(3)
#define TX_SLOT_NORMALS TX_SLOT(4)
#define TX_SLOT_GDEPTH TX_SLOT(5)
#define TX_SLOT_SS_ARG0 TX_SLOT(7)
#define TX_SLOT_SS_ARG1 TX_SLOT(8)
#define TX_SLOT_SS_ARG2 TX_SLOT(9)
#define TX_SLOT_SS_ARG3 TX_SLOT(10)
#define TX_SLOT_SS_ARG4 TX_SLOT(11)
#define TX_SLOT_SS_ARG5 TX_SLOT(12)

#define SAMPLE_COUNT 24
#ifdef TARGET_PS
  #ifdef SS_VIEWLESS
    #define SS_VIEW_DEPENDANT(TYPE, NAME) TYPE __##NAME
  #else
    #define SS_VIEW_DEPENDANT(TYPE, NAME) TYPE NAME
  #endif
  cbuffer cbData : register(b10){
    SS_VIEW_DEPENDANT(float4x4, viewProj);
    SS_VIEW_DEPENDANT(float4x4, viewProjInv);
    SS_VIEW_DEPENDANT(float4x4, viewProjPrev);
    float4 gSamplesKernel[SAMPLE_COUNT];  
    float4 gSize; 

    float ssaoRadius;
    float ssaoOpacity;
    float ssaoExp;
    float motionBlurMult;

    uint sampleCount;
    float sampleCountInv;
    float fogBlurDistanceInv;
    float ksGameTimeS;

    float ss_ksFogBlend;
    float ss_ksFogLinear;
    float ss_extFogConstantPiece_dep;
    float ss_extFogExp;

    float gCausticsScale;
    float gMapOriginY;
    float gMapResolutionInv;
    float gMapDepthSize;

    float2 gMapPointA;
    float2 gMapPointB;

    float2 gMapBlendOffset;
    float gMotionBlurNoiseMult;
    float gSSGIIntensity;

    float gTrackLightIntensity;
    float gTrackHeatFactor;
    float2 gWindOffset;

    float __pad;
    float gMainBrightnessMult;
    float gApplyGammaFix;
    float gLightsSceneMult;

    float gSSLRBrightness;
    float gSSLRSaturation;
    uint gTAAHistorySharpen_blurNew;
    float gTAASharpenMult;

    uint2 p_fog2Color_blend;
    float p_fog2Exp;
    float p_fog2Linear;

    #define gTAAHistorySharpen loadVector(gTAAHistorySharpen_blurNew).x
    #define gBlurNew loadVector(gTAAHistorySharpen_blurNew).y
  }

  #define gFrameNum 0
  #define gRandom ((float2)0)
  #define GAMMA_FIX_CONDITION gApplyGammaFix
#endif

#endif