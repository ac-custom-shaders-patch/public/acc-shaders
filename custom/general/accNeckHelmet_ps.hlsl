#include "include/common.hlsl"

// #undef SAMPLES
// #define SAMPLES 1

#if SAMPLES == 1
  Texture2D txDiffuse : register(t0);
#else
  Texture2DMS<float4> txDiffuse : register(t0); 
#endif

Texture2D txRainDrops : register(t1);
Texture2D txScene : register(t2);
Texture2D txPrevFrame : register(TX_SLOT_PREV_FRAME);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float4 gRegion;
  float4 gRainParams;
  float2 gSize;
  float gAlphaExp;
  float gAlphaMin;
}

#if USE_BLUR == 1

#include "include/poisson.hlsl"

float4 sampleTex(Texture2D tx, float2 uv) : SV_TARGET {
  float4 ret = 0;
  float tot = 0;
  #define PUDDLES_DISK_SIZE 16
  [unroll]
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.02;
    float2 sampleUV = uv + offset;
    float4 v = tx.SampleLevel(samLinearClamp, sampleUV, 0);
    float w = 1;
    ret += v * w;
    tot += w;
  }
  ret /= tot;   
  return ret;
}

#else

#define MSTEX_SAMPLES SAMPLES
#define MSTEX_SIZE gSize
#include "include/ms_tex_sampling.hlsl"

#endif

float4 main(VS_Copy pin) : SV_TARGET {
  float2 uv = ((pin.Tex * 2 - 1) * 0.95 * 0.5 + 0.5) * gRegion.zw + gRegion.xy;
  float4 ret = sampleTex(txDiffuse, uv);
  // return float4(ret.a, 0, 0, 1);

  ret.a = pow(saturate(ret.a), gAlphaExp);
  ret.a = saturate(ret.a * 1.05);
  ret.rgb *= 0.5;

  ret.a = max(ret.a, gAlphaMin);

  [branch]
  if (gRainParams.w) {
    float2 uvRain = (rotate2d(pin.Tex * 2 - 1, gRainParams.z) + gRainParams.xy) * 0.8 * gRainParams.w;
    float opacity = lerpInvSat(max(abs(uvRain.x), abs(uvRain.y)), 1, 0.9);
    float4 rainDrops = txRainDrops.SampleLevel(samLinearClamp, (uvRain * 0.5 + 0.5) * gRegion.zw + gRegion.xy, 0);
    rainDrops.rgb /= max(rainDrops.w, 0.0001);

    float4 sceneColor = txScene.SampleLevel(samLinearClamp, pin.Tex * gRegion.zw + gRegion.xy + rainDrops.xy, rainDrops.z);
    rainDrops.rgb = sceneColor.rgb;
    rainDrops.w = saturate(rainDrops.w * 1.4);
    rainDrops.w *= opacity;

    rainDrops.w *= 1 - ret.w;
    ret.rgb = (rainDrops.rgb * rainDrops.w + ret.rgb * ret.w) / (rainDrops.w + ret.w);
    ret.w = 1 - (1 - ret.w) * (1 - rainDrops.w);
  }

  if (ret.a < 0.001){
    discard;
  }

  return ret;
}