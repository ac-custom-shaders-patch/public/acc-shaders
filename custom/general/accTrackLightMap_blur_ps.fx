#include "accSS.hlsl"

Texture2D txPreviousMap : register(TX_SLOT_SS_ARG0);

#define R 2
float4 main(VS_Copy pin) : SV_TARGET {
  float4 currentShot = 0;
  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    // if (abs(x) + abs(y) != 0) continue;
    currentShot += float4(txPreviousMap.SampleLevel(samLinear, pin.Tex, 0, int2(x, y)*2).xyz, 1) 
      / (1 + 1 * length(float2(x, y)));
  }
  currentShot /= currentShot.w;
  // return float4(5, 0, 0, 1);
  return currentShot;
}