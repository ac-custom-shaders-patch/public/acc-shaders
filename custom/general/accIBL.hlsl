#include "include/common.hlsl"
#include "include/samplers.hlsl"
#include "include_new/base/cbuffers_common.fx"

TextureCube txFirst : register(t0);
Texture2D txNoise : register(TX_SLOT_NOISE);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float4x4 transform;
  uint mipIndex;
  float gLuminance_gammaFlag;
  #define gLuminance abs(gLuminance_gammaFlag)
  #define gUseLuminanceWeight (gLuminance_gammaFlag > 0 && !GAMMA_FIX_ACTIVE)
  uint gSaturation_cameraVAO;
  #define gSaturation loadVector(gSaturation_cameraVAO).x
  #define cameraVAO loadVector(gSaturation_cameraVAO).y
  float gBlurRadius;
}

/*
function conv(v){
    var l = Math.sqrt(v[0] * v[0] + v[1] * v[1]);
    var v0s = Math.sin(v[0]), v0c = Math.cos(v[0]);
    var v1s = Math.sin(v[1]), v1c = Math.cos(v[1]);
    return `float4(${v0s * v1c}, ${v1s}, ${v0c * v1c}, ${1 - l}),`;
}

`  float3(0.04937396f, 0.02976172f, 0.05765022f),
  float3(-0.2517526f, -0.1488713f, 0.2924757f),
  float3(0.08991148f, -0.3482215f, 0.3596419f),
  float3(-0.2874624f, 0.3059576f, 0.4198151f),
  float3(0.2427425f, 0.3638484f, 0.4373895f),
  float3(0.4653715f, -0.276858f, 0.5414988f),
  float3(-0.5950293f, 0.07174515f, 0.599339f),
  float3(0.5978429f, 0.1261958f, 0.6110168f),
  float3(-0.1569897f, 0.6188509f, 0.638453f),
  float3(-0.1061006f, -0.6564524f, 0.6649715f),
  float3(-0.6132807f, -0.3248489f, 0.6940029f),
  float3(0.3505149f, 0.671895f, 0.7578282f),
  float3(-0.6101021f, 0.6048418f, 0.8591031f),
  float3(0.5450442f, -0.6669612f, 0.8613422f),
  float3(-0.8746202f, -0.09585806f, 0.8798575f),
  float3(0.8157356f, -0.3300874f, 0.8799899f),
  float3(-0.4046993f, -0.8112252f, 0.9065692f),
  float3(0.8501958f, 0.3312514f, 0.9124475f),
  float3(0.07507778f, -0.9434683f, 0.9464508f),
  float3(0.9473069f, -0.005138647f, 0.9473209f),
  float3(-0.162317f, 0.9532719f, 0.9669923f),
  float3(0.6808234f, 0.6924866f, 0.9711118f),
  float3(-0.93586f, 0.3136373f, 0.9870169f),
  float3(-0.7859368f, -0.6157285f, 0.9984079f),`.split('\n').map(x => /\((.+?)f, (.+?)f,/.test(x) ? conv([+RegExp.$1, +RegExp.$2]) : null).join('\n')
*/

#define CUBEMAP_BLUR_SIZE 24
const static float4 CUBEMAP_BLUR[24] = {
  float4(0.049332045662270244, 0.029757326571175125, 0.9983390460089758, 0.9423497796757029),
  float4(-0.24634641010674366, -0.14832201160552833, 0.9577651210504637, 0.7075242991794874),
  float4(0.08440126420796994, -0.3412265916629811, 0.9361841911409108, 0.6403581124256514),
  float4(-0.27035271760986007, 0.30120644122736817, 0.9144310186362377, 0.580184940227819),
  float4(0.22462989680287987, 0.3558733211013863, 0.9071359263035486, 0.5626104945374089),
  float4(0.43166579382234566, -0.2733346532323092, 0.8596236442688726, 0.4585011678902252),
  float4(-0.5590910032613288, 0.07168361607483047, 0.8260016399854647, 0.40066100209730016),
  float4(0.5583848881976465, 0.12586111393440114, 0.8199788391365312, 0.3889832138001036),
  float4(-0.1273506502029367, 0.5800995494002498, 0.8045286350892158, 0.3615470241772696),
  float4(-0.08389129480059324, -0.6103104232350645, 0.7877077109863254, 0.335028503779207),
  float4(-0.5454513525043527, -0.31916559218768153, 0.774994288246474, 0.3059971002771674),
  float4(0.26874525277480626, 0.6224702132166523, 0.7350556596401534, 0.242171796416226),
  float4(-0.47130506429659447, 0.5686319493103249, 0.67418783924951, 0.1408968772890824),
  float4(0.40735343226135534, -0.6186012462391769, 0.6718598658769778, 0.13865777866223228),
  float4(-0.7637774241596145, -0.09571132452507848, 0.6383442556346532, 0.12014247635483422),
  float4(0.6889157191577263, -0.324125711257944, 0.6483345241454637, 0.12001007917924422),
  float4(-0.27113513671241324, -0.7251314040197273, 0.6329851376962338, 0.09343072601398517),
  float4(0.7105600548164943, 0.3252266517485917, 0.6239647694314006, 0.08755252844911676),
  float4(0.044028038645732105, -0.8095988010156074, 0.5853300891010992, 0.053549205634746966),
  float4(0.8118353075367168, -0.005138624385107347, 0.5838638779511823, 0.05267916285948804),
  float4(-0.0935724011252397, 0.8153143563573858, 0.5714076531384567, 0.033007691954786034),
  float4(0.48444996760600223, 0.6384529926756971, 0.5980685621481784, 0.02888816649825554),
  float4(-0.7658343591626613, 0.30852053374604643, 0.564192178767989, 0.012983133096860389),
  float4(-0.5775595262033494, -0.5775533885893623, 0.5769376716948786, 0.001592147814086009),
};

// void tweakSample(inout float4 value) {
//   float m = max(value.r, max(value.g, value.b));
//   value.rgb *= m;
// }

// void tweakFinal(inout float4 value) {
//   float m = max(value.r, max(value.g, value.b));
//   value.rgb *= sqrt(m) / m;
// }

void tweakSample(inout float4 value) {
  // value.rgb = pow(max(0, value.rgb), 2);
  if (!gUseLuminanceWeight) return;
  float m = max(value.r, max(value.g, value.b));
  value.rgb *= m;
}

void tweakFinal(inout float4 value) {
  // value.rgb = pow(max(0, value.rgb), 0.5);
  if (!gUseLuminanceWeight) return;
  float m = max(value.r, max(value.g, value.b));
  if (m > 0) value.rgb *= sqrt(m) / m;
  else value.rgb = 0;
}

float4 applyColorCorrection(float4 ret){
  // float x = max(ret.r, max(ret.g, ret.b));
  // float limit = 0.05;
  // if (x > limit) {
  //   ret.rgb *= limit / x;
  // }

  ret.rgb *= gLuminance;
  ret.rgb = max(0, lerp(luminance(ret.rgb), ret.rgb, gSaturation));
  return ret;
}

float2 Hammersley(uint i, uint N) {
  float ri = reversebits(i) * 2.3283064365386963e-10f;
  return float2(float(i) / float(N), ri);
}

float3 ImportanceSampleGGX(float2 Xi, float Roughness, float3 N) {
  float a = Roughness * Roughness;

  float Phi = 2 * M_PI * Xi.x;
  float CosTheta = sqrt((1 - Xi.y) / (1 + (a * a - 1) * Xi.y));
  float SinTheta = sqrt(1 - CosTheta * CosTheta);

  float3 H;
  H.x = SinTheta * cos(Phi);
  H.y = SinTheta * sin(Phi);
  H.z = CosTheta;

  float3 UpVector = abs(N.z) < 0.999 ? float3(0, 0, 1) : float3(1, 0, 0);
  float3 TangentX = normalize(cross(UpVector, N));
  float3 TangentY = cross(N, TangentX);

  return TangentX * H.x + TangentY * H.y + N * H.z;
}

float3 GetNormal(uint face, float2 uv) {
	float2 debiased = uv * 2 - 1;
	switch (face) {
		case 0: return float3(1, -debiased.y, -debiased.x);
		case 1: return float3(-1, -debiased.y, debiased.x);
		case 2: return float3(debiased.x, 1, debiased.y);
		case 3: return float3(debiased.x, -1, -debiased.y);
		case 4: return float3(debiased.x, -debiased.y, 1);
	  default: return float3(-debiased.x, -debiased.y, -1);
	};
}

float D_GGX(float a, float NdotH) {
  float d = NdotH * NdotH * (a - 1) + 1;
  return a / (M_PI * d * d);
}

#ifndef NUM_SAMPLES
  #define NUM_SAMPLES 32
#endif

float3 PrefilterEnvMap(float fRoughness, float3 R) {
	float fTotalWeight = 0.0000001f;
	float3 vView = R;
	float3 vNormal = R;
	float3 vPrefilteredColor = 0;

  // return 1;

	for (uint i = 0; i < NUM_SAMPLES; i++) {
    float2 vXi = Hammersley(i, NUM_SAMPLES);

    float3 vHalf = ImportanceSampleGGX(vXi, fRoughness, vNormal);
    float3 vLight = 2 * dot(vView, vHalf) * vHalf - vView;

    float fNdotL = saturate(dot(vNormal, vLight));
		if (fNdotL > 0) {
      // Vectors to evaluate pdf
      // float NdotH = saturate(dot(vNormal, vHalf));
      // float fVdotH = saturate(dot(vView, vHalf));

      // // Probability Distribution Function
      // float fPdf = D_GGX(fRoughness, NdotH) * NdotH / (4 * fVdotH);

      // // Solid angle represented by this sample
      // float fOmegaS = 1.0 / (NUM_SAMPLES * fPdf);

      // // Solid angle covered by 1 pixel with 6 faces that are EnvMapSize X EnvMapSize
      // float EnvMapSize = 64;
      // float fOmegaP = 4.0 * M_PI / (6.0 * EnvMapSize * EnvMapSize);
      // // Original paper suggest biasing the mip to improve the results
      // float fMipBias = 1;
      // float fMipLevel = max(0.5 * log2(fOmegaS / fOmegaP) + fMipBias, 0);
      // vPrefilteredColor += txFirst.SampleLevel(samLinear, vLight, fMipLevel).rgb * fNdotL;
      // fTotalWeight += fNdotL;

      float4 value = txFirst.SampleLevel(samLinear, vLight, mipIndex);
      // tweakSample(value);
			vPrefilteredColor += value.rgb * fNdotL;
			fTotalWeight += fNdotL;
		}
	}

	float4 ret = float4(vPrefilteredColor / fTotalWeight, 0);
  // tweakFinal(ret);
  return ret.rgb;
}

float getRoughness(){
  float roughness = saturate((float)(mipIndex - 1) / 5);
  roughness = pow(roughness, 6);
  return roughness;
}

float3 getNormal(float2 uv, float4x4 t){
  float3 vec = float3(1 - uv * 2, -1);
  return normalize(mul(vec, (float3x3)t));
}

float3 getNormal(float2 uv){
  return getNormal(uv, transform);
}

float gammaSampleWeight(float4 value) {
  float roughness = saturate((float)(mipIndex - 1) / 3);
  // float nightK = lerp(0.0002, clamp(0.00001 * extGlowBrightness / ksAmbientColor_sky0.g, 0.001, 1), roughness);
  float nightK = lerp(1 - 0.999 * cameraVAO, 1, roughness);
  // returning 1 is a safe route here, but lowering this value for low MIPs increases glow around shiny objects without messing up rough surfaces
  // nightK = 1;
  return 1 / (extMainBrightnessMult + luminance(value.rgb) * nightK);
}
