#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10){
  float2 gRadius;
  float2 gAspectRatio;
}

float3 samplePoint(float2 uv){
  return txDiffuse.SampleLevel(samLinearClamp, uv, 0).xyz;
}

#include "include/poisson.hlsl"
float4 main(VS_Copy pin) : SV_TARGET {
  float4 currentShot = 0;

  #define DISK_SIZE 16
  for (uint i = 0; i < DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(DISK_SIZE, i);
    currentShot += float4(samplePoint(pin.Tex + offset * gRadius), 1);
  }

  if (gAspectRatio.x){
    float over = gAspectRatio.x < 1 ? abs(pin.Tex.x * 2 - 1) - gAspectRatio.x : abs(pin.Tex.y * 2 - 1) - gAspectRatio.y;
    currentShot = lerp(currentShot, 0.068 * lerp(0.61, 1, saturate(over * 20)), saturate(over * 20 + 0.5));
  }

  return currentShot / DISK_SIZE;
}
