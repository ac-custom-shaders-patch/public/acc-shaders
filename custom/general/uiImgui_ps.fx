#include "include/common.hlsl"
#include "include_new/base/samplers_ps.fx"

struct PS_INPUT {
  float4 pos : SV_POSITION;
  float4 col : COLOR0;
  float2 uv  : TEXCOORD0;
  float4 mad : COLOR1;
};

#ifdef INPUT_R8_FORMAT
  Texture2D<float> txInput;
#else
  Texture2D txInput;
#endif

Texture2D txNoise : register(TX_SLOT_NOISE);

cbuffer cbData : register(b10) {
  float gDithering;
  float gSaturation;
  float gBias;
  uint gFlags;
  
  #if defined(USE_BLURRING)
    float2 gBlurring;
    float2 gPad2;
  #elif defined(USE_SHARPENING)
    float2 gSharpeningSizeInv;
    float gSharpening;
    float _pad1;
  #elif defined(USE_TONEMAPPING)
    float gGamma;
    float gWhitePoint;
    float2 _genParams;
  #endif
}

float4 prep(float4 v){
  return v;
}

#include "include/poisson.hlsl"

#define BICUBIC_SAMPLER samLinearClamp
#include "include/bicubic.hlsl"

float4 sampleTexture(float2 uv){
  #if defined(USE_BLURRING)
    float4 ret = 0;
    float tot = 0.0001;

    uint2 dim;
    uint sampleCount; 
    txInput.GetDimensions(0, dim.x, dim.y, sampleCount); 
    float2 radius = float2(dim) * gBlurring;
    float mip = log2(min(radius.x, radius.y));

    #define DISK_SIZE 32
    for (uint i = 0; i < DISK_SIZE; ++i) {
      float2 offset = SAMPLE_POISSON(DISK_SIZE, i);
      float2 sampleUV = uv + offset * gBlurring;
      float4 v = txInput.SampleLevel(samLinearClamp, sampleUV, mip);
      float w = (1 + dot(v.rgb, 100)) * v.a;
      ret += v * w;
      tot += w;
    }
    // return float4(1, 0, 0, ret.w);
    return ret / tot;
  #elif defined(USE_SHARPENING)
    [branch]
    if (gBias <= -20) {
      #ifndef INPUT_R8_FORMAT
        return sampleBicubicFixed(txInput, uv, -gBias - 20);
      #else
        return txInput.SampleBias(samLinearClamp, uv, -gBias - 20);
      #endif
    } else {
      float4 base = prep(txInput.SampleBias(samLinearClamp, uv, gBias));
      float4 blurred = 0
        + prep(txInput.Sample(samLinearClamp, uv + gSharpeningSizeInv * float2(0.5, 0.5), 0))
        + prep(txInput.Sample(samLinearClamp, uv + gSharpeningSizeInv * float2(0.5, -0.5), 0))
        + prep(txInput.Sample(samLinearClamp, uv + gSharpeningSizeInv * float2(-0.5, 0.5), 0))
        + prep(txInput.Sample(samLinearClamp, uv + gSharpeningSizeInv * float2(-0.5, -0.5), 0));
      float4 delta = base - blurred / 4;
      float4 ret = base + delta * gSharpening;
      return float4(ret.rgb, ret.w);
    }
  #else
    return txInput.SampleBias(samLinearClamp, uv, gBias);
  #endif
}

#ifdef USE_TONEMAPPING
const static float A = 0.22;
const static float B = 0.3;
const static float C = 0.1;
const static float D = 0.20;
const static float E = 0.01;
const static float F = 0.30;

float filmic_curve(float x) {
	return ((x*(0.22*x+0.1*0.3)+0.2*0.01)/(x*(0.22*x+0.3)+0.2*0.3))-0.01/0.3;
}

float3 filmic(float3 x) {
  float w = filmic_curve(gWhitePoint);
  return pow(max(0, float3(
    filmic_curve(x.r),
    filmic_curve(x.g),
    filmic_curve(x.b)) / w), abs(gGamma));
}
#endif

float3 tonemappingReinhardJodie(float3 v) {
  float l = luminance(v);
  float3 tv = v / (1 + v);
  return lerp(v / (1 + l), tv, tv);
}

// special MAD values: 
// 0, 0, 0, 0: float4(tex.rgb, tex.g)
// 0, 0, 0, 1: float4(color, tex.r)
float4 main(PS_INPUT pin) : SV_Target {
  float4 in_col = sampleTexture(pin.uv);
  #ifdef INPUT_R8_FORMAT
    in_col = float4(1, 1, 1, in_col.r);
  #endif

  if (gFlags & 1) {
    // in_col.rgb /= max(0.0001, in_col.w);
  }

  in_col.rgb = lerp(luminance(in_col.rgb), in_col.rgb, gSaturation);
  #ifndef USE_TONEMAPPING
    in_col.rgb = saturate(in_col.rgb);
  #endif
  if (dot(pin.mad.xyz, 1)){
    in_col.rgb = in_col.rgb * pin.mad.x + pin.mad.y;
    in_col.a = saturate(in_col.a * pin.mad.z + pin.mad.w);
  } else if (pin.mad.w) {
    in_col = float4(1, 1, 1, pin.mad.w < 0 ? 1 - in_col.r : in_col.r);
  } else {
    in_col = float4(in_col.rgb, in_col.g);
  }
  float4 out_col = pin.col * in_col; 

  #ifdef USE_TONEMAPPING
  out_col = max(0, out_col);
  // out_col.x = tonemap_uchimura(out_col.x);
  // out_col.y = tonemap_uchimura(out_col.y);
  // out_col.z = tonemap_uchimura(out_col.z);
  if (gGamma < 0) {
    out_col.rgb = pow(max(0, out_col.rgb * _genParams.x), _genParams.y) + dithering(pin.pos.xy);
    if (!gWhitePoint) out_col.rgb = tonemappingReinhardJodie(out_col.rgb);
  }
  if (gWhitePoint > 0) out_col.rgb = filmic(out_col.rgb);
  #endif

  out_col.w = saturate(out_col.w);
  #ifdef USE_SUBTRACT
    return 1 - out_col.w;
  #endif

  out_col.rgb += dithering(pin.pos.xy) * gDithering;
  return out_col; 
}
