[ 1, 4 ].map(channels => [ 0, 7, 15, 23, 35, 63, 127 ].map(kernelSize => ({
  source: `accBlurGaussian.hlsl`,
  target: FX.Target.PS,
  saveAs: `accBlurGaussian_ps_k${kernelSize}_c${channels}.fxo`,
  defines: { 'KERNEL_SIZE': kernelSize, 'GAUSSIAN_TYPE': channels == 1 ? 'float' : 'float' + channels }
}))).flat()