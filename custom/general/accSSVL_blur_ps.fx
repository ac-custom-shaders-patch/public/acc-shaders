// #define MIPMAPPED_GBUFFER 2
#ifndef NO_ENTRY_POINT
  #define SS_VIEWLESS
#endif
#include "accSS.hlsl"

Texture2D txVL : register(TX_SLOT_SS_ARG0);

float4 blurVL(float2 uv, float2 dir){
  // float4 random = txNoise.Sample(samPoint, uv * 397);
  // uv += (random.xy - 0.5) * gSize.zw * 4;

  float4 resultBase = txVL.SampleLevel(samLinearClamp, uv, 0);
  float coneNearMin = resultBase.w;
  resultBase.w = 1;

  float2 dirO = gSize.zw * dir * 4.7;

  [unroll] for (int y = 1; y <= 2; y++){
    float4 s1 = txVL.SampleLevel(samLinearClamp, uv + dirO * y, 0);
    float4 s2 = txVL.SampleLevel(samLinearClamp, uv + dirO * -y, 0);
    float weight = 1.0 / (abs(y) + 0.2);
    resultBase += float4(s1.rgb, 1) * weight;
    resultBase += float4(s2.rgb, 1) * weight;
    coneNearMin = min(coneNearMin, s1.w);
    coneNearMin = min(coneNearMin, s2.w);
  }
  
  return float4(resultBase.rgb / resultBase.w, coneNearMin);
}

#ifndef NO_ENTRY_POINT
float4 main(VS_Copy pin) : SV_TARGET {
  return blurVL(pin.Tex, float2(0, 1));
}
#endif