#include "include/common.hlsl"

struct VS_IN_skinned {
  AC_INPUT_SKINNED_ELEMENTS
};

struct VS_MappingView {
  float4 PosH : SV_POSITION;
};

cbuffer cbData : register(b10) {
  float2 offset;
  float zoom;
  float pad;
}

VS_MappingView main(VS_IN_skinned vin) {
  VS_MappingView vout;

  float2 pos = frac(vin.Tex);
  // pos = lerp(pos, offset, zoom);
  // pos = pos * (1 - zoom) + offset * zoom;
  // pos = pos * (1 - zoom) + offset * zoom;
  pos = (pos - offset * zoom) / (1 - zoom);
  pos.y = 1 - pos.y;
  vout.PosH = float4(pos * 2 - 1, 0, 1);
  return vout;
}
