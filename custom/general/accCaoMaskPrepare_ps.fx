#include "include/common.hlsl"

Texture2D<float> txMaskBase : register(t1);
Texture2D<float> txHeightMap : register(t2);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float main(VS_Copy pin) : SV_TARGET {
  float mask = txMaskBase.SampleLevel(samLinearClamp, pin.Tex, 0);
  float height = txHeightMap.SampleLevel(samLinearClamp, pin.Tex, 0);
  for (uint i = 0; i < 16; ++i){
    float2 uv = pin.Tex + float2(i * 0.01 * sign(pin.Tex.x * 2 - 1), 0);
    if (txHeightMap.SampleLevel(samLinearClamp, uv, 0) < height - 0.1){
      mask = 1;
    }
  }
  return mask;
}
