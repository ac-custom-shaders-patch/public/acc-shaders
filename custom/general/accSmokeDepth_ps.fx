#include "include/common.hlsl"

Texture2D<float> txDepth : register(TX_SLOT_DEPTH);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float4 gRegion;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float2 uv = pin.Tex * gRegion.zw + gRegion.xy;
  float ret = 1;
  [unroll] for (int x = -1; x <= 1; x++){
    [unroll] for (int y = -1; y <= 1; y++){
      ret = min(ret, txDepth.SampleLevel(samLinearClamp, uv, 0, int2(x, y)));
    }
  }
  return ret;
}