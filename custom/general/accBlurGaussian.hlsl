#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float2 gPixelSize;
  #ifdef HELMET_MODE
    float gAlphaExp;
    float _pad;
  #else
    float2 _pad;
  #endif
}

#ifdef HELMET_MODE
  float4 _preprocess(float4 v) {
    v.a = pow(saturate(v.a), gAlphaExp);
    return v;
  }
  #define GAUSSIAN_PREPROCESS _preprocess
#endif

#include "include/gaussian.hlsl"

Texture2D<GAUSSIAN_TYPE> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#define FN_NAME(X) GaussianBlur##X

GAUSSIAN_TYPE main(VS_Copy pin) : SV_TARGET {
  #if KERNEL_SIZE == 0
    GAUSSIAN_TYPE ret = 0;
    float tw = 0;
    [unroll(16)]
    for (int i = 0; i < 8; ++i) {
      float2 offset = gPixelSize * i;
      float w = 1 - i / 8.;
      ret += (GAUSSIAN_PREPROCESS(txDiffuse.SampleLevel(samLinearClamp, pin.Tex + offset, 0)) 
        + GAUSSIAN_PREPROCESS(txDiffuse.SampleLevel(samLinearClamp, pin.Tex - offset, 0))) * w;
      tw += w;
    }
    return ret / (tw * 2);
  #else
    return FN_NAME(KERNEL_SIZE)(txDiffuse, samLinearClamp, pin.Tex, gPixelSize);
  #endif
}