#include "accSSLR_flags.hlsl"
#include "accSS_cbuffer.hlsl"
#include "include_new/base/samplers_ps.fx"
#include "include/common.hlsl"
#include "include/bayer.hlsl"

Texture2D txNormals : register(TX_SLOT_NORMALS);
Texture2D<float> txDepth : register(TX_SLOT_GDEPTH);
Texture2D txReflections1 : register(TX_SLOT_SS_ARG0);
Texture2D txReflections2 : register(TX_SLOT_SS_ARG1);
Texture2D txNoise : register(TX_SLOT_NOISE);

Texture2D<float2> txMotion : register(TX_SLOT_MOTION);
Texture2D txPrevious : register(TX_SLOT_SS_ARG2);
Texture2D<float> txPreviousDimming : register(TX_SLOT_SS_ARG3);
Texture2D<float> txQualityLayer : register(TX_SLOT_SS_ARG5);

cbuffer cbSSLRNewData : register(b11){
  uint2 gScreenResolution;
  float2 gCrossOffsetMult;
  uint gTracingSteps;
  float3 gCameraDir;
  int gMIPsCount;
  float gSSLRScale;
  float gStartOffsetMult;
}

#define SCREEN_SIZE_INT gScreenResolution
#define CROSS_OFFSET_MULT gCrossOffsetMult
#define MAX_THICKNESS_SIMPLE 0.005
#define MAX_THICKNESS_HIZ 0.0001

struct PS_IN {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

struct PS_OUT {
  float4 main : SV_TARGET0;
  float dimming : SV_TARGET1;
};

float3 ssGetPos(float2 uv, float depth){
  float4 p = mul(float4(uv.xy, depth, 1), viewProjInv);
  return p.xyz / p.w;
}

float3 ssGetPos(float3 posS){
  return ssGetPos(posS.xy, posS.z);
}

float3 ssGetUv(float3 position){
  float4 p = mul(float4(position, 1), viewProj);
  return p.xyz / p.w;
}

float ssGetDepth(float2 uv, float level = 0){
  return txDepth.SampleLevel(samLinearClamp, uv, level).x;
}

float2 getCellCount(int mipLevel) {
  return (float2)(SCREEN_SIZE_INT >> mipLevel);
}

float2 getCell(float2 pos, float2 cellCount) {
    return float2(floor(pos * cellCount));
}

float3 intersectDepthPlane(float3 o, float3 d, float z) {
	return o + d * z;
}

float3 intersectCellBoundary(float3 o, float3 d, float2 cell, float2 cellCount, float2 crossStep, float2 crossOffset) {
	float2 index = cell + crossStep;
	float2 boundary = index / cellCount + crossOffset;	
	float2 delta = (boundary - o.xy) / d.xy;
	return intersectDepthPlane(o, d, min(delta.x, delta.y));	
}

bool crossedCellBoundary(float2 cellIdxOne, float2 cellIdxTwo) {
  return floor(cellIdxOne.x) != floor(cellIdxTwo.x) || floor(cellIdxOne.y) != floor(cellIdxTwo.y);
}

float findMaxTraceDistance(float3 posS, float3 dirS) {  
  float maxTraceDistance = 1;
  // posS.x = remap(posS.x, gEdgeLeft, gEdgeRight, 0, 1);
  maxTraceDistance = dirS.x > 0 ? (1 - posS.x) / dirS.x  : -posS.x / dirS.x;
  maxTraceDistance = min(maxTraceDistance, dirS.y < 0 ? -posS.y / dirS.y  : (1-posS.y) / dirS.y);
  maxTraceDistance = min(maxTraceDistance, dirS.z < 0 ? -posS.z / dirS.z : (1-posS.z) / dirS.z);
  return maxTraceDistance;
}

void raycastJumping(float3 startPos, float3 reflectDir, inout float3 posS, int steps) {
  float3 foundPos = ssGetPos(posS);
  float L = length(foundPos - startPos);
  [loop]
  for (int j1 = 0; j1 < steps; j1++) {
    float3 newPos = startPos + reflectDir * L;
    posS = ssGetUv(newPos);
    float newDepth = ssGetDepth(posS.xy, 0);
    float3 actualPos = ssGetPos(posS.xy, newDepth);
    L = lerp(L, length(actualPos - startPos), 0.8);
  }
}

bool raycastSimple(inout float3 posS, float3 dirS, float maxTraceDistance, int steps, float mipLevel, float bayer) {
  float3 endPosS = posS + dirS * maxTraceDistance;  
  float3 dp = (endPosS - posS) / (float)steps;  
  float4 rayS = float4(posS + dp * (0.5 + bayer), 0);
	float4 rayStartS = rayS;
  float4 stepS = float4(dp, 0);

  int hitIndex = -1;
  for (int i = 0; i < steps; i += 4) {
    float4 rayPosInTS0 = rayS + stepS * 0;
    float4 rayPosInTS1 = rayS + stepS * 1;
    float4 rayPosInTS2 = rayS + stepS * 2;
    float4 rayPosInTS3 = rayS + stepS * 3;
    float depth3 = txDepth.SampleLevel(samPointClamp, rayPosInTS3.xy, mipLevel).x;
    float depth2 = txDepth.SampleLevel(samPointClamp, rayPosInTS2.xy, mipLevel).x;
    float depth1 = txDepth.SampleLevel(samPointClamp, rayPosInTS1.xy, mipLevel).x;
    float depth0 = txDepth.SampleLevel(samPointClamp, rayPosInTS0.xy, mipLevel).x;

    {
      float thickness = rayPosInTS3.z - depth3;
      hitIndex = thickness >= 0 && thickness < MAX_THICKNESS_SIMPLE ? i+3 : hitIndex;
    }
    {
      float thickness = rayPosInTS2.z - depth2;
      hitIndex = thickness >= 0 && thickness < MAX_THICKNESS_SIMPLE ? i+2 : hitIndex;
    }
    {
      float thickness = rayPosInTS1.z - depth1;
      hitIndex = thickness >= 0 && thickness < MAX_THICKNESS_SIMPLE ? i+1 : hitIndex;
    }
    {
      float thickness = rayPosInTS0.z - depth0;
      hitIndex = thickness >= 0 && thickness < MAX_THICKNESS_SIMPLE ? i+0 : hitIndex;
    }

    if(hitIndex != -1) break;
    rayS = rayPosInTS3 + stepS;
  }

  bool intersected = hitIndex >= 0;
  if (intersected) {
    posS = rayStartS.xyz + stepS.xyz * hitIndex;
    return true;
  }
  return false;
}

bool raycastSimpleBiased(inout float3 posS, float3 dirS, float maxTraceDistance, int steps, float mipLevel, float bias) {
  float3 endPosS = posS + dirS * maxTraceDistance;  
  float3 stepS = endPosS - posS;

  int hitIndex = -1;
  for (int i = 0; i < steps; i += 4) {
    float3 rayPosInTS0 = posS + stepS * pow((i + 0.5) / (float)steps, bias);
    float3 rayPosInTS1 = posS + stepS * pow((i + 1.5) / (float)steps, bias);
    float3 rayPosInTS2 = posS + stepS * pow((i + 2.5) / (float)steps, bias);
    float3 rayPosInTS3 = posS + stepS * pow((i + 3.5) / (float)steps, bias);
    float depth3 = txDepth.SampleLevel(samPointClamp, rayPosInTS3.xy, mipLevel).x;
    float depth2 = txDepth.SampleLevel(samPointClamp, rayPosInTS2.xy, mipLevel).x;
    float depth1 = txDepth.SampleLevel(samPointClamp, rayPosInTS1.xy, mipLevel).x;
    float depth0 = txDepth.SampleLevel(samPointClamp, rayPosInTS0.xy, mipLevel).x;

    {
      float thickness = rayPosInTS3.z - depth3;
      hitIndex = thickness >= 0 && thickness < MAX_THICKNESS_SIMPLE ? i+3 : hitIndex;
    }
    {
      float thickness = rayPosInTS2.z - depth2;
      hitIndex = thickness >= 0 && thickness < MAX_THICKNESS_SIMPLE ? i+2 : hitIndex;
    }
    {
      float thickness = rayPosInTS1.z - depth1;
      hitIndex = thickness >= 0 && thickness < MAX_THICKNESS_SIMPLE ? i+1 : hitIndex;
    }
    {
      float thickness = rayPosInTS0.z - depth0;
      hitIndex = thickness >= 0 && thickness < MAX_THICKNESS_SIMPLE ? i+0 : hitIndex;
    }

    if(hitIndex != -1) break;
  }

  bool intersected = hitIndex >= 0;
  if (intersected) {
    posS = posS + stepS * pow((hitIndex + 0.5) / (float)steps, bias);
    return true;
  }
  return false;
}

bool raycastHiZ(inout float3 posS, float3 dirS, float maxThickness, inout float maxTraceDistance, inout int steps, out bool found) {
  int mipCurrent = 2;
  int mipMax = gMIPsCount;
  int mipBase = 0;
  int mipEnding = mipBase - 1;
  float2 crossStep = float2(dirS.x >= 0 ? 1 : -1, dirS.y >= 0 ? 1 : -1);
  float2 crossOffset = crossStep * CROSS_OFFSET_MULT;
  crossStep = saturate(crossStep);

  bool isBackwardRay = dirS.z < 0;
  float minZ = posS.z;
  float maxZ = posS.z + dirS.z * maxTraceDistance;
  float deltaZ = maxZ - minZ;
  float3 o = posS;
  float3 d = dirS * maxTraceDistance;
  float rayDir = isBackwardRay ? -1 : 1;
  // float mtTemp = 1e9;

  for (; steps != 0 && mipCurrent > mipEnding && posS.z * rayDir <= maxZ * rayDir; --steps) {
    if (steps == 24){
      mipBase = 1;
      mipEnding = 0;
    }

    const float2 cellCount = getCellCount(mipCurrent);
    const float2 oldCellIdx = getCell(posS.xy, cellCount);
    float cell_minZ =  txDepth.SampleLevel(samPointClamp, (oldCellIdx + 0.5) / cellCount, mipCurrent).r;
    // if (cell_minZ < minZ - 0.001 && !isBackwardRay) cell_minZ = maxZ;
    float3 tmpRay = cell_minZ > posS.z && !isBackwardRay ? intersectDepthPlane(o, d, (cell_minZ - minZ) / deltaZ) : posS;
    const float2 newCellIdx = getCell(tmpRay.xy, cellCount);
    float thickness = mipCurrent == mipBase ? (posS.z - cell_minZ) : 0;
    bool crossed = isBackwardRay && cell_minZ > posS.z || thickness > maxThickness || crossedCellBoundary(oldCellIdx, newCellIdx);
    posS = crossed ? intersectCellBoundary(o, d, oldCellIdx, cellCount, crossStep, crossOffset) : tmpRay;
    mipCurrent = crossed ? min((float)mipMax, mipCurrent + 1.) : mipCurrent - 1;
    // mtTemp = 
  }

  maxTraceDistance *= lerpInvSat(posS.z, maxZ, minZ);
  found = posS.z * rayDir <= maxZ * rayDir;
  // return true;
  // return steps == 0;
  return steps < 24;
  // return mipEnding == 0;
}

PS_OUT main(PS_IN pin) {
  // float vQuality = txQualityLayer.SampleLevel(samPoint, pin.Tex, 0);
  float vQuality = txQualityLayer.Load(int3(pin.PosH.xy / 16, 0));
  // vQuality = 0.5;

  // PS_OUT pd;
  // pd.main = vQuality;
  // return pd;

  [branch]
  if (vQuality == 0){
    return (PS_OUT)0;
  }

  bool lowQualityArea = vQuality < 1;
  float4 vReflections1 = txReflections1.SampleLevel(samPoint, pin.Tex, 0);
  float4 vNormal = txNormals.SampleLevel(samPoint, pin.Tex, 0);

  // Silly way of storing SSLR_FORCE flag
  bool forceMode = false;
  if (vReflections1.w < -0.001){
    forceMode = true;
    vReflections1.w = abs(vReflections1.w);
  }

  float bayer = -getBayer(pin.PosH);
  float3 random = txNoise.SampleLevel(samPoint, pin.PosH.xy / 32, 0).xyz;

  float3 posS = float3(pin.Tex, ssGetDepth(pin.Tex));
  float3 startPos = ssGetPos(posS);
  float3 normal = normalize(ssNormalDecode(vNormal.xyz));
  float originDistance = length(startPos);
  float3 startDir = normalize(startPos);
  float3 reflectDir = normalize(reflect(startPos, normal));

  float4 vReflections2 = txReflections2.SampleLevel(samPoint, pin.Tex, 0);
  float blur = pow(vReflections2.w, 3);
  float cameraFacingAttenuation = lerpInvSat(dot(-startDir, reflectDir) + blur * 0.8, 0.6, 0.2);
  float baseQuality = cameraFacingAttenuation;

  #ifndef SSLR_TRACE_EVERYTHING
    [branch]
    if (vReflections1.w * abs(dot(vNormal, 1)) < 0.001 || baseQuality < 0.001){
      return (PS_OUT)0;
    }
  #endif

  // reflectDir.xyz += (random.xyy - 0.5) * (blur * 0.02);
  reflectDir += normal * (pow(bayer, 4) - 0.5) * abs(dot(normal, startDir)) * saturate(blur * 2 - 1) * 0.5;
  reflectDir = normalize(reflectDir);

  // float3 dirSBase = ssGetUv(startPos + reflectDir * 10) - posS;
  // posS += dirSBase * (0.005 * (1 + saturate(dot(reflectDir, gCameraDir))) + 0.001 * originDistance);

  float3 dirSBase = ssGetUv(startPos + reflectDir * originDistance * 0.1) - posS;
  float offsetS = (2 + abs(dot(reflectDir, gCameraDir))) * gStartOffsetMult;
  // posS += dirSBase * offsetS * (1 + min(originDistance, 10) * 1);
  float3 dirS = normalize(dirSBase);

  float dirSxy = 1 / length(dirS.xy);
  if (abs(dirS.x * gSize.z) > abs(dirS.y * gSize.w)){
    posS += gSize.z * dirS * dirSxy;
  } else {
    posS += gSize.w * dirS * dirSxy;
  }

  // PS_OUT r;
  // r.main.rgb = dirS;
  // r.main.w = 1;
  // return r;
  // if (dirS.z < 0) return (PS_OUT)0;

  float maxTraceDistance = findMaxTraceDistance(posS, dirS);
  bool hitFound, needsRefinement;

  #ifdef USE_HIZ
    float maxThickness = dot(normal, posS) < 0 ? 1e6 : abs(dirSBase.z) / 20;
    [branch]
    if (lowQualityArea){
      hitFound = raycastSimple(posS, dirS, maxTraceDistance, 16, 2, bayer);
      needsRefinement = true;
    } else {
      int steps = gTracingSteps;
      needsRefinement = raycastHiZ(posS, dirS, maxThickness, maxTraceDistance, steps, hitFound);
      if (needsRefinement){
        // float3 fixedPosS = posS;
        // if (raycastSimpleBiased(fixedPosS, dirS, maxTraceDistance, 8, 2, 2)) posS = fixedPosS;
        // hitFound = raycastSimpleBiased(posS, dirS, maxTraceDistance, 8, 2, 2);
      }
      
      // float taken = 1 - (float)steps / (float)gTracingSteps;
      // PS_OUT R;
      // R.main = float4(taken, 1 - taken, 0, 1);
      // R.dimming = 0;
      // return R;
    }

    // hitFound = raycastSimple(posS, dirS, maxTraceDistance, 80, 1, bayer);
    // needsRefinement = false;
    // needsRefinement = true;
  #else
    hitFound = raycastSimple(posS, dirS, maxTraceDistance, lowQualityArea ? 16 : gTracingSteps, lowQualityArea ? 2 : 0, 0.5 + bayer);
    needsRefinement = true;
  #endif

  if (!hitFound){
    return (PS_OUT)0;
  }

  if (needsRefinement){
    raycastJumping(startPos, reflectDir, posS, 2);
  }

  if (true) {
    float dist = originDistance;
    float DK0 = pow(saturate(dist / 4 - 1), 4);
    float DK1 = pow(saturate(dist - 1), 4);

    float depth = posS.z;
    float newDepth = ssGetDepth(posS.xy);
    float3 newPosition = ssGetPos(posS);
    float3 calculatedPosition = ssGetPos(float3(posS.xy, newDepth));
    float miss = length(calculatedPosition - newPosition);
    float L = length(calculatedPosition - startPos);
    float3 newUv = posS;
    // newUv = ssGetUv(calculatedPosition);

    float3 viewDir = startDir;
    float RdotV = dot(viewDir, reflectDir);
    float NdotV = dot(viewDir, normal);
    float fresnel = saturate(6 * pow(1 + NdotV, 2));
    fresnel = max(fresnel, 1 - pow(saturate(newDepth / depth), 10000));

    float newDist = length(newPosition);  
    float threshold = dist / lerp(100, 10, DK0);
    float thresholdL = dist / 3;
    float thresholdD = dist / 100;

    float qDist = 1 - saturate(miss / threshold - 1);
    float qDistL = 1 - saturate(miss / thresholdL - 1);
    float qDistF = saturate(max(2 - dist / 3, 1.5 - miss / dist));
    float qDistD = 1 - saturate(miss / thresholdD - 1);
    float qDot = dot(normalize(newPosition - startPos), reflectDir);
    float qDir = saturate(qDot * 10 - 9);
    qDir *= saturate(newDist - dist - 1);
    float qRough = saturate(normal.y * 10 - 9);

    float quality = forceMode * qDistF * (1 - pow(dot(normal, viewDir), 2));
    quality = max(quality, qDist);
    quality = max(quality, qDir * qDistL);
    quality = max(quality, qRough * qDistL);
    quality *= saturate(2 - miss / min(dist * 2, 200));
    // quality *= lerpInvSat(dot2(newUv.xy - pin.Tex) * max(1, qDist * 100 - 150), 0.0002, 0.0003);

    #ifdef NO_DIMMING
      float dimQuality = 0;
    #else
      float dimQuality = saturate(2 - L * lerp(1, 0.25, saturate(normal.y)));
      dimQuality = 0.5 + 0.5 * dimQuality;
      quality *= lerp(qDistD, 1, saturate(dimQuality + forceMode));
    #endif
    if (newDepth == 1) quality = 0;
    // if (newDepth == 1) quality = 1;

    float hitDistance = L;
    quality *= lerp(1, lerpInvSat(hitDistance, 200, 100), blur);
    quality *= baseQuality;
    // quality = saturate(quality * 3 - 2);
    // if (newDepth == 1) quality = 1;

    float edgeK = forceMode ? 0.05 : 0.0001;
    float edge = saturate((min(newUv.x, 1 - newUv.x) + edgeK) / edgeK) 
      * saturate((min(newUv.y, 1 - newUv.y) + edgeK) / edgeK);
    L = saturate(pow(saturate(L / lerp(0.3, 10, DK1)), 1));

    // quality = 1;
    // miss = 0;
    // edge = 1;
    // fresnel = 1;
    // dimQuality = 0;
    // if (gEdgeLeft != 0 && newUv.x < gEdgeLeft || gEdgeRight != 1 && newUv.x > gEdgeRight) {
      // quality = 0;
    // }

    PS_OUT R;
    R.main = float4(newUv.xy, L, fresnel * quality * edge);
    R.dimming = max(saturate(1 - qDistD) * dimQuality, saturate(miss / 50 - 1));

    float facingK = saturate(remap(dist, 0.6, 1, 1, 0));
    [branch]
    if (facingK && edge == 1) {
      float facingQ = dot(reflectDir, ssNormalDecode(txNormals.SampleLevel(samPoint, newUv.xy, 0).xyz));
      R.dimming = max(R.dimming, facingK * saturate(facingQ * 10));
    }

    // fix dim objects in distance, especially in fog, without messing up leds reflecting backwards nearby:
    R.dimming *= saturate(remap(newDepth, 0.998, 0.999, 1, 0));
    return R;
  }

  posS.z = ssGetDepth(posS.xy);
  float3 foundPos = ssGetPos(posS);
  float3 deltaPos = foundPos - startPos;
  float hitDistance = length(deltaPos);


  // float2 UVSamplingAttenuation = smoothstep(0, 0.05, posS.xy) * (1 - smoothstep(0.95, 1, posS.xy));
  // UVSamplingAttenuation.x *= UVSamplingAttenuation.y;
  // baseQuality *= UVSamplingAttenuation.x;

  float3 ReflectionNormal = normalize(ssNormalDecode(txNormals.SampleLevel(samPoint, posS.xy, 0).xyz));
  float DirectionBasedAttenuation = smoothstep(-0.17, 0.0, dot(ReflectionNormal.xyz, -reflectDir));
  baseQuality *= DirectionBasedAttenuation;

  baseQuality *= lerp(1, lerpInvSat(hitDistance, 200, 100), blur);

  float maxDistance = max(originDistance, length(foundPos));
  float nearbyEvent = saturate(2 - maxDistance);

  float quality = lerp(saturate(dot(deltaPos / hitDistance, reflectDir) * 8 - lerp(6, 4, saturate(hitDistance))), 1, nearbyEvent) * baseQuality;
  // quality = 1;
  // hitDistance = 0;
  
  PS_OUT R;
  R.main = float4(posS.xy, saturate(hitDistance / 10), quality);
  R.dimming = lerp(1 - quality, 0, saturate(hitDistance - 1));
  if (forceMode){
    R.main.w = lerp(saturate(dot(deltaPos / hitDistance, reflectDir) * 4) * baseQuality, 1, saturate(2 - hitDistance) * saturate(normal.y * 4 - 2));
  }

  return R;
}