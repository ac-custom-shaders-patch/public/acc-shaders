#include "include/common.hlsl"

Texture2D<float> txDiffuse : register(t0);

cbuffer cbSSLRNewData : register(b11){
  float2 gRatio;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float main(VS_Copy pin) : 
#ifdef BE_HONEST
  SV_TARGET
#else
  SV_DEPTH
#endif 
{
  // float4 r = txDiffuse.GatherRed(samPoint, pin.Tex);

  float4 r;
  int2 u = int2(ceil(pin.PosH.xy * 2));
  r.x = txDiffuse.Load(int3(u - int2(0, 0), 0));
  r.y = txDiffuse.Load(int3(u - int2(1, 0), 0));
  r.z = txDiffuse.Load(int3(u - int2(1, 1), 0));
  r.w = txDiffuse.Load(int3(u - int2(0, 1), 0));

  float t = min(min(r.x, r.y), min(r.z, r.w));
  if (gRatio.y > 2) {
    t = min(t, txDiffuse.Load(int3(u - int2(0, -1), 0)));
    t = min(t, txDiffuse.Load(int3(u - int2(1, -1), 0)));
    if (gRatio.x > 2) t = min(t, txDiffuse.Load(int3(u - int2(-1, -1), 0)));
  }
  
  if (gRatio.x > 2) {
    t = min(t, txDiffuse.Load(int3(u - int2(-1, 0), 0)));
    t = min(t, txDiffuse.Load(int3(u - int2(-1, 1), 0)));
  }

  return t;
}