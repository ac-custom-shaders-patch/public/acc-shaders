#include "include/common.hlsl"

TextureCube txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10){
  float4x4 gRotate;
  float4 gHDRToLDRHint;
  float gMip;
  float gBrightness;
}

bool raySphereIntersect(float3 r0, float3 rd, float3 s0, float sr, out float distance){
  float3 oc = r0 - s0;
  float a = dot(rd, rd);
  float b = 2 * dot(oc, rd);
  float c = dot(oc, oc) - sr*sr;
  float discriminant = b * b - 4 * a * c;
  distance = -(b + sqrt(abs(discriminant))) / (2 * a);
  return discriminant > 0 && (distance > 0 || dot2(s0 - r0) < sr * sr);
}

float tonemap(float x, float P, float a, float m, float l, float c, float b) {
  float l0 = ((P - m) * l) / a;
  float L0 = m - m / a;
  float L1 = m + (1 - m) / a;
  float S0 = m + l0;
  float S1 = m + a * l0;
  float C2 = (a * P) / (P - S1);
  float CP = -C2 / P;
  float w0 = 1 - smoothstep(0, m, x);
  float w2 = step(m + l0, x);
  float w1 = 1 - w0 - w2;
  float T = m * pow(x / m, c) + b;
  float S = P - (P - S1) * exp(CP * (x - S0));
  float L = m + a * (x - m);
  return T * w0 + L * w1 + S * w2;
}

float tonemap(float x) {
  const float P = 1.0;  // max display brightness
  const float a = 1.0;  // contrast
  const float m = 0.22; // linear section start
  const float l = 0.4;  // linear section length
  const float c = 1.33; // black
  const float b = 0.0;  // pedestal
  return tonemap(x, P, a, m, l, c, b);
}

float4 main(VS_Copy pin) : SV_TARGET {
  pin.Tex.y = 1 - pin.Tex.y;

  float distance;
  if (!raySphereIntersect(float3(pin.Tex, 0), float3(0, 0, 1), 0.5, 0.5, distance)){
    discard;
  }

  float3 spherePos = float3(pin.Tex, distance);
  float3 sphereDir = spherePos - 0.5; 
  float3 reflected = normalize(reflect(float3(0, 0, 1), normalize(sphereDir)));
  reflected = mul(reflected, (float3x3)gRotate);

  float4 v = max(0, txDiffuse.SampleLevel(samLinearSimple, reflected, gMip));
  // v.xyz = v.xyz / (v.xyz + 1);
  v.xyz = pow(max(0, v.xyz * gHDRToLDRHint.x), gHDRToLDRHint.y) * gBrightness;
  v.xyz = max(v.xyz, 0);
  v.xyz = float3(tonemap(v.r), tonemap(v.g), tonemap(v.b));
  return v;
}