#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);
Texture2D txNoise : register(TX_SLOT_NOISE);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10){
  float gStep;
  float3 gPad0;
}

float3 samplePoint(float2 uv){
  return txDiffuse.SampleLevel(samLinearClamp, uv, 0).xyz;
}

#include "include/poisson.hlsl"
float4 main(VS_Copy pin) : SV_TARGET {
  float4 currentShot = 0;

  float radius = 0.02;
  if (gStep == 2) radius = 0.04;
  else if (gStep == 3) radius = 0.08;
  else if (gStep == 4) radius = 0.16;

  #define DISK_SIZE 25
  for (uint i = 0; i < DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(DISK_SIZE, i);
    float3 col = samplePoint(pin.Tex + offset * radius * float2(1, 16./9));
    currentShot += float4(col, 1) * (1 + dot(col, 10));
  }

  return currentShot / currentShot.w;
}
