#include "include/common.hlsl"

Texture2D txColor : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  int2 p = 2 * (int2)pin.PosH.xy;
  float4 v0 = txColor.Load(int3(p + int2(0, 0), 0));
  float4 v1 = txColor.Load(int3(p + int2(1, 0), 0));
  float4 v2 = txColor.Load(int3(p + int2(1, 1), 0));
  float4 v3 = txColor.Load(int3(p + int2(0, 1), 0));
  #ifdef GAMMA_FIX
    return pow(pow(v0, 0.5) + pow(v1, 0.5) + pow(v2, 0.5) + pow(v3, 0.5), 2) / 16;
  #endif
  return (v0 + v1 + v2 + v3) / 4;
  // float4 currentShot = 0;
  // float totalWeight = 0;
  // [unroll]
  // for (int x = -R; x <= R; x++)
  // [unroll]
  // for (int y = -R; y <= R; y++){
  //   if (abs(x) + abs(y) == R + R) continue;
  //   // float weight = 1 / (1 + length(float2(x, y)));
  //   float weight = 1;
  //   currentShot += txPreviousMap.SampleLevel(samLinearClamp, pin.Tex, 0, int2(x, y) * 2) * weight;
  //   totalWeight += weight;
  // }
  // return currentShot / totalWeight;
}