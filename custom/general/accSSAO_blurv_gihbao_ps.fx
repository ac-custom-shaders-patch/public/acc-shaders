#include "accSS_cbuffer.hlsl"

// #define MIPMAPPED_GBUFFER 4
#define BLUR_TEX txSsaoFirstStep
Texture2D txSsaoFirstStep : register(TX_SLOT_SS_ARG0);

#include "accSS.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  float4 res = ppBlur(pin.Tex, float2(0, 1));
  res.x = ssaoOpacity; 
  res.yzw = pow(max(res.yzw, 0), 1.2) * gSSGIIntensity; 
  return res;
}