#include "accSS.hlsl"

Texture2D<float> txHeatingMask : register(TX_SLOT_COLOR);

cbuffer cbDataExt : register(b11){
  float4x4 gTransform;
}

float sampleMask0(float3 posW){
  float4 posH = mul(float4(posW, 1), gTransform);
  return any(abs(posH.xy - 0.5) > 0.5) ? 1 : txHeatingMask.SampleCmpLevelZero(samShadow, posH.xy, posH.z);
}

float sampleMask1(float3 posW){
  float4 posH = mul(float4(posW, 1), gTransform);
  if (any(abs(posH.xy - 0.5) > 0.5)) return 1;
  float height = -(posH.z - txHeatingMask.SampleLevel(samPoint, posH.xy, 0)) * 60;
  return saturate(height);
}

float main(VS_Copy pin) : SV_TARGET {
  float3 origin = ssGetPos(pin.Tex, ssGetDepth(pin.Tex));
  float3 dir = normalize(origin);
  float4 randomValue = txNoise.SampleLevel(samPoint, pin.PosH.xy / 32, 0);
  float3 posBase0 = ksCameraPosition.xyz - float3(0, lerp(1, 8, randomValue.x), 0);
  float3 posBase1 = ksCameraPosition.xyz - float3(0, lerp(1, 8, randomValue.y), 0);

  // float ret = 0;
  // for (int i = 0; i < 10; ++i){
  //   ret += sampleMask0(posBase0 + dir * 200 * (0.5 + (1 + i + randomValue.z) / 10.));
  //   ret += sampleMask0(posBase1 + dir * 200 * (0.5 + (1 + i + randomValue.z) / 10.));
  // }
  // return ret / 20;

  float ret = 0;
  for (int i = 0; i < 10; ++i){
    ret += sampleMask1(ksCameraPosition.xyz  + dir * 200 * (0.5 + (1 + i + randomValue.z) / 10.));
  }
  return ret / 10;

  // return sampleMask(ksCameraPosition.xyz);

  // return frac(ksCameraPosition.xyz + dir * 200).y;

}