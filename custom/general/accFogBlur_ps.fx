#include "accSS.hlsl"
#include "include_new/base/cbuffers_ps.fx"

Texture2D<float> txMirageMask : register(TX_SLOT_SS_ARG0);
Texture2D<float> txHeatMask : register(TX_SLOT_SS_ARG1);

float calculateFogNewFn(float3 posC, float3 posN){
  #ifdef USE_PS_FOG
    #error WTF
    return 0;
  #else
    if (GAMMA_FIX_ACTIVE) {
      float fogRecompute = 1 - exp(-length(posC) * ss_ksFogLinear);
      float fog = ss_ksFogBlend * pow(saturate(fogRecompute), ss_extFogExp);
      float4 fog2Color_blend = loadVector(p_fog2Color_blend);
      float yK = posN.y / (ss_extFogConstantPiece_dep + sign(ss_extFogConstantPiece_dep) * abs(posN.y));
      float buggyPart = 1 - exp(-max(length(posC) - 2.4, 0) * pow(2, yK * 5) * p_fog2Linear); 
      float fog2 = fog2Color_blend.w * pow(saturate(buggyPart), p_fog2Exp);
      return lerp(fog, 1, fog2);
    } else {
      return ss_ksFogBlend * pow(saturate(ss_extFogConstantPiece_dep * (1.0 - exp(-posC.y * ss_ksFogLinear)) / posN.y), ss_extFogExp);
    }
  #endif
}

float calculateFogNewFn(float3 posC){
  return calculateFogNewFn(posC, normalize(posC));
}

float4 sampleNoise(float2 uv){
	float textureResolution = 32;
	uv = uv * textureResolution + 0.5;
	float2 i = floor(uv);
	float2 f = frac(uv);
	uv = i + f * f * (3 - 2 * f);
	uv = (uv - 0.5) / textureResolution;
	return txNoise.SampleLevel(samLinearSimple, uv, 0);
}

Texture2DArray<float> txShadowArray : register(TX_SLOT_SHADOW_ARRAY);

#define R 1
float sampleHeatMask(float2 uv) {
  float currentShot = 0;
  float totalWeight = 0;
  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    currentShot += txHeatMask.SampleLevel(samLinearClamp, uv, 2, int2(x, y));
    ++totalWeight;
  }
  return currentShot / totalWeight;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float depth = 1;
  {
    [unroll] for (int x = -2; x <= 2; x+=2)
    [unroll] for (int y = -2; y <= 2; y+=2){
      if (abs(x) + abs(y) == 4) continue;
      float2 offset = float2(x, y);
      float2 newUV = pin.Tex + gSize.zw * float2(x, y);
      depth = min(depth, ssGetDepth(newUV));
    }
  }

  float3 originC = ssGetPos(pin.Tex, 10);
  float3 dirC = normalize(originC);

  float3 origin = ssGetPos(pin.Tex, depth);
  float distance = length(origin);
  float3 dir = origin / distance;
  float beneath = saturate(0.5 - dir.y * 18);
  float beneath2 = saturate(2 - origin.y / 100);
  beneath = max(beneath, beneath2);

  // float fog = max(
  //   pow(saturate(calculateFogNewFn(origin)), 4), 
  //   saturate(distance * fogBlurDistanceInv - 0.5)) * beneath;
  float fog = pow(saturate(calculateFogNewFn(origin, dir)), 4) * beneath;
  float3 bColor = txColor.SampleLevel(samLinear, pin.Tex, 0).rgb;
  float3 vColor = bColor;
  float3 mColor = bColor;

  if (GAMMA_FIX_ACTIVE) {
    fog = sqrt(fog);
  }

  // vColor = float3(fog, 1 - fog, 0);

  {
    // Experiment A: contact shadows
    /* float4 random = txNoise.SampleLevel(samPoint, pin.PosH.xy / 32, 0);
    float3 origin = ssGetPos(pin.Tex, ssGetDepth(pin.Tex));
    float3 normal = ssGetNormal(pin.Tex);
    float ret = saturate(dot(normal, -ksLightDirection.xyz));
    float lightPassed = 0;
    float totalWeight = 0;
    for (float i = 1; i < 10; ++i){
      float3 hitUV = ssGetUv(origin - ksLightDirection.xyz * (i - random.x) * 0.01);
      if (any(hitUV.xy) < 0 || any(hitUV.xy) > 1){
        break;
      }
      float hitDepth = ssGetDepth(hitUV.xy, true);
      float collisionDepth = linearize(hitUV.z) - linearize(hitDepth);
      bool collision = collisionDepth > 0.001 && collisionDepth < 0.5;

      float weight = 1 - pow(i / 10, 2);
      lightPassed += !collision * weight;
      totalWeight += weight;
    }
    return float4(ret.xxx * pow(lightPassed / totalWeight, 4), 1); */
  }

  {
    // Experiment B: using fourth cascade for volumetric light
    /* float distance = length(origin);
    if (distance > 2000) origin *= 2000 / distance;
    float avg = 0;
    float4 random = txNoise.SampleLevel(samPoint, pin.PosH.xy / 32, 0);
    for (int i = 1; i < 20; ++i){
      float3 pos = origin * lerp(0.1, 1, sqrt((i + random.x) / 21));
      float4 uv = mul(float4(pos + ksCameraPosition.xyz, 1), ksShadowMatrix0);
      uv.xyz = uv.xyz * shadowsMatrixModifier3M + shadowsMatrixModifier3A;
      avg += txShadowArray.SampleCmpLevelZero(samShadow, float3(uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5), 3), uv.z);
    }
    return float4(bColor * pow(avg / 20, 2), 1); */
  }

  #ifdef USE_HEATING
    float3 heatPoint = dir * 200 * lerp(1, extCameraTangent * 0.01, 0.7) + ksCameraPosition.xyz + float3(0, -ksGameTimeS * 0.4, 0);
    heatPoint += float3(gWindOffset.x, 0, gWindOffset.y) * 0.1;

    float3 dirUp = float3(0, 1, 0);
    float3 dirSide = normalize(float3(dir.z, 0, -dir.x)); 

    float noisePosY = dot(heatPoint, dirUp);
    float noiseScale = 0.3;
    float4 noise0 = sampleNoise(float2(heatPoint.x, noisePosY) * noiseScale);
    float4 noise1 = sampleNoise(float2(heatPoint.z, noisePosY) * noiseScale);
    float4 noise = lerp(noise0, noise1, abs(dirSide.z));

    float heatK = saturate(distance / 100 - 1) * noise.z
      * lerp(1, 0.5, txMirageMask.SampleLevel(samLinearSimple, pin.Tex, 0))
      * (1 - sampleHeatMask(pin.Tex));
    // return sampleHeatMask(pin.Tex);
    pin.Tex.xy += (noise.xy * 2 - 1) * gTrackHeatFactor * 0.000015 / max(0.01, fwidth(noisePosY)) * heatK * 1.5;

    // return float4(noise.xyz, 1);
    // return sampleHeatMask(pin.Tex);
    // return heatK;
    // return txMirageMask.SampleLevel(samLinearSimple, pin.Tex, 0);
    // return txDepth.SampleLevel(samPointClamp, pin.Tex, 0).x;

    fog = max(fog, heatK * (1 + noise.z));
  #endif

  [branch]
  if (fog <= 0.1){
    return float4(vColor, 1);
  }

  {
    // [unroll] for (int x = -2; x <= 2; x+=2)
    // [unroll] for (int y = -2; y <= 2; y+=2){
    //   if (x == 0 && y == 0 || abs(x) + abs(y) == 4) continue;
    //   float2 newUV = pin.Tex + gSize.zw * float2(x, y) * fog;
    //   float3 c = txColor.SampleLevel(samLinearClamp, newUV, 0).rgb;
    //   vColor += c;
    //   mColor = max(mColor, c);
    // }

    static float2 poissonDisk[10] = {
      float2(-0.2027472f, -0.7174203f),
      float2(-0.4839617f, -0.1232477f),
      float2(0.4924171f, -0.06338801f),
      float2(-0.6403998f, 0.6834511f),
      float2(-0.8817205f, -0.4650014f),
      float2(0.04554421f, 0.1661989f),
      float2(0.1042245f, 0.9336259f),
      float2(0.6152743f, 0.6344957f),
      float2(0.5085323f, -0.7106467f),
      float2(-0.9731231f, 0.1328296f)
    };
    
    for (int i = 0; i < 10; i++){
      float2 offset = poissonDisk[i];
      // float2 randomDirection = reflect(poissonDisk[i], random);
      float2 newUV = pin.Tex + gSize.zw * offset * fog * 2;
      // float3 c = txColorToBlur.SampleLevel(samLinearClamp, newUV, 0).rgb;
      float3 c = txColor.SampleLevel(samLinearClamp, newUV, 0).rgb;
      vColor += c;
      mColor = max(mColor, c);
    }
    
    // [unroll] for (int x = -2; x <= 2; x+=2)
    // [unroll] for (int y = -2; y <= 2; y+=2){
    //   if (x == 0 && y == 0 || abs(x) + abs(y) == 4) continue;
    //   float2 newUV = pin.Tex + gSize.zw * float2(x, y) * fog;
    //   float3 c = txColor.SampleLevel(samLinearClamp, newUV, 0).rgb;
    //   vColor += c;
    //   mColor = max(mColor, c);
    // }
  }

  // return float4(vColor / 21, 1);
  return float4(vColor / 11, 1);
  // return float4(lerp(vColor / 21, bColor, saturate(dot(mColor, 0.33) - 0.5)) , 1);
  // return float4(lerp(vColor / 5, bColor, saturate(dot(mColor, 0.33) - 0.5)) , 1);
}