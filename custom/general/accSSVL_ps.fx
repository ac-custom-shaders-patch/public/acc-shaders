/* DOESN’T WORK */
/* THINK OF HOW TO TRACE CONE FIRST */
/* WHEN EXIT POINT GOES TO INFINITY */
/* PLUS, IT DOESN’T WORK WITH SMOKE */

#define ksDiffuse 1
#define LIGHTINGFX_SIMPLEST
#define POS_CAMERA posC

#include "include_new/base/common_ps.fx"

#define MIPMAPPED_GBUFFER 2
#define MIPMAPPED_GBUFFER_FIXED
#include "accSS.hlsl"

#include "include_new/base/_gamma.fx"
#include "include_new/ext_lightingfx/_include_vl_ps.fx"

struct VS_Copy2 {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
  noperspective float2 AbsTex : TEXCOORD1;
};

#define SECTOR_LIGHTS 32
#define SECTORS_WIDTH 4
#define SECTORS_HEIGHT 4
#define MAX_LIGHTS 64

struct sSector {
  uint4 mLightIndices[SECTOR_LIGHTS];
};

struct sLight {
  float3 pos;
  float rangeX;
  float3 dirW;
  float spotCosS0;
  float3 upW;
  float trim;
  float3 color;
  float rangeY;

  float4 coneA;
  float4 coneB;
};

cbuffer _cbExtLighting : register(b9) {
  sLight gLights[MAX_LIGHTS + 1];
  sSector gSectors[SECTORS_WIDTH * SECTORS_HEIGHT];
}

float isInsideOfCone(float3 ro, float3 pa, float3 pb, float ra, float rb) {
  float3 ba = pb - pa;
  float3 oa = ro - pa;
  float3 ob = ro - pb;

  float rba = rb - ra;
  float m0 = dot(ba, ba);
  float papa = dot(oa, oa);
  float paba = dot(oa, ba)/m0;

  float x = sqrt(papa - paba * paba * m0);

  float cax = max(0.0, x - (paba < 0.5 ? ra : rb));
  float cay = abs(paba - 0.5) - 0.5;

  float k = rba * rba + m0;
  float f = saturate((rba * (x - ra) + paba * m0) / k);

  float cbx = x - ra - f * rba;
  float cby = paba - f;    
  return cbx < 0.0 && cay < 0.0;
}

float iCappedCone(float3 ro, float3  rd, float3 pa,float3 pb, float ra, float rb) {
  if (isInsideOfCone(ro, pa, pb, ra, rb)) return 0;

  float3 ba = pb - pa;
  float3 oa = ro - pa;
  float3 ob = ro - pb;
  
  float m0 = dot(ba, ba);
  float m1 = dot(oa, ba);
  float m2 = dot(ob, ba); 
  float m3 = dot(rd, ba);

  if (m2 > 0.0) { 
    if (dot2(ob * m3 - rd * m2) < rb * rb * m3 * m3) {
      return -m2/m3;
    }
  }
  
  float m4 = dot(rd, oa);
  float m5 = dot(oa, oa);
  float rr = ra - rb;
  float hy = m0 + rr*rr;
  
  float k2 = m0 * m0 - m3 * m3 * hy;
  float k1 = m0 * m0 * m4 - m1 * m3 * hy + m0 * ra * (rr * m3);
  float k0 = m0 * m0 * m5 - m1 * m1 * hy + m0 * ra * (rr * m1 * 2.0 - m0 * ra);
    
  float h = k1 * k1 - k2 * k0;
  if (h < 0.0) return -1.0;

  float t = (-k1 - sqrt(h)) / k2;
  float y = m1 + t * m3;

  if (t >= 0 && y > 0.0 && y < m0) {
    return t;
  }
    
  return -1.0;
}

float calculateFogNewFn(float3 posC, float3 posN){
  #ifdef USE_PS_FOG
    #error WTF
    return 0;
  #else
    if (GAMMA_FIX_ACTIVE) {
      float fogRecompute = 1 - exp(-length(posC) * ss_ksFogLinear);
      float fog = ss_ksFogBlend * pow(saturate(fogRecompute), ss_extFogExp);
      float4 fog2Color_blend = loadVector(p_fog2Color_blend);
      float yK = posN.y / (ss_extFogConstantPiece_dep + sign(ss_extFogConstantPiece_dep) * abs(posN.y));
      float buggyPart = 1 - exp(-max(length(posC) - 2.4, 0) * pow(2, yK * 5) * p_fog2Linear); 
      float fog2 = fog2Color_blend.w * pow(saturate(buggyPart), p_fog2Exp);
      return lerp(fog, 1, fog2);
    } else {
      return ss_ksFogBlend * pow(saturate(ss_extFogConstantPiece_dep * (1.0 - exp(-posC.y * ss_ksFogLinear)) / posN.y), ss_extFogExp);
    }
  #endif
}

float calculateFogNewFn(float3 posC){
  return calculateFogNewFn(posC, normalize(posC));
}

float3 calculateLight_iter(sLight L, float3 posC, float3 look, float2 cone){
  float loopK = clamp(dot(normalize(L.dirW), look) * 4, -1, 1);
  float loopFix = 0.5 + 0.5 * loopK;

  float size = max(cone.y - cone.x, 0.01);
  float step = min(1, max(size / 10, 0.1));
  step = max(step, size / 20);
  step = step / size;
  step = lerp(step, 0.3 / size, saturate(loopK) * saturate(10 - cone.x) * saturate(size / 25));

  float from = max(0, lerp(0, 1 - step * 10, loopFix));
  float lim = min(1, lerp(step * 10, 1, loopFix));

  float3 r = 0;
  for (float x = from; x < lim; x += step){
    float sampleDistance = lerp(cone.x, cone.y, x);
    float3 posC2 = sampleDistance * look;
    float4 toLight = normalizeWithDistance(L.pos - posC2);
    float spotK = getSpotCone(L.dirW, toLight.xyz, L.spotCosS0);
    float attenuation = saturate(1 - toLight.w * L.rangeY);
    float edge = getEdge(L.upW, toLight.xyz, L.trim).x;
    float shadow = pow(attenuation, 1.4) * GAMMA_LINEAR_SIMPLE(pow(spotK, 4) * edge);
    shadow *= 1 + pow(saturate(1.02 - toLight.w * L.rangeY), 60) * 4;
    // r += L.color;
    r += L.color * shadow * (1 + abs(loopK) * 0.5) * saturate(sampleDistance - 1) * saturate(2 - sampleDistance / 100);
  }
  
  return r;
}

float3 calculateLight(sLight L, float3 posC, inout float coneNearMin){
  float distance = length(posC);
  float3 look = posC / distance;
  float coneNear = iCappedCone(0, look, L.coneA.xyz, L.coneB.xyz, L.coneA.w, L.coneB.w).x;
  [branch]
  if (coneNear < 0){
    return 0;
  } else {
    float oppositePoint = min(distance * 10, 320);
    float3 farPoint = look * oppositePoint;
    float2 cone = float2(coneNear,
      iCappedCone(farPoint, -look, L.coneA.xyz, L.coneB.xyz, L.coneA.w, L.coneB.w).x);

    coneNearMin = min(coneNearMin, coneNear);
    cone.y = oppositePoint - cone.y;
    cone.x = min(cone.x, distance);
    cone.y = min(cone.y, distance);

    return calculateLight_iter(L, posC, look, cone);
  }
}

float4 main(VS_Copy2 pin) : SV_TARGET {
  float depth = ssGetDepth(pin.Tex);
  float3 origin = ssGetPos(pin.Tex, depth);
  float coneNearMin = 100;

  int sectorX = int(pin.AbsTex.x * SECTORS_WIDTH);
  int sectorY = int(pin.AbsTex.y * SECTORS_HEIGHT);
  int sectorID = sectorY * SECTORS_WIDTH + sectorX;
  sSector sector = gSectors[sectorID];
  float3 lighting = 0;
  for (int i = 0; i < SECTOR_LIGHTS; i++){
    [branch]
    if (!sector.mLightIndices[i].x) break;
    lighting += calculateLight(gLights[sector.mLightIndices[i].x], origin, coneNearMin);
  }

  // return float4(sectorX / 4., sectorY / 4., 0, 1);
  // return float4(frac(origin) * 0.1, 1);
  // return depth;
  // return float4(abs(lighting) / 1, coneNearMin);
  return float4(abs(lighting) / 400, coneNearMin);
  // return float4(pow(abs(lighting) / 200, 0.5) * 1, coneNearMin);
}