#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);
Texture2D txMask1 : register(t1);
Texture2D txMask2 : register(t2);

cbuffer cbData : register(b10) {
  float2 gFrom;
  float2 gTo;
  float2 gTiling;
  uint gMask1Flags;
  uint gMask2Flags;
  float2 gMask1UV1;
  float2 gMask1UV2;
  float2 gMask2UV1;
  float2 gMask2UV2;
  float4 gColorMult;
  float4 gColorOffset;
}

#define MASK_FLAG_USE_COLOR_AVERAGE (1U<<0U)
#define MASK_FLAG_USE_ALPHA (1U<<1U)
#define MASK_FLAG_USE_RED (1U<<2U)
#define MASK_FLAG_USE_GREEN (1U<<3U)
#define MASK_FLAG_USE_BLUE (1U<<4U)
#define MASK_FLAG_USE_INVERTED_ALPHA (1U<<5U)
#define MASK_FLAG_USE_INVERTED_RED (1U<<6U)
#define MASK_FLAG_USE_INVERTED_GREEN (1U<<7U)
#define MASK_FLAG_USE_INVERTED_BLUE (1U<<8U)
#define MASK_FLAG_MIX_COLORS (1U<<9U)
#define MASK_FLAG_MIX_INVERTED_COLORS (1U<<10U)
#define MASK_FLAG_ALT_UV (1U<<16U)

void procMask(inout float4 tex, Texture2D txMask, uint flags, float2 uv, float2 altUV, float2 uv1, float2 uv2){
  if (flags == 0) return;
  if (flags & MASK_FLAG_ALT_UV) uv = altUV;
  // uv = uv * uv1 + uv2;
  uv = lerp(uv1, uv2, uv);
  float4 mask = txMask.Sample(samAnisotropic, uv);
  if (flags & MASK_FLAG_USE_COLOR_AVERAGE) tex.a *= dot(mask.rgb, 1./3.);
  if (flags & MASK_FLAG_USE_ALPHA) tex.a *= mask.a;
  if (flags & MASK_FLAG_USE_RED) tex.a *= mask.r;
  if (flags & MASK_FLAG_USE_GREEN) tex.a *= mask.g;
  if (flags & MASK_FLAG_USE_BLUE) tex.a *= mask.b;
  if (flags & MASK_FLAG_USE_INVERTED_ALPHA) tex.a *= 1 - mask.a;
  if (flags & MASK_FLAG_USE_INVERTED_RED) tex.a *= 1 - mask.r;
  if (flags & MASK_FLAG_USE_INVERTED_GREEN) tex.a *= 1 - mask.g;
  if (flags & MASK_FLAG_USE_INVERTED_BLUE) tex.a *= 1 - mask.b;
  if (flags & MASK_FLAG_MIX_COLORS) tex.rgb *= mask.rgb;
  if (flags & MASK_FLAG_MIX_INVERTED_COLORS) tex.rgb *= 1 - mask.rgb;
}

struct VS_PT {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
  noperspective float2 OrigTex : TEXCOORD1;
  noperspective float OpacityMult : TEXCOORD2;
  noperspective float3 Normal : TEXCOORD3;
};

float4 main(VS_PT pin) : SV_TARGET {
  pin.OpacityMult *= saturate(remap(pin.Normal.z, 0, -0.05, 1, 0));
  // return float4(1, 0, 0, 1);
  if (any((pin.Tex < -1 || pin.Tex > 2) && !gTiling || pin.PosH.xy < gFrom || pin.PosH.xy > gTo) || pin.OpacityMult <= 0) discard;
  if (any((pin.Tex < 0 || pin.Tex > 1) && !gTiling)) return 0;
  if (gTiling.x < 0) { pin.Tex.x = (pin.Tex.x % 2 + 2) % 2; if (pin.Tex.x > 1) pin.Tex.x = 2 - pin.Tex.x; }
  if (gTiling.y < 0) { pin.Tex.y = (pin.Tex.y % 2 + 2) % 2; if (pin.Tex.y > 1) pin.Tex.y = 2 - pin.Tex.y; }
  float4 tex = dot(gTiling, 1) ? txDiffuse.Sample(samAnisotropic, pin.Tex) : txDiffuse.Sample(samAnisotropicClamp, pin.Tex);
  tex = tex * gColorMult + gColorOffset;
  procMask(tex, txMask1, gMask1Flags, pin.Tex, pin.OrigTex, gMask1UV1, gMask1UV2);
  procMask(tex, txMask2, gMask2Flags, pin.Tex, pin.OrigTex, gMask2UV1, gMask2UV2);
  return tex * float4(1, 1, 1, saturate(pin.OpacityMult));
}