#define MODE_MAIN_FX
#define ALLOW_DYNAMIC_SHADOWS
#define POS_CAMERA posC
#define POS_TOCAMERA (-normalW)
#define LIGHTINGFX_SPECULAR_EXP 1
#define LIGHTINGFX_SPECULAR_COLOR 0
#define LIGHTINGFX_SUNSPECULAR_EXP 1
#define LIGHTINGFX_SUNSPECULAR_COLOR 0
#define LIGHTINGFX_TXDIFFUSE_COLOR albedo.rgb
#define SHADOWS_FILTER_SIZE 1
#define NO_SHADOWS_CASCADES_TRANSITION

#include "include/common.hlsl"
#include "include_new/base/_include_ps.fx"

Texture2D txAlbedo : register(t0);
Texture2D txShape : register(t1);

cbuffer cbData : register(b10){
  float4x4 gWorld;
  float3 gAABBMin;
  float gCarAO;
  float3 gAABBMax;
  float gPad1;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 albedo = txAlbedo.SampleLevel(samLinearSimple, pin.Tex, 0);
  float4 shape = txShape.SampleLevel(samLinearSimple, pin.Tex, 0);

  float2 texSigned = pin.Tex * 2 - 1;
  float dirY = 1 - length(texSigned);
  float2 dirXZ = -normalize(texSigned) * sqrt(saturate(1 - dirY * dirY));
  float3 posL = lerp(gAABBMin, gAABBMax, float3(dirXZ.x, dirY, dirXZ.y) * shape.w * float3(1, 2, 1) + float3(0.5, 0, 0.5));
  float3 posW = mul(float4(posL, 1), gWorld).xyz;
  float3 normalW = mul(normalize(shape.xyz * 2 - 1), (float3x3)gWorld);

  float3 posC = posW - ksCameraPosition.xyz;
  float4 shadowPos = mul(float4(posW, 1), ksShadowMatrix0); // TODO: use posW?
  float shadow = getShadow(posC, 1, normalW, shadowPos, gCarAO);
  
  float3 lighting = applyGamma(albedo.rgb, 1) * pow(saturate(dot(normalW, -ksLightDirection.xyz)), GAMMA_OR(6, 4)) 
    * ksLightColor.rgb * saturate(-ksLightDirection.y * GAMMA_OR(10, 5)) * shadow;
  LIGHTINGFX(lighting);
  if (GAMMA_FIX_ACTIVE) {
    lighting *= 0.025;
    lighting = lighting / (1 + lighting);
  }
  return float4(lighting * albedo.w * saturate(-normalW.y + 1), 1);
}