#include "include/common.hlsl"

Texture2D txPrevColor : register(t0);
Texture2D<float> txPrevDepth : register(t1);

cbuffer cbCamera : register(b10) {
  float4 gRegion;
  float2 gUVMargin;
  float gDepthThreshold;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
  noperspective float2 AbsTex : TEXCOORD1;
};

float4 main(VS_Copy pin) : SV_TARGET {
  [branch]
  if (!gDepthThreshold) {
    return txPrevColor.SampleLevel(samLinearClamp, pin.Tex, 0);
  }

  // discard;


  float d = txPrevDepth.SampleLevel(samPointClamp, gRegion.xy + gRegion.zw * clamp(pin.AbsTex, gUVMargin, 1 - gUVMargin), 0);
  // return pow(d, 20);
  if (d < gDepthThreshold) discard; 
  
  // return float4(frac(pin.Tex), 0, 1);
  // return float4(2, 0, 0, 1);

  float2 uv2 = gRegion.xy + gRegion.zw * clamp(pin.Tex, gUVMargin, 1 - gUVMargin);
  float d2 = txPrevDepth.SampleLevel(samPointClamp, uv2, 0);
  if (d2 < gDepthThreshold) discard;

  return txPrevColor.SampleLevel(samLinearClamp, uv2, 0);
}