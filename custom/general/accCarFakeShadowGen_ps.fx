#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float3 pos0;
  float mult;

  float3 posX;
  float pad1;
  
  float3 posY;
  float pad2;

  float4x4 transform;
}

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float main(VS_Copy pin) : SV_TARGET {
  float3 posW = pos0 + posX * (1 - pin.Tex.x) + posY * pin.Tex.y;
  float4 posH = mul(float4(posW, 1), transform);
  return saturate(1 - txDiffuse.SampleLevel(samLinearBorder1, posH.xy, 0));
}