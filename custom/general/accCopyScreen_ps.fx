#include "include/common.hlsl"
#include "include/poisson.hlsl"
#include "include_new/base/cbuffers_common.fx"

#ifdef RESOLVE_MSAA
  Texture2DMS<float4> txNow : register(t0); 
#else
  Texture2D txNow : register(t0);
#endif
Texture2D<float2> txMask : register(t1);
Texture2D txHistory : register(t3);
Texture2D<float2> txMotion : register(t4);
TextureCube txCube : register(TX_SLOT_REFLECTION_CUBEMAP);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

// cbuffer cbCamera : register(b0) {
//   float4x4 ksView;
//   float4x4 ksProjection;
//   float4x4 ksMVPInverse;
//   float4 ksCameraPosition;
//   float ksNearPlane;
//   float ksFarPlane;
//   float ksFOV;
//   float ksDofFactor;
//   float4 extScreenSize;
// }

// cbuffer cbLighting : register(b2) {  
//   float4 ksLightDirection;
//   float4 ksAmbientColor_sky;
// }

cbuffer cbData : register(b10) {
  uint gUseRainMask_gSamplesCount;
  float gWeightBaseK;
  float2 gScreenSize;
  float4x4 gViewProjInv;
}

#define gUseRainMask ((gUseRainMask_gSamplesCount >> 16) & 0xff)
#define gSamplesCount (gUseRainMask_gSamplesCount & 0xff)

float3 ssGetPos(float2 uv, float depth){
  // float4 p = mul(float4(uv.xy, depth, 1), ksMVPInverse);
  float4 p = mul(float4(uv.xy, depth, 1), gViewProjInv);
  return p.xyz / p.w;
}

float2 sampleMask(float2 uv){
  POISSON_AVG(float2, result, txMask, samLinearClamp, uv, 0.005, 8);
  result.x = saturate(result.x * 10 - 9);
  return result;
}

float4 loadNow(float2 uv, int2 coords, int2 offset){
  #ifdef RESOLVE_MSAA
    return txNow.Load(coords.xy, 0, offset);
  #else
    // return txNow.Load(int3(uv, 0), offset);
    return txNow.SampleLevel(samLinearSimple, uv, 0, offset);
  #endif
}

#ifdef GAMMA_FIX
#define WEIGHT_BASE_K GAMMA_OR(gWeightBaseK, 1.)
float3 hdr_tonemap(float3 x){ return x / (x + WEIGHT_BASE_K); }
float3 hdr_inverseTonemap(float3 x){ return WEIGHT_BASE_K * x / max(1 - x, 0.00001); }
#else
float3 hdr_tonemap(float3 x){ return x; }
float3 hdr_inverseTonemap(float3 x){ return x; }
#endif
float4 hdr_tonemap(float4 x){ return float4(hdr_tonemap(x.rgb), x.a); }
float4 hdr_inverseTonemap(float4 x){ return float4(hdr_inverseTonemap(x.rgb), x.a); }

float4 loadNow_avd(float2 uv, int2 coords){
  #ifdef RESOLVE_MSAA
    // return txNow.Load(coords.xy, 0);
    float4 r = 0;
    for (uint i = 0; i < gSamplesCount; ++i){
      r += hdr_tonemap(txNow.Load(coords.xy, i));
    }
    r /= gSamplesCount;
    return hdr_inverseTonemap(r);
  #else
    return txNow.SampleLevel(samLinearSimple, uv, 0);
  #endif
}

float3 tonemap(float3 raw){
  // return raw;
  return raw / (1 + raw * GAMMA_OR(1. / extMainBrightnessMult, 0.02));
}

float3 tonemapInv(float3 t) {
  // return t;
  return -t / (t * GAMMA_OR(1. / extMainBrightnessMult, 0.02) - 1);
}

float4 main(VS_Copy pin) : SV_TARGET {
  int2 uv = int2(pin.Tex * gScreenSize);

  #ifdef APPLY_TAA_FIX
    float4 direct = loadNow(pin.Tex, uv, 0);

    float4 nbh[9];
    nbh[0] = loadNow(pin.Tex, uv, int2(-1, -1));
    nbh[1] = loadNow(pin.Tex, uv, int2(0, -1));
    nbh[2] = loadNow(pin.Tex, uv, int2(1, -1));
    nbh[3] = loadNow(pin.Tex, uv, int2(-1, 0));
    nbh[4] = direct;
    nbh[5] = loadNow(pin.Tex, uv, int2(1, 0));
    nbh[6] = loadNow(pin.Tex, uv, int2(-1, 1));
    nbh[7] = loadNow(pin.Tex, uv, int2(0, 1));
    nbh[8] = loadNow(pin.Tex, uv, int2(1, 1));
    float4 nbhMin = nbh[0];
    float4 nbhMax = nbh[0];
    float4 nbhAvg = 0;
    float4 nbhBlur = 0;
  
    [unroll]
    for (uint i = 1; i < 9; i++) {
      float4 n = nbh[i];
      nbhMin = min(nbhMin, n);
      nbhMax = max(nbhMax, n);
    }

    nbhMin.rgb = tonemap(nbhMin.rgb);
    nbhMax.rgb = tonemap(nbhMax.rgb);
    direct.rgb = tonemap(direct.rgb);

    float2 velocity = txMotion.SampleLevel(samLinear, pin.Tex, 0);
    float speedK = saturate(length(velocity) * 100 - 0.1);
    float2 prevUV = pin.Tex - velocity;
    float4 history = txHistory.SampleLevel(samLinearBorder0, prevUV, 0);
    history.rgb = tonemap(history.rgb);
    float blendFactor = any(abs(prevUV - 0.5) > 0.5) ? 1 : lerp(0.1, 1, speedK);
    // history.rgb = lerp(history.rgb, clamp(history.rgb, nbhMin.rgb, nbhMax.rgb), 0.2);
    // if (dot(history.rgb, 1) > dot(AMBIENT_COLOR_NEARBY, 0.045) && all(history.rgb >= 0 && history.rgb < 1e5)) {
    if (dot(history.rgb, 1) > dot(AMBIENT_COLOR_NEARBY, 0.001) && all(history.rgb >= 0 && history.rgb < 1e5)) {
      history.rgb = clamp(history.rgb, nbhMin.rgb, nbhMax.rgb);
      direct.rgb = lerp(history.rgb, direct.rgb, blendFactor);
    }

    // direct = nbh[8];
    // direct.rgb = tonemap(direct.rgb);
  #else
    float4 direct = loadNow_avd(pin.Tex, uv);
    direct.rgb = tonemap(direct.rgb);
  #endif
  
  direct.a = 1;
  if (!all(direct >= 0 && direct < 1e5)) direct = 0;
  else direct.rgb = tonemapInv(direct.rgb);

  [branch]
  if (gUseRainMask){
    // pin.Tex.x *= 2;
    // if (pin.Tex.x > 1) pin.Tex.x -= 1;

    // float3 pos = -normalize(ssGetPos(pin.Tex * float2(2, -2) + float2(-1, 1), 1000));
    float3 pos = -normalize(ssGetPos(pin.Tex, 1000));
    pos.x = -pos.x;

    float2 mask = sampleMask(pin.Tex); // X: cubemap mask, Y: wipers mask
    float4 fallback = txCube.SampleLevel(samLinearSimple, pos, 0) * 0.8;
    // return float4(mask, 0, 1);
    if (mask.y > 0.3) return float4(AMBIENT_COLOR_NEARBY * 0.04, 1);
    // if (mask.y > 0.3) return fallback * 0.35;
    return gUseRainMask & 1 ? lerp(fallback, direct, saturate(mask.x + mask.y * 10)) : direct;
  } else {
    return direct;
  }
}