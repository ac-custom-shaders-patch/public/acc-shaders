#include "include/common.hlsl"

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#define R 2
float main(VS_Copy pin) : SV_TARGET {
  float currentShot = 0;
  float totalWeight = 0;
  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    currentShot += txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0, int2(x, y) * 2);
    totalWeight++;
  }
  return currentShot / totalWeight;
}