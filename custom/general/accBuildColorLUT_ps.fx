#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

cbuffer cbData : register(b10) {
  float gZ;
  float gResolution;
  float gMult;
  float gStrength;
  uint2 gSrcSize;
  uint gResolutionU;
  float gResolutionInv;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 neutralColor = float4(pin.Tex, gZ * gResolutionInv, 1);
  pin.Tex.x += gZ;
  pin.Tex.x *= gResolutionInv;
  return lerp(neutralColor, txDiffuse.SampleLevel(samPoint, pin.Tex, 0), gStrength) * gMult;
}