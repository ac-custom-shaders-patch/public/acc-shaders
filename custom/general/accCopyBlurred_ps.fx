#include "include/samplers.hlsl"
#include "include_new/base/cbuffers_ps.fx"
#include "include/bicubic.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 getBlurred(Texture2D tex, SamplerState samplerState, float2 uv, float blurMult){
  float4 b = 0;
  [unroll]
  for (int y = -2; y <= 2; y++)
  [unroll]
  for (int x = -2; x <= 2; x++){
    if (abs(x) == 2 && abs(y) == 2) continue;
    b += tex.SampleLevel(samplerState, uv, blurMult, int2(x, y));
  }
  b /= 21;
  return b;
}

float calculateFogNewFn(float3 posC, float3 posN){
  #ifdef USE_PS_FOG
    return 0;
  #else
    if (GAMMA_FIX_ACTIVE) {
      float fogRecompute = 1 - exp(-length(posC) * ksFogLinear);
      float fog = ksFogBlend * pow(saturate(fogRecompute), extFogExp);
      float4 fog2Color_blend = loadVector(p_fog2Color_blend);
      float yK = posN.y / (extFogConstantPiece + sign(extFogConstantPiece) * abs(posN.y));
      float buggyPart = 1 - exp(-max(length(posC) - 2.4, 0) * pow(2, yK * 5) * p_fog2Linear); 
      float fog2 = fog2Color_blend.w * pow(saturate(buggyPart), p_fog2Exp);
      // return 1;
      return lerp(fog, 1, fog2);
    } else {
      if (abs(posN.y) < 0.001) posN.y = 0.001;
      float buggyPart = (1 - exp(-posC.y * ksFogLinear)) / posN.y;
      buggyPart *= extFogConstantPiece;
      return ksFogBlend * pow(saturate(buggyPart), extFogExp);
    }
    // return ksFogBlend * pow(saturate(extFogConstantPiece * (1.0 - exp(-posC.y / ksFogLinear)) / posN.y), extFogExp);
  #endif
}

float calculateFogNewFn(float3 posC){
  return calculateFogNewFn(posC, normalize(posC));
}

float4 main(VS_Copy pin) : SV_TARGET {
  // return float4(pin.Tex, 0, 1);
  float4 v = 0;
  float fogAdj = calculateFogNewFn(float3(400, -200, 0));
  v += txDiffuse.SampleLevel(samLinearClamp, pin.Tex, GAMMA_OR(1.5, 2.5) + fogAdj * 1.4) * (0.2 + fogAdj * 0.5);
  // v += txDiffuse.SampleLevel(samLinearClamp, pin.Tex, GAMMA_OR(5, 7)) * 0.5;
  v += sampleBicubicFixed(txDiffuse, pin.Tex, GAMMA_OR(5, 7)) * 0.5;
  v.rgb *= v.a;
  v.rgb = max(v.rgb, 0);

  return float4(v.rgb * 1000 * GAMMA_OR(0.1, 5), 0.0001);
}