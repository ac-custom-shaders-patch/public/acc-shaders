#include "accSS.hlsl"

Texture2D txPreviousMap : register(TX_SLOT_SS_ARG0);

float4 main(VS_Copy pin) : SV_TARGET {
  float4 currentShot = 0;

  [unroll]
  for (int x = -2; x <= 2; x++)
  [unroll]
  for (int y = -2; y <= 2; y++){
    if (abs(x) + abs(y) == 4) continue;
    currentShot += float4(txColor.SampleLevel(samLinearBorder0, pin.Tex, 0, int2(x, y)).xyz, 1);
  }

  currentShot /= currentShot.w;
  // currentShot = txColor.SampleLevel(samLinear, pin.Tex, 0);

  float4 previousShot = txPreviousMap.SampleLevel(samLinearBorder0, pin.Tex - gMapBlendOffset, 0);
  // float speedK = saturate(length(gMapBlendOffset) * 100000);
  // return lerp(currentShot, previousShot, 0.999);
  // return lerp(currentShot, previousShot, 0.9 + 0.07 * speedK);
  // return lerp(currentShot, previousShot, 0.5 + 0.3 * speedK);
  return lerp(currentShot, previousShot, 0.0);
}