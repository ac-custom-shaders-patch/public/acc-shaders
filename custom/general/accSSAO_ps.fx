#define MIPMAPPED_GBUFFER 3
#define MIPMAPPED_GBUFFER_FIXED
#include "accSS.hlsl"

#define RADIUS 2
#define SUBSQ_RADIUS 1.5
#define VERT_MULTIPLIER 10

float main(VS_Copy pin) : SV_TARGET {
  float depth = ssGetDepth(pin.Tex);
  float depthL = linearize(depth);
  float depthK = saturate(1.5 - depthL / 50);

  [branch]
  if (depthK == 0) return 1;

  float3 origin = ssGetPos(pin.Tex, depth);
  float DM = ssaoRadius;

  float3 normal = ssGetNormal(pin.Tex);
  float3 look = normalize(origin);
  float fixK = pow(saturate(dot(normal, look) + 1), 6);
  normal = lerp(normal, -look, fixK);
  normal = normalize(normal);
  DM *= 2 - abs(normal.y);

  float3 random = normalize(txNoise.SampleLevel(samPoint, pin.PosH.xy / 32, 0).xyz);
  float occlusion = 0.0;
  float inTheOpen = 0;

  for (int i = 0; i < 24; i++) {
    float3 rn = reflect(gSamplesKernel[i].xyz, random);
    float3 dirL = rn * sign(dot(rn, normal));
 
    float2 newUv = ssGetUv(origin + dirL * RADIUS * DM).xy;
    if (any(abs(newUv - 0.5) > float2(0.5, 0.5))) continue;

    float depthS = ssGetDepth(newUv, true);
    float depthSL = linearize(depthS);
    float3 samW = ssGetPos(newUv, depthS);
    float3 diff = samW - origin;
    float diffL = length(diff);
    float3 diffN = diff / diffL;

    float depthD = depthL - depthSL;
    float nearT = lerp(0.1, -0.1, saturate(dot(diffN, normal)));
    // float nearT = lerp(0.01, -0.01, saturate(diffL));
    if (depthD > nearT * DM && depthD < SUBSQ_RADIUS * DM){
      // occlusion++;
      float open = length(diff.xz) / (abs(diff.y) + SUBSQ_RADIUS * DM);
      inTheOpen += open;
      occlusion += 1 - open;
      // occlusion += saturate(1.5 - diffL / 2) * saturate(dot(diffN, normal) * 5 + 2);
    }
  }

  return 1 - depthK * saturate(occlusion / 24) * (2 - saturate(inTheOpen / 16));
}