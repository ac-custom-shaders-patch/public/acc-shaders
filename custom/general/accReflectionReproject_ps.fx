  #include "include/common.hlsl"

// TextureCube txPrev : register(t1);
TextureCube txPrev : register(t3);
Texture2D<float> txDepth : register(t2);
// Texture2D<float> txDepthLinear : register(t2);

cbuffer cbCamera : register(b0) {
  float4x4 ksView;
  float4x4 ksProjection;
  float4x4 ksMVPInverse;
  float4 ksCameraPosition;
  float ksNearPlane;
  float ksFarPlane;
  float ksFOV;
  float ksDofFactor;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10){
  float4x4 transform;
  float3 gOffset;
  float gMip;
}

float3 getNormal(float2 uv){
  float3 vec = float3(1 - uv * 2, -1);
  return mul(vec, (float3x3)transform);
}

// #define R 2
// float minDepth(float2 uv) {
//   float ret = 1;
//   float totalWeight = 0;
//   [unroll]
//   for (int x = -R; x <= R; x++)
//   [unroll]
//   for (int y = -R; y <= R; y++){
//     if (abs(x) + abs(y) == R + R) continue;
//     ret = min(ret, txDepth.SampleLevel(samLinear, uv, 0, int2(x, y)));
//   }
//   return ret;
// }

float linearize(float depth){
  return 2.0 * ksNearPlane * ksFarPlane / (ksFarPlane + ksNearPlane - (2.0 * depth - 1.0) * (ksFarPlane - ksNearPlane));
}

float4 main(VS_Copy pin) : SV_TARGET {
  float3 normal = getNormal(pin.Tex);
  float depthRaw = txDepth.SampleLevel(samLinearSimple, pin.Tex, 0);
  float depth = linearize(depthRaw);
  if (depthRaw < 1 && (depth < 500 || normal.y < 0)){
    normal = normal * depth + gOffset * float3(-1, 1, 1);
  }
  return txPrev.SampleLevel(samLinearSimple, normal, gMip);
}