#include "include/common.hlsl"
#include "include_new/base/_include_vs.fx"

cbuffer cbData : register(b10) {
  float4x4 gWorldPrev;
  float4x4 gVPPrev;
  float gSpeed;
  float gBottomY;
}

struct VS_IN_collider {
  float3 PosL : POSITION;
  uint NormalL : NORMAL_ENCODED;
};

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float MixMin : TEXCOORD1;
};

PS_IN main(VS_IN_collider vin) {
  float3 normalL = normalize(float3(
    BYTE0(asuint(vin.NormalL)) / 255.f,
    BYTE1(asuint(vin.NormalL)) / 255.f,
    BYTE2(asuint(vin.NormalL)) / 255.f) * 2 - 1);

  PS_IN vout;
  float4 posW = mul(float4(vin.PosL + normalL * float3(0.1, 0.05, 0.1) * lerpInvSat(gSpeed, 0.1, 0.2), 1), gWorldPrev);
  float4 posV = mul(posW, ksView);
  vout.PosH = mul(posV, ksProjection);
  float4 posHPrev = mul(posW, gVPPrev);
  vout.Tex = posHPrev.xy / posHPrev.w;
  vout.MixMin = vin.PosL.y < gBottomY;
  return vout;
}
