#include "accSSLR_flags.hlsl"
#include "accSS_cbuffer.hlsl"
#include "include_new/base/samplers_ps.fx"
#include "include/common.hlsl"
#include "include/bayer.hlsl"

Texture2D txNormals : register(TX_SLOT_NORMALS);
Texture2D<float> txDepth : register(TX_SLOT_GDEPTH);
Texture2D txReflections1 : register(TX_SLOT_SS_ARG0);
Texture2D txReflections2 : register(TX_SLOT_SS_ARG1);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float3 ssGetPos(float2 uv, float depth){
  float4 p = mul(float4(uv.xy, depth, 1), viewProjInv);
  return p.xyz / p.w;
}

float3 ssGetPos(float3 posS){
  return ssGetPos(posS.xy, posS.z);
}

cbuffer cbSSLRNewData : register(b11){
  uint2 gScreenResolution;
  float2 gCrossOffsetMult;
  uint gTracingSteps;
  float3 gCameraDir;
  int gMIPsCount;
  float gSSLRScale;
  float gEdgeLeft;
  float gEdgeRight;
}

float4 main(VS_Copy pin) : SV_TARGET {
  // float depth = txDepth.SampleLevel(samLinearClamp, pin.Tex, 0).x;
  // float3 startPos = ssGetPos(pin.Tex, depth);
  // float3 startDir = normalize(startPos);

  float q = 0;
  float scaleInv = 1. / gSSLRScale;
  [unroll]
  for (int i = -8; i <= 7; ++i){
    float r = abs(txReflections1.Load(int3(pin.PosH.x * scaleInv * 16 + i, pin.PosH.y * scaleInv, 0)).w) > 0.001;
    float b = txReflections2.Load(int3(pin.PosH.x * scaleInv * 16 + i, pin.PosH.y * scaleInv, 0)).w < 0.905;

    // float4 vNormal = txNormals.SampleLevel(samPoint, pin.Tex, 0, int2(i, 0));
    // float3 normal = normalize(ssNormalDecode(vNormal.xyz));
    // float3 reflectDir = normalize(reflect(startPos, normal));
    // r *= lerpInvSat(dot(-startDir, reflectDir), 0.6, 0.2) > 0.001;

    q = max(q, r ? b ? 1 : 0.5 : 0);
  }
  return q;
}