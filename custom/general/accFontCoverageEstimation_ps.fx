#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10){
  float4 gOffsets[256/4];
}

float main(VS_Copy pin) : SV_TARGET {
  uint charIndex = uint(pin.PosH.x);
  // float2 uvM = float2((gOffsets[charIndex] + gOffsets[charIndex + 1]) / 2, 0.5);
  // return txDiffuse.SampleBias(samLinear, pin.Tex, -1);

  // if (charIndex > 96) return 1;

  float avg = 0;
  float count = 0;
  [unroll]
  for (int x = 1; x < 4; ++x)
  [unroll]
  for (int y = 1; y < 4; ++y){
    float tx = lerp(gOffsets[charIndex / 4][charIndex % 4], gOffsets[(charIndex + 1) / 4][(charIndex + 1) % 4], (float)x / 4.);
    float ty = lerp(0, 1, y / 4.);
    // if (tx > 1) return 1;
    avg += txDiffuse.SampleBias(samLinearBorder0, float2(tx, ty), -2).r;
    ++count;
  }
  return avg / count;
}