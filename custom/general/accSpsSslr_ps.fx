#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);
Texture2D<float> txDepth : register(t1);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10){
  float4x4 viewProj;
  float4x4 viewProjInv;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float depth = txDepth.SampleLevel(samLinearSimple, pin.Tex, 0);
  if (depth == 0 || depth > 0.99999) return 0;

  float4 posC = mul(float4(pin.Tex, depth, 1), viewProjInv);
  posC.xyz /= posC.w;

  float3 dirX = ddx(posC.xyz);
  float3 dirY = ddy(posC.xyz);
  float3 dirN = normalize(cross(dirY, dirX));
  float3 dir = reflect(normalize(posC.xyz), dirN);

  float offset = 0.2;
  float4 newUV;
  float newDepth;
  float4 newPosReal;
  for (int i = 0; i < 10; ++i){
    float3 newPos = posC.xyz + dir * offset;

    newUV = mul(float4(newPos, 1), viewProj);
    newUV.xyz /= newUV.w;

    newDepth = txDepth.SampleLevel(samLinearSimple, newUV.xy, 0);
    newPosReal = mul(float4(newUV.xy, newDepth, 1), viewProjInv);
    newPosReal.xyz /= newPosReal.w;

    offset = length(newPosReal.xyz - posC.xyz);
  }

  float uvK = min(min(newUV.x, newUV.y), min(1 - newUV.x, 1 - newUV.y));

  float quality = lerpInvSat(abs(dot(normalize(posC.xyz), dirN)), 0.6, 0.4) 
    * lerpInvSat(uvK, 0, 0.1)
    * lerpInvSat(newDepth, 1, 0.999999)
    * lerpInvSat(dot(normalize(newPosReal.xyz - posC.xyz), dir), 0.8, 0.9);

  // if (newDepth == 1 || offset < 0.01 || dot(normalize(newPosReal.xyz - posC.xyz), dir) < 0.9 || any(newUV.xy < 0) || any(newUV.xy > 1)) return 0;

  // quality = 1;
  return float4(txDiffuse.SampleLevel(samLinearClamp, newUV.xy, 0).rgb, quality);

  // return float4(frac(posC.xyz / posC.w), 1);
  // return txDepth.SampleLevel(samLinearSimple, pin.Tex, 0);
  // return 1 - txDiffuse.SampleLevel(samPoint, pin.Tex, 0);
  // return float4(1, 0, 0, 1);
  // return txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);// + dithering(pin.PosH.xy) * 0;
}
