cbuffer cbData : register(b10) {
  float4 gRegion;
  float4 gTex;
  float4 gMask1UV;
  float4 gMask2UV;
  float4 gRegionClip;
  float4 gColorMult;
  float4 gColorOffset;
  uint gMask1Flags;
  uint gMask2Flags;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float2 TexMask1 : TEXCOORD1;
  float2 TexMask2 : TEXCOORD2;
};

VS_Copy main(uint id: SV_VertexID) {
  VS_Copy vout;
  
  float2 offset = float2(id & 1, id >> 1);
  vout.Tex = gTex.xy + gTex.zw * offset;
  vout.TexMask1 = gMask1UV.xy + gMask1UV.zw * offset;
  vout.TexMask2 = gMask2UV.xy + gMask2UV.zw * offset;

  float4 region = gRegion;
  vout.PosH = float4(lerp(offset, region.xy + region.zw * offset, 1) * 2 - 1, 0, 1);
  // vout.PosH = float4(lerp(offset, offset, 1) * 2 - 1, 0, 1);
  return vout;
}
