#include "include/common.hlsl"

Texture2D txDiffuse0 : register(t0);
Texture2D txDiffuse1 : register(t1);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  return float4(
    txDiffuse0.SampleLevel(samLinearSimple, pin.Tex, 0).xyz,
    txDiffuse1.SampleLevel(samLinearSimple, pin.Tex, 0).w
  );
}