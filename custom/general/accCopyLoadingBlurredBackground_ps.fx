#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float2 gMult;
  float gOpacity;
}

// float4 main(VS_Copy pin) : SV_TARGET {
//   return txDiffuse.SampleLevel(samLinearSimple, (pin.Tex * 2 - 1) * gMult * 0.5 + 0.5, 0);
// }

#include "include/poisson.hlsl"
float4 main(VS_Copy pin) : SV_TARGET {
  float4 ret = 0;
  float tot = 0.0001;

  float radius = 0.04;
  float2 uv = (pin.Tex * 2 - 1) * gMult * 0.5 + 0.5;

  float2 uvBase = pin.Tex * 2 - 1;
  // if (abs(uvBase.y) > gMult.x) discard;

  // return txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0);

  #define PUDDLES_DISK_SIZE 32
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * radius;
    float2 sampleUV = uv + offset;
    float4 v = txDiffuse.SampleLevel(samLinearClamp, sampleUV, 4.5);
    float w = 1 + dot(v, 1);
    ret += v * w;
    tot += w;
  }
  ret /= tot;  
  ret.rgb *= 0.5;
  if (gMult.x < 1) ret.rgb *= 1 - pow(saturate(1 - (abs(uvBase.y) - gMult.x) * 4), 2) * 0.5;
  if (gMult.y < 1) ret.rgb *= 1 - pow(saturate(1 - (abs(uvBase.x) - gMult.y) * 4), 2) * 0.5;
  ret.w *= gOpacity;
  return ret;
}