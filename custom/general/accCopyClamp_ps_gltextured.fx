#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_GL pin) : SV_TARGET {
  float4 txDiffuseValue = txDiffuse.Sample(samLinearClamp, pin.Tex);
  return txDiffuseValue * pin.Color;
}
