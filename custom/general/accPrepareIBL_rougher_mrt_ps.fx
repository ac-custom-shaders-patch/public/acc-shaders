#include "accIBL.hlsl"

const static float4x4 transformFirst[6] = {
  transpose(float4x4(0, 0, -1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1)),
  transpose(float4x4(0, 0, 1, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 1)),
  transpose(float4x4(-1, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 1)),
  transpose(float4x4(-1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1)),
  transpose(float4x4(-1, 0, 0, 0, 0, 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1)),
  transpose(float4x4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1))
};

struct PS_OUT {  
  float4 rt0 : SV_Target;
  float4 rt1 : SV_Target1;
  float4 rt2 : SV_Target2;
  float4 rt3 : SV_Target3;
  float4 rt4 : SV_Target4;
  float4 rt5 : SV_Target5;
};

#include "include/poisson.hlsl"

// float4 calculate(float3 vec, float4x4 t){
// 	// return float4(PrefilterEnvMap(getRoughness(), getNormal(pin.Tex)), 1);
//   float3 normal = mul(vec, (float3x3)t);
//   float3 o1 = mul(float3(1, 0, 0) * length(vec.xy) - float3(0, 0, abs(vec.z)), (float3x3)t);
//   float3 o2 = mul(float3(0, 1, 0) * length(vec.xy) - float3(0, 0, abs(vec.z)), (float3x3)t);
//   float4 avg = 0;
//   #define PUDDLES_DISK_SIZE 52
//   for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
//     float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.8;
//     float4 value = max(0, txFirst.SampleLevel(samLinearSimple, normal + o1 * offset.x + o2 * offset.y, (float)mipIndex));
//     tweakSample(value);
//     float weight = 1 / (100 + luminance(value.rgb));
//     // weight *= 1 - SAMPLE_POISSON_LENGTH(PUDDLES_DISK_SIZE, i);
//     avg += float4(value.rgb, 1) * weight;
//   }
//   avg /= avg.w;
//   tweakFinal(avg);
//   return avg;
// }

#include "include/poisson.hlsl"

float4 calculate(float3 vec, float4x4 t){

  // Smoother last MIP:

  float3 normal = mul(vec, (float3x3)t);
  float3 o1 = normalize(cross(normal, float3(0, 1, 0)));
  float3 o2 = normalize(cross(normal, o1));
  float gBlurRadius = 0.9;
  float4 avg = 0;
  for (uint i = 0; i < CUBEMAP_BLUR_SIZE; ++i) {
    float4 dvec = CUBEMAP_BLUR[i];
    {
      float3 vecF = normal * dvec.z + o1 * (dvec.x * gBlurRadius) + o2 * (dvec.y * gBlurRadius);
      float4 value = max(0, txFirst.SampleLevel(samLinearSimple, vecF, (float)mipIndex));
      float weight = 1;
      weight *= dvec.w;
      avg += float4(value.rgb, 1) * weight;
    }
    {
      float3 vecF = normal * dvec.z - o1 * (dvec.x * gBlurRadius * 1.8) - o2 * (dvec.y * gBlurRadius * 1.8);
      float4 value = max(0, txFirst.SampleLevel(samLinearSimple, vecF, (float)mipIndex));
      float weight = 0.7;
      weight *= dvec.w;
      avg += float4(value.rgb, 1) * weight;
    }
  }
  avg /= avg.w;
  return avg;
  
  // From UE4:
	// return float4(PrefilterEnvMap(getRoughness(), mul(vec, (float3x3)t)), 1);
}

PS_OUT main(VS_Copy pin) {
  float3 vec = normalize(float3(1 - pin.Tex * 2, -1));

  float4 r[6];
  [loop]
  for (int i = 0; i < 6; ++i){
    r[i] = calculate(vec, transformFirst[i]);
  }

  PS_OUT ret;
  ret.rt0 = r[0];
  ret.rt1 = r[1];
  ret.rt2 = r[2];
  ret.rt3 = r[3];
  ret.rt4 = r[4];
  ret.rt5 = r[5];
  return ret;
}