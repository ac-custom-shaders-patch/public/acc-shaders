struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

VS_Copy main(uint id: SV_VertexID) {
  VS_Copy vout;
  
  float2 offset = float2(id & 1, id >> 1);
  vout.Tex = offset * float2(0.5, 1);

  float4 region = float4(-1, 1, 2, -2);
  vout.PosH = float4(region.xy + offset * region.zw, 0, 1);
  return vout;
}
