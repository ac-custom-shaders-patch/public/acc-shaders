#include "include/common.hlsl"
#include "include/poisson.hlsl"

Texture2D<float> txEdge : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float4 gColor;
}

float blur(float2 uv) {
  float ret = 0;
  #define PUDDLES_DISK_SIZE 16
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.008 * float2(1, 1.6/0.9);
    ret += txEdge.SampleLevel(samLinearBorder0, uv + offset, 1.5);
  }
  return ret / PUDDLES_DISK_SIZE;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float edge = txEdge.SampleLevel(samLinearSimple, pin.Tex, 0);
  float edgeAverage = blur(pin.Tex);
  edge *= saturate(remap(edgeAverage, 0.4, 0.5, 1, 0));

  // return float4(0, edge, 0, 1);
  return float4(1, 1, 1, pow(edge, 2) * 0.5) * gColor;
}