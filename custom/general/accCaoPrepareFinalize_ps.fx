#include "include/common.hlsl"
#include "include_new/base/cbuffers_common.fx"

Texture2D txColor : register(t0);
Texture2D<float> txMask : register(t1);
Texture2D<float> txHeightMap : register(t2);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 acc = 0;
  for (int i = -10; i <= 10; ++i){
    float2 uv = pin.Tex + float2((float)i * 0.01, 0);
    float4 color = txColor.SampleLevel(samLinearSimple, uv, 0);
    float depth = txHeightMap.SampleLevel(samLinearSimple, uv, 0);
    float mask = txMask.SampleLevel(samLinearSimple, uv, 0);
    float sat = 1 - min(color.r, min(color.g, color.b));
    acc += float4(color.rgb, 1) * (0.05 + mask + depth) / (1 + pow(i, 2));
  }
  return float4(GAMMA_LINEAR(acc.xyz / acc.w), txHeightMap.SampleLevel(samLinearSimple, pin.Tex, 0));
}