#include "include/common.hlsl"

#define GAMMA_FIX_CONDITION true
#include "include_new/base/_gamma.fx"

Texture2D txDiffuse : register(t0);

cbuffer cbData : register(b10) {
  // TfParams gParams;
  float4 _genParams;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 col = txDiffuse.SampleLevel(samLinearClamp, (pin.Tex * 2 - 1) * 0.5 * 0.5 + 0.5, 0);

  if (GAMMA_OR(true, false)) {
    col = pow(max(0, col * _genParams.x), _genParams.y);
  }

  col = col / (1 + col * 0.2);

  float xv = max(col.r, max(col.g, col.b));
  float iv = min(col.r, min(col.g, col.b));
  float3 rem = (col.rgb - iv) / (xv - iv);

  if (rem.b > 0.95 && rem.r < 0.05 && col.b > 0.9 && col.r < 0.8) return 0;

  return col;
  // float it = remap(xv - iv, 0, 0.45 * max(0.1, xv), 0, 1);
  // col.rgb = lerp(col.rgb, dot(col.rgb, 0.1), max(saturate(xv * 3.5 - 2.5), saturate(it * 2 - 1)));
  // // col.rgb = lerp(col.rgb, dot(col.rgb, 0.333), max(saturate(col.b * 2.5 - 1), 0.5 * saturate(it)));
  // // if (iv > 0.95 || xv < 0.05 || xv - iv > 0.45 * max(0.1, xv)) return 0;
  // return float4(col.rgb / ((0.01 + xv) / 1.01), 1);
}