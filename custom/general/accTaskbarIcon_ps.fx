SamplerState samLinearSimple : register(s5) {
  Filter = LINEAR;
  AddressU = WRAP;
  AddressV = WRAP;
};

Texture2D txIcon : register(t0);
Texture2D txBg : register(t1);
Texture2D txBgActive : register(t2);

cbuffer cbData : register(b10) {
  float2 gUvStart;
  float2 gUvSize;
  float gModeScaleDown;
  float gModeModernize;
  float gModeNormalizeAlpha;
  float gModePressed_;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float2 scaleUV(float2 uv, float scale){
  float scaleInv = 1 / scale;
  return uv * scaleInv - (scaleInv - 1) / 2;
}

float4 sampl(float2 uv, float scale){
  float2 uvNew = scaleUV(uv, scale);
  if (any(abs(uvNew - 0.5) > 0.5)) return 0;

  float4 col = txIcon.Sample(samLinearSimple, gUvStart + gUvSize * uvNew);
  return gModeNormalizeAlpha ? float4(col.rgb / col.w, col.w) : col;
}

float4 mix(float4 bg, float4 fg){
  return lerp(bg, fg, fg.a);
}

float4 modernize(float2 uv){
  float2 uvNew = scaleUV(uv, 1.4);
  if (length(uvNew - float2(0.51, 0.51)) > 0.35) return 0;
  float4 tex = sampl(uvNew, 1);
  // return tex;
  return float4(tex.rgb, sqrt(saturate(tex.a * tex.r * 1.2)));
}

struct PS_OUT {
  float4 Inactive : SV_Target0;
  float4 Active : SV_Target1;
};

float4 getValue(float2 uv, bool pressed){
  if (gModeScaleDown){
    return mix(pressed ? float4(1, 0, 0, 1) : 0, sampl(uv, gModeScaleDown));
  } else if (gModeModernize){
    return mix(pressed ? float4(1, 0, 0, 1) : 0, modernize(uv));
  } else {
    float4 icon = txIcon.Sample(samLinearSimple, gUvStart + gUvSize * (uv * 1.8 - 0.4));
    if (any(abs(uv * 1.8 - 0.9) > 0.5)) icon = 0;
    float4 bg = pressed ? txBgActive.Sample(samLinearSimple, uv) : txBg.Sample(samLinearSimple, uv);
    return lerp(bg, icon, icon.a);
  }
}

PS_OUT main(VS_Copy pin) {
  PS_OUT ret;
  ret.Inactive = getValue(pin.Tex, false);
  ret.Active = getValue(pin.Tex, true);
  // ret.Inactive.r = 1;
  // ret.Active.r = 1;
  return ret;
}
