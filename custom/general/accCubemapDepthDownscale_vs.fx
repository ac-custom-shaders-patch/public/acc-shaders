cbuffer cbData : register(b10) {
  float gFace;
  float3 gPad;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float OutputLinear : TEXCOORD1;
};

VS_Copy main(uint id: SV_VertexID) {
  VS_Copy vout;
  
  float2 offset = float2(id & 1, id >> 1);
  vout.Tex = offset;

  float2 start = float2(0.5, 0.5);
  float2 size = float2(0.5, 0.5);
  if (gFace == 2){
    start = float2(0, 0);
    size = float2(1, 1./3);
  } else if (gFace == 3){
    start = float2(0, 2./3);
    size = float2(1, 1./3);
  } else {
    float face = 0;
    if (gFace == 5) face = 1;
    else if (gFace == 1) face = 2;
    else if (gFace == 4) face = 3;
    start = float2(0.25 * face, 1./3);
    size = float2(0.25, 1./3);
  }

  vout.PosH = float4(lerp(start, start + size, offset) * float2(2, -2) + float2(-1, 1), 0, 1);
  vout.OutputLinear = gFace == 3;
  return vout;
}
