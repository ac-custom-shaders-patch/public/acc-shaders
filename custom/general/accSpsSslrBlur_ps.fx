#include "accSS.hlsl"
#define USE_POISSON

Texture2D txPreviousMap : register(t0);

cbuffer cbData : register(b11){
  float gStep;
  float3 gPad0;
}

#include "include/poisson.hlsl"
float4 main(VS_Copy pin) : SV_TARGET {
  float4 ret = 0;
  float tot = 0.0001;

  float radius = 0.004;
  if (gStep == 2) radius = 0.008;
  else if (gStep == 3) radius = 0.016;
  else if (gStep == 4) radius = 0.032;
  else if (gStep == 5) radius = 0.064;
  else if (gStep == 6) radius = 0.128;
  // radius = 0.007;

  #define PUDDLES_DISK_SIZE 16
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * radius;
    float2 sampleUV = pin.Tex + offset;
    float4 v = txPreviousMap.SampleLevel(samLinearClamp, sampleUV, 0);
    float w = 1;
    ret += v * w;
    tot += w;
  }
  ret /= tot;

  if (gStep == 1){
    ret = ret / (1 + dot(ret, 0.05));
  }
  
  return float4(10, 0, 1, 1);
  return ret;
}
