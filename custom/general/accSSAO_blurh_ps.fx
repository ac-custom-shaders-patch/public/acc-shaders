#include "accSS_cbuffer.hlsl"

#ifndef SAMPLE_BLUR_TEX
  #define SAMPLE_BLUR_TEX(UV) float4(BLUR_TEX.SampleLevel(samLinear, UV, 0).x, 0, 0, 0)
#endif

#define BLUR_TEX txSsaoFirstStep
Texture2D<float> txSsaoFirstStep : register(TX_SLOT_SS_ARG0);

#include "accSS.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  // return txSsaoFirstStep.SampleLevel(samPoint, pin.Tex, 0);
  return ppBlur(pin.Tex, float2(1, 0));
}