#include "include/common.hlsl"
#include "include/generate_rain_tyres.hlsl"

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float mult = lerp(0.1, 1, sqrt(1 - abs(pin.Tex.x * 2 - 1)));
  if (pin.Tex.y > 0.5){
    pin.Tex.y = (pin.Tex.y - 0.5) * mult + 0.5;
  } else {
    pin.Tex.y = 0.5 - (0.5 - pin.Tex.y) * mult;
  }

  float ret = 0;
  float tot = 0;
  int R = 8;
  for (int y = 0; y < R; ++y){
    for (int x = 0; x < R; ++x){
      ret += calculateDepth(pin.Tex + (float2(x, y) / R - 0.5) / float2(128, 64));
      ++tot;
    }
  }
  return 1 - ret / tot;
}
