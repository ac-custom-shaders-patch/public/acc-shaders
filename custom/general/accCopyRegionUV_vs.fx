cbuffer cbData : register(b10) {
  float4 gRegion;
  float4 gTex;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

VS_Copy main(uint id: SV_VertexID) {
  VS_Copy vout;
  
  float2 offset = float2(id & 1, id >> 1);
  vout.Tex = gTex.xy + gTex.zw * offset;

  float4 region = gRegion;
  vout.PosH = float4(lerp(offset, region.xy + region.zw * offset, 1) * 2 - 1, 0, 1);
  return vout;
}
