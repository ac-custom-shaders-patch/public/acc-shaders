#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

cbuffer cbData : register(b10) {
  float4 gColor;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  return txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0) * gColor;
}