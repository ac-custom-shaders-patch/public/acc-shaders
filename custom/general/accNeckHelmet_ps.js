[ 0, 1 ].map(samples => ({
  source: `accNeckHelmet_ps.hlsl`,
  target: FX.Target.PS,
  saveAs: `accNeckHelmet_ps_${samples}.fxo`,
  defines: { 'SAMPLES': samples == 0 ? 1 : samples, 'USE_BLUR': samples == 0 ? 1 : 0 }
})).flat()
