#define SS_VIEWLESS
#include "accSS_cbuffer.hlsl"
#include "include_new/base/_gamma.fx"

Texture2D txInput : register(TX_SLOT_COLOR);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

// float3 tonemap(float3 x){ return x / (x + 1); }
// float4 tonemap(float4 x){ return float4(tonemap(x.rgb), x.a); }
// float3 inverseTonemap(float3 x){ return x > 0.9999 ? 0 : x / max(1 - x, 0.00001); }
// float4 inverseTonemap(float4 x){ return float4(inverseTonemap(x.rgb), x.a); }

float3 tonemap(float3 x){ return x; }
float4 tonemap(float4 x){ return float4(tonemap(x.rgb), x.a); }
float3 inverseTonemap(float3 x){ return x; }
float4 inverseTonemap(float4 x){ return float4(inverseTonemap(x.rgb), x.a); }

// #define BASIC_VERSION

float4 main(VS_Copy pin) : SV_TARGET {
  #ifdef BASIC_VERSION
    float kernel[9] = { 
      0.0, -1.0, 0.0,
      -1.0, 5.0, -1.0, 
      0.0, -1.0, 0.0
    };
    int2 offset[9] = { 
      int2(-1.0, -1.0), int2( 0.0, -1.0), int2( 1.0, -1.0),
      int2(-1.0,  0.0), int2( 0.0,  0.0), int2( 1.0,  0.0),
      int2(-1.0,  1.0), int2( 0.0,  1.0), int2( 1.0,  1.0)
    };
    float4 result = 0;
    for (int i = 0; i < 9; i++){
      result += txInput.Load(int3(pin.PosH.xy, 0), offset[i]) * kernel[i];
    }
    return result;
  #else
    // float4 gSize = float4(1920, 1080, 1./1920, 1./1080);
    float4 base = tonemap(txInput.Load(int3(pin.PosH.xy, 0)));
    float4 blurred = 0
      + tonemap(txInput.SampleLevel(samLinearSimple, pin.Tex + gSize.zw * float2(0.5, 0.5), 0))
      + tonemap(txInput.SampleLevel(samLinearSimple, pin.Tex + gSize.zw * float2(0.5, -0.5), 0))
      + tonemap(txInput.SampleLevel(samLinearSimple, pin.Tex + gSize.zw * float2(-0.5, 0.5), 0))
      + tonemap(txInput.SampleLevel(samLinearSimple, pin.Tex + gSize.zw * float2(-0.5, -0.5), 0));
    float4 delta = base - blurred / 4;
    return inverseTonemap(base + (0.36 * saturate(dot(float4(delta.xyz, 0.5), 1)) - 0.18) * (gTAASharpenMult * GAMMA_OR(0.3, 1)));
  #endif
}