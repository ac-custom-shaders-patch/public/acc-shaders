#include "include/common.hlsl"
#include "include/poisson.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 blur(float2 uv) {
  float4 ret = 0;
  #define PUDDLES_DISK_SIZE 32
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.002;
    ret += txDiffuse.SampleLevel(samLinearBorder0, uv + offset, 0);
  }
  return ret / PUDDLES_DISK_SIZE;
}

float edgeBlur(float2 p){
  return saturate((fwidth(dot(1, blur(p).rgb)) - 0.05) * 4);
}

float lookup(float2 p, float2 d) {
  return dot(txDiffuse.SampleLevel(samLinearBorder0, p + d * 0.8 / float2(1920, 1080), 0).rgb, 1);
}

float edgeSobel(float2 p){
  float2 g = float2(0, 0);
  g.x += -1 * lookup(p, float2(-1, -1));
  g.x += -2 * lookup(p, float2(-1,  0));
  g.x += -1 * lookup(p, float2(-1,  1));
  g.x +=  1 * lookup(p, float2( 1, -1));
  g.x +=  2 * lookup(p, float2( 1,  0));
  g.x +=  1 * lookup(p, float2( 1,  1));  
  g.y += -1 * lookup(p, float2(-1, -1));
  g.y += -2 * lookup(p, float2( 0, -1));
  g.y += -1 * lookup(p, float2( 1, -1));
  g.y +=  1 * lookup(p, float2(-1,  1));
  g.y +=  2 * lookup(p, float2( 0,  1));
  g.y +=  1 * lookup(p, float2( 1,  1));  
  return dot(g, g);
}

float edgeNew(float2 p){
  return abs(dot(1, txDiffuse.SampleLevel(samLinearBorder0, p, 0) - txDiffuse.SampleLevel(samLinearBorder0, p, 2)));
}

float noiseMask(float2 p){
  float4 blurred = txDiffuse.SampleLevel(samLinearBorder0, p, 3);
  [unroll]
  for (int y = -2; y <= 2; ++y){
    [unroll]
    for (int x = -2; x <= 2; ++x){
      if (abs(x) + abs(y) == 4) continue;
      float4 sharp = txDiffuse.SampleLevel(samLinearBorder0, p, 0, int2(x, y) * 2);
      if (all(abs(sharp.rgb - blurred.rgb) < 0.02)){
        return 1;
      }
    }
  }
  return false;
}

float main(VS_Copy pin) : SV_TARGET {
  // float edge = edgeBlur(pin.Tex);
  return edgeSobel(pin.Tex);
  // float edge = edgeNew(pin.Tex);
  // float edge = noiseMask(pin.Tex);
  // float edge = edgeNew(pin.Tex) * noiseMask(pin.Tex);
  // return float4(0, edge, 0, 1);
  // return float4(0, 1, 0, edge);
  // float fade = saturate(2 - 2 * abs(pin.Tex.y * 2 - 1)) * saturate(2 - 2 * abs(pin.Tex.x * 2 - 1));
  // return float4(1, 1, 1, saturate(g * 2 - 1) * 0.8 * fade);
  // return float4(1, 1, 1, saturate(2 - 2 * abs(pin.Tex.y * 2 - 1)));
}