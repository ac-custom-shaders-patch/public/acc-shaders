#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float4x4 gVP;
  float4x4 gVPInv;
  float ksNearPlane;
  float ksFarPlane;
  float gReflectiveness;
  float gBlur1;
  float3 ksCameraPosition;
  float _pad;
}

#define gBlur 2

Texture2D txDiffuse : register(t0);
Texture2D<float> txDepth : register(t1);
#define TXDEPTH_DEFINED
#include "include_new/ext_functions/depth_map.fx"  

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

#include "include/poisson.hlsl"
#define PUDDLES_DISK_SIZE 96

float4 samplePoisson(float2 p, float blurLevel){
  // return txDiffuse.SampleLevel(samLinearClamp, p, sqrt(blurLevel) * 2);
  float radius = 0.01 * blurLevel;
  float4 avg = 0;
  float tw = 0;
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * radius;
    float4 s = txDiffuse.SampleLevel(samLinearClamp, p + offset, sqrt(blurLevel) * 2);
    // float w = saturate(s.a * 100);
    float w = 1 ; //dot(s.rgb, 1) + 2;
    avg += s * w;
    tw += w;
  }
  if (tw == 0) discard;
  return avg / tw;
}

float linearizeAccurate2(float depth){
  return 2 * ksNearPlane * ksFarPlane / (ksFarPlane + ksNearPlane - (2 * depth - 1) * (ksFarPlane - ksNearPlane));
}

float computeDepthW(float depth){
  // return 1 - 1 / pow(depth, 4);
  // if (depth == 1) return 0;
  return 1 - pow(saturate(depth), 20);
  return 1;
}

float4 main(VS_Copy pin) : SV_TARGET {
  if (gBlur == 0){
    // if (depth == 1) discard;
    float4 rr = txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0);
    rr.w = gReflectiveness;
    return rr;
  }

  // depth = linearizeAccurate2(depth);

  // float4 p0 = mul(float4(1, 0, 0, 0), gVP);
  // float4 p1 = mul(float4(0, 0, 1, 0), gVP);
  // p0.xyz /= p0.w;
  // p1.xyz /= p1.w;
  // float2 pD = length(p0.xy) > length(p1.xy) ? p0.xy : p1.xy;
  // float2 pDir = normalize(p1.xy - p0.xy);
  // return txDiffuse.SampleLevel(samLinear, pin.Tex + pD * 0.05, 0);

  float depth = txDepth.SampleLevel(samLinearSimple, pin.Tex, 0);
  float2 depthAcc = float2(depth, 1) * computeDepthW(depth);
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * float2(0.1, 0.05) * gBlur;
    float d = txDepth.SampleLevel(samLinearClamp, pin.Tex + offset, 0);
    depthAcc += float2(d, 1) * computeDepthW(d);
  }
  depthAcc += 1e-5;
  depth = depthAcc.x / depthAcc.y;
  
  float4 dir = mul(float4(pin.Tex, depth, 1), gVPInv);
  dir /= max(dir.w, 1e-3);
  
  // float4 dir = mul(float4(pin.Tex, 1, 1), gVPInv);
  // dir /= dir.w;
  // dir.xyz = normalize(dir.xyz) * depth;

  // depth -= length(pin.PosC);

  float3 posW = ksCameraPosition + dir.xyz;
  float belowGround = min(1, -posW.y + 0.02);
  // return float4(frac(posW * 4), 1);
  // if (depth > 0.999) return 1;
  // return frac(depth * 1);
  float4 rr = samplePoisson(pin.Tex, gBlur * 6 * belowGround);
  rr.w = gReflectiveness * lerpInvSat(belowGround, -0.01, 0) * lerpInvSat(belowGround, 3, 1);
  // rr.w = 1;
  return rr;
}
