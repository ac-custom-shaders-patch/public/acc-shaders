#include "accSS.hlsl"

float2 main(VS_Copy pin) : SV_TARGET {
  float3 origin = ssGetPos(pin.Tex, ksFarPlane);
  float2 oldTex = ssGetUv(origin, true).xy;
  return pin.Tex - oldTex;
}