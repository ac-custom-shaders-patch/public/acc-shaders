#include "include/common.hlsl"

Texture2D txColor : register(t0);
Texture2D<float> txMask : register(t1);
Texture2D<float> txHeightMap : register(t2);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 acc = 0;
  for (int i = -10; i <= 10; ++i){
    float2 uv = pin.Tex + float2(0, (float)i * 0.01);
    float4 color = txColor.SampleLevel(samLinearSimple, uv, 0);
    float mask = txMask.SampleLevel(samLinearSimple, uv, 0);
    float depth = txHeightMap.SampleLevel(samLinearSimple, uv, 0);
    float sat = 1 - min(color.r, min(color.g, color.b));
    // acc += float4(color.rgb, 1) * (0.05 + sat * 2 + mask) / (1 + pow(i, 2));
    acc += float4(color.rgb, 1) * (1 + mask) / (1 + pow(i, 2));
  }
  return acc / acc.w;
  // return float4(1, 1, 1, txHeightMap.SampleLevel(samLinearSimple, pin.Tex, 0));
}