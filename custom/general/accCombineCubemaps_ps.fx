#include "include/common.hlsl"
#include "include/samplers.hlsl"
#include "include_new/base/cbuffers_common.fx"

TextureCube txFirst : register(t0);
TextureCube<float> txSecond : register(t1);
Texture2D txNoise : register(TX_SLOT_NOISE);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float4x4 transformFirst;
  float4x4 transformSecond;
  float3 ambientColor;
  float mipLevel;
}

float sampleCubeSingle(float3 p){
  return pow(saturate(txSecond.SampleBias(samLinear, p, 2)), 0.5);
}

float sampleCubeSmooth(float3 p){
  const static float3 gridSamplingDisk[8] = {
    { 1, 1, 1 },
    { 1, 1, -1 },
    { 1, -1, 1 },
    { 1, -1, -1 },
    { -1, 1, 1 },
    { -1, 1, -1 },
    { -1, -1, 1 },
    { -1, -1, -1 },
  };
  float r = 0;
  for (int i = 0; i < 8; i++) {
    r += txSecond.SampleBias(samLinearSimple, p + gridSamplingDisk[i] / 8, 0);
  }
  return r / 8;
}

#include "include/poisson.hlsl"
float sampleCubePoisson(float3 p){
  float radius = 0.04 * pow(2, sqrt(mipLevel));
  #define PUDDLES_DISK_SIZE 6
  float avg;
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * radius;
    avg += txSecond.SampleLevel(samLinearSimple, p + float3(offset, offset.x), mipLevel);
  }
  return avg / PUDDLES_DISK_SIZE;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float3 vec = float3(1 - pin.Tex * 2, -1);
  float3 vecFirst = mul(vec, (float3x3)transformFirst);
  float3 vecSecond = mul(vec, (float3x3)transformSecond);
  float mix = sampleCubePoisson(vecSecond);
  mix = GAMMA_OR(pow(mix, 3), mix);
  // return sampleCubePoisson(vecSecond)
  return lerp(float4(ambientColor, 1), txFirst.Sample(samLinearSimple, vecFirst), mix);
}