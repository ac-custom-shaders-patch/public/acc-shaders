#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);
Texture2D txMask : register(t1);

struct PS_INPUT {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float3 gColor;
  float gUseMask;
}
            
float4 main(PS_INPUT pin) : SV_Target {
  float ret = txDiffuse.Sample(samLinear, pin.Tex).r; 
  if (gUseMask) ret *= txMask.Sample(samLinear, pin.Tex).r;
  return ret * float4(gColor, 1); 
}
