#include "include/common.hlsl"

struct VS_IN_cover {
  float2 PosL : POSITION;
};

struct PS_IN_cover {
  float4 PosH : SV_POSITION;
};

PS_IN_cover main(VS_IN_cover vin) {
  PS_IN_cover vout;  
  vout.PosH = float4(vin.PosL, 0, 1);
  return vout;
}
