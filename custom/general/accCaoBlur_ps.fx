#include "accSS.hlsl"

Texture2D txPreviousMap : register(t0);

#define R 3
float4 main(VS_Copy pin) : SV_TARGET {
  float4 acc = float4(0, 0, 0, 1);
  float tot = 0;

  [unroll]
  for (int x = -2; x <= 2; x++)
  [unroll]
  for (int y = -2; y <= 2; y++){
    if (abs(x) + abs(y) == 4) continue;
    float4 v = txPreviousMap.SampleLevel(samLinearClamp, pin.Tex, 0, int2(x * 2, y * 2));
    acc.rgb += v.rgb;
    acc.w = min(acc.w, v.w);
    tot++;
  }

  acc.rgb /= tot;
  return lerp(acc, 1, 0.1);
  // return txPreviousMap.SampleLevel(samLinearClamp, pin.Tex, 0);;

  // float sat = max(acc.r, max(acc.g, acc.b)) - min(acc.r, min(acc.g, acc.b)); 
  // return float4(lerp(luminance(acc.xyz), acc.xyz, lerp(2, 1.2, sat)), 1);
}