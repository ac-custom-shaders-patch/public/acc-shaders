#include "include/common.hlsl"

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct VS_PT {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
  noperspective float2 OrigTex : TEXCOORD1;
  noperspective float OpacityMult : TEXCOORD2;
  noperspective float3 Normal : TEXCOORD3;
};

cbuffer cbData : register(b10) {
  float4x4 gObject;
  float4x4 gCamera;
  float2 gSize;
  float2 gSkew;
  float2 gUVOffset;
  float2 gOffsetSize;
  float gDoubleSided;
  float gMaxDepth;
}

VS_PT main(VS_IN_ac vin, uint instanceID : SV_InstanceID) {
  VS_PT vout;

  float2 pos = vin.Tex - gUVOffset;
  pos.y = 1 - pos.y;
  vout.PosH = float4(pos * 2 - 1, 0, 1);

  if (instanceID > 1){
    --instanceID;
    vout.PosH.xy += (float2(instanceID / 2, instanceID % 2) * 2 - 1) * gOffsetSize * 2;
  }

  float4 posW = mul(vin.PosL, gObject);
  float4 posH = mul(posW, gCamera);
  vout.Tex = (posH.xy + posH.yx * gSkew) / gSize + 0.5;
  vout.Tex.y = 1 - vout.Tex.y;

  float3 normalW = mul(vin.NormalL, (float3x3)gObject);
  float3 normalH = mul(normalW, (float3x3)gCamera);
  if (gDoubleSided) normalH.z = 1;
  vout.Normal = normalH;

  float distance = -posH.z + 10;
  // float distance = dot2(posH.xyz);
  distance -= instanceID / 2.;
  if (vout.Normal.z < 0) distance += 100;

  // vout.Debug = saturate(instanceID / 4.);
  vout.PosH.z = 1 / distance;
  vout.OpacityMult = remap(-posH.z, gMaxDepth, gMaxDepth * 1.1, 1, 0);
  vout.OrigTex = vin.Tex;

  // if (instanceID == 0) vout.Normal = float3(1, 0, 0);
  // if (instanceID == 1) vout.Normal = float3(0, 1, 0);
  // if (instanceID == 2) vout.Normal = float3(0, 0, 1);
  // if (instanceID == 3) vout.Normal = float3(1, 1, 0);
  // if (instanceID == 4) vout.Normal = float3(0, 0, 0);

  return vout;
}
