#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

cbuffer cbData : register(b10) {
  float2 gMult;
  float gFlippedScale;
  float gPad0;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float2 uvAdj = pin.Tex * 2 - 1;
  uvAdj *= gMult;

  if (gFlippedScale){
    uvAdj = uvAdj.yx;
    uvAdj.y *= -1;
  }

  float4 v = txDiffuse.SampleLevel(samLinearSimple, uvAdj * 0.5 + 0.5, 0);
  return v / v.w;
}