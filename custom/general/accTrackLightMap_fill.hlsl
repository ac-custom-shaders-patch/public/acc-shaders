#include "accSS.hlsl"

Texture2D<float> txSurfaceDepth : register(TX_SLOT_SS_ARG0);

float main(VS_Copy pin) : SV_TARGET {
  float result = txSurfaceDepth.SampleLevel(samPointClamp, pin.Tex, 0);
  float total = 1;
  if (result == 1) {
    result = 0;
    total = 0.001;
  }
  [unroll]
  for (int x = -4; x <= 4; x++){
    [flatten]
    if (x == 0) continue;
    float v = txSurfaceDepth.SampleLevel(samPointClamp, pin.Tex, 0, OFFSET(x));
    if (v < 1) {
      result = max(result, v);
      // result += v;
      // total++;
    }
  }
  return result;
}