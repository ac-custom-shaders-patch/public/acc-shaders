#include "accSS_cbuffer.hlsl"
Texture2D txSsgi : register(TX_SLOT_SS_ARG1);
#define SAMPLE_BLUR_TEX(UV) float4(BLUR_TEX.SampleLevel(samLinear, UV, 0), txSsgi.SampleLevel(samLinear, UV, 2).xyz)
#include "accSSAO_blurh_ps.fx"
