#include "accSS_cbuffer.hlsl"
#include "accSSDashboard.hlsl"

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

Texture2D txColor : register(TX_SLOT_SS_ARG0);
Texture2D txPos : register(TX_SLOT_SS_ARG1);
Texture2D txNormal : register(TX_SLOT_SS_ARG2);

struct PS_Out {
  float4 color : SV_TARGET0;
  float4 pos : SV_TARGET1;
  float4 normal : SV_TARGET2;
};

#define R 3
PS_Out main(VS_Copy pin) {
  float4 totalColor = 0;
  float4 totalPos = 0;
  float4 totalNormal = 0;
  float totalWeight = 0;

  float4 maxColor = 0;
  float4 maxPos = 0;
  float4 maxNormal = 0;
  float maxWeight = 0;

  float maxR = length(float2(R, R));

  [unroll]
  for (int x = -R; x <= R; x++)
  [unroll]
  for (int y = -R; y <= R; y++){
    if (abs(x) + abs(y) == R + R) continue;
    // float weight = 1 / (1 + length(float2(x, y)));

    // float4 color = txColor.SampleLevel(samLinearBorder0, pin.Tex, 0, int2(x, y));
    // float4 pos = txPos.SampleLevel(samLinearBorder0, pin.Tex, 0, int2(x, y));
    // float4 normal = txNormal.SampleLevel(samLinearBorder0, pin.Tex, 0, int2(x, y));
    float4 color = txColor.SampleLevel(samPointClamp, pin.Tex, 0, int2(x, y));
    float4 pos = txPos.SampleLevel(samPointClamp, pin.Tex, 0, int2(x, y));
    float4 normal = txNormal.SampleLevel(samPointClamp, pin.Tex, 0, int2(x, y));

    float cR = length(float2(x, y));
    float weight = saturate(dashLuminance(color.rgb)) / (1 + cR);
    // float weight = dashLuminance(color.rgb) > 0.01;
    // float weight = color.g > 0.1;

    if (weight > 0.001){
      totalColor += color * weight;
      totalPos += pos * weight;
      totalNormal += normal * weight;
      totalWeight += weight;
    }

    if (weight > maxWeight){
      maxWeight = weight;
      maxColor = color;
      maxPos = pos;
      maxNormal = normal;
    }

    // totalColor += color * weight;
    // totalPos += pos * weight;
    // totalWeight += weight;
  }

  PS_Out ret;
  ret.color = totalColor / max(totalWeight, 0.1);
  ret.pos = totalPos / max(totalWeight, 0.001);
  ret.normal = totalNormal / max(totalWeight, 0.001);
  // ret.color = maxColor;
  // ret.pos = maxPos;
  // ret.normal = maxNormal;
  return ret;
}
