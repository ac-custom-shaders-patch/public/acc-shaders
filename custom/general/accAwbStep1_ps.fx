#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10){
  float gStep;
  float3 gPad0;
}

#include "include/poisson.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  float R = 0.01 * pow(2, gStep);

  float4 ret = 0;
  float tot = 0.0001;
  #define DISK_SIZE 16
  for (uint i = 0; i < DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(DISK_SIZE, i);

    float2 sampleUV = pin.Tex + offset * float2(1, 16. / 9) * R;
    float4 v = txDiffuse.SampleLevel(samLinearClamp, sampleUV, 0);
    float w = v.w;
    ret += v * w;
    tot += w;
  }
  return ret / tot;
  // return txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0);
}