#include "include/common.hlsl"
#include "include/poisson.hlsl"

Texture2D txTex0 : register(t0);
Texture2D txTex1 : register(t1);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

struct PS_OUT {
  float4 Col1 : SV_Target0;
  float4 Col2 : SV_Target1;
};

cbuffer cbData : register(b10){
  float gMult;
  float3 gPad0;
}

PS_OUT main(VS_Copy pin) {
  PS_OUT ret = (PS_OUT)0;
  float tot = 0;

  float radius = 0.008 * gMult;
  #define PUDDLES_DISK_SIZE 24
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 uv = pin.Tex + SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * radius;
    ret.Col1 += txTex0.SampleLevel(samLinearClamp, uv, 0);
    ret.Col2 += txTex1.SampleLevel(samLinearClamp, uv, 0);
    ++tot;
  }
  ret.Col1 /= tot;
  ret.Col2 /= tot;
  
  return ret;
}