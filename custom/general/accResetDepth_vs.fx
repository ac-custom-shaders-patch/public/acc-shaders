#define SPS_NO_POSC
#include "include_new/base/_include_vs.fx"

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct PS_IN { 
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD;
};

PS_IN main(VS_IN_ac vin SPS_VS_ARG) {
  PS_IN vout;
  // vout.PosH = toScreenSpaceAlt(vin.PosL);

  float4 posH = mul(mul(mul(vin.PosL, ksWorld), ksView), ksProjection);
  if (posH.z < 0) posH.z = 0;
  vout.PosH = posH;

  vout.Tex = vin.Tex;
  SPS_RET(vout);
}

