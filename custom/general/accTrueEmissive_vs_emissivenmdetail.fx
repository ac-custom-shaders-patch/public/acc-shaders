#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_NORMALIZED_TB
#define CARPAINT_NMDETAILS
#define GETNORMALW_XYZ_TX
#define SUPPORTS_AO
#define EMISSIVE_SLOT 5
#define USE_TXDETAIL
#include "accTrueEmissive_vs.fx"