#include "accAltTonemapping.hlsl"

cbuffer cbData : register(b1) {
  float W;
  float3 _pad;
}

float3 applyTonemap(float3 color) {
  color = max(color, 0.0001);
	float luma = dot(color, 0.3);
	float tone = exp(-1 / (2 * luma + 0.2)) / max(0.01, luma);
  color = lerp(luma, color, lerp(pow(max(tone, 0), 0.25), tone, saturate((W - 2) / 10)));
	return color * tone;
}
