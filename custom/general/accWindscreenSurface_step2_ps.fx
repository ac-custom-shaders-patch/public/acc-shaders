#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

cbuffer _cbExtWindscreenReprojection : register(b10) {
  float2 gDir;
  float2 _pad;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 ret = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);
  if (!ret.w) return 0;
  
  float w = 1;
  for (int x = -16; x <= 16; ++x){
    if (x == 0) continue;
    float s = 1. / (1. + 0.1 * abs(x));
    ret += txDiffuse.SampleLevel(samLinearClamp, pin.Tex + gDir * x, 0) * s;
    w += s;
  }
  ret /= w;

  if (gDir.x) {
    ret.rgb = normalize(ret.rgb);
    // ret.rgb = ret.rgb * 0.5 + 0.5;
  }

  return ret;
}