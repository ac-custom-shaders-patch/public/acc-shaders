Texture2D txDiffuse : register(t0);

SamplerState samLinearSimple : register(s15) {
  Filter = LINEAR;
  AddressU = CLAMP;
  AddressV = CLAMP;
};

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  int2 o = int2(0, 1);
  #ifdef HORIZONTAL
    o = int2(1, 0);
  #endif

  float4 color = 0;
  float w = 0;
  [unroll]
  for (int i = -7; i <= 7; i++){
    float sw = 1.0 / float(1 + abs(i) * 0.2);
    color += txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0, o * i) * sw;
    w += sw;
  }
  return color / w;
}
