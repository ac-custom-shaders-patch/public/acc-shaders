#include "accSS_cbuffer.hlsl"

Texture2DMS<float4> txDiffuse : register(TX_SLOT_COLOR); 

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float3 tonemap(float3 x){ return x / (x + 1); }
float3 inverseTonemap(float3 x){ return x / max(1 - x, 0.00001); }
// float3 tonemap(float3 x){ return x; }
// float3 inverseTonemap(float3 x){ return x; }
float4 tonemap(float4 x){ return float4(tonemap(x.rgb), x.a); }
float4 inverseTonemap(float4 x){ return float4(inverseTonemap(x.rgb), x.a); }

float4 main(VS_Copy pin) : SV_TARGET {
  float4 resValue = 0;
  for (uint i = 0; i < sampleCount; ++i){
    resValue += tonemap(txDiffuse.Load(pin.PosH.xy, i));
  }
  return inverseTonemap(resValue * sampleCountInv);
}

static const float2 SubSampleOffsets[4] = {
  float2(-0.125f, -0.375f),
  float2( 0.375f, -0.125f),
  float2(-0.375f,  0.125f),
  float2( 0.125f,  0.375f),
};

float filterSmoothstep(float x){ return 1 - smoothstep(0, 1, x); }

// float4 main(VS_Copy pin) : SV_TARGET {
//   float4 resValue = 0;

//   const float gResolveFilterDiameter = 2;
//   const int gSampleRadius = (int)((gResolveFilterDiameter / 2.0f) + 0.499f);

//   float4 sum = 0;
//   const bool InverseLuminanceFiltering = true;
//   for (int y = -gSampleRadius; y <= gSampleRadius; ++y) {
//     for (int x = -gSampleRadius; x <= gSampleRadius; ++x) {
//       float2 samplePos = pin.PosH.xy + float2(x, y);

//       [unroll]
//       for(uint subSampleIdx = 0; subSampleIdx < sampleCount; ++subSampleIdx) {
//         float2 sampleDist = abs(float2(x, y) + SubSampleOffsets[subSampleIdx].xy) / (gResolveFilterDiameter / 2.0f);
//         bool useSample = all(sampleDist <= 1);
//         if (useSample) {
//           float3 color = txDiffuse.Load(uint2(samplePos), subSampleIdx).xyz;
//           float weight = filterSmoothstep(sampleDist.x) * filterSmoothstep(sampleDist.y);
//           sum += float4(color, 1) * weight / (1 + luminance(color));
//         }
//       }
//     }
//   }

//   return float4(sum.rgb / sum.a, 1);
// }