#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  int2 gOffset;
  int2 _pad;
}

float4 main(VS_Copy pin) : SV_TARGET {
  uint2 dim;
  uint sampleCount; 
  txDiffuse.GetDimensions(0, dim.x, dim.y, sampleCount); 
  // return float4(1, 0, 0, 1);
  // if (dim.y != 1039) return 1;
  // return txDiffuse.Sample(samLinear, pin.Tex);
  return txDiffuse.Load(int3(gOffset + pin.PosH.xy, 0));
}