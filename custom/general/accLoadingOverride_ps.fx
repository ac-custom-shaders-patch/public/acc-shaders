#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);
Texture2D txBar : register(t1);

cbuffer cbData : register(b10) {
  float4 gBar;
  float gProgress;
  float gFade;
  float gRatio;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float2 mapBar(float2 uv){
  return remap(uv, gBar.xy, gBar.xy + gBar.zw, 0, 1);
}

float4 main(VS_Copy pin) : SV_TARGET {
  if (gRatio > 0){
    pin.Tex.x = (pin.Tex.x * 2 - 1) * gRatio * 0.5 + 0.5;
  } else {
    pin.Tex.y = (pin.Tex.y * 2 - 1) * -gRatio * 0.5 + 0.5;
  }
  if (!all(pin.Tex >= 0 && pin.Tex <= 1)) return float4(0, 0, 0, 1);
  float4 mult = float4(gFade, gFade, gFade, 1);
  float2 pos = mapBar(pin.Tex);
  if (all(saturate(pos) == pos) && pos.x < gProgress){
    return txBar.SampleLevel(samLinearSimple, pos, 0) * mult;
  }
  return txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0) * mult;
}