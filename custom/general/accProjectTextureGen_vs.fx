#include "include/common.hlsl"

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct VS_PT {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0; 
  noperspective float2 MeshTex : TEXCOORD1; 
  noperspective float3 NormalView : TEXCOORD2;
  noperspective float3 NormalWorld : TEXCOORD3;
  noperspective float3 PositionView : TEXCOORD4;
  noperspective float3 PositionWorld : TEXCOORD5;
};

cbuffer cbData : register(b10) {
  float4x4 gObject;
  float4x4 gCamera;
  float2 gSizeInv;
  float2 gUVOffset;
  float2 gOffsetSize;
  float2 _gPad;
  float3 gSceneOffset;
}

VS_PT main(VS_IN_ac vin, uint instanceID : SV_InstanceID) {
  VS_PT vout;

  float2 pos = vin.Tex - gUVOffset;
  pos.y = 1 - pos.y;
  vout.PosH = float4(pos * 2 - 1, 0, 1);

  if (instanceID < 4){
    vout.PosH.xy += (float2(instanceID / 2, instanceID % 2) * 2 - 1) * gOffsetSize;
  }

  float4 posW = mul(vin.PosL, gObject);
  vout.PositionWorld = posW.xyz - gSceneOffset;

  float4 posH = mul(posW, gCamera);
  vout.PositionView = posH.xyz;
  vout.Tex = posH.xy * gSizeInv + 0.5;
  vout.Tex.y = 1 - vout.Tex.y;

  float3 normalW = mul(vin.NormalL, (float3x3)gObject);
  vout.NormalWorld = normalW;

  float3 normalH = mul(normalW, (float3x3)gCamera);
  vout.NormalView = normalH;

  float distance = abs(posH.z) + 10;
  // float distance = dot2(posH.xyz);
  distance -= instanceID / 2.;
  if (vout.NormalView.z < 0) distance += 100;

  // vout.Debug = saturate(instanceID / 4.);
  vout.PosH.z = 1 / distance;
  vout.MeshTex = vin.Tex;

  return vout;
}
