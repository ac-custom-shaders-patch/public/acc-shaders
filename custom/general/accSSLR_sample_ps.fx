#define SS_VIEWLESS
#include "accSS_cbuffer.hlsl"
#include "include_new/base/_gamma.fx"

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

Texture2D txColor : register(TX_SLOT_COLOR);
Texture2D txNormals : register(TX_SLOT_NORMALS);
Texture2D txTracedExpanded : register(TX_SLOT_SS_ARG4);
Texture2D txSSLR : register(TX_SLOT_SS_ARG5);

cbuffer cbLighting : register(b2) {  
  float4 ksLightDirection;
  float4 ksAmbientColor_sky_;
  float4 ksLightColor;
  float4 ksHorizonColor;
  float4 ksZenithColor;
  // 5*16=80

  float ksExposure;
  float ksScreenWidth;
  float ksScreenHeight;
  float ksFogLinear;

  float ksFogBlend;
  float3 ksFogColor;
}

float4 main(VS_Copy pin) : SV_TARGET {
  // return txColor.SampleLevel(samLinearClamp, pin.Tex, 2);
  // return txColor.Sample(samLinearClamp, pin.Tex - float2(0, 0.1))*0.1;
  float4 vSSLR = txSSLR.SampleLevel(samPointClamp, pin.Tex, 0);
  // return vSSLR;
  // if (vSSLR.w == 0) return vSSLR;

  #ifdef USE_TAA_FIX
    float4 vSSLRmax = 0;
    float vTotal = 0;

    [unroll]
    for (int x = -1; x <= 1; x++)
    [unroll]
    for (int y = -1; y <= 1; y++){
      if (abs(x) + abs(y) == 2) continue;
      float4 v = txSSLR.SampleLevel(samPointClamp, pin.Tex, 0, int2(x, y));
      if (v.a > vSSLRmax.a) vSSLRmax = v;
      if (v.a > 0.1) {
        vSSLR += v;
        vTotal++;
      }
    }

    // if (!vTotal) return 0; 
    // vSSLR /= vTotal;

    vSSLR = vSSLRmax;
    // vSSLR = txSSLR.SampleLevel(samLinearSimple, pin.Tex, 0);
  #endif

  #ifdef USE_UPSCALE_FIX
    // float4 N = 0;
    // float C = 0;
    // [unroll]
    // for (int x = -1; x <= 1; x++)
    // [unroll]
    // for (int y = -1; y <= 1; y++){
    //   if (abs(x) + abs(y) == 2 || x == 0 && y == 0) continue;
    //   N += txSSLR.SampleLevel(samPointClamp, pin.Tex, 0, int2(x, y));
    //   ++C;
    // }
    // N /= C;
    // vSSLR = N / max(N.w, 1);
    // vSSLR.xyz = lerp(N.xyz / N.w, vSSLR.xyz / vSSLR.w, vSSLR.w);
    // vSSLR.xyz *= vSSLR.w;
    // vSSLR = lerp(N, vSSLR, vSSLR.w);
    // vSSLR.xyz = lerp(N.xyz, vSSLR.xyz, vSSLR.w);
    // vSSLR.w = max(N.w, vSSLR.w);

    if (any(vSSLR.xy <= 0 || vSSLR.xy >= 1)) {
      // if (vSSLR.w > 0) return float4(3, 0, 3, 1);
      vSSLR.xy = pin.Tex;
      vSSLR.w = 0;
    }

    float4 valueLinear = txSSLR.SampleLevel(samLinearClamp, pin.Tex, 0);
    if (any(valueLinear.xy <= 0 || valueLinear.xy >= 1)) valueLinear.xy = pin.Tex;

    if (vSSLR.w == 0 && valueLinear.w > 0){
      // vSSLR = valueLinear;
      
      // float3 nmBase = normalize(ssNormalDecode(txNormals.SampleLevel(samPointClamp, pin.Tex, 0).xyz));
      // float w = 0;
      // vSSLR = 0;
      // [unroll]
      // for (int x = -1; x <= 1; x++)
      // [unroll]
      // for (int y = -1; y <= 1; y++){
      //   if (abs(x) + abs(y) == 2 || x == 0 && y == 0) continue;
      //   float4 N = txSSLR.SampleLevel(samPointClamp, pin.Tex, 0, int2(x, y));
      //   float3 nmN = normalize(ssNormalDecode(txNormals.SampleLevel(samPointClamp, pin.Tex, 0, int2(x, y)).xyz));
      //   float C = 1;//pow(saturate(dot(nmBase, nmN)), 8);
      //   w += C;
      //   vSSLR += N * C;
      // }
      // vSSLR /= w;
      // return float4(3, 0, 3, 1);
    }

    float edgeK = vSSLR.w ? saturate(dot(fwidth(valueLinear.xy), 20) - 0.2) : 0;
    
    // if (edgeK > 0){
    //   float3 nmBase = normalize(ssNormalDecode(txNormals.SampleLevel(samPointClamp, pin.Tex, 0).xyz));
    //   float w = 1;
    //   [unroll]
    //   for (int x = -1; x <= 1; x++)
    //   [unroll]
    //   for (int y = -1; y <= 1; y++){
    //     if (abs(x) + abs(y) == 2 || x == 0 && y == 0) continue;
    //     float4 N = txSSLR.SampleLevel(samPointClamp, pin.Tex, 0, int2(x, y));
    //     float3 nmN = normalize(ssNormalDecode(txNormals.SampleLevel(samPointClamp, pin.Tex, 0, int2(x, y)).xyz));
    //     float C = pow(saturate(dot(nmBase, nmN)), 8);
    //     w += C;
    //     vSSLR += N * C;
    //   }
    //   vSSLR /= w;
    // }

    vSSLR = lerp(valueLinear, vSSLR, edgeK);
    // vSSLR = valueLinear;

    // return saturate(fwidth(valueLinear.x) * 10 - 0.1);

    // float4 depthR = txSSLR.GatherRed(samLinearClamp, pin.Tex, int2(0, 0));
    // float4 depthG = txSSLR.GatherGreen(samLinearClamp, pin.Tex, int2(0, 0));
    // float4 depthW = txSSLR.GatherAlpha(samLinearClamp, pin.Tex, int2(0, 0));
    // float4 bf = bilinearFactors(pin.Tex * float2(1920, 1080) / 2 - 0.5);
    // float wx = max(max(depthW.r, depthW.g), max(depthW.g, depthW.a));
    // float wn = min(min(depthW.r, depthW.g), min(depthW.g, depthW.a));
    // float wt = saturate(wx - wn);
    // vSSLR.x = lerp(dot(depthR.wzxy, bf), vSSLR.x, wt);
    // vSSLR.y = lerp(dot(depthG.wzxy, bf), vSSLR.y, wt);

    // float3 color = txColor.SampleBias(samLinearClamp, vSSLR.xy, 6).xyz;
    // float3 color = txColor.Sample(samLinear, saturate(vSSLR.xy)).xyz;
    float3 color = txColor.Sample(samLinear, saturate(vSSLR.xy)).xyz;

  #else
    if (any(vSSLR.xy <= 0 || vSSLR.xy >= 1)) vSSLR.xy = pin.Tex;
    float lod = txColor.CalculateLevelOfDetail(samLinearSimple, vSSLR.xy);
    float3 color = txColor.SampleLevel(samLinearClamp, vSSLR.xy, min(lod, 3)).xyz;
    // color = lerp(color, color / (gMainBrightnessMult + color), vSSLR.w == 1 ? saturate(lod / 2 - 2) : 1);
    // color = lerp(color, color / (gMainBrightnessMult + color), saturate(lod / 2 - 2)); // helped at night, but created shitty outlines during the day
    // color = lerp(color, min(color, gMainBrightnessMult * 0.01), vSSLR.w == 1 ? saturate(lod / 2 - 2) : 1);
    // color = lerp(color, float3(0, 1, 0), saturate(lod / 2 - 2));
    // color = lerp(color, 0, saturate(lod / 2 - 2));
    // vSSLR.w *= lerp(1, 0, saturate(lod / 2 - 2));
    // float3 color = txColor.SampleLevel(samLinearClamp, saturate(vSSLR.xy), 0).xyz;

    // color = lerp(color, 0, saturate(lod / 2));
  #endif

  #ifdef GAMMA_FIX
    color.rgb = pow(max(0, color.rgb), 0.5);
  #endif

  // color = txColor.SampleLevel(samLinear, saturate(vSSLR.xy), 0).xyz;
  
  // if (vSSLR.w <= 0.001) return 0; 
  // if (vSSLR.w <= 0.001) return float4(lerp(ksZenithColor.rgb, ksFogColor, ksFogBlend), 0); 
  // return vSSLR;

  // return txTracedExpanded.SampleLevel(samLinear, pin.Tex, 0);
  
  // if (any(vSSLR.xy <= 0 || vSSLR.xy >= 1)) vSSLR.xyz = txTracedExpanded.SampleLevel(samLinear, pin.Tex, 0).xyz;
  // float3 color = txColor.Sample(samLinearSimple, saturate(vSSLR.xy)).xyz;
  // return float4(max(color, 0) * saturate(vSSLR.w * 10 + 0.5), vSSLR.w);
  // return vSSLR;
  return float4(max(color, 0), vSSLR.w);
  return float4(max(color, 0) * GAMMA_OR(vSSLR.w == 1, 1), vSSLR.w);
  return float4(max(color, 0) * vSSLR.w, vSSLR.w);
}