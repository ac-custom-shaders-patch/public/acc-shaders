#include "include/common.hlsl"
#include "include/poisson.hlsl"

Texture2D<float> txDepth : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbCamera : register(b10) {
  float4x4 gVPInv;
  float4x4 gVPDof0;
  float4x4 gVPDof1;
}

float computeR(float2 uv, float d) {
  float4 h = float4(uv, d, 1);
  float4 w = mul(h, gVPInv);
  w /= w.w;

  float4 uv0 = mul(w, gVPDof0);
  uv0 /= uv0.w;

  float4 uv1 = mul(w, gVPDof1);
  uv1 /= uv1.w;

  return sqrt(max(dot2(uv0.xy - uv), dot2(uv1.xy - uv)));
}

#define N 1.

float4 main(VS_Copy pin) : SV_TARGET {
  float d = txDepth.SampleLevel(samLinearSimple, pin.Tex, 0);
  float R = 0.03;
  #define DISK_SIZE 16
  float r = pow(d, N);
  for (uint i = 0; i < DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(DISK_SIZE, i);
    float2 sampleUV = pin.Tex + offset * R;
    float S = txDepth.SampleLevel(samLinearClamp, sampleUV, 0);
    r += pow(min(d, S), N);
  }
  d = pow(r / (DISK_SIZE + 1), 1./N);
  return computeR(pin.Tex, d);

}