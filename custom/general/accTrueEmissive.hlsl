struct VS_Glw {
  float4 PosH : SV_POSITION;
  float3 PosC : TEXCOORD0;
  float3 Normal : TEXCOORD1;
  float3 Color : TEXCOORD2;
  float RadiusInv : TEXCOORD3;
  float2 Tex : TEXCOORD4;
};

#define TE_FLAG_ALPHA_BLEND 1
#define TE_FLAG_ALPHA_TEST 2
#define TE_FLAG_DEBUG 4
#define TE_FLAG_IGNORE_TEXTURE_COLOR 8

cbuffer cbGenData : register(b5){
  float gDirected;
  float gBaseRadius;
  uint gFlags;
  uint gEmissiveTweak_MipBias;
  float3 gColorMult;
  float gOverallColorMult; // WeatherFX configured multiplier
}
