#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);
Texture2D txMask1 : register(t1);
Texture2D txMask2 : register(t2);

#define MASK_FLAG_USE_COLOR_AVERAGE (1U<<0U)
#define MASK_FLAG_USE_ALPHA (1U<<1U)
#define MASK_FLAG_USE_RED (1U<<2U)
#define MASK_FLAG_USE_GREEN (1U<<3U)
#define MASK_FLAG_USE_BLUE (1U<<4U)
#define MASK_FLAG_USE_INVERTED_ALPHA (1U<<5U)
#define MASK_FLAG_USE_INVERTED_RED (1U<<6U)
#define MASK_FLAG_USE_INVERTED_GREEN (1U<<7U)
#define MASK_FLAG_USE_INVERTED_BLUE (1U<<8U)
#define MASK_FLAG_MIX_COLORS (1U<<9U)
#define MASK_FLAG_MIX_INVERTED_COLORS (1U<<10U)
#define MASK_FLAG_ALT_UV (1U<<16U)

void procMask(inout float4 tex, Texture2D txMask, uint flags, float2 uv, float2 altUV){
  if (flags == 0) return;
  if (flags & MASK_FLAG_ALT_UV) uv = altUV;
  float4 mask = txMask.Sample(samAnisotropic, uv);
  if (flags & MASK_FLAG_USE_COLOR_AVERAGE) tex.a *= dot(mask.rgb, 1./3.);
  if (flags & MASK_FLAG_USE_ALPHA) tex.a *= mask.a;
  if (flags & MASK_FLAG_USE_RED) tex.a *= mask.r;
  if (flags & MASK_FLAG_USE_GREEN) tex.a *= mask.g;
  if (flags & MASK_FLAG_USE_BLUE) tex.a *= mask.b;
  if (flags & MASK_FLAG_USE_INVERTED_ALPHA) tex.a *= 1 - mask.a;
  if (flags & MASK_FLAG_USE_INVERTED_RED) tex.a *= 1 - mask.r;
  if (flags & MASK_FLAG_USE_INVERTED_GREEN) tex.a *= 1 - mask.g;
  if (flags & MASK_FLAG_USE_INVERTED_BLUE) tex.a *= 1 - mask.b;
  if (flags & MASK_FLAG_MIX_COLORS) tex.rgb *= mask.rgb;
  if (flags & MASK_FLAG_MIX_INVERTED_COLORS) tex.rgb *= 1 - mask.rgb;
}

cbuffer cbData : register(b10) {
  float4 gRegion;
  float4 gTex;
  float4 gMask1UV;
  float4 gMask2UV;
  float2 gFrom;
  float2 gTo;
  float4 gColorMult;
  float4 gColorOffset;
  uint gMask1Flags;
  uint gMask2Flags;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
  noperspective float2 TexMask1 : TEXCOORD1;
  noperspective float2 TexMask2 : TEXCOORD2;
};

float4 main(VS_Copy pin) : SV_TARGET {
  if (any(pin.PosH.xy < gFrom || pin.PosH.xy > gTo || pin.Tex < 0 || pin.Tex > 1)) discard;
  float4 tex = txDiffuse.Sample(samAnisotropic, pin.Tex);
  tex = tex * gColorMult + gColorOffset;
  procMask(tex, txMask1, gMask1Flags, pin.TexMask1, pin.TexMask1);
  procMask(tex, txMask2, gMask2Flags, pin.TexMask2, pin.TexMask2);
  return tex;
}