#include "accSSLR_flags.hlsl"
#include "accSS_cbuffer.hlsl"
#include "include_new/base/samplers_ps.fx"
#include "include/common.hlsl"
#include "include/bayer.hlsl"

Texture2D<float> txPrev : register(TX_SLOT_SS_ARG5);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float q = 0;
  [unroll]
  for (int i = -8; i <= 7; ++i){
    float r = txPrev.Load(int3(pin.PosH.x, pin.PosH.y * 16 + i, 0));
    q = max(q, r);
  }
  return q;
}