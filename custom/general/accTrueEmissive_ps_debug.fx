#include "include/common.hlsl"
#include "accTrueEmissive.hlsl"

float4 main(VS_Glw pin) : SV_TARGET {
  return float4(pin.Color * 2.5, 1);
}