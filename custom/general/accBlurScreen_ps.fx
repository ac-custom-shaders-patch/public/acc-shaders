#include "include/common.hlsl"
#include "include_new/base/cbuffers_common.fx"

Texture2D txDiffuse : register(t0);
Texture2D txPrevious : register(t3);
Texture2D txNoise : register(TX_SLOT_NOISE);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10){
  float gStep;
  float gWeightBaseK;
  float2 gPad0;
}

#ifdef GAMMA_FIX
#define WEIGHT_BASE_K GAMMA_OR(gWeightBaseK, 1.)
float3 hdr_tonemap(float3 x){ return x / (x + WEIGHT_BASE_K); }
float3 hdr_inverseTonemap(float3 x){ return WEIGHT_BASE_K * x / max(1 - x, 0.00001); }
#else
float3 hdr_tonemap(float3 x){ return x; }
float3 hdr_inverseTonemap(float3 x){ return x; }
#endif
float4 hdr_tonemap(float4 x){ return float4(hdr_tonemap(x.rgb), x.a); }
float4 hdr_inverseTonemap(float4 x){ return float4(hdr_inverseTonemap(x.rgb), x.a); }

float3 samplePoint(float2 uv){
  if (gStep == 4){
    uv = (uv * 2 - 1) * float2(0.95, 0.85) * 0.5 + 0.5;
  }
  return txDiffuse.SampleLevel(samLinearClamp, uv, 0).xyz;

  // float2 s = float2(16, 9);
  // float2 d = saturate(abs(uv * 2 - 1) * s - (s - 1));
  // return txDiffuse.SampleLevel(samLinearClamp, uv, 0).xyz * lerp(1, 0.85, saturate(length(d)) * gStep);
}

#include "include/poisson.hlsl"
float4 main(VS_Copy pin) : SV_TARGET {
  #ifdef APPLY_TAA_FIX
    float4 previous = txPrevious.SampleLevel(samLinearClamp, pin.Tex, gStep);
  #endif

  float4 currentShot = 0;

  float radius = 0.004;
  if (gStep == 2) radius = 0.008;
  else if (gStep == 3) radius = 0.024;
  else if (gStep == 4) radius = 0.064;

  // float2 random = normalize(txNoise.SampleLevel(samLinear, pin.PosH.xy / 256, 0).xy);
  float2 random = normalize(float2(sin(gStep), sin(gStep * 17.396)));

  #define DISK_SIZE GAMMA_OR_STAT(32, 25)

  [branch]
  if (GAMMA_FIX_ACTIVE && gStep > 1) {
    for (uint i = 0; i < DISK_SIZE; ++i) {
      float2 offset = SAMPLE_POISSON(DISK_SIZE, i);
      offset = reflect(offset, random);

      // float4 s = float4(samplePoint(pin.Tex + offset * radius * float2(1, 16./9)), 1);
      // currentShot += s / (1e-20 + sqrt(dot(s.rgb, 1)));
      float4 s = float4(hdr_tonemap(samplePoint(pin.Tex + offset * radius * float2(1, 16./9))), 1);
      currentShot += s;
    }
  } else {
    for (uint i = 0; i < DISK_SIZE; ++i) {
      float2 offset = SAMPLE_POISSON(DISK_SIZE, i);
      offset = reflect(offset, random);

      float4 s = float4(hdr_tonemap(samplePoint(pin.Tex + offset * radius * float2(1, 16./9))), 1);
      currentShot += s / 1;
    }
  }

  currentShot /= currentShot.w;
  currentShot.rgb = hdr_inverseTonemap(currentShot.rgb);

  if (!(dot(currentShot.rgb, 1) >= 0 && dot(currentShot.rgb, 1) < 1e30)) currentShot.rgb = 0;

  #ifdef APPLY_TAA_FIX
    previous = clamp(previous, currentShot * 0.25, currentShot * 4);
    return lerp(previous, currentShot, 0.7);
  #else
    return currentShot;
  #endif
}

// #define R 2
// float4 main(VS_Copy pin) : SV_TARGET {
//   float4 currentShot = 0;
//   [unroll]
//   for (int x = -R; x <= R; x++)
//   [unroll]
//   for (int y = -R; y <= R; y++){
//     if (abs(x) + abs(y) == R + R) continue;
//     currentShot += float4(txDiffuse.SampleLevel(samLinearClamp, pin.Tex, 0, int2(x, y)).xyz, 1);
//   }
//   currentShot /= currentShot.w;
//   return currentShot;
// }