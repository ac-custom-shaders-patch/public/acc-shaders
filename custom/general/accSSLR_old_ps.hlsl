#include "accSSLR_flags.hlsl"
#include "accSS_cbuffer.hlsl"
#include "include_new/base/samplers_ps.fx"
#include "include/common.hlsl"
#include "include/bayer.hlsl"

Texture2D txNormals : register(TX_SLOT_NORMALS);
Texture2D<float> txDepth : register(TX_SLOT_GDEPTH);
Texture2D txReflections1 : register(TX_SLOT_SS_ARG0);
Texture2D txReflections2 : register(TX_SLOT_SS_ARG1);
Texture2D txNoise : register(TX_SLOT_NOISE);

Texture2D<float2> txMotion : register(TX_SLOT_MOTION);
Texture2D txPrevious : register(TX_SLOT_SS_ARG2);
Texture2D<float> txPreviousDimming : register(TX_SLOT_SS_ARG3);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float3 ssGetPos(float2 uv, float depth){
  float4 p = mul(float4(uv.xy, depth, 1), viewProjInv);
  return p.xyz / p.w;
}

float3 ssGetUv(float3 position){
  float4 p = mul(float4(position, 1), viewProj);
  return p.xyz / p.w;
}

float ssGetDepth(float2 uv){
  return txDepth.SampleLevel(samLinearClamp, uv, 0).x;
  return txDepth.SampleLevel(samLinearBorder1, uv, 0).x;
}

float ssGetDepthLR(float2 uv){
  return txDepth.SampleLevel(samLinearClamp, uv, 2).x;
  return txDepth.SampleLevel(samLinearBorder1, uv, 2).x;
}

#ifndef ITERATIONS_SHORT
#define ITERATIONS_SHORT 16
#endif

#ifndef ITERATIONS_LARGE
#define ITERATIONS_LARGE 16
#endif

#ifndef FIX_MULT
#define FIX_MULT 0.5
#endif

#ifndef OFFSET
#define OFFSET 0.05
#endif

#ifndef GLOW_FIX
#define GLOW_FIX 0.2
#endif

#ifndef DISTANCE_THRESHOLD
#define DISTANCE_THRESHOLD 0.01
#endif

float selfHitTest(float2 uv, float2 newUv, float depth, float newDepth){ 
  // return 1;
  float2 uvDist = newUv - uv;
  uvDist *= gSize.xy;
  uvDist = abs(uvDist);
  return saturate(max(uvDist.x, uvDist.y) - 1);
  // return max(saturate(dot(uvDist, uvDist) * 1000000), 1 - pow(saturate(newDepth / depth), 100000));
}

struct RESULT {
  float4 main : SV_TARGET0;
  float dimming : SV_TARGET1;
};

RESULT main(VS_Copy pin) {
  float4 vReflections1 = txReflections1.SampleLevel(samPoint, pin.Tex, 0);
  float4 vNormal = txNormals.SampleLevel(samPoint, pin.Tex, 0);

  float forceMode = 0;
  if (vReflections1.w < -0.001){
    // Silly way of storing SSLR_FORCE flag
    forceMode = 1;
    vReflections1.w = abs(vReflections1.w);
  }

  #ifndef SSLR_TRACE_EVERYTHING
    [branch]
    if (vReflections1.w * abs(dot(vNormal, 1)) < 0.001) {
      return (RESULT)0;
    }
  #endif

  float depth = ssGetDepth(pin.Tex);
  float3 position = ssGetPos(pin.Tex, depth);
  float3 normal = normalize(ssNormalDecode(vNormal.xyz));

  float dist = length(position);
  float3 viewDir = position / dist;
  float3 reflectDir = normalize(reflect(viewDir, normal));

  float bayer = -getBayer(pin.PosH);
  float4 vReflections2 = txReflections2.SampleLevel(samPoint, pin.Tex, 0);
  float blur = pow(vReflections2.w, 3);
  reflectDir += normal * (pow(bayer, 4) - 0.5) * abs(dot(normal, viewDir)) * saturate(blur * 2 - 1) * 0.5;
  reflectDir = normalize(reflectDir);

  // float curvedGroundFixK = saturate(dot(reflectDir, viewDir) - 0.99) 
  //   * saturate(normal.y - 0.99) * 100 * 100;
  // normal = lerp(normal, float3(0, 1, 0), curvedGroundFixK * saturate(normal.y * 1000 - 998));
  // reflectDir = normalize(reflect(viewDir, normal));

  float3 calculatedPosition, newPosition;
  float3 newUv = 0;
  float newL;
  float newDepth;

  float DK0 = pow(saturate(dist / 4 - 1), 4);
  float DK1 = pow(saturate(dist - 1), 4);

  float RdotV = dot(viewDir, reflectDir);
  float NdotV = dot(viewDir, normal);

  float N = saturate(RdotV * 30 - 29); 
  #ifdef USE_LR_DEPTH
    float L = lerp(0.2, 0.8, DK0);
  #else
    float L = lerp(0.1, 0.2, DK0);
  #endif
  L = min(L, dist / 200);
  L = max(L, dist / 100 - 1);
  // if (forceMode) L /= max(pow(1 - N, 4), 0.01);

  // L = 0.1;

  float S = lerp(0.1, 1, DK1);

  #ifdef USE_TAA
    float4 random = txNoise.SampleLevel(samPoint, (pin.PosH.xy + gFrameNum) / 32 + gRandom, 0);
    L *= lerp(0.8, 1.2, random.x);
  #endif

  [loop]
  for (int j0 = 0; j0 < ITERATIONS_SHORT; j0++) {
    calculatedPosition = position + reflectDir * L;

    newUv = ssGetUv(calculatedPosition);
    #ifdef USE_LR_DEPTH
      newDepth = ssGetDepthLR(newUv.xy);
    #else
      newDepth = ssGetDepth(newUv.xy);
    #endif
    newPosition = ssGetPos(newUv.xy, newDepth);

    float actualL = dot2(calculatedPosition - newPosition);
    [branch]
    if (newDepth < newUv.z || actualL < pow(j0 ? L * 0.25 : L * 0.4, 2)) break;

    L *= 1.4;
  }

  [unroll]
  for (int j1 = 0; j1 < ITERATIONS_LARGE; j1++) {
    calculatedPosition = position + reflectDir * L;

    newUv = ssGetUv(calculatedPosition);
    #ifdef USE_LR_DEPTH
      newDepth = j1 < ITERATIONS_LARGE ? ssGetDepthLR(newUv.xy) : ssGetDepth(newUv.xy);
    #else
      newDepth = ssGetDepth(newUv.xy);
    #endif
    newPosition = ssGetPos(newUv.xy, newDepth);
    newL = length(position - newPosition);

    if (j1 != ITERATIONS_LARGE - 1){
      L += (newL - L) * (j1 < ITERATIONS_LARGE / 3 ? S : 1);
    }
  }

  calculatedPosition = position + reflectDir * L;
  float miss = length(calculatedPosition - newPosition);
  // newUv = ssGetUv(calculatedPosition);

  float fresnel = saturate(6 * pow(1 + NdotV, 2));
  fresnel = max(fresnel, 1 - pow(saturate(newDepth / depth), 10000));

  float newDist = length(newPosition);  
  float threshold = dist / lerp(100, 10, DK0);
  float thresholdL = dist / 3;
  float thresholdD = dist / 100;

  float qDist = 1 - saturate(miss / threshold - 1);
  float qDistL = 1 - saturate(miss / thresholdL - 1);
  float qDistF = saturate(max(2 - dist / 3, 1.5 - miss / dist));
  float qDistD = 1 - saturate(miss / thresholdD - 1);
  float qDot = dot(normalize(newPosition - position), reflectDir);
  float qDir = saturate(qDot * 10 - 9);
  qDir *= saturate(newDist - dist - 1);
  float qRough = saturate(normal.y * 10 - 9);

  float quality = forceMode * qDistF * (1 - pow(dot(normal, viewDir), 2));
  quality = max(quality, qDist);
  quality = max(quality, qDir * qDistL);
  quality = max(quality, qRough * qDistL);
  quality *= saturate(2 - miss / min(dist * 2, 200));
  // quality *= lerpInvSat(dot2(newUv.xy - pin.Tex) * max(1, qDist * 100 - 150), 0.0002, 0.0003);

  #ifdef NO_DIMMING
    float dimQuality = 0;
  #else
    float dimQuality = saturate(2 - L * lerp(1, 0.25, saturate(normal.y)));
    dimQuality = 0.5 + 0.5 * dimQuality;
    quality *= lerp(qDistD, 1, saturate(dimQuality + forceMode));
  #endif
  if (newDepth == 1) quality = 0;

  float hitDistance = L;
  quality *= lerp(1, lerpInvSat(hitDistance, 200, 100), blur);

  float edgeK = forceMode ? 0.05 : 0.0001;
  float edge = saturate((min(newUv.x, 1 - newUv.x) + edgeK) / edgeK) 
    * saturate((min(newUv.y, 1 - newUv.y) + edgeK) / edgeK);
  L = saturate(pow(saturate(L / lerp(0.3, 10, DK1)), 1));

  RESULT R;
  R.main = float4(newUv.xy, L, fresnel * quality * edge);
  R.dimming = max(saturate(1 - qDistD) * dimQuality, saturate(miss / 50 - 1));

  float facingK = saturate(remap(dist, 0.6, 1, 1, 0));
  [branch]
  if (facingK && edge == 1) {
    float facingQ = dot(reflectDir, ssNormalDecode(txNormals.SampleLevel(samPoint, newUv.xy, 0).xyz));
    R.dimming = max(R.dimming, facingK * saturate(facingQ * 10));
  }

  // fix dim objects in distance, especially in fog, without messing up leds reflecting backwards nearby:
  R.dimming *= saturate(remap(newDepth, 0.998, 0.999, 1, 0));

  #ifdef USE_TAA
    float2 velocity = txMotion.SampleLevel(samLinear, pin.Tex, 0);
    float2 prevUV = pin.Tex - velocity;

    float4 history = txPrevious.SampleLevel(samLinearBorder0, prevUV, 0);
    float prevDimming = txPreviousDimming.SampleLevel(samLinearBorder0, prevUV, 0);

    if (history.w > 0.1 && history.z > 0.1 && history.w > R.main.w){
      float2 historyMotion = txMotion.SampleLevel(samLinear, history.xy, 0);
      history.xy += historyMotion;
      float2 historyMotionNew = txMotion.SampleLevel(samLinear, history.xy, 0);

      float maxVelocity = max(length(velocity), length(historyMotionNew));
      float speedK = saturate(length(maxVelocity) * 100 - 0.1);

      history.w *= lerp(0.8, 0.6, speedK);

      if (history.w > R.main.w || history.z < history.w * R.main.z){
        R.main.xyz = history.xyz;
        R.main.w = history.w;
        R.dimming = prevDimming;
      }
    }
  #endif

  if (any(R.main.xy < -0.5 || R.main.xy > 1.5)) R.main.xy = pin.Tex;
  // R.main.xy = saturate(R.main.xy);
  return R;
}