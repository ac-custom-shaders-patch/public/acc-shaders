#include "accSS_cbuffer.hlsl"
Texture2D txSsgi : register(TX_SLOT_SS_ARG1);
#define SAMPLE_BLUR_TEX(UV) float4(1, txSsgi.SampleLevel(samLinear, UV, 0).xyz)
#include "accSSAO_blurh_ps.fx"
