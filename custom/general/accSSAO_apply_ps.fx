#include "accSS_cbuffer.hlsl"

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

Texture2D txColor : register(TX_SLOT_COLOR);
Texture2D txSSAO : register(TX_SLOT_SS_ARG0);

float4 main(VS_Copy pin) : SV_TARGET {
  // return txColor.SampleLevel(samLinear, pin.Tex, 0);
  return txColor.SampleLevel(samLinear, pin.Tex, 0) 
    * (1 - txSSAO.SampleLevel(samLinearSimple, pin.Tex, 0).x);
}