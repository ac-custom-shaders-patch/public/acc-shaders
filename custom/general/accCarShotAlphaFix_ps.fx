#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  // return txDiffuse.SampleLevel(samPoint, pin.Tex, 0);
  float4 v = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);// + dithering(pin.PosH.xy) * 0;
  if (v.w == 0 && dot(v.rgb, 1)) v.w = 1;
  return v;
}