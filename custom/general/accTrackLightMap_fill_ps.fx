#include "accSS.hlsl"

Texture2D<float> txSurfaceDepth : register(TX_SLOT_SS_ARG0);

float main(VS_Copy pin) : SV_TARGET {
  float result = txSurfaceDepth.SampleLevel(samPointClamp, pin.Tex, 0);
  float total = 1;
  
  if (result == 1) {
    result = 0;
    total = 0.001;
  }

  [unroll]
  for (int y = -1; y <= 1; y++){
    [unroll]
    for (int x = -1; x <= 1; x++){
      if (y == 0 && x == 0) continue;
      float v = txSurfaceDepth.SampleLevel(samPointClamp, pin.Tex, 0, int2(x, y));
      if (v < 1) {
        result += v;
        total++;
      }
    }
  }
  return result / total;
}