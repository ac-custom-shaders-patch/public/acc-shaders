#define NO_ENTRY_POINT
#include "accSSVL_blur_ps.fx"
#include "include_new/base/cbuffers_ps.fx"

float4 main(VS_Copy pin) : SV_TARGET {
  // return blurVL(pin.Tex, float2(1, 0));
  // return txVL.SampleLevel(samLinear, pin.Tex, 0) + txColor.SampleLevel(samLinear, pin.Tex, 0) / 10;

  float depth = ssGetDepth(pin.Tex);
  float3 origin = ssGetPos(pin.Tex, depth);

  float4 resultBase = 0;
  float coneNearMin = 100;

  [unroll] for (int x = -2; x <= 2; x++)
  [unroll] for (int y = -2; y <= 2; y++){
    if (abs(x) + abs(y) == 4) continue;
    float4 s = txVL.SampleLevel(samPointClamp, pin.Tex, 0, int2(x, y));
    resultBase += s;
    coneNearMin = min(coneNearMin, s.w);
  }
  resultBase /= 21;

  // [unroll] for (int x = -1; x <= 1; x++)
  // [unroll] for (int y = -1; y <= 1; y++){
  //   if (abs(x) + abs(y) == 2) continue;
  //   float4 s = txVL.SampleLevel(samPointClamp, pin.Tex, 0, int2(x, y));
  //   resultBase += s;
  //   coneNearMin = min(coneNearMin, s.w);
  // }
  // resultBase /= 5;

  resultBase = blurVL(pin.Tex, float2(1, 0));
  coneNearMin = resultBase.w;

  // return 1;

  // return txColor.SampleLevel(samLinear, pin.Tex, 0);
  return float4(txColor.SampleLevel(samLinear, pin.Tex, 0).rgb 
    + resultBase.rgb * GAMMA_OR(0.03 * gMainBrightnessMult, 1) * saturate((length(origin) - coneNearMin) * 10), 1);
}