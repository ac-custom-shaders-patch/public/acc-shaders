#include "include/common.hlsl"
#include "include/bayer.hlsl"

#define KERNEL_COMPONENTS 2
#define KERNEL_RADIUS 8
#define KERNEL_COUNT 17
const static float4 KW = float4(0.411259, -0.548794, 0.513282, 4.561110);
const static float4 KR[] = {
  float4(0.014096, -0.022658, 0.000115, 0.009116),
  float4(-0.020612, -0.025574, 0.005324, 0.013416),
  float4(-0.038708, 0.006957, 0.013753, 0.016519),
  float4(-0.021449, 0.040468, 0.024700, 0.017215),
  float4(0.013015, 0.050223, 0.036693, 0.015064),
  float4(0.042178, 0.038585, 0.047976, 0.010684),
  float4(0.057972, 0.019812, 0.057015, 0.005570),
  float4(0.063647, 0.005252, 0.062782, 0.001529),
  float4(0.064754, 0.000000, 0.064754, 0.000000),
  float4(0.063647, 0.005252, 0.062782, 0.001529),
  float4(0.057972, 0.019812, 0.057015, 0.005570),
  float4(0.042178, 0.038585, 0.047976, 0.010684),
  float4(0.013015, 0.050223, 0.036693, 0.015064),
  float4(-0.021449, 0.040468, 0.024700, 0.017215),
  float4(-0.038708, 0.006957, 0.013753, 0.016519),
  float4(-0.020612, -0.025574, 0.005324, 0.013416),
  float4(0.014096, -0.022658, 0.000115, 0.009116),
};

float2 multComplex(float2 p, float2 q) {
  return float2(p.x * q.x - p.y * q.y, p.x * q.y + p.y * q.x);
}

float4 multComplex(float4 p, float4 q) {
  return float4(multComplex(p.xy, q.xy), multComplex(p.zw, q.zw));
}

cbuffer cbCamera : register(b10) {
  float4x4 gVPInv;
  float4x4 gVPDof0;
  float4x4 gVPDof1;
}

float computeR(float2 uv, float d) {
  float4 h = float4(uv, d, 1);
  float4 w = mul(h, gVPInv);
  w /= w.w;

  float4 uv0 = mul(w, gVPDof0);
  uv0 /= uv0.w;

  float4 uv1 = mul(w, gVPDof1);
  uv1 /= uv1.w;

  return sqrt(max(dot2(uv0.xy - uv), dot2(uv1.xy - uv)));
}

Texture2D<float> txDepth : register(t0);
Texture2D txInput : register(t1);
Texture2D<float> txBlurRadius : register(t2);
Texture2D txInR : register(t3);
Texture2D txInG : register(t4);
Texture2D txInB : register(t5);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float R = txBlurRadius.SampleLevel(samLinearSimple, pin.Tex, 0);
  float4 fallback = txInput.SampleLevel(samLinearSimple, pin.Tex, 0);

  #ifdef DOF_DEBUG
    float d = txDepth.SampleLevel(samLinearSimple, pin.Tex, 0);
    float r = computeR(pin.Tex, d);
    float4 mask = lerp(1, float4(0.5, 0.5, 1, 1), lerpInvSat(r, 0.0005, 0.001));
  #else
    float4 mask = 1;
  #endif

  [branch]
  if (R < 0.001) {
    return fallback * mask;
  }

  R *= 1 + getBayer(pin.PosH) * 0.1;
  float4 tr = 0;
  float4 tg = 0;
  float4 tb = 0;
  for (int i = -KERNEL_RADIUS; i <= KERNEL_RADIUS; ++i) {
    float2 coords = pin.Tex + float2(0, (float)i / KERNEL_RADIUS) * R;
    float4 sr = txInR.SampleLevel(samLinearClamp, coords, 0);
    float4 sg = txInG.SampleLevel(samLinearClamp, coords, 0);
    float4 sb = txInB.SampleLevel(samLinearClamp, coords, 0);
    float4 kr = KR[i + KERNEL_RADIUS];   
    tr += multComplex(sr, kr);
    tg += multComplex(sg, kr);
    tb += multComplex(sb, kr);    
  }    
  return lerp(fallback, float4(sqrt(float3(dot(tr, KW), dot(tg, KW), dot(tb, KW))), 1), lerpInvSat(R, 0.001, 0.002)) * mask;
}