#include "accAltTonemapping.hlsl"

static const float3x3 ACESInputMat = float3x3(
  0.59719, 0.35458, 0.04823,
  0.07600, 0.90834, 0.01566,
  0.02840, 0.13383, 0.83777
);

// ODT_SAT => XYZ => D60_2_D65 => sRGB
static const float3x3 ACESOutputMat = float3x3(
  1.60475, -0.53108, -0.07367,
  -0.10208,  1.10813, -0.00605,
  -0.00327, -0.07276,  1.07602
);

float3 RRTAndODTFit(float3 v) {
  float3 a = v * (v + 0.0245786) - 0.000090537;
  float3 b = v * (0.983729 * v + 0.4329510) + 0.238081;
  return a / b;
}

float3 applyTonemap(float3 color) {
  color = mul(ACESInputMat, color);
  color = RRTAndODTFit(color);
  color = mul(ACESOutputMat, color);
  return color;
}