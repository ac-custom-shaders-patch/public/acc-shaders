#include "accIBL.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  float3 normal = getNormal(pin.Tex);
  float3 o1 = normalize(cross(normal, float3(0, 1, 0)));
  float3 o2 = normalize(cross(normal, o1));
  float gBlurRadius = 0.9;
  float4 avg = 0;
  for (uint i = 0; i < CUBEMAP_BLUR_SIZE; ++i) {
    float4 dvec = CUBEMAP_BLUR[i];
    {
      float3 vecF = normal * dvec.z + o1 * (dvec.x * gBlurRadius) + o2 * (dvec.y * gBlurRadius);
      float4 value = max(0, txFirst.SampleLevel(samLinearSimple, vecF, (float)mipIndex));
      float weight = 1;
      weight *= dvec.w;
      avg += float4(value.rgb, 1) * weight;
    }
    {
      float3 vecF = normal * dvec.z - o1 * (dvec.x * gBlurRadius * 1.8) - o2 * (dvec.y * gBlurRadius * 1.8);
      float4 value = max(0, txFirst.SampleLevel(samLinearSimple, vecF, (float)mipIndex));
      float weight = 0.7;
      weight *= dvec.w;
      avg += float4(value.rgb, 1) * weight;
    }
  }
  avg /= avg.w;
  return avg;

	// return float4(PrefilterEnvMap(getRoughness(), getNormal(pin.Tex)), 1);
}