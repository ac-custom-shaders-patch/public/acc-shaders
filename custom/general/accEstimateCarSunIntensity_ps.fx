#define SHADOWS_FILTER_SIZE 2
#define NO_SHADOWS_CASCADES_TRANSITION

#include "include/common.hlsl"
#include "include_new/base/_include_ps.fx"

cbuffer cbData : register(b10){
  float3 gCarPosG;
  float gCarAO;
  float3 gCarNormal;
  float gPad1;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float main(VS_Copy pin) : SV_TARGET {
  float3 posW = gCarPosG - ksLightDirection.xyz * 2;
  float3 posC = posW - ksCameraPosition.xyz;
  float4 shadowPos = mul(float4(posW, 1), ksShadowMatrix0);
  float shadow = getShadow(posC, 1, gCarNormal, shadowPos, gCarAO);
  return saturate(-dot(gCarNormal, ksLightDirection.xyz) * 2 - 0.5) * saturate(dot(ksLightColor.rgb, 1) - 0.1) * shadow;
}
