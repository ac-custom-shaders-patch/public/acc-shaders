#include "accSS.hlsl"
#include "include_new/base/cbuffers_common.fx"
#define USE_POISSON

Texture2D txPreviousMap : register(TX_SLOT_SS_ARG5);

cbuffer cbData : register(b11){
  float gStep;
  float3 gPad0;
}

#ifdef USE_POISSON

  #include "include/poisson.hlsl"
  float4 main(VS_Copy pin) : SV_TARGET {
    float4 ret = 0;
    float tot = 0.0001;

    float radius = 0.004;
    if (gStep == 2) radius = 0.008;
    else if (gStep == 3) radius = 0.016;
    else if (gStep == 4) radius = 0.032;
    else if (gStep == 5) radius = 0.064;
    else if (gStep == 6) radius = 0.128;
    // radius = 0.007;
    if (gStep > 1) radius *= 2;
    else radius *= 1.2;
    

    #define PUDDLES_DISK_SIZE GAMMA_OR_STAT(32, 25)

    float dd = txDepth.Sample(samLinearClamp, pin.Tex);
    float3 dn = ssGetNormalMip(pin.Tex, gStep);

    [unroll]
    for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
      float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * radius;
      float2 sampleUV = pin.Tex + offset;
      float4 v = txPreviousMap.SampleLevel(samLinearClamp, sampleUV, 0);
      // v.rgb /= max(v.w, 0.001);

      float w = 1;
      // w = max(1e-8, v.w);
      // v.rgb *= max(1e-1, v.w);
      // w *= GAMMA_OR(1 / sqrt(max(gMainBrightnessMult * 0.001, dot(v.rgb, 1))), 1);
      // v.rgb *= 0.3 + 0.7 * v.w;
      
      float vdd = txDepth.Sample(samLinearClamp, sampleUV);
      // w *= saturate(1 - abs(vd - d) * 1e3);
      // w *= saturate((vdd - dd) * 100);

      float3 vdn = ssGetNormalMip(sampleUV, gStep);
      w *= 1 + pow(saturate(dot(vdn, dn)), 4);

      ret += v * w;
      tot += w;
    }
    ret /= tot;

    if (gStep == 1){
      ret.rgb = ret.rgb / (1 + dot(ret.rgb, GAMMA_OR(gMainBrightnessMult, 0.03)));
    }

    // ret.xyz *= ret.w;    
    return ret;
  }

#else

  #define R 2
  float4 main(VS_Copy pin) : SV_TARGET {
    float4 currentShot = 0;
    float totalWeight = 0;
    [unroll]
    for (int x = -R; x <= R; x++)
    [unroll]
    for (int y = -R; y <= R; y++){
      if (abs(x) + abs(y) == R + R) continue;
      // float weight = 1 / (1 + length(float2(x, y)));
      float weight = 1;
      currentShot += txPreviousMap.SampleLevel(samLinearClamp, pin.Tex, 0, int2(x, y) * 2) * weight;
      totalWeight += weight;
    }
    return currentShot / totalWeight;
  }

#endif
