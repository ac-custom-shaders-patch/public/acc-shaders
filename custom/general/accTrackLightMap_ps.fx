// #define MIPMAPPED_GBUFFER 4
#include "accSS.hlsl"
#include "include_new/base/cbuffers_ps.fx"

#define RADIUS 1
#define VERT_MULTIPLIER 10
#define SAMPLE_THRESHOLD 10.2

Texture2D<float> txSurfaceDepth : register(TX_SLOT_SS_ARG0);
Texture2D<float> txCaustics : register(TX_SLOT_SS_ARG1);
Texture2D<float4> txWaterNormal : register(TX_SLOT_SS_ARG2);

struct SamplingParams {
  float2 mapUV;
  float4 bilinearFactors;
};

SamplingParams getSamplingParams(float2 posXZ){
  SamplingParams ret;
  float2 mapRel = (posXZ - gMapPointB) / (gMapPointA - gMapPointB);
  ret.mapUV = mapRel;
  ret.bilinearFactors = bilinearFactors(ret.mapUV * gMapResolutionInv - 0.5);
  return ret;
}

float4 sampleTrackMap(float2 uv, float closeness){
  // return txColor.SampleLevel(samLinearBorder0, uv, 2);
  float dist = 1 - closeness;
  dist = dist / (1 + dist);
  return txColor.SampleLevel(samLinearBorder0, uv, clamp(0 + 15 * dist, 0, 3.7));

  float total = 0;
  float4 sum = 0;
  [unroll]
  for (int x = -1; x <= 1; x++)
  [unroll]
  for (int y = -1; y <= 1; y++){
    if (abs(x) + abs(y) == 2) continue;
    // if (abs(x) + abs(y) != 0) continue;
    // sum += txColor.SampleLevel(samLinearBorder0, uv, 3.5 - 3 * closeness, int2(x, y));
    sum += txColor.SampleLevel(samLinearBorder0, uv, 8 - 7.5 * closeness, int2(x, y));
    total++;
  }
  return sum / total;
}

#define DEPTH_FN min

float calculateDepth(SamplingParams S){
  float depthRaw = txSurfaceDepth.SampleLevel(samLinearClamp, S.mapUV, 0);
  return gMapOriginY - depthRaw * gMapDepthSize;
}

float calculateDepth(float2 uv){
  float depthRaw = txSurfaceDepth.SampleLevel(samLinearClamp, uv, 0);
  return gMapOriginY - depthRaw * gMapDepthSize;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float depth = ssGetDepth(pin.Tex);
  float3 origin = ssGetPos(pin.Tex, depth);
  float3 normal = ssGetNormal(pin.Tex);

  float2 offset = normal.xz == 0 ? 0 : normalize(normal.xz);
  SamplingParams S = getSamplingParams(origin.xz + offset * 0.5);
  float lightMapDepth = calculateDepth(S);

  float toGround = origin.y - lightMapDepth;
  float closeness = saturate(1 - toGround / 30);
  float2 mapUV = getSamplingParams(origin.xz + offset * (3 - 3 * closeness)).mapUV;
  float4 lightMap = sampleTrackMap(mapUV, closeness);
  float edgeK = min(min(mapUV.x, mapUV.y), min(1 - mapUV.x, 1 - mapUV.y));
  // lightMap.xyz = pow(saturate(lightMap.xyz), 0.9);

  // return lightMap;
  if (GAMMA_FIX_ACTIVE) {
    lightMap.rgb = lightMap.rgb / max(1 - lightMap.rgb, 0.00001);
  }
  // return 0;

  // return 0.5 + sampleTrackMap(mapUV, 0);

  // return txColor.SampleLevel(samLinearBorder0, mapUV, 0) * 0.2;

  float4 ret = lightMap 
    * gTrackLightIntensity
    * saturate(-normal.y * 0.5 + 0.5)
    // * GAMMA_OR(pow(1. / GAMMA_ALBEDO_BOOST, 2), 1)
    ;
    // * closeness;

  #ifdef WITH_CAUSTICS
    float2 waterPos = origin.xz + normalize(ksLightDirection.xyz).xz * toGround * -2;
    float2 waterUVPos1 = (ksCameraPositionWorld.xz + waterPos - extWindWater) * gCausticsScale;
    float2 waterUVPos2 = (ksCameraPositionWorld.xz + waterPos - extWindWater * 1.1) * gCausticsScale;
    float cau1 = txWaterNormal.SampleLevel(samLinearSimple, waterUVPos1, 0).w;
    float cau2 = txWaterNormal.SampleLevel(samLinearSimple, waterUVPos2 + ksGameTimeS * 0.1, 0).w;
    float cau = saturate(min(cau1, cau2) + 0.1);
    ret.rgb += 1
    * txCaustics.SampleLevel(samLinearBorder0, getSamplingParams(waterPos).mapUV, 0) 
    * pow(cau, GAMMA_OR(2.4, 1.4)) 
    * ksLightColor.xyz * GAMMA_OR(40, 4)
    * saturate(-dot(normal, ksLightDirection.xyz * float3(1, -1, 1))) 
    * smoothstep(0, 1, lerpInvSat(toGround, 3, 0) * lerpInvSat(toGround, 0, 0.2))
    // * 100
    ;
  #endif

  return ret * saturate(edgeK * 4 + saturate(2 - dot2(origin.xz) / 10000)) * (GAMMA_SCREENLIGHT_MULT * GAMMA_OR(0.1, 1));
}