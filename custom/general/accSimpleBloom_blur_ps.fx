#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float gStep;
  float gStepSqr;
  float2 gPixelSize;
}

#include "include/poisson.hlsl"
float4 main(VS_Copy pin) : SV_TARGET {
  float4 ret = 0;
  float2 radius = gStepSqr * gPixelSize * 4;
  // float2 radius = 0.01;

  #define PUDDLES_DISK_SIZE 8
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * radius;
    float2 sampleUV = pin.Tex + offset;
    float4 v = txDiffuse.SampleLevel(samLinearClamp, sampleUV, 0);
    float w = 1;
    ret += float4(v.rgb, 1) * w;
  }
  
  ret /= ret.w;
  // ret.rgb = max(0, ret.rgb - 1);
  // ret.rgb = saturate(ret.rgb);
  // ret.rgb = ret.rgb / (1 + ret.rgb);
  return ret;
}

// float4 main(VS_Copy pin) : SV_TARGET {
//   float4 hdr = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);
//   return float4(hdr.rgb, 1);
// }
