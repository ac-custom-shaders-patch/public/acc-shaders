#include "accSS_cbuffer.hlsl"
Texture2D txDiffuse : register(TX_SLOT_COLOR);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  return float4(0, txDiffuse.Sample(samLinearSimple, pin.Tex).rgb * 3);
}