cbuffer vertexBuffer : register(b0) {
  float4x4 ProjectionMatrix; 
};

struct VS_INPUT {
  float2 pos : POSITION;
  float4 col : COLOR0;
  float2 uv : TEXCOORD0;
  float4 mad : COLOR1;
};
  
struct PS_INPUT {
  float4 pos : SV_POSITION;
  float4 col : COLOR0;
  float2 uv : TEXCOORD0;
  float4 mad : COLOR1;
};
            
PS_INPUT main(VS_INPUT vin) {
  PS_INPUT pin;
  pin.pos = mul(ProjectionMatrix, float4(vin.pos.xy, 0, 1));
  pin.col = vin.col;
  pin.uv = vin.uv;
  pin.mad = vin.mad;
  // pin.mad = float2(1, 0);
  return pin;
}