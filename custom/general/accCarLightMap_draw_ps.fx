#include "include/common.hlsl"

#define MIPMAPPED_GBUFFER 2
#define MIPMAPPED_GBUFFER_FIXED
#include "general/accSS.hlsl"
#include "include_new/base/cbuffers_common.fx"

Texture2D txDiffuse : register(t0);
// Texture2D txShape : register(t1);

cbuffer cbData : register(b11){
  float3 gPos;
  float gRadius;
  float4x4 gWorldToCar;
  float3 gDir;
  float gIntensity;
  float3 gCarSize;
  float gPad1;
}

float3 getPosRel(float3 posW){
  return mul(float4(posW, 1), gWorldToCar).xyz;
}

struct VS_Clm {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

float4 main(VS_Clm pin) : SV_TARGET {
  float2 ssUV = pin.PosH.xy * gSize.zw;
  float3 origin = ssGetPos(ssUV, ssGetDepth(ssUV)) + ksCameraPosition;

  [branch]
  if (dot2(gPos - origin) > 100){
    return 0;
  }

  float3 normal = ssGetNormal(ssUV);
  
  float3 posRel = getPosRel(origin);
  float distance = length(posRel);
  float distance2 = max(abs(posRel.x), abs(posRel.z));

  float2 posA = abs(posRel.xz);
  posA.y = posA.y * 2.5 - 1.5;
  distance2 = posA.x > 0.2 && posA.y > 0.2 ? length((posA - 0.2)) + 0.2 : max(posA.x, posA.y);

  // float retMult;
  float3 dirBase;
  {
    // float len = length(posRel);
    float3 dir = normalize(posRel);
    dirBase = dir;
    // float2 uv = normalize(dir.xz) * float2(-1, -1) * (1 - dir.y) * 0.5 + 0.5;
    // float4 shape = txShape.SampleLevel(samLinearSimple, uv, 0);
    // retMult = lerpInvSat(shape.w * 2 / len, 0.3, 0.2);
  }

  float cap = 0.7;
  if (abs(posRel.x) > cap){
    posRel.x = lerp(posRel.x, sign(posRel.x), lerpInvSat(abs(posRel.x), cap, 0.85));
  }

  if (abs(posRel.z) > cap){
    posRel.z = lerp(posRel.z, sign(posRel.z), lerpInvSat(abs(posRel.z), cap, 0.85));
  }

  float3 dir = normalize(posRel);
  float2 uv = normalize(dir.xz) * float2(-1, -1) * (1 - dir.y) * 0.5 + 0.5;

  // return 20
  //   * txDiffuse.SampleLevel(samLinearClamp, uv, sqrt(lerpInvSat(distance2, 1, 2)) * 2 + 1) ;

  // float marginX = 100 * fwidth(pin.Tex.x);
  // float marginY = 100 * fwidth(pin.Tex.y);
  float marginX = 0.2;
  float marginY = 0.2;

  // if (min(fwidth(pin.Tex.x), fwidth(pin.Tex.y)) < 0.00001) return float4(0, 1, 0, 1);

  pin.Tex.x = sign(pin.Tex.x) * lerpInvSat(abs(pin.Tex.x), 1 - marginX, 1);
  pin.Tex.y = sign(pin.Tex.y) * lerpInvSat(abs(pin.Tex.y), 1 - marginY, 1);

  float4 col = txDiffuse.SampleLevel(samLinearClamp, uv, sqrt(lerpInvSat(distance2, 1, 2)) * 2 + 1);
  if (GAMMA_FIX_ACTIVE) col.rgb = col.rgb / max(1 - col.rgb, 0.00001);

  float boost = lerpInvSat(dirBase.y, 0.1, 0) * 0.3;
  return gIntensity
    * col
    * smoothstep(0, 1, lerpInvSat(distance2, 1.1 - boost, 1.4 - boost)) 
    * pow(lerpInvSat(distance * (1 + pow(dirBase.z, 2) * 0.3), 2, 1), 2) 
    * saturate(1 - dot2(pin.Tex))
    * GAMMA_SCREENLIGHT_MULT;
    // * GAMMA_OR(pow(1. / GAMMA_ALBEDO_BOOST, 2), 1)
    // * retMult
    // + float4(1, 0, 0, 1)
    // + float4(frac(pin.Tex * 10) * 4, 0, 1)
    // + float4(saturate(1 - dot2(pin.Tex)) * 10, 0, 0, 1)
    ;
}