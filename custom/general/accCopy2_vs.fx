struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
  float2 AbsTex : TEXCOORD1;
};

VS_Copy main(uint id: SV_VertexID) {
  VS_Copy vout;
  float2 v = float2(id % 2, id / 2) * 2;
  vout.Tex = float2(v.x, 1 - v.y); 
  vout.AbsTex = vout.Tex;
  vout.PosH = float4(v * 2 - 1, 0, 1);
  return vout;
}
