Texture2DMS<float4> txDiffuse : register(t0); 

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 resValue = 0;
  [unroll]
  for (uint i = 0; i < 8; ++i){
    resValue += txDiffuse.Load(pin.PosH.xy, i);
  }
  return resValue / 8;
}