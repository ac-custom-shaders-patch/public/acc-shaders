#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float2 gPixelSize;
  float gThreshold;
  float gBaseMult;
}

#include "include/poisson.hlsl"
float4 main(VS_Copy pin) : SV_TARGET {
  float4 ret = 0;

  #define PUDDLES_DISK_SIZE 8
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * gPixelSize;
    float2 sampleUV = pin.Tex + offset;
    float4 v = txDiffuse.SampleLevel(samLinearClamp, sampleUV, 0);
    ret += float4(v.rgb, 1);
  }
  
  ret /= ret.w;

  float l = max(0, luminance(ret.rgb) - gThreshold);
  l = l / (1 + l);
  ret.rgb *= l * gBaseMult;
  // ret.rgb = saturate(ret.rgb);
  ret.rgb = ret.rgb / (1 + 2 * luminance(ret.rgb));
  // ret.r = tonemap(ret.r);
  // ret.g = tonemap(ret.g);
  // ret.b = tonemap(ret.b);
  return ret;
}

// float4 main(VS_Copy pin) : SV_TARGET {
//   float4 hdr = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);
//   return float4(hdr.rgb, 1);
// }
