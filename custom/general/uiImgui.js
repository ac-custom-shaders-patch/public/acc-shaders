[
  'uiImgui_blur_ps',
  'uiImgui_ps',
  'uiImgui_sharpen_ps',
  'uiImgui_subtract_ps',
  'uiImgui_tonemap_ps',
].map(x => ({
  source: `${x}.fx`,
  target: FX.Target.PS,
  entry: 'main',
  saveAs: `${x}_s.fxo`,
  defines: {
    'INPUT_R8_FORMAT': 1
  }
}))