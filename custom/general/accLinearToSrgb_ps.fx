#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

cbuffer cbData : register(b10) {
  // TfParams gParams;
  float4 _genParams;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  return pow(max(0, txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0) * _genParams.x), _genParams.y);
  // return pow(max(0, txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0)), 1 / 2.2);
}