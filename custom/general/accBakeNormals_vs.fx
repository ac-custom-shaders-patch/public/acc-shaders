#include "include/common.hlsl"

struct VS_IN_ac {
  AC_INPUT_ELEMENTS
};

struct PS_IN_Solid {
  float4 PosH : SV_POSITION;
  float3 NormalL : COLOR;
};

PS_IN_Solid main(VS_IN_ac vin) {
  PS_IN_Solid vout;
  vout.PosH = float4((frac(vin.Tex) * 2 - 1) * float2(1, -1), 1, 1);
  vout.NormalL = vin.NormalL;
  return vout;
}
