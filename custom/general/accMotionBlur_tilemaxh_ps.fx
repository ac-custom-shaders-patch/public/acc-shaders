#define MIPMAPPED_GBUFFER 2
#define MIPMAPPED_GBUFFER_FIXED
#define SS_VIEWLESS
#include "accSS.hlsl"

Texture2D<float2> txMotionL : register(TX_SLOT_SS_ARG0);
Texture2D<float2> txMotion : register(TX_SLOT_MOTION);

float2 findMax(Texture2D<float2> tex, float2 uv, int2 dir){
  float2 res = 0;
  [unroll]
  for (int i = -8; i <= 7; i++){
    float2 v = tex.SampleLevel(samLinearBorder0, uv, 0, dir * i);
    [flatten]
    if (dot2(v) > dot2(res)) res = v;
  }
  return res;
}

float2 main(VS_Copy pin) : SV_TARGET {
  #ifdef BLURV
    return findMax(txMotionL, pin.Tex, int2(0, 1));
  #else
    return findMax(txMotion, pin.Tex, int2(1, 0));
  #endif
}

// float2 findMax(Texture2D<float2> tex, float2 uv, float2 dir){
//   float2 res = 0;
//   for (int i = -16; i < 15; ++i){
//     float2 v = tex.SampleLevel(samPointClamp, uv + dir * i, 0);

//     [flatten]
//     if (dot2(v) > dot2(res)) res = v;
//   }
//   return res;
// }

// float2 main(VS_Copy pin) : SV_TARGET {
//   #ifdef BLURV
//     return findMax(txMotionL, pin.Tex, float2(0, gSize.w));
//   #else
//     return findMax(txMotionLR, pin.Tex, float2(gSize.z, 0));
//   #endif
// }