#include "include/common.hlsl"

Texture2D txArea : register(t0);
Texture2D txWireframe : register(t1);

cbuffer cbData : register(b10) {
  float showWireframe;
  float showMapping;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float wireframeValue = txWireframe.SampleLevel(samLinearSimple, pin.Tex, 0).a;
  float mask = saturate(1 - txArea.SampleLevel(samLinearSimple, pin.Tex, 0).a - wireframeValue);
  float checkerPattern = frac((pin.PosH.x + pin.PosH.y) / 12) > 0.5;
  float4 checkerReturn = float4(lerp(0.4, 0.6, checkerPattern.xxx), mask * 0.3);
  return lerp(checkerReturn * showMapping, float4(1, 1, 0, 1), wireframeValue * showWireframe);
}