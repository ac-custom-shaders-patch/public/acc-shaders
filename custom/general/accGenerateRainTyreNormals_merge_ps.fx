#include "include/common.hlsl"
#include "include/generate_rain_tyres.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  uint pixelPhase = dot(uint2(pin.PosH.xy) % 2, uint2(1, 2));
  if (pixelPhase < phase) discard;
  return txDiffuse.SampleLevel(samPoint, pin.Tex, 0);
}