#include "include/common.hlsl"

TextureCube txCube : register(TX_SLOT_REFLECTION_CUBEMAP);

cbuffer cbData : register(b10) {
  float4x4 gViewProjInv;
  float bias;
  float zoom;
  float yPartExp; // 0.25
  float yPartMult; // 1.5
  float gMult;
}

float3 ssGetPos(float2 uv, float depth){
  float4 p = mul(float4(uv.xy, depth, 1), gViewProjInv);
  return p.xyz / p.w;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float main(VS_Copy pin) : SV_TARGET {
  if (pin.Tex.y > 0.5){
    pin.Tex.y = pin.Tex.y * 2 - 1;
    float3 pos = normalize(float3(pin.Tex.x - 0.5, 1 - pow(dot2(pin.Tex - 0.5), yPartExp) * yPartMult, pin.Tex.y - 0.5));
    return gMult * luminance(txCube.SampleLevel(samLinearSimple, pos, bias + 1).rgb);
  }

  pin.Tex.y = pin.Tex.y * 2;
  float3 pos = -normalize(ssGetPos(lerp(pin.Tex, 0.5, zoom), 1000));
  pos.x = -pos.x;
  return gMult * luminance(txCube.SampleLevel(samLinearSimple, pos, bias).rgb);
}
