#include "accSS.hlsl"
#include "accSSDashboard.hlsl"

#define RADIUS 1
#define VERT_MULTIPLIER 10
#define SAMPLE_THRESHOLD 10.2

Texture2D txDashboardColor : register(TX_SLOT_SS_ARG0);
Texture2D txDashboardPos : register(TX_SLOT_SS_ARG1);
Texture2D txDashboardNormal : register(TX_SLOT_SS_ARG2);

float3 calculateContribution(float2 uv, float3 origin, float3 normal, 
    float mipLayer, float radius = 0.3){
  float4 colorData = txDashboardColor.SampleLevel(samLinearClamp, uv, mipLayer);
  float4 posData = txDashboardPos.SampleLevel(samLinearClamp, uv, mipLayer);
  float4 normalData = txDashboardNormal.SampleLevel(samLinearClamp, uv, mipLayer);
  if (dot(colorData, 1) == 0) return 0;
  // if (dot(posData, 1) == 0) return 0;

  float3 lightNormal = normalize(normalData.xyz);
  // float3 lightNormal = (normalData.xyz);

  float3 toLight = posData.xyz - origin;
  float toLightDistance = max(length(toLight), 0.01);
  float3 toLightN = toLight / toLightDistance;

  float LdotN = saturate(dot(toLightN, normal) + 0.2);
  // float spotK = saturate(dot(-toLightN, lightNormal) * 4 - 0.5);
  float spotK = saturate(dot(-toLightN, lightNormal));
  float mult = 1
    * LdotN
    // * lerp(0.2, 1, spotK)
    * spotK
    * pow(saturate(remap(toLightDistance, 0, radius, 1, 0)), 2)
    ;
  // return lightNormal > 0 || lightNormal < 1 ? 1 : 0;
  // return abs(dot(toLightN, lightNormal));
  // return colorData.rgb;
  // return colorData.rgb * spotK;
  // return LdotN;
  // return frac(toLightDistance * 2);
  // return 1 * mult;
  return colorData.rgb * mult;
}

float3 lumaBasedReinhardToneMapping(float3 color) {
	float luma = max(1, dot(color, 0.33));
	float toneMappedLuma = luma / (1 + luma);
	color *= toneMappedLuma / luma;
	return color;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float depth = ssGetDepth(pin.Tex);
  float3 origin = ssGetPos(pin.Tex, depth);
  float3 normal = ssGetNormal(pin.Tex);

  float3 result = 0
    + calculateContribution(pin.Tex, origin, normal, 5, 0.3) * 10
    // + calculateContribution(pin.Tex, origin, normal, 6, 0.3)
    ;
  // result /= result + 0.15;
  // result *= 0.001;

  result = lumaBasedReinhardToneMapping(result);
  // result.b = saturate(normal.y) * 0.1;

  // result = 0;
  return float4(result, 1);
}