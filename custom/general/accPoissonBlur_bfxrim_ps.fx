#include "include/common.hlsl"
#include "include/poisson.hlsl"

Texture2D txNoise : register(TX_SLOT_NOISE);
Texture2D<float> txPreviousMap : register(t0);

cbuffer cbData : register(b10){
  float gRadius;
  float3 gPad0;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float main(VS_Copy pin) : SV_TARGET {
  float2 random = normalize(txNoise.Load(int3(pin.PosH.xy % 32, 0)).xy);

  float ret = 0;
  float tot = 0;
  #define PUDDLES_DISK_SIZE 96
  // [unroll]
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * gRadius;
    offset = reflect(offset, random);

    float2 sampleUV = pin.Tex + offset;
    float v = txPreviousMap.SampleLevel(samPointClamp, sampleUV, 0) == 1 ? 1 : 0;
    float w = 1;
    ret += v * w;
    tot += w;
  }
  ret /= tot;   
  return saturate(ret) * lerpInvSat(dot2(pin.Tex * 2 - 1), 1, 0.95);
}
