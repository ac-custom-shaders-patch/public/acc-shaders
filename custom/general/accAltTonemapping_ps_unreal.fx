#include "accAltTonemapping.hlsl"

float applyTonemap(float x) {
  return pow(x / (x + 0.155) * 1.019, 2.8);
}

float3 applyTonemap(float3 color){
  return float3(applyTonemap(color.x), applyTonemap(color.y), applyTonemap(color.z));  
}

