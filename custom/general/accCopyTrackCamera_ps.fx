#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

cbuffer cbData : register(b10) {
  float4 gRegion;
  float4 gHDRTweaks;
  float gAlphaValue;
  float gGammaInv;
  float gSaturation;
  float gBrightness;
  float gFishEye;
  float gChromaticAberration;
  float gVignette;
  float gFlipX;
  float gFlipY;
  float3 gPad;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float2 fakeLens(float2 uv){
  float2 offset = uv * 2 - 1;
  offset *= lerp(lerp(1, 0.9, gFishEye), 1, pow(length(offset), 4)) * lerp(1, 0.8, gFishEye);
  return offset * 0.5 + 0.5;
}

float3 getValue(float2 uv){
  float distance = length(uv - 0.5);
  float3 col = txDiffuse.SampleLevel(samLinearBorder0, uv, 0).rgb;

  [branch]
  if (gChromaticAberration){
    col = float3(
      col.r,
      txDiffuse.SampleLevel(samLinearBorder0, uv + float2(distance * 0.006 * gChromaticAberration, 0), 0).g,
      txDiffuse.SampleLevel(samLinearBorder0, uv + float2(distance * 0.012 * gChromaticAberration, 0), 0).b);
  }

  col = pow(max(0, col * gHDRTweaks.x), gHDRTweaks.y);
  return col * pow(saturate(1 - distance * 1.41), gVignette);
}

float4 main(VS_Copy pin) : SV_TARGET {
  if (gFlipX) pin.Tex.x = 1 - pin.Tex.x;
  if (gFlipY) pin.Tex.y = 1 - pin.Tex.y;
  pin.Tex = fakeLens(pin.Tex);

  float3 hdr = getValue(pin.Tex);
  float3 ldr = pow(saturate(hdr / (1 + hdr)), gGammaInv);
  float3 adj = lerp(luminance(ldr), ldr, gSaturation) * gBrightness;
  return float4(adj, gAlphaValue);
}