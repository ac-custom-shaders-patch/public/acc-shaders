#include "include/common.hlsl"
#include "include/poisson.hlsl"

Texture2D<float4> txDiffuse : register(t5);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float main(VS_Copy pin) : SV_TARGET {
  float ret = 0;
  float tot = 0;
  float radius = 0.2;

  #define PUDDLES_DISK_SIZE 16
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * radius;
    float2 sampleUV = pin.Tex + offset;
    float4 v = txDiffuse.SampleLevel(samLinearClamp, sampleUV, 0);
    float w = 1 + v.a;
    ret += v.x * w;
    tot += w;
  }
  ret /= tot;
  return ret;
}