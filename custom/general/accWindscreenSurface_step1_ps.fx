#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

cbuffer _cbExtWindscreenReprojection : register(b10) {
  float2 gDir;
  float2 _pad;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 findHit(float2 uv, float2 dir, out float distance) {
  for (int x = 1; x <= 20; ++x){
    float2 offset = dir * x * 10;
    float4 ret1 = txDiffuse.SampleLevel(samPointClamp, uv + offset, 0);
    if (ret1.w) {
      for (int y = -9; y < 0; ++y){
        float2 offset = dir * (x * 10 + y);
        float4 ret1 = txDiffuse.SampleLevel(samPointClamp, uv + offset, 0);
        if (ret1.w) { distance = x * 10 + y; return ret1; }
      }
      distance = x * 10;
      return ret1;
    }
  }
  distance = 0;
  return 0;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float4 ret = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);
  if (ret.w) return ret;

  float dl, dr;
  float4 hl = findHit(pin.Tex, gDir, dl);
  float4 hr = findHit(pin.Tex, -gDir, dr);
  if (hl.w && hr.w) {
    return (hl * dr + hr * dl) / (dl + dr);
  } else {
    return hl.w ? hl : hr;
  }
  
  return 0;
}