#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float opacity;
  float2 screenSize;
  float resolutionMult;

  float2 resolutionMultStep;
  float hdrThreshold; // 30
  float hdrExposure;  // 1.5

  float weightBoost;
  float weightExp;
  float2 pad;
}

Texture2D txDiffuse : register(t0);
Texture2D txHDR : register(t1);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float3 tonemapping(float3 color) {
	float exposure = hdrExposure;
	color *= exposure / (1 + color / exposure);
	return color;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float2 uv = pin.Tex;

  float2 pos = uv * screenSize;
  uint pixel_x = uint(resolutionMult - 1) - uint(pos.x) % uint(resolutionMult);
  uint pixel_y = uint(pos.y) % uint(resolutionMult);
  if (pixel_x != uint(resolutionMultStep.x) || pixel_y != uint(resolutionMultStep.y)) clip(-1);
  
  float3 input = txDiffuse.Load(float3(uv * screenSize / resolutionMult, 0), int2(0, 0)).rgb;
  float3 hdr = tonemapping(max(txHDR.Load(float3(uv * screenSize / resolutionMult, 0), int2(0, 0)).rgb - hdrThreshold, 0));
  float weight = 1 + dot(pow(saturate(input), weightExp), weightBoost);
  return float4(input + hdr, 1) * weight;
}