#include "accAltTonemapping.hlsl"

cbuffer cbData : register(b1) {
  float W;
  float3 _pad;
}

// From: https://iolite-engine.com/blog_posts/minimal_agx_implementation
float3 applyTonemap(float3 val) {
  float3 higher = pow((val + 0.055) / 1.055, 2.4);
  float3 lower = val / 12.92;  
  val = lerp(higher, lower, val < 0.04045);

  val = saturate((log2(mul(float3x3(
    0.842479062253094, 0.0784335999999992, 0.0792237451477643,
    0.0423282422610123,  0.878468636469772,  0.0791661274605434,
    0.0423756549057051, 0.0784336, 0.879142973793104), val)) + 12.47393) / 16.5);

  float3 va2 = val * val;
  float3 va4 = va2 * va2;  
  val = 15.5 * va4 * va2 - 40.14 * va4 * val + 31.96 * va4 
    - 6.868 * va2 * val + 0.4298 * va2 + 0.1191 * val - 0.00232;

  float luma = dot(val, float3(0.2126, 0.7152, 0.0722));
  float punch = saturate((W - 2) / 10);
  val = luma + (1 + 0.4 * punch) * (pow(max(val, 0), 1 + 0.35 * punch) - luma);

  return mul(float3x3(
    1.19687900512017, -0.0980208811401368, -0.0990297440797205,
    -0.0528968517574562, 1.15190312990417, -0.0989611768448433,
    -0.0529716355144438, -0.0980434501171241, 1.15107367264116), val);
}
