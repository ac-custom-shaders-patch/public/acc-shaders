#include "include/common.hlsl"
// #include "include/gaussian.hlsl"

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float2 gPixelSize;
  float2 _pad;
}

float GaussianBlur15(Texture2D<float> tex, SamplerState sam, float2 uv, float2 pixelOffset) {
  // kernel width 15 x 15
  // const float gWeights[4] = { 0.2496147, 0.1924633, 0.0514763, 0.0064457 };
  // const float gWeights[4] = { 0.2496147, 0.1924633, 0.0514763, 0.0064457 };
  // const float gOffsets[4] = { 0.6443417, 2.3788476, 4.2911105, 6.2166071 };
  const float gWeights[2] = { 0.4490798, 0.0509202 };
  const float gOffsets[2] = { 0.5380487, 2.0627797 };
  float ret = 0;
  float t = 0;
  [unroll(16)]
  for (int i = 0; i < 2; ++i) {
    float2 offset = gOffsets[i] * pixelOffset;
    float s0 = tex.SampleLevel(sam, uv + offset, 0);
    float s1 = tex.SampleLevel(sam, uv - offset, 0);
    float w0 = gWeights[i] * (1 + s0 * 0.1);
    float w1 = gWeights[i] * (1 + s1 * 0.1);
    ret += w0 * s0 + w1 * s1;
    t += w0 + w1;
  }
  return ret / t;
}

float main(VS_Copy pin) : SV_TARGET {
  return GaussianBlur15(txDiffuse, samLinearClamp, pin.Tex, gPixelSize);
}