#include "include/common.hlsl"

cbuffer cbCamera : register(b10) {
  float4 gRegion;
  float2 gUVMargin;
  float gDepthThreshold;
}

Texture2D txPrevColor : register(t0);
Texture2D<float> txPrevDepth : register(t1);
Texture2D txPrevTrackOnly : register(t2);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
  noperspective float MixMin : TEXCOORD1;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float2 uv = gRegion.xy + gRegion.zw * clamp(pin.Tex, gUVMargin, 1 - gUVMargin);
  float4 prev = txPrevColor.SampleLevel(samLinearClamp, uv, 0);
  float4 track = txPrevTrackOnly.SampleLevel(samLinearClamp, uv, 0);
  return lerp(track, min(prev, track), pin.MixMin);
}