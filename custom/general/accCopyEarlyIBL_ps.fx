#include "accIBL.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  float4 ret = txFirst.Sample(samLinearSimple, getNormal(pin.Tex));
  if (mipIndex == 0) ret = applyColorCorrection(ret);
  return ret;
}
