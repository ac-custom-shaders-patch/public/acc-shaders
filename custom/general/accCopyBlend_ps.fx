#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float2 gPointerCoords;
  float2 gPointerClickCoords;
  float2 gGlowRadiusK;
  float gClickIntensity;
  float gModalActive;
  float gBlendWithBg;
  float gFluentDark;
  float2 _pad0;
}

Texture2D txUITop : register(t0);
Texture2D txUIBase : register(t1);
Texture2D txGlow : register(t2);
Texture2D<float> txFluent : register(t3);
Texture2D txScene : register(t4);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 getBlurred(Texture2D tex, SamplerState samplerState, float2 uv, float blurMult){
  float4 b = tex.SampleLevel(samplerState, uv, blurMult);
  if (blurMult <= 0.005){
    return b;
  }

  [unroll]
  for (int y = -2; y <= 2; y++)
  [unroll]
  for (int x = -2; x <= 2; x++){
    if (abs(x) == 2 && abs(y) == 2 || x == 0 && y == 0) continue;
    b += tex.SampleLevel(samplerState, uv, blurMult, int2(x, y));
  }
  b /= 21;
  return b;
}

// float3 fluentLight(float2 uv, float2 lightSource, float lightIntensity, float2 fluentDir){
//   if (!dot(fluentDir, 1)) return 0;
//   float2 delta = gPointerCoords - uv;
//   delta *= gGlowRadiusK / 50;
//   float2 fluentDir = normalize(fluentDir * 2 - 1) * float2(1, -1);
//   return saturate(1 / (0.5 + dot2(delta) * 0.4))
//     * lerp(1, saturate(dot(normalize(gPointerCoor  ds - uv), fluentDir) * 0.5 + 0.5), saturate(length(delta) * 0.5))
//     * lightIntensity;
// }

float fluentLight(float2 uv, float2 lightSource, float lightIntensity, float fluentColor){
  return saturate(1 / (0.5 + dot2((gPointerCoords - uv) * gGlowRadiusK / 50) * 0.4)) * lightIntensity * fluentColor;
}

float4 main(VS_Copy pin) : SV_TARGET {
  // UI color
  float4 uiColor = txUIBase.Sample(samLinearSimple, pin.Tex);
  // uiColor.rgb /= max(0.0000001, uiColor.a);

  // discard;
  // return uiColor;
  // return txScene.SampleLevel(samLinearSimple, pin.Tex, 0);
  // uiColor = min(uiColor, 0.1);

  [branch]
  if (gModalActive){
    uiColor = lerp(uiColor, 
      getBlurred(txUIBase, samLinearClamp, pin.Tex, gBlendWithBg ? gModalActive * 1.5 : 0), 
      saturate(gModalActive * 10));
    if (gBlendWithBg) {
      uiColor = lerp(uiColor, 1, gModalActive * 0.2);
    } else {
      uiColor = lerp(uiColor, lerp(1, float4((float3)gModalActive * 0.4, 1), uiColor.a), gModalActive * 0.4);
    }
  }

  float4 uiOverColor = txUITop.Sample(samLinearSimple, pin.Tex);
  uiOverColor.rgb /= max(0.5, uiOverColor.a);
  uiColor.rgb = uiColor.rgb * (1 - uiOverColor.a) + uiOverColor.rgb;
  uiColor.a = 1 - (1 - uiColor.a) * (1 - uiOverColor.a);

  // Mixing it a bit of glow
  float4 glowColor0 = txGlow.SampleLevel(samLinearBorder0, pin.Tex, 0);
  float4 glowColor = txGlow.SampleLevel(samLinearClamp, pin.Tex, lerp(2.8, 2.4, uiColor.a));
  // return glowColor;

  glowColor.rgb = glowColor.rgb * lerp(0.4, 0.8, saturate(1 - uiColor.a * uiColor.a * 2 * dot(uiColor, 1))) * (1 - gModalActive);
  // glowColor.rgb = uiColor.a;

  // uiColor.rgb += 2 * glowColor.rgb 
  //   // * saturate(1 - dot(glowColor0.rgb, 10)) // why? 
  //   * lerp(0.5, 1, saturate(1 - uiColor.a * uiColor.a * 2 * dot(uiColor, 1))) * (1 - gModalActive);
  // uiColor.a += dot(glowColor.rgb, 0.6) * saturate((0.5 - uiColor.a) * 10) * (1 - gModalActive);

  // return glowColor;
  if (!uiColor.a && !dot(glowColor.rgb, 100)) discard;

  // Mixing with blurred scene
  [branch]
  if (gBlendWithBg && uiColor.a){
    float blurMult = max(gModalActive, gBlendWithBg * saturate(uiColor.a)) * 3;
    float4 sceneColor = txScene.SampleLevel(samLinearClamp, pin.Tex, blurMult);
    uiColor.rgb = lerp(sceneColor.rgb, uiColor.rgb / uiColor.a, uiColor.a);
  } else {
    // uiColor.rgb /= uiColor.a;
    // uiColor.a = pow(saturate(uiColor.a), 0.3);
  }

  // Adding a bit of Fluent Design light
  float fluentColor = txFluent.Sample(samPoint, pin.Tex);
  // uiColor.rgb += (gFluentDark ?  -0.05 : 0.1)
  //   * (fluentLight(pin.Tex, gPointerClickCoords, gClickIntensity, fluentColor) 
  //   + fluentLight(pin.Tex, gPointerCoords, 1, fluentColor));
  //  fluentColor = gModalActive;
  uiColor.rgb += (gFluentDark ?  -0.05 : 0.1) * fluentLight(pin.Tex, gPointerCoords, 1, fluentColor);

  // Opaque unless very transparent
  if (gBlendWithBg){
    if (uiColor.a > 0.1 - gModalActive) {
      uiColor.a = 1;
      // uiColor.rgb /= max(uiColor.a, 0.001);
    } else {
      uiColor.a /= 0.1;
    }
    uiColor.rgb *= uiColor.a;
  }

  uiColor.rgb += glowColor.rgb;

  return uiColor;
}