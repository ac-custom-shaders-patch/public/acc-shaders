#include "include/common.hlsl"
#include "include/bayer.hlsl"

#define KERNEL_RADIUS 8
#define KERNEL_COUNT 17
const static float4 KR[] = {
  float4(0.014096, -0.022658, 0.000115, 0.009116),
  float4(-0.020612, -0.025574, 0.005324, 0.013416),
  float4(-0.038708, 0.006957, 0.013753, 0.016519),
  float4(-0.021449, 0.040468, 0.024700, 0.017215),
  float4(0.013015, 0.050223, 0.036693, 0.015064),
  float4(0.042178, 0.038585, 0.047976, 0.010684),
  float4(0.057972, 0.019812, 0.057015, 0.005570),
  float4(0.063647, 0.005252, 0.062782, 0.001529),
  float4(0.064754, 0.000000, 0.064754, 0.000000),
  float4(0.063647, 0.005252, 0.062782, 0.001529),
  float4(0.057972, 0.019812, 0.057015, 0.005570),
  float4(0.042178, 0.038585, 0.047976, 0.010684),
  float4(0.013015, 0.050223, 0.036693, 0.015064),
  float4(-0.021449, 0.040468, 0.024700, 0.017215),
  float4(-0.038708, 0.006957, 0.013753, 0.016519),
  float4(-0.020612, -0.025574, 0.005324, 0.013416),
  float4(0.014096, -0.022658, 0.000115, 0.009116),
};

Texture2D<float> txDepth : register(t0);
Texture2D txDiffuse : register(t1);
Texture2D<float> txBlurRadius : register(t2);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

struct PS_OUT {
  float4 r : SV_Target0;
  float4 g : SV_Target1;
  float4 b : SV_Target2;
};

PS_OUT main(VS_Copy pin) {
  float R = txBlurRadius.SampleLevel(samLinearSimple, pin.Tex, 0);

  [branch]
  if (R < 0.0005) {
    discard;
    return (PS_OUT)0;
  }

  R *= 1 + getBayer(pin.PosH) * 0.1;
  PS_OUT o = (PS_OUT)0;
  for (int i = -KERNEL_RADIUS; i <= KERNEL_RADIUS; ++i) {
    float4 sv = pow(txDiffuse.SampleLevel(samLinearClamp, pin.Tex + float2((float)i / KERNEL_RADIUS, 0) * R, 0), 2);
    o.r += sv.r * KR[i + KERNEL_RADIUS];
    o.g += sv.g * KR[i + KERNEL_RADIUS];
    o.b += sv.b * KR[i + KERNEL_RADIUS];      
  }
  return o;
}

