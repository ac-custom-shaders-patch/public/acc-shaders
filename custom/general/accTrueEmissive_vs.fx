#define OBJECT_SHADER
#define ALLOW_CAR_FLEX

#include "include/samplers.hlsl"
#include "include_new/base/textures_ps.fx"

#ifdef MATERIAL_EMISSIVE
  #define EM_TARGET_VS_OVERRIDE
  #define EM_SAMPLER samLinearSimple
  #include "emissiveMapping.hlsl"
#elif !defined(USE_TXDETAIL)
  #define NO_CARPAINT
#endif

#ifdef MATERIAL_EMISSIVEEXTRA
  #define EME_TARGET_VS_OVERRIDE
  #define EME_SAMPLER samLinearSimple
  #include "emissiveMappingExtra.hlsl"
#endif

#include "include_new/base/cbuffers_ps.fx"
#include "include_new/base/_include_vs.fx"
#include "include/poisson.hlsl"
#include "accTrueEmissive.hlsl"

#ifdef MATERIAL_BRAKEDISCFX
  #define BDFX_TARGET_VS_OVERRIDE
  #define BDFX_SAMPLER samLinearSimple
  #include "include_new/ext_lighting_models/_include_ps.fx"
  #include "brakeDiscFX.hlsl"
#endif

#ifdef MATERIAL_HEATING
  #include "heating.hlsl"
#endif

struct Particle {
  float3 pos;
  uint nm;
  float2 tex;
  float2 texR;
  #ifdef SKINNED_MODE
    float4 boneIndices;
    float4 boneWeights;
  #endif
};

static const float2 BILLBOARD[] = {
	float2(-1, -1),
	float2(1, -1),
	float2(-1, 1),
	float2(-1, 1),
	float2(1, -1),
	float2(1, 1),
};

#if defined(TEXT_NODE_MODE)

  Texture2D<float> txCoverage : register(t23);

  cbuffer cbData : register(b8){
    float4x4 gTransform;
    float3 gColor;
    float gTextLength;
    uint4 gText[4];
  }

#elif defined(DISPLAY_NODE_MODE)

  Texture2D txBase : register(t22);
  Texture2D txTop : register(t23);

  cbuffer cbData : register(b8){
    float4x4 gTransform;
    float2 gNodeHalfSize;
    float gNodeBlend;
    float gPad1;
    float3 gColor;
    float gPad2;
  }

#else

  StructuredBuffer<Particle> particleBuffer : register(t23);

  cbuffer cbData : register(b8){
    float4x4 gTransform;
  }

#endif

float4 sampleTex(Texture2D tex, float2 uv, float2 uvR, float mipBias){  
  float level = getTextureLevel(tex, uvR);
  // return tex.SampleLevel(samLinearSimple, uv, 0);
  return tex.SampleLevel(samLinearSimple, uv, level + mipBias);

  // float4 ret = 0;
  // #define DISC_SIZE 16
  // [unroll]
  // for (uint i = 0; i < DISC_SIZE; ++i) {
  //   float2 offset = SAMPLE_POISSON(DISC_SIZE, i) * uvR;
  //   float2 sampleUV = uv + offset;
  //   ret += tex.SampleLevel(samLinearSimple, sampleUV, level);
  // }
  // return ret / DISC_SIZE;
}

float4 sampleTexBoosted(Texture2D tex, float2 uv, float2 uvR, float mipBias){
  float level = getTextureLevel(tex, uvR);
  float4 baseline = tex.SampleLevel(samLinearSimple, uv, level + mipBias);
  level *= 0.3;

  float4 accumulated = 0;
  float totalWeight = 0.001;
  #define DISC_SIZE 16
  [unroll]
  for (uint i = 0; i < DISC_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(DISC_SIZE, i) * uvR;
    float2 sampleUV = uv + offset;
    float4 sampled = tex.SampleLevel(samLinearSimple, sampleUV, level + mipBias);
    float sampleWeight = 1 + dot(sampled - baseline, 1);
    accumulated += sampled * sampleWeight;
    totalWeight += sampleWeight;
  }
  return accumulated / totalWeight;
}

void tweakTxDiffuseValue(inout float4 txDiffuseValue, Particle P, float gMipBias){
  #ifdef USE_TXDETAIL
    if (useDetail) {
      float4 txDetailValue = sampleTex(txDetail, P.tex * detailUVMultiplier, P.texR * detailUVMultiplier, gMipBias);
      txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, 1 - txDiffuseValue.a);
    }
  #endif
}

VS_Glw main(uint fakeIndex: SV_VERTEXID) {
  #if defined(TEXT_NODE_MODE) || defined(DISPLAY_NODE_MODE)
    uint vertexID = fakeIndex;
    Particle P;
    P.pos = 0;
    float3 Pnm = float3(0, 0, -1);
  #else
    uint vertexID = fakeIndex % 6;
    uint instanceID = fakeIndex / 6;
	  Particle P = particleBuffer[instanceID];
    float3 Pnm = float3(
      BYTE0(asuint(P.nm)) / 255.f,
      BYTE1(asuint(P.nm)) / 255.f,
      BYTE2(asuint(P.nm)) / 255.f) * 2 - 1;
  #endif

  VS_Glw vout;
  float2 B = BILLBOARD[vertexID];
  float3 basePos = P.pos;
  float baseRadius = gBaseRadius;

  float gEmissiveTweak = loadVector(gEmissiveTweak_MipBias).x;
  float gMipBias = loadVector(gEmissiveTweak_MipBias).y;
  // gEmissiveTweak = 0;
  // gMipBias = -100;

  #if defined(TEXT_NODE_MODE)
    vout.Color = GAMMA_KSEMISSIVE(gColor * gColorMult) * gOverallColorMult;
    float coverage = 0;
    for (int i = 0; i < 4; ++i){
      for (int j = 0; j < 4; ++j){
        coverage += txCoverage.Load(int3(gText[i][j], 0, 0));
      }
    }
    vout.Color *= coverage;
    baseRadius *= lerp(1, min(gTextLength, 4), 0.1);
  #elif defined(DISPLAY_NODE_MODE)

    #ifdef STATIC_DISPLAY_NODE_MODE
      float4 txBaseValue = txBase.SampleLevel(samLinearSimple, 0.5, 15);
      txBaseValue.rgb *= txBaseValue.w;
      vout.Color = GAMMA_KSEMISSIVE(gColor * gColorMult * txBaseValue.rgb) * gOverallColorMult;
      basePos.y += gNodeHalfSize.y;
      basePos.x -= gNodeHalfSize.x;
    #else
      float4 txBaseValue = sampleTex(txBase, float2(lerp(0.5, 1, gNodeBlend), 0.5), float2(lerp(0.5, 0, gNodeBlend), 0.5), gMipBias);
      txBaseValue.rgb *= txBaseValue.w;
      float baseBrightness = max(txBaseValue.r, max(txBaseValue.y, txBaseValue.z));
      float4 txTopValue = sampleTex(txTop, float2(lerp(0, 0.5, gNodeBlend), 0.5), float2(lerp(0, 0.5, gNodeBlend), 0.5), gMipBias);
      txTopValue.rgb *= txTopValue.w;
      float topBrightness = max(txTopValue.r, max(txTopValue.y, txTopValue.z));
      vout.Color = GAMMA_KSEMISSIVE(gColor * gColorMult * lerp(txBaseValue.rgb, txTopValue.rgb, gNodeBlend)) * gOverallColorMult;

      basePos.y += gNodeHalfSize.y;
      basePos.x -= gNodeHalfSize.x;
      if (baseBrightness < topBrightness){
        basePos.x += gNodeHalfSize.x * lerp(1 - gNodeBlend, 0, baseBrightness / topBrightness);
        baseRadius *= lerp(gNodeBlend * 0.2 + 0.8, 1, baseBrightness / topBrightness);
      } else {
        basePos.x -= gNodeHalfSize.x * lerp(gNodeBlend, 0, topBrightness / baseBrightness);
        baseRadius *= lerp((1 - gNodeBlend) * 0.2 + 0.8, 1, topBrightness / baseBrightness);
      }
    #endif

  #else

    float3 emissiveValue;

    #if defined(MATERIAL_EMISSIVE)
      bool baseSample = BYTE3(P.nm) == 0;
      float4 txDiffuseValue = baseSample ? sampleTex(txDiffuse, P.tex, P.texR, gMipBias) : sampleTexBoosted(txDiffuse, P.tex, P.texR, gMipBias);
      tweakTxDiffuseValue(txDiffuseValue, P, gMipBias);
      if (gFlags & TE_FLAG_IGNORE_TEXTURE_COLOR) txDiffuseValue = 1;
      if (emDiffuseAlphaAsMultiplier3.x < 0) txDiffuseValue.a = sampleTex(txNormal, P.tex, P.texR, gMipBias).a;

      float4 emissiveMap;
      emissiveValue = getEmissiveValue(txDiffuseValue, P.tex, P.texR, baseSample, getExtraValue(P.pos), emissiveMap);
      // emissiveValue = txDiffuseValue.rgb;
    #elif defined(MATERIAL_EMISSIVEEXTRA)
      bool baseSample = BYTE3(P.nm) == 0;
      float4 txDiffuseValue = baseSample ? sampleTex(txDiffuse, P.tex, P.texR, gMipBias) : sampleTexBoosted(txDiffuse, P.tex, P.texR, gMipBias);
      tweakTxDiffuseValue(txDiffuseValue, P, gMipBias);
      if (gFlags & TE_FLAG_IGNORE_TEXTURE_COLOR) txDiffuseValue = 1;
      emissiveValue = getEmissiveValue(txDiffuseValue, P.tex, P.texR, baseSample, getExtraValue(P.pos) < 0);
    #elif defined(MATERIAL_BRAKEDISCFX)
      float4 txDiffuseValue = sampleTex(txDiffuse, P.tex, P.texR, gMipBias);
      tweakTxDiffuseValue(txDiffuseValue, P, gMipBias);
      float reflectionOcclusion = 1, lfxMult = 1, brushedK, mapShading, cleanK, cleanK2, withinDisk, reflMask;
      float baseWorn = saturate(extWorn);
      float heatK = saturate(extGlowMult < 0 ? abs(extGlowMult) : glowLevel * extGlowMult);
      float3 pinNormalW = normalize(mul(Pnm, (float3x3)gTransform));
      float3 extWheelNormalW = normalize(mul(extWheelNormal, (float3x3)gTransform));
      if (dot(pinNormalW, extWheelNormalW) < 0) extWheelNormalW = -extWheelNormalW;
      float2 extraShadow = 1;
      float3 posRel = mul(float4(basePos - extWheelPos, 1), gTransform).xyz, specularValue = 0, reflDir;
      brakeDiscFXCompute(posRel,
        extWheelNormalW, float3(1, 0, 0), normalize(mul(extWheelUp, (float3x3)gTransform)), pinNormalW, P.tex,
        baseWorn, heatK, 0,
        float4(0.5, 0.5, 1, 1), 0,
        txDiffuseValue, pinNormalW, extraShadow, lfxMult,
        emissiveValue, specularValue, reflectionOcclusion, reflDir,
        brushedK, mapShading, cleanK, cleanK2, withinDisk, reflMask);
    #elif defined(MATERIAL_HEATING)
      float4 txDiffuseValue = 1;
      emissiveValue = EmissiveGetAll(P.pos, Pnm) * ksEmissive;
    #else
      float4 txDiffuseValue = sampleTex(txDiffuse, P.tex, P.texR, gMipBias);
      tweakTxDiffuseValue(txDiffuseValue, P, gMipBias);
      if (gFlags & TE_FLAG_IGNORE_TEXTURE_COLOR) txDiffuseValue = 1;
      emissiveValue = txDiffuseValue.rgb * ksEmissive;
    #endif

    // emissiveValue = GAMMA_KSEMISSIVE(emissiveValue);
    float3 glowApproximation = emissiveValue / (1 + gEmissiveTweak * emissiveValue);
    vout.Color = GAMMA_KSEMISSIVE(glowApproximation * gColorMult) * gOverallColorMult * extEmissiveMults[extFlags & 3];
    #ifdef USE_ALPHA_FROM_NORMAL_MAP
      txDiffuseValue.a = sampleTex(txNormal, P.tex, P.texR, gMipBias).a;
    #endif
    if (gFlags & TE_FLAG_ALPHA_BLEND) emissiveValue *= txDiffuseValue.a;
    if (gFlags & TE_FLAG_ALPHA_TEST) emissiveValue *= txDiffuseValue.a > 0.05;
  #endif

  float intensity = dot(vout.Color, 1);
  float gHalfSize = baseRadius * lerpInvSat(intensity, 0.5, 1.5);

  if (intensity < 0.5){
    DISCARD_VERTEX(VS_Glw);
  } 

  if (gFlags & TE_FLAG_DEBUG) {
    gHalfSize = 0.01;
  }

  #ifdef SKINNED_MODE
    
    float3 posWBase = 0;
    float3 normalW = 0;

    float totalWeight = 0;
    for (int i = 0; i < 4; i++){
      float weight = P.boneWeights[i];
      if (SKINNED_WEIGHT_CHECK(weight)){
        uint index = (uint)P.boneIndices[i];
        float4x4 bone = bones[index];
        posWBase += mul(bone, float4(basePos, 1)).xyz * weight;
        normalW += mul((float3x3)bone, Pnm) * weight;
        totalWeight += weight;
      }
    }

    posWBase /= totalWeight;
    normalW /= totalWeight;
    posWBase.xyz += gTransform[3].xyz;
  #else
    float3 posWBase = mul(float4(basePos, 1), gTransform).xyz;
    float3 normalW = mul(Pnm, (float3x3)gTransform);
  #endif

  float3 toSide = normalize(cross(posWBase - ksCameraPosition.xyz, extDirUp));
  float3 posWBase_alt = posWBase;
  APPLY_CFX(posWBase_alt);
  float4 posW = float4(posWBase_alt + toSide * B.x * gHalfSize + extDirUp * B.y * gHalfSize, 1);
  float4 posV = mul(posW, ksView);
  float4 posH = mul(posV, ksProjection);

  vout.PosH = posH;
  vout.PosC = posWBase - ksCameraPosition;
  vout.Normal = normalW;  
  vout.RadiusInv = 1 / pow(gHalfSize, 1);  
  vout.Tex = B;
  return vout;
}
