#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 cubic(float v){
    float4 n = float4(1.0, 2.0, 3.0, 4.0) - v;
    float4 s = n * n * n;
    float x = s.x;
    float y = s.y - 4.0 * s.x;
    float z = s.z - 4.0 * s.y + 6.0 * s.x;
    float w = 6.0 - x - y - z;
    return float4(x, y, z, w) * (1.0/6.0);
}

float4 textureBicubic(Texture2D tex, float2 texCoords){
  uint2 dim;
  uint sampleCount; 
  tex.GetDimensions(0, dim.x, dim.y, sampleCount); 

  float2 invTexSize = 1.0 / (float2)dim;   
  texCoords = texCoords * (float2)dim - 0.5;

  float2 fxy = frac(texCoords);
  texCoords -= fxy;

  float4 xcubic = cubic(fxy.x);
  float4 ycubic = cubic(fxy.y);
  float4 c = texCoords.xxyy + float2 (-0.5, +1.5).xyxy;    
  float4 s = float4(xcubic.xz + xcubic.yw, ycubic.xz + ycubic.yw);
  float4 offset = c + float4 (xcubic.yw, ycubic.yw) / s;    
  offset *= invTexSize.xxyy;
  
  float4 sample0 = tex.SampleLevel(samLinearSimple, offset.xz, 0);
  float4 sample1 = tex.SampleLevel(samLinearSimple, offset.yz, 0);
  float4 sample2 = tex.SampleLevel(samLinearSimple, offset.xw, 0);
  float4 sample3 = tex.SampleLevel(samLinearSimple, offset.yw, 0);
  float sx = s.x / (s.x + s.y);
  float sy = s.z / (s.z + s.w);
  return lerp(lerp(sample3, sample2, sx), lerp(sample1, sample0, sx), sy);
}

float4 main(VS_Copy pin) : SV_TARGET {
  // return textureBicubic(txDiffuse, pin.Tex);
  // return txDiffuse.Load(int3(pin.PosH.xy, 0));
  // return txDiffuse.SampleLevel(samPoint, pin.Tex, 0);
  // return txDiffuse.SampleLevel(samPoint, pin.Tex, 0);
  return txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);// + dithering(pin.PosH.xy) * 0;
}