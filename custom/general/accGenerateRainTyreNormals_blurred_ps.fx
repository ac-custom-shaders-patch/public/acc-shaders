#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);
Texture2D txNoise : register(TX_SLOT_NOISE);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 r = 0;
  float t = 0;
  // float o = txNoise.Load(int3(pin.PosH.xy % 32, 0)).x;
  float o = 0;
  for (int i = 0; i < 40; ++i){
    r += txDiffuse.SampleLevel(samLinearSimple, pin.Tex + float2(0, ((float)i + o) / 80), 2);
    t += 1;
  }
  return r / t;
}