#include "accSSLR_flags.hlsl"
#include "include/common.hlsl"
#include "accSS_cbuffer.hlsl"
#include "include_new/base/_gamma.fx"
#include "include_new/ext_lightingfx/utils_ps.fx"
#include "include/poisson.hlsl"

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

Texture2D txColor : register(TX_SLOT_COLOR);
Texture2D txNormals : register(TX_SLOT_NORMALS);
Texture2D<float> txDepth : register(TX_SLOT_GDEPTH);
Texture2D txReflections1 : register(TX_SLOT_SS_ARG0);
Texture2D txReflections2 : register(TX_SLOT_SS_ARG1);
Texture2D txSSLR : register(TX_SLOT_SS_ARG2);
Texture2D txSSLRlr : register(TX_SLOT_SS_ARG3);
Texture2D<float> txSSLRdim : register(TX_SLOT_SS_ARG4);

#define SL_MAX_LIGHTS 32

struct sLight {
  float3 pos;
  float rangeX;
  float3 dirW;
  float spotCosS0;
  float3 upW;
  float trim;
  float3 color;
  float rangeY;
};

cbuffer _cbExtLighting : register(b6) {
  uint gLightsCount;
  float3 gPad0;
  sLight gLights[SL_MAX_LIGHTS];
}

float guessSpecularExp(float blur){
  return lerp(40, 1200, pow(1 - blur, 2));
}

float guessSpecularStrength(float blur, float3 normalW){
  return clamp(lerp(4, 0, blur), 0, 1) * 0.5 * saturate(normalW.y * 2 - 1);
}

float3 calculateLight(sLight L, float3 posW, float3 normalW, float specularExp){
  // return L.color;

  float4 toLight = normalizeWithDistance(L.pos - posW);
  float attenuation = saturate(1 - toLight.w * L.rangeY);
  if (attenuation == 0) return 0;

  float LdotN = saturate(dot(toLight.xyz, normalW) * 40);
  float spotK = saturate(getSpotCone(L.dirW, toLight.xyz, L.spotCosS0) * 4);
  float edge = getEdge(L.upW, toLight.xyz, L.trim).x;
  float shadow = GAMMA_LINEAR_SIMPLE(edge * spotK * attenuation) * LdotN;
  // return L.color * spotK * attenuation;
  return L.color * shadow * getSpecularComponent_C(normalW, normalize(posW), toLight.xyz, specularExp);
}

float ssGetDepth(float2 uv){
  return txDepth.SampleLevel(samLinearClamp, uv, 0).x;
  return txDepth.SampleLevel(samLinearBorder1, uv, 0).x;
}

float3 ssGetPos(float2 uv, float depth){
  float4 p = mul(float4(uv.xy, depth, 1), viewProjInv);
  return p.xyz / p.w;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float3 posW = ssGetPos(pin.Tex, ssGetDepth(pin.Tex));
  // return float4(frac(posW), 1);

  float4 vColor = txColor.SampleLevel(samPointClamp, pin.Tex, 0);
  float4 vReflections1 = txReflections1.SampleLevel(samPointClamp, pin.Tex, 0);
  float4 vReflections2 = txReflections2.SampleLevel(samPointClamp, pin.Tex, 0);
  // return vReflections1;

  // Silly way of storing SSLR_FORCE flag
  vReflections1.w = abs(vReflections1.w);

  // Silly of storing GAMMA_CORRECT flag
  bool gammaCorrect = GAMMA_OR(false, dot(vReflections1.rgb, 1) < 0);
  vReflections1.rgb = abs(vReflections1.rgb);

  // return vReflections1.a * 4;
  // return vReflections2.a * 4;
  // return txNormals.SampleLevel(samPointClamp, pin.Tex, 0);
  // return pow(saturate(txDepth.SampleLevel(samPointClamp, pin.Tex, 9)), 20) 
  //   + saturate(fwidth(pow(saturate(txDepth.SampleLevel(samPointClamp, pin.Tex, 0)), 20)) * 100 - 1);
  // return txSSLRlr.SampleLevel(samLinearClamp, pin.Tex, 3);
  // return txSSLRlr.SampleLevel(samLinearClamp, pin.Tex, 0);
  // return txSSLRlr.SampleLevel(samLinearClamp, pin.Tex, 0).w;
  // return vReflections2;
  // return txReflections2.SampleLevel(samPoint, pin.Tex, 5).w;
  // return txSSLRdim.SampleLevel(samLinearSimple, pin.Tex, 0);

  // return txSSLRlr.SampleLevel(samLinearSimple, pin.Tex, 2);
  // return txSSLRlr.SampleLevel(samLinearSimple, pin.Tex, 0);
  // return pow(txSSLRlr.SampleLevel(samLinearSimple, pin.Tex, 2), 2);

  // float4 h = txSSLRlr.SampleLevel(samLinearClamp, pin.Tex, 0);
  // return h * h.w;

  // vReflections1.w = max(vReflections1.w, saturate(fwidth(vReflections1.w) * 100));

  #ifndef SSLR_TRACE_EVERYTHING
    [branch]
    if (vReflections1.w < GAMMA_OR_STAT(0.002, 0.02)) {
      return vColor;
    }
  #endif

  float4 vNormal = txNormals.SampleLevel(samPointClamp, pin.Tex, 0);
  float3 normalW = normalize(ssNormalDecode(vNormal.xyz));

  float4 vSSLR = txSSLR.SampleLevel(samLinearSimple, pin.Tex, 0);
  float4 vSSLRlr2 = txSSLR.SampleLevel(samLinearSimple, pin.Tex, 3.5);

  // vSSLR.xyz /= max(vSSLR.w, 0.001);
  // vSSLRlr2.xyz /= max(vSSLRlr2.w, 0.001);

  // float distanceV = saturate(max(vSSLR.z*0, vSSLRlr2.z));
  float distanceV = saturate(lerp(vSSLR.z, vSSLRlr2.z, vSSLRlr2.w));
  // float distanceV = vSSLRlr2.z;
  // return distanceV;
  // float distanceV = saturate(vSSLRlr2.z);
  // return float4(distanceV, 1 - distanceV, 0, 1);

  #ifdef SSLR_SHOW_TRACED
    return txSSLRlr.SampleLevel(samPointClamp, pin.Tex, 0);
  #endif
  
  // float blurAmount = vReflections2.a * 6 * lerp(lerp(0.03, 0.6, vReflections2.a * vReflections2.a), 1, distanceV);
  float blurAmount = vReflections2.a * 6 * lerp(lerp(0.03, 0.6, vReflections2.a * vReflections2.a), 1, distanceV);
  float blurK = saturate(blurAmount);
  // float4 resultBase = txSSLRlr.SampleLevel(samLinearSimple, pin.Tex, blurAmount * 0.6);
  float4 resultBase = 0;

  if (GAMMA_FIX_ACTIVE && 1) {
    float radius = blurAmount * 0.001;
    #define DISK_SIZE 8
    [unroll]
    for (uint i = 0; i < DISK_SIZE; ++i) {
      float2 offset = SAMPLE_POISSON(DISK_SIZE, i) * radius;
      resultBase += txSSLRlr.SampleLevel(samLinearClamp, pin.Tex + offset, blurAmount * 0.3);
    }
    resultBase /= DISK_SIZE;
  } else {
    resultBase = txSSLRlr.SampleLevel(samLinearSimple, pin.Tex, blurAmount * 0.45);
    // resultBase = txSSLRlr.SampleLevel(samLinearSimple, pin.Tex, blurAmount * 2);
  }

  #ifdef GAMMA_FIX
    resultBase.rgb = pow(resultBase.rgb, 2);
  #endif

  // return resultBase;

  float reflPower = saturate(vReflections1.a);
  float dimReflection = txSSLRdim.SampleLevel(samLinearSimple, pin.Tex, blurAmount + 1);
  dimReflection = lerp(dimReflection, 0, saturate(blurAmount * 0.8 - 1) * 0.4);
  resultBase.w = sqrt(saturate(resultBase.w));

  // float resultQuality = lerp(saturate((resultBase.w - 0.5) * 4), resultBase.w, blurK);
  // float resultQuality = saturate((resultBase.w - lerp(0.5, 0, blurK)) * 4);
  // float resultQuality = pow(resultBase.w, 0.2);
  float resultQuality = resultBase.w;
  float3 reflectionsColorMult = vReflections2.xyz * 2;

  // float3 underlyingSurface = max(vColor.rgb - vReflections1.rgb * resultQuality, 0);
  // float3 underlyingSurface = max(vColor.rgb - vReflections1.rgb, 0);
  // resultQuality = 1;
  // float3 underlyingSurface = gammaCorrect
  //   ? pow(max(pow(max(0, vColor.rgb), 2.2) - vReflections1.rgb * resultQuality, 0), 1/2.2)
  //   : max(vColor.rgb - vReflections1.rgb * resultQuality, 0);
  if (gammaCorrect) { vColor.rgb = pow(max(0, vColor.rgb), 2.2); }
  float3 underlyingSurface = max(vColor.rgb - vReflections1.rgb * resultQuality, 0);
  if (gammaCorrect) { underlyingSurface = pow(underlyingSurface, 1/2.2); }

  // return float4(vColor.rgb, 1);
  // return float4(vReflections1.rgb, 1);
  // return float4(vReflections1.aaa * 10, 1);
  // if (any(vColor.rgb - vReflections1.rgb < -vColor.rgb*0.001)) return float4(3, 0, 3, 1);
  // if (any(vColor.rgb - vReflections1.rgb < 0)) return float4(1, 0, 1, 1);
  // return float4(vColor.rgb - vReflections1.rgb, 1);

  // return float4(underlyingSurface, 1);
  // return resultBase;

  // return float4(float3(resultQuality, 1 - resultQuality, 0) * 0.2, 1);

  float3 resultColor = resultBase.xyz;
  float3 dimColor = min(0.3, luminance(resultColor));

  resultColor = lerp(resultColor, resultColor / (1 + resultColor) * 0.2, dimReflection);

  resultColor.rgb *= gSSLRBrightness;
  resultColor.rgb = max(0, lerp(luminance(resultColor.rgb), resultColor.rgb, gSSLRSaturation));

  float3 extraComponent = 0;
  #ifdef WITH_SPECULAR_LIGHTING
    float specularC = guessSpecularStrength(vReflections2.a, normalW);

    [branch]
    if (specularC > 0){
      float specularExp = guessSpecularExp(vReflections2.a);
      for (uint i = 0; i < gLightsCount; i++){
        extraComponent += calculateLight(gLights[i], posW, normalW, specularExp);
        // extraComponent = 1;
      }
      extraComponent *= specularC;
    }

    // extraComponent *= 100;
    // return float4(extraComponent, 1);
  #endif

  // return float4(vNormal.xyz * 0.5 + 0.5, 1);
  // return float4((float3)saturate(vNormal.y), 1);
  // return abs(vNormal - float4(0, 1, 0, 1)) * 10;
  // return float4(lerp(float3(1, 1, 1), resultColor * reflectionsColorMult, resultQuality), 1);
  // return float4(lerp(float3(0, 0, 0), pow(resultColor/4, 4), resultQuality), 1);
  // return float4(reflectionsColorMult, 1);

  // return float4(resultQuality, 1 - resultQuality, 0, 1);

  // return float4(lerp(float3(0, 0, 0), resultColor, resultQuality), 1);
  // if (frac(pin.Tex.x * 20) > 0.7){
  //   return float4(vReflections1.rgb, 1);
  // }
  // return float4(reflectionsColorMult, 1);
  // return float4(resultColor * resultQuality * reflPower * reflectionsColorMult, 1);

  // return float4(underlyingSurface, 1);
  return float4(underlyingSurface + (resultColor * resultQuality + extraComponent) * reflectionsColorMult * reflPower, 1);
  // return float4(vColor.rgb - vReflections1.rgb + resultColor * reflPower * resultQuality 
  //   + float3(10, 10, 10) * reflPower * saturate(1 - resultQuality), 1);
}