#include "include_new/base/samplers_ps.fx"

Texture2D txNormals : register(t0);
Texture2D txReflectionsMap : register(t1);
Texture2D<float> txDepthMap : register(t2);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float3 gNormalBias;
}

float4 main(VS_Copy pin) : SV_TARGET {
  float4 txNormalsValue = txNormals.SampleLevel(samLinear, pin.Tex, 0);
  if (dot(txNormalsValue, 1) == 0){
    txNormalsValue = float4(0, 0, 1, 0);
  }
  float4 txReflectionsMapValue = txReflectionsMap.SampleLevel(samLinear, pin.Tex, 0);

  float d0 = txDepthMap.SampleLevel(samLinear, pin.Tex, 0, int2(0, -1));
  float d1 = txDepthMap.SampleLevel(samLinear, pin.Tex, 0, int2(0, 1));
  if (d1 - d0 > 0.02){
    txNormalsValue = float4(0, 1, 0, 0);
    txReflectionsMapValue.w = 0.5;
  }
  if (d1 - d0 < -0.02){
    txNormalsValue = float4(0, -1, 0, 0);
    txReflectionsMapValue.w = 0.5;
  }

  txNormalsValue.rgb += gNormalBias;

  // float h0 = txDepthMap.SampleLevel(samLinear, pin.Tex, 0, int2(-1, 0));
  // float h1 = txDepthMap.SampleLevel(samLinear, pin.Tex, 0, int2(1, 0));
  // if (h1 - h0 > 0.02){
  //   txNormalsValue = float4(1, 0, 0, 0);
  //   txReflectionsMapValue.w = 1;
  // }
  // if (h1 - h0 < -0.02){
  //   txNormalsValue = float4(-1, 0, 0, 0);
  //   txReflectionsMapValue.w = 1;
  // }

  return float4(txNormalsValue.xyz, abs(txReflectionsMapValue.w));
}