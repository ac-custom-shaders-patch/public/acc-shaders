#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float3 pos0;
  float mult;

  float3 posX;
  float pad1;
  
  float3 posY;
  float pad2;

  float4x4 transform;
}

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float main(VS_Copy pin) : SV_TARGET {
  float shadow = saturate(txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0) * mult);
  float2 centerDif = pin.Tex * 2 - 1;
  centerDif.y = sign(centerDif.y) * max(abs(centerDif.y) * 2 - 1, 0);
  float edgeFalloff = saturate(2 - length(centerDif) * 2);
  return edgeFalloff * shadow * 0.7;
}