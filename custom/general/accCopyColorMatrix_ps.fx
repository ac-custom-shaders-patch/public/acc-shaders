#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

cbuffer cbData : register(b10) {
  float4x4 gMatLDR;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float4 col = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 0);
  col.rgb = mul(float4(col.rgb, 1), gMatLDR).rgb;
  return col;
}