#include "include/common.hlsl"
#include "include/samplers.hlsl"
#include "include_new/base/cbuffers_common.fx"

TextureCube txFirst : register(t0);
TextureCube<float> txSecond : register(t1);
Texture2D txNoise : register(TX_SLOT_NOISE);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

const static float4x4 transformFirst[6] = {
  transpose(float4x4(0, 0, -1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1)),
  transpose(float4x4(0, 0, 1, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 1)),
  transpose(float4x4(-1, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 1)),
  transpose(float4x4(-1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1)),
  transpose(float4x4(-1, 0, 0, 0, 0, 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1)),
  transpose(float4x4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1))
};

cbuffer cbData : register(b10) {
  float4x4 transformSecond[6];
  float3 ambientColor;
  float mipLevel;
}

float sampleCubeSingle(float3 p){
  return pow(saturate(txSecond.SampleBias(samLinear, p, 2)), 0.5);
}

float sampleCubeSmooth(float3 p){
  const static float3 gridSamplingDisk[8] = {
    { 1, 1, 1 },
    { 1, 1, -1 },
    { 1, -1, 1 },
    { 1, -1, -1 },
    { -1, 1, 1 },
    { -1, 1, -1 },
    { -1, -1, 1 },
    { -1, -1, -1 },
  };
  float r = 0;
  for (int i = 0; i < 8; i++) {
    r += txSecond.SampleBias(samLinearSimple, p + gridSamplingDisk[i] / 64, 0);
  }
  return r / 8;
}

#include "include/poisson.hlsl"
float sampleCubePoisson(float3 p){
  float radius = 0.04 * pow(2, sqrt(mipLevel));
  #define PUDDLES_DISK_SIZE 6
  float avg;
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * radius;
    avg += txSecond.SampleLevel(samLinearSimple, p + float3(offset, offset.x), mipLevel);
  }
  return avg / PUDDLES_DISK_SIZE;
}

float4 calculate(float3 vec, float4x4 tFirst, float4x4 tSecond){
  float3 vecFirst = mul(vec, (float3x3)tFirst);
  float3 vecSecond = mul(vec, (float3x3)tSecond);
  float mix = sampleCubePoisson(vecSecond);
  mix = GAMMA_OR(pow(mix, 3), mix);
  return lerp(float4(ambientColor, 1), txFirst.Sample(samLinearSimple, vecFirst), mix);
}

struct PS_OUT {  
  float4 rt0 : SV_Target;
  float4 rt1 : SV_Target1;
  float4 rt2 : SV_Target2;
  float4 rt3 : SV_Target3;
  float4 rt4 : SV_Target4;
  float4 rt5 : SV_Target5;
};

PS_OUT main(VS_Copy pin) {
  float3 vec = float3(1 - pin.Tex * 2, -1);
  PS_OUT ret;
  ret.rt0 = calculate(vec, transformFirst[0], transformSecond[0]);
  ret.rt1 = calculate(vec, transformFirst[1], transformSecond[1]);
  ret.rt2 = calculate(vec, transformFirst[2], transformSecond[2]);
  ret.rt3 = calculate(vec, transformFirst[3], transformSecond[3]);
  ret.rt4 = calculate(vec, transformFirst[4], transformSecond[4]);
  ret.rt5 = calculate(vec, transformFirst[5], transformSecond[5]);
  return ret;
}