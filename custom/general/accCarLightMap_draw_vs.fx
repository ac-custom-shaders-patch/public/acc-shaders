#include "include_new/base/_include_vs.fx"

static const float2 BILLBOARD[] = {
	float2(-1, -1),
	float2(1, -1),
	float2(-1, 1),
	float2(-1, 1),
	float2(1, -1),
	float2(1, 1),
};

cbuffer cbData : register(b11){
  float3 gPos;
  float gRadius;
  float4x4 gWorldToCar;
  float3 gDir;
  float gPad0;
  float3 gCarSize;
  float gPad1;
}

struct VS_Clm {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

VS_Clm main(uint id: SV_VertexID) {
  VS_Clm vout;
  float2 B = BILLBOARD[id];
  float distance = length(gPos - ksCameraPosition.xyz);
  float3 toSide = normalize(cross(gPos - ksCameraPosition.xyz, extDirUp));
  float3 toLook = normalize(cross(extDirUp, toSide));
  // float3 toSide = float3(1, 0, 0);

  float margin = 1 + lerpInvSat(distance, gRadius * 2, 0) * 3;
  float gHalfLength = gCarSize.z / 2 + margin;
  float gHalfWidth = max(gCarSize.x, gCarSize.y) / 2 + margin;

  float4 posW = float4(gPos
    // + toLook
    + toSide * B.x * lerp(gHalfWidth, gHalfLength, abs(dot(toSide, gDir))) 
    + extDirUp * clamp(B.y, -1, 0.5) * lerp(gHalfWidth, gHalfLength, abs(dot(extDirUp, gDir))), 1);
  float4 posV = mul(posW, ksView);
  float4 posH = mul(posV, ksProjection);

  // if (mul(float4(gPos, 1), ksView).z > 0){
  if (distance < gHalfLength){
    posH.xy = B;
    posH.zw = float2(0.001, 1);
    B /= 2;
  }

  vout.PosH = posH;
  vout.Tex = B;
  return vout;
}
