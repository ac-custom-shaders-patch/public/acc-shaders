#include "include/common.hlsl"

Texture2D<float> txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

// #include "include/poisson.hlsl"
// float main(VS_Copy pin) : SV_TARGET {
//   POISSON_AVG(float, result, txDiffuse, samLinearClamp, pin.Tex, 0.01, 32);
//   return result;
// }

cbuffer cbData : register(b10) {
  float2 gPixelSize;
  float2 _pad;
}

float2 toWeight(float v){
  return v == 1 ? 0 : float2(v, 1);
}

float main(VS_Copy pin) : SV_TARGET {
  float2 ret = float2(0.00002, 0.00001);
  for (int i = 1; i < 7; ++i){
    float2 offset = (float)i * gPixelSize;
    ret += (
      toWeight(txDiffuse.SampleLevel(samPointClamp, pin.Tex + offset, 0)) 
      + toWeight(txDiffuse.SampleLevel(samPointClamp, pin.Tex - offset, 0)));
  }

  // const float gWeights[9] = { 0.1085503, 0.1313501, 0.1040560, 0.0721595, 0.0438034, 0.0232758, 0.0108263, 0.0044078, 0.0015709 };
  // const float gOffsets[9] = { 0.6629276, 2.4790385, 4.4623189, 6.4456835, 8.4291687, 10.4128103, 12.3966427, 14.3806973, 16.3650055 };
  // [unroll(16)]
  // for (int i = 0; i < 9; ++i) {
  //   float2 offset = gOffsets[i] * gPixelSize;
  //   ret += gWeights[i] * (
  //     toWeight(txDiffuse.SampleLevel(samPoint, pin.Tex + offset, 0)) 
  //     + toWeight(txDiffuse.SampleLevel(samPoint, pin.Tex - offset, 0)));
  // }

  return saturate(ret.x / ret.y);
}