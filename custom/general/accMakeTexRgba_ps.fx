#include "include/common.hlsl"

Texture2D txInR : register(t0);
Texture2D txInG : register(t1);
Texture2D txInB : register(t2);
Texture2D txInA : register(t3);

cbuffer cbData : register(b10) {
  float4 gChannels;
}

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  return float4(
    txInR.SampleLevel(samLinear, pin.Tex, 0)[gChannels.r],
    txInG.SampleLevel(samLinear, pin.Tex, 0)[gChannels.g],
    txInB.SampleLevel(samLinear, pin.Tex, 0)[gChannels.b],
    txInA.SampleLevel(samLinear, pin.Tex, 0)[gChannels.a]
  );
}