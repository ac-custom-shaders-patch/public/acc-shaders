#include "include/common.hlsl"
#include "include/poisson.hlsl"

Texture2D txDiffuse0 : register(t0);
Texture2D txDiffuse1 : register(t1);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 blurTex(Texture2D tex, float2 uv){
  #define PUDDLES_DISK_SIZE 16
  float4 ret = 0;
  float t = 0.001;
  for (uint i = 0; i < PUDDLES_DISK_SIZE; ++i) {
    float2 offset = SAMPLE_POISSON(PUDDLES_DISK_SIZE, i) * 0.001;
    float4 s = tex.SampleLevel(samLinearClamp, uv + offset, 0);
    float w = s.w;
    ret += s * w;
    t += w;
  }
  return ret / t;
}

float4 main(VS_Copy pin) : SV_TARGET {
  // return 1;
  if (pin.Tex.x < 0.5) {
    return blurTex(txDiffuse0, pin.Tex * float2(2, 1));
    // return txDiffuse0.SampleLevel(samLinearSimple, pin.Tex * float2(2, 1), 0);
  }

  return blurTex(txDiffuse1, pin.Tex * float2(2, 1) - float2(1, 0));
  // return txDiffuse1.SampleLevel(samLinearSimple, pin.Tex * float2(2, 1) - float2(1, 0), 0);
}
