#ifndef MODE_MAIN_NOFX
	#define MODE_MAIN_FX
#endif

#include "flgTree.hlsl"

#define SUPPORTS_DISTANT_FIX
#define CUSTOM_STRUCT_FIELDS float3 TangentL : TangentL;
#include "include_new/base/_include_vs.fx"

PS_IN_PerPixel main(VS_IN vin) {
  GENERIC_PIECE(PS_IN_PerPixel);
  vout.TangentL = vin.TangentPacked;
  vout.Ao = 1;
  return vout;
}
