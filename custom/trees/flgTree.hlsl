#include "include/common.hlsl"

struct TreeVertex {
  float4 PosL;
  float3 NormalL;
  float2 Tex;
  float3 TangentPacked;
};

RWStructuredBuffer<TreeVertex> buVertices : register(u0);

float3 applyNoise(float3 pos){
  // float bump = saturate(remap(pos.y, 2, 3, 0, 1));
  // pos.y -= smoothstep(0, 1, bump) * 0.5 + 0.5 * pos.x * saturate(smoothstep(1, 2.5, pos.y) * smoothstep(4, 2.5, pos.y));
  // pos.x -= smoothstep(0, 1, bump);

  pos.x += sin(pos.y * 0.3) * sin(pos.y * 0.2);   
  pos.z += sin(pos.y * 0.3 + 37.4586) * sin(pos.y * 0.2 + 78.8871);   
  pos.x += sin(pos.y * 1.7) * sin(pos.y * 2.7) * 0.1 + sin(pos.y * 3.7) * 0.05; 
  pos.z += sin(pos.y * 1.8 + 56.99) * sin(pos.y * 2.7 + 59.41) * 0.1 + sin(pos.y * 3.2 + 54.31) * 0.05; 
  pos.y += sin(pos.y * 1.3) * sin(pos.y * 2.2) * 0.2 + sin(pos.y * 3.2) * 0.1; 
  return pos;
}

TreeVertex genVertex(float3 pos, float i){
  TreeVertex ret = (TreeVertex)0;
  ret.PosL = float4(pos, 1);
  ret.NormalL = float3(i == 0, i == 1, i == 2);
  return ret;
}

struct ShapeMaker {
  uint retIndex;
  uint quadIndex;
  uint quadPart;
  uint quadTotal;
  bool done;

  void set(uint DTid){
    retIndex = DTid.x * 3;
    quadIndex = DTid.x / 2;
    quadPart = DTid.x % 2;
    done = false;
  }

  bool make(uint size){
    if (done) return false;
    if (quadIndex < size){
      quadTotal = size;
      done = true;
      return true;
    }
    quadIndex -= size;
    return false;
  }

  void setResult(float3 p0, float3 p1, float3 p2, float3 p3){
    buVertices[retIndex++] = genVertex(p0, 0);
    buVertices[retIndex++] = genVertex(quadPart ? p2 : p3, 1);
    buVertices[retIndex++] = genVertex(quadPart ? p1 : p2, 2);
  }

  void clearResult(){
    buVertices[retIndex++] = (TreeVertex)0;
    buVertices[retIndex++] = (TreeVertex)0;
    buVertices[retIndex++] = (TreeVertex)0;
  }

  void finalize(){
    if (!done){
      clearResult();
    }
  }
};
