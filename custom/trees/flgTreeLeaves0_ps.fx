#ifndef MODE_MAIN_NOFX
	#define MODE_MAIN_FX
#endif

#define NO_CARPAINT
#define SUPPORTS_AO
#define INPUT_DIFFUSE_K 0.45
#define INPUT_AMBIENT_K 0.45
#define INPUT_SPECULAR_K 0.05
#define INPUT_SPECULAR_EXP 3
#define INPUT_EMISSIVE3 0
#define IS_TRACK_MATERIAL 1

// #include "flgTree.hlsl"
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_PerPixel pin, bool ff : SV_IsFrontFace) {
  READ_VECTORS

  if (ff) normalW = -normalW;

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = float4(0.15, pin.Tex * 0.4, 1);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  // RETURN(lighting, txDiffuseValue.a);


  ReflParams R = getReflParamsZero();
  R.fresnelEXP = 5;
  R.ksSpecularEXP = 1;
  R.finalMult = 1;
  R.metallicFix = 0;
  R.reflectionSampleParam = REFL_SAMPLE_PARAM_DEFAULT;
  R.coloredReflections = 0;
  R.fresnelMaxLevel = 0.1;
  R.fresnelC = 0; 

  float4 withReflection = calculateReflection(float4(lighting, txDiffuseValue.a), toCamera, pin.PosC, normalW, R);
  RETURN(withReflection, withReflection.a);

  // return saturate((normalW.x * normalW.y * normalW.z) * 10) * 3;
  // return saturate(normalW.y * 0.5 + 0.5) * float4(0.4, pin.Tex, 1) * (ff ? 0.6 : 1) * AO_EXTRA_LIGHTING;
}
