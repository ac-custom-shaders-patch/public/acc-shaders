#include "flgTree.hlsl"

#define SUPPORTS_COLORFUL_AO
#define SUPPORTS_DISTANT_FIX
#include "include_new/base/_include_vs.fx"

StructuredBuffer<TreeVertex> particleBuffer : register(t0);

float rand(float2 co){
  return frac(sin(dot(co, float2(12.9898, 78.233))) * 43758.5453);
}

PS_IN_PerPixel main(uint index : SV_VERTEXID) {
  TreeVertex vin = particleBuffer[index];
  GENERIC_PIECE(PS_IN_PerPixel);
  return vout;
}
