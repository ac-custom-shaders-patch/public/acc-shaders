#ifndef MODE_MAIN_NOFX
	#define MODE_MAIN_FX
#endif

#include "include_new/base/_include_vs.fx"

struct TreeLeaf {
		float3 pos;
		uint branch;
		float3 fwd;
		uint tex;
		float3 side;
		uint pad;
};

StructuredBuffer<TreeLeaf> particleBuffer : register(t0);

static const float3 BILLBOARD[] = {
	float3(0, -1, 1),
	float3(1, 0, -1),
	float3(-1, 0, -1),
	float3(-1, 0, -1),
	float3(1, 0, -1),
	float3(0, 1, 1),
};

PS_IN_PerPixel main(uint fakeIndex : SV_VERTEXID) {
	PS_IN_PerPixel vout = (PS_IN_PerPixel)0;

	uint vertexID = fakeIndex % 6;
	uint instanceID = fakeIndex / 6;

	TreeLeaf P = particleBuffer[instanceID];
	float3 B = BILLBOARD[vertexID];

  // if (instanceID < 4900){
  //   DISCARD_VERTEX(PS_IN_PerPixel);
  // }

  float3 nm = normalize(cross(P.fwd, P.side));



	float4 posW = float4(P.pos + P.fwd * B.x + P.side * B.y + nm * B.z * length(P.side) * -0.2, 1);
	float4 posV = mul(posW, ksView);
	vout.PosH = mul(posV, ksProjection);
  vout.NormalW = nm;
  if (B.z == 1){
    vout.NormalW += normalize(P.side) * B.y;
  }

  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex.x = lerp(0.6, 1, frac(P.pad * 0.02161616)) * 0.8;
  vout.Tex.y = lerp(0, 0.4, frac(P.pad * 0.04771627)) * 0.4;
  vout.Ao = 1;
  shadows(posW, SHADOWS_COORDS);

  return vout;
}