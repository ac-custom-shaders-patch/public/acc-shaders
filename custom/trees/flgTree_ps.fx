#ifndef MODE_MAIN_NOFX
	#define MODE_MAIN_FX
#endif

#define NO_CARPAINT
#define SUPPORTS_AO
#define INPUT_DIFFUSE_K 0.45
#define INPUT_AMBIENT_K 0.45
#define INPUT_SPECULAR_K 0.05
#define INPUT_SPECULAR_EXP 3
#define INPUT_EMISSIVE3 0
#define IS_TRACK_MATERIAL 1

#include "flgTree.hlsl"
#define CUSTOM_STRUCT_FIELDS float3 TangentL : TangentL;
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = float4(0.2, 0.2, 0.15, 1);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  RETURN(lighting, txDiffuseValue.a);

  // return saturate((normalW.x * normalW.y * normalW.z) * 10) * 3;
  // return saturate(normalW.y * 0.5 + 0.5) * lerp(1, 2, saturate((pin.TangentL.x * pin.TangentL.y * pin.TangentL.z) * 10)) * float4(0.6, 0.6, 0.5, 1) * shadow * AO_EXTRA_LIGHTING;
}
