#include "include_new/base/cbuffers_common.fx"

struct VS_IN {
  float3 PosL : POSITION;
  uint NormalL : NORMAL_ENCODED;
};

struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

VS_Copy main(VS_IN vin) {
  VS_Copy vout;
  vout.PosH = mul(float4(vin.PosL, 1), ksWorld);
  vout.Tex = 0;
  return vout;
}
