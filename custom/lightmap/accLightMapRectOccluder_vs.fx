struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

static const float2 BILLBOARD[] = {
	float2(-1, -1),
	float2(1, -1),
	float2(-1, 1),
	float2(-1, 1),
	float2(1, -1),
	float2(1, 1),
};

cbuffer cbData : register(b10) {
  float4x4 gTransform;
  float2 gSize;
}

VS_Copy main(uint id: SV_VertexID) {
  VS_Copy vout;
  float2 ver = BILLBOARD[id];  
  vout.PosH = mul(float4(-ver.x * 1, -1, ver.y * 1, 1), gTransform);
  vout.Tex = ver;
  return vout;
}
