struct VS_Copy {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  float d = length(pin.Tex);
  return float4(pow(saturate(d), 2).xxx, 1);
}