#define MODE_HEIGHTMAP
#include "dclSkidmarks.hlsl"

#define IN_PARALLAX_SCALE (-0.14)

float parallaxHeight(float2 UV, float2 DX, float2 DY){
  // return 0;
  return 1 - lerp(0, DX.y 
    ? cos(smoothstep(0, 1, pow(abs(UV.x), 1)) * M_PI * 1.45 + 0.05) * (0.4 + 0.1 * DY.x) + 0.5 
    : 0.2 + 0.3 * lerpInvSat(abs(abs(UV.x) - 0.75), 0.04, 0), DX.x);
}

float4 main(PS_IN pin, out float outDepth : SV_DepthGreaterEqual) : SV_TARGET {
  float2 texDx = float2(saturate(pow(pin.Alpha * 1.5, 4) - 0.4), 1);
  float height = parallaxHeight(pin.Tex, texDx, 1);
  outDepth = pin.PosH.z + (2 - height) * abs(IN_PARALLAX_SCALE) / 600 * 0.6;
  return float4(0, 0, 0, 0);
}