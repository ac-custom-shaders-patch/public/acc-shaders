#include "dclSkidmarks.hlsl"
#include "include/common.hlsl"
#include "include/samplers.hlsl"

cbuffer cbCSBuffer : register(b10) {
  uint gCount;
  uint gFade;
  uint2 _pad;
};

RWStructuredBuffer<SkidmarkBit> buBits : register(u0);

uint fadeAlpha(uint alpha) {
  if (alpha > gFade) return alpha - gFade;
  return 0;
}

[numthreads(256, 1, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  if (threadID.x < gCount) {
    SkidmarkBit b = buBits[threadID.x];

    if (needsSyncingColorWithSurface(b.COLOR1_FLAGS)) {
      float multA = 0.9;
      float multB = 0.9;
      b.A1_A2_S1_S2 = fadeAlpha(BYTE0(b.A1_A2_S1_S2)) | (fadeAlpha(BYTE1(b.A1_A2_S1_S2)) << 8) | (b.A1_A2_S1_S2 & 0xFFFF0000);
      
      // b.COLOR1_FLAGS = (b.COLOR1_FLAGS & ~0xffffff) | 0x00ff00;
      // b.COLOR2_TREADINDEX = (b.COLOR2_TREADINDEX & ~0xffffff) | 0x00ff00;

      buBits[threadID.x] = b;
    }
  }
}
