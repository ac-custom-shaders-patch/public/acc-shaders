#include "include/common.hlsl"

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 sampleTex(float2 uv){
  uv.y = uv.y * 2 - 1;
  uv.y *= 0.95;
  uv.y /= 3;
  uv.y = sin(uv.y * M_PI / 2);
  uv.y = uv.y * 0.5 + 0.5;
  return txDiffuse.SampleLevel(samLinearSimple, uv, 0);
}

float4 main(VS_Copy pin) : SV_TARGET {
  pin.Tex.y = 1 - pin.Tex.y;
  float4 r;
  if (pin.Tex.y > 0.8) r = lerp(sampleTex(pin.Tex), sampleTex(pin.Tex + float2(0, -1)), lerpInvSat(pin.Tex.y, 0.8, 1));
  else r = sampleTex(pin.Tex);
  r = lerp(float4(0.5, 0.5, 1, 0), r, saturate(r.w * 20) * pow(1 - abs(pin.Tex.x * 2 - 1), 3) * 8);
  r.xy = clamp(r.xy, -0.8, 0.8);
  // r.x = 1 - r.x;
  // r.z = 10;
  // r.x = 10;
  // if (r.w == 0) return 1;
  return r * 2 - 1;
}