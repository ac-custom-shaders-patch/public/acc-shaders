#include "include_new/base/_include_vs.fx"
#include "include/samplers.hlsl"
#include "dclSkidmarks.hlsl"

StructuredBuffer<SkidmarkBit> buBits : register(t0);
Texture2D txGrass : register(t1);
Texture2D<float> _txAreaAO : register(t2);

cbuffer _cbData : register(b10) {
  float gSimTime;
  float gPremult;
  float gCursor;
  float gCapacityInv;
  float2 gMapPointSI; // 1 / (gMapPointA - gMapPointB)
  float2 gMapPointBI; // -gMapPointB / (gMapPointA - gMapPointB)
  float4x4 gAreaHeightmapTransform;
}

float grassOpacity(float3 posW){
  float2 mapUV = posW.xz * gMapPointSI + gMapPointBI;
  return txGrass.SampleLevel(samLinearClamp, mapUV, 0).a;
}

PS_IN main(uint fakeIndex : SV_VERTEXID SPS_VS_ARG_CD) {
  uint vertexID = fakeIndex % 6;
  #ifdef USE_SPS
    uint bitID = fakeIndex / 12;
    uint instanceID = fakeIndex % 12 < 6 ? 0 : 1;
  #else
    uint bitID = fakeIndex / 6;
  #endif
  SkidmarkBit bit = buBits[bitID];
	
  float3 posBaseW = bit.P1 + extSceneOffset;
	float4 posW = float4(posBaseW, 1);
  bool useRight = vertexID == 1 || vertexID == 2 || vertexID == 4;
  bool useSecond = vertexID == 2 || vertexID >= 4;

  INIT_FLAGS(bit.COLOR1_FLAGS);
  
  #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_GBUFFER) && !defined(MODE_HEIGHTMAP) 
    bool useParallax = dirt || scratch || ice; 
    #ifdef USE_PARALLAX
      if (!useParallax) {
        DISCARD_VERTEX(PS_IN);
      }
      offroad = false;
    #else
      if (useParallax) {
        DISCARD_VERTEX(PS_IN);
      }
      dirt = false;
      scratch = false;
      ice = false;
    #endif
  #endif

  float3 d1 = loadVector(bit.Pd2_V2).xyz;
  float3 d2 = loadVector(bit.Pd4_TIMEDELTA).xyz - loadVector(bit.Pd3_BEND).xyz;
  float3 p3 = posW.xyz + loadVector(bit.Pd3_BEND).xyz * 1.001;
  float3 p4 = posW.xyz + loadVector(bit.Pd4_TIMEDELTA).xyz * 1.001;
  if (dot(d1, d2) < 0) {
    float3 p5 = p3;
    p3 = p4;
    p4 = p5;
  }

  if (vertexID == 1) {
    posW.xyz += d1;
  } else if (vertexID == 2 || vertexID == 4) {
    posW.xyz = p4;
  } else if (vertexID == 5) {
    posW.xyz = p3;
  }

  float3 normalW = normalDecode(loadVector(useSecond ? bit.NM1 : bit.NM2));

	PS_IN vout;
	vout.Tex = float2(useRight, bit.V1);
  if (useSecond) vout.Tex.y += loadVector(bit.Pd2_V2).w;

  #ifndef MODE_HEIGHTMAP
    vout.Ao = 1;
    
    [branch]
    if (gAreaHeightmapTransform[0][0]) {
      float4 areaUV = mul(float4(bit.P1.xyz, 1), gAreaHeightmapTransform);
      vout.Ao = _txAreaAO.SampleLevel(samLinearBorder1, areaUV.xy, 0);
    }
  #endif

  // float mult = 1;
  // [branch]
  // if (dirt){
  //   float3 posS0 = posBaseW + d1 / 2;
  //   float3 posS1 = p3 + d2 / 2;
  //   float grassO = grassOpacity(useSecond ? posS1 : posS0);
  //   mult = lerpInvSat(grassO, 0.5, 0.1);
  //   if (grassO > 0.5){
  //     float multO = grassOpacity(useSecond ? posS0 : posS1);
  //     if (multO > 0.5){
  //       dirt = false;
  //       offroad = true;
  //       mult = lerpInvSat(grassO, 0.5, 0.9);
  //     }
  //   }
  // }

  float mult = 1;
  #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_GBUFFER)
    [branch]
    if (dirt){
      float grassO = grassOpacity(posW.xyz);
      mult = lerpInvSat(grassO, 0.5, 0.2);
    }
  #endif

  vout.Alpha = (useSecond ? BYTE1(bit.A1_A2_S1_S2) : BYTE0(bit.A1_A2_S1_S2)) * mult / 255.;

  #ifndef MODE_HEIGHTMAP
    vout.Sweep = (useSecond ? BYTE3(bit.A1_A2_S1_S2) : BYTE2(bit.A1_A2_S1_S2)) / 255.;

    float distance = ((useSecond ? bitID + 1 : bitID) - gCursor) * gCapacityInv;
    float distanceMult = distance < 0 ? distance + 1 : distance;
    vout.Alpha *= sqrt(distanceMult) * saturate(distanceMult * 100);

    #if defined(MODE_SIMPLIFIED_FX) || defined(MODE_GBUFFER)
      if (scratch || max(BYTE1(bit.A1_A2_S1_S2), BYTE0(bit.A1_A2_S1_S2)) < 5 || sided){
        DISCARD_VERTEX(PS_IN);
      }
    #endif
    
    vout.NormalW = normalW;
    vout.PosC = posW.xyz - ksCameraPosition.xyz;

    float4 origPosW = posW;
    float3 toCamera = SPS_CAMERA_POS - posW.xyz;  
    // posW.y += 0.02;
    posW.xyz += toCamera * min(0.1, 0.1 / (0.5 + abs(toCamera.y)));

    float4 posV = mul(posW, ksView);
    float4 posH = mul(posV, ksProjection);
    // if (!offroad) 
    // posH.z -= 0.001;// * (1 - 0.5 * frac(bit.TIME / 10));
    // posH.z -= 0.1 * (1 - 0.5 * frac(bit.TIME / 10));
    // posH.z += 0.2;
    vout.PosH = posH;
    vout.Fog = calculateFog(posV);
    vout.TexQuad = float2(useRight ? 1 : -1, useSecond ? 1 : -1);

    // float3 d1 = loadVector(bit.Pd2).xyz;
    // float3 d2 = loadVector(bit.Pd4).xyz - loadVector(bit.Pd3).xyz;

    #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_GBUFFER)
      vout.Time = bit.TIME;
      if (useSecond) vout.Time += loadVector(bit.Pd4_TIMEDELTA).w;

      if (dirt){
        // vout.Time = (!useSecond ? BYTE1(bit.A1_A2_S1_S2) : BYTE0(bit.A1_A2_S1_S2)) * mult / 255.;
        // vout.Time = max(vout.Time - vout.Alpha, vout.Alpha - vout.Time);
        // vout.Alpha *= vout.Alpha;
      }

      float pieceWidth = useSecond ? length(p3 - p4) : length(d1);
      float pieceLength = length((p3 + p4) / 2 - posBaseW.xyz - d1 / 2);

      // vout.TangentW = normalize(loadVector(bit.Pd3_BEND).xyz);
      vout.BitangentW = normalize(d1);
      vout.TexBend = loadVector(bit.Pd3_BEND).w;
      vout.Ratio = pieceLength / pieceWidth;
      // vout.Ratio = 1 / vout.Ratio;
      // vout.Ratio = 1/(length(loadVector(bit.Pd3_BEND).xyz) / pieceWidth);
      // vout.Ratio = sqrt(dot2(loadVector(bit.Pd3_BEND).xyz) / dot2(d1));
      vout.PieceWidth = pieceWidth * ((IN_FLAGS & FLAG_RIGHT) ? -1 : 1);
      vout.UvRatio = loadVector(bit.Pd2_V2).w;
      // vout.TreadIndex = (float)((bit.COLOR2_TREADINDEX >> 24) & 0xff) - 1;
      // vout.PosCLenInv = 1 / length(vout.PosC);

      // if (trackDamage) vout.Sweep = 1;
      // if (trackDamage) vout.Ratio *= 4; // why?
      // if (trackDamage) 
      // vout.Ratio = 1 / length(d1); // why?
      // if (trackDamage) vout.Ratio = 1; // why?
    #elif defined(MODE_GBUFFER)
      vout.TexBend = loadVector(bit.Pd3_BEND).w;
    #endif

    // vout.Alpha = 1;

    vout.Color1_Flags = IN_FLAGS;
    vout.Color2_TreadIndex = bit.COLOR2_TREADINDEX;
    // vout.TexID = offroad ? GET_TYPES_TEX(TYPES_TEX_OFFROAD)
    //   : dirt ? GET_TYPES_TEX(TYPES_TEX_DIRT)
    //   : scratch ? GET_TYPES_TEX_SIDED(TYPES_TEX_SCRATCH, (IN_FLAGS & FLAG_SCRATCH_LEFT) ? -1 : (IN_FLAGS & FLAG_SCRATCH_RIGHT) ? 1 : 0) 
    //   : ice ? GET_TYPES_TEX_SIDED(TYPES_TEX_OFFROAD, (IN_FLAGS & FLAG_SCRATCH_LEFT) ? -1 : (IN_FLAGS & FLAG_SCRATCH_RIGHT) ? 1 : 2)
    //   : GET_TYPES_TEX(TYPES_TEX_MAIN);

    // if (ice && (IN_FLAGS & FLAG_SCRATCH_RIGHT) != 0) {
    //   vout.Tex.x -= vout.Tex.y * 4;
    //   #ifndef MODE_SIMPLIFIED_FX
    //     vout.TexBend = 0;
    //   #endif
    // }

    #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_GBUFFER)
      GENERIC_PIECE_MOTION(vin.PosL);
    #endif
    posW = origPosW;
    shadows(posW, SHADOWS_COORDS);
    SPS_RET(vout);
  #else
    if (!dirt){
      DISCARD_VERTEX(PS_IN);
    }

    // posW.y += 0.14 * 0.35;
    float4 posV = mul(posW, ksView);
    float4 posH = mul(posV, ksProjection);
    posH.z -= 0.75 * 0.14 / 600;
    vout.PosH = posH;
    vout.Tex.x = (vout.Tex.x * 2 - 1);
    return vout;
  #endif
}