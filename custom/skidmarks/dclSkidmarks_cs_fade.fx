#include "dclSkidmarks.hlsl"

cbuffer cbCSBuffer : register(b10) {
  uint gCount;
  uint gOffset;
  uint gCapacity;
  uint _pad;
  uint4 gFade[32];
};

RWStructuredBuffer<SkidmarkBit> buBits : register(u0);

void setAlphaMult(uint particle, float multA, float multB){
  SkidmarkBit b = buBits[particle];
  if (multB == 1) return;
  b.A1_A2_S1_S2 = (uint)(BYTE0(b.A1_A2_S1_S2) * multA) | ((uint)(BYTE1(b.A1_A2_S1_S2) * multB) << 8) | (b.A1_A2_S1_S2 & 0xFFFF0000);
  buBits[particle] = b;
}

[numthreads(32, 1, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  if (threadID.x < gCount) {
    uint4 fade = gFade[threadID.x];

    INIT_FLAGS(buBits[fade.w].COLOR1_FLAGS);
    if (trackDamage) {
      setAlphaMult(fade.x, 1, 0.8);
      setAlphaMult(fade.y, 0.8, 0.6);
      setAlphaMult(fade.z, 0.6, 0.4);
      setAlphaMult(fade.w, 0.4, 0);   
    } else {
      setAlphaMult(fade.z, 1, 0.7); 
      setAlphaMult(fade.w, 0.7, 0);
    }

    // setAlphaMult(fade.x, 1, 0.9);
    // setAlphaMult(fade.y, 0.9, 0.8);
    // setAlphaMult(fade.z, 0.8, 0.4);
    // setAlphaMult(fade.w, 0.4, 0);   

    // setAlphaMult(fade.x, 1, 1, 1, 0.9); 
    // setAlphaMult(fade.y, 1, 1, 0.9, 0.8); 
    // setAlphaMult(fade.z, 1, 0.7, 0.8, 0.4); 
    // setAlphaMult(fade.w, 0.7, 0, 0.4, 0);

  }
}
