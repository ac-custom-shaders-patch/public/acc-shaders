#include "include/common.hlsl"
#include "include/normal_encode.hlsl"

#define Byte4 uint
#define Half2 uint
#define Half4 uint2

#define FLAG_OFFROAD (1<<24)
#define FLAG_SCRATCH (2<<24)
#define FLAG_SCRATCH_LEFT (4<<24)
#define FLAG_SCRATCH_RIGHT (8<<24)
#define FLAG_DIRT (16<<24)
#define FLAG_RIGHT (32<<24)

struct SkidmarkBit {
  float3 P1;
  float V1;
  
  Half4 Pd2_V2;
  Half4 Pd3_BEND;

  Half4 Pd4_TIMEDELTA;
  Byte4 A1_A2_S1_S2;
  uint COLOR1_FLAGS;

  Half2 NM1;
  Half2 NM2;
  float TIME;
  uint COLOR2_TREADINDEX;
};

struct SkidmarkUpdateBit {
  Half4 Pd3_BEND;
  Half4 Pd4_TIMEDELTA;
  Byte4 p_A2_p_S2;
  uint FLAGS;
  Half2 NM2;
  uint INDEX;
  float V2;
  float3 _p2;
};

#define INIT_FLAGS(input)\
  uint IN_FLAGS = (input);\
  bool scratch = (IN_FLAGS & (FLAG_SCRATCH | FLAG_OFFROAD)) == FLAG_SCRATCH;\
  bool offroad /* marks on grass */ = (IN_FLAGS & (FLAG_SCRATCH | FLAG_OFFROAD)) == FLAG_OFFROAD;\
  bool ice = (IN_FLAGS & (FLAG_SCRATCH | FLAG_OFFROAD)) == (FLAG_SCRATCH | FLAG_OFFROAD);\
  bool dirt /* marks with parallax */ = (IN_FLAGS & (FLAG_DIRT | FLAG_OFFROAD)) == FLAG_DIRT;\
  bool trackDamage = (IN_FLAGS & (FLAG_SCRATCH_LEFT | FLAG_SCRATCH_RIGHT)) == (FLAG_SCRATCH_LEFT | FLAG_SCRATCH_RIGHT);\
  bool sided = !trackDamage && (IN_FLAGS & (FLAG_SCRATCH_LEFT | FLAG_SCRATCH_RIGHT));\
  bool sideL = (IN_FLAGS & (FLAG_SCRATCH_LEFT | FLAG_SCRATCH_RIGHT)) == FLAG_SCRATCH_LEFT;\
  bool sideR = (IN_FLAGS & (FLAG_SCRATCH_LEFT | FLAG_SCRATCH_RIGHT)) == FLAG_SCRATCH_RIGHT;

#define USE_FX_LOOK
#if defined(TARGET_VS) || defined(TARGET_PS)
  struct PS_IN {
    #ifdef MODE_HEIGHTMAP
    centroid
    #endif
    float4 PosH : SV_POSITION;
    float2 Tex : TEXCOORD0;
    float Alpha : TEXCOORD1;

    #ifndef MODE_HEIGHTMAP
      float Sweep : TEXCOORD2;
      float3 PosC : POSITION;
      float Fog : TEXCOORD6;
      float Ao : TEXCOORD46;
      float3 NormalW : NORMAL;
      float2 TexQuad : TEXCOORD27;
      #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_GBUFFER)
        float TexBend : TEXCOORD28;
        float3 BitangentW : TEXCOORD30;
        float Time : TEXCOORD25;
        // float PosCLenInv : TEXCOORD35;
        float Ratio : TEXCOORD32;
        float PieceWidth : TEXCOORD33;
        nointerpolation float UvRatio : TEXCOORD35;
      #elif defined(MODE_GBUFFER)
        float TexBend : TEXCOORD28;
      #endif
      nointerpolation uint Color1_Flags : TEXCOORD26;
      nointerpolation uint Color2_TreadIndex : TEXCOORD31;
      SHADOWS_COORDS_ITEMS
    #endif
  };

  #define TYPES_TEX_COUNT 4
  #define TYPES_TEX_MAIN 0
  #define TYPES_TEX_SCRATCH 1
  #define TYPES_TEX_OFFROAD 2
  #define TYPES_TEX_DIRT 3
  #define TYPES_TEX_SIDE_SHIFT 0.000001
  #define GET_TYPES_TEX(TYPE) ((float)(TYPE)/(float)TYPES_TEX_COUNT)
  #define GET_TYPES_TEX_SIDED(TYPE, SIDE) ((float)(TYPE)/(float)TYPES_TEX_COUNT + TYPES_TEX_SIDE_SHIFT * (SIDE))

  // scratch: FLAG_SCRATCH
  // offroad/grass: FLAG_OFFROAD
  // offroad/grass (thrown out): FLAG_OFFROAD | SIDES
  // ice cuts: FLAG_SCRATCH | FLAG_OFFROAD
  // dirt: FLAG_DIRT
  // dirt (thrown out, should act as offroad but have color syncing): FLAG_DIRT | FLAG_OFFROAD | SIDES
#else
  uint prepareDirtColor(float4 color){
    // if (any(color.rgb * 2 > 1)) return 0xff0000;
    return (packColor(color * 2) & 0xffffff);
  }

  bool needsSyncingColorWithSurface(uint flags) {
    return (flags & FLAG_DIRT)
      || (flags & (FLAG_OFFROAD | FLAG_SCRATCH)) == (FLAG_OFFROAD | FLAG_SCRATCH);
  }
#endif
