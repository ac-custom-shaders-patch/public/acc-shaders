#include "include/common.hlsl"

// #undef SAMPLES
// #define SAMPLES 1

#if SAMPLES == 1
  Texture2D txDiffuse : register(t0);
  Texture2D<float> txDepth : register(t1);
#else
  Texture2DMS<float4> txDiffuse : register(t0); 
  Texture2DMS<float> txDepth : register(t1); 
#endif

Texture2D txPrevFrame : register(TX_SLOT_PREV_FRAME);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

cbuffer cbData : register(b10) {
  float4 gRegion;
  float2 gSize;
}

#define MSTEX_SAMPLES SAMPLES
#define MSTEX_SIZE gSize
#include "include/ms_tex_sampling.hlsl"

float4 main(VS_Copy pin) : SV_TARGET {
  float2 uv = pin.Tex * gRegion.zw + gRegion.xy;

  float4 ret = sampleTex(txDiffuse, uv);
  float depth = sampleTex(txDepth, uv + float2(0, 2/gSize.y));

  [branch]
  if (depth > 0.1){
    float offset = 2.5;
    float above0 = sampleTex(txDiffuse, uv - float2(0, offset/gSize.y)).a;
    float4 fb = txPrevFrame.SampleLevel(samPointClamp, uv - float2(0, 3/gSize.y), 0);
    ret = lerp(ret, fb, lerpInvSat(above0, 0.5, 0.4) * lerpInvSat(depth, 0.1, 0.2));
    // ret.r = depth;
    // ret.gb = 0;
    // ret = fb;
    // ret = lerp(ret, fb, lerpInvSat(depth, 0.1, 0.2));
  }

  if (ret.a == 0){
    discard;
  }

  return ret;
}