#define STENCIL_VALUE 0
#define GBUFF_MASKING_MODE true
#define GBUFF_NORMAL_W_SRC float3(0,1,0)
#define GBUFF_NORMAL_W_PIN_SRC float3(0,1,0)
#define GET_MOTION(x) (float2)0
#define NO_CARPAINT
#define NO_ADJUSTING_COLOR
#define INPUT_AMBIENT_K 0.25
#define INPUT_DIFFUSE_K 0.25
#define LIGHTINGFX_KSDIFFUSE 0.25
#define LIGHTINGFX_FIND_MAIN
#define NO_EXTSPECULAR
// #define INPUT_AMBIENT_K 1
// #define INPUT_DIFFUSE_K 1

float stth(float v) { return abs(v - floor(v) - 0.5); }

#ifdef ALLOW_RAINFX
  #define RAINFX_USE_PUDDLES_MASK
#endif

#if !defined(MODE_SIMPLIFIED_FX)
  #define MODE_MAIN_FX
#endif
 
// #define CUSTOM_LFX_DIFFUSE_MULT

// float getDiffuseMultiplier(float3 normal, float3 lightDir) {  
//   return saturate(dot(normal, lightDir) * 1.05 - 0.05);
// }

// float getDiffuseMultiplier(float3 normal, float3 lightDir, float concentration) {
//   return saturate(dot(normal, lightDir) * 1.05 - 0.05);
// }

#ifdef USE_PARALLAX_PLUS
  #define CUSTOM_PS_OUT
  struct PS_OUT {
    float4 result : SV_Target0;
    float offset : SV_Target1;
  };
#endif

// #define USE_OCCLUSION_PARALLAX
#define USE_RELIEF_PARALLAX
#define PARALLAX_MAX_LAYERS 1
#define PARALLAX_MIN_LAYERS 1
#define PARALLAX_NUM_SEARCHES 5
// #define USE_BASIC_PARALLAX
#define parallaxScale 0.12
// #define IN_PARALLAX_SCALE (-0.5 * saturate(sin(ksGameTime / 600) * 0.5 + 0.5))
#define IN_PARALLAX_SCALE (-0.14)
// #define IN_PARALLAX_SCALE (-0.5)
#define IN_PARALLAX_STEPS 10
#define IN_PARALLAX_OFFSET 0.2
#define GET_HEIGHT(UV, DX, DY) lerp(0, DX.y ? cos(smoothstep(0, 1, pow(abs(UV.x), 1)) * M_PI * 1.5) * 0.5 + 0.5 : 0.3 * lerpInvSat(abs(abs(UV.x) - 0.75), 0.04, 0), DX.x)
// #define GET_HEIGHT(UV, DX, DY) lerp(1, saturate(1 - pow(UV.x * 2, 2) + pow(UV.x, 8) * 16), DX.x * saturate(pow(1 - abs(UV.x), 2) * 4))

#include "include_new/base/_include_ps.fx"
#include "../common/parallaxAltTwoDir.hlsl"
#include "dclSkidmarks.hlsl"

float parallaxHeight(float2 UV, float2 DX, float2 DY){
  // return 0;
  return 1 - lerp(0, DX.y 
    ? cos(smoothstep(0, 1, pow(abs(UV.x), 1)) * M_PI * 1.45 + 0.05) * (0.4 + 0.1 * DY.x) + 0.5 
    : 0.2 + 0.3 * lerpInvSat(abs(abs(UV.x) - 0.75), 0.04, 0), DX.x);
}

#undef AO_LIGHTING
#define AO_LIGHTING aoValue

float3 getHeatColor(float h) {
  float T = h * 5600;
  if (T < 400) return 0;
  float3 f = float3(1, 1.5, 2);
  float3 O = 100 * f * f * f / (exp(f * 19e3 / T) - 1);
  return 3 * O;
}

cbuffer _cbData : register(b10) {
  float gSimTime;
  float gPremult;
  float gCursor;
  float gCapacityInv;
}

Texture2DArray<float2> txTread : register(t1);

PS_OUT main(PS_IN pin) {
  #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_GBUFFER)
    bool isRight = pin.PieceWidth < 0;
    pin.PieceWidth = abs(pin.PieceWidth);
  #endif

  APPLY_EXTRA_SHADOW

  float aoValue = _AO_SSAO * pin.Ao;
  float3 normalW = normalize(pin.NormalW);
  float distanceToCamera = length(pin.PosC);
  float3 toCamera = pin.PosC / distanceToCamera;
  // float3 toCamera = (pin.PosC) * pin.PosCLenInv;
  
  INIT_FLAGS(pin.Color1_Flags);
  uint threadIndex = (pin.Color2_TreadIndex >> 24) & 0xff;

  #if defined(MODE_SIMPLIFIED_FX) || defined(MODE_GBUFFER)
    scratch = false;
    sided = false;
  #elif defined(USE_PARALLAX)
    offroad = false;
  #else
    ice = false;
    dirt = false;
    scratch = false;
    trackDamage = false;
  #endif

  #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_GBUFFER)
    float3 bitangentW = normalize(pin.BitangentW); // yup they are inverted, that’s not a mistake, might fix field name later
      // there was a huge error, using Pd3 for tangent was a big mistake
    float3 tangentW = normalize(cross(pin.BitangentW, normalW));
    float2 origTexQuad = pin.TexQuad;
  #endif

  float pinTexID = offroad ? GET_TYPES_TEX(TYPES_TEX_OFFROAD)
      : dirt ? GET_TYPES_TEX(TYPES_TEX_DIRT)
      : scratch || ice ? GET_TYPES_TEX(TYPES_TEX_SCRATCH) 
      : GET_TYPES_TEX(TYPES_TEX_MAIN);
  
  if (offroad){
    pin.Tex.y *= 3;
  }

  bool useParallax = false;
  float2 texDx = float2(saturate(pow(pin.Alpha * 1.5, 4) - 0.4), dirt ? 1 : 0);
  #ifdef USE_PARALLAX
    useParallax = (dirt || scratch || ice) && texDx.x;
    if (trackDamage) texDx.x *= 0.3;
  #endif
  
  float depthOffset = 0;
  float shadowMult = 1;
  float2 texDy = 1;
  float tess = 1;
  float height = 1;

  // pin.Ratio = 4;

  #define IN_PARALLAX_TANGENT bitangentW
  #define IN_PARALLAX_BITANGENT tangentW
  #define IN_PARALLAX_NORMAL normalW

  // Bending texture coordinates
  #ifndef MODE_SIMPLIFIED_FX
    pin.Tex.x += (pow(abs(pin.TexQuad.y), 2) - 1) * pin.TexBend;
    pin.TexQuad.x += (pow(abs(pin.TexQuad.y), 2) - 1) * pin.TexBend * 2;
  #endif

  #ifdef USE_PARALLAX
    if (ice) {
      pin.Tex.x += pin.Time * 2;

      float columnsCount = (float)(threadIndex & 15);
      float rowsScale = 1 + (float)((threadIndex >> 4) & 15);
      // pin.Sweep = 0;

      float widthK = columnsCount <= 4 ? lerp(2, 5, lerpInvSat(columnsCount, 4, 1)) : lerp(2, 1, lerpInvSat(columnsCount, 4, 10));
      float spikeIn = pin.Tex.x * columnsCount;
      float spikeX = frac(spikeIn) * widthK - (widthK - 1) / 2;

      float rowID = floor(spikeIn);
      float waveIn = pin.Tex.y * rowsScale + rowID * 11.811;
    
      // 60→7
      // 10→1
      // float4 noiseR = floor(txNoise.SampleLevel(samPoint, float2(1, floor(waveIn) / 31.7), 0) * 7.999);
      // if (any(noiseR == rowID)) discard;

      float4 noise = txNoise.SampleLevel(samPoint, float2(rowID / columnsCount, floor(waveIn) / 31.7), 0);
      float kw = min(0.95, lerp(0.8, 0.15, pin.Sweep) * (rowsScale / 7));
      float wave = saturate((1 - abs(frac(waveIn + lerp(-0.5 + kw / 2, 0.5 - kw / 2, noise.z)) * 2 - 1)) / kw - (1./kw - 1));
      if (!wave) discard;

      spikeX += (noise.x - 0.5) * lerp(1, 2, lerpInvSat(columnsCount, 4, 1));
      if (spikeX < 0 || spikeX > 1) discard;
      pin.Tex.x = 0.05 + 0.1 * spikeX;
      pin.TexQuad.x = pin.Tex.x * 2 - 1;
      pin.Alpha *= saturate(wave * (2 - pow(abs(pin.TexQuad.x), 2))) * lerpInvSat(fwidth(spikeIn) - pin.Sweep, 1.1, 0.8) 
        * smoothstep(0, 1, saturate(2 - 2 * abs(origTexQuad.x)))
        * lerp(0.6, 1, pin.Sweep);
      // useParallax = false;
    } else if (scratch) {
      pin.TexQuad.x = (pin.TexQuad.x) / pow(1 - pin.Sweep, 4);
    }
  #endif

  float parallaxSelfShadow = 1;
  #ifdef USE_PARALLAX
    [branch]
    if (useParallax && true){
      float2 baseQuad = pin.TexQuad;
      tangentW /= pin.Ratio;
      float3 eyeTS = -normalize(float3(dot(toCamera, IN_PARALLAX_TANGENT), dot(toCamera, IN_PARALLAX_BITANGENT), dot(toCamera, IN_PARALLAX_NORMAL)));
      // eyeTS.xz /= pin.Ratio;
      // pin.TexQuad.y *= pin.Ratio;
      // pin.TexQuad.x /= pin.Ratio;
      pin.TexQuad = parallaxMap(pin.TexQuad, eyeTS, texDx, texDy, tess, height);
      // pin.TexQuad.y /= pin.Ratio;
      float2 dTex = (pin.TexQuad - baseQuad) * 0.5;
      // pin.Tex += dTex * float2(abs(pin.PieceWidth), pin.Ratio * abs(pin.PieceWidth));
      pin.Tex += dTex * float2(1, pin.UvRatio);
      // pin.Tex += dTex;// * float2(abs(pin.PieceWidth), pin.Ratio * abs(pin.PieceWidth));
      // pin.Tex += dTex * float2(1, pin.Ratio * 1);

      // TODO: dynamic shadows should use original value!
      float3 dPos = tangentW * (dTex.y * pin.PieceWidth * pin.Ratio)
        + bitangentW * (dTex.x * pin.PieceWidth);
      float3 posShift = toCamera * dot2(dPos) / dot(dPos, toCamera);
      pin.PosC += posShift;
      pin.ShadowTex0.xyz = mul(float4(ksCameraPosition.xyz + pin.PosC - ksLightDirection.xyz * max(0.1, -posShift.y) / max(0.1, -ksLightDirection.y), 1), ksShadowMatrix0).xyz;
      pin.Alpha *= 1 + saturate((1 - pow(pin.TexQuad.x, 2)) * 3.5 - 2.5) * 0.2 * texDx.x;

      float3 lightTS = -normalize(float3(dot(ksLightDirection.xyz, IN_PARALLAX_TANGENT), dot(ksLightDirection.xyz, IN_PARALLAX_BITANGENT), dot(ksLightDirection.xyz, IN_PARALLAX_NORMAL)));
      parallaxSelfShadow = parallaxSoftShadowMultiplier(lightTS, pin.TexQuad * float2(1, pin.Ratio), height, texDx, texDy, tess, 0 PARALLAX_EXTRA_ARGS_PASS);

      // parallaxSelfShadow = 1;
      // pin.Alpha = 1;
    }

  {
    RESULT_TYPE ret;
    // ret.result = float4(txDiffuseValue.xyz * INPUT_AMBIENT_K, 1);
    ret.result = float4(sin(pin.Tex * 14) * 0.1, 0, 1);
    // ret.result = float4(pin.TexQuad * 0.1, 0, 1);
    // ret.result = float4(frac((pin.TangentW + 1) * 10) * 0.1, 1);
    // return ret;
  }
  #endif
  
  // Some random noise
  float3 posW = ksInPositionWorld;
  float4 txNoiseValue = txNoise.SampleLevel(samLinearSimple, posW.xz * 0.03, 0);
  float4 txNoiseL = txNoise.SampleLevel(samLinearSimple, posW.xz * 0.2, 0);
  float4 txNoiseX = txNoise.SampleLevel(samLinearSimple, float2(pin.Tex.x * (dirt ? 1 : 0.3), pin.Tex.y * 0.01), 0);
  float randFade = saturate((txNoiseValue.x * txNoiseValue.y + txNoiseValue.z * txNoiseValue.w) * 0.9);
  if (!scratch) {
    if (!ice) {
      pin.Tex.x = (pin.Tex.x - 0.5) * (0.9 + randFade * 0.8 + lerp(0.5, 0, saturate(pin.Alpha * 3))) + 0.5;
    }
    
    #ifndef MODE_SIMPLIFIED_FX
      if (dirt) {
        pin.Tex.x += pow(pin.TexQuad.x, 4) * (txNoiseValue.x - 0.5) / 2;
        pin.Alpha *= lerpInvSat(abs(pin.TexQuad.x) - saturate(txNoiseValue.y * 4 - 2 + 1.5 * texDx.x) / 2, 0.5, 0.4);
      }
    #endif
  } else {
    pin.Tex.x = (pin.Tex.x - 0.5) / pow(1 - pin.Sweep, 4) + 0.5;
    if (sided) clip(sideL ? pin.Tex.x - 0.5 : 0.5 - pin.Tex.x);
  }

  // Main texture
  float4 txDiffuseValue = txDiffuse.SampleBias(samLinear, pin.Tex * float2(1. / TYPES_TEX_COUNT, 1) + float2(pinTexID, 0),
    trackDamage ? 2 : 0);
  txDiffuseValue.w = max(txDiffuseValue.w, lerpInvSat(height, 0.7, 0.6));

  // txDiffuseValue.w = 1;
  // txDiffuseValue.rgb = 1;
  // txDiffuseValue.w *= frac(pin.TexQuad.y * 6) > 0.1;
  // txDiffuseValue.w *= frac(pin.TexQuad.x * 6) > 0.1;

  #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_GBUFFER)
    txDiffuseValue.xy = txDiffuseValue.xy * 2 - 1;
    txDiffuseValue.y /= 3;
    
    [branch]
    if (dirt && threadIndex) {
      float2 treadUV = (pin.TexQuad * 1.4 * 0.5 * float2(1, pin.Ratio));
      float mult = lerpInvSat(abs(treadUV.x), 0.5, 0.3) * saturate(max(txNoiseX.x, txNoiseX.y) * 2.8) * lerpInvSat(pin.Sweep, 0.4, 0.1);
      // float mult = 2;
      if (mult){
        float3 treadUVFinal = float3(treadUV + 0.5, (float)threadIndex - 1);
        if (isRight) treadUVFinal.x = 1 - treadUVFinal.x;
        float2 txTreadValue = txTread.Sample(samLinear, treadUVFinal);
        if (isRight) txTreadValue.x = -txTreadValue.x;
        txDiffuseValue.rg = lerp(txDiffuseValue.rg, txTreadValue, mult);
      }
    }

    if (!offroad || !sided) {
      normalW = normalize(-bitangentW * txDiffuseValue.x - tangentW * txDiffuseValue.y 
        + normalW * ((trackDamage ? 2 : 0) + sqrt(1 - dot2(txDiffuseValue.xy))));
    }

    txDiffuseValue.a *= saturate(10 - abs(pin.TexQuad.x) * 10); // fading for edges
  #endif

  float alpha = txDiffuseValue.a * pin.Alpha;
  float2 baseNoise = lerp(txNoiseX.xz * txNoiseX.yw, txNoiseL.xz * txNoiseL.yw * 0.5, pin.Sweep);
  alpha = saturate(alpha * lerp(1.2, 0.7, randFade) * lerp(1.2, 0, baseNoise.x));
  #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_GBUFFER)
  if (useNearbyInteriorClipping){
    alpha *= saturate(remap(distanceToCamera, 1.1, 1.2, 0, 1));
  }
  #endif
  if (dirt) alpha = saturate(alpha * (1 + 3 * texDx.x));
  #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_GBUFFER)
    // alpha *= saturate((gSimTime - pin.Time));
  #endif
  // alpha = 1;
  // clip(alpha - 0.001);

  float3 glow = 0;
  #ifndef MODE_SIMPLIFIED_FX_
    // if (dirt) baseNoise.y = max(baseNoise.y, pow(txDiffuseValue.b, 4));
    if (offroad || dirt) baseNoise.y = lerp(baseNoise.y, txDiffuseValue.b, 0.3);
    float diffuseBlend = saturate(baseNoise.y * 1.5);
    float3 c1 = unpackColor(asuint(pin.Color1_Flags)).rgb;
    float3 c2 = unpackColor(asuint(pin.Color2_TreadIndex)).rgb;
    if (dirt){
      txDiffuseValue.rgb = lerp(c1, c2, pin.TexQuad.y * 0.5 + 0.5) * lerp(1.5, 2.5, diffuseBlend);
      if (false && trackDamage) {
        txDiffuseValue.bg = 0;
        txDiffuseValue.r = 1;
      } else {
        #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_GBUFFER)
          alpha *= saturate((gSimTime - pin.Time) * 100);
        #endif
      }
    } else {
      txDiffuseValue.rgb = lerp(c1, c2, diffuseBlend);
      if (ice) {
        txDiffuseValue.rgb = txDiffuseValue.rgb * 2.5;
        // alpha = 1;
        // txDiffuseValue.bg = 0;
        // txDiffuseValue.r = 4;
      } else if (scratch) {
        pin.Alpha *= pow(1 - pin.Sweep, 4);
        #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_GBUFFER)
          glow = getHeatColor(0.5 * saturate(1 - (gSimTime - pin.Time))) * 3;
        #endif
        aoValue *= 1 - txDiffuseValue.a * 0.3;
        alpha = saturate(txDiffuseValue.a + saturate(pin.Alpha * 1.6) - 1);
      } else {
        #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_GBUFFER)
          if (offroad) {
            txDiffuseValue.rgb *= lerp(0.7, 1, saturate((gSimTime - pin.Time) / 60));
            alpha *= 1 - (txNoiseL.x + abs(pin.TexQuad.x) * (sided ? 1 : 0.5)) * abs(pin.TexQuad.x);
            if (sided) {
              if (IN_FLAGS & (FLAG_DIRT)) txDiffuseValue.rgb *= 2; 
              alpha *= 0.5;
            }
          }
          alpha *= saturate((gSimTime - pin.Time) * (sided ? 5 : 100));
        #endif
      }
    }
  #else
    txDiffuseValue.rgb = unpackColor(asuint(pin.Color1)).rgb * (dirt ? 2.5 : 1);
    if (offroad) {
      alpha *= saturate(txNoiseL.x);
    }
  #endif

  if (!ice && !offroad) {
    // alpha = max(alpha, 0.1);
  }
  clip(min(0.5 - abs(pin.Tex.x - 0.5), alpha - 0.005)); // clipping instead of repeating texture on X axis

  // Shadow and rain stuff
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW) * shadowMult;
  shadow *= parallaxSelfShadow;

  // RAINFX_INIT;
  RAINFX_WET(txDiffuseValue.xyz);

  // Main lighting
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.specularValue = dirt ? 0.03 :lerp(0, 0.1, pow(alpha, 2));
  L.specularExp = dirt ? 5 : lerp(20, 60, pow(alpha, 2));
  if (ice) {
    L.specularValue = 1;
    L.specularExp = 40;
  }
  float3 lighting = L.calculate();

  #ifndef MODE_GBUFFER
    float3 lightingBasic = lighting;
    LFX_MainLight mainLight;
    LIGHTINGFX(lighting);

    #ifdef USE_PARALLAX
      [branch]
      if (useParallax) {
        float lightingFocus = saturate(mainLight.power / max(dot(lighting - lightingBasic, 1), 0.001) * 2 - 1);
        float3 lightTS = -normalize(float3(dot(-mainLight.dir, IN_PARALLAX_TANGENT), dot(-mainLight.dir, IN_PARALLAX_BITANGENT), dot(-mainLight.dir, IN_PARALLAX_NORMAL)));
        parallaxSelfShadow = parallaxSoftShadowMultiplier(lightTS, pin.TexQuad * float2(1, pin.Ratio), height, texDx, texDy, tess, 0 PARALLAX_EXTRA_ARGS_PASS);
        parallaxSelfShadow = lerp(1, parallaxSelfShadow, lightingFocus * tess);
        lighting = lerp(lightingBasic, lighting, parallaxSelfShadow);
      }
    #endif
  #endif

  if (GAMMA_FIX_ACTIVE) {
    glow = pow(glow, 2) * getEmissiveMult();
  }

  // ReflParams R = getReflParamsZero();
  // R.useBias = true;

  //   R.fresnelEXP = 5;
  //   R.ksSpecularEXP = 255;
  //   R.finalMult = 1;
  //   R.metallicFix = 1;
  //   R.reflectionSampleParam = REFL_SAMPLE_PARAM_DEFAULT;
  //   R.coloredReflections = 0;
  //   R.coloredReflectionsColor = 0;
  //   R.fresnelMaxLevel = 1;
  //   R.fresnelC = 0.4; 
  // R.useBias = true;
  // APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);
  // float4 withReflection = calculateReflection(float4(lighting, txDiffuseValue.a), toCamera, pin.PosC, normalW, R);
  float4 withReflection = float4(lighting, txDiffuseValue.a);

  float4 result = float4(withReflection.rgb + glow, saturate(alpha));

  // result.rg += frac(pin.Tex * 4) > 0.95;
  // result.rg += frac(pin.TexQuad * 4) > 0.95;
  // result.r = shadowMult;
  // result.rg = txTread.Sample(samLinear, float3(pin.TexQuad, pin.TreadIndex));

  // float v = frac(parallaxHeight(pin.TexQuad, texDx, texDy));
  float v = shadow;
  // result.rgb = (lighting.g - lightingBasic.g) + frac(ksInPositionWorld.xyz * 5);
  // result.rgb = float3(v, 1 - v, 0);
  // result.rgb = float3(height, 1 - height, 0);
  // result.rgb = float3(parallaxSelfShadow, 1 - parallaxSelfShadow, 0);
  // result.rgb = frac(ksInPositionWorld.xyz * 5);
  // result.rgb = frac(ksInPositionWorld.z * 5);
  // result.rg = abs(pin.TexQuad > 0);
  // result.rgb = float3(saturate(baseNoise.y * 1.5), 1 - saturate(baseNoise.y * 1.5), 0);
  // result.rgb = float3(frac(pin.Tex.y), 1 - frac(pin.Tex.y), 0);
  #ifdef NO_LIGHTING
    // result.rgb = 0.0;
  #endif  
  #ifdef ALLOW_RAINFX
    result.a *= 1 - RP.water;
    RP = (RainParams)0;
  #endif

  #if !defined(MODE_SIMPLIFIED_FX) && !defined(MODE_GBUFFER)
    if (gPremult){
      result.rgb *= saturate(result.a * 6); 
    }
  #endif

  #ifdef MODE_GBUFFER
    {
      RESULT_TYPE ps_result;
      // alpha = 1;
      ps_result.normal = 1;
      #ifdef CREATE_REFLECTION_BUFFER
        ps_result.baseReflection = pow(1 - alpha, 2);
        ps_result.reflectionColorBlur = 1;
      #endif
      #ifdef CREATE_MOTION_BUFFER
        ps_result.motion = 1;
        ps_result.stencil = 1;
      #endif
      return ps_result;
    }
  #else
    // result.r = 10;

    // result.rgb = txDiffuseValue.rgb;
    // result.a = 1;
    {
      RESULT_TYPE ret;
      ret.result = float4(txDiffuseValue.xyz * INPUT_AMBIENT_K, 1);
      // ret.result = float4(sin(pin.Tex * 4) * 0.1, 0, 1);
      // ret.result = float4(frac((pin.TangentW + 1) * 10) * 0.1, 1);
      // return ret;
    }

    result.a = GAMMA_ALPHA(result.a);
    RETURN_BASE(result.rgb, result.a);

    #ifdef USE_PARALLAX_PLUS
      ps_result.offset = height * (1 - pow(pin.TexQuad.x, 2)) * lerpInvSat(alpha, 0.3, 0.6)
        * lerpInvSat(abs(dot(bitangentW, toCamera)), 0.04, 0.02) * lerpInvSat(abs(dot(tangentW, toCamera)), 0.95, 0.97);
      ps_result.offset = lerpInvSat(height, 0.8, 0.4) * saturate(abs(ddy(origTexQuad.y)) * min(extCameraTangent * 0.6, 4) - 1)
        * lerpInvSat(abs(dot(bitangentW, toCamera)), 0.05, 0.03) * lerpInvSat(abs(dot(normalize(pin.NormalW), toCamera)), 0.04, 0.02)
        ;
      return ps_result;
    #endif
  #endif

}
