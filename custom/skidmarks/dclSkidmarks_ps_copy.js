[ 1, 2, 4, 8 ].map(samples => ({
  source: `dclSkidmarks_ps_copy.hlsl`,
  target: FX.Target.PS,
  saveAs: `dclSkidmarks_ps_copy_${samples}.fxo`,
  defines: { 'SAMPLES': samples }
})).flat()
