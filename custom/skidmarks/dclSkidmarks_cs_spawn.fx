#include "dclSkidmarks.hlsl"
#include "include/packing_cs.hlsl"
#include "include/common.hlsl"
#include "include/samplers.hlsl"

cbuffer cbCSBuffer : register(b10) {
  uint gCount;
  uint gOffset;
  uint gCapacity;
  uint _pad;
  float2 gMapPointA;
  float2 gMapPointB;
  SkidmarkBit gBits[32];

  float3 gSceneOffset;
  float _pad2;
  float4x4 gAreaHeightmapTransform;
};

RWStructuredBuffer<SkidmarkBit> buBits : register(u0);

Texture2D txGrass : register(t21);
Texture2D txAreaColor : register(t22);
Texture2D<float> txAreaDepth : register(t23);

#include "include/track_area_heightmap.hlsl"

float grassOpacity(float3 posW){
  float2 mapUV = (posW.xz - gMapPointB) / (gMapPointA - gMapPointB);
  return lerpInvSat(txGrass.SampleLevel(samLinearClamp, mapUV, 0).a, 0.7, 0.2);
}

void fixY(inout float3 posW){  
  float4 areaUV = mul(float4(posW, 1), gAreaHeightmapTransform);
  if (all(abs(areaUV.xyz - 0.5) < 0.5)) {
    float depth = txAreaDepth.SampleLevel(samLinearClamp, areaUV.xy, 0);
    float distanceToGround = (depth - areaUV.z) * 300;
    if (distanceToGround > -0.5 && distanceToGround < 0.5) {
      posW.y -= distanceToGround;
    }
  }
}

[numthreads(32, 1, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  if (threadID.x < gCount) {
    SkidmarkBit bit = gBits[threadID.x];

    float3 p1 = bit.P1;
    float3 p2 = p1 + loadVector(bit.Pd2_V2).xyz;
    float3 p3 = p1 + loadVector(bit.Pd3_BEND).xyz;
    float3 p4 = p1 + loadVector(bit.Pd4_TIMEDELTA).xyz;
    fixY(p1);
    fixY(p2);
    fixY(p3);
    fixY(p4);

    bit.P1 = p1;
    __pack(bit.Pd2_V2, float4(p2 - p1, loadVector(bit.Pd2_V2).w));
    __pack(bit.Pd3_BEND, float4(p3 - p1, loadVector(bit.Pd3_BEND).w));
    __pack(bit.Pd4_TIMEDELTA, float4(p4 - p1, loadVector(bit.Pd4_TIMEDELTA).w));

    [branch]
    if (needsSyncingColorWithSurface(bit.COLOR1_FLAGS)) {
      float4 c0 = TA_getSurfaceColor((p1 + p2) / 2);
      if (c0.w > 0.99) bit.COLOR1_FLAGS = (bit.COLOR1_FLAGS & ~0xffffff) | prepareDirtColor(c0);
      // else bit.COLOR1_FLAGS = (bit.COLOR1_FLAGS & ~0xffffff);
      float4 c1 = TA_getSurfaceColor((p3 + p4) / 2);
      if (c1.w > 0.99) bit.COLOR2_TREADINDEX = (bit.COLOR2_TREADINDEX & ~0xffffff) | prepareDirtColor(c1);
      // else bit.COLOR2_TREADINDEX = (bit.COLOR2_TREADINDEX & ~0xffffff);
    }
    
    // bit.COLOR1_FLAGS = (bit.COLOR1_FLAGS & ~0xffffff) | 0xff00ff;
    // bit.COLOR2_TREADINDEX = (bit.COLOR2_TREADINDEX & ~0xffffff) | 0xff00ff;

    // bit.A1_A2_S1_S2 = (bit.A1_A2_S1_S2 & 0xFFFF0000)
    //   | ((uint)((float)BYTE0(bit.A1_A2_S1_S2) * grassOpacity((p1 + p2) / 2)) << 0)
    //   | ((uint)((float)BYTE1(bit.A1_A2_S1_S2) * grassOpacity((p3 + p4) / 2)) << 8);

    buBits[(gOffset + threadID.x) % gCapacity] = bit;
  }
}
