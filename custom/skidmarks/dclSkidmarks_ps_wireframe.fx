#define STENCIL_VALUE 0
#define GBUFF_MASKING_MODE true
#define GBUFF_NORMAL_W_SRC float3(0,1,0)
#define GBUFF_NORMAL_W_PIN_SRC float3(0,1,0)
#define GET_MOTION(x) (float2)0
#define NO_CARPAINT
#define NO_ADJUSTING_COLOR
#define INPUT_AMBIENT_K 0.2
#define INPUT_DIFFUSE_K 0.2
#define MODE_MAIN_FX

#include "include_new/base/_include_ps.fx"
#include "dclSkidmarks.hlsl"

float4 main(PS_IN pin) : SV_TARGET {
  // discard;
  return float4(1, 0, 0, 1);
}
