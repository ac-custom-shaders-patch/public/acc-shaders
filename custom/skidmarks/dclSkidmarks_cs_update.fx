#include "dclSkidmarks.hlsl"
#include "include/packing_cs.hlsl"
#include "include/samplers.hlsl"

cbuffer cbCSBuffer : register(b10) {
  uint gCount;
  uint gOffset;
  uint gCapacity;
  uint _pad;
  float2 gMapPointA;
  float2 gMapPointB;
  SkidmarkUpdateBit gUpdate[32];

  float3 gSceneOffset;
  float _pad2;
  float4x4 gAreaHeightmapTransform;
};

RWStructuredBuffer<SkidmarkBit> buBits : register(u0);

Texture2D txGrass : register(t21);
Texture2D txAreaColor : register(t22);
Texture2D<float> txAreaDepth : register(t23);

#include "include/track_area_heightmap.hlsl"

float grassOpacity(float3 posW){
  float2 mapUV = (posW.xz - gMapPointB) / (gMapPointA - gMapPointB);
  return lerpInvSat(txGrass.SampleLevel(samLinearClamp, mapUV, 0).a, 0.7, 0.2);
}

void fixY(inout float3 posW){  
  float4 areaUV = mul(float4(posW, 1), gAreaHeightmapTransform);
  if (all(abs(areaUV.xyz - 0.5) < 0.5)) {
    float depth = txAreaDepth.SampleLevel(samLinearClamp, areaUV.xy, 0);
    float distanceToGround = (depth - areaUV.z) * 300;
    if (distanceToGround > -0.5 && distanceToGround < 0.5) {
      posW.y -= distanceToGround;
    }
  }
}

[numthreads(32, 1, 1)]
void main(uint3 threadID : SV_DispatchThreadID) {
  if (threadID.x < gCount) {
    SkidmarkUpdateBit update = gUpdate[threadID.x];
    SkidmarkBit bit = buBits[update.INDEX];

    Half4 bak_Pd2_V2 = bit.Pd2_V2;
    float4 decoded = loadVector(bit.Pd2_V2);
    decoded.w = update.V2 - bit.V1;
    __pack(bit.Pd2_V2, decoded);
    bit.Pd2_V2 = 0xFFFFFF & bak_Pd2_V2 | 0xFF000000 & bit.Pd2_V2;

    bit.Pd3_BEND = update.Pd3_BEND;
    bit.Pd4_TIMEDELTA = update.Pd4_TIMEDELTA;
    bit.NM2 = update.NM2;
    // bit.V2 = update.V2;

    float3 p1 = bit.P1;
    float3 p2 = p1 + loadVector(bit.Pd2_V2).xyz;
    float3 p3 = p1 + loadVector(bit.Pd3_BEND).xyz;
    float3 p4 = p1 + loadVector(bit.Pd4_TIMEDELTA).xyz;

    fixY(p3);
    fixY(p4);
    __pack(bit.Pd3_BEND, float4(p3 - p1, loadVector(bit.Pd3_BEND).w));
    __pack(bit.Pd4_TIMEDELTA, float4(p4 - p1, loadVector(bit.Pd4_TIMEDELTA).w));

    bit.A1_A2_S1_S2 = (bit.A1_A2_S1_S2 & 0x00FF00FF) 
      | (update.p_A2_p_S2 & 0xFF00FF00);
      // | (update.p_A2_p_S2 & 0xFF000000)
      // | ((uint)((float)BYTE1(update.p_A2_p_S2) * grassOpacity((p3 + p4) / 2)) << 8);
    bit.COLOR1_FLAGS = (bit.COLOR1_FLAGS & 0xFFFFFF) | (update.FLAGS & 0xFF000000);

    [branch]
    if (needsSyncingColorWithSurface(bit.COLOR1_FLAGS)) {
      float4 c1 = TA_getSurfaceColor((p3 + p4) / 2);
      if (c1.w > 0.99) bit.COLOR2_TREADINDEX = (bit.COLOR2_TREADINDEX & ~0xffffff) | prepareDirtColor(c1);
    }

    buBits[update.INDEX] = bit;
  }
}
