add(`dclSkidmarks_ps_main.hlsl`, { saveAs: `dclSkidmarks_ps_mirror.fxo`, defines: { 'MODE_SIMPLIFIED_FX': 1 } });
add(`dclSkidmarks_ps_main.hlsl`, { saveAs: `dclSkidmarks_ps_gbuff.fxo`, defines: { 'MODE_GBUFFER': 1, 'CREATE_REFLECTION_BUFFER': true } });
add(`dclSkidmarks_vs.fx`, { saveAs: `dclSkidmarks_vs_heightmap.fxo`, defines: { 'MODE_HEIGHTMAP': 1 } });
add(`dclSkidmarks_vs.fx`, { saveAs: `dclSkidmarks_vs_mirror.fxo`, defines: { 'MODE_SIMPLIFIED_FX': 1 } });
add(`dclSkidmarks_vs.fx`, { saveAs: `dclSkidmarks_vs_gbuff.fxo`, defines: { 'MODE_GBUFFER': 1, 'CREATE_REFLECTION_BUFFER': true } });
add(`dclSkidmarks_vs.fx`, { saveAs: `dclSkidmarks_vs_parallax.fxo`, defines: { 'USE_PARALLAX': 1 } });

[ null, 'ALLOW_RAINFX' ].map(RAIN => [ null, 'ALLOW_DYNAMIC_SHADOWS' ].map(SHADOWS => [ null, 'USE_PARALLAX_BASE', 'USE_PARALLAX_PLUS' ].map(PARALLAX => {
  // if (RAIN || SHADOWS 
  //   || !PARALLAX 
  //   || PARALLAX == 'USE_PARALLAX_BASE') return;
  add(`dclSkidmarks_ps_main.hlsl`, {
    saveAs: `dclSkidmarks_ps_main_${(PARALLAX === 'USE_PARALLAX_PLUS' ? 'x' : PARALLAX ? 'p' : 'n')}${(RAIN ? 'r' : 'n')}${(SHADOWS ? 's' : 'n')}.fxo`.replace(/_(?=\.)/, ''),
    defines: { [RAIN]: 1, [SHADOWS]: 1, [PARALLAX]: 1, [PARALLAX ? 'USE_PARALLAX' : null]: 1 }
  });
}))).flat(1e9) 
