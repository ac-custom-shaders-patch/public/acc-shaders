#define FXAA_PC 1
#define FXAA_HLSL_5 1

struct FXAAShaderConstants
{
  // {x_} = 1.0/screenWidthInPixels
  // {_y} = 1.0/screenHeightInPixels
  float2 rcpFrame;

  float2 _packing0;

  // This must be from a constant/uniform.
  // {x___} = 2.0/screenWidthInPixels
  // {_y__} = 2.0/screenHeightInPixels
  // {__z_} = 0.5/screenWidthInPixels
  // {___w} = 0.5/screenHeightInPixels
  float4 rcpFrameOpt;

  // {x___} = -N/screenWidthInPixels  
  // {_y__} = -N/screenHeightInPixels
  // {__z_} =  N/screenWidthInPixels  
  // {___w} =  N/screenHeightInPixels 
  float4 rcpFrameOpt2;

  // This can effect sharpness.
  //   1.00 - upper limit (softer)
  //   0.75 - default amount of filtering
  //   0.50 - lower limit (sharper, less sub-pixel aliasing removal)
  //   0.25 - almost off
  //   0.00 - completely off
  float fxaaQualitySubpix;

  // The minimum amount of local contrast required to apply algorithm.
  //   0.333 - too little (faster)
  //   0.250 - low quality
  //   0.166 - default
  //   0.125 - high quality 
  //   0.063 - overkill (slower)
  float fxaaQualityEdgeThreshold;

  // This used to be the FXAA_QUALITY__EDGE_THRESHOLD_MIN define.
  float fxaaQualityEdgeThresholdMin;

  // This used to be the FXAA_CONSOLE__EDGE_SHARPNESS define.
  float fxaaConsoleEdgeSharpness;

  // This used to be the FXAA_CONSOLE__EDGE_THRESHOLD define.
  float fxaaConsoleEdgeThreshold;

  // This used to be the FXAA_CONSOLE__EDGE_THRESHOLD_MIN define.
  float fxaaConsoleEdgeThresholdMin;

  float2 _packing1;
    //    
    // Extra constants for 360 FXAA Console only.
    // float4 fxaaConsole360ConstDir;
};

cbuffer cbData : register(b10) {
  FXAAShaderConstants g_FXAA;
  // float2 gTexMult;
  // float2 gTexAdd;
}

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

// #define FxaaInt2 int2
// struct FxaaTex { SamplerState smpl; Texture2D tex; };
// #define FxaaTexTop(t, p) t.tex.SampleLevel(t.smpl, saturate(p) * gTexMult + gTexAdd, 0.0)
// #define FxaaTexOff(t, p, o, r) t.tex.SampleLevel(t.smpl, saturate(p) * gTexMult + gTexAdd, 0.0, o)
// #define FxaaTexAlpha4(t, p) t.tex.GatherAlpha(t.smpl, saturate(p) * gTexMult + gTexAdd)
// #define FxaaTexOffAlpha4(t, p, o) t.tex.GatherAlpha(t.smpl, saturate(p) * gTexMult + gTexAdd, o)
// #define FxaaTexGreen4(t, p) t.tex.GatherGreen(t.smpl, saturate(p) * gTexMult + gTexAdd)
// #define FxaaTexOffGreen4(t, p, o) t.tex.GatherGreen(t.smpl, saturate(p) * gTexMult + gTexAdd, o)

#include "FXAA3_11.hlsl"
#include "include_new/base/samplers_ps.fx"

float4 main(VS_Copy pin) : SV_TARGET {
  FxaaTex tex;
  tex.smpl = samLinearClamp;
  tex.tex = txDiffuse;
  return FxaaPixelShader(
    pin.Tex, // pos
    float4(0, 0, 0, 0), // fxaaConsolePosPos
    tex, // tex
    tex, // fxaaConsole360TexExpBiasNegOne
    tex, // fxaaConsole360TexExpBiasNegTwo
    g_FXAA.rcpFrame, // fxaaQualityRcpFrame: 1.0/screenWidthInPixels, 1.0/screenHeightInPixels
    g_FXAA.rcpFrameOpt, // fxaaConsoleRcpFrameOpt
    g_FXAA.rcpFrameOpt2, // fxaaConsoleRcpFrameOpt2
    g_FXAA.rcpFrameOpt2, // fxaaConsole360RcpFrameOpt2
    g_FXAA.fxaaQualitySubpix, // fxaaQualitySubpix
    g_FXAA.fxaaQualityEdgeThreshold, // fxaaQualityEdgeThreshold
    g_FXAA.fxaaQualityEdgeThresholdMin, // fxaaQualityEdgeThresholdMin
    g_FXAA.fxaaConsoleEdgeSharpness, // fxaaConsoleEdgeSharpness
    g_FXAA.fxaaConsoleEdgeThreshold, // fxaaConsoleEdgeThreshold
    g_FXAA.fxaaConsoleEdgeThresholdMin, // fxaaConsoleEdgeThresholdMin
    float4(0, 0, 0, 0) // fxaaConsole360ConstDir
    );
}