#define FXAA_PC 1
#define FXAA_HLSL_5 1
#include "FXAA3_11.hlsl"
#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float2 rcpFrame;
  float2 _packing0;
  float4 rcpFrameOpt;
}

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  FxaaTex tex;
  tex.smpl = samLinearSimple;
  tex.tex = txDiffuse;
  return FxaaPixelShader(pin.Tex, float4(0, 0, 0, 0), tex, tex, tex, 
    rcpFrame, rcpFrameOpt, float4(0, 0, 0, 0), float4(0, 0, 0, 0),
    1, 0.166, 0.0833, 0, 0, 0, float4(0, 0, 0, 0));
}