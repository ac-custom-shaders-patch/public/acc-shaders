#include "FXAA.hlsl"
#include "include_new/base/samplers_ps.fx"

cbuffer cbData : register(b10) {
  float4 gScreenSize;
}

Texture2D txDiffuse : register(t0);

struct VS_Copy {
  float4 PosH : SV_POSITION;
  noperspective float2 Tex : TEXCOORD0;
};

float4 main(VS_Copy pin) : SV_TARGET {
  FxaaTex tex = { samLinearClamp, txDiffuse };
  float3 aaImage = FxaaPixelShader(pin.Tex, tex, gScreenSize.xy);
  return float4(aaImage, 1.0);
}