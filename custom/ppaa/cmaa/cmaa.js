Object.entries({
  edges: 'EdgesColor2x2CS',
  candidates: 'ProcessCandidatesCS',
  color: 'DeferredColorApply2x2CS',
  args: 'ComputeDispatchArgsCS',
  debug: 'DebugDrawEdgesCS'
}).map(([stepKey, stepValue]) => Object.entries({ 
  default: 0, 
  sharp: 1 
}).map(([sharpnessKey, sharpnessValue]) => Object.entries({ 
  medium: 1, 
  high: 2, 
  ultra: 3 
}).map(([qualityKey, qualityValue]) => Object.entries({ 
  ldr: 0, 
  hdr: 1 
}).map(([hdrKey, hdrValue]) => ({
  source: `CMAA2.hlsl`,
  target: FX.Target.CS,
  entry: stepValue,
  saveAs: `accCmaa_${stepKey}_${sharpnessKey}_${qualityKey}_${hdrKey}.fxo`,
  defines: {
    'CMAA2_STATIC_QUALITY_PRESET': qualityValue,
    'CMAA2_EXTRA_SHARPNESS': sharpnessValue,
    'CMAA2_SUPPORT_HDR_COLOR_RANGE': hdrValue,
    'CMAA2_UAV_STORE_TYPED': 1,
    'CMAA2_UAV_STORE_TYPED_UNORM_FLOAT': 0,
    'CMAA2_UAV_STORE_CONVERT_TO_SRGB': 0,
  }
}))))).flat(Infinity)