add('MLAA11.hlsl', { target: FX.Target.PS, entry: 'MLAA_SeperatingLines_PS', saveAs: 'accMlaa_separateedges_luma.fxo' });
add('MLAA11.hlsl', { target: FX.Target.PS, entry: 'MLAA_SeperatingLines_PS', saveAs: 'accMlaa_separateedges_green.fxo', defines: { 'MLAA_GREEN_AS_LUMA': 1 } });
add('MLAA11.hlsl', { target: FX.Target.PS, entry: 'MLAA_ComputeLineLength_PS', saveAs: 'accMlaa_computelinelength.fxo' });
add('MLAA11.hlsl', { target: FX.Target.PS, entry: 'MLAA_BlendColor_PS', saveAs: 'accMlaa_blendcolor_final_luma.fxo' });
add('MLAA11.hlsl', { target: FX.Target.PS, entry: 'MLAA_BlendColor_PS', saveAs: 'accMlaa_blendcolor_debug_luma.fxo', defines: { 'SHOW_EDGES': 1, 'MLAA_GREEN_AS_LUMA': 1 } });
add('MLAA11.hlsl', { target: FX.Target.PS, entry: 'MLAA_BlendColor_PS', saveAs: 'accMlaa_blendcolor_final_green.fxo', defines: { 'MLAA_GREEN_AS_LUMA': 1 } });
add('MLAA11.hlsl', { target: FX.Target.PS, entry: 'MLAA_BlendColor_PS', saveAs: 'accMlaa_blendcolor_debug_green.fxo', defines: { 'SHOW_EDGES': 1, 'MLAA_GREEN_AS_LUMA': 1 } });
