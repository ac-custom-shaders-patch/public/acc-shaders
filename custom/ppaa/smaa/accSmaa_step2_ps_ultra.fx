#define SMAA_PRESET_ULTRA
#include "accSmaa_common.hlsl"

float4 main(VS_Smaa_step2 pin) : SV_TARGET {
  return SMAABlendingWeightCalculationPS(pin.Tex, pin.Pix, pin.Offset, txDiffuse, txAreaTexMap, txSearchTexMap, float4(0, 0, 0, 0));
}