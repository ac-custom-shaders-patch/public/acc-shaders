#include "include/samplers.hlsl"

cbuffer cbData : register(b10) {
  float4x4 gTransform;
}

Texture2D<float4> txNormals : register(t0);
ByteAddressBuffer inVertices : register(t1);
RWTexture2D<float2> rwOutput : register(u0);

[numthreads(256, 1, 1)]
void main(uint3 threadID : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint3 Gid : SV_GroupID) {
  float4 v = asfloat(inVertices.Load4(threadID.x * 16));
  float4 texCoord = mul(float4(v.xyz, 1), gTransform);
  float4 texValue = txNormals.SampleLevel(samPointClamp, texCoord.xy, 0);
  if (all(texCoord.xy >= 0 && texCoord.xy <= 1) && texValue.a) {
    rwOutput[uint2(GTid.x, Gid.x)] = normalize(texValue.xyz * 2 - 1).xz * 0.5 + 0.5;
  }
}

