// _use dxc: yes
// _override target: cs_6_2
// _compile argument: /enable-16bit-types

#define SAMPLE_EASU 0
#define SAMPLE_RCAS 1
#define SAMPLE_SLOW_FALLBACK 1
#define SAMPLE_BILINEAR 0
#define WIDTH 64
#define HEIGHT 1
#define DEPTH 1
#include "include/fsr_pass.hlsl"
