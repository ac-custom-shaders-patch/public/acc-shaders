cbuffer cb : register(b0) {
  uint4 const0;
  uint4 const1;
};

Texture2D InputTexture : register(t0);
RWTexture2D<float4> OutputTexture : register(u0);

#define A_GPU 1
#define A_HLSL 1
#define CAS_SAMPLE_SHARPEN_ONLY 0

#if CAS_SAMPLE_FP16
  #define A_HALF 1
  #define CAS_PACKED_ONLY 1
#endif

#include "include/ffx_a.h"

#if CAS_SAMPLE_FP16
  AH3 CasLoadH(ASW2 p) {
    return InputTexture.Load(ASU3(p, 0)).rgb;
  }
  void CasInputH(inout AH2 r, inout AH2 g, inout AH2 b) {}
#else
  AF3 CasLoad(ASU2 p) {
    return InputTexture.Load(int3(p, 0)).rgb;
  }
  void CasInput(inout AF1 r, inout AF1 g, inout AF1 b) {}
#endif

#include "include/ffx_cas.h"

[numthreads(64, 1, 1)]
void main(uint3 LocalThreadId : SV_GroupThreadID, uint3 WorkGroupId : SV_GroupID) {
  // Do remapping of local xy in workgroup for a more PS-like swizzle pattern.
  AU2 gxy = ARmp8x8(LocalThreadId.x) + AU2(WorkGroupId.x << 4u, WorkGroupId.y << 4u);

  bool sharpenOnly;
  #if CAS_SAMPLE_SHARPEN_ONLY
    sharpenOnly = true;
  #else
    sharpenOnly = false;
  #endif

  // OutputTexture[ASU2(gxy)] = InputTexture[ASU2(gxy)];
  // OutputTexture[ASU2(gxy) + ASU2(8, 0)] = InputTexture[ASU2(gxy) + ASU2(8, 0)];
  // OutputTexture[ASU2(gxy) + ASU2(0, 8)] = InputTexture[ASU2(gxy) + ASU2(0, 8)];
  // OutputTexture[ASU2(gxy) + ASU2(8, 8)] = InputTexture[ASU2(gxy) + ASU2(8, 8)];
  // return;

  // uint4 const0;
  // uint4 const1;
  // CasSetup(const0, const1, 1, 1920, 1080, 1920, 1080);

  #if CAS_SAMPLE_FP16  
    AH4 c0, c1;
    AH2 cR, cG, cB;
    
    CasFilterH(cR, cG, cB, gxy, const0, const1, sharpenOnly);
    CasDepack(c0, c1, cR, cG, cB);
    OutputTexture[ASU2(gxy)] = AF4(c0);
    OutputTexture[ASU2(gxy) + ASU2(8, 0)] = AF4(c1);
    gxy.y += 8u;
    
    CasFilterH(cR, cG, cB, gxy, const0, const1, sharpenOnly);
    CasDepack(c0, c1, cR, cG, cB);
    OutputTexture[ASU2(gxy)] = AF4(c0);
    OutputTexture[ASU2(gxy) + ASU2(8, 0)] = AF4(c1);  
  #else    
    AF3 c;
    
    CasFilter(c.r, c.g, c.b, gxy, const0, const1, sharpenOnly);
    OutputTexture[ASU2(gxy)] = AF4(c, 1);
    gxy.x += 8u;
    
    CasFilter(c.r, c.g, c.b, gxy, const0, const1, sharpenOnly);
    OutputTexture[ASU2(gxy)] = AF4(c, 1);
    gxy.y += 8u;
    
    CasFilter(c.r, c.g, c.b, gxy, const0, const1, sharpenOnly);
    OutputTexture[ASU2(gxy)] = AF4(c, 1);
    gxy.x -= 8u;
    
    CasFilter(c.r, c.g, c.b, gxy, const0, const1, sharpenOnly);
    OutputTexture[ASU2(gxy)] = AF4(c, 1);
  #endif
}