#define TREE_NO_FLG_CB
#define IGNORE_VERTICAL_OFFSET

#include "flgTree_bb.hlsl"

struct PS_IN_TreeBillboard {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD2;
};

float main(PS_IN_TreeBillboard pin) : SV_TARGET {
  if (dot2(pin.Tex) > 0.5) discard;
  return 0;
}
