#define CUSTOM_STRUCT_FIELDS\
  float4 TmbExtra : EXTRA0;\
  float2 ScreenPos : EXTRA1;

#include "flgTree_bb.hlsl"

struct BAKE_TYPE {
  float4 color : SV_TARGET0;
  float4 normal : SV_TARGET1;
  float leaf : SV_TARGET2;
};

cbuffer cbData : register(b10) {
  float gAngle;
  float gPKAmount;
  float gHWRatio;
};

BAKE_TYPE main(PS_IN_Nm pin, bool ff : SV_IsFrontFace) {
  if (!ff){
    pin.NormalW = -pin.NormalW;
    pin.BitangentW = -pin.BitangentW;
  }

  READ_VECTORS_NM

  float4 txDiffuseValue = txDiffuse.SampleBias(samAnisotropic16, pin.Tex, -1);
  float txAlphaValue = txDiffuse.SampleBias(samAnisotropic16, pin.Tex, -5).w;
  float4 txNormalValue = txNormal.SampleBias(samAnisotropic16, pin.Tex, -5);
  float txLeafValue = getLeafValue(txDiffuseValue);
  float txAOValue = gUseAOChannel ? txNormalValue.w : 1;
  txDiffuseValue.w = txAlphaValue;

  float relDist = dot2(pin.ScreenPos);
  clip(txDiffuseValue.w - lerp(0.1, 0.7, relDist));
  
  if (gTxNormalsWAsLeavesMap && !gBarkMesh) {
    txLeafValue = txNormalValue.w;
    txAOValue = 1;
  }
  txAOValue *= pow(saturate(pin.Ao), 0.4);
  
  float3 normalL = txNormalValue.xyz * 2 - 1;
  normalL.z = sqrt(saturate(1 - dot2(normalL.xy)));
  normalW = normalize(
    -pin.TangentW * normalL.x 
    + pin.BitangentW * normalL.y 
    + pin.NormalW * normalL.z * lerp(1, gNormalsAlign, txLeafValue) 
    + pin.TmbExtra.xyz * gNormalsSphere);

  BAKE_TYPE ret;
  ret.color = txDiffuseValue;
  ret.normal = float4(normalW.xyz * 0.5 + 0.5, txAOValue);
  ret.leaf = txLeafValue;
  return ret;
}
