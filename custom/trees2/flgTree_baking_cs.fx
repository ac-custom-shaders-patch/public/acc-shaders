#include "include/samplers.hlsl"

SamplerComparisonState samShadow2 : register(s14) {
  Filter = COMPARISON_MIN_MAG_MIP_LINEAR;
  AddressU = BORDER;
  AddressV = BORDER;
  AddressW = BORDER;
  BorderColor = 1;
  ComparisonFunc = GREATER;
};

cbuffer cbData : register(b10) {
  float4x4 gTransform;
  float4x4 gTexTransform;
}

cbuffer cbPerMeshData : register(b9) {
  float3 gLightDir;
  float _gPad;
}

Texture2D<float> txShadow : register(t0);
ByteAddressBuffer inVertices : register(t1);
RWTexture2D<float> rwOutput : register(u0);

float sampleOcclusion(float3 pos){
  float4 posH = mul(float4(pos, 1), gTexTransform);
  return txShadow.SampleLevel(samLinearBorder1, posH.xy, 0) > posH.z ? 1 : 0;
}

[numthreads(256, 1, 1)]
void main(uint3 threadID : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint3 Gid : SV_GroupID) {
  float4 v = asfloat(inVertices.Load4(threadID.x * 16));
  rwOutput[uint2(GTid.x, Gid.x)] += sampleOcclusion(v.xyz);
  // rwOutput[uint2(GTid.x, Gid.x)] += 1;
}

