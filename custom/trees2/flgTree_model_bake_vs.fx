#define CUSTOM_STRUCT_FIELDS\
  float4 TmbExtra : EXTRA0;\
  float2 ScreenPos : EXTRA1;

#include "flgTree_bb.hlsl"

cbuffer cbData : register(b10) {
  float gAngle;
  float gPKAmount;
  float gHWRatio;
};

PS_IN_Nm main(VS_IN vin SPS_VS_ARG) {
  PS_IN_Nm vout = (PS_IN_Nm)0;

  float3 origPos = vin.PosL.xyz;
  if (gAngle == -1){
    vout.PosH = float4(vin.PosL.xzy, 1);
    vout.PosH.x = -vout.PosH.x;
    // vout.PosH.y = -vout.PosH.y;
    vout.PosC = float3(0, -1, 0);
  } else {
    vin.PosL.xz = rotate2d(vin.PosL.xz, -gAngle);
    vin.PosL.y = vin.PosL.y * 2 - 1;
    vout.PosH = float4(vin.PosL.xyz, 1);

    float pk = lerp(0.93, 1.07, vout.PosH.z * 0.5 + 0.5);
    pk = lerp(1, pk, gPKAmount);
    vout.PosH.x *= pk;
    vout.PosH.y *= lerp(1, pk, gHWRatio);

    float2 dir = -rotate2d(float2(0, 1), gAngle);
    vout.PosC = float3(dir.x, 0, dir.y);
  }
  
  vout.PosH.z = 1 - (vout.PosH.z * 0.5 + 0.5);
  vout.Tex = vin.Tex;
  vout.Fog = 1;

  vout.NormalW = normalize(vin.NormalL);
  vout.TangentW = normalize(vsLoadTangent(vin.TangentPacked));
  vout.BitangentW = normalize(cross(vout.TangentW, vout.NormalW));

    // origPos.xz = rotate2d(origPos.xz, gAngle);
  vout.TmbExtra.xyz = origPos;
  vout.TmbExtra.y = pow(vout.TmbExtra.y, 4);
  // vout.TmbExtra.xyz = 0;
  // vout.TmbExtra.xyz = float3(0, 1, 0);
  // vout.TmbExtra.w = saturate((1 - min(dot(-normalize(vout.PosC.xz), normalize(relPos.xz)), 1 - pow(1.1 * relPos.y / PhalfSize.y, 2))) * 1.4);
  vout.TmbExtra.w = 0;
  vout.ScreenPos = vout.PosH.xy;

  vout.Ao = vsLoadAo0(vin.TangentPacked);
  return vout;
} 
