#define TREE_BILLBOARD_CB
#include "flgTree_bb.hlsl"
// alias: flgTree_bb_sides_mainobj_a2c_vs
// alias: flgTree_bb_sides_leaves_mainobj_vs
// alias: flgTree_bb_sides_leaves_mainobj_a2c_vs

struct PS_IN_TreeBillboard {
  float4 PosH : SV_POSITION;
  float3 DirW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  nointerpolation uint Color : TEXCOORD24;
  nointerpolation float Ratio : TEXCOORD29;
  nointerpolation float ShadowMult : TEXCOORD27;
  nointerpolation float UVSYMult : TEXCOORD30;
  centroid float Angle : TEXCOORD20;
  centroid float2 Tex : TEXCOORD2;
  centroid float WindIntensity : TEXCOORD25;
  float WindBend : TEXCOORD26;
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  float3 SideW : TEXCOORD6;
  float3 UpW : TEXCOORD7;
  float Fog : TEXCOORD8;
};

PS_IN_TreeBillboard main(uint fakeIndex : SV_VERTEXID) {
	uint vertexID = fakeIndex % 6;
	uint instanceID = fakeIndex / 6;

	float2 B = BILLBOARD[vertexID];
	Tree P = particleBuffer[instanceID].unpack();

  float3 baseDir = P.pos + extSceneOffset - ksCameraPosition.xyz;

	PS_IN_TreeBillboard vout;
  float2 wind;
  float4 posWPrev;
  float4 posW = getPosW(P, vertexID, baseDir, vout.DirW, vout.SideW, vout.UpW, vout.Tex, vout.Angle, posWPrev, wind);
	float4 posV = mul(posW, ksView);
	vout.PosH = mul(posV, ksProjection);
  GENERIC_PIECE_WORLD(posWPrev);

  vout.WindIntensity = cos(extWindWave * 1.773) * abs(dot(extWindVel, vout.SideW.xz)) * 0.1;
  vout.WindBend = dot(wind, vout.SideW.xz) / P.halfSize.x * 0.1;
  vout.Color = P.color;
  vout.Ratio = P.halfSize.y / P.halfSize.x;
  vout.ShadowMult = surfaceShadowMult(P);

  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.UVSYMult = getUVSYMult(P, vout.DirW, vout.PosC);
  vout.PosC -= vout.DirW * P.halfSize.x / 4;
  vout.Fog = calculateFog(posV);
  shadows(posW - float4(ksLightDirection.xyz, 0) * lerp(P.halfSize.x, P.halfSize.y, abs(ksLightDirection.y)), SHADOWS_COORDS);

  return vout;
}