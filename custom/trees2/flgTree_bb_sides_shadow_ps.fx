#define TREE_BILLBOARD_CB
#define TREE_BILLBOARD_SHADOW

#include "flgTree_bb.hlsl"

float main(PS_IN_TreeShadowBillboard pin) : SV_TARGET {
  float4 dif, nm;
  float leaf;
  sampleTreeBillboardTex(pin.Angle, pin.Tex, float3(1, 1, 0), saturate(-pin.DirWY), pin.Ratio, pin.UVSYMult, 0, dif, nm, leaf);
  clip(dif.a - 0.2);
  return pin.PosH.z + pin.PosHZ2 * -getBayer(pin.PosH);
}
