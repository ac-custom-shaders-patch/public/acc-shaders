#define TREE_BILLBOARD_CB

#include "flgTree_bb.hlsl"

struct PS_IN_TreeBillboard {
  float4 PosH : SV_POSITION;
  float3 DirW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  nointerpolation uint Color : TEXCOORD24;
  nointerpolation float Ratio : TEXCOORD29;
  nointerpolation float ShadowMult : TEXCOORD27;
  nointerpolation float UVSYMult : TEXCOORD30;
  centroid float Angle : TEXCOORD20;
  centroid float2 Tex : TEXCOORD2;
  centroid float WindIntensity : TEXCOORD25;
  float WindBend : TEXCOORD26;
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  float3 SideW : TEXCOORD6;
  float3 UpW : TEXCOORD7;
  float Fog : TEXCOORD8;
};

RESULT_TYPE main(PS_IN_TreeBillboard pin PS_INPUT_EXTRA) {
  float3 toCamera = normalize(pin.PosC);
  pin.Tex.x += sqrt(abs(pin.WindBend)) * sign(pin.WindBend);

  float shadow = getShadow(pin.PosC, pin.PosH, float3(0, 1, 0), SHADOWS_COORDS, AO_FALLBACK_SHADOW) * pin.ShadowMult;
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue, txNormalValue;
  float txLeafValue;
  sampleTreeBillboardTex(pin.Angle, pin.Tex, pin.PosC, saturate(-pin.DirW.y), pin.Ratio, pin.UVSYMult, pin.WindIntensity, txDiffuseValue, txNormalValue, txLeafValue);
  applyTxDiffuseCorrection(txDiffuseValue, pin.Color);
  float txAOValue = txNormalValue.w;
  
  float3 normalL = txNormalValue.xyz * 2 - 1;
  float3 offset = lerp(lerp(-pin.DirW, pin.SideW * sign(pin.Tex.x * 2 - 1), abs(pin.Tex.x * 2 - 1)), pin.UpW, pow(1 - pin.Tex.y, 2) * (1 - pow(pin.Tex.x * 2 - 1, 2)));
  float3 normalW = normalize(pin.SideW * normalL.x - pin.UpW * normalL.y - pin.DirW * normalL.z + normalize(offset) * dot(txDiffuseValue.rgb, 0.5));

  float3 extraDirW = pin.SideW * (pin.Tex.x * 2 - 1) * 2 + pin.UpW * (1 - pin.Tex.y * 2);
  float geoSelfShadow = computeBillboardGeoShadow(normalW, toCamera, extraDirW, pin.DirW, pin.Tex, txAOValue);
  float shadowAOModifier = saturate(pow(txAOValue, 2) * 1.4);
  TREE_BB_SCATTERING_BASE_MULT_CALC(extraDirW);
  TREE_LIGHTING;

  A2C_ALPHA(withReflection.a);
  RETURN(withReflection, withReflection.a);
}
