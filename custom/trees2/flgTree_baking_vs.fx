#include "include_new/base/_include_vs.fx"
#include "include/common.hlsl"

cbuffer cbData : register(b10) {
  float4x4 gTransform;
}

struct VS_IN_collider {
  float3 PosL : POSITION;
  uint NormalL : NORMAL_ENCODED;
};

struct PS_IN {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD;
};

float4 project(float4 posL){
  float4 posH = mul(posL, gTransform);
  if (posH.z < 0) posH.z = 0;
  return posH;
} 

PS_IN main(VS_IN_collider vin) {
  PS_IN vout;
  vout.PosH = project(float4(vin.PosL, 1));
  vout.Tex = loadVector(asuint(vin.NormalL));
  return vout;
}

