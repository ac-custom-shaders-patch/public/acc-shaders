#define TREE_NO_FLG_CB
#define IGNORE_VERTICAL_OFFSET

#include "flgTree_bb.hlsl"

cbuffer cbData : register(b10) {
  float4x4 gViewProj;
}

struct PS_IN_TreeBillboard {
  float4 PosH : SV_POSITION;
  float2 Tex : TEXCOORD2;
};

PS_IN_TreeBillboard main(uint fakeIndex : SV_VERTEXID) {
	uint vertexID = fakeIndex % 6;
	uint instanceID = fakeIndex / 6;

	float2 B = BILLBOARD[vertexID];
	Tree P = particleBuffer[instanceID].unpack();
  P.pos += extSceneOffset;
  // P.pos.y += 20;
  // P.halfSize.x *= 1.4;

	PS_IN_TreeBillboard vout;
	float4 posW = float4(P.pos + float3(P.halfSize.x * B.x, 0, P.halfSize.x * B.y), 1);
	vout.PosH = mul(posW, gViewProj);
  vout.Tex = B;
  return vout;
}