#define TREE_BILLBOARD_CB
#include "flgTree_bb.hlsl"
// alias: flgTree_bb_array_mainobj_a2c_vs
// alias: flgTree_bb_array_leaves_mainobj_vs
// alias: flgTree_bb_array_leaves_mainobj_a2c_vs

struct PS_IN_TreeBillboard {
  float4 PosH : SV_POSITION;
  float3 DirW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  nointerpolation float Alpha : TEXCOORD22;
  nointerpolation float BaseAngle : TEXCOORD23;
  nointerpolation float ShadowMult : TEXCOORD27;
  nointerpolation uint Ratio_NmShadowOffset : TEXCOORD29;
  nointerpolation uint Color : TEXCOORD24;
  nointerpolation uint2 Sh : TEXCOORD28;
  nointerpolation float UVSYMult : TEXCOORD30;
  centroid float WindIntensity : TEXCOORD25;
  centroid float Angle : TEXCOORD20;
  centroid float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  float3 ExtraDirW : TEXCOORD6;
  float Fog : TEXCOORD8;
};

PS_IN_TreeBillboard main(uint fakeIndex : SV_VERTEXID) {
	uint vertexID = fakeIndex % 6;
	uint instanceID = fakeIndex / 6;

  float2 B = BILLBOARD[vertexID];
	Tree P = particleBuffer[instanceID].unpack();

  float3 baseDir = P.pos + extSceneOffset - ksCameraPosition.xyz;
  #if defined(MODE_MAIN_FX) || defined(MODE_GBUFFER)
    float alpha = lerpInvSat(length(baseDir) * getLODMult(P), gBillboardLODDistance.x, gBillboardLODDistance.y) 
      * lerpInvSat(length(baseDir), gLODOutDistance, gLODOutDistance * 0.95); // fading at the furthest distance
    if (alpha == 0) DISCARD_VERTEX(PS_IN_TreeBillboard);
  #else
    float alpha = 1;
  #endif

	PS_IN_TreeBillboard vout;
  float3 sideW, upW;
  float4 posWPrev;
  float4 posW = getPosW(P, vertexID, baseDir, vout.DirW, sideW, upW, vout.Tex, vout.Angle, posWPrev);
	float4 posV = mul(posW, ksView);
	vout.PosH = mul(posV, ksProjection);
  GENERIC_PIECE_WORLD(posWPrev);

  vout.ExtraDirW = sideW * B.x + upW * B.y;
  vout.Alpha = sqrt(alpha);
  // vout.Alpha = 1;
  vout.BaseAngle = P.angle;
  vout.WindIntensity = cos(extWindWave * 1.773) * abs(dot(extWindVel, sideW.xz)) * 0.3;
  vout.Color = P.color;
  vout.Ratio_NmShadowOffset = packVector(float2(P.halfSize.y / P.halfSize.x, P.halfSize.x));
  vout.ShadowMult = surfaceShadowMult(P);

  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.UVSYMult = getUVSYMult(P, vout.DirW, vout.PosC);
  vout.PosC -= vout.DirW * P.halfSize.x / 4;
  vout.Fog = calculateFog(posV);
  vout.Sh = P.shPacked; 

  // posW -= float4(ksLightDirection.xyz, 0) * lerp(P.halfSize.x, P.halfSize.y, abs(ksLightDirection.y));
  posW.y += P.halfSize.y * saturate(-vout.DirW.y);
  // posW += float4(ksLightDirection.xyz, 0) * 10;
  shadows(posW, SHADOWS_COORDS);

  float4 posV1 = mul(float4(P.pos + extSceneOffset + float3(0, 4, 0), 1), ksView);
  posV1.xyz *= 1 + 0.01 * B.x;
  float4 posH1 = mul(posV1, ksProjection);
	vout.PosH.z = posH1.z * (vout.PosH.w / posH1.w);

  return vout;
}