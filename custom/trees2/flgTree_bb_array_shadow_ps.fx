#define TREE_BILLBOARD_CB
#define TREE_ARRAY_MAPPING
#define TREE_BILLBOARD_SHADOW

#include "flgTree_bb.hlsl"

float main(PS_IN_TreeShadowBillboard pin) : SV_Target {
  float4 dif, nm;
  float leaves;
  sampleTreeBillboardTex(pin.Angle, pin.Tex, float3(1, 1, 0), saturate(-pin.DirWY), pin.Ratio, pin.UVSYMult, 0, dif, nm, leaves);
  clip(dif.a - 0.2);
  
  return pin.PosH.z + pin.PosHZ2 * -getBayer(pin.PosH);
}


// float main(PS_IN_TreeShadowBillboard pin) : SV_Target {
//   float4 dif, nm;
//   float leaves;
//   sampleTreeBillboardTex(pin.Angle, pin.Tex, float3(1, 1, 0), saturate(-pin.DirWY), pin.Ratio, pin.UVSYMult, 0, dif, nm, leaves);
//   clip(dif.a - 0.1);
//   return 1;
// }
