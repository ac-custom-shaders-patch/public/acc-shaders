#define TREE_BILLBOARD_CB
#define TREE_ARRAY_MAPPING
#define TREE_BILLBOARD_SHADOW
#define TREE_DYNAMIC_SHADOW

#include "flgTree_bb.hlsl"

float main(PS_IN_TreeShadowBillboard pin) : SV_TARGET {
  float4 dif, nm;
  float leaf;
  sampleTreeBillboardTex(pin.Angle, pin.Tex, float3(1, 1, 0), saturate(-pin.DirWY), pin.Ratio, pin.UVSYMult, 0, dif, nm, leaf);
  clip(min(dif.a - 0.1, length(pin.PosC) - gLightClip));
  return length(pin.PosC) * gLightRangeInv;
}
