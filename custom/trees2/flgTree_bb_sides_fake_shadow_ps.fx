#define TREE_BILLBOARD_CB

#include "flgTree_bb.hlsl"
#include "include/bayer.hlsl"

struct PS_IN_TreeBillboard {
  centroid noperspective float4 PosH : SV_POSITION;
  centroid noperspective float2 DepthReal : TEXCOORD14;
  float2 Tex : TEXCOORD2;
  float Opacity : TEXCOORD13;
  float BayerShift : TEXCOORD15;
  float3 PosC : TEXCOORD4;
  SHADOWS_COORDS_ITEMS
};

float4 main(PS_IN_TreeBillboard pin, out float outDepth : SV_DepthGreaterEqual) : SV_TARGET {
  float value = pow(saturate(1 - dot2(pin.Tex)), 1);
  // value = 1.001;

  #ifndef SIMPLE_MODE
    float shadow = getShadow(pin.PosC, pin.PosH, float3(0, 1, 0), SHADOWS_COORDS, 1);
    float ambientValue = max(dot(ksAmbientColor_sky0, 1), 2);
    float lightValue = max(dot(ksLightColor, 1), 0);
    value *= 1 - shadow * saturate(lightValue / (lightValue + ambientValue));
  #endif

  float gOpacity = GAMMA_OR(0.71, 0.4);
  float result = gOpacity * pin.Opacity * value;
  // result = 1;
  // result = 0.5;
  // result = 0;

  // float depth = 

  outDepth = lerp(pin.PosH.z, pin.DepthReal.x / pin.DepthReal.y, 
    -0.25 * getBayer(pin.PosH + int4((int)pin.BayerShift & 3, (int)pin.BayerShift >> 2, 0, 0)));
  // outDepth = pin.PosH.z + pin.DepthReal;
  // outDepth = pin.PosH.z;
  // outDepth = pin.PosH.z + 0.0001;

  #ifdef MODE_GBUFFER
    PS_OUT ret = (PS_OUT)1;
    ret.baseReflection = 1 - result;
    return ret;
  #else
    return float4(0, 0, 0, result);
  #endif
}
