#define TREE_BILLBOARD_CB

#include "flgTree_bb.hlsl"

PS_IN_TreeShadowBillboard main(uint fakeIndex : SV_VERTEXID) {
	uint vertexID = fakeIndex % 6;
	uint instanceID = fakeIndex / 6;
	Tree P = particleBuffer[instanceID].unpack();
  #ifdef TREE_DYNAMIC_SHADOW
    float3 lightDirBase = P.pos + extSceneOffset - gLightPos;
  #else
    float3 lightDirBase = ksLightDirection.xyz;
    P.halfSize *= lerpInvSat(dot2(P.pos + extSceneOffset - ksCameraPosition), 2000 * 2000, 1200 * 1200);
  #endif

	PS_IN_TreeShadowBillboard vout;
  float3 dirW, sideW, upW;
  float4 posWPrev;
  float4 posW = getPosW(P, vertexID, lightDirBase, dirW, sideW, upW, vout.Tex, vout.Angle, posWPrev);
  #ifdef TREE_DYNAMIC_SHADOW
    vout.PosH = mul(posW, gTransform);
    vout.PosC = (posW.xyz - gLightPos);
  #else
    // float4 posV = mul(posW, ksView);
    // float4 posH = mul(posV, ksProjection);
    float4 posH = mul(posW, ksMVPInverse);
    if (posH.z < 0) posH.z = 0;
    vout.PosH = posH;
    vout.PosHZ2 = mul(mul(posW + float4(dirW * P.halfSize.x * 0.2, 0), ksView), ksProjection).z - posH.z;
    vout.PosH.z += vout.PosHZ2 * 0.7;
  #endif
  vout.DirWY = dirW.y;
  vout.Ratio = P.halfSize.y / P.halfSize.x;
  vout.UVSYMult = getUVSYMult(P, dirW, posW.xyz - ksCameraPosition.xyz, true);
  return vout;
}