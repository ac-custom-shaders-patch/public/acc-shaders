#define TREE_BILLBOARD_CB
#define IGNORE_VERTICAL_OFFSET

#include "flgTree_bb.hlsl"

struct PS_IN_TreeBillboard {
  centroid noperspective float4 PosH : SV_POSITION;
  centroid noperspective float2 DepthReal : TEXCOORD14;
  float2 Tex : TEXCOORD2;
  float Opacity : TEXCOORD13;
  float BayerShift : TEXCOORD15;
  float3 PosC : TEXCOORD4;
  SHADOWS_COORDS_ITEMS
};

PS_IN_TreeBillboard main(uint fakeIndex : SV_VERTEXID) {
	uint vertexID = fakeIndex % 6;
	uint instanceID = fakeIndex / 6;

	float2 B = BILLBOARD[vertexID];
	Tree P = particleBuffer[instanceID].unpack();

  float3 side = normalize(cross(P.surfaceNormal, float3(0, 0, 1)));
  float3 up = normalize(cross(P.surfaceNormal, side));

  P.pos += extSceneOffset + P.surfaceNormal * 0.03;
  P.pos.y -= P.halfSize.y;
  P.halfSize.x *= gFakeShadowSize * saturate(1 - pow(lerp(gVerticalOffset.y, gVerticalOffset.x, P.surfaceNormal.y) / (P.halfSize.y * 2), 2));

  float3 offset = side * (B.x * P.halfSize.x) + up * (B.y * P.halfSize.x);
  float3 coverOffset = P.surfaceNormal * 0.02;
  coverOffset.y += 0.2 * (1 + dot(normalize(P.pos - ksCameraPosition.xyz), normalize(offset)));

	PS_IN_TreeBillboard vout;  
	float4 posW = float4(P.pos + offset + coverOffset, 1);
	float4 posV = mul(posW, ksView);
  // posV.z -= posV.z * min(0.05, 1 / length(posV.z));
  // posV.xyz -= posV.xyz * min(0.05, 1 / length(posV.xyz));
	vout.PosH = mul(posV, ksProjection);
  // vout.PosH.z -= 0.01;
  // vout.PosH.z -= 0.00002 * vout.PosH.w;
  vout.Tex = B;
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Opacity = P.fakeShadow * gFakeShadowOpacity; // TODO: pow(FOG_FAKE_SHADOW_MULT, 2)?
  vout.BayerShift = instanceID % 16;
  if (vout.Opacity < 0.05){
    DISCARD_VERTEX(PS_IN_TreeBillboard);
  }

  // Done after vertex discard so no shadow would break
  vout.Opacity *= pow(saturate(1 - FOG_FAKE_SHADOW_VS_MULT(posV, vout.PosC)), GAMMA_OR(32, 4));

  {
    float4 pos1W = float4(P.pos + offset, 1);
    float4 pos1V = mul(pos1W, ksView);
    float4 pos1H = mul(pos1V, ksProjection);
    vout.DepthReal = pos1H.zw;
  }

  shadows(posW, SHADOWS_COORDS);
  return vout;
}