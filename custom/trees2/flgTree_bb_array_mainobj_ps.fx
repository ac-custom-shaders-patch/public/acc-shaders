#define TREE_BILLBOARD_CB
// #define TREE_SAMPLE_0_LOD
// #define TREE_SAMPLE_TURN_NORMALS
#define TREE_ARRAY_MAPPING

#include "flgTree_bb.hlsl"

struct PS_IN_TreeBillboard {
  float4 PosH : SV_POSITION;
  float3 DirW : TEXCOORD0;
  float3 PosC : TEXCOORD1;
  nointerpolation float Alpha : TEXCOORD22;
  nointerpolation float BaseAngle : TEXCOORD23;
  nointerpolation float ShadowMult : TEXCOORD27;
  nointerpolation uint Ratio_NmShadowOffset : TEXCOORD29;
  nointerpolation uint Color : TEXCOORD24;
  nointerpolation uint2 Sh : TEXCOORD28;
  nointerpolation float UVSYMult : TEXCOORD30;
  centroid float WindIntensity : TEXCOORD25;
  centroid float Angle : TEXCOORD20;
  centroid float2 Tex : TEXCOORD2;
  MOTION_BUFFER
  SHADOWS_COORDS_ITEMS
  float3 ExtraDirW : TEXCOORD6;
  float Fog : TEXCOORD8;
};

RESULT_TYPE main(PS_IN_TreeBillboard pin PS_INPUT_EXTRA) {
  float3 toCamera = normalize(pin.PosC);

  float4 txDiffuseValue, txNormalValue;
  float txLeafValue;
  sampleTreeBillboardTex(pin.Angle, pin.Tex, pin.PosC, saturate(-pin.DirW.y), loadVector(pin.Ratio_NmShadowOffset).x, pin.UVSYMult, pin.WindIntensity, 
    txDiffuseValue, txNormalValue, txLeafValue);  
  applyTxDiffuseCorrection(txDiffuseValue, pin.Color);
  float txAOValue = txNormalValue.w;

  // txDiffuseValue.rgb = float3(frac(pin.BaseAngle * 888.885), frac(pin.BaseAngle * 571.424), frac(pin.BaseAngle * 594.841));
  
  float3 normalW = normalize(txNormalValue.xyz * 2 - 1);
  normalW.xz = rotate2d(normalW.xz, -pin.BaseAngle);

  #if defined(MODE_MAIN_FX) 
    pin.ShadowTex0.xyz += mul(normalW * loadVector(pin.Ratio_NmShadowOffset).y, (float3x3)ksShadowMatrix0);
  #endif
  float shadow = getShadow(pin.PosC, pin.PosH, float3(0, 1, 0), SHADOWS_COORDS, AO_FALLBACK_SHADOW) * pin.ShadowMult;
  APPLY_EXTRA_SHADOW

  float geoSelfShadow = computeBillboardGeoShadow(normalW, toCamera, pin.ExtraDirW, pin.DirW, pin.Tex, txAOValue);
  txAOValue *= extVaoMode == 3 ? 1 : shUnproject(loadVector(pin.Sh), pin.ExtraDirW);
  float shadowAOModifier = lerpInvSat(pow(txAOValue, 2), 0, 0.15);
  TREE_BB_SCATTERING_BASE_MULT_CALC(pin.ExtraDirW);
  TREE_LIGHTING;

  #if defined(MODE_MAIN_FX) || defined(MODE_GBUFFER)
    TREE_FADING(gBillboardLODIndex % 2, pin.Alpha);
  #endif

  // txDiffuseValue.a = 1;
  A2C_ALPHA(withReflection.a);

  // clip(-1);

  // withReflection.rgb = float3(txAOValue, 1 - txAOValue, 0);
  // withReflection.rgb = shUnproject(loadVector(pin.Sh), pin.ExtraDirW);
  // withReflection.rgb = float3(withReflection.r, 1 -  withReflection.r, 0);
  // withReflection.rgb = float3(geoSelfShadow, 1 - geoSelfShadow, 0);
  // withReflection.rgb = float3(gSeasonWinter, 1 - gSeasonWinter, 0);
  // withReflection.rgb = float3(shadow, 1 - shadow, 0);
  // withReflection.rgb = pin.ExtraDirW;
  // withReflection.rgb = float3(txLeafValue * 3, 3, 0);

  // if (!dot(withReflection, 1) >= 0) discard;

  RETURN(withReflection, withReflection.a);
}
