#define FIX_BASIC_A2C

#include "flgTree_bb.hlsl"

RESULT_TYPE main(PS_IN_TreeModel pin, bool ff : SV_IsFrontFace PS_INPUT_EXTRA) {
  if (!ff){
    pin.NormalW = -pin.NormalW;
    #ifndef NO_NORMALMAPS
      pin.BitangentW = -pin.BitangentW;
    #endif
  }

  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, pin.PosH, float3(0, 1, 0), SHADOWS_COORDS, AO_FALLBACK_SHADOW) * pin.ShadowMult;
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samAnisotropic, pin.Tex);
  txDiffuseValue.w *= 1.6;
  float txLeafValue = getLeafValue(txDiffuseValue);
  applyTxDiffuseCorrection(txDiffuseValue, pin.Color);

  #ifndef NO_NORMALMAPS
    float4 txNormalValue = txNormal.Sample(samAnisotropic, pin.Tex);
    float txAOValue = gUseAOChannel ? txNormalValue.w : 1;
    float3 normalL = txNormalValue.xyz * 2 - 1;
    normalL.z = sqrt(saturate(1 - dot2(normalL.xy)));
    normalW = normalize(-pin.TangentW * normalL.x + pin.BitangentW * normalL.y + pin.NormalW * normalL.z * lerp(1, gNormalsAlign, txLeafValue) 
      + pin.Extra.xyz * gNormalsSphere);
  #else
    float4 txNormalValue = 1;
    float txAOValue = 1;
  #endif
  
  if (gTxNormalsWAsLeavesMap && !gBarkMesh) {
    txLeafValue = txNormalValue.w;
    txAOValue = 1;
  }  

  float detailAO = saturate(1 - dot2(pin.PosC) / pow(gBillboardLODDistance.x, 2));
  shadow *= lerp(1, saturate(abs(dot(-ksLightDirection.xyz, normalW)) + 2 * pow(txAOValue, 3) - 1), (1 - txLeafValue) * detailAO);
  txAOValue = lerp(txAOValue, 1, detailAO * (1 - abs(dot(normalize(pin.NormalW), toCamera))));
  txAOValue = txAOValue * pow(saturate(pin.Ao), 0.4);
  
  float scatteringBaseMult = pin.Extra.w;
  float shadowAOModifier = lerp(0.5 - detailAO * 0.5, 1, lerpInvSat(pow(txAOValue, 2), 0, 0.15));
  float geoSelfShadow = saturate(dot(pin.Extra.xyz, -ksLightDirection.xyz) * 2 + 1);
  TREE_LIGHTING;

  TREE_FADING(pin.Alpha < 0, saturate(abs(pin.Alpha) * 1.02));

  A2C_ALPHA(withReflection.a);
  // withReflection.a = 1;

  // withReflection.rgb = txNormalValue.rgb;
  // withReflection.rgb = pin.Ao;
  // withReflection.rgb = float3(pin.Ao, 1 - pin.Ao, 0);
  // withReflection.rgb = float3(geoSelfShadow, 1 - geoSelfShadow, 0);
  // withReflection.rgb = float3(detailAO, 1 - detailAO, 0);
  // withReflection.rgb = float3(detailAO, 1 - detailAO, 0);
  // withReflection.rgb = float3(shadow, 1 - shadow, 0);
  // withReflection.rgb = pin.Extra.xyz; 
  // withReflection.rgb = pin.NormalW + 1;
  // normalW = pin.NormalW;
  
  RETURN(withReflection, withReflection.a);
}
