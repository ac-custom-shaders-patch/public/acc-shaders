#include "flgTree_bb.hlsl"
// alias: flgTree_model_mainobj_a2c_vs

cbuffer cbData : register(b5) {
  uint gLODIndex;
  uint3 _gPad0;
  TreePacked gEntries[1000];
};

PS_IN_TreeModel main(VS_IN vin, uint instanceBaseID : SV_InstanceID SPS_VS_ARG) {
  PS_IN_TreeModel vout;

  Tree P = gEntries[instanceBaseID].unpack();
  vin.PosL.xz = rotate2d(vin.PosL.xz, -P.angle);
  vin.NormalL.xz = rotate2d(vin.NormalL.xz, -P.angle);
  float3 origPos = vin.PosL.xyz;
  float yNorm = saturate(origPos.y);

  float3 posGSpace = P.pos + extSceneOffset;
  vin.PosL.y -= saturate(yNorm * 8) * dot2(vin.PosL.xz) * 0.04 * saturate(extSceneWetness * 100) 
    * saturate(1 - dot2(posGSpace - ksCameraPosition.xyz) / pow(gBillboardLODDistance.x, 2));
  vin.PosL.y *= P.halfSize.y * 2;
  vin.PosL.xz *= P.halfSize.x;
  vin.PosL.xyz += posGSpace;
  vin.PosL.y -= P.halfSize.y;
  vin.PosL.xz += P.tilt * P.surfaceNormal.xz * P.halfSize.y * 2 * yNorm;
  
  float windIntensity = yNorm * max(0.5 * pow(yNorm, 2), abs(dot(origPos.zx * float2(1, -1), normalize(extWindVel))));
  float4 posW = float4(vin.PosL.xyz, 1);
	float4 posWPrev = posW;
  applyWindModel(posW, windIntensity, origPos, yNorm, P, false);
	float4 posV = mul(posW, ksView);
	vout.PosH = mul(posV, ksProjection);

  applyWindModel(posWPrev, windIntensity, origPos, yNorm, P, true);
  GENERIC_PIECE_WORLD(posWPrev);

  vout.NormalW = normalize(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  vout.Fog = calculateFog(posV);
  
  #ifndef NO_NORMALMAPS
    vout.TangentW = normalize(vsLoadTangent(vin.TangentPacked));
    vout.TangentW.xz = rotate2d(vout.TangentW.xz, -P.angle);
    vout.BitangentW = normalize(cross(vout.TangentW, vout.NormalW));
  #endif

  // RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);
  vout.Ao = vsLoadAo0(vin.TangentPacked); 

  vout.Extra.xyz = origPos;
  vout.Extra.y = pow(vout.Extra.y, 4);
  vout.Extra.w = saturate((1 - min(dot(-normalize(vout.PosC.xz), normalize(origPos.xz)), 1 - pow(1.1 * yNorm, 2))) * 1.4);

  vout.Ao *= extVaoMode == 3 ? 1 : pow(shUnproject(P.sh, yNorm * 2 - 1), 2);
  vout.Alpha = P.fakeShadow > 0.995 ? 1 : gLODIndex % 2 ? -P.fakeShadow : P.fakeShadow;
  vout.Color = P.color;
  vout.ShadowMult = surfaceShadowMult(P);

  // posW -= float4(ksLightDirection.xyz, 0) * lerp(P.halfSize.x, P.halfSize.y, abs(ksLightDirection.y));
  shadows(posW, SHADOWS_COORDS);

  #ifdef MODE_FLGTREE_OUTLINE
    vout.PosH.z *= 0.9;
  #endif

  SPS_RET(vout);
} 
