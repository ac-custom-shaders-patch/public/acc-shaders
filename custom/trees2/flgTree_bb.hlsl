#ifdef MODE_SURFACESAMPLE
  #undef MODE_SURFACESAMPLE
  #define MODE_MAIN_FX
  #define MODE_FLGTREE_OUTLINE
  #define DISALLOW_CUSTOM_A2C
#endif

#define GAMMA_EXPECTED_KS 0.3
#include "include/normal_encode.hlsl"
#include "include/bayer.hlsl"
#include "include/common.hlsl"

#define ksAlphaRef 0
#define STENCIL_VALUE 0.00499
#define GBUFF_MASKING_MODE 0
#define GBUFF_SKIP_REFLECTION
#define GBUFF_NORMAL_W_PIN_SRC normalW
#define RAINFX_STATIC_OBJECT
#define RAINFX_FOLIAGE
#define RAINFX_NO_RAINDROPS
#define NO_EXTAMBIENT

float4 shEvaluate(float3 dir) {
	float4 result;
	result.x = 1;
	result.y = -1.732 * dir.y;
	result.z = 1.732 * dir.z;
	result.w = -1.732 * dir.x;
	return result;
}

float shUnproject(float4 sh, float3 dir) {
	return max(0, lerp(sh.x, dot(sh, shEvaluate(normalize(dir))), saturate(length(dir) * 2)));
}

cbuffer cbTreeMeshData : register(b5) {
  float gBarkMesh_inner;
}

#ifndef TREE_DYNAMIC_SHADOW
  #define TREE_SHADOW_STRUCT_MODIFIER centroid noperspective
#else
  #define TREE_SHADOW_STRUCT_MODIFIER linear
#endif

#ifdef TREE_DYNAMIC_SHADOW
  cbuffer cbData : register(b10) {
    float4x4 gTransform;
    float3 gLightPos;
    float gLightRangeInv;
    float gLightClip;
    float3 gSceneOrigin;
  }
#endif

struct PS_IN_TreeShadowBillboard {
  TREE_SHADOW_STRUCT_MODIFIER float4 PosH : SV_POSITION;
  TREE_SHADOW_STRUCT_MODIFIER float DirWY : TEXCOORD0;
  #ifndef TREE_DYNAMIC_SHADOW
    TREE_SHADOW_STRUCT_MODIFIER float PosHZ2 : TEXCOORD1;
  #else
    TREE_SHADOW_STRUCT_MODIFIER float3 PosC : TEXCOORD1;
  #endif
  TREE_SHADOW_STRUCT_MODIFIER float Angle : TEXCOORD20;
  TREE_SHADOW_STRUCT_MODIFIER float2 Tex : TEXCOORD2;
  TREE_SHADOW_STRUCT_MODIFIER float Ratio : TEXCOORD29;
  TREE_SHADOW_STRUCT_MODIFIER float UVSYMult : TEXCOORD30;
};

#ifndef TREE_NO_FLG_CB
  cbuffer cbData : register(b4) {
    float4 gPivots[16];
    float4 gAbove;
    #define gBaseHalfSize (gPivots[0].zw)

    #ifdef TREE_BILLBOARD_CB
      #define gMatSpecular (gPivots[1].z * 0.7)
    #else
      #define gMatSpecular (gPivots[1].z)
    #endif

    #define gMatSubscattering (gPivots[1].w)
    #define gMatReflectivity (gPivots[2].z)
    #define gMatBrightness (gPivots[2].w)
    #define gSeasonAutumn (gPivots[3].z)
    #define gSeasonWinter (gPivots[3].w)
    #define gNormalsAlign (gPivots[4].z)
    #define gNormalsSphere (gPivots[4].w)
    #define gLeavesMult (gPivots[5].z)
    #define gLeavesOffset (gPivots[5].w)
    #define gTxNormalsWAsLeavesMap (gPivots[6].z)
    #define gUseBarkFilter (gPivots[6].w)
    #define gBarkMesh (gUseBarkFilter && gBarkMesh_inner)
    #define gUseAOChannel (gPivots[7].z)
    #define gFakeShadowSize (gPivots[8].z)
    #define gFakeShadowOpacity (gPivots[8].w)
    #define gLODPadding (gPivots[9].z)
    #define gVerticalOffset (gPivots[10].zw)

    float2 gAboveTrim;
    float gTrunkWidthInv;
    float gSidesCount;

    float gSideIndexMult;
    float gSideIndexAdd;
    float2 gSideSize;

    float2 gBillboardLODDistance;
    uint gBillboardLODIndex;
    float gLODOutDistance;
  }

  float getBaseLeafValue(float4 txDiffuseValue){
    return saturate((txDiffuseValue.g - txDiffuseValue.r) * gLeavesMult + gLeavesOffset);
  }
#endif

struct Tree {
  float3 pos;
  float2 halfSize;
  uint color;
  float fakeShadow;
  float3 surfaceNormal;
  float tilt;
  float angle;
  float4 sh;
  uint2 shPacked;
  uint angle_uint;
};

struct TreePacked {
  float3 pos;
  uint halfSize;
  uint color_fakeShadow;
  uint surfaceNormal_tilt_angle;
  uint2 sh;

  Tree unpack(){
    Tree r;
    r.pos = pos;
    r.halfSize = loadVector(halfSize);
    r.color = color_fakeShadow;
    r.fakeShadow = BYTE3(color_fakeShadow) / 255.;
    r.surfaceNormal = float3(BYTE0(surfaceNormal_tilt_angle), 0, BYTE1(surfaceNormal_tilt_angle)) / 129 - 1;
    r.surfaceNormal.y = sqrt(saturate(1 - dot2(r.surfaceNormal.xz)));
    r.surfaceNormal = normalize(r.surfaceNormal);
    r.tilt = BYTE2(surfaceNormal_tilt_angle) / 255.;
    r.angle_uint = BYTE3(surfaceNormal_tilt_angle);
    r.angle = r.angle_uint * (M_TAU / 255.);
    r.sh = loadVector(sh);
    r.shPacked = sh;

    #ifndef IGNORE_VERTICAL_OFFSET
      r.pos.y += lerp(gVerticalOffset.y, gVerticalOffset.x, r.surfaceNormal.y);
    #endif

    return r;
  }
};

#define INPUT_SEASION_AUTUMN gSeasonAutumn
#define INPUT_SEASION_WINTER gSeasonWinter

#ifdef MODE_CUSTOM
	#define MODE_MAIN_FX
#endif

#ifdef TARGET_VS

  #include "include_new/base/_include_vs.fx"
  #include "include_new/ext_functions/wind.fx"

  StructuredBuffer<TreePacked> particleBuffer : register(t0);

  static const float2 BILLBOARD[] = {
    float2(-1, -1),
    float2(1, -1),
    float2(-1, 1),
    float2(-1, 1),
    float2(1, -1),
    float2(1, 1),
  };

  float surfaceShadowMult(Tree P){
    return GAMMA_LINEAR_SIMPLE(saturate(dot(P.surfaceNormal, -ksLightDirection.xyz) * 8));
  }

  float2 applyWindBillboard(inout float4 posW, float2 B, float3 dir, Tree P, bool usePrev){    
    // float windPos = lerp(lerp(dir.y * 0.3, 1, saturate(B.y)), 1, lerpInvSat(-dir.y, 0.9, 1));
    float windPos = lerp(saturate(B.y), 1, lerpInvSat(-dir.y, 0.9, 1));
    float windIntensity = windPos * 0.3;
    float2 wind = usePrev ? windOffsetPrev(posW.xyz - extSceneOffset, windIntensity, 1) : windOffset(posW.xyz - extSceneOffset, windIntensity, 1);
    posW.xz += wind * P.halfSize.y;
    if (windPos > 0) {
      posW.y -= (windPos - sqrt(max(0, pow(windPos, 2) - dot2(wind)))) * 0.5 * P.halfSize.y;
    }
    return wind;
  }

  float2 applyWindModel(inout float4 posW, float windIntensity, float3 origPos, float yNorm, Tree P, bool usePrev){
    float2 wind = usePrev ? windOffsetPrev(P.pos + origPos.xyz, windIntensity, 1) : windOffset(P.pos + origPos.xyz, windIntensity, 1);
    posW.xz += wind * P.halfSize.y;
    posW.y -= (yNorm - sqrt(max(0, pow(yNorm, 2) - 0.5 * dot2(wind) * pow(yNorm, 2)))) * P.halfSize.y;
    return wind;
  }

  float getLODMult(Tree P){
    #ifndef TREE_NO_FLG_CB
			float fovMult = 1 - 0.75 * lerpInvSat(extCameraTangent, 5, 15);
      return (1 + (float)P.angle_uint * gLODPadding) * fovMult;
    #else
      return 1;
    #endif
    // return 0.9 - sin(P.pos.z * 80) * 0.1;
    // return 1 - frac((float)(P.color & 0xffffff) / (float)0xff) * 0.2;
    // return 1 - frac(P.pos.z * 1234.5678) * 0.2;
  }

  float4 getPosW(Tree P, uint vertexID, float3 baseDir, out float3 dir, out float3 side, out float3 up, out float2 tex, out float angle, out float4 posWPrev, out float2 wind){    
    P.pos += extSceneOffset;
    float2 B = BILLBOARD[vertexID];

    dir = baseDir;
    if (dir.y > 0) dir.y = 0;
    dir = normalize(dir);

    side = normalize(cross(dir, float3(0, 1, 0)));
    up = normalize(cross(side, dir));
    tex = B * float2(1, -1) * 0.5 + 0.5;
    angle = P.angle - atan2(dir.x, dir.z);

    float4 posW = float4(P.pos + side * (B.x * P.halfSize.x) + up * (B.y * P.halfSize.y), 1);
    posW.xz += P.tilt * P.surfaceNormal.xz * P.halfSize.y * 2 * (1 - tex.y);
  
    posWPrev = posW;
    #ifndef TREE_WITHOUT_WIND
      wind = applyWindBillboard(posW, B, dir, P, false);
      applyWindBillboard(posWPrev, B, dir, P, true);
    #else
      wind = 0;
    #endif
    return posW;
  }

  float4 getPosW(Tree P, uint vertexID, float3 baseDir, out float3 dir, out float3 side, out float3 up, out float2 tex, out float angle, out float4 posWPrev){    
    float2 wind;
    return getPosW(P, vertexID, baseDir, dir, side, up, tex, angle, posWPrev, wind);
  }

  float getUVSYMult(Tree P, float3 dirW, float3 posC, bool orthogonalProjection = false){
    float tilt = saturate(-dirW.y);
    return orthogonalProjection 
      ? 1 / sqrt(1 - pow(tilt, 2)) 
      : 1 / (sqrt(1 - pow(tilt, 2)) * length(posC) / (length(posC) + tilt * P.halfSize.y));
  }

#endif

#if defined(TARGET_PS) && !defined(TREE_NO_FLG_CB)

  #define NO_CARPAINT
  #define INPUT_DIFFUSE_K GAMMA_EXPECTED_KS
  #define INPUT_AMBIENT_K GAMMA_EXPECTED_KS
  #define INPUT_SPECULAR_K 0.05
  #define INPUT_SPECULAR_EXP 3
  #define INPUT_EMISSIVE3 0
  #define IS_TRACK_MATERIAL 1
  #define USE_FRESNEL_MAX_LEVEL_AS_FINAL_FRESNEL
  #define LIGHTINGFX_FUNC_PARAM_DECLARE , float backlitMult
  #define LIGHTINGFX_FUNC_PARAM_PASS , backlitMult
  #define CUSTOM_LFX_DIFFUSE_MULT
  float customDiffuseMultiplier(float3 normal, float3 lightDir, float concentration, float backlitMult){
    float v = dot(normal, lightDir);
    return saturate(max(pow(-v, 2) * backlitMult, lerp(1, v, concentration)));
  }
  #define getDiffuseMultiplier(normal, lightDir) customDiffuseMultiplier(normal, lightDir, 0.88, backlitMult)
  #define getDiffuseMultiplierConcentrated(normal, lightDir, concentration) customDiffuseMultiplier(normal, lightDir, concentration, backlitMult)

  float3 adjustFlgTextureColor(float3 color, float3 normal, float variation, float leaf){
    #ifdef NO_ADJUSTING_COLOR
      return color;
    #else
      [branch]
      if (INPUT_SEASION_WINTER + INPUT_SEASION_AUTUMN > 0){
        if (INPUT_SEASION_AUTUMN > 0){
          color = lerp(color, float3(
            saturate(color.r * 0.2 + color.g * 1.6), 
            saturate(color.g * (1.3 - variation * variation) + color.r * 0.2), 
            saturate(color.b - 0.4 * color.g)), INPUT_SEASION_AUTUMN * leaf);
        }

        color = lerp(color, saturate(luminance(color) * ((float3(1, 1.1, 1.2) * 1.6 + saturate(INPUT_SEASION_WINTER * 2 - 1) * 0.4) * 0.85)), saturate(INPUT_SEASION_WINTER * 2) * leaf);
      }
      return color;
    #endif
  }

  #ifdef ALLOW_RAINFX
    #define TREE_RAINFX_INIT(AO)\
      RainSurfaceInput RP_input;\
      RP_input.posH = pin.PosH;\
      RP_input.posC = pin.PosC;\
      RP_input.normalW = normalW;\
      RP_input.toCamera = toCamera;\
      RP_input.posL = ksInPositionWorld;\
      RP_input.normalL = toCamera;\
      RainParams RP = _rainFx_rainMap(RP_input, txDiffuseValue, saturate(AO * 1.4 + 0.4));\
      float3 rainNormal = RP.normal;\
      txDiffuseValue.rgb *= lerp(1, 0.6, RP.damp);
    #define TREE_RAINFX_WETNESS (RP.damp * 0.5 + _rainFx_sceneWetness() * 0.5)
  #else
    #define TREE_RAINFX_INIT(AO)
    #define TREE_RAINFX_WETNESS 0
  #endif

  #ifdef TREE_BILLBOARD_CB
    #define TREE_RAINFX_SPECULAR_VALUE 0.2
    #define TREE_RAINFX_SPECULAR_EXP 200
    #define TREE_BRIGHTNESS_ADJUSTMENT txDiffuseValue.rgb = saturate(txDiffuseValue.rgb * gMatBrightness);
  #else
    #define TREE_RAINFX_SPECULAR_VALUE 0.3
    #define TREE_RAINFX_SPECULAR_EXP 160
    #define TREE_BRIGHTNESS_ADJUSTMENT txDiffuseValue.rgb = saturate(txDiffuseValue.rgb * gMatBrightness);
  #endif

  #define TREE_LIGHTING\
    TREE_BRIGHTNESS_ADJUSTMENT\
    txDiffuseValue.rgb = adjustFlgTextureColor(txDiffuseValue.rgb, normalW, posBasedVariationW(ksInPositionWorld), txLeafValue);\
    TREE_RAINFX_INIT(txAOValue)\
    shadow = min(shadow, GAMMA_LINEAR(geoSelfShadow * shadowAOModifier));\
    LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.rgb, shadow, 1);\
    txAOValue = GAMMA_OR(pow(txAOValue, 2), txAOValue);\
    L.ambientAoMult *= min(txAOValue, _AO_SSAO); /*TODO:VAO+SSAO?*/\
    float wetnessSpec = TREE_RAINFX_WETNESS / (1 + dot2(pin.PosC) / 5000);\
    L.specularValue = (0.02 + gMatSpecular * txLeafValue + TREE_RAINFX_SPECULAR_VALUE * wetnessSpec) * GAMMA_OR(0.4, 1);\
    L.specularExp = 3 + 10 * txLeafValue + TREE_RAINFX_SPECULAR_EXP * wetnessSpec;\
    float3 lighting = L.calculate();\
    float scatteringMult = gMatSubscattering * scatteringBaseMult * txAOValue * txLeafValue;\
    lighting += ksLightColor.rgb * txDiffuseValue.rgb * shadow * scatteringMult * pow(saturate(dot(ksLightDirection.xyz, normalW)), 2);\
    lighting += ksLightColor.rgb * txDiffuseValue.rgb * shadow * saturate(scatteringMult * 2)\
      * pow(saturate(dot(ksLightDirection.xyz, reflect(-toCamera, normalW))), 2) * pow(saturate(dot(-toCamera, ksLightDirection.xyz)), 4);\
    lighting += txCube.SampleLevel(samLinearSimple, -normalW * float3(-1, 1, 1), 6).rgb * scatteringMult * txDiffuseValue.rgb * lerp(0, 0.25, saturate(-normalW.y));\
    float backlitMult = scatteringMult;\
    L.txDiffuseValue.rgb *= saturate(shadowAOModifier * 2.5);\
    LIGHTINGFX(lighting);\
    ReflParams R = getReflParamsZero();\
    R.fresnelEXP = 5;\
    R.ksSpecularEXP = 0;\
    R.finalMult = 1;\
    R.metallicFix = 0;\
    R.reflectionSampleParam = REFL_SAMPLE_PARAM_DEFAULT;\
    R.coloredReflections = 0;\
    R.fresnelMaxLevel = (txAOValue * lerp(0.02, lerp(0.2, 0.4, txLeafValue), pow(saturate(1 - abs(dot(normalW, toCamera))), 4))\
      + saturate(normalW.y * (0.5 + 1.5 * saturate(1 + dot(normalW, toCamera)))) * txLeafValue * (gMatReflectivity * (1 + 0.5 * TREE_RAINFX_WETNESS))\
      * saturate(txAOValue * GAMMA_OR(0.6, 1.4)));\
    R.useStereoApproach = false;\
    float4 withReflection = float4(calculateReflection(lighting.rgb, toCamera, pin.PosC, normalW, R), txDiffuseValue.w);\
    float debugValue = saturate(-dot(ksLightDirection.xyz, normalW));\
    // withReflection.rgb = float3(gMatSpecular, 1 - gMatSpecular, 0);\
    // withReflection.rgb = float3(txLeafValue, 1 - txLeafValue, 0);\
    // withReflection.rgb = (normalW + 1) / 2;\
    // withReflection.rgb = float3(scatteringBaseMult, 1 - scatteringBaseMult, 0);\
    // withReflection.rgb = float3(txAOValue, 1 - txAOValue, 0);\
    // withReflection.rgb = float3(geoSelfShadow, 1 - geoSelfShadow, 0);\
    // withReflection.rgb = float3(debugValue, 1 - debugValue, 0);\
    // withReflection.rgb = float3(shadow, 1 - shadow, 0);
    // withReflection.rgb = (normalW.y + 1) / 2;\
    // withReflection.rgb = float3(RP.wetness, 1 - RP.wetness, 0);\
    // withReflection.rgb = txDiffuseValue.rgb;\
    // withReflection.rgb = float3(shadowAOModifier, 1 - shadowAOModifier, 0);

  #ifdef MODE_FLGTREE_OUTLINE
    #undef TREE_LIGHTING
    #define TREE_LIGHTING\
      float4 withReflection;\
      withReflection.rgb = extWhiteRefPoint * lerp(float3(1, 0.5, 0), float3(0, 1, 0.5), txLeafValue);\
      withReflection.a = saturate(1 - abs(txDiffuseValue.w * 2 - 1));\
      { RETURN(withReflection, withReflection.a); }
  #endif

  #ifndef MODE_MAIN_FX
    #undef USE_CUSTOM_COVERAGE
  #endif

  #include "include_new/base/_include_ps.fx"

  #define FIX_BASIC_A2C
  #include "smoothA2C.hlsl"

  void applyTxDiffuseCorrection(inout float4 txDiffuseValue, uint packedColor){
    txDiffuseValue.rgb *= unpackColor(packedColor).rgb + 0.5;
  }

  float computeBillboardGeoShadow(float3 normalW, float3 toCamera, inout float3 extraDirW, float3 dirW, float2 uv, float aoValue){
    uv = uv * 2 - 1;
    if (uv.y > 0) uv.y = 0;
    else uv.y *= 0.6;

    extraDirW -= toCamera * pow(1 - dot2(uv), 2) * 0.5;    
    extraDirW *= aoValue;
    float flip = lerpInvSat(dot(normalW, toCamera), -0.05, 0.05);
    extraDirW.xz = lerp(extraDirW.xz, -reflect(extraDirW.xz, normalize(toCamera.zx * float2(-1, 1))), flip);

    float3 dirWAlt = extraDirW;
    dirWAlt.y = dirWAlt.y * 0.5 + 0.5;
    dirWAlt.y = pow(dirWAlt.y, 4);
    return saturate(dot(dirWAlt, -ksLightDirection.xyz) * 4 + 1);
  }

  #define TREE_BB_SCATTERING_BASE_MULT_CALC(X) \
    shadow = GAMMA_OR(sqrt(shadow), shadow);\
    float scatteringBaseMult;{\
    float3 origPos = X;\
    origPos.y = origPos.y * 0.5 + 0.5;\
    float yNorm = origPos.y;\
    scatteringBaseMult = GAMMA_LINEAR_SIMPLE(saturate((1 - min(dot(-normalize(pin.PosC.xz), normalize(origPos.xz)), 1 - pow(1.1 * yNorm, 2))) * 1.4));}

  #define TREE_FADING(INDEX_ODD, ALPHA)\
    [branch]\
    if (ALPHA < 1) {\
      float bayerValue = getBayer(pin.PosH);\
      clip(INDEX_ODD ? ALPHA - 1 - bayerValue : ALPHA + bayerValue);\
    }

#endif

#ifndef TREE_NO_FLG_CB
  struct PS_IN_TreeModel {
    float4 PosH : SV_POSITION;
    float3 NormalW : TEXCOORD0;
    float3 PosC : TEXCOORD1;
    float4 Extra : EXTRAVALUE;
    nointerpolation float ShadowMult : TEXCOORD28;
    nointerpolation float Alpha : TEXCOORD20;
    nointerpolation uint Color : TEXCOORD24;
    float2 Tex : TEXCOORD2;
    MOTION_BUFFER
    SHADOWS_COORDS_ITEMS
    float Fog : TEXCOORD8;
    #ifndef NO_NORMALMAPS
      float3 TangentW : TEXCOORD6;
      float3 BitangentW : TEXCOORD7;
    #endif
    AO_NORMALS
    RAINFX_REGISTERS
    CUSTOM_MODE_REGISTERS
  };
#endif

#if defined(TARGET_PS) && !defined(TREE_BILLBOARD_CB) && !defined(TREE_NO_FLG_CB)
  float getLeafValue(float4 txDiffuseValue){
    return gBarkMesh ? 0 : getBaseLeafValue(txDiffuseValue);
  }
#endif

#if defined(TARGET_PS) && defined(TREE_BILLBOARD_CB)

  #ifdef TREE_WITH_LEAVES_MAP
    Texture2D<float> txLeaves : register(TX_SLOT_MAT_2);
  #endif

  #ifdef TREE_ARRAY_MAPPING
    #define gSidesCount 15
    #define gSideSize float2(1./16, 1)
    #define gLODSize float2(1./12, 1)
  #else
    #define gLODSize gSideSize
  #endif
  
  #ifdef TREE_ARRAY_MAPPING
    #define TREE_INDEX_TYPE float
  #else
    #define TREE_INDEX_TYPE int
  #endif

  struct TreeData {
    float4 col;
    float4 nm;
    float leaf;
  };

  TreeData lerp(TreeData a, TreeData b, float v){
    TreeData ret;
    ret.col = lerp(a.col, b.col, v);
    ret.nm = lerp(a.nm, b.nm, v);
    ret.leaf = lerp(a.leaf, b.leaf, v);
    return ret;
  }

  void applyAlphaFix(inout float4 col, inout float4 nm) {
    // float mult = 1 / saturate(max(1e-30, col.w * 1e6));
    // col.rgb *= mult;
    // nm.rgb *= mult;
  }

  TreeData sampleQuad(TREE_INDEX_TYPE index, float2 uv, float2 duvdx, float2 duvdy){
    #ifdef TREE_SAMPLE_0_LOD
      duvdx = 0;
      duvdy = 0;
    #endif
    float4 p = gPivots[index];
    #ifdef EXCLUDE_TOP_FACE
      float lodShift = 1 + lerpInvSat(ksLightDirection.y, -0.3, -0.1);
      duvdx *= lodShift;
      duvdy *= lodShift;
    #endif
    uv = p.xy + gSideSize * uv;
    TreeData ret;
    ret.col = txDiffuse.SampleGrad(samLinearBorder0, uv, duvdx, duvdy);
    ret.nm = txNormal.SampleGrad(samLinearBorder0, uv, duvdx, duvdy);
    #ifdef TREE_WITH_LEAVES_MAP
      ret.leaf = txLeaves.SampleGrad(samLinearBorder0, uv, duvdx, duvdy);
    #endif
    applyAlphaFix(ret.col, ret.nm);
    return ret;
  }

  TreeData sampleAbove(float2 dir, float2 uv, float angle){
    uv = rotate2d(uv * 2 - 1, angle);
    #ifdef TREE_ARRAY_MAPPING
      if (dot2(uv) > 0.99) return (TreeData)0;
      uv = float2(31.f / 32.f, 0.25f) + float2(1 / 32., 0.25) * uv;
    #else
      if (length(uv * gAboveTrim) > 1) return (TreeData)0;
      uv = gAbove.xy + gAbove.zw * uv;
    #endif

    TreeData ret;
    #ifdef TREE_ARRAY_MAPPING
      float2 duvdx = ddx(uv);
      float2 duvdy = ddy(uv);
      ret.col = txDiffuse.SampleGrad(samLinearBorder0, uv, duvdx, duvdy);
      ret.nm = txNormal.SampleGrad(samLinearBorder0, uv, duvdx, duvdy);
      #ifdef TREE_WITH_LEAVES_MAP
        ret.leaf = txLeaves.SampleGrad(samLinearBorder0, uv, duvdx, duvdy);
      #endif
    #else
      ret.col = txDiffuse.Sample(samLinearBorder0, uv);
      ret.nm = txNormal.Sample(samLinearBorder0, uv);
      #ifdef TREE_WITH_LEAVES_MAP
        ret.leaf = txLeaves.Sample(samLinearBorder0, uv);
      #endif
    #endif
    applyAlphaFix(ret.col, ret.nm);
    return ret;

    // return tx.Sample(samLinearBorder0, uv);
  }

  TreeData mixSides(TreeData v0, TreeData v1, float mix){  
    TreeData ret = lerp(v0, v1, mix);
    // float4 ret = lerp(v0, v1, smoothstep(0, 1, mix));
    // return v0;
    #ifdef TREE_ARRAY_MAPPING
      ret.col.w = pow(saturate(ret.col.w), 1.2 - 0.2 * abs(mix * 2 - 1));
    #else
      ret.col.w = pow(saturate(ret.col.w), 2 - abs(mix * 2 - 1));
    #endif
    // return lerp(v0, v1, lerpInvSat(mix, 0.4, 0.6));

    // float4 ret = lerp(v0, v1, mix);
    // ret.w = (ret.w + min(v0.w, v1.w)) / 2;
    // ret.w = lerp(min(v0.w, v1.w), ret.w, pow(abs(mix * 2 - 1), 0.5));
    return ret;
  }

  TreeData sampleQuadMix(float index, float2 uv){
    float mult = any(uv < 0) || any(uv > 1) ? 0 : 1;
    float2 duvdx = ddx(gSideSize * uv);
    float2 duvdy = ddy(gSideSize * uv);
    #ifdef SINGLE_SIDE_MODE
      TreeData ret = sampleQuad(0, uv, duvdx, duvdy);
    #else
      if (index < 0) index += gSidesCount;
      float indexBase = floor(index);
      float indexNext = indexBase + 1;
      TreeData v0 = sampleQuad(indexBase >= gSidesCount ? indexBase - gSidesCount : indexBase, uv, duvdx, duvdy);
      #ifdef TREE_BILLBOARD_SHADOW
        return v0;
      #endif
      TreeData v1 = sampleQuad(indexNext >= gSidesCount ? indexNext - gSidesCount : indexNext, uv, duvdx, duvdy);
      float mix = index - indexBase;
      TreeData ret = mixSides(v0, v1, mix);
    #endif
    ret.col.w *= mult;
    return ret;
  }

  TreeData sample3D(float angle, float aboveAngle, float2 uv, float3 posC, float tilt, float ratio, float uvSYMult,
      float noiseValue){
    #ifdef TREE_ARRAY_MAPPING
      float sideIndex = angle * (gSidesCount / M_TAU) + (gSidesCount / 2.);
    #else
      float sideIndex = angle * gSideIndexMult + gSideIndexAdd;
    #endif
    // float sideIndex = (angle / 3.1415 + 1) * 8;
    float aboveMix = saturate((tilt - uv.y * (1 - tilt) + noiseValue * 0.1 * (1 - 2 * abs(uv.x - 0.5)) - 0.2) * 4 - 1);

    float2 uvS = uv;
    uvS.y = (uvS.y - 0.5) * uvSYMult + 0.5;
    #if !defined(SIMPLIFIED_EFFECTS)
      float m = abs(uvS.x - 0.5) * gTrunkWidthInv;
      if (m < 1){
        uvS.y -= (1 - pow(m, 2)) * tilt * saturate(uvS.y) / gTrunkWidthInv;
      }
    #endif

    TreeData vSide = sampleQuadMix(sideIndex, uvS);

    #ifdef EXCLUDE_TOP_FACE
      return vSide;
    #else
      float2 uvA = uv;
      uvA.y = (uvA.y - 0.5) * ratio + 0.5;
      uvA.y = 1 - pow(saturate(1 - uvA.y), 3 - 2 * tilt - 2 * pow(uv.x * 2 - 1, 2) * (1 - tilt));
      TreeData vAbove = sampleAbove(posC.xz, uvA, aboveAngle);
      
      // return vSide;
      // return vAbove;
      // if (uv.y < 0.01 || uv.y > 0.99) return float4(1, 0, 0, 1);

      vSide.col.w *= 2;
      vAbove.col.w *= saturate((aboveMix - 0.4) / 0.2);
      vSide.col.w *= saturate((aboveMix - 0.85) / -0.1 + uv.y * 10 - 3);
      float precalculatedMix = max(saturate(1 - vSide.col.w * 4), vAbove.col.w * smootherstep(aboveMix));
      #ifdef TREE_BILLBOARD_SHADOW
        precalculatedMix = smootherstep(aboveMix);
      #endif

      return lerp(vSide, vAbove, precalculatedMix);
    #endif
  }

  void sampleTreeBillboardTex(float angle, float2 uv, float3 posC, float tilt, float ratio, float uvSYMult, float windIntensity,
      out float4 col, out float4 nm, out float leaf){
    float aboveAngle = angle;
    float4 noise = txNoise.SampleLevel(samLinear, uv * 0.7, 0);
    #ifndef SINGLE_SIDE_MODE
      angle += (noise.x - 0.5) * lerp(windIntensity, 0.5, abs(uv.x * 2 - 1));
    #endif

    TreeData d = sample3D(angle, aboveAngle, uv, posC, tilt, ratio, uvSYMult, noise.y);
    col = d.col;
    nm = d.nm;
    #ifdef TREE_WITH_LEAVES_MAP
      leaf = d.leaf;
    #else
      leaf = getBaseLeafValue(d.col);
    #endif

    // float mult = 1 / saturate(max(0.001, col.w * 4));
    // col.rgb *= mult;
    // nm.rgb *= mult;
  }

#endif

uint packVector(float2 vec){ return f32tof16(vec.x) | (f32tof16(vec.y) << 16); }