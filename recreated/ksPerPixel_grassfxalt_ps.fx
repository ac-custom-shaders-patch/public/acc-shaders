#define A2C_SHARPENED_THRESHOLD 0.3
#define ALPHATEST_THRESHOLD 10
#define A2C_SHARPENED
#define NO_CARPAINT
#define NO_GRASSFX_COLOR
#define INCLUDE_MULTILAYER_CB
#define SUPPORTS_AO
#define RAINFX_STATIC_OBJECT
#define USE_PS_FOG_IN_MAIN
#define APPLY_LFX_HEADLIGHTS_FIX
#include "include_new/base/_include_ps.fx"

Texture2D txDiffuseAlt : register(TX_SLOT_MAT_1);

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = textureSampleVariation(txDiffuse, samLinear, pin.Tex);
  #ifdef USE_MULTIMAP_TEXTURE_NM_VARIATION
    txDiffuseValue = lerp(textureSampleVariation(txDiffuseAlt, samLinear, pin.Tex), txDiffuseValue, saturate(length(pin.PosC) * extGrassFx12RangeInv - 0.2));
  #endif
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue.xyz);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  RAINFX_REFLECTIVE_WATER_ROUGH(lighting);
  RETURN(lighting, txDiffuseValue.a);
}
