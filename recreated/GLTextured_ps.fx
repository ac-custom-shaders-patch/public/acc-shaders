#define GAMMA_FIX_ADAPTIVE
#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_GL pin) : SV_TARGET {
  float4 txDiffuseValue = txDiffuse.Sample(samLinearClamp, pin.Tex);
  float4 result = txDiffuseValue * pin.Color;
  result.rgb = GAMMA_KSEMISSIVE(result.rgb);
  result.a = GAMMA_ALPHA(result.a);
  return result;
}
