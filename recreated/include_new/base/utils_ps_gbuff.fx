#include "include/common.hlsl"

#ifndef PIN_FOG
  #ifdef NO_FOG
    #define PIN_FOG 0
  #else
    #define PIN_FOG pin.Fog
  #endif
#endif

float3 getRGB(float4 v){ return v.rgb; }
float3 getRGB(float3 v){ return v; }
float3 getRGB(float v){ return v; }

#ifdef GAMMA_TWEAKABLE_ALPHA
void applyAlphaGammaFix(inout float a) {
  if (GAMMA_FIX_ACTIVE) {
    a = saturate(a);
    if (HAS_FLAG(FLAG_ALPHA_S0)) {
      float i = 1.0001 - a;
      if (HAS_FLAG(FLAG_ALPHA_S1)) {
        i = 2.2 * pow(i, 2);
      }
      a = pow(a, i);
    } else if (HAS_FLAG(FLAG_ALPHA_S1)) {
      a = GAMMA_LINEAR(a);
    }
  }
}
#else
void applyAlphaGammaFix(inout float a) {}
#endif

// void ditherFading(float2 posH){  
//   #if defined(ALLOW_DITHER_FADING) && defined(SUPPORTS_DITHER_FADING)
//     [branch]
//     if (extDitherFading != 1) {
//       uint2 p = uint2(posH) % 2;
//       p.y = 1 - p.y;
//       float v = dot(p, float2(1./4, 2./4));
//       if (HAS_FLAG(FLAG_FADING_INV)) v = 1 - v;
//       clip(extDitherFading - v);
//     }
//   #endif
// }
// #define DITHERFADING ditherFading(pin.PosH.xy);
#define DITHERFADING 

#if defined(MODE_SHADOW)
  
  struct PS_OUT {
    float result : SV_Target;
  };

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha)\
    PS_OUT ps_result;\
    clip(alpha - 0.1);\
    ps_result.result = 1;\
    return ps_result;
  #define RETURN_BASE(reflColor, alpha) RETURN(reflColor, alpha)
  #define RETURN_NOTHING return (PS_OUT)0;

#elif defined(MODE_GBUFFER)

  struct PS_OUT {  
    float4 normal : SV_Target;
    #ifdef CREATE_REFLECTION_BUFFER
      float4 baseReflection : SV_Target1;
      float4 reflectionColorBlur : SV_Target2;
      #ifdef CREATE_MOTION_BUFFER
        float2 motion : SV_Target3;
        float stencil : SV_Target4;
      #endif
    #else
      #ifdef CREATE_MOTION_BUFFER
        float2 motion : SV_Target1;
        float stencil : SV_Target2;
      #endif
    #endif
  };

  #ifndef GET_MOTION
    #ifdef CREATE_MOTION_BUFFER
      float2 getMotion(float3 pos0, float3 pos1){
        float2 vVelocity = (pos0.xy / pos0.z) - (pos1.xy / pos1.z);
        vVelocity *= 0.5f;
        vVelocity.y *= -1;
        return vVelocity;
      }
      #define GET_MOTION(x) getMotion(x.PosCS0, x.PosCS1)
    #else
      #define GET_MOTION(x) (float2)0
    #endif
  #endif

  #ifndef ALPHATEST_THRESHOLD
    #ifdef A2C_SHARPENED_THRESHOLD
      #define ALPHATEST_THRESHOLD A2C_SHARPENED_THRESHOLD
    #else
      #define ALPHATEST_THRESHOLD 0.01
    #endif
  #endif

  #ifndef STENCIL_VALUE
    #define STENCIL_VALUE extMotionStencil
  #endif

  #ifndef GBUFF_MASKING_MODE
    #define GBUFF_MASKING_MODE extExtraSharpLocalReflections == -17
  #endif

  PS_OUT prepareResult(float3 posC, float3 toCamera, float3 reflColor, float alpha, float2 reflectionPower, float3 reflectionColorMult, 
      float3 normalW, float2 motion, bool noReflection, float pinFog
      #ifdef ALLOW_RAINFX
      , bool taaDisable
      #endif
      #ifdef PASS_GBUFF_PIN
      , PASS_GBUFF_PIN pin
      #endif
      ){
    PS_OUT ps_result;

    float reflMult = 1.0;
    #ifdef USE_ALPHATEST 
      // clip(alpha - ALPHATEST_THRESHOLD);
      reflMult = alpha;
    #endif

    #ifdef OBJECT_SHADER 
      if (HAS_FLAG(FLAG_GBUFFER_ALPHA_TEST)){
        clip(alpha - ksAlphaRef);
      }
    #endif

    float stencilValue = STENCIL_VALUE; 
    if (stencilValue == 0.005 && dot2(posC) > 85*85){
      stencilValue = 0.01; 
    }

    #ifdef ALLOW_RAINFX
      if (taaDisable){
        stencilValue = 1;
      }
    #endif

    #ifdef CREATE_REFLECTION_BUFFER
      #ifndef GBUFF_SKIP_REFLECTION
      if (noReflection){
      #endif
        ps_result.normal = float4(ssNormalEncode(normalW), alpha);
        ps_result.baseReflection = 0;
        ps_result.reflectionColorBlur = 0;
        #ifdef CREATE_MOTION_BUFFER
          ps_result.motion = motion;
          ps_result.stencil = stencilValue;
        #endif
      #ifndef GBUFF_SKIP_REFLECTION
      } else {
        ps_result.normal = float4(ssNormalEncode(normalW), alpha);
        reflectionPower.y *= saturate(1 - abs(extExtraSharpLocalReflections));
        if (reflectionPower.y > 5.95) reflMult = 0;
        if (HAS_FLAG(FLAG_GBUFFER_PREMULTIPLIED_ALPHA)) reflMult *= alpha;
        reflMult = withFog(reflMult, pinFog, 1);
        float reflPowerAdj = reflectionPower.x * reflMult;
        #ifdef SSLR_FORCE
          if (SSLR_FORCE){ reflPowerAdj = -reflPowerAdj; }
        #else
          if (extExtraSharpLocalReflections < 0){ reflPowerAdj = -reflPowerAdj; }
        #endif
        #if defined(USE_GAMMA_REFLECTIONS) && !defined(GAMMA_FIX)
          reflMult = -reflMult;
        #endif
        ps_result.baseReflection = float4(reflColor.rgb * reflMult * GAMMA_FINAL_MULT, reflPowerAdj);
        ps_result.reflectionColorBlur = float4(reflectionColorMult.rgb, reflectionPower.y / 6);
        #ifdef CREATE_MOTION_BUFFER
          ps_result.motion = motion;
          ps_result.stencil = stencilValue;
        #endif
      }
      #endif
    #else
      ps_result.normal = float4(ssNormalEncode(normalW), alpha);
      #ifdef CREATE_MOTION_BUFFER
        ps_result.motion = motion;
        ps_result.stencil = stencilValue;
      #endif
    #endif

    if (GBUFF_MASKING_MODE){
      ps_result.normal = 1;
      #ifdef CREATE_REFLECTION_BUFFER
        ps_result.baseReflection = 1 - alpha;
        #ifdef GBUFF_MASKING_FALLBACK_ONLY
          ps_result.baseReflection.a = 1;
        #endif
        ps_result.reflectionColorBlur = 1;
      #endif
      #ifdef CREATE_MOTION_BUFFER
        ps_result.motion = 1;
        ps_result.stencil = 1;
      #endif
    }
    
    #ifdef SUPPORTS_DITHER_FADING
      // if (extDitherFading != 1) {
      //   ps_result.stencil = 0.48;
      // }
    #endif

    return ps_result;
  }

  #ifndef GBUFF_NORMALMAPS_DISTANCE
    #define GBUFF_NORMALMAPS_DISTANCE 10
  #endif

  #ifndef GBUFF_NORMALMAPS_NEUTRAL
    #define GBUFF_NORMALMAPS_NEUTRAL normalize(vertexNormalW)
  #endif

  float3 fixNormal(float3 normalW, float3 vertexNormalW, float3 posC){
    return normalize(lerp(normalW, GBUFF_NORMALMAPS_NEUTRAL, saturate(dot(posC, posC) / (GBUFF_NORMALMAPS_DISTANCE * GBUFF_NORMALMAPS_DISTANCE) - 1) ));
  }

  #ifndef GBUFF_NORMAL_W_SRC
    #define GBUFF_NORMAL_W_SRC normalW
  #endif

  #ifndef GBUFF_NORMAL_W_PIN_SRC
    #define GBUFF_NORMAL_W_PIN_SRC pin.NormalW
  #endif

  #ifdef ALLOW_RAINFX
    #define GBU_ARG_RN ,RP.taaDisable
  #else
    #define GBU_ARG_RN
  #endif

  #ifdef PASS_GBUFF_PIN
    #define GBU_ARG_PIN ,pin
  #else
    #define GBU_ARG_PIN
  #endif

  #define RESULT_TYPE PS_OUT

  float3 estimateReflectionColor(float3 color, float colorMult, float3 secondaryColor){
    return lerp(1, color, colorMult) * (1 + secondaryColor) / (1 + dot(secondaryColor, 1./4.)) / 2;
  }

  #ifdef NO_GBUFFER
    #define RETURN(reflColor, alpha) clip(-1); return (PS_OUT)0;
    #define RETURN_BASE(reflColor, alpha) clip(-1); return (PS_OUT)0;
    #define RETURN_NOTHING clip(-1); return (PS_OUT)0;
  #elif defined(ZERO_GBUFFER)
    #define RETURN(reflColor, alpha) return (PS_OUT)0;
    #define RETURN_BASE(reflColor, alpha) return (PS_OUT)0;
    #define RETURN_NOTHING return (PS_OUT)0;
  #else
    #ifdef USE_SECONDARY_REFLECTION_LAYER
      #define REFL_COLOR_CALL estimateReflectionColor(R.coloredReflectionsColor, R.coloredReflections, R.secondaryIntensity)
    #else
      #define REFL_COLOR_CALL estimateReflectionColor(R.coloredReflectionsColor, R.coloredReflections, 0)
    #endif

    #define RETURN(reflColor, alpha) DITHERFADING return prepareResult(pin.PosC, toCamera, R.resultColor, alpha, float2(R.resultPower, R.resultBlur), \
      REFL_COLOR_CALL, fixNormal(GBUFF_NORMAL_W_SRC, GBUFF_NORMAL_W_PIN_SRC, pin.PosC), GET_MOTION(pin), false, PIN_FOG GBU_ARG_RN GBU_ARG_PIN);
    #define RETURN_BASE(reflColor, alpha) DITHERFADING return prepareResult(pin.PosC, toCamera, getRGB(reflColor), alpha, 0, 0,\
      fixNormal(GBUFF_NORMAL_W_SRC, GBUFF_NORMAL_W_PIN_SRC, pin.PosC), GET_MOTION(pin), true, PIN_FOG GBU_ARG_RN GBU_ARG_PIN);
    #define RETURN_NOTHING clip(-1); return (PS_OUT)0;
  #endif

#elif defined(GBUFFER_GRASSFX)

  #define GRASS_PARAMS_MASK_ONLY
  #include "grass/flgGrass_configurations.hlsl"

  cbuffer cbGrassFXMap : register(b5) {
    PackedGrassParams gp[PACKED_GRASS_PARAMS_COUNT];
  }

  Texture2D txAdjustments : register(TX_SLOT_GRASS_PASS_ADJUSTMENTS);
  GrassParams getGrassParams(float2 uv){
    return getGrassParams(gp, txAdjustments.SampleLevel(samLinearClamp, uv, 0));
  }

  #define VAR_INV_MASKING_MODE ksExposure
  #define FLAG_INV_MASKING_MODE (-17)

  float adjustAlpha(float v){
    if (VAR_INV_MASKING_MODE == FLAG_INV_MASKING_MODE){
      return 1 - v;
    }
    #ifdef USE_OPAQUE_FIX
      return ksAlphaRef > 0 ? v : 1;
    #else
      return v;
    #endif
  }

  float3 clipSaturation(float3 color, float2 posH){
    if (ksScreenWidth == 0 || VAR_INV_MASKING_MODE == FLAG_INV_MASKING_MODE) return color;    
    GrassParams GP = getGrassParams(posH / float2(ksScreenWidth, ksScreenHeight));
    float colorTest = colorThreshold(color.rgb, GP.mask_mainThreshold, GP.mask_redThreshold, GP.mask_minLuminance, GP.mask_maxLuminance);
    clip(colorTest - 0.2);
    return color;
  }

  #ifdef LIGHTINGFX_NOSPECULAR
    #define LIGHTINGFX_SPECULAR_EXP 1
    #define LIGHTINGFX_SPECULAR_COLOR 0
  #else
    #ifndef LIGHTINGFX_SPECULAR_EXP
      #define LIGHTINGFX_SPECULAR_EXP L.specularExp
    #endif
    #ifndef LIGHTINGFX_SPECULAR_COLOR
      #define LIGHTINGFX_SPECULAR_COLOR (L.specularValue * L.txSpecularValue)
    #endif
  #endif

  struct PS_OUT {
    float4 result : SV_Target;
    float4 normal_ao : SV_Target1;
    float4 amb_dif_spec_specexp : SV_Target2;
  };

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha)\
    PS_OUT ps_result;\
    clip(normalW.y - 0.2);\
    float3 reflColor_ = clipSaturation(getRGB(reflColor), pin.PosH.xy);\
    ps_result.result = float4(reflColor_, adjustAlpha(alpha));\
    ps_result.normal_ao.xyz = normalW * 0.5 + 0.5;\
    ps_result.normal_ao.w = dot((float3)AO_LIGHTING, 1.0/3.0);\
    ps_result.amb_dif_spec_specexp.x = INPUT_DIFFUSE_K;\
    ps_result.amb_dif_spec_specexp.y = INPUT_AMBIENT_K;\
    ps_result.amb_dif_spec_specexp.z = dot(LIGHTINGFX_SPECULAR_COLOR, 1./3.);\
    ps_result.amb_dif_spec_specexp.w = LIGHTINGFX_SPECULAR_EXP / 255;\
    return ps_result;
  #define RETURN_BASE(reflColor, alpha) RETURN(reflColor, alpha)
  #define RETURN_NOTHING return (PS_OUT)0;

#elif defined(GBUFFER_NORMALSAMPLE)

  #define VAR_SPECIAL_MODE ksExposure
  #define FLAG_SPECIAL_MODE_TYRES (-19)

  struct PS_OUT {
    float4 normal_ao : SV_Target;
  };

  float3 getNmModeTyres(float3 normal, float3 posC){
    float3 posR = posC + ksCameraPosition.xyz - extDirUp;
    float3 posL = posR + dot(posR, extCPLDir) * extCPLDir;
    float3 nmL = normalize(cross(posL, extCPLDir));
    return float3(dot(normal, extCPLDir),  dot(normalize(nmL), normal), 1);
  }

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha)\
    PS_OUT ps_result;\
    ps_result.normal_ao.xyz = (VAR_SPECIAL_MODE == FLAG_SPECIAL_MODE_TYRES ? getNmModeTyres(normalW, pin.PosC) : normalW) * 0.5 + 0.5;\
    ps_result.normal_ao.w = 1;\
    return ps_result;
  #define RETURN_BASE(reflColor, alpha) RETURN(reflColor, alpha)
  #define RETURN_NOTHING return (PS_OUT)0;

#elif defined(MODE_COLORSAMPLE)

  #define VAR_SPECIAL_MODE ksExposure
  #define VAR_SPECIAL_MODE2 ksSaturation
  #define FLAG_SPECIAL_MODE_SOLID (-19)
  #define FLAG_SPECIAL_MODE_FILTER (-20)
  #define FLAG_SPECIAL_MODE_AREA (-21)

  float3 adjustColor(float3 color){
    if (VAR_SPECIAL_MODE == FLAG_SPECIAL_MODE_SOLID){
      return 1;
    }
    if (VAR_SPECIAL_MODE == FLAG_SPECIAL_MODE_FILTER){
      float value = dot(color, 3) - 1;
      clip(value);
      return saturate(value);
      return 1;
    }
    return color;
  }

  float adjustAlpha(float v){
    #ifdef USE_OPAQUE_FIX
      if (VAR_SPECIAL_MODE2 == FLAG_SPECIAL_MODE_AREA){
        return v;
      }
      return ksAlphaRef > 0 ? v : 1;
    #else
      return v;
    #endif
  }

  struct PS_OUT {
    float4 result : SV_Target0;
    float ao : SV_Target1;
  };
  
  #ifndef _AO_VAO
  #define _AO_VAO 1
  #endif

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha)\
    PS_OUT ps_result;\
    ps_result.result = float4(adjustColor(getRGB(reflColor)), adjustAlpha(alpha));\
    ps_result.ao = _AO_VAO;\
    return ps_result;
  #define RETURN_BASE(reflColor, alpha) RETURN(reflColor, alpha)
  #define RETURN_NOTHING return (PS_OUT)0;

  #define damageZones ((float4)0)
  #define blurLevel 0.0
  #define dirtyLevel 0.0

#elif defined(MODE_SURFACESAMPLE)

  cbuffer _cbData : register(b7) {
    float4x4 gWorldToCar;
    float3 gAABBMin;
    float gPad0;
    float3 gAABBMax;
    float gPad1;
  }

  float3 getPosRel(float3 posW){
    float3 ret = (mul(float4(posW, 1), gWorldToCar).xyz - gAABBMin) / (gAABBMax - gAABBMin);
    ret.xz = ret.xz * 2 - 1;
    return ret;
  }

  struct PS_OUT {
    float4 color : SV_Target0;
    float4 surface : SV_Target1;
    float4 material1 : SV_Target2;
  };

  #ifndef GBUFF_NORMAL_W_SRC
    #define GBUFF_NORMAL_W_SRC normalW
  #endif
    
  #define RESULT_TYPE PS_OUT
  #define RETURN_GEN(reflColor, alpha, materialData1)\
    float3 nmLocal = mul(GBUFF_NORMAL_W_SRC, (float3x3)gWorldToCar);\
    PS_OUT ps_result;\
    ps_result.color = float4((float3)reflColor, alpha);\
    ps_result.surface = float4(nmLocal * 0.5 + 0.5, length(pin.PosC) / 2);\
    ps_result.material1 = materialData1;\
    return ps_result;

  #define GATHER_MATERIAL_DATA1 float4(R.fresnelC * R.finalMult, R.fresnelEXP / 10, R.fresnelMaxLevel * R.finalMult, R.resultBlur / 6)
  #define RETURN(reflColor, alpha) RETURN_GEN(reflColor, alpha, GATHER_MATERIAL_DATA1)
  #define RETURN_BASE(reflColor, alpha) RETURN_GEN(reflColor, alpha, 0)
  #define RETURN_NOTHING return (PS_OUT)0;

  #define damageZones ((float4)0)
  #define blurLevel 0.0
  #define dirtyLevel 0.0

#elif defined(MODE_PUDDLES)

  struct PS_OUT {
    float2 result : SV_Target;
  };

  PS_OUT getResult(RainParams RP, float alpha){
    PS_OUT ps_result;    
    ps_result.result.x = ENCODE_NZN_UINT8(sqrt(saturate(RP.water)));
    ps_result.result.y = ENCODE_NZN_UINT8(saturate(RP.wetness));
    return ps_result;
  }

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha) return getResult(RP, alpha);
  #define RETURN_BASE(reflColor, alpha) return getResult(RP, alpha);
  #define RETURN_NOTHING return (PS_OUT)0;

#elif defined(MODE_SHADOWS_HEAT)

  struct PS_OUT {
    float result : SV_Target;
  };

  PS_OUT getResult(float shadow, float alpha){
    PS_OUT ps_result;    
    ps_result.result = clamp(shadow, 0.01, 1);
    return ps_result;
  }

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha) return getResult(shadow, alpha);
  #define RETURN_BASE(reflColor, alpha) return getResult(shadow, alpha);
  #define RETURN_NOTHING return (PS_OUT)0;

#elif defined(MODE_COLORMASK)

  struct PS_OUT {
    float4 result : SV_Target;
  };

  cbuffer cbMaskParams : register(b8) {
    float3 extraColor;
    float opacityMult;
    float3 colorMult;
    float opacityClip;
  }

  float calculateStValue(float shadow, float3 normalW){
    return saturate(abs(dot(normalize(normalW), ksLightDirection.xyz)) * 2) * shadow;
  }

  PS_OUT getResult(float3 c, float a, float fog, float stValue){
    clip(a - opacityClip);
	  c *= colorMult;
    c += extraColor;

		#ifdef COLORMARK_SEETHROUGH
      float s = saturate((max(c.r, max(c.g, c.b)) - min(c.r, min(c.g, c.b))) * 5);
      a *= s;
      c = max(0, lerp(luminance(c), c, 2));
      c *= (AMBIENT_COLOR_NEARBY * 2 + stValue * ksLightColor.rgb) / (AMBIENT_COLOR_NEARBY * 2 + ksLightColor.rgb);
    #else
      if (opacityMult < 0) {
        c *= 1 - pow(saturate(a), 16);
      } else {
        float m = max(c.r, max(c.g, c.b));
        float d = 1 / max(0.5, m);
        c *= d;
        m *= d;
        c += 1 - m;
        c = lerp(1, c, pow(1 - fog, 2));
      }
		#endif

    a = saturate(a * abs(opacityMult));
    c = saturate(lerp(1, c, a));
    c = GAMMA_LINEAR(c);

    PS_OUT ps_result;    
    ps_result.result = float4(c, 1);
    return ps_result;
  }

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha) return getResult(getRGB(reflColor), alpha, PIN_FOG, calculateStValue(shadow, pin.NormalW));
  #define RETURN_BASE(reflColor, alpha) return getResult(getRGB(reflColor), alpha, PIN_FOG, calculateStValue(shadow, pin.NormalW));
  #define RETURN_NOTHING return (PS_OUT)0;

  #define damageZones ((float4)0)
  #define blurLevel 0.0
  #define dirtyLevel 0.0

#elif defined(MODE_EMISSIVESAMPLE)

  struct PS_OUT {
    float4 result : SV_Target;
  };

  PS_OUT getResult(float4 txDiffuseValue, float4 txEmissiveValue){
    PS_OUT ps_result;    
    #ifdef USE_TXEMISSIVE_AS_EMISSIVE
      ps_result.result = txEmissiveValue;
    #else
      ps_result.result = float4(luminance(txDiffuseValue.rgb), 0, 0, 0);
    #endif
    return ps_result;
  }

  #ifndef USE_TXEMISSIVE_AS_EMISSIVE
    #define TXEMISSIVE_VALUE 0
  #endif

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha) return getResult(txDiffuseValue, TXEMISSIVE_VALUE);
  #define RETURN_BASE(reflColor, alpha) return getResult(txDiffuseValue, TXEMISSIVE_VALUE);
  #define RETURN_NOTHING return (PS_OUT)0;

  #define damageZones ((float4)0)
  #define blurLevel 0.0
  #define dirtyLevel 0.0

#elif defined(MODE_LIGHTS)

  struct PS_OUT {
    float4 result : SV_Target;
  };

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha)\
    PS_OUT ps_result;\
    ps_result.result = float4(getRGB(reflColor) * alpha, 0);\
    return ps_result;
  #define RETURN_BASE(reflColor, alpha) RETURN(reflColor, alpha)
  #define RETURN_NOTHING return (PS_OUT)0;

#elif defined(MODE_LIGHTMAP)

  struct PS_OUT {
    float4 result : SV_Target;
    #ifdef LIGHTMAP_CAUSTICS
      float caustics : SV_Target1;
    #endif
  };

  #ifdef LIGHTMAP_CAUSTICS
    #define LC_RETURN_SET ps_result.caustics = extCausticsBrightness * shadow
  #else
    #define LC_RETURN_SET
  #endif

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha)\
    PS_OUT ps_result;\
    ps_result.result = float4(getRGB(reflColor), alpha);\
    if (GAMMA_FIX_ACTIVE) { ps_result.result.rgb = saturate(ps_result.result.rgb / (1 + ps_result.result.rgb)); }\
    LC_RETURN_SET;\
    return ps_result;
  #define RETURN_BASE(reflColor, alpha) RETURN(reflColor, alpha)
  #define RETURN_NOTHING return (PS_OUT)0;

#elif defined(MODE_SHADOWS_ADVANCED)

  struct PS_OUT {
    float result : SV_Target;
  };

  #define RESULT_TYPE PS_OUT
  #define RETURN(reflColor, alpha)\
    PS_OUT ps_result;\
    ps_result.result = 0;\
    return ps_result;
  #define RETURN_BASE(reflColor, alpha) RETURN(reflColor, alpha)
  #define RETURN_NOTHING return (PS_OUT)0;

#else

  #ifndef A2C_SHARPENED_THRESHOLD
    #define A2C_SHARPENED_THRESHOLD 0.7
  #endif

  float adjustAlpha(float alpha){
    // #ifdef A2C_SHARPENED
    //   alpha = saturate((alpha - A2C_SHARPENED_THRESHOLD) / max(fwidth(alpha), 0.0001) + 0.5);
    //   clip(alpha - 0.1);
    // #endif
    // #ifdef A2C_SHARPENED_SIMPLE
    //   clip(alpha - A2C_SHARPENED_THRESHOLD);
    //   alpha = 1;
    // #endif
    applyAlphaGammaFix(alpha);
    return alpha;
  }

  float3 applyAoMult(float3 color, float mult){
    #ifdef USE_PERVERTEX_AO_AS_MULT
      return color * mult;
    #endif
    return color;
  }

  float3 applyAoMult(float3 color, float4 mult){
    #ifdef USE_PERVERTEX_AO_AS_MULT
      return color * mult.xyz;
    #endif
    return color;
  }

  void ensureVarsUsed(inout float v){
    #ifdef ENSURE_VARS_USED
      v += saturate(extExtraSharpLocalReflections) * 0.00001;
    #endif
  }

  #ifndef CUSTOM_PS_OUT
    struct PS_OUT {
      float4 result : SV_Target;
    };
    #define PS_OUT_RET return ps_result;
  #else
    #define PS_OUT_RET 
  #endif

  #define RESULT_TYPE PS_OUT

  #ifdef WINDSCREEN_REPROJECTION_CRAZE
    #define RETURN(reflColor, alpha)\
      PS_OUT ps_result;\
      ps_result.result = float4(applyAoMult(getRGB(reflColor), AO_LIGHTING) * pin.DepthOffset, adjustAlpha(alpha));\
      DITHERFADING\
      ps_result.result *= GAMMA_FINAL_MULT;\
      PS_OUT_RET;
  #else
    #define RETURN(reflColor, alpha)\
      PS_OUT ps_result;\
      ensureVarsUsed(reflColor.r);\
      ps_result.result = withFog(applyAoMult(getRGB(reflColor), AO_LIGHTING), PIN_FOG, adjustAlpha(alpha));\
      DITHERFADING\
      ps_result.result *= GAMMA_FINAL_MULT;\
      PS_OUT_RET;
  #endif

  #define RETURN_BASE(reflColor, alpha) RETURN(reflColor, alpha)
  #define RETURN_NOTHING return (PS_OUT)0;

#endif