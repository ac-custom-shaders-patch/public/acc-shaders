#ifdef GAMMA_FIX
  #undef FOG_NEW_FORMULA
  #define FOG_NEW_FORMULA true
#endif

#ifndef FOG_NEW_FORMULA
  #define FOG_NEW_FORMULA extUseNewFog
#endif

#if defined(GAMMA_FIX) || defined(USE_PS_FOG_IN_MAIN)
  #define USE_PS_FOG
#endif

#ifdef FOG_MULT_MODE
float withFogImpl(float baseResult, float3 posC, float3 posN, float fog, float fogMult, float alphaValue){
#elif defined(FOG_FEEDBACK)
float4 withFogImpl(float3 baseResult, float3 posC, float3 posN, float fog, float fogMult, float alphaValue, out float outFogIntensity){
#else
float4 withFogImpl(float3 baseResult, float3 posC, float3 posN, float fog, float fogMult, float alphaValue){
#endif
  #ifdef USE_OPAQUE_FIX
    alphaValue = ksAlphaRef >= 0 ? alphaValue : 1;
  #endif

  #if defined(SKIP_PS_FOG)
    if (GAMMA_FIX_ACTIVE)
  #endif

  #if defined(NO_FOG) || defined(SKIP_PS_FOG)
    #ifdef FOG_FEEDBACK
      outFogIntensity = 0;
    #endif
    #ifdef FOG_MULT_MODE
      return baseResult;
    #else
      return float4(baseResult.rgb, alphaValue);
    #endif
  #endif

  if (GAMMA_FIX_ACTIVE) {
    #ifdef USE_PS_FOG_RECOMPUTE
      float fogRecompute = 1 - exp(-length(posC) * ksFogLinear);
      fog = ksFogBlend * pow(saturate(fogRecompute), extFogExp);
    #endif

    float3 fogColor = ksFogColor;
    float3 fogIntensity = saturate(fog * fogMult);
    fogIntensity.r = lerp(fogIntensity.r, pow(fogIntensity.r, 3), extFogAtmosphere);
    fogIntensity.g = lerp(fogIntensity.g, pow(fogIntensity.g, 2), extFogAtmosphere);

    float sunAmount = saturate(dot(-posN, ksLightDirection.xyz));
    #if defined(USE_BACKLIT_FOG)
      fogColor += ksLightColor.xyz * pow(sunAmount, extFogBacklitExp) * extFogBacklitMult;
    #endif
    #if !defined(FOG_MULT_MODE) && !defined(FOG_FAKE_SHADOW_MODE)
      baseResult = lerp(baseResult, fogColor, fogIntensity);
    #endif

    #ifdef FOG_FEEDBACK
      outFogIntensity = fogIntensity.b;
    #endif

    {      
      float4 fog2Color_blend = loadVector(p_fog2Color_blend);
      #if defined(USE_BACKLIT_FOG)
        fog2Color_blend.rgb += ksLightColor.xyz * pow(sunAmount, extFogBacklitExp) * extFogBacklitMult;
      #endif

      float yK = posN.y / (extFogConstantPiece + sign(extFogConstantPiece) * abs(posN.y));
      float buggyPart = 1 - exp(-max(length(posC) - 2.4, 0) * pow(2, yK * 5) * p_fog2Linear); 
      float fog2 = fog2Color_blend.w * pow(saturate(buggyPart), p_fog2Exp);

      #ifdef FOG_FEEDBACK
        outFogIntensity = lerp(outFogIntensity, 1, saturate(fog2 * fogMult));
      #endif

      #ifdef FOG_MULT_MODE
        return baseResult * saturate(1 - fog * fogMult) * saturate(1 - fog2);
      #elif !defined(FOG_FAKE_SHADOW_MODE)
        baseResult = lerp(baseResult, fog2Color_blend.rgb, saturate(fog2 * fogMult));
      #else
        alphaValue *= pow(saturate(1 - fog * fogMult) * saturate(1 - fog2), GAMMA_OR(4, 2));
      #endif

      // baseResult = float3(fog, 1 - fog, 0) * 0.5;
      // baseResult = float3(fog2, 1 - fog2, 0) * 0.5;
      // baseResult = float3(fogIntensity.b, 1 - fogIntensity.b, 0) * 0.05;
    }

    #ifndef FOG_MULT_MODE
      return float4(baseResult, alphaValue);
    #endif
  } else {
    #if defined(USE_PS_FOG) && !defined(MODE_KUNOS)
      #ifndef FOG_NEW_FORMULA_NO_BRANCH
        [branch]
      #endif
      if (FOG_NEW_FORMULA){
        float buggyPart;
        if (abs(posN.y) < 0.001) posN.y = 0.001;
        buggyPart = (1 - exp(-length(posC) * posN.y * ksFogLinear)) / posN.y;
        buggyPart *= extFogConstantPiece;
        fog = ksFogBlend * pow(saturate(buggyPart), extFogExp);
      }
    #endif
    
    #ifdef FOG_MULT_MODE
      return baseResult * saturate(1 - fog * fogMult);
    #else
      float3 fogColor = ksFogColor;
      float3 fogIntensity = saturate(fog * fogMult);
      fogIntensity.r = lerp(fogIntensity.r, pow(fogIntensity.r, 3), extFogAtmosphere);
      fogIntensity.g = lerp(fogIntensity.g, pow(fogIntensity.g, 2), extFogAtmosphere);

      // float t = fog;
      // float3 col = baseResult.rgb;
      // float3 ext = exp2(-t*0.00025*float3(1,1.5,4)); 
      // return float4(col*ext + (1.0-ext) * fogColor, alphaValue); // 0.55

      // fogIntensity = (1 - exp(-dot2(posC) / 1e3));
      // if (fogIntensity.r > 1) {
      //   return float4(0, 0, 1, 1);
      // }
      // fog = 0.999;

      #if defined(USE_BACKLIT_FOG) && !defined(MODE_KUNOS)
        float sunAmount = saturate(dot(-posN, ksLightDirection.xyz));
        fogColor += ksLightColor.xyz * pow(sunAmount, extFogBacklitExp) * extFogBacklitMult;
      #endif
      #ifdef FOG_FEEDBACK
        outFogIntensity = fogIntensity.b;
      #endif
      return float4(lerp(baseResult.rgb, fogColor, fogIntensity), alphaValue);
    #endif
  }
}

#if !defined(FOG_INPUT_POSC) && defined(USE_PS_FOG)
  #ifdef MODE_GBUFFER
    #define FOG_INPUT_POSC posC
  #else
    #define FOG_INPUT_POSC pin.PosC
  #endif
#else
  #define FOG_INPUT_POSC 0
#endif

#if !defined(FOG_INPUT_TOCAMERA) && (defined(USE_PS_FOG) || defined(USE_BACKLIT_FOG))
  #define FOG_INPUT_TOCAMERA toCamera
#else
  #define FOG_INPUT_TOCAMERA 0
#endif

#ifdef FOG_FEEDBACK
  #define withFog(_BASE_RESULT, _FOG, _ALPHA_VALUE, _OUT_FOGINTENSITY) withFogImpl(_BASE_RESULT, FOG_INPUT_POSC, FOG_INPUT_TOCAMERA, _FOG, 1, _ALPHA_VALUE, _OUT_FOGINTENSITY)
#else
  #define withFog(_BASE_RESULT, _FOG, _ALPHA_VALUE) withFogImpl(_BASE_RESULT, FOG_INPUT_POSC, FOG_INPUT_TOCAMERA, _FOG, 1, _ALPHA_VALUE)
#endif
#define FOG_FAKE_SHADOW_MULT pow(withFog(1, pin.Fog, 1), 2)