// Utils

#ifndef EXCLUDE_PEROBJECT_CB

  float3 normals(float3 input){
    return mul(input, (float3x3)ksWorld);
  }

  float3 bitangent(float3 normalL, float3 tangentL){
    #ifndef NO_NORMALMAPS
      return mul(cross(tangentL, normalL), (float3x3)ksWorld);
    #else
      return 0;
    #endif
  }

#endif

#ifdef NO_SHADOWS
  #define SHADOWS_COORDS 0
  void shadows(float4 posW, float v){ }
  void noShadows(float v){ }
#elif defined(MODE_KUNOS)
  #define SHADOWS_COORDS vout.ShadowTex0, vout.ShadowTex1, vout.ShadowTex2
  void shadows(float4 posW, out float4 tex0, out float4 tex1, out float4 tex2){
    tex0 = mul(posW, ksShadowMatrix0);
    tex1 = mul(posW, ksShadowMatrix1);
    tex2 = mul(posW, ksShadowMatrix2);
  }

  void noShadows(out float4 tex0, out float4 tex1, out float4 tex2){
    tex0 = 0;
    tex1 = 0;
    tex2 = 0;
  }
#else
  #define SHADOWS_COORDS vout.ShadowTex0
  void shadows(float4 posW, out float4 tex0){
    tex0 = mul(posW, ksShadowMatrix0);
  }

  void noShadows(out float4 tex0){
    tex0 = 0;
  }
#endif

#ifdef NO_REFLECTIONS
  #define isAdditive 1
#endif

#include "include/fog.hlsl"

// Local-to-sceen conversion

#ifndef EXCLUDE_PEROBJECT_CB

  #ifndef ALTER_POSL 
    #define ALTER_POSL(local, prevFrame) ;
  #endif

  #ifndef ALTER_POSW 
    #define ALTER_POSW(local, prevFrame, world) ;
  #endif

  #ifndef ALTER_POSV 
    #define ALTER_POSV(local) ;
  #endif

  #if defined(MODE_LIGHTMAP) || defined(MODE_COLORSAMPLE) && !defined(SKINNED_VERTEX)
    #define CLIP_PROBES 16
    cbuffer cbClipPlane : register(b13) {
      float gClipBaseWeight;
      float gClipBaseWeightY;
      float2 gClipPad0;
      float4 gClipProbes[CLIP_PROBES];
    }
    float findClipPlane(float2 posXZ){
      float2 valAndWeight = float2(gClipBaseWeightY, gClipBaseWeight);
      for (int i = 0; i < CLIP_PROBES; i++){
        valAndWeight += gClipProbes[i].zw / dot2(gClipProbes[i].xy - posXZ);
      }
      return valAndWeight.x / valAndWeight.y;
    }
    void lightMapClipping(inout float3 posW){
      #ifdef MODE_COLORSAMPLE
      #define FLAG_SPECIAL_MODE_AREA (-21)
      if (ksExposure != FLAG_SPECIAL_MODE_AREA) return;
      #endif
      float clipPlane = findClipPlane(posW.xz);
      if (clipPlane < posW.y) posW.y += 1e8;
    }
  #else
    void lightMapClipping(inout float3 posW){}
  #endif

  void alterPosW(float3 posL, bool prevFrame, inout float3 posW){  
    ALTER_POSW(posL, prevFrame, posW);
    lightMapClipping(posW);
  }

  void alterPosW(float3 posL, bool prevFrame, inout float4 posW){  
    alterPosW(posL, prevFrame, posW.xyz);
  }

  cbuffer _cbCarFlex : register(b9) {
    float3 flexPivot;
    float flexLimit;
    float3 flexAxis;
    float _flexPad1;
    float4x4 flexData[2];
    float3 flexPivot_prev;
    float _flexPad0_prev;
    float3 flexAxis_prev;
    float _flexPad1_prev;
    float4x4 flexData_prev[2];
  }

  void applyPointFlex(inout float3 posW){
    [branch]
    if (flexPivot.x) { 
      // 164 with per-vertex flexOffset, 165 done this way
      float flexAmount = dot(flexAxis, posW - flexPivot);
      if (flexAmount > flexLimit) flexAmount = saturate(2 - flexAmount);
      posW = lerp(posW, mul(float4(posW, 1), flexData[flexAmount > 0]).xyz, saturate(abs(flexAmount)));
    }
  }

  void applyPointFlex_prev(inout float3 posW){
    [branch]
    if (flexPivot.x) { 
      float flexAmount = dot(flexAxis_prev, posW - flexPivot_prev);
      if (flexAmount > flexLimit) flexAmount = saturate(2 - flexAmount);
      posW = lerp(posW, mul(float4(posW, 1), flexData_prev[flexAmount > 0]).xyz, saturate(abs(flexAmount)));
    }
  }

  #if defined(ALLOW_CAR_FLEX) && (!defined(RAINFX_STATIC_OBJECT) || defined(RAINFX_STATIC_BUT_KEEP_FLEX)) && !defined(RAINFX_NO_PERVERTEX_OCCLUSION) && !defined(USE_PS_FOG) && !defined(RAINFX_USE_PUDDLES_MASK)
    #define APPLY_CFX(posW) applyPointFlex(posW.xyz)
  #else
    #define APPLY_CFX(posW) 
  #endif

  float4 __toScreenSpace(float4 posL, float3 tangentPacked, out float4 posW, out float4 posWnoflex, out float4 posV SPS_VS_TOSS_ARG){
    ALTER_POSL(posL.xyz, false);

    #ifdef POSL_IS_POSW
    posW = posL;
    #else
    posW = mul(posL, ksWorld);
    #endif
    alterPosW(posL.xyz, false, posW);
    posWnoflex = posW;
    APPLY_CFX(posW);

    #ifdef MODE_SHADOWS_ADVANCED
      posV = 0;
      return mul(posW, ksMVPInverse);
    #endif

    posV = mul(posW, ksView);
    ALTER_POSV(posV.xyz);

    float4 posVL = posV;
    // #if defined(SUPPORTS_DISTANT_FIX_) && defined(ALLOW_DISTANT_FIX)
    //   [branch]
    //   if (extDistantFixStart > 0){
    //     float dist = length(posV.xyz);
    //     if (dist > extDistantFixStart){
    //       float newDist = extDistantFixStart + (dist - extDistantFixStart) * extDistantFixMult;
    //       posVL.xyz *= newDist / dist;
    //     }
    //   }
    // #endif

    #if defined(SUPPORTS_DISTANT_FIX) && defined(ALLOW_DISTANT_FIX)
      #define DISTANT_FIX_K 0.8

      [branch]
      if (HAS_FLAG(FLAG_APPLY_DISTANT_FIX)){
        float dist = -posV.z / ksFarPlane;
        if (dist > DISTANT_FIX_K){
          float newDist = min(remap(dist, 1, 5, DISTANT_FIX_K, 0.95), 0.95);
          posVL.xyz *= newDist / dist;
        }
      }
    #endif

    float4 posH = mul(posVL, ksProjection);
    // #if defined(SUPPORTS_DISTANT_FIX) && defined(ALLOW_DISTANT_FIX)
    //   [branch]
    //   if (extDistantFixStart > 0){
    //     posH.z = min(posH.z, (1 - 0.0001 / (1 + length(posV.xyz) / 1e6)) * posH.w);
    //   }
    // #endif

    // #ifdef MODE_COLORSAMPLE
    // if (gDebugFlag){
    //   float4 posCar = mul(posW, gWorldToCar);
    //   posCar /= posCar.w;
    //   posH.xz /= 1 + posCar.y;
    // }
    // posH.z += 10;
    // #endif

    return posH;
  }

  #define toScreenSpace(posL, posW, posWnoflex, posV)\
    __toScreenSpace(posL, vin.TangentPacked, posW, posWnoflex, posV SPS_VS_TOSS_PASS)

  #ifndef SKINNED_WEIGHT_CHECK
    #define SKINNED_WEIGHT_CHECK(a) (a != 0)
  #endif

  #ifdef CREATE_MOTION_BUFFER
    float4 getMotion(float4 posL, float4 basePosH){
      ALTER_POSL(posL.xyz, true);
      float4 posW = mul(posL, ksWorldPrev);
      alterPosW(posL.xyz, true, posW);
      applyPointFlex_prev(posW.xyz);
      float4 posH = mul(posW, ksViewProjPrev);
      return posH;
    }

    float4 getMotionWorld(float4 posL, float4 basePosH){
      ALTER_POSL(posL.xyz, true);
      float4 posW = posL;
      alterPosW(posL.xyz, true, posW);
      float4 posH = mul(posW, ksViewProjPrev);
      return posH;
    }

    float4 getMotionVelocity(float4 posL, float3 velocity, float4 basePosH){
      ALTER_POSL(posL.xyz, true);
      float4 posW = posL;
      posW.xyz -= velocity;
      alterPosW(posL.xyz, true, posW);
      float4 posH = mul(posW, ksViewProjPrev);
      return posH;
    }

    float4 getMotionSkinned(float4 posL, float4 posWN, float4 boneWeights, float4 boneIndices, float4 basePosH){
      ALTER_POSL(posL.xyz, true);
      float4 posW = 0;
      for (int i = 0; i < 4; i++){
        float weight = boneWeights[i];
        if (SKINNED_WEIGHT_CHECK(weight)){
          uint index = (uint)boneIndices[i];
          float4x4 bone = bonesPrev[index];
          posW += (mul(bone, posL) + float4(ksWorldPrev[3].xyz, 0)) * weight;
        }
      }
      alterPosW(posL.xyz, true, posW);
      applyPointFlex_prev(posW.xyz);
      return mul(posW, ksViewProjPrev);
    }
  #endif /* CREATE_MOTION_BUFFER */

  // Skinned

  float4 __toScreenSpaceSkinned(float4 posL, float3 normalL, float3 tangentPacked, float4 boneWeights, float4 boneIndices, 
      out float4 posW, out float4 posWnoflex, out float3 normalW, out float3 tangentW, out float4 posV SPS_VS_TOSS_ARG){
    ALTER_POSL(posL.xyz, false);

    posW = 0;
    normalW = 0;
    tangentW = 0;

    float totalWeight = 0;
    for (int i = 0; i < 4; i++){
      float weight = boneWeights[i];
      if (SKINNED_WEIGHT_CHECK(weight)){
        uint index = (uint)boneIndices[i];
        float4x4 bone = bones[index];
        posW += mul(bone, posL) * weight;
        normalW += mul((float3x3)bone, normalL) * weight;
        tangentW += mul((float3x3)bone, vsLoadTangent(tangentPacked)) * weight;
        totalWeight += weight;
      }
    }

    posW /= totalWeight;
    normalW /= totalWeight;
    tangentW /= totalWeight;
    
    posW.xyz += ksWorld[3].xyz;
    posW.w = 1;

    // posW = mul(posL, ksWorld);
    alterPosW(posL.xyz, false, posW);

    posWnoflex = posW;
    APPLY_CFX(posW);
    posV = mul(posW, ksView);
    return mul(posV, ksProjection);
  }

  #define toScreenSpaceSkinned(posL, normalL, tangentPacked, boneWeights, boneIndices, posW, posWnoflex, normalW, tangentW, posV)\
    __toScreenSpaceSkinned(posL, normalL, tangentPacked, boneWeights, boneIndices, posW, posWnoflex, normalW, tangentW, posV SPS_VS_TOSS_PASS)
#endif /* !EXCLUDE_PEROBJECT_CB */

#ifdef CREATE_MOTION_BUFFER
  float4 getMotionStatic(float4 posW){
    return mul(posW, ksViewProjPrev);
  }
#endif