#ifndef _SPSUTILS_FX_
#define _SPSUTILS_FX_

#ifdef USE_SPS
  #if !defined(ALLOW_SPS) && defined(OBJECT_SHADER)
    // #error wrong configuration
  #endif

  #define SPS_VS_ARG_CD , out float clipDistance : SV_ClipDistance
  #define SPS_VS_ARG , uint instanceID : SV_InstanceID, out float clipDistance : SV_ClipDistance
  #define SPS_VS_TOSS_ARG , uint instanceID
  #define SPS_VS_TOSS_PASS , instanceID

  #ifdef WINDSCREEN_REPROJECTION_CRAZE
    #define SPS_RET(v) return v;
  #elif defined(SPS_NO_POSC)
    #define SPS_RET(v) float3 ___posC = 0; spsAlterPosH(instanceID, v.PosH, ___posC, clipDistance); return v;
  #else
    #define SPS_RET(v) spsAlterPosH(instanceID, v.PosH, v.PosC, clipDistance); return v;
  #endif

  cbuffer _cbSPS : register(b8) {
    float4x4 spsView;
    float4x4 spsProjection;
    float3 spsCameraPos;
  }

  void spsAlterPosH(uint instanceID, inout float4 posH, inout float3 posC, out float clipDistance) {
    if (instanceID) {
      posH.x -= posH.w;
      posH.x /= 2;
      clipDistance = -posH.x;
      posC -= spsCameraPos;
    } else {
      posH.x += posH.w;
      posH.x /= 2;
      clipDistance = posH.x;
    }
  }

  #define SPS_CAMERA_POS (instanceID ? ksCameraPosition + spsCameraPos : ksCameraPosition)

  #undef DISCARD_VERTEX
  #define DISCARD_VERTEX(TYPE) { TYPE vout = (TYPE)0; vout.PosH = -10; clipDistance = -1; return vout; }

  #define ksView (instanceID ? spsView : ksView_base)
  #define ksProjection (instanceID ? spsProjection : ksProjection_base)
#else
  #define SPS_VS_ARG_CD 
  #define SPS_VS_ARG 
  #define SPS_VS_TOSS_ARG 
  #define SPS_VS_TOSS_PASS 
  #define SPS_RET(v) return v;
  #define ksView ksView_base
  #define ksProjection ksProjection_base
  #define SPS_CAMERA_POS ksCameraPosition
#endif

#endif