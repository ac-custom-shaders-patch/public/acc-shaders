#ifndef _BASE_INCLUDE_VS_FX_
#define _BASE_INCLUDE_VS_FX_

#include "_flags.fx"

#include "include/common.hlsl"
#include "sps_utils.fx"
#include "structs_vs.fx"
#include "structs_ps.fx"
#include "cbuffers_vs.fx"

#ifdef MODE_BAKED
#include "cbuffers_baked.fx"
#endif

#include "utils_vs.fx"
#include "../ext_vao/_include_vs.fx"

#ifdef ALLOW_RAINFX
  #include "rainFX.hlsl"
#else
  #define RAINFX_VERTEX(X)
#endif

#ifdef ALLOW_TREE_SHADOWS
  #define PS_TREE_INPUT PS_IN_PerPixel
#else
  #define PS_TREE_INPUT PS_IN_Tree
#endif

#ifdef CREATE_MOTION_BUFFER
  #define STORE_CS_POS(x) (x).xyw

  #ifndef ZERO_MOTION_BUFFER
    #define GENERIC_PIECE_MOTION(x)\
      vout.PosCS0 = STORE_CS_POS(vout.PosH);\
      vout.PosCS1 = STORE_CS_POS(getMotion(x, vout.PosH));
    #define GENERIC_PIECE_WORLD(x)\
      vout.PosCS0 = STORE_CS_POS(vout.PosH);\
      vout.PosCS1 = STORE_CS_POS(getMotionWorld(x, vout.PosH));
    #define GENERIC_PIECE_VELOCITY(x, vel)\
      vout.PosCS0 = STORE_CS_POS(vout.PosH);\
      vout.PosCS1 = STORE_CS_POS(getMotionVelocity(x, vel, vout.PosH));
    #define GENERIC_PIECE_STATIC(x)\
      vout.PosCS0 = STORE_CS_POS(vout.PosH);\
      vout.PosCS1 = STORE_CS_POS(getMotionStatic(x));
    #define GENERIC_PIECE_MOTION_SKINNED(x)\
      vout.PosCS0 = STORE_CS_POS(vout.PosH);\
      vout.PosCS1 = STORE_CS_POS(getMotionSkinned(x, posW, vin.BoneWeights, vin.BoneIndices, vout.PosH));
  #else
    #define CS_POS_ZERO STORE_CS_POS(float4(0, 0, 0, 1))
    #define GENERIC_PIECE_MOTION(x) vout.PosCS0 = CS_POS_ZERO; vout.PosCS1 = CS_POS_ZERO;
    #define GENERIC_PIECE_WORLD(x) vout.PosCS0 = CS_POS_ZERO; vout.PosCS1 = CS_POS_ZERO;
    #define GENERIC_PIECE_STATIC(x) vout.PosCS0 = CS_POS_ZERO; vout.PosCS1 = CS_POS_ZERO;
    #define GENERIC_PIECE_VELOCITY(x) vout.PosCS0 = CS_POS_ZERO; vout.PosCS1 = CS_POS_ZERO;
    #define GENERIC_PIECE_MOTION_SKINNED(x) vout.PosCS0 = CS_POS_ZERO; vout.PosCS1 = CS_POS_ZERO;
  #endif
#else
  #define GENERIC_PIECE_MOTION(x) 
  #define GENERIC_PIECE_WORLD(x) 
  #define GENERIC_PIECE_STATIC(x) 
  #define GENERIC_PIECE_VELOCITY(x) 
  #define GENERIC_PIECE_MOTION_SKINNED(x) 
#endif

#ifdef SPS_NO_POSC
  #define OPT_STRUCT_FIELD_POSC(x)
#else
  #define OPT_STRUCT_FIELD_POSC(x) x
#endif

#ifdef MODE_SURFACESAMPLE
  cbuffer _cbData : register(b7) {
    float4x4 gWorldToCar;
    float3 gAABBMin;
    float gPad0;
    float3 gAABBMax;
    float gPad1;
  }

  float3 getPosRel(float3 posW){
    float3 ret = (mul(float4(posW, 1), gWorldToCar).xyz - gAABBMin) / (gAABBMax - gAABBMin);
    ret.xz = ret.xz * 2 - 1;
    return ret;
  }

    // vout.PosH.xy = (dir.xz) / max(abs(dir.x), abs(dir.z)) * float2(-1, 1) * (1 - dir.y);\

  #define VERTEX_POSTPROCESS(vout) {\
    float3 posRel = getPosRel(posW.xyz);\
    float3 dir = normalize(posRel);\
    vout.PosH.xy = normalize(dir.xz) * float2(-1, 1) * (1 - dir.y);\
    vout.PosH.z = 1 / (1 + length(posRel));\
    vout.PosH.w = 1;\
    OPT_STRUCT_FIELD_POSC(vout.PosC = posRel); }
  
#elif defined(WINDSCREEN_REPROJECTION_CRAZE)

  #include "include/samplers_vs.hlsl"
  #include "include/normal_encode.hlsl"
  cbuffer _cbExtWindscreenReprojection : register(b10) {
    float3 gShotDir;
    float gDepthOffset;
    float4x4 gCarToWorld;
    float4x4 gWorldToUV;
    float3 gDepthPos;
    float gCurvatureFactor;
  }

  Texture2D txWindscreenSurface : register(TX_SLOT_MAT_6);

  void windscreenReprojectionCraze(inout float4 posH
      #ifndef SPS_NO_POSC
      , inout float3 posC
      #endif
      , out float doffset
      , float4 posW
      #ifdef USE_SPS
      , uint instanceID
      , out float clipDistance
      #endif
      ){
    float4 origPosW = posW;
    float4 origUV = mul(float4(posW.xyz, 1), gWorldToUV);
    float4 surfaceData = txWindscreenSurface.SampleLevel(samPointClamp, origUV.xy, 0);
    float3 normalW = mul(surfaceData.xyz, (float3x3)gCarToWorld);
    float3 origNormalW = normalW;
    float distanceToWindscreen = (origUV.z - surfaceData.w) * 2;
    
    {
      for (int i = 0; i < 16; ++i) {
        float3 posOnWindscreen = posW.xyz - normalW * distanceToWindscreen;
        float3 reflected = reflect(posOnWindscreen - origPosW.xyz, normalW);
        float3 target = normalize(ksCameraPosition.xyz - posOnWindscreen) * length(reflected);
        posW.xyz += (target - reflected) * gCurvatureFactor;
        
        float4 uv = mul(float4(posW.xyz, 1), gWorldToUV);
        float4 sam = txWindscreenSurface.SampleLevel(samPointClamp, uv.xy, 0);
        float3 normalW2 = normalize(normalW + mul(sam.xyz, (float3x3)gCarToWorld));
        distanceToWindscreen /= dot(normalW, normalW2);
        normalW = normalW2;
      }
      posW.xyz = origPosW.xyz - normalW * (distanceToWindscreen - gDepthOffset) * 2;
    }

    posH = mul(mul(posW, ksView), ksProjection);
    doffset = saturate(1 - distanceToWindscreen * 4);

    if (dot(gDepthPos, 1)) {        
      float distanceToWindscreen = dot(origPosW.xyz - gDepthPos, gShotDir);
      float3 nearestOnWindow = origPosW.xyz - gShotDir * distanceToWindscreen * 2;
      float4 posHAlt = mul(mul(float4(nearestOnWindow, 1), ksView), ksProjection);
      posH.z = posHAlt.z / posHAlt.w * posH.w;
    } else {
      posH.z = max(0, distanceToWindscreen * 0.01 + 0.0005) * posH.w;
    }

    if (any(abs(origUV * 2 - 1) > 1.2)) {
      posH.z = 1e3;
    }

    #ifdef USE_SPS
      #ifdef SPS_NO_POSC
        float3 posC = 0;
      #endif
      spsAlterPosH(instanceID, posH, posC, clipDistance);
    #endif

    // float3 nearestOnWindow = origPosW.xyz - normalW * distanceToWindscreen;
    // float3 reflectedCameraPosition = nearestOnWindow + reflect(ksCameraPosition.xyz - nearestOnWindow, normalW);
    // OPT_STRUCT_FIELD_POSC(posC = origPosW.xyz - reflectedCameraPosition);
    OPT_STRUCT_FIELD_POSC(posC = normalW * length(posC));
  }

  #ifndef WINDSCREEN_OUTPUT_FADING
    #define WINDSCREEN_USE_POSCREAL
    #define WINDSCREEN_OUTPUT_FADING vout.DepthOffset
  #endif

  #ifdef USE_SPS
    #define SPS_IID , instanceID, clipDistance
  #else
    #define SPS_IID 
  #endif

  #ifdef SPS_NO_POSC
    #define VERTEX_POSTPROCESS(vout) windscreenReprojectionCraze(vout.PosH, WINDSCREEN_OUTPUT_FADING, posW SPS_IID)
  #elif defined(WINDSCREEN_USE_POSCREAL)
    #define VERTEX_POSTPROCESS(vout) vout.PosCReal = vout.PosC; windscreenReprojectionCraze(vout.PosH, vout.PosC, WINDSCREEN_OUTPUT_FADING, posW SPS_IID)
  #else
    #define VERTEX_POSTPROCESS(vout) windscreenReprojectionCraze(vout.PosH, vout.PosC, WINDSCREEN_OUTPUT_FADING, posW SPS_IID)
  #endif

#else
  #define VERTEX_POSTPROCESS(vout)
#endif

#define GENERIC_PIECE_NOSHADOWS(_OUTPUTTYPE)\
  _OUTPUTTYPE vout;\
  float4 posW, posWnoflex, posV;\
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);\
  vout.NormalW = normals(vin.NormalL);\
  OPT_STRUCT_FIELD_POSC(vout.PosC = posW.xyz - ksCameraPosition.xyz);\
  vout.Tex = vin.Tex;\
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV))\
  GENERIC_PIECE_MOTION(vin.PosL);\
  PREPARE_AO(vout.Ao);

#define GENERIC_PIECE(_OUTPUTTYPE)\
  GENERIC_PIECE_NOSHADOWS(_OUTPUTTYPE);\
  shadows(posW, SHADOWS_COORDS);

#ifdef NO_NORMALMAPS
  #define PREPARE_TANGENT
  #define GENERIC_PIECE_NM(_OUTPUTTYPE)\
    GENERIC_PIECE(_OUTPUTTYPE);
#else
  #define PREPARE_TANGENT \
    vout.TangentW = normals(vsLoadTangent(vin.TangentPacked));\
    vout.BitangentW = bitangent(vin.NormalL, vsLoadTangent(vin.TangentPacked))
  #define GENERIC_PIECE_NM(_OUTPUTTYPE)\
    GENERIC_PIECE(_OUTPUTTYPE);\
    PREPARE_TANGENT;
#endif

#ifdef MODE_SHADOWS_ADVANCED
  struct PS_IN_shadows {
    float4 PosH : SV_POSITION;
    float2 Tex : TEXCOORD;
  };

  PS_IN_shadows _convertToShadows(float4 posH, float2 tex){
    PS_IN_shadows ret;
    ret.PosH = posH;
    ret.Tex = tex;
    return ret;
  }

  #define GENERIC_RETURN_TYPE(_RET_TYPE) PS_IN_shadows
  #define GENERIC_PIECE_RETURN(_RET) _convertToShadows(_RET.PosH, _RET.Tex)
#else
  #define GENERIC_RETURN_TYPE(_RET_TYPE) _RET_TYPE
  #define GENERIC_PIECE_RETURN(_RET) _RET
#endif

#endif