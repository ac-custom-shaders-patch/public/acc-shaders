#ifndef _BASE_CBUFFERS_HLSL_
#define _BASE_CBUFFERS_HLSL_

#if defined(OBJECT_SHADER) && !defined(CUSTOM_MATERIAL_PROPERTIES)
#if defined(CARPAINT_NM_UVMULT)

  #define CB_MATERIAL_EXTRA_1\
    float diffuseMult;\
    float normalMult;

#endif

#if defined(INCLUDE_MULTILAYER_CB)

  #define CB_MATERIAL_EXTRA_0\
    float multR;\
    float multG;\
    float multB;\
    float magicMult;\
    float2 multA;\
    PARAM_CSP(float, seasonAutumn);\
    PARAM_CSP(float, seasonWinter);

#elif defined(INCLUDE_MULTILAYER2_CB)

  #define CB_MATERIAL_EXTRA_0\
    float2 multR;\
    float2 multG;\
    float2 multB;\
    float2 multA;\
    float magicMult;\
    PARAM_CSP(float, seasonAutumn);\
    PARAM_CSP(float, seasonWinter);

#elif defined(INCLUDE_MULTILAYER3_CB)

  #define CB_MATERIAL_EXTRA_0\
    float multR;\
    float multG;\
    float multB;\
    float magicMult;\
    float2 multA;\
    float fresnelC;\
    float fresnelEXP;\
    float fresnelMaxLevel;\
    float tarmacSpecularMultiplier;\
    float2 detailNMMult;\
    float mirageMult;\
    PARAM_CSP(float, seasonAutumn);\
    PARAM_CSP(float, seasonWinter);\
    float extColoredReflection;\
    float extColoredReflectionNorm;

#elif defined(INCLUDE_GRASS_CB)

  #define CB_MATERIAL_EXTRA_0\
    float2 scale;\
    float gain;\
    float boh;\
    PARAM_CSP(float, seasonAutumn);\
    PARAM_CSP(float, seasonWinter);

#elif defined(INCLUDE_TYRE_CB)

  #if defined(USE_BRAKE_DISC_MODE)
    #define TYRE_OR_BRAKEDISC_VAR glowLevel
  #else
    #define TYRE_OR_BRAKEDISC_VAR dirtyLevel
  #endif

  #define CB_MATERIAL_EXTRA_0\
    float blurLevel;\
    float TYRE_OR_BRAKEDISC_VAR;\
    float fresnelC;\
    float fresnelEXP;\
    float isAdditive;\
    float fresnelMaxLevel;
  #define nmObjectSpace false

#elif !defined(NO_CARPAINT)

  #if defined(CARPAINT_SIMPLE)

    #define CB_MATERIAL_EXTRA_0\
      float fresnelC;\
      float fresnelEXP;\
      float fresnelMaxLevel;\
      float extColoredReflection;\
      float extColoredReflectionNorm;\
      float isAdditive;

  #elif defined(CARPAINT_NM)
  
    #define CB_MATERIAL_EXTRA_0\
      float fresnelC;\
      float fresnelEXP;\
      float fresnelMaxLevel;\
      float extColoredReflection;\
      float extColoredReflectionNorm;\
      float nmObjectSpace;\
      float isAdditive;

  #elif defined(CARPAINT_NM_UVMULT)
  
    #define CB_MATERIAL_EXTRA_0\
      float fresnelC;\
      float fresnelEXP;\
      float fresnelMaxLevel;\
      float extColoredReflection;\
      float extColoredReflectionNorm;

  #elif defined(CARPAINT_SKINNED_NM)

    #define CB_MATERIAL_EXTRA_0\
      float fresnelC;\
      float fresnelEXP;\
      float fresnelMaxLevel;\
      float isAdditive;\
      float useDetail;\
      float detailUVMultiplier;\
      float detailNormalBlend;

  #elif defined(CARPAINT_SKINNED)
  
    #define CB_MATERIAL_EXTRA_0\
      float fresnelC;\
      float fresnelEXP;\
      float fresnelMaxLevel;\
      float nmObjectSpace;\
      float isAdditive;\
      float useDetail;\
      float detailUVMultiplier;

  #elif defined(CARPAINT_SIMPLE_REFL)

    #define CB_MATERIAL_EXTRA_0\
      float fresnelC;\
      float fresnelEXP;\
      float fresnelMaxLevel;\
      float nmObjectSpace;\
      float isAdditive;\
      float useDetail;\
      float detailUVMultiplier;\
      float shadowBiasMult;

  #elif defined(CARPAINT_AT_NMDETAILS)

    #define CB_MATERIAL_EXTRA_0\
      float fresnelC;\
      float fresnelEXP;\
      float fresnelMaxLevel;\
      float extColoredReflection;\
      float extColoredReflectionNorm;\
      float isAdditive;\
      float useDetail;\
      float detailUVMultiplier;\
      float shadowBiasMult;\
      float normalUVMultiplier;\
      float detailNormalBlend;

  #elif defined(CARPAINT_NMDETAILS)
  
    #define CB_MATERIAL_EXTRA_0\
      float fresnelC;\
      float fresnelEXP;\
      float fresnelMaxLevel;\
      float extColoredReflection;\
      float extColoredReflectionNorm;\
      float isAdditive;\
      float useDetail;\
      float detailUVMultiplier;\
      float shadowBiasMult;\
      float detailNormalBlend;

  #elif defined(CARPAINT_AT)
  
    #define CB_MATERIAL_EXTRA_0\
      float fresnelC;\
      float fresnelEXP;\
      float fresnelMaxLevel;\
      float extColoredReflection;\
      float extColoredReflectionNorm;\
      float nmObjectSpace;\
      float isAdditive;\
      float useDetail;\
      float detailUVMultiplier;\
      float shadowBiasMult;\
      float normalUVMultiplier;

  #elif defined(CARPAINT_GLASS)
  
    #define CB_MATERIAL_EXTRA_0\
      float fresnelC;\
      float fresnelEXP;\
      float nmObjectSpace;\
      float isAdditive;\
      float fresnelMaxLevel;\
      float glassDamage;

  #elif defined(CARPAINT_DAMAGEZONESDIRT)

    #define nmObjectSpace 0

    #define CB_MATERIAL_EXTRA_0\
      float fresnelC;\
      float fresnelEXP;\
      float fresnelMaxLevel;\
      float isAdditive;\
      float4 damageZones;\
      float extColoredReflection;\
      float extColoredReflectionNorm;\
      float extColoredBaseReflection;\
      float useDetail;\
      float detailUVMultiplier;\
      float shadowBiasMult;\
      float dirt;\
      float sunSpecular;\
      float sunSpecularEXP;
    #define HAS_SUN_SPECULAR

  #else

    #define CB_MATERIAL_EXTRA_0\
      float fresnelC;\
      float fresnelEXP;\
      float fresnelMaxLevel;\
      float extColoredReflection;\
      float extColoredReflectionNorm;\
      float extColoredBaseReflection;\
      float isAdditive;\
      float useDetail;\
      float detailUVMultiplier;\
      float shadowBiasMult;\
      float nmObjectSpace;\
      float sunSpecular;\
      float sunSpecularEXP;
    #define HAS_SUN_SPECULAR

  #endif

  #ifdef EXTRA_CARPAINT_FIELDS
    #define CB_MATERIAL_EXTRA_1 EXTRA_CARPAINT_FIELDS
  #endif
  
  #define CB_MATERIAL_EXTRA_2\
    PARAM_CSP(float, seasonAutumn);\
    PARAM_CSP(float, seasonWinter);

#elif !defined(CB_MATERIAL_EXTRA_0)

  #define CB_MATERIAL_EXTRA_0\
    PARAM_CSP(float, seasonAutumn);\
    PARAM_CSP(float, seasonWinter);

#endif
#endif

#include "cbuffers_common.fx"
#endif