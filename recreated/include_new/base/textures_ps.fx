#ifndef _BASE_TEXTURES_HLSL_
#define _BASE_TEXTURES_HLSL_

#include "include/common.hlsl"

Texture2D txDiffuse : register(TX_SLOT_MAT_0);
Texture2D txVariation : register(TX_SLOT_MAT_1);
Texture2D txNormal : register(TX_SLOT_MAT_1);
Texture2D txBlur : register(TX_SLOT_MAT_2);
Texture2D txNormalBlur : register(TX_SLOT_MAT_3);
Texture2D txDirty : register(TX_SLOT_MAT_4);
Texture2D txGlow : register(TX_SLOT_MAT_4);
Texture2D txMask : register(TX_SLOT_MAT_1);
Texture2D txDetailR : register(TX_SLOT_MAT_2);
Texture2D txDetailG : register(TX_SLOT_MAT_3);
Texture2D txDetailB : register(TX_SLOT_MAT_4);
Texture2D txDetailA : register(TX_SLOT_MAT_5);
Texture2D txMaps : register(TX_SLOT_MAT_2);
Texture2D txDetail : register(TX_SLOT_MAT_3);
Texture2D txNormalDetail : register(TX_SLOT_MAT_4);
Texture2D txDamage : register(TX_SLOT_MAT_4);
Texture2D txDust : register(TX_SLOT_MAT_5);
Texture2D txDetailNM : register(TX_SLOT_MAT_6);
Texture2D txDamageMask : register(TX_SLOT_MAT_6);

#ifdef MODE_KUNOS

  Texture2D<float> txShadow0 : register(TX_SLOT_SHADOW_0);
  Texture2D<float> txShadow1 : register(TX_SLOT_SHADOW_1);
  Texture2D<float> txShadow2 : register(TX_SLOT_SHADOW_2);
  Texture2D txNoise : register(TX_SLOT_NOISE);

#else

  #ifndef NO_SHADOWS
    Texture2DArray<float> txShadowArray : register(TX_SLOT_SHADOW_ARRAY);
  #endif

  #ifndef TXDEPTH_DEFINED
    #define TXDEPTH_DEFINED
    Texture2D<float> txDepth : register(TX_SLOT_DEPTH);
  #endif

  #ifndef TXNOISE_DEFINED
    #define TXNOISE_DEFINED
    Texture2D txNoise : register(TX_SLOT_NOISE);
  #endif

  Texture2D txAO : register(TX_SLOT_AO);
  Texture2D txPrevFrame : register(TX_SLOT_PREV_FRAME);
  Texture2D<float> txCloudShadow : register(TX_SLOT_SHADOW_CLOUDS);
  Texture2D txColorShadow : register(TX_SLOT_SHADOW_COLOR);
  Texture2D<float> txMirageMask : register(TX_SLOT_MIRAGE_MASK);
#endif

TextureCube txCube : register(TX_SLOT_REFLECTION_CUBEMAP);

#endif