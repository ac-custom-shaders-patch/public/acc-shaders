#ifndef _GAMMA_HLSL_
#define _GAMMA_HLSL_

#if \
    !defined(MODE_SIMPLEST) &&\
    !defined(MODE_PUDDLES) &&\
    !defined(MODE_GRASSFX) &&\
    !defined(MODE_EMISSIVESAMPLE) &&\
    !defined(MODE_NORMALSAMPLE) &&\
    !defined(MODE_COLORSAMPLE) &&\
    !defined(MODE_SURFACESAMPLE) &&\
    !defined(MODE_SHADOWS_HEAT) &&\
    !defined(MODE_SHADOWS_ADVANCED) 
  // #define GAMMA_FIX
#endif

#ifndef GAMMA_FIX_CONDITION
  #error define GAMMA_FIX_CONDITION
#endif

// #define GAMMA_FIX_ADAPTIVE // TODO: Disable for public release! Makes things slow

#ifndef GAMMA_EXPECTED_KS
  #define GAMMA_EXPECTED_KS 0.45
#endif

// Syncs brightness with fuPBR
#define _GAMMA_INNER_NONPBR_MULT 2.

#ifdef GAMMA_FIX
  #ifdef GAMMA_FIX_ADAPTIVE
    #define GAMMA_ALBEDO_BOOST (GAMMA_FIX_CONDITION ? _GAMMA_INNER_NONPBR_MULT/GAMMA_EXPECTED_KS : 1)
    #define GAMMA_OR(ACTIVE, INACTIVE) (GAMMA_FIX_CONDITION ? (ACTIVE) : (INACTIVE))
    #define GAMMA_OR_STAT(ACTIVE, INACTIVE) ACTIVE
    #define GAMMA_LINEAR(X) pow(max(0, X), GAMMA_FIX_CONDITION ? 2.2 : 1)
  #else
    #define GAMMA_ALBEDO_BOOST (_GAMMA_INNER_NONPBR_MULT/GAMMA_EXPECTED_KS)
    #define GAMMA_OR(ACTIVE, INACTIVE) (ACTIVE)
    #define GAMMA_OR_STAT(ACTIVE, INACTIVE) ACTIVE
    #define GAMMA_LINEAR(X) pow(max(0, X), 2.2)
  #endif

  #ifdef MODE_GRASSFX
    #define GAMMA_MULT_ADJ(X) (X)
  #else
    #define GAMMA_MULT_ADJ(X) (X * GAMMA_ALBEDO_BOOST)
  #endif
#else
  #define GAMMA_ALBEDO_BOOST 1.
  #define GAMMA_OR(ACTIVE, INACTIVE) (INACTIVE)
  #define GAMMA_OR_STAT(ACTIVE, INACTIVE) INACTIVE
  #define GAMMA_LINEAR(X) (X)
  #define GAMMA_MULT_ADJ(X) (X)
#endif

#define GAMMA_FINAL_MULT 1
#define GAMMA_DYNAMIC_LIGHTS_MULT GAMMA_OR(GAMMA_FIX_CONDITION, 1)
#define GAMMA_NONPBR_MULT GAMMA_OR(_GAMMA_INNER_NONPBR_MULT, 1)
#define GAMMA_SCREENLIGHT_MULT GAMMA_OR(0.05, 1)
#define GAMMA_ALPHA(X) GAMMA_OR(pow(saturate(X), 1.0001 - saturate(X)), X)
#define GAMMA_FIX_ACTIVE GAMMA_OR(true, false)
#define GAMMA_ACTUAL_VALUE GAMMA_OR(2.2, 1.0)
#define GAMMA_BLINNPHONG_EXP(X) GAMMA_OR(1.6 * (X), X)
#define GAMMA_BLINNPHONG_ADJ(X) GAMMA_OR(1.6 * (X), X)
#define GAMMA_VAO_EXP(X) GAMMA_OR(2.2 * (X), X)
#define GAMMA_VAO_EXP_INV(X) GAMMA_OR((X) / 2.2, X)
#define GAMMA_VAO_INV(X) GAMMA_OR(pow(saturate(X), 0.4545), X)
#define GAMMA_GENERIC_EXP(X) GAMMA_OR(2.2 * (X), X)
#define GAMMA_LINEAR_SIMPLE(X) GAMMA_OR(pow(max(0, X), 2), X)
#define GAMMA_KSEMISSIVE(X) GAMMA_LINEAR(X)

float3 applyGammaPBR(float3 txDiffuseValue, float inputK) {
  if (GAMMA_FIX_ACTIVE) txDiffuseValue = GAMMA_LINEAR(txDiffuseValue * (inputK / GAMMA_EXPECTED_KS));
  return txDiffuseValue;
}

float applyGamma(float txDiffuseValue, float inputK) {
  if (GAMMA_FIX_ACTIVE) txDiffuseValue = GAMMA_LINEAR(txDiffuseValue * GAMMA_MULT_ADJ(inputK));
  return txDiffuseValue;
}

float3 applyGamma(float3 txDiffuseValue, float inputK) {
  if (GAMMA_FIX_ACTIVE) txDiffuseValue = GAMMA_LINEAR(txDiffuseValue * GAMMA_MULT_ADJ(inputK));
  return txDiffuseValue;
}

float4 applyGamma(float4 txDiffuseValue, float inputK) {
  return float4(applyGamma(txDiffuseValue.rgb, inputK), txDiffuseValue.w);
}

float3 applyGamma(float3 lightDiffuse, float3 lightAmbient, float3 txDiffuseValue, float multDiffuse, float multAmbient) {
  #ifdef GAMMA_FIX
    #ifdef GAMMA_FIX_ADAPTIVE
      if (GAMMA_FIX_CONDITION)
    #endif
    return lightDiffuse * applyGamma(txDiffuseValue, multDiffuse) + lightAmbient * applyGamma(txDiffuseValue, multAmbient);
    #ifdef GAMMA_FIX_ADAPTIVE
      return (lightDiffuse + lightAmbient) * txDiffuseValue;
    #endif
  #else
    return (lightDiffuse + lightAmbient) * txDiffuseValue;
  #endif
}

#endif