#ifndef _FLAGS_HLSL_
#define _FLAGS_HLSL_

#include "_texture_slots.fx"

#define ALLOW_CLEARCOAT

#ifdef MODE_KUNOS

  #define USE_MULTIMAP_TEXTURE_VARIATION
  #define NO_ADJUSTING_COLOR
  #define NO_WIND
  #define NO_DEPTH_MAP

#elif defined(MODE_GBUFFER)

  #define GBUFFER_SHADERS_SET
  #define GBUFFER_NORMALS_REFLECTION
  #define ALLOW_CAR_FLEX
  #define ALLOW_TYRESFX
  #define ALLOW_EXTRA_SHADOW
  #define ALLOW_BRAKEDISCFX
  // #define WINDSCREEN_RAIN
  #define ALLOW_PBR_EXTRAS_BASE
  #define ALLOW_PBR_EXTRAS_PLUS
  #define ALLOW_PARALLAX
  #define USE_MULTIMAP_TEXTURE_NM_VARIATION // allow tiling fixing, but only if it affects normals
  #define USE_CPL
  // #define USE_GLOBAL_EMISSIVE_MULT
  #define CREATE_MOTION_BUFFER
  #define FOG_MULT_MODE
  #define ADVANCED_WATER
  #define ALLOW_FUR
  // #define ALLOW_DITHER_FADING // extra logic for modes in _gbuff.fx
  // #define ADVANCED_GLASS

  #ifndef GBUFFER_FORCE_SHADOWS
    #define NO_LIGHTING
    #define NO_SHADOWS
  #endif
  
  #define EXTENDED_PEROBJECT_CB
  
  #define USE_REFLECTION_BLUR_MULT
  #define ALLOW_PERVERTEX_AO
  // #define ALLOW_PERVERTEX_AO_DEBUG
  // #define ALLOW_PERVERTEX_AO_DEBUG__NOREFL
  #define ALLOW_EXTRA_FEATURES
  #define MICROOPTIMIZATIONS
  #define OPTIMIZE_FOG
  #define OPTIMIZE_SKY

#elif defined(MODE_MAIN_FX) || defined(MODE_MAIN_NOFX) || defined(MODE_MAIN_FX_NOSHADOWS) || defined(MODE_MAIN_NOFX_NOSHADOWS) || defined(MODE_MAIN_FX_DISCSHADOWS) || defined(MODE_MAIN_NOFX_DISCSHADOWS)

  #if defined(MODE_MAIN_FX) || defined(MODE_MAIN_FX_NOSHADOWS) || defined(MODE_MAIN_FX_DISCSHADOWS)
    #define ALLOW_LIGHTINGFX
    #define ALLOW_LIGHTINGFX_BACKLIT
  #endif

  #if defined(MODE_MAIN_FX_NOSHADOWS) || defined(MODE_MAIN_NOFX_NOSHADOWS)
    #define NO_SHADOWS
  #endif

  #if defined(MODE_MAIN_FX_DISCSHADOWS) || defined(MODE_MAIN_NOFX_DISCSHADOWS)
    #define USE_DISC_SHADOWS
  #endif

  // #if !defined(ALLOW_DYNAMIC_SHADOWS)
  //   #define ENSURE_VARS_USED
  // #endif

  // #define FIX_LIGHT_MIXING

  #define MAIN_SHADERS_SET
  #define ALLOW_SPS
  #define ALLOW_VRS_FIX
  #define ALLOW_CAR_FLEX
  #define ALLOW_TYRESFX
  #define ALLOW_BRAKEDISCFX
  #define ALLOW_SH_AMBIENT
  #define ALLOW_EXTRA_LIGHTING_MODELS
  #define ALLOW_EXTRA_SHADOW
  #define ALLOW_TREE_SHADOWS
  #define ALLOW_VERTEX_ADJUSTMENTS
  #define ALLOW_SSAO
  #define ALLOW_DISTANT_FIX
  #define ALLOW_PARALLAX
  #define ALLOW_BOUNCEBACK_LIGHTING
  #define ALLOW_PBR_EXTRAS_BASE
  #define ALLOW_PBR_EXTRAS_PLUS
  #define ALLOW_CUSTOM_A2C
  #define ALLOW_EXTRA_VISUAL_EFFECTS
  #define ALLOW_FUR
  // #define ALLOW_DITHER_FADING // extra logic for modes in _gbuff.fx
  #define ADVANCED_FAKE_SHADOWS
  #define ADVANCED_WATER
  #define ADVANCED_WATER_SHADING
  #define ADVANCED_GLASS
  #define BLUR_SHADOWS
  #define WINDSCREEN_RAIN
  #define WINDSCREEN_ADVANCED_REFLECTION
  #define USE_MULTIMAP_TEXTURE_VARIATION
  #define USE_MULTIMAP_TEXTURE_NM_VARIATION
  #define USE_CPL
  #define USE_GLOBAL_EMISSIVE_MULT
  #define USE_REFLECTION_BLUR_MULT
  #define EXTENDED_PEROBJECT_CB
  #define USE_BACKLIT_FOG
  #define USE_AO_SPLITTING
  #define USE_LAMBERT_GAMMA
  #define SHADOWS_CASCADES_TRANSITION
  
  #define ALLOW_PERVERTEX_AO
  #define ALLOW_PERVERTEX_AO_DEBUG
  // #define ALLOW_PERVERTEX_AO_DEBUG__NOREFL
  #define ALLOW_EXTRA_FEATURES
  #define MICROOPTIMIZATIONS
  #define OPTIMIZE_FOG
  #define OPTIMIZE_SKY

#elif defined(MODE_WINDSCREEN_FX) || defined(MODE_WINDSCREEN_NOFX)

  // #define FIX_LIGHT_MIXING

  #define WINDSCREEN_REPROJECTION_CRAZE
  #define ALLOW_LIGHTINGFX
  #define ALLOW_LIGHTINGFX_BACKLIT
  #define ALLOW_SPS
  // #define SHADOWS_FILTER_SIZE 2

  #define USE_FIRST_CASCADE_ONLY
  #define NO_SHADOWS_CASCADES_TRANSITION

  #define STRUCT_POSH_MODIFIER centroid noperspective
  #ifdef SPS_NO_POSC
    #define CUSTOM_MODE_REGISTERS float DepthOffset : CMR0; 
  #else
    #define CUSTOM_MODE_REGISTERS float DepthOffset : CMR0; float3 PosCReal : CMR1;
    #define PIN_POSC_POSITION pin.PosCReal
  #endif
  #define NO_FOG

  // #if !defined(ALLOW_DYNAMIC_SHADOWS)
  //   #define ENSURE_VARS_USED
  // #endif

  // #define ALLOW_SPS
  // #define ALLOW_VRS_FIX
  // #define ALLOW_CAR_FLEX
  // #define ALLOW_TYRESFX
  // #define ALLOW_BRAKEDISCFX
  #define ALLOW_SH_AMBIENT
  #define ALLOW_EXTRA_LIGHTING_MODELS
  #define ALLOW_EXTRA_SHADOW
  // #define ALLOW_TREE_SHADOWS
  // #define ALLOW_VERTEX_ADJUSTMENTS
  // #define ALLOW_SSAO
  // #define ALLOW_DISTANT_FIX
  // #define ALLOW_PARALLAX
  // #define ALLOW_BOUNCEBACK_LIGHTING
  #define ALLOW_PBR_EXTRAS_BASE
  #define ALLOW_PBR_EXTRAS_PLUS
  #define ALLOW_CUSTOM_A2C
  #define ALLOW_EXTRA_VISUAL_EFFECTS
  // #define ALLOW_FUR
  // #define ALLOW_DITHER_FADING // extra logic for modes in _gbuff.fx
  // #define ADVANCED_FAKE_SHADOWS
  // #define ADVANCED_WATER
  // #define ADVANCED_WATER_SHADING
  #define ADVANCED_GLASS
  // #define BLUR_SHADOWS
  // #define WINDSCREEN_RAIN
  // #define USE_MULTIMAP_TEXTURE_VARIATION
  // #define USE_MULTIMAP_TEXTURE_NM_VARIATION
  #define USE_CPL
  #define USE_GLOBAL_EMISSIVE_MULT
  #define USE_REFLECTION_BLUR_MULT
  #define EXTENDED_PEROBJECT_CB
  // #define USE_BACKLIT_FOG
  #define USE_AO_SPLITTING
  #define USE_LAMBERT_GAMMA
  // #define SHADOWS_CASCADES_TRANSITION
  
  #define ALLOW_PERVERTEX_AO
  // #define ALLOW_PERVERTEX_AO_DEBUG
  // #define ALLOW_PERVERTEX_AO_DEBUG__NOREFL
  #define ALLOW_EXTRA_FEATURES
  #define MICROOPTIMIZATIONS
  #define OPTIMIZE_FOG
  #define OPTIMIZE_SKY

#elif defined(MODE_SIMPLIFIED_FX) || defined(MODE_SIMPLIFIED_NOFX)

  // #define FIX_LIGHT_MIXING
  
  #ifdef MODE_SIMPLIFIED_FX
    #define ALLOW_LIGHTINGFX
    #define ALLOW_TYRESFX
    #define ALLOW_CAR_FLEX
    #define USE_SMOOTH_SHADOWS
    #define USE_LAST_SHADOWS_CASCADE_ONLY
  #else
    #define NO_WIND
    #define NO_SHADOWS
  #endif

  #define DIM_NEARBY_EMISSIVES
  #define ALLOW_EXTRA_SHADOW
  #define ALLOW_DISTANT_FIX
  #define NO_DEPTH_MAP
  #define NO_REFLECTIONS_FOR_CUBEMAP
  #define NO_NORMALMAPS
  #define HEAVIER_LFX_EXTRA_SHADOW_CONTRUBUTION
  #define NO_EXTSPECULAR
  #define USE_BACKLIT_FOG
  #define USE_GLOBAL_EMISSIVE_MULT
  #define USE_LAMBERT_GAMMA
  #define SIMPLIFIED_EFFECTS
  #define EXTENDED_PEROBJECT_CB
  #define INCREASE_GLOW_DOT

  #define ALLOW_PERVERTEX_AO
  #define ALLOW_EXTRA_FEATURES
  #define ALLOW_PBR_EXTRAS_BASE
  // #define ADVANCED_GLASS
  #define MICROOPTIMIZATIONS
  #define OPTIMIZE_FOG
  #define OPTIMIZE_SKY

#elif defined(MODE_SIMPLEST)

  #define NO_SHADOWS
  #define NO_DEPTH_MAP
  #define NO_FOG
  #define NO_REFLECTIONS
  #define NO_ANISOTROPIC_FILTERING
  #define BLACK_REFLECTIONS
  #define NO_NORMALMAPS
  #define NO_WIND
  // #define NO_ADJUSTING_COLOR
  #define SIMPLIFED_SPECULARS
  #define SIMPLER_LIGHTING
  #define USE_GLOBAL_EMISSIVE_MULT
  #define EXTENDED_PEROBJECT_CB
  #define USE_OPAQUE_FIX

  #define ALLOW_PERVERTEX_AO
  #define USE_PERVERTEX_AO_AS_MULT
  #define ALLOW_EXTRA_FEATURES
  #define MICROOPTIMIZATIONS
  #define OPTIMIZE_FOG
  #define OPTIMIZE_SKY

#elif defined(MODE_PUDDLES)

  #define NO_SHADOWS
  #define NO_DEPTH_MAP
  #define NO_FOG
  #define NO_REFLECTIONS
  #define NO_ANISOTROPIC_FILTERING
  #define BLACK_REFLECTIONS
  #define NO_NORMALMAPS
  #define NO_WIND
  #define ALLOW_PERVERTEX_AO

#elif defined(MODE_SHADOWS_HEAT)

  #define SHADOWS_FILTER_SIZE 2
  #define USE_FIRST_SHADOWS_CASCADE_ONLY
  #define NO_DEPTH_MAP
  #define NO_FOG
  #define NO_REFLECTIONS
  #define NO_ANISOTROPIC_FILTERING
  #define BLACK_REFLECTIONS
  #define NO_NORMALMAPS
  #define NO_WIND
  #define ALLOW_PERVERTEX_AO

#elif defined(MODE_EMISSIVESAMPLE)

  #define NO_SHADOWS
  #define NO_DEPTH_MAP
  #define NO_FOG
  #define NO_REFLECTIONS
  #define NO_ANISOTROPIC_FILTERING
  #define BLACK_REFLECTIONS
  #define NO_NORMALMAPS
  #define NO_WIND
  // #define NO_ADJUSTING_COLOR
  #define SIMPLIFED_SPECULARS
  #define SIMPLER_LIGHTING
  #define USE_GLOBAL_EMISSIVE_MULT
  #define EXTENDED_PEROBJECT_CB
  #define USE_OPAQUE_FIX

  #define ALLOW_PERVERTEX_AO
  #define USE_PERVERTEX_AO_AS_MULT
  #define ALLOW_EXTRA_FEATURES
  #define MICROOPTIMIZATIONS
  #define OPTIMIZE_FOG
  #define OPTIMIZE_SKY

#elif defined(MODE_GRASSFX)

  #define GBUFFER_GRASSFX
  #define NO_SHADOWS
  #define NO_DEPTH_MAP
  #define NO_FOG
  #define NO_WIND
  #define NO_REFLECTIONS
  #define NO_ANISOTROPIC_FILTERING
  #define BLACK_REFLECTIONS
  // #define NO_NORMALMAPS
  // #define NO_ADJUSTING_COLOR
  #define SIMPLEST_LIGHTING
  #define USE_MULTIMAP_TEXTURE_VARIATION
  #define USE_GLOBAL_EMISSIVE_MULT
  #define EXTENDED_PEROBJECT_CB
  #define USE_OPAQUE_FIX

  #define ALLOW_PERVERTEX_AO
  #define ALLOW_EXTRA_FEATURES
  #define MICROOPTIMIZATIONS
  #define OPTIMIZE_FOG
  #define OPTIMIZE_SKY

  #undef GETNORMALW_SAMPLER
  #define GETNORMALW_SAMPLER samLinear

#elif defined(MODE_NORMALSAMPLE)

  #define GBUFFER_NORMALSAMPLE
  #define NO_SHADOWS
  #define NO_DEPTH_MAP
  #define NO_FOG
  #define NO_WIND
  #define NO_REFLECTIONS
  #define NO_ANISOTROPIC_FILTERING
  #define BLACK_REFLECTIONS
  // #define NO_NORMALMAPS
  // #define NO_ADJUSTING_COLOR
  #define SIMPLEST_LIGHTING
  #define USE_MULTIMAP_TEXTURE_VARIATION
  #define USE_GLOBAL_EMISSIVE_MULT
  #define EXTENDED_PEROBJECT_CB
  #define USE_OPAQUE_FIX

  #define ALLOW_PERVERTEX_AO
  #define ALLOW_EXTRA_FEATURES
  #define MICROOPTIMIZATIONS
  #define OPTIMIZE_FOG
  #define OPTIMIZE_SKY

  #undef GETNORMALW_SAMPLER
  #define GETNORMALW_SAMPLER samLinear

#elif defined(MODE_LIGHTMAP)

  // #define GBUFFER_GRASSFX
  #define ALLOW_LIGHTINGFX
  #define LIGHTINGFX_NOSPECULAR
  // #define NO_SHADOWS
  #define NO_DEPTH_MAP
  #define NO_FOG
  #define NO_WIND
  #define NO_REFLECTIONS
  #define NO_ANISOTROPIC_FILTERING
  #define BLACK_REFLECTIONS
  #define NO_NORMALMAPS
  // #define NO_ADJUSTING_COLOR
  // #define SIMPLEST_LIGHTING
  // #define USE_MULTIMAP_TEXTURE_VARIATION
  #define USE_GLOBAL_EMISSIVE_MULT
  #define EXTENDED_PEROBJECT_CB
  #define USE_OPAQUE_FIX

  #define ALLOW_PERVERTEX_AO
  #define ALLOW_EXTRA_FEATURES
  #define MICROOPTIMIZATIONS
  #define OPTIMIZE_FOG
  #define OPTIMIZE_SKY

  #undef GETNORMALW_SAMPLER
  #define GETNORMALW_SAMPLER samLinearSimple

#elif defined(MODE_COLORSAMPLE) || defined(MODE_SURFACESAMPLE)

  #define REDUCE_DIFFUSE_AO
  #define NO_SHADOWS
  #define NO_DEPTH_MAP
  #define NO_FOG
  #define NO_ANISOTROPIC_FILTERING
  #define NO_WIND

  #if defined(MODE_COLORSAMPLE)
    #define NO_REFLECTIONS
  #endif
  #if defined(MODE_SURFACESAMPLE)
    #define KEEP_REFLECTION_PARAMS
  #endif
  #define BLACK_REFLECTIONS
  #define KEEP_ORIGINAL_SATURATION

  // #define NO_NORMALMAPS
  // #define NO_ADJUSTING_COLOR
  #define SIMPLEST_LIGHTING
  #define USE_MULTIMAP_TEXTURE_VARIATION
  #define USE_GLOBAL_EMISSIVE_MULT
  #define EXTENDED_PEROBJECT_CB
  #define USE_OPAQUE_FIX
  
  #define ALLOW_PERVERTEX_AO
  #define ALLOW_EXTRA_FEATURES
  #define MICROOPTIMIZATIONS
  #define OPTIMIZE_FOG
  #define OPTIMIZE_SKY

  #undef GETNORMALW_SAMPLER
  #define GETNORMALW_SAMPLER samLinear

#elif defined(MODE_COLORMASK)

  #define ALLOW_CAR_FLEX
  #define REDUCE_DIFFUSE_AO

  #ifdef COLORMARK_SEETHROUGH
    #define SHADOWS_FILTER_SIZE 1
  #else
    #define NO_SHADOWS
  #endif

  #define NO_DEPTH_MAP
  #define NO_FOG
  #define NO_REFLECTIONS
  #define NO_ANISOTROPIC_FILTERING
  #define NO_WIND
  #define BLACK_REFLECTIONS
  // #define NO_NORMALMAPS
  // #define NO_ADJUSTING_COLOR
  #define SIMPLEST_LIGHTING
  #define USE_MULTIMAP_TEXTURE_VARIATION
  #define USE_GLOBAL_EMISSIVE_MULT
  #define EXTENDED_PEROBJECT_CB

  #define ALLOW_EXTRA_FEATURES
  #define MICROOPTIMIZATIONS
  #define OPTIMIZE_FOG
  #define OPTIMIZE_SKY

  #undef GETNORMALW_SAMPLER
  #define GETNORMALW_SAMPLER samLinear

#elif defined(MODE_SHADOWS_ADVANCED)

  #define NO_SHADOWS
  #define NO_DEPTH_MAP
  #define NO_FOG
  #define NO_REFLECTIONS
  #define NO_ANISOTROPIC_FILTERING
  #define BLACK_REFLECTIONS
  #define NO_NORMALMAPS
  #define NO_WIND
  #define NO_OPTIONAL_MAPS
  #define ALLOW_PERVERTEX_AO
  #define SIMPLEST_LIGHTING
  #define USE_MULTIMAP_TEXTURE_VARIATION
  #define EXTENDED_PEROBJECT_CB

  #define ALLOW_EXTRA_FEATURES
  #define MICROOPTIMIZATIONS
  #define OPTIMIZE_FOG
  #define OPTIMIZE_SKY

  #undef GETNORMALW_SAMPLER
  #define GETNORMALW_SAMPLER samLinearSimple

#endif

#endif

#if defined(FIX_LIGHT_MIXING) && !defined(ACTUAL_PBR)
  #define LFM_CONTRIB(X) pow((X), 2)
  #define LFM_SUM(X) sqrt((X))
#else
  #define LFM_CONTRIB(X) (X)
  #define LFM_SUM(X) (X)
#endif
