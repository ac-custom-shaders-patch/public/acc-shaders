#ifndef _BASE_INCLUDE_PS_FX_
#define _BASE_INCLUDE_PS_FX_

#include "_flags.fx"

#ifndef PIN_POSC_POSITION
  #define PIN_POSC_POSITION pin.PosC
#endif

#include "common_ps.fx"
#include "include/common.hlsl"
#include "samplers_ps.fx"
#include "textures_ps.fx"
#include "cbuffers_ps.fx"

#if defined(MODE_COLORSAMPLE) || defined(MODE_SURFACESAMPLE)
  #define SIMPLEST_LIGHTING_FN(albedo) ((albedo) * ((INPUT_AMBIENT_K + INPUT_DIFFUSE_K) / 2))
#elif defined(MODE_GRASSFX)
  // #define SIMPLEST_LIGHTING_FN(albedo) (albedo)
  #define SIMPLEST_LIGHTING_FN(albedo) ((albedo) * INPUT_AMBIENT_K)
  // #define SIMPLEST_LIGHTING_FN(albedo) ((albedo) * INPUT_DIFFUSE_K)
#else
  #define SIMPLEST_LIGHTING_FN(albedo) ((albedo) * INPUT_AMBIENT_K)
#endif

#ifdef MODE_BAKED
  #include "cbuffers_baked.fx"
#endif

#include "structs_ps.fx"

#include "../ext_lighting_models/_include_ps.fx"

#if !defined(MODE_KUNOS) && !defined(NO_SHADOWS) && !( defined(SUPPORTS_BLUR_SHADOWS) && defined(BLUR_SHADOWS) )
  #ifndef SHADOWS_COORDS
    #define SHADOWS_COORDS pin.ShadowTex0
  #endif
  #include "../ext_shadows/_include_ps.fx"
#endif

#ifdef ALLOW_RAINFX
  #include "rainFX.hlsl"
#else
  #define RAINFX_INIT ;
  #define RAINFX_WET(X) ;
  #define RAINFX_WET_FOLIAGE(X) ;
  #define RAINFX_WET_DIM(X) ;
  #define RAINFX_WET_DIRT(X) ;
  #define RAINFX_SHINY(X) ;
  #define RAINFX_SHINY_PBR(X) ;
  #define RAINFX_REFLECTIVE_SKIP(X) ;
  #define RAINFX_REFLECTIVE_FOLIAGE(X) ;
  #define RAINFX_REFLECTIVE(X) ;
  #define RAINFX_WATER(X) ;
  #define RAINFX_WATER_ALPHA(X, Y) ;
  #define RAINFX_REFLECTIVE_WATER_ROUGH(X) ReflParams R = getReflParamsZero();
#endif


#ifdef MODE_SHADOWS_ADVANCED
  struct PS_IN_shadows {
    float4 PosH : SV_POSITION;
    float2 Tex : TEXCOORD;
  };

  void _convertFromShadows(PS_IN_shadows pin, out float4 posH, out float2 tex){
    posH = pin.PosH;
    tex = pin.Tex;
  }

  #define GENERIC_INPUT_TYPE(_IN_TYPE) PS_IN_shadows _pin_shadows
  #define GENERIC_PIECE_INPUT(_IN_TYPE) _IN_TYPE pin = (_IN_TYPE)0; _convertFromShadows(_pin_shadows, pin.PosH, pin.Tex)
#else
  #define GENERIC_INPUT_TYPE(_IN_TYPE) _IN_TYPE pin
  #define GENERIC_PIECE_INPUT(_IN_TYPE) 
#endif

#ifdef NO_LIGHTING

  #define LIGHTINGFX(_X) _X.rgb = 0;
  #define LIGHTINGFX_GET ((float3)0)
  #define LIGHTINGFX_APPLY(_X, _G) ((_X).rgb)

#elif defined(ALLOW_LIGHTINGFX)

  #include "../ext_lightingfx/_include_ps.fx"

#else

  #if defined(ALLOW_PERVERTEX_AO_DEBUG) && defined(SUPPORTS_AO)
    #define LIGHTINGFX(_X) _X.rgb = extVaoMode == 4 ? IN_VAO : extVaoMode == 5 ? pin.NormalW : _X.rgb;
    #define LIGHTINGFX_GET ((float3)0)
    #define LIGHTINGFX_APPLY(_X, _G) ((_X).rgb)
  #else
    #define LIGHTINGFX(x) ;
    #define LIGHTINGFX_GET ((float3)0)
    #define LIGHTINGFX_APPLY(_X, _G) ((_X).rgb)
  #endif

#endif

#if defined(ALLOW_SSAO) && !defined(NO_SSAO)
  #define _AO_SSAO GAMMA_LINEAR_SIMPLE(1 - txAO.SampleLevel(samLinear, pin.PosH.xy * extScreenSize.zw, 0).x)
#else
  #define _AO_SSAO 1
#endif

#if defined(ALLOW_SSAO) && !defined(NO_SSGI)
  #define _AO_SSGI (txAO.SampleLevel(samLinear, pin.PosH.xy * extScreenSize.zw, 0).yzw / GAMMA_SCREENLIGHT_MULT)
#else
  #define _AO_SSGI 0
#endif

#ifndef _AO_VAO
  #if defined(ALLOW_PERVERTEX_AO) && defined(SUPPORTS_AO)
    #ifdef ALLOW_PERVERTEX_AO_DEBUG
      #define _AO_VAO (extVaoMode == 3 ? 1 : IN_VAO)
    #else
      #define _AO_VAO IN_VAO
    #endif
  #else
    #define _AO_VAO 1
  #endif
#endif

#ifndef IS_TRACK_MATERIAL
  #define IS_TRACK_MATERIAL HAS_FLAG(FLAG_IS_TRACK_MATERIAL)
#endif

#ifndef IS_INTERIOR_MATERIAL
  #define IS_INTERIOR_MATERIAL HAS_FLAG(FLAG_IS_INTERIOR_MATERIAL)
#endif

#define AO_FALLBACK_SHADOW (saturate(min(_AO_VAO, _AO_SSAO) * 3 - 1))
#define AO_LIGHTING float4((float3)min(_AO_VAO, _AO_SSAO), _AO_SSAO)
#define AO_LIGHTING_OCC(VAL) float4((float3)min(min(VAL, _AO_VAO), _AO_SSAO), _AO_SSAO)
#define AO_EXTRA_LIGHTING min(saturate(_AO_VAO * 2 + IS_TRACK_MATERIAL), _AO_SSAO)
#define AO_REFLECTION saturate(((float3)(_AO_VAO)).g * 2 + IS_TRACK_MATERIAL)  // if you want to use SSAO in here, fix reflection subtraction of SSLR
#define GI_LIGHTING _AO_SSGI * AO_EXTRA_LIGHTING * saturate(_AO_VAO + IS_TRACK_MATERIAL)

#ifdef ALLOW_TREE_SHADOWS
  #define PS_TREE_INPUT PS_IN_PerPixel
#else
  #define PS_TREE_INPUT PS_IN_Tree
#endif

#include "utils_ps.fx"
#include "utils_ps_gbuff.fx"

#ifdef ALLOW_RAINFX
  #include "rainFX_impl.hlsl"
#endif

#endif