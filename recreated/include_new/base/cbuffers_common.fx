#ifndef _BASE_CBUFFERS_COMMON_HLSL_
#define _BASE_CBUFFERS_COMMON_HLSL_

#include "include/common.hlsl"

#ifndef CB_MATERIAL_EXTRA_0
  #define CB_MATERIAL_EXTRA_0
#endif

#ifndef CB_MATERIAL_EXTRA_1
  #define CB_MATERIAL_EXTRA_1
#endif

#ifndef CB_MATERIAL_EXTRA_2
  #define CB_MATERIAL_EXTRA_2
#endif

#ifndef CB_MATERIAL_EXTRA_3
  #define CB_MATERIAL_EXTRA_3
#endif

#ifndef CB_MATERIAL_EXTRA_4
  #define CB_MATERIAL_EXTRA_4
#endif

// First two flags 1 and 2 are used for indexing of emissive mult
#define FLAG_EMISSIVE_FIXED 4
#define FLAG_ALPHA_TEST 8
#define FLAG_IS_TRACK_MATERIAL 16
#define FLAG_IS_INTERIOR_MATERIAL 32
#define FLAG_GBUFFER_ALPHA_TEST 64
#define FLAG_GBUFFER_PREMULTIPLIED_ALPHA 128
#define FLAG_RAINFX_ROUGH 256
#define FLAG_RAINFX_SOAKING 512
#define FLAG_RAINFX_NO_RUNNING_WATER 1024
#define FLAG_MATERIAL_0 2048
#define FLAG_MATERIAL_1 4096
#define FLAG_USE_CAO 8192
#define FLAG_APPLY_VRS_FIX 16384
#define FLAG_APPLY_DISTANT_FIX 32768
#define FLAG_APPLY_GRASSFX_COLOR 65536
#define FLAG_RAINFX_DROPS 131072
#define FLAG_ALPHA_S0 262144
#define FLAG_ALPHA_S1 524288
#define FLAG_MATERIAL_2 1048576
#define FLAG_DIM_EMISSIVE_IN_CUBEMAP 2097152
#define HAS_FLAG(FLAG) ((bool)(extFlags & FLAG))

#ifndef FULLY_CUSTOM_MATERIAL
  #ifdef OBJECT_SHADER  // set via compilation script 
    cbuffer cbMaterial : register(b4) {
      float ksAmbient;
      float ksDiffuse;
      float ksSpecular;
      float ksSpecularEXP;

      #ifdef REDEFINE_KSEMISSIVE
        float3 ksEmissivePad0;
      #else
        float3 ksEmissive;
      #endif
      float ksAlphaRef;

      PARAM_CSP(uint, extFlags);
      PARAM_CSP(float, extExtraSharpLocalReflections);

      CB_MATERIAL_EXTRA_0
      CB_MATERIAL_EXTRA_1
      CB_MATERIAL_EXTRA_2
      CB_MATERIAL_EXTRA_3
      CB_MATERIAL_EXTRA_4
    }

    #ifdef SUPPORTS_DISTANT_FIX
      // cbuffer cbMaterialVS : register(b6) {
      //   float extDistantFixStart;
      //   float extDistantFixMult; // 0.01
      // }
    #endif
  #else
    #define extFlags 0
    #define extExtraSharpLocalReflections 0
  #endif
#endif

#ifdef MODE_KUNOS
  #define extFlags 0
  #define extExtraSharpLocalReflections 0
#endif

#ifndef CBCAMERA_DEFINED
  cbuffer cbCamera : register(b0) {
    float4x4 ksView_base;
    float4x4 ksProjection_base;
    float4x4 ksMVPInverse;
    float3 ksCameraPosition;
    float extCameraTangent;
    float ksNearPlane;
    float ksFarPlane;
    float ksFOV;
    float ksDofFactor;

    #ifndef MODE_KUNOS
      float4 extScreenSize;
      float4x4 ksViewProjPrev;

      float3 extAltCameraPosition;
      float extAltCameraThreshold;
    #endif
  }
#endif

#ifndef EXCLUDE_PEROBJECT_CB
  cbuffer cbPerObject : register(b1) {
    float4x4 ksWorld;

    #ifndef MODE_KUNOS
      float vaoSecondaryMix;
      float perObjAmbientMultiplier;
      float extMotionStencil;
      float _cbPerObject_pad0;

      #ifdef CREATE_MOTION_BUFFER
        float4x4 ksWorldPrev;
      #endif
    #endif
  }
#endif

cbuffer cbLighting : register(b2) {  
  float4 ksLightDirection;
  float4 ksAmbientColor_sky0;
  float4 ksLightColor;
  float4 ksHorizonColor;
  float4 ksZenithColor;
  // 5*16=80

  float ksExposure;
  float ksScreenWidth;
  float ksScreenHeight;
  float ksFogLinear;

  float ksFogBlend;
  float3 ksFogColor;

  float ksCloudCover;
  float ksCloudCutoff;
  float ksCloudColor;
  float ksCloudOffset;

  float ksMinExposure;
  float ksMaxExposure;
  float ksDofFocus;
  float ksDofRange;

  float ksSaturation;
  float ksGameTime;

  #ifndef MODE_KUNOS
    float extSceneRain;
    float extSceneWetness;
    float extSceneWater;
    float extFrameTime;

    float extVaoMode;
    uint extMSAA; // not used with gamma fix applied

    float extFogConstantPiece; 
    float extFogBacklitMult; // 0.5
    float extFogBacklitExp; // 12
    float extFogExp;

    float3 extAdditionalAmbientColor;
    float extPhotoElBoost;
    
    float3 extAdditionalAmbientDir;
    float extAdditionalAmbient_mix;

    float3 extBaseAmbient;
    float extLODDistanceMult;

    float3 extSpecularColor;
    float extSunSpecularMult;

    float2 extWindVel;
    float extWindSpeed;
    float extWindWave;

    uint extWindVelPrev_packed;
    uint extWindSpeedWavePrev_packed;
    uint extGlowBrightness_extForceEmissive;
    float extLambertGamma_inner;
    #define extLambertGamma GAMMA_OR_STAT(1., extLambertGamma_inner)
    #define extMainBrightnessMult GAMMA_OR_STAT(extLambertGamma_inner, 1.)
    #define extGlowBrightness (loadVector(extGlowBrightness_extForceEmissive).x)
    #define extForceEmissive (loadVector(extGlowBrightness_extForceEmissive).y)

    float4 extEmissiveMults; // emissive mult, emissive mult×adaptive mult nearby, emissive mult×adaptive mult distant, white ref emissive
    #define extWhiteRefPoint extEmissiveMults.w

    // With gamma fix applied these four are actually used for secondary fog
    float extIBLAmbient;
    float extIBLAmbientBaseThreshold;  
    float extIBLAmbientBrightness;
    float extIBLAmbientSaturation;
    #define p_fog2Color_blend asuint(float2(extIBLAmbient, extIBLAmbientBaseThreshold))
    #define p_fog2Exp extIBLAmbientBrightness
    #define p_fog2Linear extIBLAmbientSaturation

    float extVAOMult;
    float extVAOAdd;
    float extVAOExp;
    float extCloudShadowOpacity;

    // TODO: Move?
    float4x4 extCloudShadowMatrix;

    float3 extCPLDir;
    float extCPLFactor;

    float3 extDirLook;
    float extFogAtmosphere;

    float3 extDirUp;
    uint extPad_screenMode_frameNum_A2COffset;

    float3 extSceneOffset;
    uint extTiltBias_extGrassFx12RangeInv;

    float2 extWindWater;
    #ifdef GAMMA_FIX
      uint extWfxHint01;
    #else
      float extFresnelGamma;
    #endif
    float _extPad;

    float3 ksAmbientColor_sky1;
    float ksAmbientColor_sky1_mix;

    float3 extExtraAoAmbientColor;
    float extExtraAoAmbientColor_exp;

    #define extScreenMode (uint)((extPad_screenMode_frameNum_A2COffset >> 16) & 0xff)
    #define extFrameNum (uint)((extPad_screenMode_frameNum_A2COffset >> 8) & 0xff)
    #define extA2COffset float2((extPad_screenMode_frameNum_A2COffset & 1), (extPad_screenMode_frameNum_A2COffset & 2) ? 1 : 0)
    #define isScreenTriple (extScreenMode == 1)
    #define isScreenVR (extScreenMode == 2)
    #define racingLineDebug ((extPad_screenMode_frameNum_A2COffset & 4) != 0)
    #define transparentOutlinePass ((extPad_screenMode_frameNum_A2COffset & 8) != 0)
    #define useNearbyInteriorClipping ((extPad_screenMode_frameNum_A2COffset & (1 << 24)) != 0)
    #define extWindVelPrev (loadVector(extWindVelPrev_packed))
    #define extWindSpeedPrev (loadVector(extWindSpeedWavePrev_packed).x)
    #define extWindWavePrev (loadVector(extWindSpeedWavePrev_packed).y)
    #define extTiltBias (loadVector(extTiltBias_extGrassFx12RangeInv).x)
    #define extGrassFx12RangeInv (loadVector(extTiltBias_extGrassFx12RangeInv).y)

    #ifdef GAMMA_FIX
      #define extUseNewFog 1
    #else
      #define extUseNewFog ((extPad_screenMode_frameNum_A2COffset & (1 << 25)) != 0)
    #endif
    
    #ifndef GAMMA_FIX_CONDITION
      #define GAMMA_FIX_CONDITION (_extPad)
    #endif
  #else
    #define GAMMA_FIX_CONDITION false
    #define extUseNewFog 0
  #endif
}

#ifdef OBJECT_SHADER
  float getEmissiveMult(){
    #ifdef USE_GLOBAL_EMISSIVE_MULT
      return HAS_FLAG(FLAG_EMISSIVE_FIXED) ? 1 : extEmissiveMults[extFlags & 3];
    #else
      return 1;
    #endif
  }
#else
  float getEmissiveMult(){
    return extEmissiveMults.x;
  }
#endif

#include "_gamma.fx"

#if defined(USE_LAMBERT_GAMMA) && !defined(GAMMA_FIX)
  #define LAMBERT(N, L) pow(saturate(dot(N, L)), extLambertGamma) * saturate(dot(N, L) * 10)
#else
  #define LAMBERT(N, L) saturate(dot(N, L))
#endif

#ifndef MODE_KUNOS
  #define getCloudShadow(posC) \
    saturate(1 - txCloudShadow.SampleLevel(samLinearClamp, mul(float4(posC, 1), extCloudShadowMatrix).xy, 0) * extCloudShadowOpacity)
  #define IN_VAO pin.Ao
#else
  #define getCloudShadow(posC) 1
  #define IN_VAO 1
#endif

// #ifndef NO_SHADOWS
cbuffer cbShadowMaps : register(b3) {
  float4x4 ksShadowMatrix0;
  #if defined(MODE_KUNOS)
    float4x4 ksShadowMatrix1;
    float4x4 ksShadowMatrix2;
  #endif
  float3 bias;
  float pixelSize;

  #if !defined(MODE_KUNOS)
    float shadowR1;
    float3 shadowPos;

    float shadowR2;
		float3 shadowsTransitionWeight;

    float3 shadowsMatrixModifier1A;
    float reflectionsTweak;

    float3 shadowsMatrixModifier1M;
    float shadowReflectionOcclusion;

    float3 shadowsMatrixModifier2A;
		float extShadowsOpacityMult;

    float3 shadowsMatrixModifier2M;
		float realTextureSize;

    float3 shadowsMatrixModifier3A;
		float realTextureSizeSqr2;

    float3 shadowsMatrixModifier3M;
    float sceneShadowOpacity;

		float4 shadowsBias;
		float4 shadowsDistanceSqrInv;
  #endif
}
// #endif

#ifdef INCLUDE_PARTICLE_CB 
  cbuffer cbParticle : register(b5) {
    float emissiveBlend;
    float minDistance;
    float shaded;
    float boh;
  }

  #define extCaoUV (float4x4)0
#else
  cbuffer _cbColorAmbientOcclusion : register(b13) {
    float extCaoBias;
    float3 _extCaoPad;
    float4x4 extCaoUV;
  }
#endif

#if defined(MODE_GRASSFX)
  // #define ksAmbientColor_sky float3(0, 0, 0)
  #define extBaseAmbient float3(0, 0, 0)
#endif

#if defined(MODE_LIGHTMAP)
  // #define ksAmbientColor_sky float3(0, 0, 0)
  #define extBaseAmbient float3(0, 0, 0)
  #define ksSpecular 0
#endif

#ifdef MODE_KUNOS
  #define extBaseAmbient float3(0,0,0)
  #define extAdditionalAmbientColor float3(0,0,0)
  #define extAdditionalAmbientDir float3(0,0,0)
  #define extDirUp float3(0,1,0)
  #define extScreenSize float4(1920,1080,1./1920,1./1080)
  #define perObjAmbientMultiplier 1
  #define extForceEmissive 0
  #define extSpecularColor ksLightColor.xyz
  #define extGlowBrightness 0
  #define extMSAA 1
  #define extPhotoElBoost 0
  #define extSceneRain 0
  #define extSceneWetness 0
  #define extSceneWater 0
  #define extExtraAoAmbientColor 0
  #define extExtraAoAmbientColor_exp 1
  #define extVAOExp 1
  #define extSceneOffset float3(0,0,0)
  #define extWhiteRefPoint 3
  #define extSunSpecularMult 1
  #define extFresnelGamma 1
  #define ksAmbientColor_sky ksAmbientColor_sky0.rgb

  #define AMBIENT_COLOR_NEARBY (ksAmbientColor_sky0.rgb)
  #define AMBIENT_COLOR_REMOTE (ksAmbientColor_sky0.rgb)
  #define AMBIENT_COLOR_AT(POS_C) (ksAmbientColor_sky0.rgb)
#else
  #ifdef GAMMA_FIX
    #define FOG_NEW_FORMULA_NO_BRANCH
  #endif

  float ambientAFade(float3 posC){
    #ifdef MODE_LIGHTMAP
      return 0;
    #endif
    float k = dot2(posC) * extAdditionalAmbient_mix; 
    return k / (1 + k);
    // return saturate(k);
  }

  float ambient2Fade(float3 posC){
    #ifdef MODE_LIGHTMAP
      return 0;
    #endif
    float k = dot2(posC) * ksAmbientColor_sky1_mix; 
    return k / (1 + k);
    // return saturate(k);
  }

  #define AMBIENT_COLOR_NEARBY (ksAmbientColor_sky0.rgb)
  #define AMBIENT_COLOR_REMOTE (ksAmbientColor_sky0.rgb + ksAmbientColor_sky1)
  #define AMBIENT_COLOR_AT(POS_C)\
    (ksAmbientColor_sky0.rgb + ksAmbientColor_sky1 * ambient2Fade(POS_C))
    // (ksAmbientColor_sky0.rgb + ksAmbientColor_sky1 * pow(saturate(dot2(POS_C) * ksAmbientColor_sky1_mix), 0.5))
  #define ksAmbientColor_sky AMBIENT_COLOR_AT(posC)
#endif

#ifdef ALLOW_SPS
  #define ksRealCameraPosition (pin.PosH.x < extAltCameraThreshold ? extAltCameraPosition : ksCameraPosition.xyz)
  #define SPSAwarePosC (pin.PosC + (extAltCameraPosition - ksCameraPosition.xyz) * (pin.PosH.x < extAltCameraThreshold ? 1 : 0))
#else
  #define ksRealCameraPosition (ksCameraPosition.xyz)
  #define SPSAwarePosC (pin.PosC + (extAltCameraPosition - ksCameraPosition.xyz) * (pin.PosH.x < extAltCameraThreshold ? 1 : 0))
#endif
#define ksCameraPositionWorld (ksRealCameraPosition - extSceneOffset)
#define ksInPositionWorld (pin.PosC + ksCameraPositionWorld)
#define RENDERING_REFLECTION_CUBEMAP (ksNearPlane == 0.047)

#ifndef AMBIENT_SIMPLE_FN
  #define AMBIENT_SIMPLE_FN(x) saturate((x).y * 0.25 + 0.75)
#endif

float3 getAmbientBaseAt(float3 posC, float3 dirW, float4 ambientAoMult){
  #ifdef MICROOPTIMIZATIONS
    float ambientMultiplier = AMBIENT_SIMPLE_FN(dirW);
  #else
    float upMultiplier = dirW.y * 0.5 + 0.5;
    float ambientMultiplier = saturate(upMultiplier * 0.5 + 0.5);
  #endif

  float3 additionalAmbient = extAdditionalAmbientColor * (saturate(dot(dirW, extAdditionalAmbientDir)) * (1 - ambientAFade(posC)));
  float3 ambientMult = min(ambientAoMult.xyz, ambientMultiplier);
  return ambientAoMult.w * extBaseAmbient
    + max(0, (ksAmbientColor_sky + additionalAmbient) * ambientMult
      #ifndef MODE_LIGHTMAP
      + extExtraAoAmbientColor * (1 - ambient2Fade(posC)) * pow(saturate(ambientAoMult.xyz), extExtraAoAmbientColor_exp)
      #endif
    );
}

float3 getAmbientBaseNonDirectional(float3 posC){
  return extBaseAmbient + (ksAmbientColor_sky + extAdditionalAmbientColor * (1 - ambientAFade(posC))
    #ifndef MODE_LIGHTMAP
    + extExtraAoAmbientColor * (1 - ambient2Fade(posC))
    #endif    
    ) * 0.75
  ;
}

#endif