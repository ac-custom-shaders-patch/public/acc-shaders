#ifndef SHADOWS_COORDS
  #define SHADOWS_COORDS pin.ShadowTex0, pin.ShadowTex1, pin.ShadowTex2
#endif

bool checkCascade(float4 uv){
  return -1 < uv.x && uv.x < 1 && -1 < uv.y && uv.y < 1;
}

float cascadeShadowStep(float deltaZ, float4 uv, Texture2D<float> tx){
  float result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  [unroll]
  for (int x = -1; x <= 1; x++){
    [unroll]
    for (int y = -1; y <= 1; y++){
      #ifdef MICROOPTIMIZATIONS
        result += tx.SampleCmpLevelZero(samShadow, uv.xy + pixelSize * float2(x, y), comparisonValue);
      #else
        result += tx.SampleCmpLevelZero(samShadow, uv.xy + float2(y * pixelSize, x * pixelSize), comparisonValue);
      #endif
    }
  }

  return result;
}

float getShadowBiasMult(float3 posC, float4 posH, float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2, float biasMultiplier, 
    float fallbackValue){
  float deltaZBase = max(1 - abs(dot(normalW, ksLightDirection.xyz)), 0.1) * biasMultiplier;

  if (checkCascade(shadowTex0)){
    return cascadeShadowStep(deltaZBase * bias.x, shadowTex0, txShadow0) / 9;
  } else

  if (checkCascade(shadowTex1)){
    return cascadeShadowStep(deltaZBase * bias.y, shadowTex1, txShadow1) / 9;
  } else

  if (checkCascade(shadowTex2)){
    return min(cascadeShadowStep(deltaZBase * bias.z, shadowTex2, txShadow2) / 9 
        + max(-(shadowTex2.y + 0.5), 0) * 2, 1);
  } else

  return 1;
}

float getShadow(float3 posC, float4 posH, float3 normalW, float4 shadowTex0, float4 shadowTex1, float4 shadowTex2, float fallbackValue){
  #ifdef USE_SHADOW_BIAS_MULT
    float biasMultiplier = shadowBiasMult + 1;
    return getShadowBiasMult(posC, posH, normalW, shadowTex0, shadowTex1, shadowTex2, biasMultiplier, fallbackValue);
  #elif defined GETSHADOW_BIAS_MULTIPLIER
    return getShadowBiasMult(posC, posH, normalW, shadowTex0, shadowTex1, shadowTex2, GETSHADOW_BIAS_MULTIPLIER, fallbackValue);
  #else
    return getShadowBiasMult(posC, posH, normalW, shadowTex0, shadowTex1, shadowTex2, 1, fallbackValue);
  #endif
}