#include "include/common.hlsl"

#ifdef OBJECT_SHADER  // set via compilation script 
  #ifndef INPUT_AMBIENT_K
    #define INPUT_AMBIENT_K ksAmbient
  #endif
  #ifndef INPUT_DIFFUSE_K
    #define INPUT_DIFFUSE_K ksDiffuse
  #endif
  #ifndef INPUT_EMISSIVE3
    #define INPUT_EMISSIVE3 ksEmissive
  #endif
  #ifndef INPUT_SPECULAR_K
    #define INPUT_SPECULAR_K ksSpecular
  #endif
  #ifndef INPUT_SPECULAR_EXP
    #define INPUT_SPECULAR_EXP ksSpecularEXP
  #endif
  #ifndef INPUT_NM_OBJECT_SPACE
    #define INPUT_NM_OBJECT_SPACE nmObjectSpace
  #endif
  #ifndef INPUT_DETAIL_UV_MULT
    #define INPUT_DETAIL_UV_MULT detailUVMultiplier
  #endif
  #ifndef INPUT_USE_DETAIL
    #define INPUT_USE_DETAIL useDetail
  #endif
#else
  #ifndef INPUT_AMBIENT_K
    #define INPUT_AMBIENT_K 0.4
  #endif
  #ifndef INPUT_DIFFUSE_K
    #define INPUT_DIFFUSE_K 0.4
  #endif
  #ifndef INPUT_EMISSIVE3
    #define INPUT_EMISSIVE3 0
  #endif
  #ifndef INPUT_SPECULAR_K
    #define INPUT_SPECULAR_K 0
  #endif
  #ifndef INPUT_SPECULAR_EXP
    #define INPUT_SPECULAR_EXP 0
  #endif
  #ifndef INPUT_NM_OBJECT_SPACE
    #define INPUT_NM_OBJECT_SPACE 0
  #endif
  #ifndef INPUT_DETAIL_UV_MULT
    #define INPUT_DETAIL_UV_MULT 0
  #endif
  #ifndef INPUT_USE_DETAIL
    #define INPUT_USE_DETAIL 0
  #endif
#endif

#ifdef ALLOW_VRS_FIX
  #define APPLY_VRS_FIX \
    if (HAS_FLAG(FLAG_APPLY_VRS_FIX)) { txDiffuseValue.rgb *= directedMult; }
#else
  #define APPLY_VRS_FIX {}
#endif

#ifndef GET_NORMALW_TEX_ARGS_DECLARE
  #define GET_NORMALW_TEX_ARGS_DECLARE
#endif

#ifndef GET_NORMALW_TEX_ARGS_PASS
  #define GET_NORMALW_TEX_ARGS_PASS
#endif

#ifndef GET_NORMALW_TEX_PREPROCESS_TEX
  #define GET_NORMALW_TEX_PREPROCESS_TEX(X)
#endif

// Colored specular and reflections

float3 getBaseSpecularColor(float3 txDiffuseValue, float normalizationK){
  txDiffuseValue = GAMMA_LINEAR(txDiffuseValue);
  return txDiffuseValue / max(txDiffuseValue.r, max(txDiffuseValue.g, max(txDiffuseValue.b, normalizationK + 0.001)));
}

#define GET_SPEC_COLOR lerp(1, getBaseSpecularColor(txDiffuseValue.xyz, extColoredReflectionNorm), extColoredReflection)
#define GET_SPEC_COLOR_MASK_DETAIL lerp(1, getBaseSpecularColor(txDiffuseValue.xyz, extColoredReflectionNorm), \
  extColoredReflection == 2 ? txMapsValue.a : extColoredReflection == 3 ? 1 - txDiffuseValue.a : extColoredReflection)
#define GET_SPEC_COLOR_MASK_DETAIL_EXTRA(X) lerp(1, getBaseSpecularColor(txDiffuseValue.xyz, extColoredReflectionNorm), \
  (extColoredReflection == 2 ? txMapsValue.a : extColoredReflection == 3 ? 1 - txDiffuseValue.a : extColoredReflection) * (X))
  
// Shadows

#ifdef MODE_SHADOWS_ADVANCED
  #define READ_VECTORS\
    float3 toCamera = (float3)0;\
    float3 normalW = (float3)0;
  #define READ_VECTORS_NM\
    READ_VECTORS\
    float3 tangentW = (float3)0;\
    float3 bitangentW = (float3)0;
#else
  #define READ_VECTOR_TOCAMERA\
    float3 toCamera = normalize(pin.PosC);

  #define READ_VECTORS\
    READ_VECTOR_TOCAMERA\
    float3 normalW = normalize(pin.NormalW);\
    float3 __normalWFlat = normalW;

  #ifdef NO_NORMALMAPS
    #define READ_VECTORS_NM\
      READ_VECTORS\
      float3 tangentW = float3(1, 0, 0);\
      float3 bitangentW = float3(1, 0, 0);
  #else
    #define READ_VECTORS_NM\
      READ_VECTORS\
      float3 tangentW = normalize(pin.TangentW);\
      float3 bitangentW = normalize(pin.BitangentW);
  #endif
#endif

#ifdef NO_SHADOWS

  #ifndef SHADOWS_COORDS
    #define SHADOWS_COORDS 0, 0, 0
  #endif

  #define getShadow(posC, posH, normalW, shadowTex0, shadowTex1, shadowTex2, fallbackValue) (getCloudShadow(posC) * luminance(_AO_VAO) * perObjAmbientMultiplier)
  #define getShadowBiasMult(posC, posH, normalW, shadowTex0, shadowTex1, shadowTex2, biasMultiplier, fallbackValue) (getCloudShadow(posC) * luminance(_AO_VAO) * perObjAmbientMultiplier)

#elif defined(MODE_KUNOS)

  #include "basic_shadows.fx"

#endif

// Fix for tiling

float4 textureSampleVariation(Texture2D tx, SamplerState sam, float2 uv, float bias, float noiseMult){
  #ifdef MODE_KUNOS
    float k = 0;
  #else
    float k = txNoise.Sample(samLinear, noiseMult * uv).x;
  #endif

  float2 duvdx = ddx(uv) * bias;
  float2 duvdy = ddy(uv) * bias;
    
  float l = k * 8.0;
  float i = floor(l);
  float f = frac(l);

  float2 offa = sin(float2(3.0, 7.0) * (i + 0.0));
  float2 offb = sin(float2(3.0, 7.0) * (i + 1.0));
  float4 cola = tx.SampleGrad(sam, uv + offa, duvdx, duvdy);
  float4 colb = tx.SampleGrad(sam, uv + offb, duvdx, duvdy);

  return lerp(cola, colb, smoothstep(0.2, 0.8, f - 0.1 * dot(cola - colb, 1)));
}

float4 textureSampleVariation(Texture2D tx, SamplerState sam, float2 uv, float bias = 1){
  return textureSampleVariation(tx, sam, uv, bias, 0.05);
}

// Normals

#if !defined(NO_CARPAINT) || defined(USE_GETNORMALW)

#ifdef USE_GETNORMALW
  #undef INPUT_NM_OBJECT_SPACE
  #define INPUT_NM_OBJECT_SPACE false
#endif

#ifndef GETNORMALW_SAMPLER
  #define GETNORMALW_SAMPLER samLinear
#endif

#ifdef CARPAINT_DAMAGES

float3 getDamageNormalW(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW, float damage, 
    out float normalAlpha GET_NORMALW_TEX_ARGS_DECLARE){
  #ifdef NO_NORMALMAPS
    normalAlpha = 0.0;
    return normalW;
  #else

    #ifndef GETNORMALW_NORMALIZED_INPUT
      normalW = normalize(normalW);
    #endif

    #ifdef GETNORMALW_XYZ_TX
      float4 txNormalValue = txNormal.Sample(samLinearSimple, uv).xzyw;
    #else
      float4 txNormalValue = txNormal.Sample(samLinearSimple, uv).xyzw;
    #endif
    GET_NORMALW_TEX_PREPROCESS_TEX(txNormalValue);

    normalAlpha = txNormalValue.a;

    #ifdef CARPAINT_GLASS
      normalAlpha = damage * txNormalValue.a;
    #endif

    if (INPUT_NM_OBJECT_SPACE){
      float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
      txNormalAligned.z *= -1;
      float3x3 m = (float3x3)ksWorld;
      return normalize(mul(txNormalAligned, m));
    } else {
      float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
      txNormalAligned = normalize(txNormalAligned);
      #ifdef CARPAINT_GLASS
        float3x3 m = float3x3(tangentW, bitangentW, normalW);
      #else
        float3x3 m = float3x3(tangentW, normalW, bitangentW);
      #endif
      return lerp(normalW, normalize(mul(transpose(m), txNormalAligned)), damage);
    }

  #endif
}

#endif

#if defined(CARPAINT_NM) || defined(USE_GETNORMALW) || defined(CARPAINT_AT)

float3 getNormalW(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW, out float alpha, out float diffuseMult GET_NORMALW_TEX_ARGS_DECLARE){
  #ifdef NO_NORMALMAPS
    #ifdef USE_BLURRED_NM
      alpha = 1.0;
    #else 
      #ifdef GETNORMALW_UV_MULT
        alpha = txNormal.Sample(GETNORMALW_SAMPLER, uv * GETNORMALW_UV_MULT).a;
      #else
        alpha = txNormal.Sample(GETNORMALW_SAMPLER, uv).a;
      #endif
    #endif

    diffuseMult = 1;
    return normalW;
  #else

    #ifdef USE_BLURRED_NM
      alpha = 1;

      float3 T = normalize(tangentW);
      float3 B = normalize(bitangentW);
      float4 txNormalValue = txNormal.Sample(samLinearSimple, uv);
      GET_NORMALW_TEX_PREPROCESS_TEX(txNormalValue);

      float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
      float4 txNormalBlurValue = txNormalBlur.Sample(samLinearSimple, uv);
      float3 txNormalBlurAligned = txNormalBlurValue.xyz * 2 - (float3)1;
      float3x3 m = float3x3(T, normalW, B);
      float3 normalStatic = normalize(mul(transpose(m), txNormalAligned.xzy));
      float3 normalBlur = normalize(mul(transpose(m), txNormalBlurAligned.xzy));
      alpha = lerp(txNormalValue.a, txNormalBlurValue.a, blurLevel);
      diffuseMult = 1;
      return lerp(normalStatic, normalBlur, blurLevel);
    #else 
      #ifdef GETNORMALW_UV_MULT
        float4 txNormalValue = txNormal.Sample(GETNORMALW_SAMPLER, uv * GETNORMALW_UV_MULT);
      #else
        float4 txNormalValue = txNormal.Sample(GETNORMALW_SAMPLER, uv);
      #endif
      GET_NORMALW_TEX_PREPROCESS_TEX(txNormalValue);

      if (INPUT_NM_OBJECT_SPACE){
        alpha = txNormalValue.a;
        float3 txNormalAligned = txNormalValue.xzy * 2 - 1;
        diffuseMult = 1;
        txNormalAligned.z *= -1;
        float3x3 m = (float3x3)ksWorld;
        return normalize(mul(txNormalAligned, m));
      } else {
        float3 T = normalize(tangentW);
        float3 B = normalize(bitangentW);
        alpha = txNormalValue.a;
        float3 txNormalAligned = txNormalValue.xyz * 2 - 1;
        float3x3 m = float3x3(T, normalW, B);
        float3 retBase = mul(transpose(m), txNormalAligned.xzy);
        diffuseMult = length(retBase);
        return normalize(retBase);
      }
    #endif

  #endif
}

#elif defined CARPAINT_NM_UVMULT
#else

float3 getNormalW(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW, bool objectSpace, out float diffuseMult GET_NORMALW_TEX_ARGS_DECLARE){
  #ifndef GETNORMALW_NORMALIZED_INPUT
    normalW = normalize(normalW);
  #endif

  #ifdef NO_NORMALMAPS
    diffuseMult = 1;
    return normalW;
  #else
    float4 txNormalValueBase = txNormal.Sample(GETNORMALW_SAMPLER, uv);
    GET_NORMALW_TEX_PREPROCESS_TEX(txNormalValueBase);

    if (objectSpace){
      #ifdef GETNORMALW_XYZ_TX
        float4 txNormalValue = txNormalValueBase;
        float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
        txNormalAligned.z *= -1;
      #else
        float4 txNormalValue = txNormalValueBase;
        float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
        txNormalAligned.z *= -1;
      #endif
      float3x3 m = (float3x3)ksWorld;
      diffuseMult = 1;
      return normalize(mul(txNormalAligned, m));
    } else {
      #ifdef GETNORMALW_NORMALIZED_TB
        float3 T = tangentW;
        float3 B = bitangentW;
      #else
        float3 T = normalize(tangentW);
        float3 B = normalize(bitangentW);
      #endif

      #ifdef GETNORMALW_XYZ_TX
        float3 txNormalValue = txNormalValueBase.xyz;
      #else
        float3 txNormalValue = txNormalValueBase.xzy;
      #endif

      float3 txNormalAligned = txNormalValue * 2 - 1;
      float3x3 m = float3x3(T, normalW, B);
      float3 retBase = mul(transpose(m), txNormalAligned.xzy);
      diffuseMult = length(retBase);
      return normalize(retBase);
    }
  #endif
}

float3 getNormalW(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW, out float diffuseMult GET_NORMALW_TEX_ARGS_DECLARE){
  #ifdef NO_NORMALMAPS
    diffuseMult = 1;
    return normalW;
  #elif defined(CARPAINT_NMDETAILS) || defined(CARPAINT_SIMPLE)
    return getNormalW(uv, normalW, tangentW, bitangentW, false, diffuseMult GET_NORMALW_TEX_ARGS_PASS);
  #else
    return getNormalW(uv, normalW, tangentW, bitangentW, INPUT_NM_OBJECT_SPACE, diffuseMult GET_NORMALW_TEX_ARGS_PASS);
  #endif
}

#endif

#endif

float3 fixReflDir(float3 dir, float3 pos, float3 normal, float reflBlur, float modifier = 1){
  float3 result;
  #ifdef EXTENDED_PEROBJECT_CB
    float fixAmount = reflectionsTweak;
    fixAmount *= saturate(dot(dir, normalize(pos)) * 2 + 1);
    result = normalize(dir + pos * fixAmount);
  #else 
    result = dir;
  #endif
  return result;
}

float4 getSkyColor(float3 direction){
  float v = saturate(extUseNewFog ? ksFogBlend : 900 / ksFogLinear);
  float3 color;
  if (direction.y > 0) {
    color = lerp(ksHorizonColor.rgb, ksZenithColor.rgb, 1 / rsqrt(direction.y));
    color = lerp(color, AMBIENT_COLOR_REMOTE.r * 0.5, v);
  } else {
    color = lerp(ksHorizonColor.rgb, float3(0.15, 0.125, 0.125), saturate(direction.y * -7));
    color = lerp(color, AMBIENT_COLOR_REMOTE.r * 0.5, v);
  }
  return float4(color, v);
}

#ifdef RAINBOW_REFLECTIONS_K
  #ifndef RAINBOW_REFLECTIONS_MULT
    #define RAINBOW_REFLECTIONS_MULT 1
  #endif

  float3 getReflectionRainbowValue(float3 dir, float blur){
    return txCube.SampleLevel(samLinearSimple, dir, max(2, blur)).rgb;
  }

  float3 getReflectionRainbow(float3 reflDir, float3 look, float blur, float mask){
    #ifdef BLACK_REFLECTIONS
      return 0;
    #else
      float3 reflCubeDir = float3(-reflDir.x, reflDir.y, reflDir.z);
      float offset = mask * RAINBOW_REFLECTIONS_K / (abs(dot(reflDir, look)) + 1);
      float3 extraOffset = mask * RAINBOW_REFLECTIONS_K * float3(0, 0.1, 0);
      float c0 = getReflectionRainbowValue(reflCubeDir + look * offset + extraOffset, blur).r;
      float c1 = getReflectionRainbowValue(reflCubeDir, blur).g;
      float c2 = getReflectionRainbowValue(reflCubeDir - look * offset - extraOffset, blur).b;
      return max(lerp(c1, float3(c0, c1, c2), RAINBOW_REFLECTIONS_MULT), 0);
    #endif
  }
#endif

#ifndef SAMPLE_REFLECTION_FN
  #define SAMPLE_REFLECTION_FN sampleReflectionCube
#endif

#ifndef REFL_SAMPLE_PARAM_TYPE
  #define REFL_SAMPLE_PARAM_TYPE float4
  #define REFL_SAMPLE_PARAM_DEFAULT 1
#elif !defined(REFL_SAMPLE_PARAM_DEFAULT)
  #define REFL_SAMPLE_PARAM_DEFAULT (REFL_SAMPLE_PARAM_TYPE)0
#endif

#ifndef REFLECTION_BLUR_MULT
  #define REFLECTION_BLUR_MULT 1
#endif

float3 sampleReflectionCube(float3 reflDir, float blur, bool useBias, REFL_SAMPLE_PARAM_TYPE extraParam){
  #ifdef BLACK_REFLECTIONS
    return 0;
  #else
    float3 reflCubeDir = float3(-reflDir.x, reflDir.y, reflDir.z);
    #ifdef MODE_SIMPLIFIED_FX
      return txCube.SampleBias(samLinearSimple, reflCubeDir, blur * REFLECTION_BLUR_MULT).rgb;
    #else
      if (useBias){
        float level = txCube.CalculateLevelOfDetail(samLinearSimple, reflDir);
        return txCube.SampleLevel(samLinearSimple, reflCubeDir, blur * REFLECTION_BLUR_MULT + level * saturate(2 - blur) * 0.4).rgb;
      } else {
        return txCube.SampleLevel(samLinearSimple, reflCubeDir, blur * REFLECTION_BLUR_MULT).rgb;
      }
    #endif
  #endif
}

#ifndef NOREFLECTIONS

// Reflections (new)

float3 getFallbackReflectionsAt(float3 reflDir, float blur){
  float v = saturate(extUseNewFog ? ksFogBlend : 900 / ksFogLinear);
  float3 color = lerp(
    lerp(ksHorizonColor.rgb, ksZenithColor.rgb, saturate(reflDir.y)) * 1.5, 
    ksFogColor.rgb, v);
  color = lerp(color, float3(0.05, 0.11, 0.08), saturate(reflDir.y * -60 / (blur + 1) + 0.1));
  return color * saturate(perObjAmbientMultiplier * 2);
}

struct ReflParams {
  float fresnelMaxLevel;
  float fresnelC;
  float fresnelEXP;
  float ksSpecularEXP;
  float finalMult;
  float globalOcclusionMult;
  float metallicFix;

  float coloredReflections;
  float3 coloredReflectionsColor;

  bool useBias;
  bool isCarPaint;
  bool useSkyColor;
  bool useStereoApproach;
  bool useMatteFix;
  bool useGlobalOcclusionMult;

  #ifdef USE_SECONDARY_REFLECTION_LAYER
    float secondaryLevel;
    float3 secondaryIntensity;
  #endif

  float stereoExtraMult;
  REFL_SAMPLE_PARAM_TYPE reflectionSampleParam;

  bool forceReflectedDir;
  float3 forcedReflectedDir;

  #if defined(ALLOW_RAINFX) && !defined(RAINFX_REGULAR_REFLECTIONS)
    float fresnelMaxLevelDry;
    float fresnelCDry;
    float fresnelEXPDry;
  #endif

  // Out:
  float3 resultColor;
  float resultPower;
  float resultBlur;
};

float3 getReflectionAt(float3 reflDir, float3 look, float blur, bool useBias, 
    #ifdef USE_SECONDARY_REFLECTION_LAYER
      float coloredReflections, float3 coloredReflectionsColor,
      float secondaryLevel, float3 secondaryIntensity, 
    #endif
    REFL_SAMPLE_PARAM_TYPE extraParam){
  #ifdef BLACK_REFLECTIONS
    return 0;
  #elif defined(NO_REFLECTIONS) 
    return getFallbackReflectionsAt(reflDir, blur);
  #elif defined(NO_REFLECTIONS_FOR_CUBEMAP)
    return RENDERING_REFLECTION_CUBEMAP ? getFallbackReflectionsAt(reflDir, blur) : SAMPLE_REFLECTION_FN(reflDir, blur, useBias, extraParam);
  #elif defined(FORCE_BLURREST_REFLECTIONS)
    return SAMPLE_REFLECTION_FN(reflDir, 100, false, extraParam);
  #else
    #ifdef RAINBOW_REFLECTIONS_K
      return getReflectionRainbow(reflDir, look, blur, (extraParam).x);
    #else
      float3 ret = SAMPLE_REFLECTION_FN(reflDir, blur, useBias, extraParam);
      #ifdef USE_SECONDARY_REFLECTION_LAYER
        ret = lerp(ret, ret * coloredReflectionsColor, coloredReflections);
        ret = max(ret, secondaryIntensity * SAMPLE_REFLECTION_FN(reflDir, secondaryLevel, false, extraParam));  
      #endif
      return ret;
    #endif
  #endif
}

#ifndef IS_ADDITIVE_VAR
  #ifndef OBJECT_SHADER
    #define IS_ADDITIVE_VAR 0
  #elif defined(CARPAINT_NM_UVMULT)
    #define IS_ADDITIVE_VAR 0
  #elif defined(CARPAINT_GLASS)
    #define IS_ADDITIVE_VAR 1
  #elif defined(NO_CARPAINT)
    #define IS_ADDITIVE_VAR 0
  #else
    #define IS_ADDITIVE_VAR isAdditive
  #endif
#endif

#ifndef GET_REFL_DIR
  #define GET_REFL_DIR(LOOK, SURFACE) normalize(reflect(LOOK, SURFACE))
#endif

float getCPLMult(float3 reflDir){
  #ifdef USE_CPL
    return 1 - saturate(dot(reflDir, extCPLDir) * extCPLFactor);
  #else
    return 1;
  #endif
}

float smin(float a, float b){
  // return min(a, b);
  float h = saturate(b - abs(a - b));
  return min(a, b) - h * h / max(0.00001, b * 3) * (1 - b);
}

float4 calculateReflection(float4 lighting, float3 toCamera, float3 posC, float3 normalW, inout ReflParams P){
  #ifdef BLACK_REFLECTIONS
    #ifndef KEEP_REFLECTION_PARAMS
      P = (ReflParams)0;
    #else
      {
        float reflBlurBase = saturate(1 - P.ksSpecularEXP / (P.isCarPaint && IS_ADDITIVE_VAR == 2 ? 8 : 255));
        float reflBlur = pow(reflBlurBase, 2) * 6;
        P.resultBlur = reflBlurBase * 6;
        P.resultPower = 0;
        P.resultColor = 0;
        P.useMatteFix = false;
      }
    #endif
    return lighting;
  #endif

  #ifdef ALLOW_PERVERTEX_AO_DEBUG__NOREFL
    [branch]
    if (extVaoMode == 4){
      P = (ReflParams)0;
      return lighting;
    } else {
  #endif

  P.fresnelEXP = P.fresnelEXP * GAMMA_ACTUAL_VALUE;
  // P.fresnelC = GAMMA_OR(pow(saturate(P.fresnelC), 1.75), P.fresnelC);
  P.fresnelC = GAMMA_OR(pow(saturate(P.fresnelC), 2), P.fresnelC);
  P.fresnelMaxLevel = GAMMA_OR(pow(saturate(P.fresnelMaxLevel), 2), P.fresnelMaxLevel);
  // P.fresnelC = GAMMA_OR(pow(saturate(P.fresnelC), 1.5), P.fresnelC);
  // P.fresnelMaxLevel = GAMMA_OR(pow(saturate(P.fresnelMaxLevel), 1.1), P.fresnelMaxLevel);

  #ifdef USE_SECONDARY_REFLECTION_LAYER
    P.secondaryIntensity = GAMMA_OR(pow(saturate(P.secondaryIntensity), 2), P.secondaryIntensity);
  #endif

  float facingToCamera = dot(normalW, -toCamera);
  float input = saturate(1 - facingToCamera);
  #ifdef REFLECTION_FRESNELEXP_BOUND
    float fresnel = pow(input, IS_ADDITIVE_VAR == 0 ? max(P.fresnelEXP, 1) : P.fresnelEXP);
  #else
    float fresnel = pow(input, P.fresnelEXP * lerp(0.8, 1, P.fresnelMaxLevel) /* offsetting gradient to compensate for smin() */);
  #endif

  fresnel += P.useStereoApproach
    ? lerp(P.fresnelC, P.fresnelMaxLevel, saturate(2 * P.finalMult - 1)) * P.stereoExtraMult
    : P.fresnelC;
  fresnel = smin(fresnel, P.fresnelMaxLevel);

  #if defined(ALLOW_RAINFX) && !defined(RAINFX_REGULAR_REFLECTIONS)
    P.fresnelEXPDry = P.fresnelEXPDry * GAMMA_ACTUAL_VALUE;
    P.fresnelCDry = GAMMA_OR(pow(saturate(P.fresnelCDry), 2), P.fresnelCDry);
    P.fresnelMaxLevelDry = GAMMA_OR(pow(saturate(P.fresnelMaxLevelDry), 2), P.fresnelMaxLevelDry);
    float fresnelDry = smin(pow(input, P.fresnelEXPDry) + P.fresnelCDry, P.fresnelMaxLevelDry);
  #else
    float fresnelDry = fresnel;
  #endif

  float origFinalMult = P.finalMult;
  if (P.useStereoApproach){
    P.finalMult = saturate(2.0 * origFinalMult);
  }

  float reflBlurBase = saturate(1 - P.ksSpecularEXP / (P.isCarPaint && IS_ADDITIVE_VAR == 2 ? 8 : 255));
  float reflBlur = pow(reflBlurBase, 2) * 6;
  P.resultBlur = reflBlurBase * 6;

  float3 reflDir = P.forceReflectedDir ? P.forcedReflectedDir : GET_REFL_DIR(toCamera, normalW);
  float cplMult = getCPLMult(reflDir);
  P.finalMult *= cplMult;

  reflDir = fixReflDir(reflDir, posC, normalW, reflBlurBase);

  float3 reflColor = (P.useSkyColor 
    ? getSkyColor(reflDir).rgb 
    : getReflectionAt(reflDir, -toCamera, reflBlur, P.useBias, 
      #ifdef USE_SECONDARY_REFLECTION_LAYER
        P.coloredReflections, P.coloredReflectionsColor,
        P.secondaryLevel, P.secondaryIntensity, 
      #endif
      P.reflectionSampleParam)) * P.finalMult;

  if (P.useGlobalOcclusionMult) {
    reflColor *= P.globalOcclusionMult;
  }

  #ifdef DIM_REFLECTTIONS_WITH_SAMPLE_PARAM
    reflColor *= (P.reflectionSampleParam).x;
  #endif

  #ifndef USE_SECONDARY_REFLECTION_LAYER
    reflColor = lerp(reflColor, reflColor * P.coloredReflectionsColor, P.coloredReflections);
  #endif

  #ifdef USE_FRESNEL_MAX_LEVEL_AS_FINAL_FRESNEL
    fresnel = P.fresnelMaxLevel;
  #endif

  #ifndef GAMMA_FIX
    fresnel = pow(saturate(fresnel), extFresnelGamma);
  #endif
  P.resultPower = fresnel * P.finalMult;
  P.resultPower *= luminance(lerp(1, P.coloredReflectionsColor, P.coloredReflections));
  // P.resultPower += round(P.coloredReflections * 10) * 2; // TODO: SSLR+Metalness
  P.resultColor = reflColor * fresnel;

  #ifdef USE_SECONDARY_REFLECTION_LAYER
    P.coloredReflections = lerp(P.coloredReflections, 0, dot(P.secondaryIntensity, 0.1));
  #endif

  float3 finalColor = lighting.rgb;
  if (!(P.useSkyColor || (P.isCarPaint ? IS_ADDITIVE_VAR == 1 : IS_ADDITIVE_VAR))){
    float fixBase = saturate(P.metallicFix * fresnel);
    fixBase = GAMMA_OR(sqrt(fixBase), fixBase); // super experimental
    finalColor *= saturate(1 - fixBase * (P.useStereoApproach || P.useMatteFix ? P.finalMult : cplMult));
  }
  finalColor += reflColor * fresnel;
  return float4(finalColor, (P.isCarPaint ? IS_ADDITIVE_VAR == 1 : IS_ADDITIVE_VAR) 
    ? lighting.w : lerp(lighting.w, 1, fresnelDry));
    
  #ifdef ALLOW_PERVERTEX_AO_DEBUG__NOREFL
    }
  #endif
}

float3 calculateReflection(float3 lighting, float3 toCamera, float3 posC, float3 normalW, inout ReflParams P){
  return calculateReflection(float4(lighting, 1), toCamera, posC, normalW, P).rgb;
}

ReflParams getReflParamsZero(){
  ReflParams R = (ReflParams)0;
  R.useBias = false;
  R.finalMult = 1;
  R.metallicFix = 1;
  return R;
}

#if !defined(NO_CARPAINT) && defined(OBJECT_SHADER)

  ReflParams getReflParams(float3 specColor, float extraMultiplier, float3 txMapsValue){
    ReflParams result = (ReflParams)0;
    result.fresnelEXP = fresnelEXP;
    result.ksSpecularEXP = txMapsValue.y * INPUT_SPECULAR_EXP;
    result.finalMult = txMapsValue.z;
    result.metallicFix = 1;
    result.reflectionSampleParam = REFL_SAMPLE_PARAM_DEFAULT;
    result.coloredReflections = 1;
    result.coloredReflectionsColor = specColor;
    #ifdef CALCULATEREFLECTION_USEFRESNELMULT_AS_VALUE
      result.fresnelMaxLevel = extraMultiplier;
      result.fresnelC = fresnelC;
    #else
      result.fresnelMaxLevel = fresnelMaxLevel * extraMultiplier;
      result.fresnelC = fresnelC * extraMultiplier;
    #endif    
    return result;
  }

  ReflParams getReflParamsBase(float extraMultiplier = 1, float3 txMapsValue = 1){
    ReflParams result = (ReflParams)0;
    result.fresnelEXP = fresnelEXP;
    result.ksSpecularEXP = txMapsValue.y * INPUT_SPECULAR_EXP;
    result.finalMult = txMapsValue.z;
    result.metallicFix = 1;
    result.reflectionSampleParam = REFL_SAMPLE_PARAM_DEFAULT;
    #ifdef CALCULATEREFLECTION_USEFRESNELMULT_AS_VALUE
      result.fresnelMaxLevel = extraMultiplier;
      result.fresnelC = fresnelC;
    #else
      result.fresnelMaxLevel = fresnelMaxLevel * extraMultiplier;
      result.fresnelC = fresnelC * extraMultiplier;
    #endif    
    return result;
  }

#endif

#endif

// Lighting

float3 getAmbientAt(float3 posC, float3 dirW, float4 ambientAoMult){
  #ifndef NO_AMBIENT_MULT
    ambientAoMult.xyz *= perObjAmbientMultiplier;
  #endif
  #if defined(ALLOW_SH_AMBIENT) && !defined(NO_EXTAMBIENT)
    float3 base = getAmbientBaseAt(posC, dirW, ambientAoMult);
    if (GAMMA_FIX_ACTIVE) {
      // if (IS_TRACK_MATERIAL) return base;
      // float3 sampled = SAMPLE_REFLECTION_FN(dirW, 100, false, REFL_SAMPLE_PARAM_DEFAULT);
      // return sampled * (dot(base, 1) / max(1e-9, dot(sampled, 1)));
      return base;
    } else {      
      float3 sampled = SAMPLE_REFLECTION_FN(dirW, 100, false, REFL_SAMPLE_PARAM_DEFAULT);
      return lerp(base, 
        max(
          base * extIBLAmbientBaseThreshold, 
          extIBLAmbientBrightness * lerp(dot(sampled, 0.333), sampled, extIBLAmbientSaturation)) * ambientAoMult.xyz, 
        IS_TRACK_MATERIAL ? 0 : extIBLAmbient);
    }
  #else
    return getAmbientBaseAt(posC, dirW, ambientAoMult);
  #endif
}

float3 calculateAmbient(float3 posC, float3 normalW, float4 ambientAoMult){
  return getAmbientAt(posC, normalW, ambientAoMult) * GAMMA_OR(1, INPUT_AMBIENT_K);
}

float3 calculateEmissiveSpecial(float3 txDiffuseValue, float3 txEmissiveValue){
  // Used only by MODE_EMISSIVESAMPLE and MODE_SIMPLEST
  #ifdef USE_TXEMISSIVE_AS_EMISSIVE
    return 0;
  #else
    return (extForceEmissive >= 0 ? extForceEmissive : INPUT_EMISSIVE3) 
      * (txDiffuseValue * txEmissiveValue) 
      * getEmissiveMult();
  #endif
}

#ifndef TXEMISSIVE_PASSTHROUGH
  #define TXEMISSIVE_PASSTHROUGH 0
#endif

float3 limitEmissiveIntensity(float3 emissive) {
  #if defined(DIM_NEARBY_EMISSIVES) 
    float limit = extWhiteRefPoint;
    if (HAS_FLAG(FLAG_DIM_EMISSIVE_IN_CUBEMAP)) {
      float v = max(emissive.r, max(emissive.g, emissive.b));
      if (v > limit) emissive *= limit / v;
    }
  #endif
  return emissive;
}

float3 calculateEmissive(float3 posC, float3 txDiffuseValue, float3 txEmissiveValue){
  #ifdef SIMPLER_LIGHTING
    return limitEmissiveIntensity(calculateEmissiveSpecial(txDiffuseValue, txEmissiveValue));
  #endif

  #ifdef DIM_NEARBY_EMISSIVES
    if (RENDERING_REFLECTION_CUBEMAP){
      txEmissiveValue *= saturate(dot(posC, posC) / 200 - 0.25);
    }
  #endif
  if (TXEMISSIVE_PASSTHROUGH) {
    return limitEmissiveIntensity(txEmissiveValue);
  }
  if (GAMMA_FIX_ACTIVE && HAS_FLAG(FLAG_EMISSIVE_FIXED)){
    #ifdef USE_TXEMISSIVE_AS_EMISSIVE
      return limitEmissiveIntensity(txEmissiveValue);
    #else
      return limitEmissiveIntensity(INPUT_EMISSIVE3 * txDiffuseValue * txEmissiveValue);
    #endif
  }
  #ifdef USE_TXEMISSIVE_AS_EMISSIVE
    return limitEmissiveIntensity(GAMMA_LINEAR(txEmissiveValue) * getEmissiveMult());
  #else
    return limitEmissiveIntensity(GAMMA_LINEAR(INPUT_EMISSIVE3 * txDiffuseValue * txEmissiveValue) * getEmissiveMult());
  #endif
}

// Basic shaders

float3 simplerLighting(float3 normalW, float3 txDiffuseValue, float4 ambientAoMult, float3 txEmissiveValue){
  return saturate(0.6 + 0.4 * normalW.y) * INPUT_AMBIENT_K * ambientAoMult.xyz * txDiffuseValue.rgb
    + calculateEmissiveSpecial(txDiffuseValue, txEmissiveValue);
}

struct LightingParams {
  float3 posC;
  float3 toCamera;
  float3 normalW;
  float3 shadow;
  float3 txDiffuseValue;
  float3 txSpecularValue;
  float3 txEmissiveValue;
  // float3 txMapsValue;
  float4 ambientAoMult;
  float specularValue;
  float specularExp;
  bool txMapsApplied;

  #ifdef HAS_SUN_SPECULAR
    float sunSpecularValue;
    float sunSpecularExp;
  #endif

  LightingParams fill(float3 posC, float3 toCamera, float3 normalW, float3 txDiffuseValue, float3 shadow, float4 ambientAoMult, float directedMult){
    this.posC = posC;
    this.toCamera = toCamera;
    this.normalW = normalW;
    this.txDiffuseValue = txDiffuseValue;
    this.txSpecularValue = 1;
    this.txEmissiveValue = 1;
    this.shadow = shadow * directedMult;
    this.ambientAoMult = ambientAoMult;
    this.specularValue = INPUT_SPECULAR_K;
    this.specularExp = INPUT_SPECULAR_EXP;
    #ifdef HAS_SUN_SPECULAR
      this.sunSpecularValue = sunSpecular;
      this.sunSpecularExp = sunSpecularEXP;
    #endif
    return this;
  }

  LightingParams multAO(float mult){
    this.ambientAoMult *= mult;
    return this;
  }

  LightingParams setSpecularMult(float mult, float expMult){
    #ifdef CALCULATELIGHTING_USESPECULARMULT_AS_VALUE
      #error CALCULATELIGHTING_USESPECULARMULT_AS_VALUE is obsolete 
    #endif
    #ifdef CALCULATELIGHTING_USESPECULAREXPMULT_AS_VALUE
      #error CALCULATELIGHTING_USESPECULAREXPMULT_AS_VALUE is obsolete 
    #endif
  
    specularValue *= mult;
    specularExp *= expMult;
    return this;
  }

  LightingParams applyTxMaps(float3 txMapsValue){    
    txMapsApplied = true;
    specularValue = txMapsValue.x * specularValue;
    specularExp = txMapsValue.y * specularExp + 1;
    #ifdef HAS_SUN_SPECULAR
      sunSpecularValue = (txMapsValue.z * txMapsValue.y) * sunSpecularValue;
      sunSpecularExp = txMapsValue.y * sunSpecularExp + 1;  
    #endif
    return this;
  }

  float3 calculate();
};

LightingParams getLightingParams(float3 posC, float3 toCamera, float3 normalW, float3 txDiffuseValue, float3 shadow, float4 ambientAoMult, float directedMult = 1){
  LightingParams result = (LightingParams)0;
  result.fill(posC, toCamera, normalW, txDiffuseValue, shadow, ambientAoMult, directedMult);
  return result;
}

// #if defined(ALLOW_LIGHTINGFX) && !defined(NO_FOG) && !defined(INCLUDE_PARTICLE_CB)
#if defined(ALLOW_EXTRA_VISUAL_EFFECTS) && !defined(INCLUDE_PARTICLE_CB)
  float3 calculateCao(float3 posW, float3 normalW){
    float4 uv = mul(float4(posW, 1), extCaoUV);
    float v = txColorShadow.SampleLevel(samLinearBorder0, uv.xy, 2).w;
    float d = extCaoBias + saturate(uv.z - v) * 8;
    return txColorShadow.SampleLevel(samLinearBorder1, uv.xy, d + saturate(-normalW.y) * d).rgb;
  }

  void applyCao(inout float3 shadow, inout float3 aoMult, float2 shadowTex0, float3 posC, float3 normalW){
    [branch]
    if (HAS_FLAG(FLAG_USE_CAO)){
      shadow *= txColorShadow.SampleLevel(samLinearBorder1, shadowTex0 * float2(0.5, -0.5) + float2(0.5, 0.5), 0).rgb;
      aoMult *= calculateCao(posC, normalW);
    }
  }
  #define APPLY_CAO_CUSTOM(SHADOW, AO) \
    float _ensureCaoUnique;\
    applyCao(SHADOW, AO, pin.ShadowTex0.xy, PIN_POSC_POSITION + ksRealCameraPosition, normalW);
#else
  void applyCao(inout float3 shadow, inout float3 aoMult, float2 shadowTex0, float3 posC, float3 normalW){}
  #define APPLY_CAO_CUSTOM(SHADOW, AO) \
    float _ensureCaoUnique;
#endif

#define APPLY_CAO APPLY_CAO_CUSTOM(L.shadow, L.ambientAoMult.rgb);

#if defined(NO_CARPAINT) || defined(CARPAINT_SIMPLE) || defined(CARPAINT_NM) || defined(CARPAINT_NM_UVMULT) || defined(CARPAINT_GLASS)

#ifndef SUNLIGHT_LAMBERT
  #define SUNLIGHT_LAMBERT(N, L) LAMBERT(N, L)
#endif

float3 calculateLighting_base_LFM(float3 posC, float3 normalW, float3 txDiffuseValue, float3 shadow, float4 ambientAoMult){
  float dotValue = SUNLIGHT_LAMBERT(normalW, -ksLightDirection.xyz);
  float3 light = ksLightColor.rgb * (GAMMA_OR(1, INPUT_DIFFUSE_K) * shadow) * dotValue;
  // return (light + calculateAmbient(posC, normalW, ambientAoMult)) * txDiffuseValue.rgb * 2;
  return applyGamma(light, calculateAmbient(posC, normalW, ambientAoMult),
    txDiffuseValue.rgb, INPUT_DIFFUSE_K, INPUT_AMBIENT_K);
}

float3 calculateLighting_spec(LightingParams L){
  #ifdef CALCULATELIGHTING_USETXSPECULAR_AS_REFLECTANCE_MODEL
    return L.txSpecularValue * L.specularValue * L.shadow * extSpecularColor;
  #endif
  return reflectanceModel(-L.toCamera, -ksLightDirection.xyz, L.normalW, max(L.specularExp, 1)) 
    * L.specularValue * L.shadow * extSpecularColor * L.txSpecularValue;
}

float3 calculateLighting(LightingParams L){
  #ifdef SIMPLEST_LIGHTING
    return SIMPLEST_LIGHTING_FN(L.txDiffuseValue);
  #endif

  #ifdef SIMPLER_LIGHTING
    return simplerLighting(L.normalW, L.txDiffuseValue, L.ambientAoMult, L.txEmissiveValue);
  #endif

  #if defined(SIMPLIFED_SPECULARS) && defined(NO_EXTSPECULAR)
    return LFM_SUM(calculateLighting_base_LFM(L.posC, L.normalW, L.txDiffuseValue, L.shadow, L.ambientAoMult)
      + LFM_CONTRIB(calculateEmissive(L.posC, L.txDiffuseValue.rgb, L.txEmissiveValue)));
  #else
    return LFM_SUM(calculateLighting_base_LFM(L.posC, L.normalW, L.txDiffuseValue, L.shadow, L.ambientAoMult)
      + LFM_CONTRIB(calculateLighting_spec(L))
      + LFM_CONTRIB(calculateEmissive(L.posC, L.txDiffuseValue.rgb, L.txEmissiveValue)));
  #endif
}

float3 LightingParams::calculate(){
  return calculateLighting(this);
}

// MultiMap shaders

#elif defined(OBJECT_SHADER)

float4 getTxDetailValue(float2 uv, bool sampleBias = false){
  #ifdef TXDETAIL_VARIATION
    [branch]
    if (TXDETAIL_VARIATION) {
      return textureSampleVariation(txDetail, samLinear, uv * INPUT_DETAIL_UV_MULT, 
        sampleBias ? 0.8 : 1, 0.05);
    } else {
  #endif
  return txDetail.SampleBias(samLinear, uv * INPUT_DETAIL_UV_MULT, sampleBias ? -0.5 : 0);
  #ifdef TXDETAIL_VARIATION
    }
  #endif
}

float considerDetails(float2 uv, inout float4 txDiffuseValue, inout float3 txMapsValue, bool sampleBias = false){
  #if defined(REDUCE_DIFFUSE_AO) && defined(DIFFUSE_USUALLY_CONTAINS_AO)
    float maxValue = max(max(txDiffuseValue.x, max(txDiffuseValue.y, txDiffuseValue.z)), txDiffuseValue.w);
    txDiffuseValue /= lerp(1, max(maxValue, 0.1), 0.92);
  #endif
  if (!INPUT_USE_DETAIL) return 1;
  float4 txDetailValue = getTxDetailValue(uv, sampleBias);
  txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, 1 - txDiffuseValue.a);
  txMapsValue.x = lerp(txMapsValue.x, txMapsValue.x * txDetailValue.a, 1 - txDiffuseValue.a);
  return lerp(1, txDetailValue.a, 1 - txDiffuseValue.a);
}

float considerDetails(float2 uv, inout float4 txDiffuseValue, bool sampleBias = false){
  #if defined(REDUCE_DIFFUSE_AO) && defined(DIFFUSE_USUALLY_CONTAINS_AO)
    float maxValue = max(max(txDiffuseValue.x, max(txDiffuseValue.y, txDiffuseValue.z)), txDiffuseValue.w);
    txDiffuseValue /= lerp(1, max(maxValue, 0.1), 0.92);
  #endif
  if (!INPUT_USE_DETAIL) return 1;
  float4 txDetailValue = getTxDetailValue(uv, sampleBias);
  txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, 1 - txDiffuseValue.a);
  return lerp(1, txDetailValue.a, 1 - txDiffuseValue.a);
}

#ifndef NMDETAILS_DEFAULT_SAMPLER
  #ifdef WINDSCREEN_REPROJECTION_CRAZE
    #define NMDETAILS_DEFAULT_SAMPLER samLinearSimple
  #else
    #define NMDETAILS_DEFAULT_SAMPLER samLinear
  #endif
#endif

#ifdef CARPAINT_NMDETAILS
float considerNmDetails(float2 uv, float3 tangentW, float3 bitangentW,
    inout float4 txDiffuseValue, inout float3 txMapsValue, inout float3 normalW
    #ifdef USE_HILL12_REORIENTATION
    , float3 txNormalValue
    #endif
    #ifdef CARPAINT_NMDETAILS_PBR_MODE
    , bool usePBRFormat, float blurColor, out float3 pbrValues
    #endif
    #ifdef CARPAINT_NMDETAILS_ALPHA
    , out float alpha
    #endif
    , inout float directedMult){
  #if defined(REDUCE_DIFFUSE_AO) && defined(DIFFUSE_USUALLY_CONTAINS_AO)
    float maxValue = max(max(txDiffuseValue.x, max(txDiffuseValue.y, txDiffuseValue.z)), txDiffuseValue.w);
    txDiffuseValue /= lerp(1, max(maxValue, 0.1), 0.92);
  #endif

  #ifdef NO_NORMALMAPS
    #ifdef CARPAINT_NMDETAILS_PBR_MODE
      pbrValues = 1;
    #endif

    #ifdef CARPAINT_NMDETAILS_ALPHA
      alpha = 1;
    #endif

    #ifdef CONSIDERNMDETAILS_LOAD_TX
      txDiffuseValue = txDiffuse.Sample(NMDETAILS_DEFAULT_SAMPLER, uv);
      txMapsValue = txMaps.Sample(NMDETAILS_DEFAULT_SAMPLER, uv).xyz;
    #endif

    if (INPUT_USE_DETAIL){
      uv *= INPUT_DETAIL_UV_MULT;
      float4 txDetailValue = txDetail.Sample(NMDETAILS_DEFAULT_SAMPLER, uv);

      #if !defined(CARPAINT_SKINNED_NM)
        txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, 1 - txDiffuseValue.a);
      #endif
    }

    return 1;
  #else
    if (INPUT_USE_DETAIL){
      uv *= INPUT_DETAIL_UV_MULT;

      #ifndef CARPAINT_NMDETAILS_PBR_MODE
        float blurColor = 0;
      #endif

      #if defined(USE_MULTIMAP_TEXTURE_NM_VARIATION) && !defined(NO_MULTIMAP_TEXTURE_VARIATION)
        float4 txNormalDetailValue;
        float4 txDetailValue;
        
        [branch]
        if (ksAlphaRef == -193){
          txNormalDetailValue = textureSampleVariation(txNormalDetail, NMDETAILS_DEFAULT_SAMPLER, uv, 0.5);
          txDetailValue = textureSampleVariation(txDetail, NMDETAILS_DEFAULT_SAMPLER, uv, lerp(0.5, 10, blurColor));
        } else {
          txNormalDetailValue = txNormalDetail.Sample(NMDETAILS_DEFAULT_SAMPLER, uv);
          txDetailValue = txDetail.SampleBias(NMDETAILS_DEFAULT_SAMPLER, uv, blurColor * 10);
        }
      #else
        #ifdef CARPAINT_NMDETAILS_ALPHA
          float4 txNormalDetailValue = txNormalDetail.Sample(NMDETAILS_DEFAULT_SAMPLER, uv);
        #else
          float4 txNormalDetailValue = txNormalDetail.Sample(samLinearSimple, uv);
        #endif
        float4 txDetailValue = txDetail.Sample(NMDETAILS_DEFAULT_SAMPLER, uv);
      #endif

      #ifdef CARPAINT_NMDETAILS_ALPHA
        alpha = txNormalDetailValue.a;
      #endif

      #ifdef CARPAINT_NMDETAILS_PBR_MODE
        if (usePBRFormat){
          pbrValues = float3(txNormalDetailValue.zw, txDetailValue.w);
          txDetailValue.a = 1;
          txNormalDetailValue.z = sqrt(saturate(1 - dot2(txNormalDetailValue.xy * 2 - 1))) * 0.5 + 0.5;
        } else {
          pbrValues = float3(1, 0, 1);
        }
      #endif

      float transparency = 1 - txDiffuseValue.a;
      #ifdef USE_HILL12_REORIENTATION

        float3x3 m = float3x3(tangentW, normalW, bitangentW);
        #ifdef USE_HILL12_REORIENTATION_UDN
          float3 t = txNormalValue.xzy * 2.0 - 1.0;
          float3 u = lerp(float3(0, 0, 1), txNormalDetailValue.xyz * 2.0 - 1.0, detailNormalBlend * transparency);
          normalW = normalize(mul(float3(t.xy + u.xy, t.z), m));
        #else
          float3 t = txNormalValue.xzy * 2 + float3(-1, -1, 0);
          float3 u = lerp(float3(0, 0, 1), txNormalDetailValue.xyz * float3(-2, -2, 2) + float3(1, 1, -1), detailNormalBlend * transparency);
          float3 n = mul(t * dot(t, u) - u * t.z, m) + 0.00001;
          normalW = normalize(n);
        #endif

      #else
        float3 txNormalDetailAligned = txNormalDetailValue.xyz * 2 - 1;
        float3x3 m = float3x3(cross(normalW, tangentW), normalW, cross(normalW, bitangentW));
        float3 normalBase = mul(transpose(m), txNormalDetailAligned.xzy);
        float3 normalDetailW = normalize(normalBase);

        #ifdef CARPAINT_NMDETAILS_NO_ALPHA_MASK
          normalW = normalize(lerp(normalW, normalDetailW, detailNormalBlend));
        #else
          normalW = normalize(lerp(normalW, normalDetailW, detailNormalBlend * transparency));
        #endif

        directedMult *= length(normalBase);
      #endif

      #if !defined(CARPAINT_SKINNED_NM)
        txDiffuseValue = lerp(txDiffuseValue, txDetailValue * txDiffuseValue, transparency);
        txMapsValue.x = lerp(txMapsValue.x, txMapsValue.x * txDetailValue.a, transparency);
        if (HAS_FLAG(FLAG_MATERIAL_2)) {
          txMapsValue.z = lerp(txMapsValue.z, txMapsValue.z * txDetailValue.a, transparency);
        }
      #endif

      return lerp(1, txDetailValue.a, 1 - txDiffuseValue.a);
    } else {
      #ifdef CARPAINT_NMDETAILS_PBR_MODE
        pbrValues = 1;
      #endif
      #ifdef CARPAINT_NMDETAILS_ALPHA
        alpha = 1;
      #endif
      return 1;
    }
  #endif
}
#endif

#define NO_SUNSPEC_MAT (defined(CARPAINT_NMDETAILS) || defined(CARPAINT_SIMPLE_REFL) || defined(CARPAINT_AT) || defined(CARPAINT_SKINNED))

#ifndef SUMMARIZE_DIFFUSE_COMPONENT
  #define SUMMARIZE_DIFFUSE_COMPONENT(X, Y, Z) ((X) * (Y))
#endif

#if !NO_SUNSPEC_MAT
float3 calculateMapsLighting_wSun(LightingParams L){
  if (!L.txMapsApplied) return 0/0;
  
  #ifdef SIMPLEST_LIGHTING
    return SIMPLEST_LIGHTING_FN(L.txDiffuseValue);
  #endif

  #ifdef SIMPLER_LIGHTING
    return simplerLighting(L.normalW, L.txDiffuseValue, L.ambientAoMult, L.txEmissiveValue);
  #endif

  float lightValue = LAMBERT(L.normalW, -ksLightDirection.xyz);
  float3 diffuse = ksLightColor.rgb * GAMMA_OR(1, INPUT_DIFFUSE_K) * lightValue;

  float specularBase = saturate(dot(normalize(-L.toCamera - ksLightDirection.xyz), L.normalW));
  #ifdef SIMPLIFED_SPECULARS
    float3 sunSpecularResult = pow(specularBase, max(L.sunSpecularExp, 1)) * L.shadow;
    float3 specularPart = sunSpecularResult * L.sunSpecularValue * extSunSpecularMult * extSpecularColor * L.txSpecularValue;
  #else
    float3 baseSpecularResult = pow(specularBase, max(L.specularExp, 1)) * lerp(1, getBaseSpecularColor(L.txDiffuseValue.xyz, extColoredReflectionNorm), extColoredBaseReflection);
    float3 sunSpecularResult = pow(specularBase, max(L.sunSpecularExp, 1)) * L.txSpecularValue;
    float3 specularPart = (baseSpecularResult * L.specularValue + sunSpecularResult * L.sunSpecularValue * extSunSpecularMult) * L.shadow * extSpecularColor;
  #endif

  #ifdef GAMMA_FIX
    float3 diffuseAmbient = SUMMARIZE_DIFFUSE_COMPONENT(diffuse * L.shadow, applyGamma(L.txDiffuseValue, INPUT_DIFFUSE_K), lightValue * L.shadow) + SUMMARIZE_DIFFUSE_COMPONENT(calculateAmbient(L.posC, L.normalW, L.ambientAoMult), applyGamma(L.txDiffuseValue, INPUT_AMBIENT_K), lightValue * L.shadow);
  #else
    float3 diffuseSum = diffuse * L.shadow + calculateAmbient(L.posC, L.normalW, L.ambientAoMult);
    float3 diffuseAmbient = SUMMARIZE_DIFFUSE_COMPONENT(diffuseSum, L.txDiffuseValue, lightValue * L.shadow);
  #endif

  return diffuseAmbient
    + specularPart * lightValue
    + calculateEmissive(L.posC, L.txDiffuseValue, L.txEmissiveValue);
}
#endif

float3 calculateMapsLighting_woSun(LightingParams L){
  if (!L.txMapsApplied) return 0/0;

  #ifdef SIMPLEST_LIGHTING
    return SIMPLEST_LIGHTING_FN(L.txDiffuseValue);
  #endif

  #ifdef SIMPLER_LIGHTING
    return simplerLighting(L.normalW, L.txDiffuseValue, L.ambientAoMult, L.txEmissiveValue);
  #endif

  float lightValue = LAMBERT(L.normalW, -ksLightDirection.xyz);
  float3 diffuse = ksLightColor.rgb * GAMMA_OR(1, INPUT_DIFFUSE_K) * lightValue;

  float specularBase = saturate(dot(normalize(-L.toCamera - ksLightDirection.xyz), L.normalW));
  float3 specularPart = pow(specularBase, max(L.specularExp, 1)) * L.shadow * L.specularValue * extSpecularColor;  

  #ifdef GAMMA_FIX
    float3 diffuseAmbient = SUMMARIZE_DIFFUSE_COMPONENT(diffuse * L.shadow, applyGamma(L.txDiffuseValue, INPUT_DIFFUSE_K), lightValue * L.shadow) + SUMMARIZE_DIFFUSE_COMPONENT(calculateAmbient(L.posC, L.normalW, L.ambientAoMult), applyGamma(L.txDiffuseValue, INPUT_AMBIENT_K), lightValue * L.shadow);
  #else
    float3 diffuseSum = diffuse * L.shadow + calculateAmbient(L.posC, L.normalW, L.ambientAoMult);
    float3 diffuseAmbient = SUMMARIZE_DIFFUSE_COMPONENT(diffuseSum, L.txDiffuseValue, lightValue * L.shadow);
  #endif

  return diffuseAmbient 
    + specularPart * lightValue * L.txSpecularValue
    + calculateEmissive(L.posC, L.txDiffuseValue, L.txEmissiveValue);
}

float3 calculateMapsLighting(LightingParams L){
  if (!L.txMapsApplied) return 0/0;

  #ifdef SIMPLEST_LIGHTING
    return SIMPLEST_LIGHTING_FN(L.txDiffuseValue);
  #endif

  #ifdef SIMPLER_LIGHTING
    return simplerLighting(L.normalW, L.txDiffuseValue, L.ambientAoMult, L.txEmissiveValue);
  #endif

  #if NO_SUNSPEC_MAT
    return calculateMapsLighting_woSun(L);
  #else
    return calculateMapsLighting_wSun(L);
  #endif
}

float3 LightingParams::calculate(){
  return calculateMapsLighting(this);
}

#endif

float3 calculateBounceBackLighting(float3 toCamera, float3 normalW,
    float3 txDiffuseValue, float3 txBounceBackValue, float shadow, float expValue){
  float3 lightValue = ksLightColor.rgb * txDiffuseValue * shadow;
  return pow(saturate(dot(toCamera, ksLightDirection.xyz)), expValue) * lightValue * txBounceBackValue;
}

#include "utils_ps_fog.fx"

#if defined(ALLOW_EXTRA_SHADOW) // && !defined(NO_SHADOWS)
  float2 getExtraShadow(float3 pos){
    float3 rel = pos - shadowPos;
    float dist = dot(rel, rel);

    float result = saturate(dist * shadowR1 + shadowR2);
    // if (GAMMA_FIX_ACTIVE) result = pow(result, 2);
    return GAMMA_LINEAR_SIMPLE(float2(result, lerp(1, result, sceneShadowOpacity)));
  }

  #define APPLY_EXTRA_SHADOW \
      float2 extraShadow = getExtraShadow(SPSAwarePosC);

  #define APPLY_EXTRA_SHADOW_INC_DIFFUSE \
      float2 extraShadow = getExtraShadow(SPSAwarePosC);\
      shadow *= extraShadow.x;
#else
  float getExtraShadow(float3 pos){
    return 1;
  }

  #define APPLY_EXTRA_SHADOW float2 extraShadow = 1;
  #define APPLY_EXTRA_SHADOW_INC_DIFFUSE float2 extraShadow = 1;
#endif

// Warning: extra shadow is not applied with G-buffer
#ifdef EXTRA_SHADOW_AFFECTS_REFLECTION
  #define EXTRA_SHADOW_REFLECTION extraShadow.x
#else
  #define EXTRA_SHADOW_REFLECTION 1
#endif

#ifdef ALLOW_EXTRA_SHADOW
  #define GET_EXTRA_SHADOW_OCCLUSION(REFL_DIR) (getExtraShadow(SPSAwarePosC + REFL_DIR * shadowReflectionOcclusion).x)
  #define APPLY_EXTRA_SHADOW_OCCLUSION(R)\
    if (shadowReflectionOcclusion) { R.useGlobalOcclusionMult = true; R.globalOcclusionMult = GET_EXTRA_SHADOW_OCCLUSION(reflect(toCamera, normalW)); }
#else
  #define GET_EXTRA_SHADOW_OCCLUSION(REFL_DIR) 1
  #define APPLY_EXTRA_SHADOW_OCCLUSION(R) 
#endif

#if (defined(MODE_KUNOS) || !defined(OBJECT_SHADER)) && !defined(INPUT_SEASION_AUTUMN) && !defined(INPUT_SEASION_WINTER)

  #define ADJUSTCOLOR(v) 
  #define ADJUSTCOLOR_PV(v) 

#else

  float posBasedVariationW(float3 posW){
    return txNoise.SampleLevel(samLinearSimple, posW.xz / 1000, 0).x;
  }

  #ifndef INPUT_SEASION_AUTUMN
    #define INPUT_SEASION_AUTUMN seasonAutumn
  #endif

  #ifndef INPUT_SEASION_WINTER
    #define INPUT_SEASION_WINTER seasonWinter
  #endif

  float3 adjustTextureColor(float3 color, float3 normal, float variation, float3 posC){
    #ifdef NO_ADJUSTING_COLOR
      return color;
    #else
      [branch]
      if (INPUT_SEASION_WINTER + INPUT_SEASION_AUTUMN 
        #if defined(USE_MULTIMAP_TEXTURE_NM_VARIATION) && !defined(NO_GRASSFX_COLOR)
          + HAS_FLAG(FLAG_APPLY_GRASSFX_COLOR)
        #endif
          > 0){
        float leaf = saturate(INPUT_SEASION_WINTER * saturate(normal.y) + (color.g * 2 - color.r - color.b) * 20);

        if (INPUT_SEASION_AUTUMN > 0){
          color = lerp(color, float3(
            saturate(color.r * 0.2 + color.g * 1.6), 
            saturate(color.g * (1.3 - variation * variation) + color.r * 0.2), 
            saturate(color.b - 0.4 * color.g)), INPUT_SEASION_AUTUMN * leaf);
        }

        color = lerp(color, saturate(luminance(color) * ((float3(1, 1.1, 1.2) * 1.6 + saturate(INPUT_SEASION_WINTER * 2 - 1) * 0.4) * 0.85)), 
          saturate(INPUT_SEASION_WINTER * 2) * leaf);

        #if defined(USE_MULTIMAP_TEXTURE_NM_VARIATION) && !defined(NO_GRASSFX_COLOR)
          if (HAS_FLAG(FLAG_APPLY_GRASSFX_COLOR)){
            color *= lerp(1, float3(0.8, 0.7, 0.6), saturate(1 - length(posC) * 0.1)); 
          }
        #endif
      }

      return color;
    #endif
  }

  #define ADJUSTCOLOR(v) v.rgb = adjustTextureColor(v.rgb, normalW, 0.5, pin.PosC);
  #define ADJUSTCOLOR_PV(v) v.rgb = adjustTextureColor(v.rgb, normalW, posBasedVariationW(ksInPositionWorld), pin.PosC);

#endif

#ifdef ALLOW_EXTRA_VISUAL_EFFECTS
  #define TILT_BIAS (pow(saturate(1 - abs(dot(normalize(pin.NormalW), toCamera)) * 8), 8) * extTiltBias)
#else
  #define TILT_BIAS 0
#endif

// #define ADJUSTCOLOR(v) v.rgb = v.rgb;
// #define ADJUSTCOLOR_PV(v) v.rgb = v.rgb;