#include "cbuffers_common.fx"

cbuffer _cbBones : register(b13) {
  float4x4 bones[55];
}

#ifdef CREATE_MOTION_BUFFER
cbuffer _cbBonesPrev : register(b10) {
  float4x4 bonesPrev[55];
}
#endif

#ifdef INCLUDE_FLAGS_CB 
cbuffer cbFlags : register(b10) {
  float frequency;
  float distortion;
  float2 boh;
}
#endif
