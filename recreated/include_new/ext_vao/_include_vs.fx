#ifndef AO_MIX_INPUT
  #define AO_MIX_INPUT vaoSecondaryMix
#endif

#ifdef SUPPORTS_OBJSP_AO
  #error No longer supported
#endif

#ifdef SUPPORTS_COLORFUL_AO
  #undef SUPPORTS_COLORFUL_AO
  #define SUPPORTS_NORMALS_AO
#endif

#define ADJUST_AO(V) pow(saturate((V) * extVAOMult + extVAOAdd), GAMMA_VAO_EXP(extVAOExp))

#if defined(ALLOW_PERVERTEX_AO) && defined(SUPPORTS_NORMALS_AO)
  #ifdef USE_AO_SPLITTING
    #define PREPARE_AO(x) x = ADJUST_AO(lerp(vsLoadAo0(vin.TangentPacked), vsLoadAo1(vin.TangentPacked), AO_MIX_INPUT));
    #define ORIGINAL_AO_VALUE vsLoadAo0(vin.TangentPacked)
  #else
    #define PREPARE_AO(x) x = ADJUST_AO(vsLoadAo0(vin.TangentPacked));
    #define ORIGINAL_AO_VALUE vsLoadAo0(vin.TangentPacked)
  #endif
#elif defined(ALLOW_PERVERTEX_AO) && defined(SUPPORTS_OBJSP_AO)
  #define PREPARE_AO(x) x = ADJUST_AO(length(vin.NormalL) * 2 - 1);
  #define ORIGINAL_AO_VALUE (length(vin.NormalL) * 2 - 1)
#elif defined(ALLOW_PERVERTEX_AO) && defined(SET_AO_TO_ONE)
  #define PREPARE_AO(x) x = 1;
  #define ORIGINAL_AO_VALUE 1
#else
  #define PREPARE_AO(x) 
#endif

// #undef PREPARE_AO
// #define PREPARE_AO(x) x = vaoSecondaryMix;

