#include "include/common.hlsl"
#include "include/samplers.hlsl"

#ifndef SHADOWS_FILTER_SIZE
  #define SHADOWS_FILTER_SIZE 3
#endif

#if defined(FORCE_DISC_SHADOWS)
	#define USE_DISC_SHADOWS
#elif defined(TARGET_VS)
  #undef USE_DISC_SHADOWS
#elif defined(USE_OPTIMIZED_SHADOWS) || defined(USE_SMOOTH_SHADOWS)
  #undef USE_DISC_SHADOWS
#else
  #define USE_DISC_SHADOWS
  #undef USE_DISC_SHADOWS
#endif

#ifdef USE_DISC_SHADOWS
  #define SHADOW_SAMPLE_ARGS_DECL float deltaZ, float bias, float4 uv, float2 aliasingFix, float tx, float3 posC, float4 posH, float3 normalW, float biasMultiplier, float scaleParam
  #define SHADOW_SAMPLE_ARGS_PREP(DZ, BI, UV, TX) (DZ)*(BI), BI, UV, UV##_fwidth, TX, posC, posH, normalW, biasMultiplier, scaleParam
#else
  #define SHADOW_SAMPLE_ARGS_DECL float deltaZ, float4 uv, float tx
  #define SHADOW_SAMPLE_ARGS_PREP(DZ, BI, UV, TX) (DZ)*(BI), UV, TX
#endif

// from https://github.com/TheRealMJP/Shadows/blob/master/Shadows/Mesh.hlsl
static const float SHADOWS_W[3][3] = { { 0.5, 1.0, 0.5 }, { 1.0, 1.0, 1.0 }, { 0.5, 1.0, 0.5 } };

float cascadeShadowStep_optimized(SHADOW_SAMPLE_ARGS_DECL) {
  float result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  const int FS_2 = SHADOWS_FILTER_SIZE / 2;
  float2 tc = uv.xy;
  float4 s = 0.0f;
  float2 stc = (realTextureSize * tc.xy) + float2(0.5f, 0.5f);
  float2 tcs = floor(stc);
  float2 fc;
  int row;
  int col;
  float w = 0.0f;
  float4 v1[FS_2 + 1];
  float2 v0[FS_2 + 1];
  fc.xy = stc - tcs;
  tc.xy = tcs * pixelSize;

  for (row = 0; row < SHADOWS_FILTER_SIZE; ++row) {
    for (col = 0; col < SHADOWS_FILTER_SIZE; ++col) {
      w += SHADOWS_W[row][col];
    }
  }

  // -- loop over the rows
  [unroll]
  for (row = -FS_2; row <= FS_2; row += 2) {
    [unroll]
    for (col = -FS_2; col <= FS_2; col += 2) {
      float value = SHADOWS_W[row + FS_2][col + FS_2];
      if (col > -FS_2) value += SHADOWS_W[row + FS_2][col + FS_2 - 1];
      if (col < FS_2) value += SHADOWS_W[row + FS_2][col + FS_2 + 1];

      if (row > -FS_2) {
        value += SHADOWS_W[row + FS_2 - 1][col + FS_2];
        if (col < FS_2) value += SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1];
        if (col > -FS_2) value += SHADOWS_W[row + FS_2 - 1][col + FS_2 - 1];
      }

      if (value != 0.0f) {
        float sampleDepth = comparisonValue;
        v1[(col + FS_2) / 2] = txShadowArray.GatherCmp(samShadow, float3(tc.xy, tx), sampleDepth, int2(col, row));
      } else {
        v1[(col + FS_2) / 2] = 0.0f;
      }

      if (col == -FS_2) {
        s.x += (1.0f - fc.y) * (v1[0].w * (SHADOWS_W[row + FS_2][col + FS_2]
                   - SHADOWS_W[row + FS_2][col + FS_2] * fc.x)
                   + v1[0].z * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2]
                   - SHADOWS_W[row + FS_2][col + FS_2 + 1.0f])
                   + SHADOWS_W[row + FS_2][col + FS_2 + 1]));
        s.y += fc.y * (v1[0].x * (SHADOWS_W[row + FS_2][col + FS_2]
                   - SHADOWS_W[row + FS_2][col + FS_2] * fc.x)
                   + v1[0].y * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2]
                   - SHADOWS_W[row + FS_2][col + FS_2 + 1])
                   +  SHADOWS_W[row + FS_2][col + FS_2 + 1]));
        if (row > -FS_2) {
          s.z += (1.0f - fc.y) * (v0[0].x * (SHADOWS_W[row + FS_2 - 1][col + FS_2]
                       - SHADOWS_W[row + FS_2 - 1][col + FS_2] * fc.x)
                       + v0[0].y * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2]
                       - SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1])
                       + SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1]));
          s.w += fc.y * (v1[0].w * (SHADOWS_W[row + FS_2 - 1][col + FS_2]
                    - SHADOWS_W[row + FS_2 - 1][col + FS_2] * fc.x)
                    + v1[0].z * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2]
                    - SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1])
                    + SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1]));
        }
      } else if (col == FS_2) {
        s.x += (1 - fc.y) * (v1[FS_2].w * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2 - 1]
                   - SHADOWS_W[row + FS_2][col + FS_2]) + SHADOWS_W[row + FS_2][col + FS_2])
                   + v1[FS_2].z * fc.x * SHADOWS_W[row + FS_2][col + FS_2]);
        s.y += fc.y * (v1[FS_2].x * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2 - 1]
                   - SHADOWS_W[row + FS_2][col + FS_2] ) + SHADOWS_W[row + FS_2][col + FS_2])
                   + v1[FS_2].y * fc.x * SHADOWS_W[row + FS_2][col + FS_2]);
        if (row > -FS_2) {
          s.z += (1 - fc.y) * (v0[FS_2].x * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2 - 1]
                    - SHADOWS_W[row + FS_2 - 1][col + FS_2])
                    + SHADOWS_W[row + FS_2 - 1][col + FS_2])
                    + v0[FS_2].y * fc.x * SHADOWS_W[row + FS_2 - 1][col + FS_2]);
          s.w += fc.y * (v1[FS_2].w * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2 - 1]
                    - SHADOWS_W[row + FS_2 - 1][col + FS_2])
                    + SHADOWS_W[row + FS_2 - 1][col + FS_2])
                    + v1[FS_2].z * fc.x * SHADOWS_W[row + FS_2 - 1][col + FS_2]);
        }
      } else {
        s.x += (1 - fc.y) * (v1[(col + FS_2) / 2].w * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2 - 1]
                  - SHADOWS_W[row + FS_2][col + FS_2 + 0] ) + SHADOWS_W[row + FS_2][col + FS_2 + 0])
                  + v1[(col + FS_2) / 2].z * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2 - 0]
                  - SHADOWS_W[row + FS_2][col + FS_2 + 1]) + SHADOWS_W[row + FS_2][col + FS_2 + 1]));
        s.y += fc.y * (v1[(col + FS_2) / 2].x * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2-1]
                  - SHADOWS_W[row + FS_2][col + FS_2 + 0]) + SHADOWS_W[row + FS_2][col + FS_2 + 0])
                  + v1[(col + FS_2) / 2].y * (fc.x * (SHADOWS_W[row + FS_2][col + FS_2 - 0]
                  - SHADOWS_W[row + FS_2][col + FS_2 + 1]) + SHADOWS_W[row + FS_2][col + FS_2 + 1]));
        if (row > -FS_2) {
          s.z += (1 - fc.y) * (v0[(col + FS_2) / 2].x * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2 - 1]
                      - SHADOWS_W[row + FS_2 - 1][col + FS_2 + 0]) + SHADOWS_W[row + FS_2 - 1][col + FS_2 + 0])
                      + v0[(col + FS_2) / 2].y * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2 - 0]
                      - SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1]) + SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1]));
          s.w += fc.y * (v1[(col + FS_2) / 2].w * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2 - 1]
                      - SHADOWS_W[row + FS_2 - 1][col + FS_2 + 0]) + SHADOWS_W[row + FS_2 - 1][col + FS_2 + 0])
                      + v1[(col + FS_2) / 2].z * (fc.x * (SHADOWS_W[row + FS_2 - 1][col + FS_2 - 0]
                      - SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1]) + SHADOWS_W[row + FS_2 - 1][col + FS_2 + 1]));
        }
      }
      if (row != FS_2) v0[(col + FS_2) / 2] = v1[(col + FS_2) / 2].xy;
    }
  }

  return saturate((uv.z > 1 ? 1 : 0) + dot(s, 1.0f) / w);
}

#ifdef USE_SMOOTH_SHADOWS
  #define FilterSize_ 5
#elif SHADOWS_FILTER_SIZE == 4
  #define FilterSize_ 7
#elif SHADOWS_FILTER_SIZE == 3
  #define FilterSize_ 5
#elif SHADOWS_FILTER_SIZE == 2
  #define FilterSize_ 3
#else
  #define FilterSize_ 2
#endif

float SampleShadowMap(in float2 baseUV, in float u, in float v, in float comparisonValue, float tx) {
    float2 uv = baseUV + float2(u, v) * pixelSize;
    return txShadowArray.SampleCmpLevelZero(samShadow, float3(uv, tx), comparisonValue);
}

float cascadeShadowStep_witness(SHADOW_SAMPLE_ARGS_DECL) {
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  float2 uvAdj = uv.xy * realTextureSize;
  float2 baseUV = floor(uvAdj + 0.5);
  float s = uvAdj.x + 0.5 - baseUV.x;
  float t = uvAdj.y + 0.5 - baseUV.y;

  baseUV -= float2(0.5, 0.5);
  baseUV *= pixelSize;

  float sum = 0;
  float mult = saturate(comparisonValue * 40) * saturate((1 - comparisonValue) * 40) * extShadowsOpacityMult;

  #if FilterSize_ == 2
    return lerp(1, txShadowArray.SampleCmpLevelZero(samShadow, float3(uvAdj.xy, 0), comparisonValue), mult);
  #elif FilterSize_ == 3
    float uw0 = (3 - 2 * s);
    float uw1 = (1 + 2 * s);
    float u0 = (2 - s) / uw0 - 1;
    float u1 = s / uw1 + 1;
    float vw0 = (3 - 2 * t);
    float vw1 = (1 + 2 * t);
    float v0 = (2 - t) / vw0 - 1;
    float v1 = t / vw1 + 1;
    sum += uw0 * vw0 * SampleShadowMap(baseUV, u0, v0, comparisonValue, tx);
    sum += uw1 * vw0 * SampleShadowMap(baseUV, u1, v0, comparisonValue, tx);
    sum += uw0 * vw1 * SampleShadowMap(baseUV, u0, v1, comparisonValue, tx);
    sum += uw1 * vw1 * SampleShadowMap(baseUV, u1, v1, comparisonValue, tx);
    return lerp(1, sum / 16, mult);
  #elif FilterSize_ == 5
    float uw0 = (4 - 3 * s);
    float uw1 = 7;
    float uw2 = (1 + 3 * s);
    float u0 = (3 - 2 * s) / uw0 - 2;
    float u1 = (3 + s) / uw1;
    float u2 = s / uw2 + 2;
    float vw0 = (4 - 3 * t);
    float vw1 = 7;
    float vw2 = (1 + 3 * t);
    float v0 = (3 - 2 * t) / vw0 - 2;
    float v1 = (3 + t) / vw1;
    float v2 = t / vw2 + 2;
    sum += uw0 * vw0 * SampleShadowMap(baseUV, u0, v0, comparisonValue, tx);
    sum += uw1 * vw0 * SampleShadowMap(baseUV, u1, v0, comparisonValue, tx);
    sum += uw2 * vw0 * SampleShadowMap(baseUV, u2, v0, comparisonValue, tx);
    sum += uw0 * vw1 * SampleShadowMap(baseUV, u0, v1, comparisonValue, tx);
    sum += uw1 * vw1 * SampleShadowMap(baseUV, u1, v1, comparisonValue, tx);
    sum += uw2 * vw1 * SampleShadowMap(baseUV, u2, v1, comparisonValue, tx);
    sum += uw0 * vw2 * SampleShadowMap(baseUV, u0, v2, comparisonValue, tx);
    sum += uw1 * vw2 * SampleShadowMap(baseUV, u1, v2, comparisonValue, tx);
    sum += uw2 * vw2 * SampleShadowMap(baseUV, u2, v2, comparisonValue, tx);
    return lerp(1, sum / 144, mult);
  #elif FilterSize_ == 7
    float uw0 = (5 * s - 6);
    float uw1 = (11 * s - 28);
    float uw2 = -(11 * s + 17);
    float uw3 = -(5 * s + 1);
    float u0 = (4 * s - 5) / uw0 - 3;
    float u1 = (4 * s - 16) / uw1 - 1;
    float u2 = -(7 * s + 5) / uw2 + 1;
    float u3 = -s / uw3 + 3;
    float vw0 = (5 * t - 6);
    float vw1 = (11 * t - 28);
    float vw2 = -(11 * t + 17);
    float vw3 = -(5 * t + 1);
    float v0 = (4 * t - 5) / vw0 - 3;
    float v1 = (4 * t - 16) / vw1 - 1;
    float v2 = -(7 * t + 5) / vw2 + 1;
    float v3 = -t / vw3 + 3;
    sum += uw0 * vw0 * SampleShadowMap(baseUV, u0, v0, comparisonValue, tx);
    sum += uw1 * vw0 * SampleShadowMap(baseUV, u1, v0, comparisonValue, tx);
    sum += uw2 * vw0 * SampleShadowMap(baseUV, u2, v0, comparisonValue, tx);
    sum += uw3 * vw0 * SampleShadowMap(baseUV, u3, v0, comparisonValue, tx);
    sum += uw0 * vw1 * SampleShadowMap(baseUV, u0, v1, comparisonValue, tx);
    sum += uw1 * vw1 * SampleShadowMap(baseUV, u1, v1, comparisonValue, tx);
    sum += uw2 * vw1 * SampleShadowMap(baseUV, u2, v1, comparisonValue, tx);
    sum += uw3 * vw1 * SampleShadowMap(baseUV, u3, v1, comparisonValue, tx);
    sum += uw0 * vw2 * SampleShadowMap(baseUV, u0, v2, comparisonValue, tx);
    sum += uw1 * vw2 * SampleShadowMap(baseUV, u1, v2, comparisonValue, tx);
    sum += uw2 * vw2 * SampleShadowMap(baseUV, u2, v2, comparisonValue, tx);
    sum += uw3 * vw2 * SampleShadowMap(baseUV, u3, v2, comparisonValue, tx);
    sum += uw0 * vw3 * SampleShadowMap(baseUV, u0, v3, comparisonValue, tx);
    sum += uw1 * vw3 * SampleShadowMap(baseUV, u1, v3, comparisonValue, tx);
    sum += uw2 * vw3 * SampleShadowMap(baseUV, u2, v3, comparisonValue, tx);
    sum += uw3 * vw3 * SampleShadowMap(baseUV, u3, v3, comparisonValue, tx);
    return lerp(1, sum / 2704, mult);
  #endif
}

#include "include/poisson.hlsl"

#ifdef USE_DISC_SHADOWS
  float cascadeShadowStep_disc(SHADOW_SAMPLE_ARGS_DECL) {
    #if defined(TARGET_PS) && defined(ALLOW_EXTRA_VISUAL_EFFECTS)

      #define SHADOW_POISSON_DISK_SIZE 16
      aliasingFix *= pixelSize * 2048;
      float2 sampleScale = clamp(aliasingFix * 0.25, pixelSize * extShadowsBlurMult, pixelSize * 6);
      // float valueOffset = lerp(0.0002, 0.00005, abs(dot(normalW, ksLightDirection.xyz))) * min(tx + 1, 3) * 0.33;

      float aliasingFixAmount = dot2(min(aliasingFix, pixelSize * 16)) * realTextureSizeSqr2;
      float valueOffset = aliasingFixAmount * shadowsRangeInv[tx];
      // deltaZ = lerp(1, 0.2, abs(dot(normalW, ksLightDirection.xyz))) * shadowsExtraBias[tx] * biasMultiplier;
      // deltaZ = lerp(0.1, 1, pow(1 - abs(dot(normalW, ksLightDirection.xyz)), 2)) * (1 + tx) * 0.02 * shadowsRangeInv[tx] * biasMultiplier;
      deltaZ = lerp(0.1, 1, pow(1 - abs(dot(normalW, ksLightDirection.xyz)), 2)) * shadowsExtraBias[tx] * biasMultiplier;

    #else

      #ifndef SHADOW_POISSON_DISK_SIZE
        #define SHADOW_POISSON_DISK_SIZE 6
      #endif
      #ifndef SHADOW_POISSON_DISK_SCALE
        #define SHADOW_POISSON_DISK_SCALE 2
      #endif
      float2 sampleScale = pixelSize * SHADOW_POISSON_DISK_SCALE * scaleParam;
      float valueOffset = 0;

    #endif

    uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);
    float comparisonValue = uv.z - deltaZ;
    float mult = saturate(comparisonValue * 40) * saturate((1 - comparisonValue) * 40) * extShadowsOpacityMult;
    // float2 random = normalize(txNoise.SampleLevel(samPoint, posH.xy / 32, 0).xy);

    float ret = 0;
    float count = 0;
    for (uint i = 0; i < SHADOW_POISSON_DISK_SIZE; ++i) {
      float2 sampleDir = SAMPLE_POISSON(SHADOW_POISSON_DISK_SIZE, i);
      // if (!(i % 4)) sampleDir = reflect(sampleDir, random);
      // sampleDir = reflect(sampleDir, random);

      ret += txShadowArray.SampleCmpLevelZero(samShadow, float3(uv.xy + sampleDir * sampleScale, tx), 
        comparisonValue - SAMPLE_POISSON_LENGTH(SHADOW_POISSON_DISK_SIZE, i) * valueOffset);
      ++count;
    }
    
    return lerp(1, ret / SHADOW_POISSON_DISK_SIZE, mult);
  }
#endif

#ifdef USE_DISC_SHADOWS
  #define cascadeShadowStep cascadeShadowStep_disc
#elif defined(USE_OPTIMIZED_SHADOWS)
  #define cascadeShadowStep cascadeShadowStep_optimized
#elif defined(USE_SMOOTH_SHADOWS)
  #define cascadeShadowStep cascadeShadowStep_witness
#elif defined(MODE_LIGHTMAP)
  #define cascadeShadowStep cascadeShadowStep_single
#else
  #define cascadeShadowStep cascadeShadowStep_witness
  // #define cascadeShadowStep cascadeShadowStep_optimized
#endif

bool checkCascade(float4 uv, float limit = 1) {
  return all(abs(uv.xy) < limit);
}

float cascadeShadowStep_samplecmp(SHADOW_SAMPLE_ARGS_DECL) {
  float result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  [unroll]
  for (int x = -1; x <= 1; x += 2) {
    [unroll]
    for (int y = -1; y <= 1; y += 2) {
      result += txShadowArray.SampleCmpLevelZero( samShadow, float3(uv.xy, tx), comparisonValue, int2(x, y) );
    }
  }

  return result * 0.25;
}

float cascadeShadowStep_sharper(SHADOW_SAMPLE_ARGS_DECL) {
  float4 result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  [unroll]
  for (int x = -1; x <= 0; x += 1) {
    [unroll]
    for (int y = -1; y <= 0; y += 1) {
      result += txShadowArray.GatherCmp( samShadow, float3(uv.xy, tx), comparisonValue, int2(x, y) );
    }
  }

  float4 interpolation = bilinearFactors(uv.xy * realTextureSize - 0.5);
  return dot(result.wzxy, interpolation / 4);
}

float cascadeShadowStep_single(SHADOW_SAMPLE_ARGS_DECL) {
  return uv.z >= 1 ? 1 : txShadowArray.SampleCmpLevelZero(samShadow, float3(uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5), tx), uv.z);
}

float cascadeShadowStep_orig(SHADOW_SAMPLE_ARGS_DECL) {
  float result = 0;
  float comparisonValue = uv.z - deltaZ;
  uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

  float2 rotate_at = float2(frac(10000 * uv.x), frac(11000 * uv.y));

  [unroll]
  for (int x = -1; x <= 0; x++) {
    [unroll]
    for (int y = -1; y <= 0; y++) {
      result += txShadowArray.SampleCmpLevelZero(samShadow, float3(uv.xy + pixelSize * reflect(float2(x, y), rotate_at), tx), comparisonValue);
    }
  }

  return result;
}

#if SHADOWS_FILTER_SIZE == 1
  #define CASCADESHADOWSTEP cascadeShadowStep_single
#else
  #define CASCADESHADOWSTEP cascadeShadowStep
#endif
// #define CASCADESHADOWSTEP cascadeShadowStep_single

float calculateBias(int split){
  return shadowsBias[split];
}

float getShadowBiasMult(float3 posC, float4 posH, float3 normalW, float4 shadowTex0, 
    #ifdef USE_STRETCHED_SHADOWS
    float2 blurDir,
    #endif
    #ifdef USE_SCALE_PARAM
    float scaleParam,
    #endif
    float biasMultiplier, float fallbackShadow) {
  float deltaZBase = max(pow(1 - abs(dot(normalW, ksLightDirection.xyz)), 2), 0.1) * biasMultiplier;

  #ifndef USE_SCALE_PARAM
  float scaleParam = 1;
  #endif

  #ifdef USE_STRETCHED_SHADOWS
    #define ARGS_EXTRA ,blurDir
  #else
    #define ARGS_EXTRA 
  #endif

  #ifdef NO_CLOUD_SHADOW
    float cloudShadowsMult = 1;
  #else
    float cloudShadowsMult = getCloudShadow(posC);
  #endif

  float4 shadowTex1 = float4(shadowTex0.xyz * shadowsMatrixModifier1M + shadowsMatrixModifier1A, 1);
  float4 shadowTex2 = float4(shadowTex0.xyz * shadowsMatrixModifier2M + shadowsMatrixModifier2A, 1);
  float4 shadowTex3 = float4(shadowTex0.xyz * shadowsMatrixModifier3M + shadowsMatrixModifier3A, 1);

  #ifdef USE_LAST_SHADOWS_CASCADE_ONLY

    [branch]
    float shadThis = shadowsMatrixModifier3M.x ? CASCADESHADOWSTEP(SHADOW_SAMPLE_ARGS_PREP(deltaZBase, 0.002, shadowTex3, 3) ARGS_EXTRA) : fallbackShadow;
    return (cloudShadowsMult * (checkCascade(shadowTex3) ? min(shadThis, fallbackShadow) : fallbackShadow));

  #elif defined(USE_FIRST_SHADOWS_CASCADE_ONLY)

    deltaZBase = 0;
    float shadThis = CASCADESHADOWSTEP(SHADOW_SAMPLE_ARGS_PREP(0.008, lerp(1, 0.01, pow(1 - abs(ksLightDirection.y), 2)), shadowTex0, 0) ARGS_EXTRA);
    return checkCascade(shadowTex0) ? (shadThis) : 0.5 /* TODO */;

  #else

    #ifdef USE_DISC_SHADOWS 
      float2 shadowTex_fwidth[4] = { fwidth(shadowTex0.xy), fwidth(shadowTex1.xy), fwidth(shadowTex2.xy), fwidth(shadowTex3.xy) };
    #endif

    float cascade = -1;
    if (checkCascade(shadowTex0)){
      cascade = 0;
    #ifndef USE_FIRST_CASCADE_ONLY
    } else if (checkCascade(shadowTex1)){
      cascade = 1;
    } else if (checkCascade(shadowTex2)){
      cascade = 2;
    } else if (checkCascade(shadowTex3)){
      cascade = 3;
    #endif
    }

    // cascade = 3;

    [branch]
    if (cascade == -1){
      return (cloudShadowsMult * fallbackShadow);
    } else {
      #ifdef USE_FIRST_CASCADE_ONLY
        float4 texThis = shadowTex0;
      #else
        float4 texArray[4] = { shadowTex0, shadowTex1, shadowTex2, shadowTex3 };
        float4 texThis = texArray[cascade];
      #endif
      #ifdef USE_DISC_SHADOWS 
        float2 texThis_fwidth = shadowTex_fwidth[cascade];
      #endif
      float shadThis = CASCADESHADOWSTEP(SHADOW_SAMPLE_ARGS_PREP(deltaZBase, calculateBias(cascade), texThis, cascade) ARGS_EXTRA);

      #if defined(SHADOWS_CASCADES_TRANSITION) && !defined(NO_SHADOWS_CASCADES_TRANSITION)
        float texThisV = max(abs(texThis).x, abs(texThis).y);
        float distanceMult = saturate(dot2(posC) * shadowsDistanceSqrInv[cascade] - 1);
        texThisV = saturate(texThisV * 5 - 4);

        [branch]
        if (cascade != 3 && distanceMult){
          float4 texNext = texArray[cascade + 1];
          float weightA = (1 - texThisV) * shadowsTransitionWeight[cascade];
          float weightB = saturate(1 - max(abs(texNext.x), abs(texNext.y)));
          weightB = lerp(0, weightB, texThisV);
          float transition = distanceMult * weightB / (weightA + weightB);

          [branch]
          if (transition){
            #ifdef USE_DISC_SHADOWS 
              float2 texNext_fwidth = shadowTex_fwidth[cascade + 1];
            #endif
            float fadeTo = CASCADESHADOWSTEP(SHADOW_SAMPLE_ARGS_PREP(deltaZBase, calculateBias(cascade + 1), texNext, cascade + 1) ARGS_EXTRA);
            shadThis = lerp(shadThis, fadeTo, transition);
            // return transition;
          }
        } else {
          float transition = lerpInvSat(distanceMult * texThisV, 0.9, 1);
          shadThis = lerp(shadThis, fallbackShadow, transition);
        }
      #endif

      return (cloudShadowsMult * shadThis);
    }
  #endif
}

#if !defined(USE_STRETCHED_SHADOWS) && !defined(USE_SCALE_PARAM)
  float getShadow(float3 posC, float4 posH, float3 normalW, float4 shadowTex0, float fallbackShadow) {
    #ifdef USE_SHADOW_BIAS_MULT
      float biasMultiplier = shadowBiasMult + 1;
      return getShadowBiasMult(posC, posH, normalW, shadowTex0, biasMultiplier, fallbackShadow);
    #elif defined GETSHADOW_BIAS_MULTIPLIER
      return getShadowBiasMult(posC, posH, normalW, shadowTex0, GETSHADOW_BIAS_MULTIPLIER, fallbackShadow);
    #else
      return getShadowBiasMult(posC, posH, normalW, shadowTex0, 1, fallbackShadow);
    #endif
  }

  float getShadow(float3 posC, float4 posH, float3 normalW, float4 shadowTex0, float3 fallbackShadow) {
    return getShadow(posC, posH, normalW, shadowTex0, fallbackShadow.g);
  }

  float getShadowBiasMult(float3 posC, float4 posH, float3 normalW, float4 shadowTex0, float biasMultiplier, float3 fallbackShadow) {
    return getShadowBiasMult(posC, posH, normalW, shadowTex0, biasMultiplier, fallbackShadow.g);
  }
#endif
