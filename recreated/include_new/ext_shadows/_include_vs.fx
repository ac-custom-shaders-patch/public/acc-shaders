#include "include/samplers.hlsl"
#include "include/common.hlsl"

#ifndef MODE_KUNOS
  Texture2D<float> txCloudShadow : register(TX_SLOT_SHADOW_CLOUDS);
#endif

#if defined(NO_SHADOWS) || defined(MODE_KUNOS)

  #ifndef SHADOWS_COORDS
    #define SHADOWS_COORDS 0, 0, 0
  #endif
  #define getShadow(posC, posH, normalW, shadowTex0, fallbackValue) (getCloudShadow(posC) * luminance(vaoValue) * perObjAmbientMultiplier)
  #define getShadowBiasMult(posC, posH, normalW, shadowTex0, biasMultiplier, fallbackValue) (getCloudShadow(posC) * luminance(vaoValue) * perObjAmbientMultiplier)

#else

  Texture2DArray<float> txShadowArray : register(TX_SLOT_SHADOW_ARRAY);
  #include "include_new/ext_shadows/_include_ps.fx"

#endif