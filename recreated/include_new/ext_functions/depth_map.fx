#ifndef _DEPTH_MAP_FX_
#define _DEPTH_MAP_FX_

#if !defined(NO_DEPTH_MAP) && !defined(MODE_KUNOS)

  #include "include/common.hlsl"

  #ifndef TXDEPTH_DEFINED
    Texture2D<float> txDepth : register(TX_SLOT_DEPTH);
  #endif

  float linearize(float depth){
    return (2 * ksNearPlane) / (ksFarPlane + ksNearPlane - depth * (ksFarPlane - ksNearPlane));
  }

  float linearizeAccurate(float depth){
    return 2 * ksNearPlane * ksFarPlane / (ksFarPlane + ksNearPlane - (2 * depth - 1) * (ksFarPlane - ksNearPlane));
  }

  float getDepthRaw(float4 posH){
    float value = txDepth.Load(int3(posH.xy, 0)).x;
    return value ? value : 1e9;
  }

  float getDepth(float4 posH){
    float value = txDepth.Load(int3(posH.xy, 0)).x;
    return value ? linearize(value) : 1e9;
  }

  float getDepthAccurate(float4 posH){
    float v = txDepth.Load(int3(posH.xy, 0)).x;
    return v ? linearizeAccurate(v) : ksFarPlane;
  }

  float getMinDepth(float4 posH){
    float value = max(
      max(
        txDepth.Load(int3(posH.xy, 0), int2(-1, -1)).x,
        txDepth.Load(int3(posH.xy, 0), int2(1, -1)).x),
      max(
        txDepth.Load(int3(posH.xy, 0), int2(-1, 1)).x,
        txDepth.Load(int3(posH.xy, 0), int2(1, 1)).x)
    );
    return linearize(value);
  }

#else

  float getDepth(float4 posH){ return 0; }

#endif
#endif