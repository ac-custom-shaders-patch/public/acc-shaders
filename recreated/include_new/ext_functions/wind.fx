#define MAX_WIND_SPEED 20

#ifndef WIND_COORDS_MULT
#define WIND_COORDS_MULT 1
#endif

float2 windOffset(float3 pos, float windPower, float freqMult){
  #ifdef NO_WIND
    return 0;
  #else
    float waveOffset = saturate(0.1 + extWindSpeed / MAX_WIND_SPEED);
    float wave = sin(extWindWave * 1.773 * freqMult + pos.z * freqMult * WIND_COORDS_MULT / 17) 
      * sin(extWindWave * freqMult + pos.x * freqMult * WIND_COORDS_MULT / 13);
    return extWindVel * windPower * (1 + wave + waveOffset) / (20 + extWindSpeed);
  #endif
}

float2 windOffsetPrev(float3 pos, float windPower, float freqMult){
  #ifdef NO_WIND
    return 0;
  #else
    float waveOffset = saturate(0.1 + extWindSpeedPrev / MAX_WIND_SPEED);
    float wave = sin(extWindWavePrev * 1.773 * freqMult + pos.z * freqMult * WIND_COORDS_MULT / 17) 
      * sin(extWindWavePrev * freqMult + pos.x * freqMult * WIND_COORDS_MULT / 13);
    return extWindVelPrev * windPower * (1 + wave + waveOffset) / (20 + extWindSpeed);
  #endif
}
