#ifdef LIGHTING_MODEL_COOKTORRANCE
cbuffer cbReflectanceParams : register(b12) {  
  float roughnessValue;
  float lambertFactor;
  float fresnelReflectance;
  float lambertFactorIOR;
}
#endif

#ifdef LIGHTING_MODEL_FROSTBITE
cbuffer cbReflectanceParams : register(b12) {  
  float roughnessValue;
  float fresnelReflectance;
  float2 padding;
}
#endif