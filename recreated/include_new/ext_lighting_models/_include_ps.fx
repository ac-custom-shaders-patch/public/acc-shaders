#if defined ALLOW_EXTRA_LIGHTING_MODELS
  #include "cbuffers_ps.fx"
#endif

#if defined ALLOW_EXTRA_LIGHTING_MODELS && defined LIGHTING_MODEL_COOKTORRANCE

  float reflectanceModel(float3 viewDir, float3 lightDir, float3 normalW, float specularExp){ 
    float3 halfDirection = normalize(lightDir + viewDir);
    float NdotL = saturate(dot(normalW, lightDir));
    float NdotH = saturate(dot(normalW, halfDirection)) + 0.0001;
    float NdotV = saturate(dot(normalW, viewDir)) + 0.0001;
    float VdotH = saturate(dot(viewDir, halfDirection)) + 0.0001;
    
    float roughnessValueDyn = lerp(1, roughnessValue, saturate(specularExp / max(ksSpecularEXP, 0.0001)));
    float roughnessSquared = roughnessValueDyn * roughnessValueDyn;

    float NH2 = 2.0 * NdotH;
    float g1 = (NH2 * NdotV) / VdotH;
    float g2 = (NH2 * lambertFactor) / VdotH;
    float geoAtt = min(1.0, min(g1, g2));
    float r1 = 1.0 / (4.0 * roughnessSquared * pow(NdotH, 4.0));
    float r2 = (NdotH * NdotH - 1.0) / (roughnessSquared * NdotH * NdotH);
    float roughness = r1 * exp(r2);
    float fresnel = pow(1.0 - VdotH, 5.0);
    fresnel *= 1.0 - fresnelReflectance;
    fresnel += fresnelReflectance;

    return NdotL * (fresnel * geoAtt * roughness) / (NdotV * lambertFactorIOR);
  }

#elif defined ALLOW_EXTRA_LIGHTING_MODELS && defined LIGHTING_MODEL_FROSTBITE

  // https://github.com/turanszkij/WickedEngine/blob/master/WickedEngine/brdf.hlsli

  float V_SmithGGXCorrelated(float NdotL, float NdotV, float alphaG2){
    // Original formulation of G_SmithGGX Correlated 
    // lambda_v = (-1 + sqrt(alphaG2 * (1 - NdotL2) / NdotL2 + 1)) * 0.5f; 
    // lambda_l = (-1 + sqrt(alphaG2 * (1 - NdotV2) / NdotV2 + 1)) * 0.5f; 
    // G_SmithGGXCorrelated = 1 / (1 + lambda_v + lambda_l); 
    // V_SmithGGXCorrelated = G_SmithGGXCorrelated / (4.0f * NdotL * NdotV); 

    // This is the optimized version 
    // float alphaG2 = alphaG * alphaG;

    // Caution: the "NdotL *" and "NdotV *" are explicitely inversed , this is not a mistake. 
    float Lambda_GGXV = NdotL * sqrt((-NdotV * alphaG2 + NdotV) * NdotV + alphaG2);
    float Lambda_GGXL = NdotV * sqrt((-NdotL * alphaG2 + NdotL) * NdotL + alphaG2);
    return 0.5f / (Lambda_GGXV + Lambda_GGXL);
  }

  float D_GGX(float NdotH, float m2){
    // Divide by PI is apply later 
    // float m2 = m * m;
    float f = (NdotH * m2 - NdotH) * NdotH + 1;
    return m2 / (f * f);
  }

  float F_Schlick(in float f0, in float f90, in float u){
    return f0 + (f90 - f0) * pow(1.f - u, 5.f);
  }

  float reflectanceModel(float3 viewDir, float3 lightDir, float3 normalW, float specularExp){ 
    float3 halfDirection = normalize(lightDir + viewDir);
    float NdotH = saturate(dot(normalW, halfDirection));
    float NdotV = abs(dot(normalW, viewDir)) + 1e-5f;
    float NdotL = saturate(dot(normalW, lightDir));
    float VdotH = saturate(dot(viewDir, halfDirection));

    float roughnessValueDyn = lerp(1, roughnessValue, saturate(specularExp / max(ksSpecularEXP, 0.0001)));
    float f90 = saturate(50.0 * fresnelReflectance);
    float F = F_Schlick(fresnelReflectance, f90, VdotH);
    float Vis = V_SmithGGXCorrelated(NdotV, NdotL, roughnessValueDyn);
    float D = D_GGX(NdotH, roughnessValueDyn);
    float Fr = D * F * Vis / 3.141593;
    return Fr * NdotL;
  }

#else

  float reflectanceModel(float3 viewDir, float3 lightDir, float3 normalW, float specularExp){
    float specularBase = saturate(dot(normalize(viewDir + lightDir), normalW));
    return pow(specularBase, GAMMA_BLINNPHONG_EXP(specularExp)) * GAMMA_BLINNPHONG_ADJ(LAMBERT(normalW, -ksLightDirection.xyz));
  }

#endif