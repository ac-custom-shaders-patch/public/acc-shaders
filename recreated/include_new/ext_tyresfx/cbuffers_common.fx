cbuffer _cbExtTyres : register(b6) {
  float3 wheelPos;
  float rimRadiusSqr;

  float3 dirUp;
  float tyreSquashDistance;

  float3 dirForward;
  uint tyreSkewX_tyreSkewY;

  float3 dirSide;
  float tyreRadiusDelta;

  float3 contactNormal;
  float profileSizeInv;

  float3 contactPos;
  uint tyreWidthInv_tyreAngularSpeed;

  float damageWidthK;
  float damageOffsetK;
  float skewMultInv;
  uint distanceMult_damageOcclusionMult;

  float3 damageSpecSpecExpReflMult;
  float damageNormalsMult_isSideA;

  uint shadowBiasMult_tyreWear;
  float3 grassColorA;

  uint dirtWidthK_blownTyresTemperature;
  float3 grassColorB;

  float dirtFade;
  float3 dirtColorA;

  uint blownTyresWear_blownTyresBrightness;
  float3 dirtColorB;

  float3 reflectionOcclusionPos;
  float reflectionOcclusionRadius;

  float2 customNormalsCoords;
  uint customNormalsMad_p;
  uint dmgMatAlt_p;

  float customNormalsScaleY;
  float3 soilColorA;

  float dirtOffsetK;
  float3 soilColorB;
};

#define damageNormalsMult abs(damageNormalsMult_isSideA)
#define isSideA (damageNormalsMult_isSideA > 0)
#define shadowBiasMult loadVector(shadowBiasMult_tyreWear).x
#define tyreWear loadVector(shadowBiasMult_tyreWear).y
#define tyreWidthInv loadVector(tyreWidthInv_tyreAngularSpeed).x
#define tyreAngularSpeed loadVector(tyreWidthInv_tyreAngularSpeed).y
#define distanceMult loadVector(distanceMult_damageOcclusionMult).x
#define damageOcclusionMult loadVector(distanceMult_damageOcclusionMult).y
#define blownTyresWear loadVector(blownTyresWear_blownTyresBrightness).x
#define blownTyresBrightness loadVector(blownTyresWear_blownTyresBrightness).y
#define dirtWidthK loadVector(dirtWidthK_blownTyresTemperature).x
#define blownTyresTemperature loadVector(dirtWidthK_blownTyresTemperature).y
#define customNormalsMad loadVector(customNormalsMad_p)
#define dmgMatAlt loadVector(dmgMatAlt_p)
