float3 calculateNewPosition(float blown, float3 inputPosition, float3 normalW, float3 dirRoadForward, float sideDot, float squashedSkewMult){
  float3 posD = inputPosition - contactPos;

  // if (length(posD) < 0.2){
  //   return inputPosition + float3(1, 0, 0);
  // }
  // return inputPosition;

  float squashedBase = 1 - abs(dot(posD, contactNormal)) * profileSizeInv;
  float squashed = saturate(squashedBase);

  // if (squashed > 0.0) inputPosition += contactNormal;

  float squashedUp = squashed * squashed;
  float squashedHalf = sin(squashed * M_PI);

  float squashedSkew = squashedBase * skewMultInv + (1 - skewMultInv);
  squashedSkew = saturate(squashedSkew);
  squashedSkew = pow(squashedSkew, 1.6) * squashedSkewMult;

  float tyreSkewX = loadVector(tyreSkewX_tyreSkewY).x;
  float tyreSkewY = loadVector(tyreSkewX_tyreSkewY).y;

  float3 dirNeutral = normalize(inputPosition - wheelPos);
  inputPosition += (dirNeutral - dirSide * dot(dirSide, dirNeutral)) * (squashedSkewMult * tyreRadiusDelta);

  if (!blown) {
    inputPosition.xyz += contactNormal * squashedUp * tyreSquashDistance;
    inputPosition.xyz += dirSide * (sideDot * squashedHalf * tyreSquashDistance + squashedSkew * tyreSkewX);
    inputPosition.xyz += dirRoadForward * squashedSkew * tyreSkewY;
  } else if (blown < 0.5) {
    float d = -dot(posD, contactNormal) + 0.005 * saturate(1 - blown * 2);
    d *= pow(lerpInvSat(dot(posD, contactNormal), 0.1, 0), 2);
    // d = 0;
    float dMult = saturate(d * 100);
    // float om = saturate(dot(dirSide, normalW));
    inputPosition.xyz += contactNormal * d;

    float offsetMult = saturate(blown * 2);
    float tyreSkewM = lerp(tyreSkewX, tyreSkewY, sign(dot(dirNeutral, dirSide)));
    if ((tyreSkewX > 0) != (sideDot > 0)) offsetMult = pow(offsetMult, 2);
    float tyreSkewX_ = tyreSkewX * dirtFade;
    inputPosition.xyz += dirSide * (d * saturate(blown * 20) * 0.5 + tyreSkewX_ * (1 - dMult) * 0.08 * offsetMult);
    inputPosition.xyz = lerp(inputPosition.xyz, wheelPos, pow(tyreSkewX_, 2) * (1 - dMult) * 0.1 * offsetMult);
  }

  return inputPosition;
}

void squashTyre(float blown, inout float3 posW, inout float3 normalW){
  float3 posD = posW - contactPos;
  float squashedBase = 1 - dot(posD, contactNormal) * profileSizeInv;
  // if (profileSizeInv == 0 || squashedBase < -2) return;

  float3 dirRoadForward = -normalize(cross(contactNormal, dirSide));
  float sideDot = dot(posD, dirSide) * tyreWidthInv;

  float3 posE = posW - wheelPos;
  float lenE = dot2(posE - dot(posE, dirSide) * dirSide);
  // float lenE = dot2(posE);
  float squashedSkewMult = saturate((lenE - rimRadiusSqr) * profileSizeInv * 2.0);

  // float3 u = calculateNewPosition(posW + contactNormal * 0.001, dirRoadForward, sideDot, squashedSkewMult);
  float3 origPosW = posW;
  posW = calculateNewPosition(blown, posW, normalW, dirRoadForward, sideDot, squashedSkewMult);

  // posW.y += 0.5;
  
  float3 deltaPosW = posW - origPosW;
  if (dot2(deltaPosW) > 0.1 * 0.1){
    // To keep things sane, limiting max offset by 10 cm
    posW = origPosW + deltaPosW * 0.1 / sqrt(dot2(deltaPosW));
  }
  
  // posW += contactNormal * 0.1;

  // float3 n = normalize(u - posW);
  // float3 nj = cross(n, sideDot < 0 ? -dirRoadForward : dirRoadForward);
  // normalW = lerp(normalW, nj, saturate(abs(sideDot) * 3.0 - 1.0));
  // normalW = lerp(normalW, nj, saturate(dot(normalW, sideDot < 0 ? -dirSide : dirSide) * 4.0 - 2.0));
}

#define ADJUST_WORLD_POS(x, y) squashTyre(vsLoadMat0f(vin.TangentPacked), x.xyz, y.xyz);
