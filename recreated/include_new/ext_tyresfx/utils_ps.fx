#define DIRT_BLUR_LAYERS 3

float3 getTyreCoords(float3 posC, out float2 dirZY){
  float3 dir = normalize(posC + ksCameraPosition.xyz - wheelPos);
  float sideDot = dot(dirSide, dir);
  float3 dirYZ = dir - dirSide * sideDot;
  dir = normalize(dirYZ);

  float upDot = dot(dirUp, dir);
  float forwardDot = dot(dirForward, dir);
  dirZY = float2(forwardDot, upDot);
  float angle = atan2(upDot, forwardDot) / (M_PI * 2.0) + 0.5;
  return float3(angle, sideDot, atan2(upDot + 0.1, forwardDot) / (M_PI * 2.0) + 0.5);
}

bool raySphereIntersect(float3 r0, float3 rd, float3 s0, float sr) {
  float3 oc = r0 - s0;
  float a = dot(rd, rd);
  float b = 2.0 * dot(oc, rd);
  float c = dot(oc, oc) - sr*sr;
  float discriminant = b * b - 4 * a * c;
  return dot(rd, oc) < 0 && discriminant > 0;
}

bool raySphereIntersect(float3 r0, float3 rd, float3 s0, float sr, out float distance){
  float3 oc = r0 - s0;
  float a = dot(rd, rd);
  float b = 2 * dot(oc, rd);
  float c = dot(oc, oc) - sr*sr;
  float discriminant = b * b - 4 * a * c;
  distance = -(b + sqrt(abs(discriminant))) / (2 * a);
  return discriminant > 0 && (distance > 0 || dot2(s0 - r0) < sr * sr);
}

float reflectionOcclusion(float3 posC, float3 normalW, float3 toCamera){
  float reflectionOcclusionRadius = 2.95;
  float3 posW = posC + ksCameraPosition.xyz;
  float3 reflectionRay = normalize(reflect(toCamera, normalW));

  float intersectionDistance;
  if (!raySphereIntersect(posW, reflectionRay, reflectionOcclusionPos, reflectionOcclusionRadius, intersectionDistance)){
    return 1;
  }

  float3 intersectionPoint = posW + reflectionRay * intersectionDistance;
  float3 intersectionDelta = intersectionPoint - reflectionOcclusionPos;
  return saturate(1 - abs(dot(normalize(intersectionDelta), reflectionRay)));
}

struct ReflectionCoverParams {
  float valueBase;
  float valueOccluded;
  float occlusionParam;
};

float sampleNoise(float3 tyreCoords){
  float2 uv = tyreCoords.xy * float2(1, 1.5);
  // if (true){
  //   float textureResolution = 32;
  //   uv = uv * textureResolution + 0.5;
  //   float2 i = floor(uv);
  //   float2 f = frac(uv);
  //   uv = i + f * f * (3 - 2 * f);
  //   uv = (uv - 0.5) / textureResolution;
  // }
	return txNoise.SampleLevel(samLinearSimple, uv, 1.8).w;
}

float getTyreColorDirt(float3 posC, float3 toCamera, float3 baseNormalW, float blown, float blownFlap,
    inout float3 normalW, float3 normalShapeW, inout float3 txDiffuseValue, inout TyresMaterialParams TM,
    float rainOcclusion, out ReflectionCoverParams coverParams){
  float2 dirZY;
  const float3 tyreCoords = getTyreCoords(posC, dirZY);
  // txDiffuseValue = 0.1;
  // txDiffuseValue.r = frac(tyreCoords.y * 3);
  // txDiffuseValue.g = frac(tyreCoords.y * 2);
  // txDiffuseValue.b = frac(tyreCoords.y * 1);

  #ifdef MODE_NORMALSAMPLE
    float blurLevel = 0;
    float dirtyLevel = 0;
  #endif

  float2 uv = float2(tyreCoords.x, tyreCoords.y + 0.5);
  float4 txTyresDamageNmValue = txTyresDamageNm.SampleLevel(samLinear, uv, blurLevel);
  float4 txTyresMarblesNmValue = txTyresMarblesNm.SampleLevel(samLinear, uv, blurLevel);

  #if DIRT_BLUR_LAYERS == 1
    float blurLayer = 0.5;
  #else
    float blurLevelAdj = blurLevel * blurLevel;
    float blurLayer = (0.5 / DIRT_BLUR_LAYERS) + blurLevelAdj * (1.0 - 1.0 / DIRT_BLUR_LAYERS);
  #endif

  float2 dmgBoth = txTyresDamage.SampleLevel(samLinearSimple, float2(0.25 * M_PI - tyreCoords.x, blurLayer), 0);
  // dmgBoth.x = 0.1;
  // dmgBoth = 0;
  // float dmgWear = (sin(ksGameTime / 100) * 0.5 + 0.5) * lerp(0.2, 0.4, sampleNoise(tyreCoords));

  float dmg = max(dmgBoth.x, dmgBoth.y);
  float toUpDmg = smoothstep(0, 1, saturate(damageWidthK * pow(saturate(dmg), 0.3) - abs(tyreCoords.y + damageOffsetK) * 4.2 - 1.0));
  dmgBoth *= toUpDmg;
  dmg *= toUpDmg;


  // clip(abs(tyreCoords.y) - lerp(0.6, 0.8, txNoise.SampleLevel(samLinearSimple, uv, 0.8).w));


  float dmgWear = saturate(lerp(0.5 * (saturate(damageWidthK - abs(tyreCoords.y + damageOffsetK) * 6.2 - 1.8)), 
    saturate((damageWidthK - abs(tyreCoords.y + damageOffsetK) * 4.2 - 1.6) * 3), 
    sampleNoise(tyreCoords)));
  // dmgWear = smoothstep(0, 1, dmgWear);
  // dmgWear *= (sin(ksGameTime / 100) * 0.5 + 0.5);
  float dmgWearArea = damageWidthK - abs(tyreCoords.y + damageOffsetK) * 4.2 - 1.6;
  dmgWear *= tyreWear;
  dmgWear *= 0.95 + 0.05 * txTyresMarblesNmValue.w;
  // dmgWear *= dot(baseNormalW, normalW);

  float toUpDirtBase = saturate(dirtWidthK - abs(tyreCoords.y + dirtOffsetK) * 4.2);
  float toUpDirt = toUpDirtBase * dirtFade;
  float4 dirtBoth = txTyresDirt.SampleLevel(samLinearSimple, float2(0.25 * M_PI - tyreCoords.x, blurLayer), 0);
  dirtBoth.xy *= toUpDirt;

  float level = txTyresGrassComb.CalculateLevelOfDetail(samLinearClamp, uv);
  level = min(level, txTyresGrassComb.CalculateLevelOfDetail(samLinearClamp, tyreCoords.zy));
  level -= 2;

  float uvYDirt = ((uv.y + 2) % 1) * 0.5;
  float4 txTyresGrassCombValue = lerp(
    txTyresGrassComb.SampleLevel(samLinearSimple, float2(uv.x, uvYDirt), level),
    txTyresGrassComb.SampleLevel(samLinearSimple, float2(uv.x, uvYDirt + 0.5), level), blurLevel);
  float4 txTyresDirtCombValue = lerp(
    txTyresDirtComb.SampleLevel(samLinearSimple, float2(uv.x, uvYDirt), level),
    txTyresDirtComb.SampleLevel(samLinearSimple, float2(uv.x, uvYDirt + 0.5), level), blurLevel);
  
  float snowRatio = dirtBoth.z / max(0.0001, max(dirtBoth.y, dirtBoth.z)) * dirtFade;
  dirtBoth.y = max(dirtBoth.y, dirtBoth.z);

  // snowRatio = dirtFade;

  dirtBoth.xy = saturate(dirtBoth.xy) * float2(1, 0.8);
  float dirtK = saturate(((txTyresDirtCombValue.a + 0.5) * dirtBoth.y * 0.5 - (1 - dirtBoth.y) * (1 - txTyresDirtCombValue.a) * 0.5 - pow(abs(tyreCoords.y + 0.05) * lerp(1.8, 0.8, dirtBoth.y), 2)) * 1);
  // dirtK *= 1 + snowRatio;
  dirtK = saturate(dirtK + 0.4 * saturate(1 - pow(abs(tyreCoords.y + 0.05) * 2.5, 2)) * dirtBoth.y);
  float grassK = saturate((txTyresGrassCombValue.a - (1 - dirtBoth.x)) * 2);
  float ambientMult = baseNormalW.y * 0.25 + 0.75;
  float combinedDirt = saturate(dirtK + grassK);
  
  // txDiffuseValue.rgb *= lerp(1, 0.3, dmgWear);
  float4 diffuseBaseColor = txDiffuse.SampleLevel(samLinearSimple, TM.uv, 3.5);
  diffuseBaseColor.rgb = dot(diffuseBaseColor.rgb, 1/3.);
  txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, diffuseBaseColor.rgb, saturate(dmgWear * 2));
  // txDiffuseValue.rgb *= lerp(1, 0.8, dmgWear);
  // txDiffuseValue.rgb = float3(dirtBoth.z, 1 - dirtBoth.z, 0);
  // txDiffuseValue.rgb = float3(dirtFade, 1 - dirtFade, 0);

  float brMult = 0.8 / max(0.01, ksAmbient + ksDiffuse);

  // blending two together
  txTyresDamageNmValue = (
    txTyresDamageNmValue * dmgBoth.x 
    + txTyresMarblesNmValue * dmgBoth.y
    + float4(txTyresDirtCombValue.xy, 1, 0) * dirtK) / (0.001 + dmgBoth.x + dmgBoth.y + dirtK);

  TM.aoMult = 1 - saturate(dot(dmgBoth, float2(txTyresDamageNmValue.a, txTyresMarblesNmValue.a)))
    * damageOcclusionMult * (1 - combinedDirt);
  TM.wetMaskBlend = 1 - combinedDirt;

  float4 nmTyresCustom = float4(0.5, 0.5, 1, 0);
  float4 nmTyresDamage = float4(txTyresDamageNmValue.xyz, dmg * abs(damageNormalsMult));
  float4 nmTyresDirt = float4(lerp(txTyresDirtCombValue.xy, 0.5, pow(dirtK, 2)), 1, combinedDirt);

  #ifdef ALLOW_RAINFX
    // float water = max(rainOcclusion, dirtBoth.z * 0.5 * saturate(5 - tyreCoords.y * 10));
    // float water = rainOcclusion;
    // float water = dirtBoth.z * saturate(5 - tyreCoords.y * 10);
    float water = max(rainOcclusion * 0.5, dirtBoth.w * saturate(5 - abs(tyreCoords.y) * 10));
  #else
    float water = 0;
  #endif

  [branch]
  #ifdef ALLOW_RAINFX
    if (customNormalsCoords.x) {
  #else
    if (customNormalsScaleY) {
  #endif
    float normalsPos = dot(posC + ksCameraPosition.xyz - wheelPos, dirSide);
    float2 normalsTex = float2(
      remap(normalsPos, customNormalsCoords.x, customNormalsCoords.y, 0, 1), 
      uv.x * customNormalsScaleY);
      // remap(normalsPos, -0.112101, 0.105305, 0, 1), uv.x * customNormalsScaleY);
    if (normalsTex.x > 0 && normalsTex.x < 1) {
      normalsTex.x = normalsTex.x * customNormalsMad.x + customNormalsMad.y;
      float mip0 = txTyresCustomNormals.CalculateLevelOfDetail(samLinear, normalsTex);
      float mip1 = txTyresCustomNormals.CalculateLevelOfDetail(samLinear, frac(normalsTex - float2(0, 0.05)));
      float mip = min(mip0, mip1);
      float4 txTyresCustomNormalsValue = lerp(
        txTyresCustomNormals.SampleLevel(samLinear, normalsTex, mip),
        txTyresCustomNormalsBlurred.Sample(samLinear, normalsTex), blurLevel);
      if (txTyresCustomNormalsValue.z){
        nmTyresCustom = float4(txTyresCustomNormalsValue.xy, 1, 
          saturate(remap(dmg, 0.6, 0.9, 1, 0)) * saturate(normalsTex.x * 100) * saturate((1 - normalsTex.x) * 100));
        TM.aoMult *= txTyresCustomNormalsValue.a;
      } else {
        nmTyresCustom = float4(0.5, 0.5, 1, saturate(remap(dmg, 0.6, 0.9, 1, 0)) * saturate(normalsTex.x * 5) * saturate((1 - normalsTex.x) * 5) * water * 0.8);
      }

      txDiffuseValue = lerp(txDiffuseValue, diffuseBaseColor.rgb, nmTyresCustom.w);
    }
  }

  dirtK = saturate(dirtK * (1 + 2 * dirtBoth.z * snowRatio));
  
  txDiffuseValue = lerp(txDiffuseValue, brMult * lerp(dirtColorA + dirtColorB * txTyresDirtCombValue.z, 0.8 + 0.2 * txTyresDirtCombValue.z, snowRatio) * (1 - grassK * 0.8), dirtK);
  txDiffuseValue = lerp(txDiffuseValue, brMult * lerp(soilColorA + soilColorB * txTyresGrassCombValue.z, 
    grassColorA + grassColorB * txTyresGrassCombValue.z, saturate(txTyresGrassCombValue.a * 8 - 0.7)), grassK);

  float dmgMat = dmg; // * saturate(abs(txTyresDamageNmAligned.y) * 5);

  // txDiffuseValue.rgb = float3(wearK, 1 - wearK, 0);

  // dmgWearArea = max(dmgWearArea, 0);
  // dmgWearArea = dmgWearArea / (1 + dmgWearArea);

  float wearK = saturate(1 / max(0.0001, dmgWearArea + 1.6));
  float shineWear = tyreWear * lerp(20, 10000, pow(1 - wearK, 4));
  shineWear = 1 - shineWear / (shineWear + 1);

  TM.reflectivity *= 1 + dmgMatAlt.x * shineWear;
  TM.specularValue *= 1 + dmgMatAlt.x * shineWear;
  TM.specularExp *= 1 + dmgMatAlt.y * shineWear;

  float wearMat = 1 - pow(saturate(dmgWear * 1.2), 4) * 0.85;
  TM.specularValue *= wearMat;
  TM.specularExp *= wearMat;
  TM.reflectivity *= wearMat;

  TM.specularValue *= lerp(1, damageSpecSpecExpReflMult.x, dmgMat);
  TM.specularExp *= lerp(1, damageSpecSpecExpReflMult.y, dmgMat);
  TM.reflectivity *= lerp(1, damageSpecSpecExpReflMult.z, dmgMat);

  TM.specularValue = lerp(TM.specularValue, 0.06, dirtK);
  TM.specularExp = lerp(TM.specularExp, 20, dirtK);
  TM.reflectivity = lerp(TM.reflectivity, 0.05 * ambientMult, dirtK);

  TM.specularValue = lerp(TM.specularValue, 0.12, grassK);
  TM.specularExp = lerp(TM.specularExp, 40, grassK);
  TM.reflectivity = lerp(TM.reflectivity, 0.1 * ambientMult, grassK);

  [branch]
  if (blown){
    [branch]
    if (blown < 0.4) {
      // float blownTyresWear_ = 0.5 + 0.5 * sin(ksGameTime / 100);
      float blownTyresWear_ = blownTyresWear;

      float xOffset = blownFlap * saturate(tyreAngularSpeed / 60 - 0.5) * -0.03;
      float yOffset = tyreWear;
      float coord = tyreCoords.x + xOffset;
      float4 noise = txNoise.SampleLevel(samLinearSimple, float2(coord * 2.5, tyreCoords.y * 0.05 + yOffset), 0);
      float4 noiseLQ = txNoise.SampleLevel(samLinearSimple, float2(coord, tyreCoords.y * 0.05 + yOffset), 1);
      float4 noiseHQ = txNoise.SampleLevel(samLinearSimple, float2(coord * 17.3, 0), 0);
      noiseHQ = lerp(noiseHQ, 0.5, blurLevel);
      float3 relPos = posC + ksCameraPosition.xyz - wheelPos;
      float relRad = length(relPos - dot(relPos, dirSide) * dirSide);

      // if (dot(relPos, dirSide) < 0) txDiffuseValue.r = 1;
      // if (dot(baseNormalW, dirSide) < 0) txDiffuseValue.r = 1;
      // if (dot(normalW, dirSide) < 0) txDiffuseValue.r = 1;

      // float clipValue = rimRadiusSqr - relRad * relRad - noise.x * noise.y * 0.12 - noiseHQ.x * 0.01 + 0.05 + 0.05 * (1 - dirtFade);
      // clip(clipValue);
      // float clipValue = rimRadiusSqr - relRad * relRad - noise.x * noise.y * 0.12 - noiseHQ.x * 0.01 + 0.05 + 0.05 * (1 - dirtFade);
      clip(1 - pow(noiseLQ.z, 2) - noise.x * noise.y * noise.z * 0.4 - noiseHQ.y * 0.1 - pow(saturate(noise.z), 60) * 100 * (1 - blurLevel)
        - blownFlap * (1 + blownTyresWear_ * 1.4 * (noiseLQ.y + noise.w)));
      txDiffuseValue.rgb *= lerp(1, float3(0.4, 0.35, 0.3) * 0.5, saturate(blownFlap * 20) * blownTyresWear_);
      // if (clipValue < 0) {
      //   // TM.specularValue = 0;
      //   // TM.specularExp = 1;
      //   // TM.reflectivity = 0;
      //   // txDiffuseValue.rgb = 0;
      // }

      // txDiffuseValue.rgb = float3(blownTyresWear_, 1 - blownTyresWear_, 0);
    } else if (blown < 0.8) {
      TM.specularValue = 0.01;
      TM.specularExp = 10;
      TM.reflectivity = 0.01;
      txDiffuseValue.rgb = 0.02;
    } else {
      clip(blownTyresBrightness);
      float4 noise = txNoise.SampleLevel(samLinearSimple, tyreCoords.xy, 0.9);
      noise = lerp(noise, txNoise.SampleLevel(samLinearSimple, tyreCoords.xy * 2, 0), (1 - blurLevel) * 0.1);
      noise.r = saturate(lerp(noise.r, smoothstep(0, 1, noise.r), 4));
      txDiffuseValue.rgb = lerp(0.13, (noise.r * 0.05 + 0.1) * blownTyresBrightness, dirtFade);

      TM.specularValue = 0 + 0.3 * pow(noise.r, 4);
      TM.specularExp = 4 + 40 * pow(noise.r, 4);
      TM.reflectivity = 0.03 + 0.15 * pow(noise.r, 4);
    }
    // clip(0.5 - noise.w);
  }
  
  coverParams.valueBase = lerp(extNeutralReflectionOcclusion, saturate(remap(normalShapeW.y, -0.5, 0, 0, 1)), distanceMult);
  coverParams.valueOccluded = extNeutralReflectionOcclusion;
  coverParams.occlusionParam = reflectionOcclusion(posC, normalW, toCamera);

  #ifndef MODE_NORMALSAMPLE
    nmTyresCustom.xyz = lerp(float3(0.5, 0.5, 1), nmTyresCustom.xyz, nmTyresCustom.w);
    nmTyresCustom = lerp(nmTyresCustom, float4(nmTyresDamage.xyz, 1), nmTyresDamage.w);

    nmTyresCustom.xyz = lerp(float3(0.5, 0.5, 1), nmTyresCustom.xyz, nmTyresCustom.w);
    nmTyresCustom = lerp(nmTyresCustom, float4(nmTyresDirt.xyz, 1), nmTyresDirt.w);

    TM.aoMult = lerp(TM.aoMult, 1, grassK);
  #endif

  float3 nmTyresAligned = nmTyresCustom.xyz * 2 - 1;
  float3 nmZ = baseNormalW;
  float3 nmX = -dirSide;
  float3 nmY = normalize(cross(nmX, nmZ)) * (isSideA ? -1 : 1);
  float3 nmTyresW = normalize(nmX * nmTyresAligned.x + nmY * nmTyresAligned.y + nmZ * nmTyresAligned.z);
  normalW = lerp(normalW, nmTyresW, nmTyresCustom.w);

  normalW = lerp(normalW, normalShapeW, saturate(dmgWear * 3 - 1));

  // txDiffuseValue = normalW + 1;

  // float waterUp = saturate(damageWidthK - abs(tyreCoords.y + damageOffsetK) * 4.2 - 1.0);
  return water;
}