Texture2D<float2> txTyresDamage : register(TX_SLOT_MAT_5);
Texture2D<float4> txTyresDirt : register(TX_SLOT_MAT_6);
Texture2D txTyresDamageNm : register(TX_SLOT_MAT_7);
Texture2D txTyresMarblesNm : register(TX_SLOT_MAT_8);
Texture2D txTyresGrassComb : register(TX_SLOT_MAT_9);
Texture2D txTyresDirtComb : register(TX_SLOT_MAT_10);
Texture2D txTyresCustomNormals : register(TX_SLOT_MAT_11);
Texture2D txTyresCustomNormalsBlurred : register(TX_SLOT_MAT_12);