#include "_consts.fx"
#include "utils_ps.fx"

#ifndef LIGHTING_SPECULAR_EXP
#define LIGHTING_SPECULAR_EXP ksSpecularEXP
#endif

#ifndef LIGHTING_SPECULAR_POWER
#define LIGHTING_SPECULAR_POWER ksSpecular
#endif

#ifndef LIGHTINGFX_KSDIFFUSE
#define LIGHTINGFX_KSDIFFUSE ksDiffuse
#endif

float getNoLightMultiplier(int index, float2 edge, float3 posW){
  float3 v = posW - gNoLightAt[index].xyz;
  float d = dot(v, v);
  return saturate(d * edge.y - edge.x);
}

float getNoLightMultiplier(float3 posW){
  return saturate(getNoLightMultiplier(0, gNoLightEdgeA, posW) * getNoLightMultiplier(1, gNoLightEdgeB, posW) + gNoLightAt[0].w);
}

float2 LFX_loadVector(uint vec){
  return float2(f16tof32(vec), f16tof32(vec >> 16));
}

float4 LFX_loadVector(uint2 vec){
  return float4(f16tof32(vec.x), f16tof32(vec.x >> 16), f16tof32(vec.y), f16tof32(vec.y >> 16));
}

float4 LFX_loadVector(float4 vec){
  return vec;
}

struct LFX_MainLight {
  #ifdef LIGHTINGFX_FIND_MAIN
    float3 dir;
    float power;
  #endif
};

void getLight_spot(uint _lightData, float3 normal, float3 posW, const float3 txDiffuseValue, const float specularExp, 
    inout float3 diffuse, inout float3 specular, inout LFX_MainLight mainLight) {
  float4 DirW_SpotCosS0 = LFX_loadVector(L_DirW_SpotCosS0_F);

  float4 toLight = normalizeWithDistance(L_PosW.xyz - posW);
  float spotK = getSpotCone(DirW_SpotCosS0.xyz, toLight.xyz, DirW_SpotCosS0.w);

  float4 DiffConc_SingWav_NoShadw_= LFX_loadVector(L_DiffConc_SingWav_NoShadw_);
  float2 Range_Extras = LFX_loadVector(L_RangeS0LI);
  float4 Color_SpecMult = LFX_loadVector(L_Color_SpecMult_G);

  float attenuation = getAttenuation(Range_Extras.x, Range_Extras.y, toLight.w);
  float shadow = attenuation * spotK;

  diffuse += Color_SpecMult.rgb * shadow;
}

void getLight_car(uint _lightData, float3 normal, float3 posW, const float3 txDiffuseValue, const float specularExp, 
    inout float3 diffuse, inout float3 specular, inout LFX_MainLight mainLight) {
  float4 DirW_SpotCosS0 = LFX_loadVector(L_DirW_SpotCosS0_F);
  float4 SecSpotCosS0LI_SecTrimS0LI = LFX_loadVector(L_SecSpotCosS0LI_SecTrimS0LI);

  float4 toLight = normalizeWithDistance(L_PosW.xyz - posW);

  float cosAngle = dot(DirW_SpotCosS0.xyz, -toLight.xyz);
  float spotK0 = lerpInv10(cosAngle, DirW_SpotCosS0.w);
  float spotK1 = lerpInv10(cosAngle, SecSpotCosS0LI_SecTrimS0LI.x, SecSpotCosS0LI_SecTrimS0LI.y);

  [branch]
  if (spotK0 == 0 && spotK1 == 0) return;

  float2 Range_Extras = LFX_loadVector(L_RangeS0LI);
  float4 Color_SpecMult = LFX_loadVector(L_Color_SpecMult_G);
  float4 UpW_SecondRangeLI = LFX_loadVector(L_UpW_SecondRangeLI);
  float4 SpotEdgeOff_SSInt = LFX_loadVector(L_SpotEdgeOff_SSInt);

  float attenuation = getAttenuation(Range_Extras.x, Range_Extras.y, toLight.w);
  float secondAttenuation = getTrimmedAttenuation(UpW_SecondRangeLI.w, SecSpotCosS0LI_SecTrimS0LI.z, SecSpotCosS0LI_SecTrimS0LI.w, toLight.w);

  #ifdef NO_EXTSPECULAR
    float noLight = 1;
  #else
    float noLight = saturate(getNoLightMultiplier(0, gNoLightEdgeA, posW) + gNoLightAt[0].w);
  #endif

  float3 shadow = getDiffuseMultiplier(normal, toLight.xyz) 
    * (spotK0 * (attenuation * getEdge(UpW_SecondRangeLI.xyz, toLight.xyz, SpotEdgeOff_SSInt.xyz))
      + spotK1 * secondAttenuation * SpotEdgeOff_SSInt.w)
    * noLight;

  diffuse += Color_SpecMult.rgb * shadow;
}
 
float3 getDynamicLights(float3 normalW, float3 posW, float3 txDiffuseValue, const float specularExp, const float specularValue
  #ifdef LIGHTINGFX_FIND_MAIN
    , out LFX_MainLight mainLight
  #endif
    ) {
  #ifdef NO_EXTSPECULAR
  [branch]
  if (ksSpecular >= 0){
  #endif

  #ifdef LIGHTINGFX_TREE
    normalW = float3(0, 1, 0);
  #endif

  float3 diffuse = 0.0;
  float3 specular = 0.0;
  float3 diffuseCar = 0.0;
  float3 specularCar = 0.0;
  float3 diffuseFixed = 0.0;
  
  #ifndef LIGHTINGFX_FIND_MAIN
    LFX_MainLight mainLight;
  #endif
  mainLight = (LFX_MainLight)0;

  float distSqr = dot(posW, posW);
  // int index = distSqr <= gEdge0 ? 0 : distSqr <= gEdge1 ? gIndex0 : gIndex1;
  int index = 0;
  int safeStop = 0;

  // some extra lights should work no matter if there is a shadow
  // #ifdef NO_EXTSPECULAR
  //   float noLight = 1;
  //   float noCarLight = 1;
  // #else
  //   float noLight = saturate(getNoLightMultiplier(1, gNoLightEdgeB, posW) + gNoLightAt[0].w);
  //   float noCarLight = saturate(getNoLightMultiplier(0, gNoLightEdgeA, posW) + gNoLightAt[0].w);
  // #endif

  [loop]
  while (true) {
    sPackedBVHLight node = gLightBVHNodeData[index];

    [branch]
    if (all(abs(node.mCenter - posW.xz) <= LFX_NODE_HSIZE(node))) {
      [call]
      switch (LFX_NODE_LIGHTTYPE(node)){
        case 1:
          getLight_spot(LFX_NODE_LIGHTINDEX(node), normalW, posW, txDiffuseValue, specularExp, diffuse, specular, mainLight);
          break;
        case 2:
          getLight_car(LFX_NODE_LIGHTINDEX(node), normalW, posW, txDiffuseValue, specularExp, diffuse, specular, mainLight);
          break;

        /*case 2:
          getLight_spot(node.mLightIndex, normalW, posW, specularExp, noLight, diffuse, specular);
          break; 
        case 4:
          getLight_spotDiff(node.mLightIndex, normalW, posW, diffuse);
          break;*/
      }
      index++;
    } else {
      index = LFX_NODE_MISSINDEX(node);
    }

    if (!index) break;
    // if (++safeStop > 200) return float3(4, 0, 4);
  }

  #if defined LIGHTINGFX_SIMPLEST || defined LIGHTINGFX_NOSPECULAR
    return diffuse;
  #else
    return diffuse + specular * specularValue;
  #endif

  #ifdef NO_EXTSPECULAR
  } else return 0;
  #endif
}

#ifndef LIGHTING_SPECULAR_POWER_MULT
#define LIGHTING_SPECULAR_POWER_MULT 1.0
#endif

#ifndef LIGHTING_SPECULAR_EXP_MULT
#define LIGHTING_SPECULAR_EXP_MULT 1.0
#endif

static const float2 extraShadow = float2(1, 1);

#ifndef POS_CAMERA
  #define POS_CAMERA pin.PosC
#endif

#ifdef LIGHTINGFX_SIMPLEST
  #ifdef LIGHTINGFX_FIND_MAIN
    #define LIGHTINGFX_GET getDynamicLights((float3)0, POS_CAMERA, 1, 0, 0, mainLight)
  #else
    #define LIGHTINGFX_GET getDynamicLights((float3)0, POS_CAMERA, 1, 0, 0)
  #endif
#elif defined(LIGHTINGFX_SIMPLEST_NORMAL)
  #ifdef LIGHTINGFX_FIND_MAIN
    #define LIGHTINGFX_GET getDynamicLights(normalW, POS_CAMERA, 1, 0, 0, mainLight)
  #else
    #define LIGHTINGFX_GET getDynamicLights(normalW, POS_CAMERA, 1, 0, 0)
  #endif
#else
  #define LIGHTINGFX_GET getDynamicLights(normalW, POS_CAMERA, txDiffuseValue.rgb, \
    (LIGHTING_SPECULAR_EXP) * (LIGHTING_SPECULAR_EXP_MULT), \
    (LIGHTING_SPECULAR_POWER) * (LIGHTING_SPECULAR_POWER_MULT))
#endif

#define LIGHTINGFX_APPLY(_X, _G) ((_X).rgb + extraShadow.x * AO_EXTRA_LIGHTING * (_G))

#if defined(ALLOW_PERVERTEX_AO_DEBUG) && !defined(GAMMA_FIX) && defined(SUPPORTS_AO)
  #define LIGHTINGFX(_X) _X.rgb = extVaoMode == 4 ? IN_VAO : extVaoMode == 5 ? pin.NormalW : \
    LIGHTINGFX_APPLY(_X, LIGHTINGFX_GET);
#else
  #define LIGHTINGFX(_X) _X.rgb = LIGHTINGFX_APPLY(_X, LIGHTINGFX_GET);
#endif

#if defined ALLOW_PERVERTEX_AO && defined SUPPORTS_AO
  #if defined(ALLOW_PERVERTEX_AO_DEBUG) && !defined(GAMMA_FIX)
    #define _AO_VAO (extVaoMode == 3 ? 1 : IN_VAO)
  #else
    #define _AO_VAO IN_VAO
  #endif
#else
  #define _AO_VAO 1
#endif