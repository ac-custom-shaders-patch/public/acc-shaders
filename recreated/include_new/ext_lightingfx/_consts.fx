// 8 KB:
// #define MAX_VALUES_AMOUNT 500

// 12 KB:
// #define MAX_PACKEDBVHLIGHT_AMOUNT 375

// 16 * 640 = 10240 B:
#define MAX_VALUES_AMOUNT 640

// 32 * 320 = 10240 B:
#define MAX_PACKEDBVHLIGHT_AMOUNT 320

// 96 * 32 = 3072 B:
#define MAX_SHADOWS_AMOUNT 32

#define BVH_NODE_HALF
#define LIGHTS_COMPACT_HALF

// LI stands for “length inverted”
// S0 stands for “start”, in pair with LI
// DirW is premultiplied by SpotCosLI to save space
// UpW is premultiplied to save space

// common
#define L_PosW                        gLightsData[_lightData + 0].xyz
#define L_RangeS0LI                   asuint(gLightsData[_lightData + 0].w)

// spot, car
#define L_DirW_SpotCosS0_F            gLightsData[_lightData + 1]
#define L_Color_SpecMult_G            asuint(gLightsData[_lightData + 2].xy)

// spot (fourth param is distance clipping)
#define L_DiffConc_SingWav_NoShadw_   asuint(gLightsData[_lightData + 2].zw)

// car
#define L_SpotEdgeOff_SSInt           asuint(gLightsData[_lightData + 2].zw)
#define L_SecSpotCosS0LI_SecTrimS0LI  asuint(gLightsData[_lightData + 3].xy)
#define L_UpW_SecondRangeLI           asuint(gLightsData[_lightData + 3].zw)

// line
#define L_Color_SpecMult_L            asuint(gLightsData[_lightData + 1].xy)
#define L_DiffBA_DistInv              asuint(gLightsData[_lightData + 1].zw)
#define L_DirW_SpotCosS0_L            asuint(gLightsData[_lightData + 2].xy)
#define L_Color2_DiffConc             asuint(gLightsData[_lightData + 2].zw)

#ifdef BVH_NODE_HALF
  struct sPackedBVHLight {
    float2 mCenter;
    uint pHalfSize;
    uint pMissIndex_LightIndex_ShadowIndex_LightType;
  };

  #define LFX_NODE_HSIZE(X) loadVector((X).pHalfSize)
  #define LFX_NODE_MISSINDEX(X) ((X).pMissIndex_LightIndex_ShadowIndex_LightType & 0x3ff)
  #define LFX_NODE_LIGHTINDEX(X) (((X).pMissIndex_LightIndex_ShadowIndex_LightType >> 10) & 0x3ff)
  #define LFX_NODE_SHADOWINDEX(X) (((X).pMissIndex_LightIndex_ShadowIndex_LightType >> 20) & 0x7f)
  #define LFX_NODE_LIGHTTYPE(X) (((X).pMissIndex_LightIndex_ShadowIndex_LightType >> 27) & 0x7)
#else
  struct sPackedBVHLight {
    float2 mCenter;
    float2 mHalfSize;
    int mMissIndex;
    int mLightIndex;
    int mShadowIndex;
    int mLightType;
  };

  #define LFX_NODE_HSIZE(X) ((X).mHalfSize)
  #define LFX_NODE_MISSINDEX(X) ((X).mMissIndex)
  #define LFX_NODE_LIGHTINDEX(X) ((X).mLightIndex)
  #define LFX_NODE_SHADOWINDEX(X) ((X).mShadowIndex)
  #define LFX_NODE_LIGHTTYPE(X) ((X).mLightType)
#endif

#ifdef ALLOW_DYNAMIC_SHADOWS
  Texture2D<float> txShadowsAtlas : register(TX_SLOT_DYNAMIC_SHADOWS);

  struct sShadowParams {
    float2 mCenter;
    float mSize;
    float mRangeInvExpFactor;
    float mMaxRange;
    float mBiasMult;
    float mThicknessFixMult;
    float mThicknessFixAdd;
    float3 mShadowPos;
    float mPad2;
    float4x4 mTransform;
  };
#endif

cbuffer _cbExtLighting : register(b7) {
  float4 gNoLightAt[2];
  float2 gNoLightEdgeA, gNoLightEdgeB;

  sPackedBVHLight gLightBVHNodeData[MAX_PACKEDBVHLIGHT_AMOUNT];
  float4 gLightsData[MAX_VALUES_AMOUNT];

  #ifdef ALLOW_DYNAMIC_SHADOWS
    sShadowParams gShadowParams[MAX_SHADOWS_AMOUNT];
  #endif
}