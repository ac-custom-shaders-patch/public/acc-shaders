float sqr(float a){
  return a * a;
}

bool testBs(float3 posW, float3 bsPosW, float bsRadiusSqr){
  float3 bsVec = posW - bsPosW;
  float bsDist = dot(bsVec, bsVec);
  return bsDist < bsRadiusSqr;
}

float4 normalizeWithDistance(float3 vec){
  float dist = length(vec);
  return float4(vec / dist, dist);
}

#ifndef CUSTOM_LFX_DIFFUSE_MULT
  float getDiffuseMultiplier(float3 normal, float3 lightDir) {  
    #ifdef LIGHTINGFX_SIMPLEST
      return 1;
    #elif defined(LIGHTINGFX_TREE)
      return 0.75; // saturate(lerp(1, dot(normal, lightDir), concentration * 0.25));
    #elif defined(LIGHTINGFX_STATIC_PARTICLES)
      if (GAMMA_FIX_ACTIVE) return lerp(saturate(dot(normal, lightDir)), 1, 0.4);
      return lerp(saturate(dot(normal, lightDir) * 0.88 + 0.12), 1, 0.4);
    #else
      if (GAMMA_FIX_ACTIVE) return saturate(dot(normal, lightDir));
      return saturate(dot(normal, lightDir) * 0.88 + 0.12);
    #endif
  }

  float getDiffuseMultiplier(float3 normal, float3 normalFlat, float3 lightDir) {  
    #ifdef LIGHTINGFX_SIMPLEST
      return 1;
    #elif defined(LIGHTINGFX_TREE)
      return 0.75; // saturate(lerp(1, dot(normal, lightDir), concentration * 0.25));
    #elif defined(LIGHTINGFX_STATIC_PARTICLES)
      if (GAMMA_FIX_ACTIVE) return lerp(saturate(dot(normal, lightDir)), 1, 0.4);
      return lerp(saturate(dot(normal, lightDir) * 0.88 + 0.12), 1, 0.4);
    #else
      if (GAMMA_FIX_ACTIVE) return saturate(max(dot(normal, lightDir), dot(normalFlat, lightDir)));
      return saturate(dot(normal, lightDir) * 0.88 + 0.12);
    #endif
  }

  float getDiffuseMultiplierConcentrated(float3 normal, float3 lightDir, float concentration) {
    #ifdef LIGHTINGFX_SIMPLEST
      return 1;
    #elif defined(LIGHTINGFX_TREE)
      return 0.75; // saturate(lerp(1, dot(normal, lightDir), concentration * 0.25));
    // #elif defined(LIGHTINGFX_EXTRA_FOCUSED)
    //   return pow(saturate(dot(normal, lightDir)), 3);
    #elif defined(LIGHTINGFX_GLASS_BACKLIT) && defined(ALLOW_LIGHTINGFX_BACKLIT)
      return abs(dot(normal, lightDir));
    #elif defined(LIGHTINGFX_STATIC_PARTICLES)
      return lerp(saturate(lerp(1, dot(normal, lightDir), concentration)), 1, 0.4);
    #else
      // concentration = GAMMA_OR(lerp(concentration, 1, 0.2), concentration);
      // concentration = GAMMA_OR(pow(concentration, 0.7), concentration);
      return saturate(lerp(1, dot(normal, lightDir), concentration));
    #endif
  }

  float getDiffuseMultiplier(float3 normal0, float3 normal1, float normalMix, float3 lightDir) {  
    return lerp(getDiffuseMultiplier(normal0, lightDir), getDiffuseMultiplier(normal1, lightDir), normalMix);
  }

  float getDiffuseMultiplierConcentrated(float3 normal, float3 normalFlat, float3 lightDir, float concentration) {  
    return getDiffuseMultiplierConcentrated(normal, lightDir, concentration);
  }

  float getDiffuseMultiplierConcentrated(float3 normal0, float3 normal1, float normalMix, float3 lightDir, float concentration) {  
    return lerp(getDiffuseMultiplierConcentrated(normal0, lightDir, concentration), getDiffuseMultiplierConcentrated(normal1, lightDir, concentration), normalMix);
  }
#endif

float3 closestPointOnSegment(float3 a, float3 ab, float distInv, float3 c, out float abValue) {
  abValue = dot(c - a, ab) * distInv;
  return a + saturate(abValue) * ab;
}

float lerpInv10(float val, float start, float lengthInv){
  // return saturate(1.0 - (val - start) * lengthInv);
  return saturate(start - val * lengthInv);
}

float lerpInv10(float val, float start){
  return saturate(start - val);
}

float lerpInv01(float val, float start, float lengthInv){
  return saturate(val * lengthInv - start);
}

/*float getTrimmedAttenuation(float rangeStart, float rangeLengthInv, float trimStart, float trimLengthInv, float d) {
  float df = max(lerpInv01(d, trimStart, trimLengthInv) - lerpInv10(d, rangeStart, rangeLengthInv), 0.0);
  return df * df;
}*/

float getTrimmedAttenuation(float rangeLengthInv, float trimStart, float trimLengthInv, float d) {
  float df = max(lerpInv01(d, trimStart, trimLengthInv) - saturate(d * rangeLengthInv), 0.0);
  return df * df;
}

float3 getDoubleSpotCone(float3 lightDirW, float3 toLight, 
    float spotCosStart, float3 attenuation,
    float secondSpotCosStart, float secondSpotCosLengthInv, float3 secondAttenuation){  
  float cosAngle = dot(lightDirW, -toLight);
  return lerpInv10(cosAngle, spotCosStart) * attenuation 
    + lerpInv10(cosAngle, secondSpotCosStart, secondSpotCosLengthInv) * secondAttenuation;
}

float getSpotCone(float3 lightDirW, float3 toLight, float spotCosStart){  
  float cosAngle = dot(lightDirW, -toLight);
  return lerpInv10(cosAngle, spotCosStart);
}

float3 getEdge(float3 lightUpW, float3 toLight, float3 spotEdgeOffset){  
  float up = dot(lightUpW, -toLight);
  return saturate(spotEdgeOffset - up);
}

float getAttenuation(float rangeStart, float rangeLengthInv, float d) {
  float df = lerpInv10(d, rangeStart, rangeLengthInv);
  return GAMMA_OR_STAT(df, df * df);
}

float getNDotH_C(float3 normalW, float3 toCamera, float3 lightDir) {
  float3 toEye = toCamera;
  float3 halfway = normalize(toEye - lightDir);
  return saturate(dot(-halfway, normalW));
}

float getNDotH_C(float3 normalW, float3 normalFlat, float3 toCamera, float3 lightDir) {
  float3 toEye = toCamera;
  float3 halfway = normalize(toEye - lightDir);
  return saturate(dot(-halfway, normalW));
}

float getNDotHL_C(float3 normalW, float3 toCamera, float3 lightDir) {
  float3 toEye = toCamera;
  float3 halfway = normalize(toEye + lightDir);
  return saturate(dot(halfway, normalW));
}

float calculateSpecularLight(float nDotH, float exp) {
  return pow(nDotH, exp);
}

float getSpecularComponent_C(float3 normalW, float3 toCamera, float3 lightDir, float specularExp){
  float nDotH = getNDotH_C(normalW, toCamera, lightDir);
  return calculateSpecularLight(nDotH, specularExp); 
}

#ifdef LIGHTINGFX_TWO_NORMALS_TWO_SPECULARS
  float getSpecularComponent_C(float3 normalW, float3 normalFlat, float3 toCamera, float3 lightDir, float specularExp){
    return getSpecularComponent_C(normalW, toCamera, lightDir, specularExp);
  }
  
  float getSpecularComponent_C(float3 normalW0, float3 normalW1, float normalMix, float3 toCamera, float3 lightDir, float specularExp0, float specularExp1){
    return lerp(getSpecularComponent_C(normalW0, toCamera, lightDir, specularExp0), getSpecularComponent_C(normalW1, toCamera, lightDir, specularExp1), normalMix);
  }
#else
  float getSpecularComponent_C(float3 normalW, float3 normalFlat, float3 toCamera, float3 lightDir, float specularExp){
    return getSpecularComponent_C(normalW, toCamera, lightDir, specularExp);
  }

  float getSpecularComponent_C(float3 normalW0, float3 normalW1, float normalMix, float3 toCamera, float3 lightDir, float specularExp){
    return lerp(0, getSpecularComponent_C(normalW1, toCamera, lightDir, specularExp), normalMix);
  }
#endif
