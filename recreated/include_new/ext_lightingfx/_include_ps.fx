#include "_consts.fx"

#ifndef LIGHTINGFX_KSDIFFUSE
  #ifdef OBJECT_SHADER
    #define LIGHTINGFX_KSDIFFUSE ksDiffuse
  #else
    #define LIGHTINGFX_KSDIFFUSE 0.5
  #endif
#endif

#ifndef LFX_COLOR_PREPARE
  #define LFX_COLOR_PREPARE(X) X
#endif

#if !defined(LIGHTINGFX_KSSPECULAR) && defined(NO_EXTSPECULAR) && defined(OBJECT_SHADER)
  #define LIGHTINGFX_KSSPECULAR ksSpecular
#endif

#ifndef LIGHTINGFX_FUNC_SPEC_PREPARE
  #define LIGHTINGFX_FUNC_SPEC_PREPARE(x) x
#endif

#ifndef LIGHTINGFX_FUNC_PARAM_DECLARE
  #define LIGHTINGFX_FUNC_PARAM_DECLARE
#endif

#ifndef LIGHTINGFX_FUNC_PARAM_PASS
  #define LIGHTINGFX_FUNC_PARAM_PASS
#endif

#ifndef LIGHTINGFX_SUMMARIZE_DIFFUSE_COMPONENT
  #define LIGHTINGFX_SUMMARIZE_DIFFUSE_COMPONENT(X, Y, Z) ((X) * (Y))
#endif

#ifdef ALLOW_DYNAMIC_SHADOWS
  #include "include/poisson.hlsl"
  #include "include/samplers.hlsl"

  float lfxDynamicShadow(int index, float3 posW, float distanceToLight, float biasMult){
    sShadowParams params = gShadowParams[index];

    #ifdef LIGHTINGFX_SHADOW_GRASS_OFFSET
      posW.y -= 0.1;
    #endif

    float4 posH = mul(float4(posW, 1), params.mTransform);
    posH.xyz /= posH.w;

    [branch]
    if (all(abs(posH.xy - params.mCenter) < params.mSize) && posH.w > 0){
      // return 0;
      // return txShadowsAtlas.SampleLevel(samLinearSimple, posH.xy, 0) < distanceToLight ? 1 : 0;
      // return txShadowsAtlas.SampleCmpLevelZero(samShadow, posH.xy, posH.z);

      // float2 relative = (posH.xy - params.mCenter) / params.mSize;
      // float2 pos = pow(abs(relative), 0.8);
      // posH.xy = pos * sign(relative) * params.mSize + params.mCenter;

      // distanceToLight = min(distanceToLight, params.mMaxRange);
      distanceToLight = min(length(posW - params.mShadowPos), params.mMaxRange);
      float occluder = txShadowsAtlas.SampleLevel(samLinearSimple, posH.xy, 0);
      float receiver = exp(params.mBiasMult * biasMult - distanceToLight * params.mRangeInvExpFactor);
      return saturate((occluder * receiver) * params.mThicknessFixMult + params.mThicknessFixAdd);

      // #define LFX_DISK_SIZE 8
      // float ret = 0;
      // for (uint i = 0; i < LFX_DISK_SIZE; ++i) {
      //   float2 sampleUV = posH.xy + SAMPLE_POISSON(LFX_DISK_SIZE, i) * 0.0005;
      //   ret += txShadowsAtlas.SampleCmpLevelZero(samShadow, sampleUV, posH.z);
      // }
      // return ret / LFX_DISK_SIZE;
    } else {
      return 1;
    }
  }

  #define ALLOW_DYNAMIC_SHADOWS_ARG int shadowIndex,
  #define ALLOW_DYNAMIC_SHADOWS_ARG_PASS LFX_NODE_SHADOWINDEX(node),
#else
  #define ALLOW_DYNAMIC_SHADOWS_ARG 
  #define ALLOW_DYNAMIC_SHADOWS_ARG_PASS
#endif

float getNoLightMultiplier(int index, float2 edge, float3 posW){
  float3 v = posW - gNoLightAt[index].xyz;
  float d = dot(v, v);
  return saturate(d * edge.y - edge.x);
}

float getNoLightMultiplier(float3 posW){
  return saturate(getNoLightMultiplier(0, gNoLightEdgeA, posW) * getNoLightMultiplier(1, gNoLightEdgeB, posW) + gNoLightAt[0].w);
}

float2 LFX_loadVector(uint vec){
  return float2(f16tof32(vec), f16tof32(vec >> 16));
}

float4 LFX_loadVector(uint2 vec){
  return float4(f16tof32(vec.x), f16tof32(vec.x >> 16), f16tof32(vec.y), f16tof32(vec.y >> 16));
}

float4 LFX_loadVector(float4 vec){
  return vec;
}

struct LFX_MainLight {
  #ifdef LIGHTINGFX_FIND_MAIN
    float3 dir;
    float power;

    void update(float3 diffuseAdd, float3 toLight){
      float p = dot(diffuseAdd, 1);
      if (p > power){
        dir = toLight;
        power = p;
      }
    }
  #endif
};

struct LFX_Result {
  float3 diffuse;
  float3 specular;
  float2 specularSingleFrequency;
  #if defined(HAS_SUN_SPECULAR) && !defined(LIGHTINGFX_TWO_NORMALS_TWO_SPECULARS)
    float3 sunSpecular;
    float2 sunSpecularSingleFrequency;
  #endif
  #ifdef LIGHTINGFX_BOUNCEBACK
    float3 bounceBack;
  #endif
  #ifdef LIGHTINGFX_RAINFX
    float3 rainSpecular;
    float3 rainBase;
  #endif
  #ifdef LIGHTINGFX_FUR
    float3 fur;
  #endif
};

#ifdef LIGHTINGFX_RAINFX
  #define LIGHTINGFX_INPUT_NORMAL_RAIN ,rainNormal
#else
  #define LIGHTINGFX_INPUT_NORMAL_RAIN
#endif

#ifdef LIGHTINGFX_FUR
  #define LIGHTINGFX_INPUT_FUR_MULT ,furLightingMult
#else
  #define LIGHTINGFX_INPUT_FUR_MULT
#endif

#ifndef LIGHTINGFX_RAIN_EXP
  #if defined(RAINFX_USE_DROPS)
    #define LIGHTINGFX_RAIN_EXP 250
    #define LIGHTINGFX_RAIN_BASE 0.2
  #else
    #define LIGHTINGFX_RAIN_EXP 2
    #define LIGHTINGFX_RAIN_BASE 1
  #endif
#endif

#ifdef LIGHTINGFX_TWO_NORMALS
  #define LIGHTINGFX_INPUT_NORMAL_INNER normalW0, normalW1, normalMix
#elif defined(GAMMA_FIX) && defined(APPLY_LFX_HEADLIGHTS_FIX) && defined(MAIN_SHADERS_SET)
  #define LIGHTINGFX_INPUT_NORMAL_INNER normalW, __normalWFlat
#else
  #define LIGHTINGFX_INPUT_NORMAL_INNER normalW
#endif

#ifndef LIGHTINGFX_INPUT_NORMAL
  #define LIGHTINGFX_INPUT_NORMAL LIGHTINGFX_INPUT_NORMAL_INNER LIGHTINGFX_INPUT_NORMAL_RAIN LIGHTINGFX_INPUT_FUR_MULT
#endif

#ifdef LIGHTINGFX_RAINFX
  #define LIGHTINGFX_ARGS_NORMAL_RAIN ,float3 rainNormal
#else
  #define LIGHTINGFX_ARGS_NORMAL_RAIN
#endif

#ifdef LIGHTINGFX_FUR
  #define LIGHTINGFX_ARGS_FUR ,float furLightingMult
#else
  #define LIGHTINGFX_ARGS_FUR
#endif

#ifdef LIGHTINGFX_TWO_NORMALS
  #define LIGHTINGFX_ARGS_NORMAL float3 normalW0, float3 normalW1, float normalMix LIGHTINGFX_ARGS_NORMAL_RAIN LIGHTINGFX_ARGS_FUR
#elif defined(GAMMA_FIX) && defined(APPLY_LFX_HEADLIGHTS_FIX) && defined(MAIN_SHADERS_SET)
  #define LIGHTINGFX_ARGS_NORMAL float3 normalW, float3 __normalWFlat LIGHTINGFX_ARGS_NORMAL_RAIN LIGHTINGFX_ARGS_FUR
#else
  #define LIGHTINGFX_ARGS_NORMAL float3 normalW LIGHTINGFX_ARGS_NORMAL_RAIN LIGHTINGFX_ARGS_FUR
#endif

#ifndef LIGHTINGFX_BOUNCEBACK_EXP
  #define LIGHTINGFX_BOUNCEBACK_EXP 100
#endif

#include "utils_ps.fx"

void getLight_spot(ALLOW_DYNAMIC_SHADOWS_ARG uint _lightData, LIGHTINGFX_ARGS_NORMAL, float3 posW, float3 toCamera, float3 txDiffuseValue, float specularExp,
    #ifdef LIGHTINGFX_TWO_NORMALS_TWO_SPECULARS
      float specularExp1,
    #elif defined(HAS_SUN_SPECULAR)
      float sunSpecularExp,
    #endif 
    inout LFX_Result R, inout LFX_MainLight ML LIGHTINGFX_FUNC_PARAM_DECLARE) {
  float4 DirW_SpotCosS0 = LFX_loadVector(L_DirW_SpotCosS0_F);

  float4 toLight = normalizeWithDistance(L_PosW.xyz - posW);
  float spotK = getSpotCone(DirW_SpotCosS0.xyz, toLight.xyz, DirW_SpotCosS0.w);

  float4 DiffConc_SingWav_NoShadw_ = LFX_loadVector(L_DiffConc_SingWav_NoShadw_);

  [branch]
  if (spotK == 0 || dot(L_PosW.xyz - posW, DirW_SpotCosS0.xyz) > DiffConc_SingWav_NoShadw_.w) return;

  float2 Range_Extras = LFX_loadVector(L_RangeS0LI);
  float4 Color_SpecMult = LFX_loadVector(L_Color_SpecMult_G);

  #ifdef NO_EXTSPECULAR
    float noLight = 1;
  #else
    float noLight = saturate(DiffConc_SingWav_NoShadw_.z + getNoLightMultiplier(1, gNoLightEdgeB, posW) + gNoLightAt[0].w);
  #endif

  float attenuation = getAttenuation(Range_Extras.x, Range_Extras.y, toLight.w);
  float LdotN = getDiffuseMultiplierConcentrated(LIGHTINGFX_INPUT_NORMAL_INNER, toLight.xyz, DiffConc_SingWav_NoShadw_.x);
  LdotN = LFX_COLOR_PREPARE(LdotN);

  float shadow = attenuation * spotK * noLight;
  shadow = GAMMA_LINEAR_SIMPLE(shadow); // applying to all but LdotN

  #ifdef ALLOW_DYNAMIC_SHADOWS
    shadow *= lfxDynamicShadow(shadowIndex, posW, toLight.w, 0);
  #endif

  #ifdef LIGHTINGFX_EXTRA_FOCUSED
    LdotN *= lerp(0.1, 1, getSpecularComponent_C(LIGHTINGFX_INPUT_NORMAL_INNER, toCamera, toLight.xyz, 8));
  #endif

  float3 lightColorAdj = LFX_COLOR_PREPARE(Color_SpecMult.rgb) * shadow;
  float3 diffColor = lightColorAdj * lerp(txDiffuseValue,
    (float3)normalize2Colors(Color_SpecMult.rgb, GAMMA_LINEAR_SIMPLE(txDiffuseValue)) * GAMMA_LINEAR_SIMPLE(luminance(txDiffuseValue)), 
    DiffConc_SingWav_NoShadw_.y);
  #if defined(LIGHTINGFX_GLASS_BACKLIT) && defined(ALLOW_LIGHTINGFX_BACKLIT)
    if (dot(normalW, toLight.xyz) < 0){
      diffColor = max(0, lerp(dot(diffColor, 0.33), diffColor, 1.6)) * shadow;
    }
  #endif
  float3 diffuseAdd = GAMMA_OR(1, LIGHTINGFX_KSDIFFUSE) * LIGHTINGFX_SUMMARIZE_DIFFUSE_COMPONENT(LdotN, diffColor, LdotN * shadow);
  R.diffuse += LFM_CONTRIB(diffuseAdd);

  #if defined(LIGHTINGFX_RAINFX) && !defined(NO_EXTSPECULAR)
    R.rainBase += LFM_CONTRIB(LdotN * lightColorAdj);
    R.rainSpecular += LFM_CONTRIB(getSpecularComponent_C(rainNormal, toCamera, toLight.xyz, LIGHTINGFX_RAIN_EXP) * LdotN * lightColorAdj);
  #endif

  #ifdef LIGHTINGFX_FUR
    R.fur += LFM_CONTRIB(lightColorAdj
      * saturate(dot(toLight.xyz, toCamera)) 
      * max(LdotN, saturate(dot(toLight.xyz, normalW) * 2 + 0.25)));
  #endif

  #if !defined(NO_EXTSPECULAR) && !defined(LIGHTINGFX_SIMPLEST) && !defined(LIGHTINGFX_NOSPECULAR)
    float3 specBase = lightColorAdj * LdotN * Color_SpecMult.a;
    // specBase *= saturate(shadow);    
    #ifdef LIGHTINGFX_TWO_NORMALS_TWO_SPECULARS
      float3 specAdd = specBase * getSpecularComponent_C(LIGHTINGFX_INPUT_NORMAL_INNER, toCamera, toLight.xyz, specularExp, specularExp1);
      specAdd = LIGHTINGFX_FUNC_SPEC_PREPARE(specAdd);
      diffuseAdd += specAdd;
      R.specular += LFM_CONTRIB(specAdd);
      R.specularSingleFrequency += float2(DiffConc_SingWav_NoShadw_.y, 1) * dot(specAdd, 1);
    #elif defined(HAS_SUN_SPECULAR)
      float nDotH = getNDotH_C(LIGHTINGFX_INPUT_NORMAL_INNER, toCamera, toLight.xyz);
      
      float3 specAdd = specBase * calculateSpecularLight(nDotH, specularExp);
      specAdd = LIGHTINGFX_FUNC_SPEC_PREPARE(specAdd);
      diffuseAdd += specAdd;
      R.specular += LFM_CONTRIB(specAdd);

      float3 sunSpecAdd = specBase * calculateSpecularLight(nDotH, sunSpecularExp);
      diffuseAdd += sunSpecAdd;
      R.sunSpecular += LFM_CONTRIB(sunSpecAdd);

      R.specularSingleFrequency += float2(DiffConc_SingWav_NoShadw_.y, 1) * dot(specAdd, 1);
      R.sunSpecularSingleFrequency += float2(DiffConc_SingWav_NoShadw_.y, 1) * dot(sunSpecAdd, 1);
    #else
      float3 specAdd = specBase * getSpecularComponent_C(LIGHTINGFX_INPUT_NORMAL_INNER, toCamera, toLight.xyz, specularExp);
      specAdd = LIGHTINGFX_FUNC_SPEC_PREPARE(specAdd);
      diffuseAdd += specAdd;
      R.specular += LFM_CONTRIB(specAdd);
      R.specularSingleFrequency += float2(DiffConc_SingWav_NoShadw_.y, 1) * dot(specAdd, 1);
    #endif
  #endif
  
  #ifdef LIGHTINGFX_BOUNCEBACK
    float3 bbAdd = pow(saturate(-dot(toCamera, toLight.xyz)), LIGHTINGFX_BOUNCEBACK_EXP) * diffColor;
    diffuseAdd += bbAdd;
    R.bounceBack += LFM_CONTRIB(bbAdd);
  #endif

  #ifdef LIGHTINGFX_FIND_MAIN
    ML.update(diffuseAdd * DiffConc_SingWav_NoShadw_.x, toLight.xyz);
  #endif
}

void getLight_car(ALLOW_DYNAMIC_SHADOWS_ARG uint _lightData, LIGHTINGFX_ARGS_NORMAL, float3 posW, float3 toCamera, float3 txDiffuseValue, float specularExp, 
    #ifdef LIGHTINGFX_TWO_NORMALS_TWO_SPECULARS
      float specularExp1,
    #elif defined(HAS_SUN_SPECULAR)
      float sunSpecularExp,
    #endif
    inout LFX_Result R, inout LFX_MainLight ML LIGHTINGFX_FUNC_PARAM_DECLARE) {
  float4 DirW_SpotCosS0 = LFX_loadVector(L_DirW_SpotCosS0_F);
  float4 SecSpotCosS0LI_SecTrimS0LI = LFX_loadVector(L_SecSpotCosS0LI_SecTrimS0LI);

  float4 toLight = normalizeWithDistance(L_PosW.xyz - posW);

  float cosAngle = dot(DirW_SpotCosS0.xyz, -toLight.xyz);
  float spotK0 = lerpInv10(cosAngle, DirW_SpotCosS0.w);
  float spotK1 = lerpInv10(cosAngle, SecSpotCosS0LI_SecTrimS0LI.x, SecSpotCosS0LI_SecTrimS0LI.y);

  [branch]
  if (spotK0 == 0 && spotK1 == 0) return;

  float2 Range_Extras = LFX_loadVector(L_RangeS0LI);
  float4 Color_SpecMult = LFX_loadVector(L_Color_SpecMult_G);
  float4 UpW_SecondRangeLI = LFX_loadVector(L_UpW_SecondRangeLI);
  float4 SpotEdgeOff_SSInt = LFX_loadVector(L_SpotEdgeOff_SSInt);

  float attenuation = getAttenuation(Range_Extras.x, Range_Extras.y, toLight.w);
  float secondAttenuation = getTrimmedAttenuation(UpW_SecondRangeLI.w, SecSpotCosS0LI_SecTrimS0LI.z, SecSpotCosS0LI_SecTrimS0LI.w, toLight.w);

  #ifdef NO_EXTSPECULAR
    float noLight = 1;
  #else
    float noLight = saturate(getNoLightMultiplier(0, gNoLightEdgeA, posW) + gNoLightAt[0].w);
  #endif

  float LdotN = getDiffuseMultiplier(LIGHTINGFX_INPUT_NORMAL_INNER, toLight.xyz);
  LdotN = LFX_COLOR_PREPARE(LdotN);
  float3 shadow = (spotK0 * attenuation * getEdge(UpW_SecondRangeLI.xyz, toLight.xyz, SpotEdgeOff_SSInt.xyz)
      + spotK1 * secondAttenuation * SpotEdgeOff_SSInt.w)
    * noLight;
  shadow = GAMMA_LINEAR_SIMPLE(shadow); // applying to all but LdotN

  #ifdef ALLOW_DYNAMIC_SHADOWS
    #ifdef LIGHTINGFX_TWO_NORMALS
      shadow *= lfxDynamicShadow(shadowIndex, posW, toLight.w, 0.5);
    #else
      shadow *= lfxDynamicShadow(shadowIndex, posW, toLight.w, 1 - abs(dot(toLight.xyz, normalW)));
    #endif
  #endif

  #ifdef LIGHTINGFX_EXTRA_FOCUSED
    LdotN *= lerp(0.1, 1, getSpecularComponent_C(LIGHTINGFX_INPUT_NORMAL_INNER, toCamera, toLight.xyz, 8));
  #endif

  float3 lightColorAdj = LFX_COLOR_PREPARE(Color_SpecMult.rgb) * shadow;
  float3 diffColor = lightColorAdj * txDiffuseValue;
  float3 diffuseAdd = GAMMA_OR(1, LIGHTINGFX_KSDIFFUSE) * LIGHTINGFX_SUMMARIZE_DIFFUSE_COMPONENT(LdotN, diffColor, LdotN * shadow);
  R.diffuse += LFM_CONTRIB(diffuseAdd);

  #if defined(LIGHTINGFX_RAINFX) && !defined(NO_EXTSPECULAR)
    R.rainBase += LFM_CONTRIB(LdotN * lightColorAdj);
    R.rainSpecular += LFM_CONTRIB(getSpecularComponent_C(rainNormal, toCamera, toLight.xyz, LIGHTINGFX_RAIN_EXP) * LdotN * lightColorAdj);
  #endif

  #ifdef LIGHTINGFX_FUR
    R.fur += LFM_CONTRIB(lightColorAdj
      * saturate(dot(toLight.xyz, toCamera)) 
      * max(LdotN, saturate(dot(toLight.xyz, normalW) * 2 + 0.25)));
  #endif

  #if !defined(NO_EXTSPECULAR) && !defined(LIGHTINGFX_SIMPLEST) && !defined(LIGHTINGFX_NOSPECULAR)
    float3 specBase = lightColorAdj * LdotN * Color_SpecMult.a;
    // specBase *= saturate(shadow);    
    #ifdef LIGHTINGFX_TWO_NORMALS_TWO_SPECULARS
      float3 specAdd = specBase * getSpecularComponent_C(LIGHTINGFX_INPUT_NORMAL_INNER, toCamera, toLight.xyz, specularExp, specularExp1);
      specAdd = LIGHTINGFX_FUNC_SPEC_PREPARE(specAdd);
      diffuseAdd += specAdd;
      R.specular += LFM_CONTRIB(specAdd);
      R.specularSingleFrequency += dot(specAdd, 1);
    #elif defined(HAS_SUN_SPECULAR)
      float nDotH = getNDotH_C(LIGHTINGFX_INPUT_NORMAL_INNER, toCamera, toLight.xyz);
      
      float3 specAdd = specBase * calculateSpecularLight(nDotH, specularExp);
      specAdd = LIGHTINGFX_FUNC_SPEC_PREPARE(specAdd);
      diffuseAdd += specAdd;
      R.specular += LFM_CONTRIB(specAdd);

      float3 sunSpecAdd = specBase * calculateSpecularLight(nDotH, sunSpecularExp);
      diffuseAdd += sunSpecAdd;
      R.sunSpecular += LFM_CONTRIB(sunSpecAdd);

      R.specularSingleFrequency += dot(specAdd, 1);
      R.sunSpecularSingleFrequency += dot(sunSpecAdd, 1);
    #else
      float3 specAdd = specBase * getSpecularComponent_C(LIGHTINGFX_INPUT_NORMAL_INNER, toCamera, toLight.xyz, specularExp);
      specAdd = LIGHTINGFX_FUNC_SPEC_PREPARE(specAdd);
      diffuseAdd += specAdd;
      R.specular += LFM_CONTRIB(specAdd);
      R.specularSingleFrequency += dot(specAdd, 1);
    #endif
  #endif

  #ifdef LIGHTINGFX_BOUNCEBACK
    float3 bbAdd = pow(saturate(-dot(toCamera, toLight.xyz)), LIGHTINGFX_BOUNCEBACK_EXP) * diffColor;
    diffuseAdd += bbAdd;
    R.bounceBack += LFM_CONTRIB(bbAdd);
  #endif

  #ifdef LIGHTINGFX_FIND_MAIN
    ML.update(diffuseAdd, toLight.xyz);
  #endif
}

void getLight_line(uint _lightData, LIGHTINGFX_ARGS_NORMAL, float3 posW, float3 toCamera, const float3 txDiffuseValue, const float specularExp,
    inout LFX_Result R, inout LFX_MainLight ML LIGHTINGFX_FUNC_PARAM_DECLARE) {
  float4 DiffBA_DistInv = LFX_loadVector(L_DiffBA_DistInv);
  float2 Range_Extras = LFX_loadVector(L_RangeS0LI);
  float4 Color_SpecMult = LFX_loadVector(L_Color_SpecMult_L);
  float4 Color2_DiffConc = LFX_loadVector(L_Color2_DiffConc);
  float4 DirW_SpotCosS0 = LFX_loadVector(L_DirW_SpotCosS0_L);

  float abValue;
  float3 closestPoint = closestPointOnSegment(L_PosW.xyz, DiffBA_DistInv.xyz, DiffBA_DistInv.w, posW, abValue);
  float4 toLight = normalizeWithDistance(closestPoint - posW);
  float attenuation = getAttenuation(Range_Extras.x, Range_Extras.y, toLight.w);

  [branch]
  if (attenuation == 0) return;

  float cosAngle = dot(DirW_SpotCosS0.xyz, -toLight.xyz);
  float spotK0 = lerpInv10(cosAngle, DirW_SpotCosS0.w);

  float LdotN = getDiffuseMultiplierConcentrated(LIGHTINGFX_INPUT_NORMAL_INNER, toLight.xyz, Color2_DiffConc.w);
  LdotN = LFX_COLOR_PREPARE(LdotN);
  
  float shadow = attenuation * spotK0;
  shadow = GAMMA_LINEAR_SIMPLE(shadow); // applying to all but LdotN

  float3 lightColorAdj = (LFX_COLOR_PREPARE(Color_SpecMult.rgb) + LFX_COLOR_PREPARE(Color2_DiffConc.rgb) * saturate(abValue)) * shadow;
  float3 diffColor = lightColorAdj * txDiffuseValue;
  float3 diffuseAdd = GAMMA_OR(1, LIGHTINGFX_KSDIFFUSE) * LIGHTINGFX_SUMMARIZE_DIFFUSE_COMPONENT(LdotN, diffColor, LdotN * shadow);
  R.diffuse += LFM_CONTRIB(diffuseAdd);

  #if defined(LIGHTINGFX_RAINFX) && !defined(NO_EXTSPECULAR)
    R.rainBase += LFM_CONTRIB(LdotN * lightColorAdj);
    R.rainSpecular += LFM_CONTRIB(getSpecularComponent_C(rainNormal, toCamera, toLight.xyz, LIGHTINGFX_RAIN_EXP) * LdotN * lightColorAdj);
  #endif

  #ifdef LIGHTINGFX_FUR
    R.fur += LFM_CONTRIB(lightColorAdj
      * saturate(dot(toLight.xyz, toCamera)) 
      * max(LdotN, saturate(dot(toLight.xyz, normalW) * 2 + 0.25)));
  #endif

  #if !defined(NO_EXTSPECULAR) && !defined(LIGHTINGFX_SIMPLEST) && !defined(LIGHTINGFX_NOSPECULAR) && !defined(LIGHTINGFX_TWO_NORMALS)
    [branch]
    if (Color_SpecMult.a == 0) return;

    float3 r = reflect(toCamera, normalW);

    // First, the closest point to the ray on the segment
    float3 L0 = L_PosW.xyz - posW;
    float3 L1 = L_PosW.xyz + DiffBA_DistInv.xyz - posW;
    float3 Ld = DiffBA_DistInv.xyz;

    float t = dot(r, L0) * dot(r, Ld) - dot(L0, Ld);
    t /= dot(Ld, Ld) - sqr(dot(r, Ld));

    float3 L = (L0 + saturate(t) * Ld);

    float3 centerToRay = dot(L, r) * r - L;
    L = L + centerToRay * saturate((1.2 - Color2_DiffConc.a) / 4 / length(centerToRay));

    float nDotH = getNDotH_C(normalW, toCamera, normalize(L));
    float3 specAdd = lightColorAdj * LdotN * calculateSpecularLight(nDotH, specularExp) * Color_SpecMult.a;
    // specAdd *= saturate(shadow);    
    specAdd = LIGHTINGFX_FUNC_SPEC_PREPARE(specAdd);
    diffuseAdd += specAdd;
    R.specular += LFM_CONTRIB(specAdd);
    R.specularSingleFrequency += dot(specAdd, 1);
  #endif
  
  // #ifdef LIGHTINGFX_BOUNCEBACK
  //   float3 bbAdd = pow(saturate(-dot(toCamera, toLight.xyz)), LIGHTINGFX_BOUNCEBACK_EXP) * diffColor;
  //   diffuseAdd += bbAdd;
  //   R.bounceBack += LFM_CONTRIB(bbAdd);
  // #endif

  #ifdef LIGHTINGFX_FIND_MAIN
    ML.update(diffuseAdd * Color2_DiffConc.w, toLight.xyz);
  #endif
}

float3 getAdjustedTxDiffuseValue(float3 txDiffuseValue) {
  #ifdef GAMMA_LIGHTINGFX_KSDIFFUSE_ONE
    txDiffuseValue = GAMMA_LINEAR(txDiffuseValue);
  #else
    txDiffuseValue = applyGamma(txDiffuseValue, LIGHTINGFX_KSDIFFUSE);
  #endif
  return txDiffuseValue;
}
 
float3 getDynamicLights(LIGHTINGFX_ARGS_NORMAL, float3 posW, float3 toCamera, float3 txDiffuseValue, float specularExp, const float3 specularValue
  #ifdef LIGHTINGFX_TWO_NORMALS_TWO_SPECULARS
    , float specularExp1
  #elif defined(HAS_SUN_SPECULAR)
    , float sunSpecularExp, float3 sunSpecularValue
  #endif
  #ifdef LIGHTINGFX_RAINFX
    , float3 rainBaseMult
    , float rainSpecularMult
  #endif
  #ifdef LIGHTINGFX_BOUNCEBACK
    , float3 bounceBackMult
  #endif
  #ifdef LIGHTINGFX_FIND_MAIN
    , out LFX_MainLight ML
  #endif
    LIGHTINGFX_FUNC_PARAM_DECLARE) {
  #if defined(NO_EXTSPECULAR) && defined(LIGHTINGFX_KSSPECULAR)
  [branch]
  if (LIGHTINGFX_KSSPECULAR >= 0){
  #endif

  #ifdef LIGHTINGFX_TREE
    #ifdef LIGHTINGFX_TWO_NORMALS
      normalW0 = float3(0, 1, 0);
      normalW1 = float3(0, 1, 0);
      normalMix = 0;
    #else
      normalW = float3(0, 1, 0);
    #endif
  #endif

  LFX_Result R = (LFX_Result)0;
  R.specularSingleFrequency = 0.001;

  specularExp = GAMMA_BLINNPHONG_EXP(max(specularExp, 0.1)); // GAMMA_BLINNPHONG_ADJ is added later
  #if defined(LIGHTINGFX_TWO_NORMALS_TWO_SPECULARS)
    specularExp1 = GAMMA_BLINNPHONG_EXP(max(specularExp1, 0.1));
  #elif defined(HAS_SUN_SPECULAR)
    sunSpecularExp = GAMMA_BLINNPHONG_EXP(max(sunSpecularExp, 0.1));
    R.sunSpecularSingleFrequency = 0.001;
  #endif
  
  #ifndef LIGHTINGFX_FIND_MAIN
    LFX_MainLight ML;
  #endif
  ML = (LFX_MainLight)0;

  float distSqr = dot(posW, posW);
  int index = 0;
  int safeStop = 0;

  [loop]
  while (true) {
    sPackedBVHLight node = gLightBVHNodeData[index];

    #define LFX_GETLIGHT_ARGS_BASE LFX_NODE_LIGHTINDEX(node), LIGHTINGFX_INPUT_NORMAL, posW, toCamera, txDiffuseValue, specularExp
    #ifdef LIGHTINGFX_TWO_NORMALS_TWO_SPECULARS
      #define LFX_GETLIGHT_ARGS_EXT LFX_GETLIGHT_ARGS_BASE, specularExp1
    #elif defined(HAS_SUN_SPECULAR)
      #define LFX_GETLIGHT_ARGS_EXT LFX_GETLIGHT_ARGS_BASE, sunSpecularExp
    #else
      #define LFX_GETLIGHT_ARGS_EXT LFX_GETLIGHT_ARGS_BASE
    #endif

    [branch]
    if (all(abs(node.mCenter - posW.xz) < LFX_NODE_HSIZE(node))) {
      [call]
      switch (LFX_NODE_LIGHTTYPE(node)){
        case 1:
          getLight_spot(ALLOW_DYNAMIC_SHADOWS_ARG_PASS LFX_GETLIGHT_ARGS_EXT, R, ML LIGHTINGFX_FUNC_PARAM_PASS);
          break;
        case 2:
          getLight_car(ALLOW_DYNAMIC_SHADOWS_ARG_PASS LFX_GETLIGHT_ARGS_EXT, R, ML LIGHTINGFX_FUNC_PARAM_PASS);
          break;
        case 3:
          getLight_line(LFX_GETLIGHT_ARGS_BASE, R, ML LIGHTINGFX_FUNC_PARAM_PASS);
          break;
      }
      index++;
    } else {
      index = LFX_NODE_MISSINDEX(node);
    }

    if (!index) break;

    #ifdef LIGHTING_SAFE
      if (++safeStop > 100) return float3(4, 0, 4);
    #endif
  }

  float3 ret = R.diffuse;
  #if !defined(LIGHTINGFX_SIMPLEST) && !defined(LIGHTINGFX_NOSPECULAR)
    ret += R.specular * lerp(specularValue, 
      (float3)normalize2Colors(R.specular, specularValue) * luminance(specularValue),
      saturate(R.specularSingleFrequency.x / R.specularSingleFrequency.y)) * GAMMA_OR(4, 1);
    #if defined(HAS_SUN_SPECULAR) && !defined(LIGHTINGFX_TWO_NORMALS_TWO_SPECULARS)
      ret += R.sunSpecular * lerp(sunSpecularValue, 
        (float3)normalize2Colors(R.sunSpecular, sunSpecularValue) * luminance(sunSpecularValue),
        saturate(R.sunSpecularSingleFrequency.x / R.sunSpecularSingleFrequency.y)) * GAMMA_OR(4, 1);;
    #endif
    #ifdef LIGHTINGFX_BOUNCEBACK
      ret += R.bounceBack * bounceBackMult;
    #endif
    #ifdef LIGHTINGFX_RAINFX
      ret += R.rainBase * rainBaseMult;
      ret += R.rainSpecular * rainSpecularMult * LIGHTINGFX_RAIN_BASE;
    #endif
    #ifdef LIGHTINGFX_FUR
      ret += R.fur * furLightingMult;
    #endif
  #endif
  ret *= GAMMA_DYNAMIC_LIGHTS_MULT;
  return ret;

  #if defined(NO_EXTSPECULAR) && defined(LIGHTINGFX_KSSPECULAR)
  } else return 0;
  #endif
}

static const float2 extraShadow = float2(1, 1);

#ifndef POS_CAMERA
  // #define POS_CAMERA PIN_POSC_POSITION
  // #define POS_CAMERA (extAltCameraThreshold && pin.PosH.x > extAltCameraThreshold ? PIN_POSC_POSITION + extAltCameraPosition - ksCameraPosition.xyz : PIN_POSC_POSITION)
  // #define POS_CAMERA (pin.PosH.x > extAltCameraThreshold ? PIN_POSC_POSITION + extAltCameraPosition : PIN_POSC_POSITION)
  #define POS_CAMERA (PIN_POSC_POSITION + (extAltCameraPosition - ksCameraPosition.xyz) * (pin.PosH.x < extAltCameraThreshold ? 1 : 0))
#endif

#ifndef POS_TOCAMERA
  #define POS_TOCAMERA toCamera
#endif

#ifdef LIGHTINGFX_NOSPECULAR
  #define LIGHTINGFX_SPECULAR_EXP 1
  #define LIGHTINGFX_SPECULAR_COLOR 0
  #ifdef HAS_SUN_SPECULAR
    #define LIGHTINGFX_SUNSPECULAR_EXP 1
    #define LIGHTINGFX_SUNSPECULAR_COLOR 0
  #endif
#else
  #ifndef LIGHTINGFX_SPECULAR_EXP
    #define LIGHTINGFX_SPECULAR_EXP L.specularExp
  #endif
  #ifndef LIGHTINGFX_SPECULAR_COLOR
    #define LIGHTINGFX_SPECULAR_COLOR (L.specularValue * L.txSpecularValue)
  #endif
  #ifdef HAS_SUN_SPECULAR
    #ifndef LIGHTINGFX_SUNSPECULAR_EXP
      #define LIGHTINGFX_SUNSPECULAR_EXP L.sunSpecularExp
    #endif
    #ifndef LIGHTINGFX_SUNSPECULAR_COLOR
      #define LIGHTINGFX_SUNSPECULAR_COLOR (L.sunSpecularValue * L.txSpecularValue)
    #endif
  #endif
#endif

  #ifndef LIGHTINGFX_TXDIFFUSE_COLOR
    #define LIGHTINGFX_TXDIFFUSE_COLOR (L.txDiffuseValue.rgb)
  #endif

#ifdef LIGHTINGFX_FIND_MAIN
  #define LFX_ARG_ML ,mainLight
#else
  #define LFX_ARG_ML
#endif
#ifdef LIGHTINGFX_BOUNCEBACK
  #define LFX_ARG_BB ,bounceBackMult
#else
  #define LFX_ARG_BB
#endif
#ifdef LIGHTINGFX_RAINFX
  #if defined(RAINFX_USE_DROPS)
    // version with extra light for rain drops, causes nasty snowy look at nights, waste of instructions (?)
    // #define LFX_ARG_RN ,RP.reflStrength * RP.reflColorMult * 0.1 ,pow(RP.alpha, 2) * RP.occlusion * 2
    #define LFX_ARG_RN ,0 ,pow(RP.alpha, 2) * RP.occlusion * 2
  #else
    #define LFX_ARG_RN ,RP.alpha * lerp(2, 0, RP.water) ,RP.alpha * RP.alpha * lerp(40, 4, RP.water)
  #endif
#else
  #define LFX_ARG_RN
#endif
#ifdef LIGHTINGFX_TWO_NORMALS_TWO_SPECULARS
  #define LFX_ARG_SS ,LIGHTINGFX_SPECULAR_EXP1
#elif defined(HAS_SUN_SPECULAR)
  #define LFX_ARG_SS ,LIGHTINGFX_SUNSPECULAR_EXP,LIGHTINGFX_SUNSPECULAR_COLOR 
#else
  #define LFX_ARG_SS
#endif
#define EXTRA_ARGS LFX_ARG_SS LFX_ARG_RN LFX_ARG_BB LFX_ARG_ML LIGHTINGFX_FUNC_PARAM_PASS

#ifdef LIGHTING_SPECULAR_EXP
  #error LIGHTING_SPECULAR_EXP is obsolete, use L or LIGHTINGFX_SPECULAR_EXP
#endif
#ifdef LIGHTING_SPECULAR_EXP_MULT
  #error LIGHTING_SPECULAR_EXP_MULT is obsolete, use L or LIGHTINGFX_SPECULAR_EXP
#endif
#ifdef LIGHTING_SPECULAR_POWER
  #error LIGHTING_SPECULAR_POWER is obsolete, use L or LIGHTINGFX_SPECULAR_COLOR
#endif
#ifdef LIGHTING_SPECULAR_POWER_MULT
  #error LIGHTING_SPECULAR_POWER_MULT is obsolete, use L or LIGHTINGFX_SPECULAR_COLOR
#endif
#ifdef LIGHTING_SPECULAR_COLOR
  #error LIGHTING_SPECULAR_COLOR is obsolete, use L or LIGHTINGFX_SPECULAR_COLOR
#endif

#ifndef LFX_EXTRA_SHADOW_CONTRUBUTION
  #ifdef HEAVIER_LFX_EXTRA_SHADOW_CONTRUBUTION
    #define LFX_EXTRA_SHADOW_CONTRUBUTION pow(extraShadow.x, 2)
  #else
    #define LFX_EXTRA_SHADOW_CONTRUBUTION extraShadow.x
  #endif
#endif

#ifndef MODE_GBUFFER
  #ifdef LIGHTINGFX_SIMPLEST
    #define LIGHTINGFX_GET getDynamicLights((float3)0, POS_CAMERA, POS_TOCAMERA, 1, 0, 0 EXTRA_ARGS)
    #define LIGHTINGFX_APPLY(_X, _G) ((_X).rgb + LFM_CONTRIB(LFX_EXTRA_SHADOW_CONTRUBUTION * AO_EXTRA_LIGHTING) * (_G))
  #elif defined(LIGHTINGFX_SIMPLEST_NORMAL)
    #define LIGHTINGFX_GET getDynamicLights(LIGHTINGFX_INPUT_NORMAL, POS_CAMERA, POS_TOCAMERA, 1, \
    LIGHTINGFX_SPECULAR_EXP, LIGHTINGFX_SPECULAR_COLOR EXTRA_ARGS)
    #define LIGHTINGFX_APPLY(_X, _G) ((_X).rgb + LFM_CONTRIB(LFX_EXTRA_SHADOW_CONTRUBUTION * AO_EXTRA_LIGHTING) * (_G))
  #else
    #define LIGHTINGFX_GET getDynamicLights(LIGHTINGFX_INPUT_NORMAL, POS_CAMERA, POS_TOCAMERA, getAdjustedTxDiffuseValue(LIGHTINGFX_TXDIFFUSE_COLOR), \
    LIGHTINGFX_SPECULAR_EXP, LIGHTINGFX_SPECULAR_COLOR EXTRA_ARGS)
    #define LIGHTINGFX_APPLY(_X, _G) ((_X).rgb + LFM_CONTRIB(LFX_EXTRA_SHADOW_CONTRUBUTION * AO_EXTRA_LIGHTING) * (_G) + LFM_CONTRIB(LFX_COLOR_PREPARE(GI_LIGHTING) * GAMMA_OR(1, LIGHTINGFX_KSDIFFUSE) * getAdjustedTxDiffuseValue(LIGHTINGFX_TXDIFFUSE_COLOR)))
  #endif

  #if defined(ALLOW_PERVERTEX_AO_DEBUG) && !defined(GAMMA_FIX) && defined(SUPPORTS_AO)
    #define LIGHTINGFX(_X) _X.rgb = extVaoMode == 4 ? IN_VAO : extVaoMode == 5 ? normalW : \
      LFM_SUM(LIGHTINGFX_APPLY(LFM_CONTRIB(_X), LIGHTINGFX_GET));
  #else
    #define LIGHTINGFX(_X) _X.rgb = LFM_SUM(LIGHTINGFX_APPLY(LFM_CONTRIB(_X), LIGHTINGFX_GET));
  #endif

  #if defined ALLOW_PERVERTEX_AO && defined SUPPORTS_AO
    #if defined(ALLOW_PERVERTEX_AO_DEBUG) && !defined(GAMMA_FIX)
      #define _AO_VAO (extVaoMode == 3 ? 1 : IN_VAO)
    #else
      #define _AO_VAO IN_VAO
    #endif
  #else
    #define _AO_VAO 1
  #endif
#endif