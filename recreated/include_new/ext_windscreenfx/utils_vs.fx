void shadowsBlur(float4 posW, out float4 tex0, out float4 tex1, out float4 tex2){
  float4 offset = float4(carVelocity, 0);
  tex0 = mul(posW, ksShadowMatrix0);
  tex1 = mul(posW + offset, ksShadowMatrix0);
  tex2 = 0;
}