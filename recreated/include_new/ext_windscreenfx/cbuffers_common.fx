cbuffer _cbExtWindscreen : register(b6) {
	float3 carVelocity;
	uint noiseMult_disableTexture;
}

#define noiseMult f16tof32(noiseMult_disableTexture)
#define disableTexture ((noiseMult_disableTexture >> 16) != 0)

#ifdef ALLOW_RAIN_EFFECT
	cbuffer _cbExtWindscreenRain : register(b12) {
		float3 rainVelocity;
		float rainAmount;

		float4 rainTime;

		float4x4 rainInvModel;
		float4x4 rainWindscreenMatrix;
		float4x4 rainWiperMatrix;

		float4 rainWiperOffset;

		float rainWiperSpeed;
		float3 dummy;
	};
#endif