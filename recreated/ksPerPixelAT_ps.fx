#define A2C_SHARPENED_THRESHOLD 0.3
#define ALPHATEST_THRESHOLD 10
#define A2C_SHARPENED
#define NO_CARPAINT
#define SUPPORTS_AO
#define GAMMA_TWEAKABLE_ALPHA
#define LIGHTINGFX_TREE
#include "lightingBounceBack.hlsl"
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue.xyz);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_DIFFUSEMASK);
  LIGHTINGFX(lighting);

  #if defined(ALLOW_EXTRA_FEATURES) && !defined(NO_LIGHTING) && defined(MAIN_SHADERS_SET)
    if (ksAlphaRef == -193){
      lighting += GAMMA_KSEMISSIVE(ksEmissive) * GAMMA_LINEAR_SIMPLE(saturate(dot(normalize(-toCamera), normalize(normalW))) * 4 - 3) * 12;
    }
  #endif  

  RAINFX_REFLECTIVE_WATER_ROUGH(lighting);
  RETURN(lighting, txDiffuseValue.a);
}
