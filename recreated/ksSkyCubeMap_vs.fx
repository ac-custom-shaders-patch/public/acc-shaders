#define SPS_NO_POSC
#include "include_new/base/_include_vs.fx"

PS_IN_Sky main(VS_IN vin SPS_VS_ARG) {
  PS_IN_Sky vout;
  float4 posW, posWnoflex, posV;
  float4 PosC;
  PosC.xyz = vin.PosL.xyz + ksCameraPosition.xyz;
  PosC.w = vin.PosL.w;
  vout.PosH = toScreenSpace(PosC, posW, posWnoflex, posV);
  vout.PosW = posW.xyz;
  vout.NormalL = vin.NormalL;
  vout.Tex = posW.xz;
  vout.LightH = mul(mul(ksLightDirection.xyz, (float3x3)ksView), (float3x3)ksProjection);
  GENERIC_PIECE_MOTION(vin.PosL);
  SPS_RET(vout);
}
