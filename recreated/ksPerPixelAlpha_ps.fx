#define CARPAINT_SIMPLE
#define CALCULATELIGHTING_SPECIAL_ALPHA
#define NO_CARPAINT
#define SUPPORTS_AO
#define GAMMA_TWEAKABLE_ALPHA
#define CB_MATERIAL_EXTRA_0\
  float alpha;\
  PARAM_CSP(float, seasonAutumn);\
  PARAM_CSP(float, seasonWinter);
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  #if defined(ALLOW_EXTRA_FEATURES) && !defined(NO_LIGHTING) && defined(MAIN_SHADERS_SET)
    if (ksAlphaRef == -193){
      lighting += GAMMA_KSEMISSIVE(ksEmissive) * GAMMA_LINEAR_SIMPLE(saturate(dot(normalize(-toCamera), normalize(normalW))) * 4 - 3) * 12;
    }
  #endif
  
  RAINFX_REFLECTIVE_WATER_ROUGH(lighting);
  RETURN(lighting, saturate(txDiffuseValue.a * alpha));
}
