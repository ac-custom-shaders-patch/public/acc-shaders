#define NO_CARPAINT
#define INCLUDE_MULTILAYER3_CB
#define SUPPORTS_AO
#define RAINFX_STATIC_OBJECT
#define USE_PS_FOG_IN_MAIN
#define APPLY_LFX_HEADLIGHTS_FIX

#define BOUNCEBACK_EXTERNALMULT
#define CB_MATERIAL_EXTRA_4\
  float4 extBounceBack /* = 2 */;
#include "lightingBounceBack.hlsl"

#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  #if !defined(NO_NORMALMAPS)
    float2 uv = pin.Tex * detailNMMult;
    float4 txNormalValue;
    [branch]
    if (ksAlphaRef == -193){
      txNormalValue = textureSampleVariation(txDetailNM, samAnisotropic, uv, 1, 0.02);
    } else {
      txNormalValue = txDetailNM.Sample(samAnisotropic, uv);
      // txNormalValue = txDetailNM.Sample(samLinearSimple, uv);
    }
    // float4 txNormalValue = textureSampleVariation(txDetailNM, samAnisotropic, uv, 1, 0.01);
    float3 txNormalAligned = txNormalValue.xyz * 2 - 1;
    float3x3 m = float3x3(tangentW, normalW, bitangentW);
    float3 normalWRaw = mul(transpose(m), txNormalAligned.xzy);
    // normalW = float3(0, 1, 0);
    // normalW *= 0.5;

    float directedMult = length(normalWRaw);
    normalW = normalize(normalWRaw);
    // normalW = normalize(pin.NormalW);
    // directedMult = 1;
  #else
    float directedMult = 1;
  #endif

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.SampleBias(samLinear, pin.Tex, TILT_BIAS);
  float4 txMaskValue = txMask.Sample(samLinear, pin.Tex);

  float3 posW = ksInPositionWorld;
  #ifdef USE_MULTIMAP_TEXTURE_VARIATION
    float4 txDetailRValue;
    float4 txDetailGValue;
    float4 txDetailBValue;
    float4 txDetailAValue;

    [branch]
    if (ksAlphaRef == -193){
      txDetailRValue = textureSampleVariation(txDetailR, samLinear, posW.xz * multR, 1, 0.02);
      txDetailGValue = textureSampleVariation(txDetailG, samLinear, posW.xz * multG, 1, 0.02);
      txDetailBValue = textureSampleVariation(txDetailB, samLinear, posW.xz * multB, 1, 0.02);
      txDetailAValue = textureSampleVariation(txDetailA, samLinear, posW.xz * multA, 1, 0.02);
    } else {
      txDetailRValue = txDetailR.Sample(samLinear, posW.xz * multR);
      txDetailGValue = txDetailG.Sample(samLinear, posW.xz * multG);
      txDetailBValue = txDetailB.Sample(samLinear, posW.xz * multB);
      txDetailAValue = txDetailA.Sample(samLinear, posW.xz * multA);
    }
  #else
    float4 txDetailRValue = txDetailR.Sample(samLinear, posW.xz * multR);
    float4 txDetailGValue = txDetailG.Sample(samLinear, posW.xz * multG);
    float4 txDetailBValue = txDetailB.Sample(samLinear, posW.xz * multB);
    float4 txDetailAValue = txDetailA.Sample(samLinear, posW.xz * multA);
  #endif

  float4 combined = 
      txDetailRValue * txMaskValue.x
    + txDetailGValue * txMaskValue.y
    + txDetailBValue * txMaskValue.z
    + txDetailAValue * txMaskValue.w;
  txDiffuseValue *= combined;
  txDiffuseValue *= magicMult;
  ADJUSTCOLOR(txDiffuseValue);
  APPLY_VRS_FIX;
  RAINFX_WET(txDiffuseValue);

  float coefficient = saturate(1 - dot(normalW, -toCamera));
  float intensity = min(
      tarmacSpecularMultiplier * pow(
        coefficient, 
        fresnelEXP * GAMMA_ACTUAL_VALUE) 
        + GAMMA_OR(pow(saturate(fresnelC), 1.75), fresnelC), 
      GAMMA_OR(pow(saturate(fresnelMaxLevel), 2), fresnelMaxLevel));
  intensity = txDiffuseValue.a * saturate(intensity);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING, 1);
  L.txSpecularValue = GET_SPEC_COLOR;
  L.specularValue = intensity; // ksSpecular is not used, it’s not a mistake
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  BOUNCEBACKFX(lighting, dot(extBounceBack, txMaskValue));
  LIGHTINGFX(lighting);

  #ifdef MODE_GRASSFX
    float damp = saturate(extSceneWetness * 100) * ((float3)(_AO_VAO)).g;
    L.specularExp = lerp(L.specularExp, 40, damp);
    L.specularValue = lerp(L.specularValue, 0.3, damp);
  #endif

  RAINFX_REFLECTIVE_WATER_ROUGH(lighting);

  #if !defined(ALLOW_RAINFX) && !defined(ALLOW_RAINFX_) && (defined(ALLOW_EXTRA_VISUAL_EFFECTS) || defined(MODE_GBUFFER))
    // float mirageMult = 1;
    float3 normalWBase = normalize(pin.NormalW);
    float NdotV = dot(-toCamera, normalWBase);
    float mirageBaseMult = mirageMult && saturate(remap(NdotV, 0.0035 * mirageMult, 0.0034 * mirageMult, 0, 1));

    [branch]
    if (mirageBaseMult){
      float distSqr = dot2(pin.PosC);
      float reflIntensity = mirageBaseMult * saturate((distSqr - 40000) / 10000) 
        * (1 - txMirageMask.SampleLevel(samLinearSimple, pin.PosH.xy * extScreenSize.zw, 0) * 0.7);
      float3 reflDir = normalize(reflect(toCamera, normalWBase));
      float3 reflColor = getReflectionAt(reflDir, -toCamera, 0, true, 0);
      lighting = lerp(lighting, reflColor, reflIntensity);
      R.resultPower = reflIntensity;
      R.resultColor = reflIntensity * reflColor;
      R.resultBlur = remap(NdotV, 0.0035 * mirageMult, 0, 1, 0);
    }
  #endif

  // lighting = intensity;
  // lighting = shadow;
  // lighting = directedMult;
  // lighting.rg = frac(posW.xz * 5);
  // lighting = dot(txMaskValue, float4(0, 0, 1, 0));

  RETURN(lighting, 1);
}
