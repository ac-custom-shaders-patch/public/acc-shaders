#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#define SUPPORTS_DITHER_FADING

#include "shadingToon.hlsl"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  // normalW = normalize(pin.NormalW * float3(1, 0, 1));
  
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  RAINFX_WET(txDiffuseValue.xyz);

  float directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, directedMult);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);
  ADJUSTCOLOR(txDiffuseValue);
  APPLY_VRS_FIX;

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING, 1);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  SHTOON_SSS;

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  APPLY_EXTRA_SHADOW_OCCLUSION(R); 

  // float3 dir = reflect(toCamera, normalW);
  // float3 pos = pin.PosC + dir * (max(0, dot(dir, shadowPos - pin.PosC)) + 0.15);
  // R.globalOcclusionMult = getExtraShadow(pos).x;

  RAINFX_REFLECTIVE(R);
  float4 withReflection = float4(calculateReflection(lighting, toCamera, pin.PosC, normalW, R), 1);

  #ifdef MODE_GBUFFER
    // R.resultPower = 0;
    // R.resultColor = 0;
  #endif

  // withReflection.rgb = normalW.y + 1;
  
  RAINFX_WATER(withReflection.rgb);
  SHTOON_OUTLINE;
  RETURN(withReflection.rgb, withReflection.w);
}
