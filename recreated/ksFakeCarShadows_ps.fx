#define STENCIL_VALUE 0
#define GBUFF_MASKING_MODE true
#define GBUFF_NORMAL_W_SRC float3(0,1,0)
#define GBUFF_NORMAL_W_PIN_SRC float3(0,1,0)
// #define GBUFFER_FORCE_SHADOWS
#define GET_MOTION(x) (float2)0

#include "include_new/base/_flags.fx"
#include "include_new/base/cbuffers_common.fx"
#include "include_new/base/structs_ps.fx"
#include "include_new/base/samplers_ps.fx"
#include "include_new/base/textures_ps.fx"
#include "include_new/base/utils_ps_fog.fx"
#include "include_new/base/utils_ps_gbuff.fx"

#if !defined(NO_SHADOWS) && !defined(MODE_KUNOS)
  #include "include_new/ext_shadows/_include_ps.fx"
  #define SHADOWS_COORDS pin.ShadowTex0
#endif
#define AO_LIGHTING 1

RESULT_TYPE main(PS_IN_FakeCarShadows pin) {
  float3 toCamera = normalize(pin.PosC);
  float3 nothing = 0;

  #ifdef MODE_GBUFFER
    // return (RESULT_TYPE)0.5;
  #endif

  #ifdef NO_LIGHTING
    RETURN_NOTHING;
  #else
    float value = txDiffuse.Sample(samLinearSimple, pin.Tex).x;
    RETURN_BASE(nothing, value * 1.2);
  #endif
}
