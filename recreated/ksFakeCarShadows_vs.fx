#define ALLOW_CAR_FLEX
#include "include_new/base/_include_vs.fx"

float4 toScreenSpaceAlt(float4 posL, out float4 posW, out float4 posV SPS_VS_TOSS_ARG){
  posW = mul(posL, ksWorld);
  // APPLY_CFX(posW);
  posV = mul(posW, ksView);
  posV.xyz *= 0.997;
  return mul(posV, ksProjection);
}

PS_IN_FakeCarShadows main(VS_IN vin SPS_VS_ARG) {
  PS_IN_FakeCarShadows vout;
  float4 posW, posV;
  vout.PosH = toScreenSpaceAlt(vin.PosL, posW, posV SPS_VS_TOSS_PASS);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  shadows(posW, SHADOWS_COORDS);
  SPS_RET(vout);
}
