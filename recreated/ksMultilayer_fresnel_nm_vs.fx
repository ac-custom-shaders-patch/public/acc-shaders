#define SUPPORTS_NORMALS_AO
#define SUPPORTS_DISTANT_FIX
#define RAINFX_STATIC_OBJECT
#define AO_MIX_INPUT 0
#define USE_PS_FOG_IN_MAIN
#include "include_new/base/_include_vs.fx"
// alias: ksMultilayer_fresnel_nm4_vs
// alias: ksMultilayer_objsp_nm4_vs

PS_IN_Nm main(VS_IN vin SPS_VS_ARG) {
  GENERIC_PIECE_NM(PS_IN_Nm);
  SPS_RET(vout);
}
