#define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"

PS_IN_AtNs main(VS_IN vin SPS_VS_ARG) {
  GENERIC_PIECE_NOSHADOWS(PS_IN_AtNs);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
