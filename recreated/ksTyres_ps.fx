#define INCLUDE_TYRE_CB
#define CARPAINT_NM
#define USE_BLURRED_NM
#define SPECULAR_DIFFUSE_FIX
#define FORCE_BLURREST_REFLECTIONS
#define EXTRA_SHADOW_AFFECTS_REFLECTION
#define RAINFX_STATIC_OCCLUDABLE_OBJECT
#define NO_ADJUSTING_COLOR

#define CB_MATERIAL_EXTRA_4\
	float extNeutralReflectionOcclusion;\
	float extGroundOcclusion;\
  float extRoughnessExp;\
  float extAlphaMultInv;\
  uint extTyresFlags_uint;
#define CUSTOM_STRUCT_FIELDS\
  float Blown : TEXCOORD20;

#ifdef MICROOPTIMIZATIONS
  #define GETSHADOW_BIAS_MULTIPLIER 4
#else
  #define GETSHADOW_BIAS_MULTIPLIER 10
#endif

#include "include_new/base/_include_ps.fx"

float3 normalTexToW(float3 txNormalValue, float3 normalW, float3 tangentW, float3 bitangentW){
  float3x3 m = float3x3(tangentW, normalW, bitangentW);
  return normalize(mul(transpose(m), (txNormalValue * 2 - 1).xzy));
}

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  // APPLY_EXTRA_SHADOW_INC_DIFFUSE
  APPLY_EXTRA_SHADOW

  #ifdef MODE_NORMALSAMPLE
    float blurLevel = 0;
    float dirtyLevel = 0;
  #endif

  float4 txDirtyValue = txDirty.Sample(samLinear, pin.Tex);
  float4 txDiffuseValue = lerp(txDiffuse.Sample(samLinear, pin.Tex), txBlur.Sample(samLinear, pin.Tex), blurLevel);
  #ifndef NO_NORMALMAPS
    float4 txNormalValue = lerp(txNormal.Sample(samLinear, pin.Tex), txNormalBlur.Sample(samLinear, pin.Tex), blurLevel);
    txNormalValue.xy = (extTyresFlags_uint & uint2(1, 2)) ? 1 - txNormalValue.xy : txNormalValue.xy;
  #endif

  float dirtyLevelAdj = dirtyLevel * txDirtyValue.a;
  float reflectionMult = fresnelMaxLevel * (1 - dirtyLevelAdj) * (extRoughnessExp ? txDiffuseValue.a : 1);
  txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, txDirtyValue.rgb, dirtyLevelAdj);
  RAINFX_INIT;

  #ifndef NO_NORMALMAPS
    float alpha = txNormalValue.w;
    normalW = normalTexToW(txNormalValue.xyz, normalW, tangentW, bitangentW);
  #else
    float alpha = 0;
  #endif

  #if defined(ALLOW_RAINFX) && !defined(MODE_SIMPLIFIED_FX)
    float water = pin.RainOcclusion;
  #else
    float water = 0;
  #endif
  float wetK = water * saturate(txDiffuseValue.a * 10);
  txDiffuseValue.xyz *= lerp(1, 0.2, wetK);

  #ifndef NO_NORMALMAPS
    bool useAlphaFromNormals = extTyresFlags_uint & 4;
    bool hasRoughtnessMap = extRoughnessExp && useAlphaFromNormals;
  #else
    bool useAlphaFromNormals = false;
    bool hasRoughtnessMap = false;
  #endif

  txDiffuseValue = lerp(txDiffuseValue, float4(0.13, 0.13, 0.13, 0), pin.Blown);

  float dirtyInv = saturate(1 - dirtyLevel);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.setSpecularMult(dirtyInv * txDiffuseValue.a, 1);
  L.multAO(saturate(extraShadow.y + saturate(-normalW.y)));
  L.specularValue = lerp(L.specularValue, 1, wetK);
  L.specularExp = lerp(L.specularExp, extRoughnessExp, hasRoughtnessMap ? alpha : 0);
  L.specularExp = lerp(L.specularExp, 400, wetK);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  lighting *= lerp(1, sqrt(saturate(normalW.y + 1)), extGroundOcclusion);

  ReflParams R = getReflParamsBase(dirtyInv * EXTRA_SHADOW_REFLECTION);
  R.finalMult = saturate(normalW.y + 1) * EXTRA_SHADOW_REFLECTION;
  R.fresnelC = lerp(R.fresnelC, 0.01, wetK);
  R.fresnelMaxLevel = lerp(reflectionMult, 0.4, wetK);
  R.fresnelEXP = lerp(R.fresnelEXP, 4, wetK);
  R.ksSpecularEXP = lerp(R.ksSpecularEXP, extRoughnessExp, hasRoughtnessMap ? alpha : 0);
  R.ksSpecularEXP = lerp(R.ksSpecularEXP, 100, wetK);

  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  R.resultColor = R.resultPower = 0;

  #ifdef ALLOW_RAINFX
    RP.alpha *= EXTRA_SHADOW_REFLECTION;
  #endif

  // withReflection.xyz = txDiffuseValue.rgb * 4;
  
  RAINFX_WATER(withReflection);
  RETURN(withReflection, (1 - extAlphaMultInv) * (useAlphaFromNormals ? alpha : 1));
}
