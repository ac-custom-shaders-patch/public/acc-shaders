#define NO_CARPAINT
#define SUPPORTS_AO
#define RAINFX_STATIC_OBJECT
#define APPLY_LFX_HEADLIGHTS_FIX
// #define INPUT_SPECULAR_K 1
// #define INPUT_SPECULAR_EXP 20
  
#include "include/common.hlsl"
#define CB_MATERIAL_EXTRA_0\
  float2 multR;\
  float2 multG;\
  float2 multB;\
  float2 multA;\
  \
  float multTilingFix;\
  float magicMult;\
  PARAM_CSP(float, seasonAutumn);\
  PARAM_CSP(float, seasonWinter);\
  \
  float tilingFixR;\
  float tilingFixG;\
  float tilingFixB;\
  float tilingFixA;\
  \
  float extColoredReflection;\
  float extColoredReflectionNorm;
#include "include_new/base/_include_ps.fx"

Texture2D txNormalR : register(TX_SLOT_MAT_6);
Texture2D txNormalG : register(TX_SLOT_MAT_7);
Texture2D txNormalB : register(TX_SLOT_MAT_8);
Texture2D txNormalA : register(TX_SLOT_MAT_9);

struct TilingParams {
  float f;
  float2 offa;
  float2 offb;
};

float rand(float n){ return frac(sin(n) * 43758.5453123); }
float hash(float2 p){ return rand(p.x * 0.317 + p.y * 0.819); }

float procNoise(float2 p){
  float2 i = floor(p);
  float2 f = frac(p);	
  return lerp(
    lerp(hash(i + float2(0, 0)), hash(i + float2(1, 0)), f.x),
    lerp(hash(i + float2(0, 1)), hash(i + float2(1, 1)), f.x), f.y);
}

TilingParams getTilingParams(float2 uv, float noiseMult) {
  #ifdef MODE_KUNOS
    float noise = procNoise(32 * uv * noiseMult);
  #else
    float noise = txNoise.Sample(samLinear, uv * noiseMult).x;
  #endif
  float l = noise * 8.0;
  float i = floor(l);
  TilingParams ret;
  ret.f = frac(l);
  ret.offa = sin(float2(3.0, 7.0) * (i + 0.0));
  ret.offb = sin(float2(3.0, 7.0) * (i + 1.0));
  return ret;
}

float4 txTilingFix(Texture2D tx, SamplerState sam, float2 uv, TilingParams TP, float bias = 1){
  float2 duvdx = ddx(uv) * bias;
  float2 duvdy = ddy(uv) * bias;    
  float4 cola = tx.SampleGrad(sam, uv + TP.offa, duvdx, duvdy);
  float4 colb = tx.SampleGrad(sam, uv + TP.offb, duvdx, duvdy);
  return lerp(cola, colb, smoothstep(0.2, 0.8, TP.f - 0.1 * dot(cola - colb, 1)));
}

bool useTilingFix(float v){
  // return false;
  #if defined(USE_MULTIMAP_TEXTURE_NM_VARIATION) || defined(MODE_KUNOS)
    return v;
  #else
    return false;
  #endif
}

RESULT_TYPE main(PS_IN_Nm pin) {  
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.SampleBias(samLinear, pin.Tex, TILT_BIAS);
  float4 txMaskValue = txMask.Sample(samLinear, pin.Tex);

  float4 txDetailRValue;
  float4 txDetailGValue;
  float4 txDetailBValue;
  float4 txDetailAValue;
  float4 txNormalRValue;
  float4 txNormalGValue;
  float4 txNormalBValue;
  float4 txNormalAValue;

  float3 posW = ksInPositionWorld;
  TilingParams TP = getTilingParams(posW.xz, multTilingFix * 0.05);

  [branch]
  if (useTilingFix(tilingFixR)){
    txDetailRValue = txTilingFix(txDetailR, samLinear, pin.Tex * multR, TP);
    txNormalRValue = txTilingFix(txNormalR, samLinearSimple, pin.Tex * multR, TP);
  } else {
    txDetailRValue = txDetailR.Sample(samLinear, pin.Tex * multR);
    txNormalRValue = txNormalR.Sample(samLinearSimple, pin.Tex * multR);
  }

  [branch]
  if (useTilingFix(tilingFixG)){
    txDetailGValue = txTilingFix(txDetailG, samLinear, pin.Tex * multG, TP);
    txNormalGValue = txTilingFix(txNormalG, samLinearSimple, pin.Tex * multG, TP);
  } else {
    txDetailGValue = txDetailG.Sample(samLinear, pin.Tex * multG);
    txNormalGValue = txNormalG.Sample(samLinearSimple, pin.Tex * multG);
  }

  [branch]
  if (useTilingFix(tilingFixB)){
    txDetailBValue = txTilingFix(txDetailB, samLinear, pin.Tex * multB, TP);
    txNormalBValue = txTilingFix(txNormalB, samLinearSimple, pin.Tex * multB, TP);
  } else {
    txDetailBValue = txDetailB.Sample(samLinear, pin.Tex * multB);
    txNormalBValue = txNormalB.Sample(samLinearSimple, pin.Tex * multB);
  }

  [branch]
  if (useTilingFix(tilingFixA)){
    txDetailAValue = txTilingFix(txDetailA, samLinear, pin.Tex * multA, TP);
    txNormalAValue = txTilingFix(txNormalA, samLinearSimple, pin.Tex * multA, TP);
  } else {
    txDetailAValue = txDetailA.Sample(samLinear, pin.Tex * multA);
    txNormalAValue = txNormalA.Sample(samLinearSimple, pin.Tex * multA);
  }

  #if !defined(NO_NORMALMAPS) 
    float4 nmCombined = 
        txNormalRValue * txMaskValue.x
      + txNormalGValue * txMaskValue.y
      + txNormalBValue * txMaskValue.z
      + txNormalAValue * txMaskValue.w;
    float3 txNormalAligned = nmCombined.xyz * 2 - (float3)1;
    float3x3 m = float3x3(tangentW, normalW, bitangentW);
    normalW = normalize(mul(transpose(m), txNormalAligned.xzy));
  #endif

  float4 combined = 
      txDetailRValue * txMaskValue.x
    + txDetailGValue * txMaskValue.y
    + txDetailBValue * txMaskValue.z
    + txDetailAValue * txMaskValue.w;
  txDiffuseValue *= combined;
  txDiffuseValue *= magicMult;
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.setSpecularMult(txDiffuseValue.a, 1);
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  RAINFX_REFLECTIVE_WATER_ROUGH(lighting);
  RETURN(lighting, 1);
}
