#define SPS_NO_POSC
#include "include_new/base/_include_vs.fx"
// alias: GL_vs

PS_IN_GL main(VS_IN vin SPS_VS_ARG) {
  PS_IN_GL vout;
  float4 posW, posWnoflex, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.Color = float4(vin.NormalL, vin.TangentPacked.x);
  vout.Tex = vin.Tex;
  vout.TangentH = (float3)0;
  SPS_RET(vout);
}
