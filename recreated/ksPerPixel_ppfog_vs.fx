#define SUPPORTS_COLORFUL_AO
#define SUPPORTS_DISTANT_FIX
#define USE_PS_FOG
#include "include_new/base/_include_vs.fx"
// alias: ksPerPixel_horizon_vs
// alias: ksPerPixel_tilingfix_vs

PS_IN_PerPixel main(VS_IN vin SPS_VS_ARG) {
  GENERIC_PIECE(PS_IN_PerPixel);
  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
