#define INCLUDE_PARTICLE_CB
#define NO_CARPAINT
#define NO_EXTAMBIENT
// #define SHADOWS_FILTER_SIZE 2
// #define LIGHTINGFX_SIMPLEST
#define LIGHTINGFX_STATIC_PARTICLES
#define LIGHTINGFX_NOSPECULAR
#define LIGHTINGFX_KSDIFFUSE (ksDiffuse * 0.32 / 0.035)
#define LIGHTINGFX_TXDIFFUSE_COLOR txDiffuseValue.rgb
// #define SUPPORTS_AO

#define ALLOW_BRAKEDISCFX
#define ADVANCED_FAKE_SHADOWS
#define ALLOW_EXTRA_LIGHTING_MODELS
// #define ALLOW_EXTRA_SHADOW
#define ALLOW_TREE_SHADOWS
#define BLUR_SHADOWS
#define WINDSCREEN_RAIN

#define PREPARE_TEXTURE_COLOR(X) (X)

#include "include_new/base/_include_ps.fx"

float3 getNormalW(float4 txNormalValue, float3 normalW, float3 tangentW, float3 bitangentW, bool objectSpace){
  float3 T = tangentW;
  float3 B = bitangentW;
  float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
  float3x3 m = float3x3(T, normalW, B);
  return normalize(mul(transpose(m), txNormalAligned.xzy));
}

RESULT_TYPE main(PS_IN_Particle pin) {
  float3 color;
  float alpha;

  READ_VECTOR_TOCAMERA
  float3 normalW;

  if (shaded) {
    float shadow = getShadow(pin.PosC, pin.PosH, float3(0, 1, 0), SHADOWS_COORDS, 1);

    float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
    float4 txNormalValue = txNormal.Sample(samLinear, pin.Tex);

    bool hasNmMap = dot(txNormalValue, 1) != 0;
    float3 nX = normalize(pin.NormalW);
    float3 nY = float3(0, 1, 0);
    float3 nZ = normalize(cross(nY, nX));
    normalW = hasNmMap ? getNormalW(txNormalValue, nZ, nX, nY, false) : float3(0, 1, 0);
    normalW = normalize(normalW + toCamera * 0.5);

    float dotValue = pow(saturate(dot(normalW, -ksLightDirection.xyz) * 0.5 + 0.5), 1.8);
    float ksDiffuseAlt = ksDiffuse * 0.32 / 0.035;
    float ksAmbientAlt = ksAmbient * 0.42 / 0.1;
    float3 light = ksLightColor.rgb * (dotValue * shadow * GAMMA_OR(1, ksDiffuseAlt));
    float3 ambient = max(0, getAmbientBaseAt(pin.PosC, normalW, AO_LIGHTING) * (hasNmMap ? txNormalValue.w : 1)) * GAMMA_OR(1, ksAmbientAlt);
    color = applyGamma(light, ambient, txDiffuseValue.rgb, ksDiffuseAlt, ksAmbientAlt);
    txDiffuseValue.rgb *= hasNmMap ? 1 : 0.8;
    txDiffuseValue.a = (txDiffuseValue.a - 0.5) / max(fwidth(txDiffuseValue.a), 0.0001) + 0.5;
    alpha = txDiffuseValue.a;

    LIGHTINGFX(color);
    alpha *= pin.Color.a;
  } else {
    float4 baseColor = txDiffuse.Sample(samLinearSimple, pin.Tex);
    color = ksLightColor.rgb * pin.Tex.y + getAmbientBaseNonDirectional(pin.PosC);
    color = lerp(color, 1, emissiveBlend);
    color *= GAMMA_LINEAR(baseColor.rgb * pin.Color.rgb);
    alpha = baseColor.a * saturate((pin.Extra - minDistance) / minDistance);
    normalW = 0;
    alpha *= pin.Color.a;
    alpha = GAMMA_OR(pow(saturate(alpha), 1.6), alpha);
  }

  clip(alpha - GAMMA_OR(0.001, 0.01));
  RETURN_BASE(color, alpha);
}
