// #define SUPPORTS_NORMALS_AO
#define SET_AO_TO_ONE
#define RAINFX_STATIC_OCCLUDABLE_OBJECT
#define RAINFX_CAR_WETNESS_MIP 1.5
#define RAINFX_NO_PERVERTEX_OCCLUSION
#define CUSTOM_STRUCT_FIELDS\
  float Blown : TEXCOORD20;
#include "include_new/base/_include_vs.fx"

PS_IN_Nm main(VS_IN vin SPS_VS_ARG) {
  GENERIC_PIECE_NM(PS_IN_Nm);
  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);
  vout.Blown = vsLoadMat0f(vin.TangentPacked) > 0.5 ? 1 : 0.01;
  SPS_RET(vout);
}
