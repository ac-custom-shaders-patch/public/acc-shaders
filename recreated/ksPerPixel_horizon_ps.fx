#define NO_CARPAINT
#define SUPPORTS_AO
#define USE_PS_FOG
#define USE_PS_FOG_RECOMPUTE
#define GBUFF_SKIP_REFLECTION
#include "include_new/base/_include_ps.fx"

cbuffer cbCCParams : register(b12) {
  float ccSaturation;
  float3 ccTintAdd;

  float ccBrightness;
  float3 ccTintMult;

  float ccGamma;
  float3 ccTintSatAdd;

  float ccLocalContrast;  
  float3 ccTintSatMult;

  float ccLocalConstrastBlur;
  float ccFogMult;
  float ccAlignNormals;
  float ccNoAo;
}

float getSaturation(float3 c){
  float a = max(c.r, max(c.g, c.b));
  if (a <= 0) return 0;
  float i = min(c.r, min(c.g, c.b));
  return (a - i) / a;
}

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS
  normalW = normalize(lerp(normalW, float3(0, 1, 0), ccAlignNormals));

  float ao = lerp(dot(AO_LIGHTING, 0.333), 1, ccNoAo);
  // float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, ao);
  float shadow = getCloudShadow(pin.PosC);
  float2 extraShadow = 1;

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  txDiffuseValue.rgb = saturate(lerp(txDiffuse.SampleLevel(samLinearSimple, pin.Tex, ccLocalConstrastBlur).xyz, txDiffuseValue.rgb, ccLocalContrast));
  float sat = getSaturation(txDiffuseValue.rgb);
  txDiffuseValue.rgb = lerp(txDiffuseValue.rgb, txDiffuseValue.rgb * ccTintSatMult + ccTintSatAdd, sat);
  txDiffuseValue.rgb = txDiffuseValue.rgb * ccTintMult + ccTintAdd;
  txDiffuseValue.rgb = saturate(lerp(dot(txDiffuseValue.rgb, 0.333), txDiffuseValue.rgb, ccSaturation) * ccBrightness);
  txDiffuseValue.rgb = pow(txDiffuseValue.rgb, max(ccGamma, 0.01));
  ADJUSTCOLOR(txDiffuseValue);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, 1);
  float3 lighting = L.calculate();
  // LIGHTINGFX(lighting);

  pin.PosC *= ccFogMult;
  RETURN_BASE(lighting, txDiffuseValue.a);
}
