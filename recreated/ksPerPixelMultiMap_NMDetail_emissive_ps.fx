#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_NORMALIZED_TB
#define CARPAINT_NMDETAILS
#define GETNORMALW_XYZ_TX
#define SUPPORTS_AO
#define EMISSIVE_SLOT 5
#include "uv2Utils.hlsl"
#include "lightingBounceBack.hlsl"
#include "emissiveMapping.hlsl"

RESULT_TYPE main(PS_IN_NmExtra4 pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  RAINFX_WET(txDiffuseValue.xyz);
  
  float directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, false, directedMult);
  considerNmDetails(pin.Tex, tangentW, bitangentW, txDiffuseValue, txMapsValue.xyz, normalW, directedMult);
  // APPLY_VRS_FIX;

  float2 emissiveUV = pin.Tex;
  if (HAS_FLAG(FLAG_MATERIAL_1)) {
    emissiveUV = PIN_TEX2;
  }

  float4 emissiveMap;
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING, 1);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  
  float4 emissiveInput = txDiffuseValue;
  #ifndef MODE_KUNOS
    if (emSkipDiffuseMap == -1) emissiveInput = getTxDetailValue(pin.Tex, false);
  #endif

  L.txEmissiveValue = getEmissiveValue(emissiveInput, emissiveUV, pin.Extra, emissiveMap);
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_DIFFUSEMASK * saturate(dot(emissiveMap, extBounceBackMask)));
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  
  RAINFX_WATER(withReflection);
  RETURN(withReflection, 1);
}
