#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define CARPAINT_AT
// #define A2C_SHARPENED
// #define GETNORMALW_UV_MULT normalUVMultiplier // Not used, apparently?
#define GETNORMALW_SAMPLER samLinear // John514 fix
#define SUPPORTS_AO
#define SUPPORTS_DITHER_FADING
#define GAMMA_TWEAKABLE_ALPHA

#define CB_MATERIAL_EXTRA_3\
  float extUseDiffuseAlpha;

#include "lightingBounceBack.hlsl"
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  RAINFX_WET(txDiffuseValue.xyz);

  float alpha, directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha, directedMult);

  if (extUseDiffuseAlpha){
    float a = alpha;
    alpha = txDiffuseValue.a;
    txDiffuseValue.a = a;
  }

  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);
  ADJUSTCOLOR(txDiffuseValue);
  APPLY_VRS_FIX;
  
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING, 1);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_DIFFUSEMASK);
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  // withReflection.r = txDiffuseValue.g;

  RAINFX_WATER(withReflection);
  RETURN(withReflection, alpha);
}
