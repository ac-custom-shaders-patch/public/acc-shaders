#define SUPPORTS_NORMALS_AO
#define SKINNED_VERTEX
// #define ALTER_POSW(local, prevFrame, world) \
//   world.y += sin((world.x + ksGameTime / 1e3 * 2) * 30) * 0.002;

#include "include_new/base/_include_vs.fx"
// alias: ksSkinnedMesh_NMDetaill_vs
// alias: ksSkinnedMesh_toon_vs

PS_IN_Nm main(VS_IN_Skinned vin SPS_VS_ARG) {
  PS_IN_Nm vout;
  float4 posW, posWnoflex, posV;
  float3 normalW, tangentW;
  vout.PosH = toScreenSpaceSkinned(vin.PosL, vin.NormalL, vin.TangentPacked, vin.BoneWeights, vin.BoneIndices,
    posW, posWnoflex, normalW, tangentW, posV);

  normalW = normalize(normalW);
  tangentW = normalize(tangentW);

  // normalW = -normalW;

  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  shadows(posW, SHADOWS_COORDS);
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));

  vout.NormalW = normalW;
  vout.Tex = vin.Tex;
  #ifndef NO_NORMALMAPS
    vout.TangentW = tangentW;
    vout.BitangentW = cross(normalW, tangentW);
  #endif
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION_SKINNED(vin.PosL);
  RAINFX_VERTEX(vout);
  #ifdef RAINFX_USE_REGISTERS
  // vout.RainOcclusion = 2;
  #endif

  // if (vout.Ao != 1 ){
  //   posW.xyz += normalW.xyz * (sin((posW.x + ksGameTime / 1e3 * 2) * 30)) * 0.002 * saturate(vout.Ao * 2);
  //   vout.NormalW.x -= sin((posW.x + ksGameTime / 1e3 * 2) * 30) * 0.2 * saturate(vout.Ao * 2);
  //   posV = mul(posW, ksView);
  //   vout.PosH = mul(posV, ksProjection);
  // }

  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
