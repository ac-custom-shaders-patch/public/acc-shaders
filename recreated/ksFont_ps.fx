#include "include_new/base/_include_ps.fx"

float4 main(PS_IN_Font pin) : SV_TARGET {
  #if defined(MODE_GBUFFER) || !defined(MODE_MAIN_FX)
    discard;
  #endif

  float base = saturate(txDiffuse.Sample(samLinearSimple, pin.Tex).r);
  float3 lighting = GAMMA_KSEMISSIVE(pin.Color.rgb) * extEmissiveMults.y;
  float alpha = GAMMA_ALPHA(base);
  return float4(lighting, alpha);
}
