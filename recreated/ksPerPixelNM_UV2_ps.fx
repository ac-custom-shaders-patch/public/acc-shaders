#define CARPAINT_NM
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#define APPLY_LFX_HEADLIGHTS_FIX
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_Uv2 pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float alpha, directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha, directedMult);

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  ADJUSTCOLOR(txDiffuseValue);
  APPLY_VRS_FIX;
  
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING, 1);
  APPLY_CAO;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  RETURN_BASE(lighting, 1);
}
