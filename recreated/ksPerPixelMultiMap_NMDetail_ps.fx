#define GBUFF_NORMALMAPS_DISTANCE 5
#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define GETNORMALW_NORMALIZED_TB
#define CARPAINT_NMDETAILS
#define GETNORMALW_XYZ_TX
#define SUPPORTS_AO
#define APPLY_LFX_HEADLIGHTS_FIX
// #define SUPPORTS_DITHER_FADING
#include "lightingBounceBack.hlsl"
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  RAINFX_WET(txDiffuseValue.xyz);

  // txDiffuseValue.g = 1;

  // txDiffuseValue.xyz = float3(0.4, 0.2, 0.1);

  float directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, false, directedMult);
  considerNmDetails(pin.Tex, tangentW, bitangentW, txDiffuseValue, txMapsValue.xyz, normalW, directedMult);
  ADJUSTCOLOR(txDiffuseValue);
  APPLY_VRS_FIX;
  
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING, 1);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;
  // L.applyTxMaps(txMapsValue.xyz);
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  // lighting = txDiffuseValue.rgb * INPUT_DIFFUSE_K;
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_DIFFUSEMASK);
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = true;
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);

  // txDiffuseValue.rg = 0.2;
  // txDiffuseValue.b = sin(ksInPositionWorld.x / 3) * 0.5 + 0.5;

  // #ifdef MODE_COLORSAMPLE
  // {
  //   RESULT_TYPE ret;
  //   ret.result = float4(txDiffuseValue.xyz * (ksAmbient + ksDiffuse) / 2, 1);
  //   return ret;
  // }
  // #endif
  // {
  //   RESULT_TYPE ret;
  //   // ret.result = float4(txDiffuseValue.xyz * ksAmbient, 1);
  //   // ret.result = float4(lighting, 1);
  //   // return ret;
  // }

  // withReflection = txDiffuseValue.xyz * ksAmbient;

  // withReflection = float3(1, 0, 0);

  RAINFX_WATER(withReflection);

  // withReflection.rgb /= 1e20;
  // #if defined(ALLOW_RAINFX) && !defined(RAINFX_USE_PUDDLES_MASK) && !defined(MODE_SIMPLIFIED_FX)
  //   withReflection.rgb += float3(saturate(pin.RainOcclusion), 1 - saturate(pin.RainOcclusion), 0) * extWhiteRefPoint;
  // #else
  //   withReflection.rgb += float3(0, 0, 1) * extWhiteRefPoint;
  // #endif

  RETURN(withReflection, 1);
}
