#define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"
// alias: ksPerPixel_dual_layer_vs
// alias: ksPerPixel_nosdw_vs 
// alias: ksPerPixelSimpleRefl_vs
// alias: ksColourShader_vs
// alias: ksCarPaintSimple_vs

PS_IN_PerPixel main(VS_IN vin SPS_VS_ARG) {
  GENERIC_PIECE(PS_IN_PerPixel);
  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
