#define FOG_NMUV2_VERSION
// #define SUPPORTS_COLORFUL_AO

#include "include_new/base/_include_vs.fx"

PS_IN_SkidMark main(VS_IN vin SPS_VS_ARG) {
  vin.PosL.xyz += extSceneOffset;
  GENERIC_PIECE(PS_IN_SkidMark);
  vout.SkidMarkThing = vin.TangentPacked.x;
  SPS_RET(vout);
}
