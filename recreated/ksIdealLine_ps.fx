#include "include/common.hlsl"
#include "include_new/base/_include_ps.fx"

struct PS_IN {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR0;
  float2 Tex : TEXCOORD0;
  float IdealLineDepth : IDEAL_LINE_DEPTH;
  float3 Tangent : TANGENT;
};

#define EXTRA_BRIGHTNESS 1
#define ALPHA_EXP 1

float4 main(PS_IN pin) : SV_TARGET {
  float opacityMult = pin.IdealLineDepth < 1 
    ? exp((1 - pin.IdealLineDepth) * -4.60517) 
    : pin.IdealLineDepth;
  opacityMult = opacityMult <= 0.01 ? 0 : opacityMult;
  float4 color = txDiffuse.Sample(samLinear, pin.Tex) * pin.Color;
  return float4(
    GAMMA_KSEMISSIVE(color.xyz) * extWhiteRefPoint,
    GAMMA_ALPHA(pow(opacityMult * color.a, ALPHA_EXP))
  );
}
