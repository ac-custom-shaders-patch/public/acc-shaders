#define SPS_NO_POSC
#include "include_new/base/_include_vs.fx"

struct PS_IN {
  float4 PosH : SV_POSITION;
  float4 Color : COLOR0;
  float2 Tex : TEXCOORD0;
  float IdealLineDepth : IDEAL_LINE_DEPTH;
  float3 Tangent : TANGENT;
};

PS_IN main(VS_IN vin SPS_VS_ARG) {
  PS_IN vout;
  float4 posW, posWnoflex, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.Color.xyz = vin.NormalL.xyz;
  vout.Color.w = vin.TangentPacked.x;
  vout.Tex = vin.Tex;
  vout.IdealLineDepth = saturate(posV.z * -0.1);
  vout.Tangent = 0;
  SPS_RET(vout);
}
