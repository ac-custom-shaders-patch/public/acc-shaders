#define REFLECTION_FRESNELEXP_BOUND
#define CARPAINT_SKINNED
#define CARPAINT_SKINNED_NM
#define GETNORMALW_NORMALIZED_INPUT
#define GETNORMALW_NORMALIZED_TB
#define GETNORMALW_XYZ_TX
#define CARPAINT_NMDETAILS
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#define GAMMA_TWEAKABLE_ALPHA
#include "include_new/base/_include_ps.fx"

// #define EPSILON 1e-10

// float3 hue2rgb(in float hue) {
//   float3 rgb = abs(hue * 6. - float3(3, 2, 4)) * float3(1, -1, -1) + float3(-1, 2, 2);
//   return clamp(rgb, 0., 1);
// }

// float3 rgb2hcv(in float3 rgb) {
//   float4 p = (rgb.g < rgb.b) ? float4(rgb.bg, -1, 2 / 3.) : float4(rgb.gb, 0., -1 / 3.);
//   float4 q = (rgb.r < p.x) ? float4(p.xyw, rgb.r) : float4(rgb.r, p.yzx);
//   float c = q.x - min(q.w, q.y);
//   float h = abs((q.w - q.y) / (6 * c + EPSILON) + q.z);
//   return float3(h, c, q.x);
// }

// float3 hsv2rgb(in float3 hsv) {
//   float3 rgb = hue2rgb(hsv.x);
//   return ((rgb - 1) * hsv.y + 1) * hsv.z;
// }

// float3 hsl2rgb(in float3 hsl) {
//   float3 rgb = hue2rgb(hsl.x);
//   float c = (1 - abs(2 * hsl.z - 1)) * hsl.y;
//   return (rgb - 0.5) * c + hsl.z;
// }

// float3 rgb2hsv(in float3 rgb) {
//   float3 hcv = rgb2hcv(rgb);
//   float s = hcv.y / (hcv.z + EPSILON);
//   return float3(hcv.x, s, hcv.z);
// }

// float3 rgb2hsl(in float3 rgb) {
//   float3 hcv = rgb2hcv(rgb);
//   float z = hcv.z - hcv.y * 0.5;
//   float s = hcv.y / (1 - abs(z * 2 - 1) + EPSILON);
//   return float3(hcv.x, s, z);
// }

// float3 cartonify(float3 i){
//   // return round(i * 12) / 12;
//   float3 hsl = rgb2hsl(i);
//   hsl.x = round(hsl.x * 12) / 12;
//   hsl.y = round(hsl.y * 4) / 4;
//   hsl.z = round(hsl.z * 10) / 10;
//   return hsl2rgb(clamp(hsl, 0.000001, 0.99999));
// }

// float3 tonemap(float3 x){ return x / (x + 1); }
// float3 inverseTonemap(float3 x){ return x > 0.9999 ? 0 : x / max(1 - x, 0.00001); }

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  // normalW = float3(0, 1, 0);

  // float gradient = fwidth(abs(dot(normalW, normalize(pin.PosC))));
  // float toon = abs(dot(normalW, normalize(pin.PosC))) < gradient * 4;

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 txMapsValue = txMaps.Sample(samLinear, pin.Tex).xyz;
  RAINFX_WET(txDiffuseValue.xyz);

  // float4 txDiffuseValueBak = txDiffuseValue;
  // txDiffuseValue.rgb = 1;

  float directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, directedMult);
  considerNmDetails(pin.Tex, tangentW, bitangentW, txDiffuseValue, txMapsValue, normalW, directedMult);  
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING, 1);
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = calculateMapsLighting_woSun(L);
  LIGHTINGFX(lighting);

  ReflParams R = getReflParamsBase(EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue);
  R.useBias = true;
  R.isCarPaint = true;

  RAINFX_REFLECTIVE(R);
  float4 withReflection = calculateReflection(float4(lighting, txDiffuseValue.a), toCamera, pin.PosC, normalW, R);
  // withReflection.rgb = 2 - withReflection.rgb;
  // withReflection.rgb = shadow * 1.5;


  // withReflection.rgb = lerp(txDiffuseValueBak.rgb * inverseTonemap(cartonify(tonemap(withReflection.rgb))), float3(1, 0, 0), toon);

  RAINFX_WATER(withReflection);
  RETURN(withReflection.rgb, withReflection.a);
}
