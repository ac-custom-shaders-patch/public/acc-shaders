#define NO_CARPAINT
#define SUPPORTS_AO
#define SUPPORTS_DITHER_FADING
#define GAMMA_TWEAKABLE_ALPHA
// #define CB_MATERIAL_EXTRA_4 float extShowRainOcclusion;
#define APPLY_LFX_HEADLIGHTS_FIX
#include "lightingBounceBack.hlsl"
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS

  #ifdef NO_SELFSHADOW
    float shadow = getCloudShadow(pin.PosC);
    float2 extraShadow = 1;
  #else
    float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
    APPLY_EXTRA_SHADOW
  #endif

  float4 txDiffuseValue = txDiffuse.SampleBias(samLinear, pin.Tex, TILT_BIAS);
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue.xyz);

  // bool2 checkerFlag = frac(pin.PosH.xy / 64) > 0.5;
  // float checkerPattern = all(checkerFlag) || all(!checkerFlag);
  // checkerPattern = frac((pin.PosH.x + pin.PosH.y) / 8) > 0.3;

  // float4 shadowTex0 = pin.ShadowTex0;
  // float4 shadowTex1 = float4(shadowTex0.xyz * shadowsMatrixModifier1M + shadowsMatrixModifier1A, 1);
  // float4 shadowTex2 = float4(shadowTex0.xyz * shadowsMatrixModifier2M + shadowsMatrixModifier2A, 1);
  // float4 shadowTex3 = float4(shadowTex0.xyz * shadowsMatrixModifier3M + shadowsMatrixModifier3A, 1);
  // float3 cascadeMask = float3(checkCascade(shadowTex0), checkCascade(shadowTex1), checkCascade(shadowTex2));
  // if (!dot(cascadeMask, 1)) cascadeMask = 1;
  // txDiffuseValue.rgb = float3(checkCascade(shadowTex0), checkCascade(shadowTex1), checkCascade(shadowTex2));
  // txDiffuseValue.rgb = float3(checkCascade(shadowTex1), checkCascade(shadowTex2), checkCascade(shadowTex3));
  // txDiffuseValue.rgb *= 0.75 + 0.25 * normalW.y;

  // txDiffuseValue.rg = frac(shadowTex0.xy);

  // if (checkCascade(pin.ShadowTex0)) txDiffuseValue.rgb = float3(0.5, 0, 0);
  // else if (checkCascade(pin.ShadowTex1)) txDiffuseValue.rgb = float3(0, 0.5, 0);
  // else if (checkCascade(pin.ShadowTex2)) txDiffuseValue.rgb = float3(0, 0, 0.5);
  // else txDiffuseValue.rgb = float3(0, 0, 0);
  // if (pin.ShadowTex0.z >= 1) txDiffuseValue.x *= checkerPattern;
  // if (pin.ShadowTex1.z >= 1) txDiffuseValue.y *= checkerPattern;
  // if (pin.ShadowTex2.z >= 1) txDiffuseValue.z *= checkerPattern;
  // txDiffuseValue.x *= frac((pin.ShadowTex0.x * 0.5 + 0.5) * 2048 / 2) > 0.5 || frac((pin.ShadowTex0.y * 0.5 + 0.5) * 2048 / 2) > 0.5;
  // txDiffuseValue.y *= frac((pin.ShadowTex1.x * 0.5 + 0.5) * 2048 / 2) > 0.5 || frac((pin.ShadowTex1.y * 0.5 + 0.5) * 2048 / 2) > 0.5;
  // txDiffuseValue.z *= frac((pin.ShadowTex2.x * 0.5 + 0.5) * 2048 / 2) > 0.5 || frac((pin.ShadowTex2.y * 0.5 + 0.5) * 2048 / 2) > 0.5;
  // txDiffuseValue.rgb = 0.5;
  // txDiffuseValue.rgb = shadow;
  // txDiffuseValue.rgb *= saturate(-dot(normalW, ksLightDirection.xyz));

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  #ifndef NO_SELFSHADOW
    APPLY_CAO;
  #endif
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_DIFFUSEMASK);
  LIGHTINGFX(lighting);

  #ifndef NO_SELFSHADOW
    #if defined(ALLOW_EXTRA_FEATURES) && !defined(NO_LIGHTING) && defined(MAIN_SHADERS_SET)
      if (ksAlphaRef == -193){
        lighting += GAMMA_KSEMISSIVE(ksEmissive) * GAMMA_LINEAR_SIMPLE(saturate(dot(normalize(-toCamera), normalize(normalW))) * 4 - 3) * 12;
      }
    #endif

    // if (extShowRainOcclusion){
    //   #if defined(ALLOW_RAINFX) && !defined(RAINFX_USE_PUDDLES_MASK) && !defined(MODE_SIMPLIFIED_FX)
    //     lighting = float3(saturate(pin.RainOcclusion), 1 - saturate(pin.RainOcclusion), 0);
    //   #else
    //     lighting = float3(0, 0, 1);
    //   #endif
    // }
  #endif

  // lighting = 3 * cascadeMask * lerp(0.5, 1, shadow) * saturate(dot(normalW, -ksLightDirection.xyz));
  // lighting = frac(float3(pin.Tex, 0));

  RAINFX_REFLECTIVE_WATER_ROUGH(lighting);
  RETURN(lighting, txDiffuseValue.a);
}
