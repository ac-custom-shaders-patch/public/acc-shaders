#define SUPPORTS_COLORFUL_AO
#define AO_MIX_INPUT 0
#define RAINFX_STATIC_OBJECT
#include "include_new/base/_include_vs.fx"
#include "include_new/ext_functions/wind.fx"

PS_IN_Grass main(VS_IN vin SPS_VS_ARG) {
  float windStrength = vsLoadHeating(vin.TangentPacked);

  float4 posL = vin.PosL;
  vin.PosL.xz += windOffset(vin.PosL.xyz, windStrength, 3.5) / 10;
  GENERIC_PIECE(PS_IN_Grass);
  vout.GrassThing = posW.xz - extSceneOffset.xz;

  posL.xz += windOffsetPrev(posL.xyz, windStrength, 3.5) / 10;
  GENERIC_PIECE_MOTION(posL);

  SPS_RET(vout);
}
