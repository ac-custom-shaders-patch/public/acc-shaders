#define SPS_NO_POSC
#include "include_new/base/_include_vs.fx"

PS_IN_ShadowGetAt main(VS_IN vin SPS_VS_ARG) {
  PS_IN_ShadowGetAt ret;
  float4 posW, posWnoflex, posV;
  ret.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  ret.Tex = vin.Tex;
  SPS_RET(ret);
}
