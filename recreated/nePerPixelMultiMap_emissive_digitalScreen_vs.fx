#define SUPPORTS_NORMALS_AO
// #define SUPPORTS_DISTANT_FIX
#include "emissiveMapping.hlsl"

PS_IN_NmExtra4 main(VS_IN vin SPS_VS_ARG) {
  PS_IN_NmExtra4 vout;
  float4 posW, posWnoflex, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  vout.Extra = getExtraValue(vin.PosL.xyz);
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
