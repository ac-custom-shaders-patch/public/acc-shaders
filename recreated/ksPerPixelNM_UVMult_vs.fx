#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"
// alias: ksPerPixelMultiMapSimpleRefl_vs

PS_IN_Nm main(VS_IN vin SPS_VS_ARG) {
  GENERIC_PIECE_NM(PS_IN_Nm);
  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout); 
}
  