#define USE_BRAKE_DISC_MODE
#define USE_BLURRED_NM
#define INCLUDE_TYRE_CB
#define CARPAINT_NM
#define NO_ADJUSTING_COLOR

#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txBlurValue = txBlur.Sample(samLinear, pin.Tex);
  float3 txGlowValue = txGlow.Sample(samLinear, pin.Tex).rgb;
  txDiffuseValue = lerp(txDiffuseValue, txBlurValue, blurLevel);

  float alpha = 1, directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha, directedMult);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.setSpecularMult(txDiffuseValue.a, 1);
  float3 lighting = L.calculate();
  lighting += txGlowValue * glowLevel * txDiffuseValue.rgb;
  LIGHTINGFX(lighting);

  ReflParams R = getReflParamsBase();
  R.useBias = true;
  float4 withReflection = calculateReflection(float4(lighting, alpha), toCamera, pin.PosC, normalW, R);
  RETURN(withReflection, 1);
}
