#define CARPAINT_NM
#define GETNORMALW_SAMPLER samLinear
#define SUPPORTS_AO
#define SUPPORTS_DITHER_FADING
#define GAMMA_TWEAKABLE_ALPHA
#define APPLY_LFX_HEADLIGHTS_FIX
#include "lightingBounceBack.hlsl"
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float alpha, directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha, directedMult);

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  ADJUSTCOLOR(txDiffuseValue);
  APPLY_VRS_FIX;
  RAINFX_WET(txDiffuseValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING, 1);
  L.txSpecularValue = GET_SPEC_COLOR;
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_DIFFUSEMASK);
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, 1);
  R.useBias = true;
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);
  float4 withReflection = calculateReflection(float4(lighting, alpha), toCamera, pin.PosC, normalW, R);
  if (HAS_FLAG(FLAG_ALPHA_TEST)
  #ifdef MODE_GBUFFER
    || HAS_FLAG(FLAG_GBUFFER_PREMULTIPLIED_ALPHA)
  #endif
  ) withReflection.a = alpha;

  RAINFX_WATER(withReflection);

  #ifdef MODE_GBUFFER
    // if (HAS_FLAG(FLAG_GBUFFER_PREMULTIPLIED_ALPHA)) {
    //   R.resultColor = lerp(10, R.resultColor, withReflection.a);
    //   R.resultPower = lerp(1, R.resultPower, withReflection.a);
    //   R.resultBlur = lerp(0, R.resultBlur, withReflection.a);
    //   R.coloredReflections = lerp(1, R.coloredReflections, withReflection.a);
    //   R.coloredReflectionsColor = lerp(0, R.coloredReflectionsColor, withReflection.a);
    //   withReflection.a = 1;
    // }
  #endif

  RETURN(withReflection, withReflection.a);
}
