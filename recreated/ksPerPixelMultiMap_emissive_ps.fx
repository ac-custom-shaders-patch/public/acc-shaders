#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define USE_GETNORMALW
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#define GAMMA_TWEAKABLE_ALPHA
#include "uv2Utils.hlsl"
#include "lightingBounceBack.hlsl"
#include "emissiveMapping.hlsl"
// alias: ksPerPixelMultiMap_AT_emissive_ps

RESULT_TYPE main(PS_IN_NmExtra4 pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  RAINFX_WET(txDiffuseValue.xyz);

  float alpha, directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha, directedMult);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);
  APPLY_VRS_FIX;

  float4 emissiveMap;
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING, 1);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL;

  float4 emissiveInput = txDiffuseValue;
  #ifndef MODE_KUNOS
    if (emDiffuseAlphaAsMultiplier3.x < 0) emissiveInput.a = alpha;
    if (emSkipDiffuseMap == -1) emissiveInput = getTxDetailValue(pin.Tex, false);
  #endif

  float2 emissiveUV = pin.Tex;
  if (HAS_FLAG(FLAG_MATERIAL_1)) {
    emissiveUV = PIN_TEX2;
  }
  L.txEmissiveValue = getEmissiveValue(emissiveInput, emissiveUV, pin.Extra, emissiveMap);
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_DIFFUSEMASK * saturate(dot(emissiveMap, extBounceBackMask)));
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, AO_REFLECTION, txMapsValue.xyz);
  R.useBias = true;
  R.isCarPaint = !(emAlphaFromDiffuse < 0);
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);

  if (abs(emAlphaFromDiffuse) == 1) alpha = txDiffuseValue.a;
  float4 withReflection = calculateReflection(float4(lighting, alpha), toCamera, pin.PosC, normalW, R);
  
  RAINFX_WATER(withReflection);
  alpha = emAlphaFromDiffuse == 2 ? 1 : emAlphaFromDiffuse < 0 ? withReflection.a : alpha;
  // withReflection = emissiveMap * 10;
  RETURN(withReflection.xyz, alpha);
}
