#define SUPPORTS_COLORFUL_AO
// #define SPS_NO_POSC

#include "include_new/base/_flags.fx"
#if defined(ALLOW_EXTRA_VISUAL_EFFECTS)
  #define CUSTOM_STRUCT_FIELDS\
    float3 PosL : TEXCOORD20;\
    float3 NormalL : TEXCOORD21;
#endif

#include "include_new/base/_include_vs.fx"

PS_IN_PerPixel main(VS_IN vin SPS_VS_ARG) {
  PS_IN_PerPixel vout;
  float4 posW, posWnoflex, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.NormalW = normalize(normals(vin.NormalL));
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  shadows(posW, SHADOWS_COORDS);
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  #if defined(ALLOW_EXTRA_VISUAL_EFFECTS)
    vout.PosL = vin.PosL.xyz;
    vout.NormalL = vin.NormalL.xyz;
  #endif
  SPS_RET(vout);
}
