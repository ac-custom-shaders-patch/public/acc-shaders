#define CARPAINT_NM_UVMULT
#define SUPPORTS_AO
#define GAMMA_TWEAKABLE_ALPHA
#define APPLY_LFX_HEADLIGHTS_FIX
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);

  #ifndef NO_NORMALMAPS
    float2 uvValueNormal = pin.Tex * (normalMult + 1);
    float4 txNormalValue = txNormal.Sample(samLinearSimple, uvValueNormal);
    float3 txNormalAligned = txNormalValue.xyz * 2 - (float3)1;
    float3x3 normalMatrix = float3x3(tangentW, normalW, bitangentW);
    normalW = normalize(mul(transpose(normalMatrix), txNormalAligned.xzy));
  #endif

  float2 uvValueDiffuse = pin.Tex * (diffuseMult + 1);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, uvValueDiffuse);
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR;
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, AO_REFLECTION, 1);
  R.useBias = true;
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);
  float4 withReflection = calculateReflection(float4(lighting, txDiffuseValue.a), toCamera, pin.PosC, normalW, R);
  
  RAINFX_WATER(withReflection);
  RETURN(withReflection, withReflection.a);

  // Possibly, this one is closer to Kunos logic
  // RETURN(withReflection, txDiffuseValue.a);
}
