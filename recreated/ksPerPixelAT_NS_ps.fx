#define NO_CARPAINT
#define NO_SHADOWS
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_AtNs pin) {
  READ_VECTORS
  float shadow = 1;
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  ADJUSTCOLOR(txDiffuseValue);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, 1, AO_LIGHTING);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);
  RETURN_BASE(lighting, txDiffuse.SampleLevel(samPoint, pin.Tex, 0).a);
}
