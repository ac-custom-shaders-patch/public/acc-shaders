#define CARPAINT_NM
#define GETNORMALW_SAMPLER samLinear
#define SUPPORTS_AO
#define SUPPORTS_DITHER_FADING
#define EMISSIVE_SLOT 2
#define USE_TXEMISSIVE_AS_EMISSIVE
#define TXEMISSIVE_VALUE emissiveMap
#define GAMMA_TWEAKABLE_ALPHA

#define CB_MATERIAL_EXTRA_3\
  float emSkipDiffuseMap;

#include "lightingBounceBack.hlsl"
#include "include_new/base/_include_ps.fx"

TEXTURE2D(txEmissive, EMISSIVE_SLOT);

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float alpha, directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha, directedMult);

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  APPLY_VRS_FIX;
  RAINFX_WET(txDiffuseValue.xyz);
  
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR;

  float4 emissiveMap = txEmissive.Sample(samLinear, pin.Tex);
  L.txEmissiveValue = emissiveMap.rgb * INPUT_EMISSIVE3 * lerp(1, txDiffuseValue.rgb, saturate(emSkipDiffuseMap));
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_DIFFUSEMASK);
  LIGHTINGFX(lighting);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, 1);
  R.useBias = true;
  R.isCarPaint = true;
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, normalW, R);
  
  RAINFX_WATER(withReflection);
  RETURN(withReflection, txDiffuseValue.a);
}
