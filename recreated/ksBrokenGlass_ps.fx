#define USE_GETNORMALW
#define NO_SSAO
#define NO_SSGI
#define NO_GBUFFER
#define GETNORMALW_NORMALIZED_INPUT
#define GETNORMALW_XYZ_TX
#define CARPAINT_DAMAGES
#define CARPAINT_GLASS

#define INPUT_DIFFUSE_K 0.0
#define INPUT_AMBIENT_K 0.0
#define INPUT_EMISSIVE3 0
#define IS_ADDITIVE_VAR 0

#include "include_new/base/_include_ps.fx"

float4 sampleSharpen(float2 uv){
  float sizeInv = 1./1024;
  float4 base = txNormal.Sample(samLinear, uv);
  float4 blurred = 0
    + txNormal.Sample(samLinear, uv + sizeInv * float2(0.5, 0.5), 0)
    + txNormal.Sample(samLinear, uv + sizeInv * float2(0.5, -0.5), 0)
    + txNormal.Sample(samLinear, uv + sizeInv * float2(-0.5, 0.5), 0)
    + txNormal.Sample(samLinear, uv + sizeInv * float2(-0.5, -0.5), 0);
  float4 delta = base - blurred / 4;
  return base;
  return base + 0.8 * saturate(dot(float4(delta.xyz, 0.5), 1)) - 0.4;
  // return base + 0.36 * saturate(dot(float4(delta.xyz, 0.5), 1)) - 0.18;
}

float3 getDamageNormalWLocal(float2 uv, float3 normalW, float3 tangentW, float3 bitangentW, float damage, 
    out float normalAlpha){
  #ifdef NO_NORMALMAPS
    clip(-1);
    normalAlpha = 0.0;
    return normalW;
  #else

    float4 txNormalValue = sampleSharpen(uv).xzyw;
    normalAlpha = txNormalValue.a;
    normalAlpha = damage * txNormalValue.a;

    if (INPUT_NM_OBJECT_SPACE){
      float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
      txNormalAligned.z *= -1;
      float3x3 m = (float3x3)ksWorld;
      return normalize(mul(txNormalAligned, m));
    } else {
      float3 txNormalAligned = txNormalValue.xzy * 2 - (float3)1;
      // txNormalAligned.xy *= 1.5;
      txNormalAligned = normalize(txNormalAligned);
      float3x3 m = float3x3(tangentW, bitangentW, normalW);
      return lerp(normalW, normalize(mul(transpose(m), txNormalAligned)), damage);
    }

  #endif
}

#include "refraction.hlsl"

RESULT_TYPE main(PS_IN_Nm pin) {
  #ifndef ALLOW_EXTRA_VISUAL_EFFECTS
    clip(-1);
    return (RESULT_TYPE)0;
  #endif

  READ_VECTORS_NM
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);

  float alpha;
  normalW = getDamageNormalWLocal(pin.Tex, normalW, tangentW, bitangentW, glassDamage, alpha);

  clip(alpha - 0.05);
  alpha = saturate(alpha * 4);

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.specularValue = 1;
  L.specularExp = 10;
  L.txSpecularValue = 1;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting.rgb);

  float extRefraction = 0.02;
  float additiveMode = 1;

  #ifndef MODE_KUNOS
    [branch]
    if (extRefraction){
      float2 offset = calculateRefractionOffset(pin.PosC, toCamera, normalW, 0.02);
      float4 prev = calculateRefractionColor(pin.PosH.xy * extScreenSize.zw + offset);
      if (prev.a > 0 && abs(dot(normalW, toCamera)) < 0.5){
        lighting += prev.xyz;
        additiveMode = 0;
      }
    }
  #endif

  ReflParams R = getReflParamsBase();
  R.useBias = true;
  R.fresnelMaxLevel = 0.5;
  R.fresnelC = 0;
  R.fresnelEXP = 5;
  R.ksSpecularEXP = 120;
  float3 withReflection = calculateReflection(lighting, toCamera, pin.PosC, -normalW, R);

  if (additiveMode){
    withReflection *= alpha;
    alpha = 0.1;
    withReflection /= alpha;
  }
  
  RETURN(withReflection, alpha);
}
