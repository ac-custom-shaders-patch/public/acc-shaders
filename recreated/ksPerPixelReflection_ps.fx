#define CARPAINT_SIMPLE
#define SUPPORTS_AO
#define SUPPORTS_DITHER_FADING
#define GAMMA_TWEAKABLE_ALPHA

// #define SAMPLE_REFLECTION_FN sampleReflectionTest
// float3 sampleReflectionTest(float3 reflDir, float blur, bool useBias, float4 extraParam);

#include "lightingBounceBack.hlsl"
#include "include_new/base/_include_ps.fx"

#include "include/poisson.hlsl"

#ifndef MODE_KUNOS
  float4 sampleNoise(float2 uv){
    float textureResolution = 32;
    uv = uv * textureResolution + 0.5;
    float2 i = floor(uv);
    float2 f = frac(uv);
    uv = i + f * f * (3 - 2 * f);
    uv = (uv - 0.5) / textureResolution;
    return txNoise.SampleLevel(samLinearSimple, uv, 0);
  }

  // void addNormalNoise(float3 posC, inout float3 normalW){
  //   float4 noise0 = sampleNoise(ksCameraPosition.xyz + posC, float3(1, 0, 0), extNormalNoiseScale);
  //   float4 noise1 = sampleNoise(ksCameraPosition.xyz + posC, float3(0, 0, 1), extNormalNoiseScale);
  //   float4 noise = lerp(noise0.x, noise1.x, abs(normalW.x));
  //   normalW += (noise.xyz - 0.5) * extNormalNoiseAmount;
  // }

  float sampleEmissiveNoise(float3 posW, float3 posC, float3 normalW){
    float3 side = normalize(cross(normalW, float3(0, 1, 0)));
    float3 input0 = posW + normalize(posC) * 4;
    float2 uv0 = float2(dot(side, input0), input0.y * 0.5) * 0.01;
    float3 input1 = posW + normalize(posC) * 2;
    float2 uv1 = float2(dot(side, input1), input1.y * 0.5) * 0.02;
    // return lerp(0, 2, (sampleNoise(uv0) * lerp(1, sampleNoise(uv1), 0.5)).x);
    return lerp(0.3, 1.7, (sampleNoise(uv0) * lerp(1, sampleNoise(uv1), 0.5)).x);
  }
#endif

static uint shuffle[9] = {2, 6, 7, 4, 1, 0, 8, 3, 5};

// Texture2D txSslrReflection : register(TX_SLOT_RAIN_DROPS);

// float3 sampleReflectionTest(float3 reflDir, float blur, bool useBias, float4 extraParam){
//   float4 r = txSslrReflection.SampleLevel(samLinearSimple, extraParam.xy, 0);  
//   float3 reflCubeDir = float3(-reflDir.x, reflDir.y, reflDir.z);
//   float4 c;
//   if (useBias){
//     float level = txCube.CalculateLevelOfDetail(samLinearSimple, reflDir);
//     c = txCube.SampleLevel(samLinearSimple, reflCubeDir, blur * REFLECTION_BLUR_MULT + level * saturate(2 - blur) * 0.4);
//   } else {
//     c = txCube.SampleLevel(samLinearSimple, reflCubeDir, blur * REFLECTION_BLUR_MULT);
//   }
//   return lerp(c.rgb, r.rgb, r.w);
// }

RESULT_TYPE main(PS_IN_PerPixel pin) {
  // float emissiveNoise = sampleEmissiveNoise(ksCameraPosition.xyz + pin.PosC, pin.PosC, pin.NormalW);
  // addNormalNoise(pin.PosC, pin.NormalW);
  READ_VECTORS
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR;
  // L.txEmissiveValue *= emissiveNoise;
  APPLY_CAO;

  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_DIFFUSEMASK);
  LIGHTINGFX(lighting);

  // txDiffuseValue.a = frac(pin.Tex.x * 100);

  ReflParams R = getReflParams(L.txSpecularValue, EXTRA_SHADOW_REFLECTION * AO_REFLECTION, 1);
  R.useBias = true;
  // R.reflectionSampleParam.xy = pin.PosH.xy * extScreenSize.zw; // TODO
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE(R);
  float4 withReflection = calculateReflection(float4(lighting, txDiffuseValue.a), toCamera, pin.PosC, normalW, R);
  RAINFX_WATER(withReflection);

  // withReflection.rgb = GI_LIGHTING;

  // #ifdef ALLOW_EXTRA_VISUAL_EFFECTS
    // [branch]
    // if (_cbPerObject_pad0 != 1) {
    //   float v = dot(uint2(pin.PosH.xy) % 2, float2(1./4, 2./4));
    //   if (HAS_FLAG(FLAG_FADING_INV)) v = 1 - v;
    //   clip(_cbPerObject_pad0 - v);
    // }
  // #endif
  
  RETURN(withReflection, withReflection.a);
}
