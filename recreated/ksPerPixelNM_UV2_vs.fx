#define SUPPORTS_NORMALS_AO
#include "include_new/base/_include_vs.fx"

PS_IN_Uv2 main(VS_IN vin SPS_VS_ARG) {
  GENERIC_PIECE_NM(PS_IN_Uv2);
  vout.Tex2 = vin.Tex;
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
