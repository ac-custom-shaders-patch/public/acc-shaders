// #define USE_NEW_CODE
#ifdef USE_NEW_CODE
// #inclu_de "../custom_objects/smCarPaint_ps.fx"
#else

#define REFLECTION_FRESNELEXP_BOUND
#define USE_SHADOW_BIAS_MULT
#define CARPAINT_DAMAGES
#define CARPAINT_DAMAGEZONESDIRT
// #define LIGHTING_SPECULAR_EXP_MULT txMapsValue.y
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"
#include "../custom_objects/common/carDirt.hlsl"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  // damage-related
  float4 txDamageMaskValue = txDamageMask.Sample(samLinear, pin.Tex);
  float4 txDamageValue = txDamage.Sample(samLinear, pin.Tex);
  float normalMultiplier = dot(txDamageMaskValue, damageZones);
  float damageInPoint = saturate(normalMultiplier * txDamageValue.a);

  // normals piece
  float normalAlpha;
  normalW = getDamageNormalW(pin.Tex, normalW, tangentW, bitangentW, normalMultiplier, normalAlpha);

  // usual stuff
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMapsValue = txMaps.Sample(samLinear, pin.Tex);
  RAINFX_INIT;
  
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue.xyz);

  // new damaged txDiffuseValue
  txDiffuseValue.xyz = damageInPoint > 0 
      ? lerp(txDiffuseValue.xyz, txDamageValue.xyz, damageInPoint) 
      : txDiffuseValue.xyz;

  // dirt
  float4 txDustValue = txDust.Sample(samLinear, pin.Tex);
  COMPUTE_DIRT_LEVEL;
  txDiffuseValue.xyz = lerp(txDiffuseValue.xyz, txDustValue.xyz, dirtLevel);

  // both damages and dirt affecting txMaps
  float damageInverted = 1 - damageInPoint;
  float mapsMultiplier = saturate(1 - dirtLevel * 10);
  float damageFactor = damageInPoint * (normalAlpha - 1) + 1;
  mapsMultiplier *= damageInverted;
  mapsMultiplier *= damageFactor;
  txMapsValue.x *= mapsMultiplier;

  // lighting
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING);
  L.txSpecularValue = GET_SPEC_COLOR_MASK_DETAIL_EXTRA(damageInverted * (1 - dirtLevel));
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  float extraMultiplier = isAdditive ? mapsMultiplier * EXTRA_SHADOW_REFLECTION : EXTRA_SHADOW_REFLECTION;
  ReflParams R;
  R.fresnelMaxLevel = fresnelMaxLevel * extraMultiplier;
  R.fresnelC = fresnelC * extraMultiplier;
  R.fresnelEXP = fresnelEXP;
  R.ksSpecularEXP = txMapsValue.y * ksSpecularEXP;
  R.finalMult = txMapsValue.z * AO_REFLECTION;
  R.useBias = true;
  R.isCarPaint = true;
  R.useSkyColor = false;
  R.metallicFix = 1;
  R.coloredReflections = 1;
  R.coloredReflectionsColor = L.txSpecularValue;
  R.useStereoApproach = false;
  R.forceReflectedDir = false;
  R.forcedReflectedDir = 0;
  R.stereoExtraMult = 0;
  R.reflectionSampleParam = 0;
  R.globalOcclusionMult = 1;
  R.useGlobalOcclusionMult = false;
  R.useMatteFix = false;
  
  APPLY_EXTRA_SHADOW_OCCLUSION(R); RAINFX_REFLECTIVE_SKIP(R);
  float3 withReflection = calculateReflection(float4(lighting, 1), toCamera, pin.PosC, normalW, R).rgb;

  RAINFX_WATER(withReflection);
  RETURN(withReflection, 1);
}

#endif