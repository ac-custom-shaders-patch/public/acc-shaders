#define SPS_NO_POSC
#define SKINNED_WEIGHT_CHECK(a) (a)
#define SKINNED_VERTEX
#include "include_new/base/_include_vs.fx"

struct VS_OUT {
  float4 PosH : SV_POSITION;
};

VS_OUT main(VS_IN_Skinned vin SPS_VS_ARG) {
  float4 posW, posWnoflex, posV;
  float3 normalW, tangentW;

  VS_OUT ret;
  ret.PosH = toScreenSpaceSkinned(vin.PosL, vin.NormalL, vin.TangentPacked, vin.BoneWeights, vin.BoneIndices,
    posW, posWnoflex, normalW, tangentW, posV);
  SPS_RET(ret);
}
