#define A2C_SHARPENED
#define NO_CARPAINT
// #define NO_GBUFFER
#define NO_SSAO
#define NO_GRASSFX_COLOR
#define NO_EXTAMBIENT
#define INCLUDE_GRASS_CB
#define NO_EXTSPECULAR
#define RAINFX_FOLIAGE
#define RAINFX_STATIC_OBJECT
#define RAINFX_NO_RAINDROPS
#define LIGHTINGFX_GRASS
#define SUPPORTS_AO
#include "include_new/base/_include_ps.fx"
#include "smoothA2C.hlsl"

RESULT_TYPE main(PS_IN_Grass pin PS_INPUT_EXTRA) {
  READ_VECTORS
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  
  // float4 txDiffuseBlurred = txDiffuse.SampleBias(samLinear, pin.Tex, 5.5);
  // float foliageBaseHighlight = luminance(txDiffuseValue.rgb - txDiffuseBlurred.rgb) * 4;
  // float foliageSpecular = saturate(foliageBaseHighlight + lerp(1, 0.2, txDiffuseBlurred.a));
  float foliageSpecular = 0.5;

  float2 uv = pin.GrassThing * scale;
  float3 var = txVariation.Sample(samLinear, uv).xyz - (float3)0.5;
  var *= gain;
  txDiffuseValue.rgb = txDiffuseValue.rgb * var + txDiffuseValue.rgb;
  txDiffuseValue.a *= saturate(1 - boh);
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue);

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  float3 lighting = L.calculate();

  // #ifdef ALLOW_RAINFX
  //   L.txDiffuseValue *= lerp(0.2, 2 + RP.damp * 4, foliageSpecular);
  // #else
  //   L.txDiffuseValue *= lerp(0.2, 2, foliageSpecular);
  // #endif
  LIGHTINGFX(lighting);

  RAINFX_REFLECTIVE(lighting);
  A2C_ALPHA(txDiffuseValue.a);
  RETURN_BASE(lighting, txDiffuseValue.a);
}
