#define NO_CARPAINT
#define SUPPORTS_AO

#define CB_MATERIAL_EXTRA_1\
  float translucencyMult;

#define STENCIL_VALUE 1
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_PerPixel pin, bool ff : SV_IsFrontFace) {
  READ_VECTORS

  if (ff && HAS_FLAG(FLAG_MATERIAL_1)) {
    normalW *= -1;
    pin.Tex.x = 1 - pin.Tex.x;
  }

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);

  #if defined(MAIN_SHADERS_SET) || defined(SIMPLIFIED_EFFECTS)
    float wetK = (extSceneWetness * 40 / (1 + extSceneWetness * 40)) * pin.Ao * 0.35;
    txDiffuseValue.rgb *= 1 - wetK;
    txDiffuseValue.a = lerp(txDiffuseValue.a, 1, wetK * 2);
  #endif

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  #if defined(MAIN_SHADERS_SET) || defined(SIMPLIFIED_EFFECTS)
    L.specularValue = lerp(L.specularValue, 1, wetK);
    L.specularExp = lerp(L.specularExp, 100, wetK);
  #endif
  float3 lighting = L.calculate();
  lighting += shadow
    * ksLightColor.xyz 
    * applyGamma(txDiffuseValue.xyz * GAMMA_OR(1, INPUT_DIFFUSE_K), INPUT_DIFFUSE_K)
    * saturate(sqrt(dot(ksLightDirection.xyz, normalW))) 
    * translucencyMult;

  if (HAS_FLAG(FLAG_ALPHA_TEST)) {
    clip(txDiffuseValue.a - 0.01);
  }

  LIGHTINGFX(lighting);
  RETURN_BASE(lighting, txDiffuseValue.a);
}
