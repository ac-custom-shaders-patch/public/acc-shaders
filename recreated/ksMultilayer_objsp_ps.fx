#define NO_CARPAINT
#define INCLUDE_MULTILAYER2_CB
#define SUPPORTS_AO
#define RAINFX_STATIC_OBJECT
#define APPLY_LFX_HEADLIGHTS_FIX
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_PerPixel pin) {  
  READ_VECTORS
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float4 txMaskValue = txMask.SampleBias(samLinear, pin.Tex, TILT_BIAS);
  float4 txDetailRValue = txDetailR.Sample(samLinear, pin.Tex * multR);
  float4 txDetailGValue = txDetailG.Sample(samLinear, pin.Tex * multG);
  float4 txDetailBValue = txDetailB.Sample(samLinear, pin.Tex * multB);
  float4 txDetailAValue = txDetailA.Sample(samLinear, pin.Tex * multA);

  float4 combined = 
      txDetailRValue * txMaskValue.x
    + txDetailGValue * txMaskValue.y
    + txDetailBValue * txMaskValue.z
    + txDetailAValue * txMaskValue.w;
  txDiffuseValue *= combined;
  txDiffuseValue *= magicMult;
  ADJUSTCOLOR(txDiffuseValue);
  RAINFX_WET(txDiffuseValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  L.setSpecularMult(txDiffuseValue.a, 1);
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  RAINFX_REFLECTIVE_WATER_ROUGH(lighting);
  RETURN(lighting, 1);
}
