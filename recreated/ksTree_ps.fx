
#ifndef BUSH_MODE
  #define SHADOWS_FILTER_SIZE 2
#else
  #define CUSTOM_STRUCT_FIELDS\
    float ShadowMult : SHADOW1;
#endif

#define NO_CARPAINT
#define NO_SSAO
#define A2C_SHARPENED
// #define FIX_BASIC_A2C_ALT 
// #define NO_SHADOWS
#define NO_EXTAMBIENT
#define AMBIENT_SIMPLE_FN(x) saturate((x).y * 0.4 + 0.6)
#define NO_EXTSPECULAR
#define NO_GRASSFX_COLOR
#define RAINFX_STATIC_OBJECT
#define RAINFX_FOLIAGE
#define RAINFX_NO_RAINDROPS
#define LIGHTINGFX_TREE 
// #define LIGHTINGFX_BOUNCEBACK
// #define LIGHTINGFX_BOUNCEBACK_EXP 20
#define SUPPORTS_AO
#define USE_PS_FOG_IN_MAIN
// #define CUSTOM_STRUCT_FIELDS float Fade : COLOR4;
#include "include_new/base/_include_ps.fx"
#include "smoothA2C.hlsl"

#ifdef BUSH_MODE
RESULT_TYPE main(PS_IN_PerPixel pin PS_INPUT_EXTRA) {
#else
RESULT_TYPE main(PS_IN_Tree pin PS_INPUT_EXTRA) {
#endif
  READ_VECTOR_TOCAMERA

  float3 normalW = pin.NormalW;
  #ifndef BUSH_MODE
    float extrasMult = saturate(pin.Wind * 4 - 0.2);
    float shadow = pin.Shadow * getCloudShadow(pin.PosC) * saturate(dot(AO_LIGHTING, 0.5));
  #else
    float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW) * pin.ShadowMult;
  #endif

  float4 txDiffuseValue = txDiffuse.SampleBias(samLinearSimple, pin.Tex, -0.5);
  // float4 txDiffuseValue = txDiffuse.Sample(samLinearSimple, pin.Tex);
  // float4 txDiffuseValue = txDiffuse.SampleLevel(samLinearSimple, pin.Tex, 10);
  
  // float4 txDiffuseBlurred = txDiffuse.SampleBias(samLinear, pin.Tex, 5.5);
  // float foliageBaseHighlight = luminance(txDiffuseValue.rgb - txDiffuseBlurred.rgb);
  // float foliageSpecular = saturate(foliageBaseHighlight + lerp(1, 0.2, txDiffuseBlurred.a)) * extrasMult;
  float foliageSpecular = 0.5;

  // normalW = normalize(normalW * 0.25 + (ddy(dot(txDiffuseValue.rgb, 1)) * extDirUp + ddx(dot(txDiffuseValue.rgb, 1)) * normalize(cross(extDirUp, extDirLook))));

  ADJUSTCOLOR_PV(txDiffuseValue);
  RAINFX_WET(txDiffuseValue);

  // float4 txDiffuseValueB = txDiffuse.SampleBias(samLinear, pin.Tex, 2.5);
  // normalW = normalize(normalW + float3(0, (0.1 + txDiffuseValue.g - txDiffuseValueB.g) * 10, 0));

  // normalW = float3(0, 1, 0);
  // shadow = 1;
  if (GAMMA_FIX_ACTIVE) {
    pin.Ao *= saturate(normalW.y * 0.4 + 0.6);
  }

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  float3 lighting = L.calculate();
  
  // #ifdef ALLOW_RAINFX
  //   L.txDiffuseValue *= lerp(0.2, 2 + RP.damp * 4, foliageSpecular);
  // #else
  //   L.txDiffuseValue *= lerp(0.2, 2, foliageSpecular);
  // #endif
  LIGHTINGFX(lighting);

  // lighting = float3(txDiffuseBlurred.a, 1 - txDiffuseBlurred.a, 0);
  // lighting = float3(foliageSpecular, 1 - foliageSpecular, 0);
  // lighting = float3(pin.Wind, 1 - pin.Wind, 0);

  [branch]
  if (HAS_FLAG(FLAG_MATERIAL_0)) {
    float3 dpos = ddx(pin.PosC);
    float3 dnorm = normalize(cross(dpos, float3(0, 1, 0)));
    float4 noise = txNoise.Sample(samLinearSimple, pin.Tex * 3.7);
    txDiffuseValue.a *= saturate((abs(dot(dnorm, toCamera)) + abs(toCamera.y) * 0.3) * 10 - 2.5 - noise.x - length(normalW.xz) * 2);
  }

  RAINFX_REFLECTIVE(lighting);
  A2C_ALPHA(txDiffuseValue.a);

  // lighting = pin.Shadow;
  // pin.Fade = length(normalW.xz);
  // lighting = float3(pin.Fade, 1 - pin.Fade, 0);

  // txDiffuseValue.a = (txDiffuseValue.a - 0.35) / max(fwidth(txDiffuseValue.a), 0.0001) + 0.5;

  RETURN_BASE(lighting, txDiffuseValue.a);
}
