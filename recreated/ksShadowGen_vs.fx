#define SPS_NO_POSC
#include "include_new/base/_include_vs.fx"
// alias: ksSelectedMesh_vs

struct VS_OUT {
  float4 PosH : SV_POSITION;
};

VS_OUT main(VS_IN vin SPS_VS_ARG) {
  float4 posW, posWnoflex, posV;
  VS_OUT ret;
  ret.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  SPS_RET(ret);
}
