#define STENCIL_VALUE 0
#define GBUFF_MASKING_MODE true
#define GBUFF_NORMAL_W_SRC float3(0,1,0)
#define GBUFF_NORMAL_W_PIN_SRC float3(0,1,0)
#define GET_MOTION(x) (float2)0
#define NO_CARPAINT
#define NO_ADJUSTING_COLOR

#include "include_new/base/_include_ps.fx"

#undef AO_LIGHTING
#define AO_LIGHTING 0

RESULT_TYPE main(PS_IN_SkidMark pin) {
  READ_VECTORS
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);

  float4 txDiffuseValue = txDiffuse.Sample(samLinearSimple, pin.Tex);
  txDiffuseValue.rgb *= GAMMA_OR(0.6, 1);
  RAINFX_INIT;

  float alpha = txDiffuseValue.a * pin.SkidMarkThing;
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING);
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  float4 result = float4(lighting, alpha);
  #ifdef NO_LIGHTING
    result.rgb = 0.0;
  #endif  
  #ifdef ALLOW_RAINFX
    result.a *= 1 - RP.water;
    RP = (RainParams)0;
  #endif

  result.a = GAMMA_ALPHA(result.a);
  RETURN_BASE(result.rgb, result.a);
}
