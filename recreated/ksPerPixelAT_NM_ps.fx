#define NO_CARPAINT
#define USE_GETNORMALW
#define GETNORMALW_SAMPLER samLinear // John514 fix
#define SUPPORTS_AO
#define SUPPORTS_DITHER_FADING
#define GAMMA_TWEAKABLE_ALPHA
#include "lightingBounceBack.hlsl"
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  
  float alpha, directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, alpha, directedMult);
  
  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);  
  ADJUSTCOLOR(txDiffuseValue);
  APPLY_VRS_FIX;
  RAINFX_WET(txDiffuseValue.xyz);

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, AO_LIGHTING, 1);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = L.calculate();
  BOUNCEBACKFX(lighting, BOUNCEBACKFX_DIFFUSEMASK);
  LIGHTINGFX(lighting);

  RAINFX_REFLECTIVE_WATER_ROUGH(lighting);
  // txDiffuseValue.a = GAMMA_LINEAR(txDiffuseValue.a);
  RETURN(lighting, txDiffuseValue.a);
}
