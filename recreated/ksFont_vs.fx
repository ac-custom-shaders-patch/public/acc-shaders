#define SPS_NO_POSC
#include "include_new/base/_include_vs.fx"

PS_IN_Font main(VS_IN vin SPS_VS_ARG) {
  PS_IN_Font vout;
  float4 posW, posWnoflex, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.Color = vin.NormalL;
  vout.Tex = vin.Tex;
  vout.TangentH = (float3)0;
  GENERIC_PIECE_MOTION(vin.PosL);
  VERTEX_POSTPROCESS(vout);
  // vout.PosH.z *= 0.9;
  SPS_RET(vout);
}
