#define REFLECTION_FRESNELEXP_BOUND
#define CARPAINT_SKINNED
#define GETNORMALW_NORMALIZED_INPUT
#define GETNORMALW_NORMALIZED_TB
#define GETNORMALW_XYZ_TX
#define GETNORMALW_SAMPLER samLinearSimple
#define SUPPORTS_AO
#define GAMMA_TWEAKABLE_ALPHA
#include "include_new/base/_include_ps.fx"
#include "include_new/ext_functions/depth_map.fx"

RESULT_TYPE main(PS_IN_Nm pin) {
  READ_VECTORS_NM

  // float gradient = fwidth(abs(dot(normalW, normalize(pin.PosC))));
  // float toon = abs(dot(normalW, normalize(pin.PosC))) < gradient * 4;

  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float3 txMapsValue = txMaps.Sample(samLinear, pin.Tex).xyz;
  RAINFX_WET(txDiffuseValue.xyz);

  // float4 txDiffuseValueBak = txDiffuseValue;
  // txDiffuseValue.rgb = 1;
  // shadow = lerp(0.5, 1, lerpInvSat(shadow, 0.3, 0.7));

  float directedMult;
  normalW = getNormalW(pin.Tex, normalW, tangentW, bitangentW, directedMult);
  considerDetails(pin.Tex, txDiffuseValue, txMapsValue);
  APPLY_VRS_FIX;

  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y * AO_LIGHTING, 1);
  L.applyTxMaps(txMapsValue.xyz);
  APPLY_CAO;
  RAINFX_SHINY(L);
  float3 lighting = calculateMapsLighting_woSun(L);
  LIGHTINGFX(lighting);

  ReflParams R = getReflParamsBase(EXTRA_SHADOW_REFLECTION * AO_REFLECTION, txMapsValue);
  R.useBias = true;
  R.isCarPaint = true;

  RAINFX_REFLECTIVE(R);
  float4 withReflection = calculateReflection(float4(lighting, txDiffuseValue.a), toCamera, pin.PosC, normalW, R);

  // if (transparentOutlinePass){
  //   float toon = 0;
  //   float n = linearizeAccurate(pin.PosH.z);
  //   float2 dd = float2(ddx(n), ddy(n));
  //   [unroll] for (int y = -1; y <= 1; ++y){
  //     [unroll] for (int x = -1; x <= 1; ++x){
  //       if (x == 0 && y == 0) continue;
  //       float d = linearizeAccurate(txDepth.Sample(samLinear, pin.PosH.xy * extScreenSize.zw, int2(x, y)));
  //       float f = d - (n + dd.x * x + dd.y * y); 
  //       toon = max(toon, lerpInvSat(f, 0.008, 0.018));
  //     }
  //   }
  //   withReflection = float4(0, 0, 0, toon * 0.8);
  //   // withReflection = float4(toon, 1 - toon, 0, toon * 0.8);
  //   // n = linearizeAccurate(txDepth.Sample(samLinear, pin.PosH.xy * extScreenSize.zw, int2(0, -2)));
  //   // withReflection = float4(n, 1 - n, 0, toon * 0.8);
  //   // clip(withReflection.a - 0.01);
  // }

  // TODO: Later, could be done better, but for now I want drops to show up on carbon of RSS 2010 V8, which has
  // alpha channel set to 0 with alpha blending disabled.
  float alphaBak = withReflection.a;
  withReflection.a = 1;
  RAINFX_WATER(withReflection);
  withReflection.a = alphaBak;

  // withReflection.rgb /= 1e20;
  // #if defined(ALLOW_RAINFX) && !defined(RAINFX_USE_PUDDLES_MASK) && !defined(MODE_SIMPLIFIED_FX)
  //   withReflection.rgb += float3(saturate(pin.RainOcclusion), 1 - saturate(pin.RainOcclusion), 0) * extWhiteRefPoint;
  // #else
  //   withReflection.rgb += float3(0, 0, 1) * extWhiteRefPoint;
  // #endif

  // withReflection.rgb = frac(pin.NormalR * 4);

  RETURN(withReflection.rgb, withReflection.a);
}
