#define NO_CARPAINT
#define ALPHATEST_THRESHOLD 0.5

#define CB_MATERIAL_EXTRA_3\
	float bannerMode;\
	float solidBrightnessAdjustment;\
	float3 extEdgePosL;\
	float extEdgeRefractionBias;\
	float extEdgeThreshold;\
	float reflectionsIntensity;\
	float alphaGamma;

#include "include_new/base/_flags.fx"
#if defined(ALLOW_EXTRA_VISUAL_EFFECTS)
  #define CUSTOM_STRUCT_FIELDS\
    float3 PosL : TEXCOORD20;\
    float3 NormalL : TEXCOORD21;
#endif

#include "include_new/base/_include_ps.fx"
#include "include_new/ext_functions/depth_map.fx"
#include "windscreenBase.hlsl"

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS

	float3 resultColor;
	float resultAlpha;
	float4 txDiffuseValue;
	float shadow;
	doWindscreenBase(pin, normalW, toCamera, resultColor, resultAlpha, txDiffuseValue, shadow);
	
	// float2 ssUV = pin.PosH.xy * extScreenSize.zw;
	// resultColor = txPrevFrame.SampleLevel(samLinearClamp, ssUV, 2).rgb;
	// resultAlpha = 1;

	bool earlyExit;
	#if defined(MODE_GBUFFER) || defined(MODE_KUNOS)
		earlyExit = true;
	#elif defined(MODE_COLORMASK)
		earlyExit = false;
	#else
		earlyExit = extMotionStencil == 17;
		[branch]
	#endif
	if (earlyExit){
		#if defined(ALLOW_EXTRA_VISUAL_EFFECTS)
			float depthZ = 10;
			depthZ = min(depthZ, getDepthAccurate(pin.PosH + float4(+1, -1, 0, 0)));
			depthZ = min(depthZ, getDepthAccurate(pin.PosH + float4(+1, +1, 0, 0)));
			depthZ = min(depthZ, getDepthAccurate(pin.PosH + float4(-1, +1, 0, 0)));
			depthZ = min(depthZ, getDepthAccurate(pin.PosH + float4(-1, -1, 0, 0)));
			
			float depthC = linearizeAccurate(pin.PosH.z);
			clip(depthC - depthZ);
		#else
			clip(resultAlpha - 0.9);
		#endif
	}

  #ifdef MODE_COLORMASK
    resultColor = txDiffuseValue.rgb;
		resultAlpha = txDiffuseValue.a;
  #endif

	#if defined(ALLOW_EXTRA_VISUAL_EFFECTS)
		float extraBackgroundBlur = max(0, bannerMode - 1);
		resultAlpha = GAMMA_ALPHA(resultAlpha);

		[branch]
		if (extEdgeRefractionBias || extraBackgroundBlur){
			float edgeBase = dot(normalize(extEdgePosL - pin.PosL), normalize(pin.NormalL));
			float edgeK = saturate(remap(edgeBase, extEdgeThreshold - 0.01, extEdgeThreshold, 0, 1));

      [branch]
      if (edgeK > 0 || extraBackgroundBlur){
        float2 ssUV = pin.PosH.xy * extScreenSize.zw;
        float4 refracted = txPrevFrame.SampleLevel(samLinearClamp, ssUV, extEdgeRefractionBias + extraBackgroundBlur);
				if (GAMMA_FIX_ACTIVE) refracted.rgb *= lerp(1, GAMMA_LINEAR(txDiffuseValue.rgb), txDiffuseValue.a);
        mixLayerBelow(resultColor, resultAlpha, refracted.rgb, edgeK * refracted.a);

				if (extraBackgroundBlur){
					resultColor = lerp(refracted.rgb, resultColor, resultAlpha);
					resultAlpha = 1;
				}

        if (extEdgeRefractionBias < 0){
          resultColor = float3(1 - edgeK, edgeK, 0) * 3;
          resultAlpha = 1;
        }
      }
		}
	#endif

  RETURN_BASE(resultColor, resultAlpha);
}
