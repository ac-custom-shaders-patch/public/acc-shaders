#define SUPPORTS_COLORFUL_AO
#include "include_new/base/_include_vs.fx"
// alias: ksCarPaintSimple_vs

PS_IN_PerPixel main(VS_IN vin SPS_VS_ARG) {
  PS_IN_PerPixel vout;
  float4 posW, posWnoflex, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.NormalW = normalize(normals(vin.NormalL));
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  // vout.Tex += frac(posW.xz) * float2(0.05, 0);
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  shadows(posW, SHADOWS_COORDS);
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);
  SPS_RET(vout);
}
