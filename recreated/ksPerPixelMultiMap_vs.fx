#define SUPPORTS_NORMALS_AO
// #define SUPPORTS_DISTANT_FIX
#include "include_new/base/_include_vs.fx"
// alias: ksBrokenGlass_vs
// alias: ksPerPixelAT_NM_vs
// alias: ksPerPixelMultiMap_AT_NMDetail_vs
// alias: ksPerPixelMultiMap_AT_vs
// alias: ksPerPixelMultiMap_damage_dir2_vs
// alias: ksPerPixelMultiMap_damage_dirt_vs
// alias: ksPerPixelMultiMap_damage_vs
// alias: ksPerPixelMultiMap_NMDetail_vs 
// alias: ksPerPixelMultiMap_toon_vs

// #include "include/samplers_vs.hlsl"
// cbuffer _cbColorAmbientOcclusion : register(b5) {
//   float4x4 extCaoUV;
// }
// Texture2D<float> txCarHeightMap : register(TX_SLOT_PREV_FRAME);
// Texture2D txColorShadow : register(TX_SLOT_SHADOW_COLOR);
// float3 calculateCao(float3 posW, float3 normalW){
//   float4 uv = mul(float4(posW, 1), extCaoUV);
//   float v = txCarHeightMap.SampleLevel(samLinearBorder0, uv.xy, 0);
//   float d = (uv.z - v) * 5;
//   return lerp(1, txColorShadow.SampleLevel(samLinearBorder0, uv.xy, 0.5 + d * 3).rgb, saturate(normalW.y + 2 - d * 2));
// }

PS_IN_Nm main(VS_IN vin SPS_VS_ARG) {
  PS_IN_Nm vout;
  float4 posW, posWnoflex, posV;
  vout.PosH = toScreenSpace(vin.PosL, posW, posWnoflex, posV);
  vout.NormalW = normals(vin.NormalL);
  vout.PosC = posW.xyz - ksCameraPosition.xyz;
  vout.Tex = vin.Tex;
  OPT_STRUCT_FIELD_FOG(vout.Fog = calculateFog(posV));
  shadows(posW, SHADOWS_COORDS);
  PREPARE_TANGENT;
  PREPARE_AO(vout.Ao);
  GENERIC_PIECE_MOTION(vin.PosL);
  RAINFX_VERTEX(vout);
  VERTEX_POSTPROCESS(vout);

  // vout.Ao *= calculateCao(posW.xyz, vout.NormalW);

  SPS_RET(vout);
}
