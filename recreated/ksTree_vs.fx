#define SUPPORTS_COLORFUL_AO
#define AO_MIX_INPUT 0
#define NO_CLOUD_SHADOW
// #define SHADOWS_FILTER_SIZE 4
#define SHADOWS_FILTER_SIZE 1
// #define cascadeShadowStep cascadeShadowStep_custom
// #define USE_STRETCHED_SHADOWS
#define USE_OPTIMIZED_SHADOWS
#define USE_LAST_SHADOWS_CASCADE_ONLY
#define NO_SHADOWS_CASCADES_TRANSITION
#define RAINFX_STATIC_OBJECT
// #define CUSTOM_STRUCT_FIELDS float Fade : COLOR4;
#include "include_new/base/_include_vs.fx"
#include "include_new/ext_shadows/_include_vs.fx"
#include "include_new/ext_functions/wind.fx"

#if defined(NO_SHADOWS) || defined(MODE_KUNOS)

  float getShadow_avg(float3 posBase){
    return 1;
  }

#else

  float getShadow_step(float3 posBase){
    #ifdef MODE_KUNOS
      return 1;
    #else
      float4 uv = float4(mul(float4(posBase, 1), ksShadowMatrix0).xyz * shadowsMatrixModifier3M + shadowsMatrixModifier3A, 1);
      // return 1;
      return uv.z >= 1 ? 1 : txShadowArray.SampleCmpLevelZero(samShadow, float3(uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5), 3), uv.z);
    #endif
  }

  /* #define AREA_DISK_SIZE 6
  float sampleArea(float3 uv){
    // return txShadowArray.SampleLevel(samLinearSimple, uv, 0);

    float ret = 0;
    for (int i = 0; i < AREA_DISK_SIZE; ++i){
      ret += txShadowArray.SampleLevel(samLinearSimple, uv + float3(SAMPLE_POISSON(AREA_DISK_SIZE, i), 0) * 0.002, 0);
    }
    return ret / AREA_DISK_SIZE;
  }

  float approximateLamberOcclusion(float3 posBase){
    float4 uv = float4(mul(float4(posBase, 1), ksShadowMatrix0).xyz * shadowsMatrixModifier3M + shadowsMatrixModifier3A, 1);
    float3 samplePoint = float3(uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5), 3);

    const float R = 0.003;
    float p0 = txShadowArray.SampleLevel(samLinearSimple, samplePoint + float3(0, 0, 0), 0);
    float pU = sampleArea(samplePoint + float3(0, R, 0));
    float pD = sampleArea(samplePoint + float3(0, -R, 0));
    float pL = sampleArea(samplePoint + float3(R, 0, 0));
    float pR = sampleArea(samplePoint + float3(-R, 0, 0));

    const float M = 5 * textureFrameRangeRatio;
    float3 dir0 = normalize(cross(float3(0, R * M, pU - pD), float3(R * M, 0, pL - pR)));
    return saturate(abs(dir0.z) * 1.4);
  } */

  float getShadow_step2(float3 posBase){
    #ifdef MODE_KUNOS
      return 1;
    #else
      float4 uv = float4(mul(float4(posBase, 1), ksShadowMatrix0).xyz * shadowsMatrixModifier3M + shadowsMatrixModifier3A, 1);
      // return 1;
      return uv.z >= 1 ? 1 : txShadowArray.SampleCmpLevelZero(samShadow, float3(uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5), 3), uv.z);
    #endif
  }

  float getShadowWit(float2 uv, float2 realTextureSize, float comparisonValue){    
    float2 uvAdj = uv.xy * realTextureSize;
    float2 baseUV = floor(uvAdj + 0.5);
    float s = uvAdj.x + 0.5 - baseUV.x;
    float t = uvAdj.y + 0.5 - baseUV.y;

    baseUV -= float2(0.5, 0.5);
    baseUV *= pixelSize;
    float sum = 0;
    float tx = 3;
    float uw0 = (5 * s - 6);
    float uw1 = (11 * s - 28);
    float uw2 = -(11 * s + 17);
    float uw3 = -(5 * s + 1);
    float u0 = (4 * s - 5) / uw0 - 3;
    float u1 = (4 * s - 16) / uw1 - 1;
    float u2 = -(7 * s + 5) / uw2 + 1;
    float u3 = -s / uw3 + 3;
    float vw0 = (5 * t - 6);
    float vw1 = (11 * t - 28);
    float vw2 = -(11 * t + 17);
    float vw3 = -(5 * t + 1);
    float v0 = (4 * t - 5) / vw0 - 3;
    float v1 = (4 * t - 16) / vw1 - 1;
    float v2 = -(7 * t + 5) / vw2 + 1;
    float v3 = -t / vw3 + 3;
    sum += uw0 * vw0 * SampleShadowMap(baseUV, u0, v0, comparisonValue, tx);
    sum += uw1 * vw0 * SampleShadowMap(baseUV, u1, v0, comparisonValue, tx);
    sum += uw2 * vw0 * SampleShadowMap(baseUV, u2, v0, comparisonValue, tx);
    sum += uw3 * vw0 * SampleShadowMap(baseUV, u3, v0, comparisonValue, tx);
    sum += uw0 * vw1 * SampleShadowMap(baseUV, u0, v1, comparisonValue, tx);
    sum += uw1 * vw1 * SampleShadowMap(baseUV, u1, v1, comparisonValue, tx);
    sum += uw2 * vw1 * SampleShadowMap(baseUV, u2, v1, comparisonValue, tx);
    sum += uw3 * vw1 * SampleShadowMap(baseUV, u3, v1, comparisonValue, tx);
    sum += uw0 * vw2 * SampleShadowMap(baseUV, u0, v2, comparisonValue, tx);
    sum += uw1 * vw2 * SampleShadowMap(baseUV, u1, v2, comparisonValue, tx);
    sum += uw2 * vw2 * SampleShadowMap(baseUV, u2, v2, comparisonValue, tx);
    sum += uw3 * vw2 * SampleShadowMap(baseUV, u3, v2, comparisonValue, tx);
    sum += uw0 * vw3 * SampleShadowMap(baseUV, u0, v3, comparisonValue, tx);
    sum += uw1 * vw3 * SampleShadowMap(baseUV, u1, v3, comparisonValue, tx);
    sum += uw2 * vw3 * SampleShadowMap(baseUV, u2, v3, comparisonValue, tx);
    sum += uw3 * vw3 * SampleShadowMap(baseUV, u3, v3, comparisonValue, tx);
    return sum / 2704;
  }

  #define SHADOW_DISK_SIZE 16
  float getShadow_avg(float3 posBase){
    // float ret = 0;
    // for (int i = 0; i < SHADOW_DISK_SIZE; i++){
    //   float2 base = SAMPLE_POISSON(SHADOW_DISK_SIZE, i);
    //   float3 offset = float3(base.x, frac(dot(base, 17.33)), base.y) * 6;
    //   ret += getShadow_step(posBase + offset);
    // }
    float ret = 0;
    posBase += ksLightDirection.xyz * -5;

    float4 uv = float4(mul(float4(posBase, 1), ksShadowMatrix0).xyz * shadowsMatrixModifier3M + shadowsMatrixModifier3A, 1);
    uv.xy = uv.xy * float2(0.5, -0.5) + float2(0.5, 0.5);

    // ret += txShadowArray.SampleCmpLevelZero(samShadow, float3(uv.xy, 3), uv.z);

    ret = getShadowWit(uv.xy, realTextureSize, uv.z);

    // ret += getShadow_step(posBase + float3(2, 3, 2));
    // ret += getShadow_step(posBase + float3(2, 3, -2));
    // ret += getShadow_step(posBase + float3(-2, 3, 2));
    // ret += getShadow_step(posBase + float3(-2, 3, -2));
    // ret += getShadow_step(posBase + float3(3, 5, 3));
    // ret += getShadow_step(posBase + float3(3, 5, -3));
    // ret += getShadow_step(posBase + float3(-3, 5, 3));
    // ret += getShadow_step(posBase + float3(-3, 5, -3));
    // ret /= SHADOW_DISK_SIZE;
    // ret /= 8;
    // return approximateLamberOcclusion(posBase) * lerp(1, ret / SHADOW_DISK_SIZE, extShadowsOpacityMult);
    return lerp(1, ret, extShadowsOpacityMult);
  }

#endif

// float4 toScreenSpaceAlt(float4 posL){
//   float4 posW = mul(posL, ksWorld);
//   float4 posV = mul(posW, ksView);
//   return mul(posV, ksProjection);
// }

Texture2D<float> txWindOffsets : register(t21);
Texture2D<float> txWindOffsetsPrev : register(t19);

void applyWind(float phaseOffset, inout float3 pos, float3 up, float affectedByWind, float height, bool prev){
  Texture2D<float> tx = prev ? txWindOffsetsPrev : txWindOffsets;
  float windMult = affectedByWind 
    * (tx.SampleLevel(samLinearSimple, (pos.xyz - extSceneOffset).xz * 0.1 / 80 + phaseOffset * 0.05, 0) 
      + tx.SampleLevel(samLinearSimple, (pos.xyz - extSceneOffset).xz * 0.1 / 21.71 + phaseOffset * 0.05, 0) * 0.471);
  float2 windOffset = windMult * (prev ? extWindVelPrev : extWindVel);
  windOffset = windOffset / (1 + abs(windOffset));
  pos.xz += windOffset * height;
  pos.y -= min(0.5, 1 - sqrt(1 - dot2(windOffset))) * (0.5 + frac(558.476 * phaseOffset)) * height;
}

PS_IN_Tree main(VS_IN vin SPS_VS_ARG) {
  float windStrength = vsLoadHeating(vin.TangentPacked);
  // windStrength = 1;
  float4 posL = vin.PosL;
  vin.PosL.xz += windOffset(vin.PosL.xyz, windStrength, 1) * 4;
  // vin.NormalL.xz += windOffset(vin.PosL.xyz, windStrength, 1.3) * length(vin.NormalL);
  // applyWind(0, vin.PosL.xyz, float3(0, 1, 0), windStrength, 2, false);
  GENERIC_PIECE_NOSHADOWS(PS_IN_Tree);

  #if defined(ALLOW_EXTRA_VISUAL_EFFECTS) && defined(RECEIVE_SHADOWS)
    float3 ao = 1;
    PREPARE_AO(ao);
    vout.Shadow = shadowsMatrixModifier3M.x ? getShadow_avg(posW.xyz) : 1;
  #else
    vout.Shadow = 1;
  #endif

  float byteX = BYTE0(asuint(vin.TangentPacked.y));
  float byteZ = BYTE1(asuint(vin.TangentPacked.y));
  if (byteX && byteZ) {
    float3 landscapeNormal = float3(byteX / 127. - 1, 0, byteZ / 127. - 1);
    landscapeNormal.xz = clamp(landscapeNormal.xz, -1, 1);
    landscapeNormal.y = sqrt(saturate(1 - dot2(landscapeNormal.xz)));
    landscapeNormal = normalize(landscapeNormal);
    vout.Shadow *= saturate(dot(landscapeNormal, -ksLightDirection.xyz) * 4);
  }

  // vout.NormalW = normalize(vout.NormalW);
  posL.xz += windOffsetPrev(posL.xyz, windStrength, 1) * 4;
  GENERIC_PIECE_MOTION(posL);

  vout.Wind = windStrength;

  // uint dirXb = BYTE2(asuint(vin.TangentPacked.y));
  // uint dirZb = BYTE3(asuint(vin.TangentPacked.y));
  // if (dirXb && dirZb) {
  //   // vout.Fade = 1 - abs(dot(float2(dirXb / 127. - 1, dirZb / 127. - 1), normalize(vout.PosC.xz)));
  //   vout.Fade = dirXb / 255;
  //   // vout.Fade = 1 ;  
  // } else {
  //   vout.Fade = 1;
  // }

  SPS_RET(vout);
}
