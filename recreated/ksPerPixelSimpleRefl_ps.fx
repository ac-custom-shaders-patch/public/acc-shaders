#define CARPAINT_NM
#define GETNORMALW_SAMPLER samLinear
#define SUPPORTS_AO
#define GAMMA_TWEAKABLE_ALPHA
#include "include_new/base/_include_ps.fx"

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS
  
  float shadow = getShadow(pin.PosC, pin.PosH, normalW, SHADOWS_COORDS, AO_FALLBACK_SHADOW);
  APPLY_EXTRA_SHADOW

  float4 txDiffuseValue = txDiffuse.Sample(samLinear, pin.Tex);
  float alpha = txDiffuseValue.a;
  ADJUSTCOLOR(txDiffuseValue);
  
  LightingParams L = getLightingParams(pin.PosC, toCamera, normalW, txDiffuseValue.xyz, shadow, extraShadow.y);
  APPLY_CAO;
  float3 lighting = L.calculate();
  LIGHTINGFX(lighting);

  ReflParams R = getReflParamsBase(EXTRA_SHADOW_REFLECTION * AO_REFLECTION);
  R.useBias = true;
  R.useSkyColor = true;
  float4 withReflection = calculateReflection(float4(lighting, alpha), toCamera, pin.PosC, normalW, R);
  RETURN(withReflection, withReflection.a);
}
