#define NO_CARPAINT
#define SUPPORTS_AO

#define INPUT_DIFFUSE_K 0.25
#define INPUT_AMBIENT_K 0.45
#define INPUT_EMISSIVE3 0
#define INPUT_SPECULAR_K 0
#define INPUT_SPECULAR_EXP 0

#define LIGHTINGFX_KSDIFFUSE 0.55
#define LIGHTINGFX_NOSPECULAR

#include "include_new/base/_include_ps.fx"
#include "refraction.hlsl"
#include "include/bayer.hlsl"

cbuffer cbSingleColor : register(b5) {
  float4 colour;
  float extGlow;
  float extRefraction; // 0.2 if enabled
  float2 extPad0;
}

RESULT_TYPE main(PS_IN_PerPixel pin) {
  READ_VECTORS
  float3 lighting = colour.rgb;
  lighting *= AO_REFLECTION;
  // lighting *= lerp(1, 16, extGlow * AO_REFLECTION * AO_REFLECTION);

  float fade = colour.a * saturate(length(pin.PosC) / 4 - 1);
  if (fade < 0.001) discard;

  float fresnel = saturate(pow(1 - abs(dot(normalW, toCamera)), 4));
  float alpha = saturate(fade * lerp(1, 0, fresnel));
  lighting *= lerp(1, 20, extGlow * fresnel);

  #ifdef ALLOW_EXTRA_VISUAL_EFFECTS
    if (extRefraction) {
      float4 refracted = calculateRefraction(pin.PosH, pin.PosC, toCamera, normalW, 
        (0 + 0.1 * length(pin.PosC)) * fade * extRefraction);
      refracted.a *= saturate(fade * 10);
      if (refracted.a) {
        lighting = lerp(refracted.xyz, lighting, alpha);
        alpha = lerp(alpha, 1, refracted.a);
      }
    }
  #else
    alpha += extRefraction * 0.00001;
  #endif

  #ifdef MODE_GBUFFER
    uint2 pos = uint2(pin.PosH.xy);
    uint2 xy3 = pos.xy % 3;
    float fadeAdj = saturate(fade * 2) * saturate(remap(length(pin.PosC), 3, 6, 0, 1));

    if (fadeAdj < -getBayer(pin.PosH)) {
      discard;
    }
  #else
    clip(alpha - 0.001);
  #endif

  RETURN_BASE(lighting, alpha);
  // RETURN_BASE(lighting, fade);
}
