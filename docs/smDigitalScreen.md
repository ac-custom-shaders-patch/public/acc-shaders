Shader for digital screens, emulating either IPS or TNT matrix. As one of shaders in “sm…” series, doesn’t take lighting parameters into account. They’re there for compatibility reasons, but they don’t affect anything.

Supported types:
- TNT: `smScreenType = 0`;
- IPS: `smScreenType = 1`.

To get it working, set non-zero *ksEmissive* (screens should glow or they’re not functioning), adjust smScreenScale (think of it as resolution) and, in most cases, set *smUseTextureCoordinates* to 1 (although in some rare cases it might only work correctly with 0 in there).

If you need a new type, please contact me. Also, in general, if you’re targeting CSP only, it would be much better to set up water using extension config and “materials_track.ini”.