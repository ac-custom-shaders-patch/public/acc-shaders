This shader is an exact copy of **ksPerPixelMultiMap_NMDetail**, but with an extra *txEmissive* map used as multiplier for emissive color. You can use a black and white mask in there, or add color. Original shader multiplies *txDiffuse* by *ksEmissive* (similar to “Multiply” blending in Photoshop), this one also adds *txEmissive* multiplication step.

Few examples:
- White *txDiffuse*, red *txEmissive*, *ksEmissive* set to `(1, 1, 1)`: red glow.
- Red *txDiffuse*, yellow *txEmissive*, *ksEmissive* set to `(1, 1, 1)`: red glow.

Set *emSkipDiffuseMap* to `1` to stop *txDiffuse* from affecting emissive color. 

### Multi-channel mode

To enable multi-channel mode, set *emChannelsMode* option to `1`. In that mode, shader will use red channel of *txEmissive* as a mask for *ksEmissive*, green channel is mask for *ksEmissive1*, blue is for *ksEmissive2* and alpha is for *ksEmissive3*. Later, in either car or track config, you can bind different emissive variables to different events, allowing you to use a single mesh for, for example, braking, parking and reverse lights without splitting.

If you have a symmetrical mesh, you can use more than four channels: for example, if you set *emMirrorChannel3As4* to `1`, anything on one side of symmetry plane would use *ksEmissive4* instead of *ksEmissive3*. Use *emMirrorDir* and *emMirrorOffset* to setup symmetry plane (default value for mirror direction would be `1 0 0`, to mirror from left to right; also, keep in mind it uses local mesh coordinates). With this option, you can get all car’s rear lights from a single mesh: use three normal channels for brake, parking and reverse lights, and third+fourth for turning lights (so map for turning lights would be set in alpha channel of txEmissive).