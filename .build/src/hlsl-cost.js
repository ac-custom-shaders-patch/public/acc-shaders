module.exports = function (data) {
  function calculateCost(command, dims) {
    if (command.substr(command.length - 4) == '_sat') {
      command = command.substr(0, command.length - 4);
    }
    switch (command) {
      case 'add':
      case 'and':
      case 'breakc_nz':
      case 'deriv_rtx_coarse':
      case 'deriv_rty_coarse':
      case 'discard_nz':
      case 'dp2':
      case 'dp3':
      case 'dp4':
      case 'eq':
      case 'f16tof32':
      case 'ftou':
      case 'iadd':
      case 'imax':
      case 'ineg':
      case 'ishl':
      case 'itof':
      case 'if_nz':
      case 'if_z':
      case 'lt':
      case 'mad':
      case 'max':
      case 'min':
      case 'mov':
      case 'movc':
      case 'mul':
      case 'ne':
      case 'switch':
      case 'ubfe':
      case 'uge':
      case 'ushr':
      case 'utof':
      case 'xor':
        return dims;
      case 'exp':
      case 'log':
      case 'rsq':
      case 'sincos':
        return 4 * dims;
      case 'div':
      case 'sqrt':
        return 5 * dims;
      case 'udiv':
        return 10 * dims;
      default:
        if (/^sample_\w+\((\w+)\)/.test(command)) {
          return RegExp.$1 == 'texturecube' ? 15 : 10;
        }
        // console.log(command);
        return dims;
    }
  }

  function estimateLine(raw) {
    if (!/\s*\d+:\s+(.+)/.test(raw)) return 0;

    const line = RegExp.$1;
    const commands = line.match(/^(\S+)(?: [\w[\]]+(?:\.(\w+))?)/);
    if (!commands) {
      // console.log(line);
      return 1;
    }

    if (commands[1] == 'label') {
      return 0;
    }

    if (commands[2] == null) {
      // console.log(line);
      return 1;
    }

    return calculateCost(commands[1], commands[2].length);
  }

  return data.split('\n').reduce((a, b) => a + estimateLine(b), 0);
};