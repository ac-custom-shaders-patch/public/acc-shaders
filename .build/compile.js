/**
 * Simple script for compiling changed shaders, designed for VS Code. Based on shelljs module, somewhat modified
 * by shelljs-wrap.js.
 */

// Configuration:
β.config.colorful = true;

// Apps used:
if (!process.env['SHADERS_FXC_LOCATION']) {
  $.echo(β.yellow(`Variable SHADERS_FXC_LOCATION is not set, using guessed value`));
  process.env['SHADERS_FXC_LOCATION'] = 'C:/Program Files (x86)/Windows Kits/10/bin/10.0.18362.0/x64/fxc.exe';
}

if (!fs.existsSync(process.env['SHADERS_FXC_LOCATION'])) {
  process.env['SHADERS_FXC_LOCATION'] = 'C:/Program Files (x86)/Microsoft DirectX SDK (June 2010)/Utilities/bin/x64/fxc.exe';
}

if (!fs.existsSync(process.env['SHADERS_FXC_LOCATION'])) {
  $.fail('Couldn’t find “fxc.exe” to compile shaders');
}

if (!fs.existsSync(process.env['SHADERS_FXC_ALT_LOCATION'])) {
  process.env['SHADERS_FXC_ALT_LOCATION'] = process.env['SHADERS_FXC_LOCATION'];
}

const $fxc = $[process.env['SHADERS_FXC_LOCATION']].withOptions({ quiet: true });
$fxc.target.CS = 'cs_5_0';

const $fxcAlt = $[process.env['SHADERS_FXC_ALT_LOCATION']].withOptions({ quiet: true });
const $dxc = $[process.env['SHADERS_FXC_LOCATION'].replace('fxc.exe', 'dxc.exe')].withOptions({ quiet: true });
// const $dxc = $['C:\\Apps\\VulkanSDK\\1.3.236.0\\Bin\\dxc.exe'].withOptions({ quiet: true });
// const $dxc = $['C:\\Program Files (x86)\\Windows Kits\\10\\bin\\10.0.19041.0\\x64\\dxc.exe'].withOptions({ quiet: true });

// Params:
let forceCompile = process.argv.indexOf('--force') !== -1 || process.argv.indexOf('-f') !== -1;
let verboseRun = process.argv.indexOf('--verbose') !== -1 || process.argv.indexOf('-v') !== -1;
let processAll = process.argv.indexOf('--process-all') !== -1 || process.argv.indexOf('-a') !== -1;
let useDxcForAll = process.argv.indexOf('--use-dxc') !== -1 || process.argv.indexOf('-d') !== -1;

// Consts:
const source = process.cwd();
const shaderParamsFilename = `${source}/SHADER_PARAMS.txt`;
const shaderListsFilename = `${source}/SHADER_LISTS.txt`;
const shadersDir = `${source}/.output`;
const sdkShadersDir = `${source}/.sdk`;
const acShadersDir = process.env['AC_SHADERS_LOCATION'] && fs.existsSync(process.env['AC_SHADERS_LOCATION']) ? process.env['AC_SHADERS_LOCATION'] : null;
const temporary = `${os.tmpdir()}/acc-shaders`;
const FX = { Target: { CS: 'cs_5_0', PS: 'ps_5_0', VS: 'vs_5_0', HS: 'hs_5_0', DS: 'ds_5_0', GS: 'gs_5_0' } };
let concurrency = process.argv.indexOf('--single-thread') !== -1 ? 1 : 4;
const hlslCost = require('./src/hlsl-cost');

// Shaders to build for various categories
function isTrackSpecific(x) {
  return [
    'Flags', 'Grass', 'Tree', 'light_glow', 'ksMultilayer', 'ksPerPixel_horizon', 'smWaterSurface', 'smCable',
    'nePerPixelMultiMap_tessellation', 'nePerPixelMultiMap_windows', 'flgTree', 'smTyreMarks', 'smWaterDrain'
  ].some(y => x.indexOf(y) !== -1);
}

function isNeverACarShader(x) {
  return [
    'IdealLine', 'ksParticle'
  ].some(y => x.indexOf(y) !== -1);
}

const FilterLists = {};
if (fs.existsSync(shaderListsFilename)) {
  let currentList = null;
  for (let s of $.readText(shaderListsFilename).split('\n')) {
    s = s.trim();
    if (/(.+?):$/.test(s)) {
      currentList = FilterLists[RegExp.$1.trim().replace(/\W+/g, '').toLowerCase()] = [];
    } else if (s && s[0] != '#') {
      currentList.push(β.toRegExp(s));
    }
  }
}

function listFilter(x) {
  let list = FilterLists[x.replace(/\W+/g, '').toLowerCase()] || [];
  return {
    test: x => {
      let name = x.replace(/^.+[\/\\]|_(?:[a-z]s|vss)\.fx$/g, '')
      return list.some(y => y.test(name));
    }
  };
}

const CategoryFilters = {
  Custom: { test: x => /^custom\b/.test(x) && !/_mainobj_/.test(x) },
  CustomNoVariations: /^custom_novariations\b/,
  Object: /^(recreated|custom_objects)\b|_mainobj_/,
  ObjectOrLayers: /^(recreated|custom_objects)\b|_mainobj_/,
  MainNoLighting: listFilter('mainNoLighting'),
  ColorMaskSeeThrough: listFilter('colorSample, see-through'),
  ShadowsAdvanced: listFilter('ShadowsAdvanced'),
  Kunos: listFilter('basic'),
  SampleEmissive: {
    test: x => /^(recreated|custom_objects)\b|_mainobj_/.test(x) && [
      'GL', 'BrakeDisc', 'Broken', 'Clouds', 'Colour', 'Fake', 'Font', 'Particle',
      'damage', 'Selected', 'Shadow', 'SkidMark', 'Skinned', 'Sky',
      'Windscreen', 'Tyres', 'smTrackLines'
    ].every(y => x.indexOf(y) === -1) && !isTrackSpecific(x) && !isNeverACarShader(x)
  },
  WindscreenReflected: {
    test: x => /^(recreated|custom_objects)\b|_mainobj_/.test(x) && [
      'GL', 'BrakeDisc', 'Broken', 'Clouds', 'Fake', 'Particle',
      'Selected', 'Shadow', 'SkidMark', 'Sky',
      'Windscreen', 'Tyres', 'smTrackLines'
    ].every(y => x.indexOf(y) === -1) && !isTrackSpecific(x) && !isNeverACarShader(x)
  },
  Wet: listFilter('wet'),
  WetDrops: listFilter('wet, drops'),
  WetPuddles: listFilter('wet, puddles'),
  WetWipermask: listFilter('wet, wipers'),
  GrassFX: {
    test: x => /^recreated\\ks/.test(x) && [
      'GL', 'BrakeDisc', 'Broken', 'CarPaint', 'Clouds', 'Colour', 'Fake', 'Flags', 'Font', 'Grass', 'Particle',
      'damage', 'emissive', 'Selected', 'Shadow', 'SkidMark', 'Skinned', 'Sky', 'Tree',
      'Windscreen', 'Tyres', 'damage_dirt', 'smTrackLines'
    ].every(y => x.indexOf(y) === -1) || /fuPBR/.test(x) || /smWaterSurface/.test(x)
  },
  LightMap: {
    test: x => /^recreated\\ks/.test(x) && [
      'GL', 'BrakeDisc', 'Broken', 'CarPaint', 'Clouds', 'Colour', 'Fake', 'Flags', 'Font', 'Grass', 'Particle',
      'damage', 'Selected', 'Shadow', 'SkidMark', 'Skinned', 'Sky', 'Tree',
      'Windscreen', 'Tyres', 'damage_dirt'
    ].every(y => x.indexOf(y) === -1) || /\bsmWaterSurface/.test(x)
  },
  SampleNormal: {
    test: x => /^(recreated|custom_objects)\b/.test(x) && [
      'GL', 'Clouds', 'Colour', 'Fake', 'Flags', 'Font', 'Grass', 'Particle',
      'Selected', 'Shadow', 'SkidMark', 'Skinned', 'Sky', 'smTrackLines'
    ].every(y => x.indexOf(y) === -1) || /\bsmWaterSurface/.test(x)
  },
  ColorMask: {
    test: x => /^(recreated|custom_objects)\b/.test(x) && [
      'GL', 'BrakeDisc', 'Broken', 'CarPaint', 'Clouds', 'Colour', 'Fake', 'Flags', 'Font', 'Particle',
      'damage', 'emissive', 'SimpleRefl', 'Selected', 'Shadow', 'SkidMark', 'Skinned', 'Sky', 'Tree',
      'Tyres', 'damage_dirt', 'stPerPixelMulti', 'ksMultilayer', 'ksPerPixel_horizon',
      'ksPerPixel_ppfog', 'ksPerPixel_tilingfix', 'smCarPaint', 'nePerPixelMultiMap_windows',
      'parallax', 'tessellation', 'smCable', 'smDigitalScreen', 'heating', 'smRefractingCover', 'smWater', 'PBR',
      'Grass', 'nePerPixel_light', 'nePerPixelMultiMap_digitalScreen', 'smTrackLines'
    ].every(y => x.indexOf(y) === -1)
  },
};

// If you don’t need some categories or shaders, simply comment them out:
let Categories = [
  { filter: CategoryFilters.Custom, destination: `${shadersDir}/custom`, title: 'custom', defines: { 'MODE_CUSTOM': 1 }, withVss: true },
  { filter: CategoryFilters.CustomNoVariations, destination: `${shadersDir}/custom_nv`, title: 'custom, NV', defines: { 'MODE_SIMPLEST': 1 } },
  // { filter: CategoryFilters.Kunos, destination: `${shadersDir}/basic`, title: 'basic', defines: { 'MODE_KUNOS': 1 }, skipCopying: true },

  { filter: CategoryFilters.ObjectOrLayers, destination: `${shadersDir}/main_fx`, title: 'main, FX', defines: { 'MODE_MAIN_FX': 1 }, withVss: true },
  { filter: CategoryFilters.ObjectOrLayers, destination: `${shadersDir}/main_fs`, title: 'main, FS', defines: { 'MODE_MAIN_FX': 1, 'ALLOW_DYNAMIC_SHADOWS': 1 }, withVss: true },
  { filter: CategoryFilters.MainNoLighting, destination: `${shadersDir}/main_nolighting`, title: 'main, NL', defines: { 'MODE_MAIN_NOFX': 1 }, withVss: true },
  { filter: CategoryFilters.ShadowsAdvanced, destination: `${shadersDir}/shadows_advanced`, title: 'shadows', defines: { 'MODE_SHADOWS_ADVANCED': 1 } },

  { filter: CategoryFilters.WindscreenReflected, destination: `${shadersDir}/windscreen_fx`, title: 'windscreen, FX', defines: { 'MODE_WINDSCREEN_FX': 1 }, withVss: true },
  { filter: CategoryFilters.WindscreenReflected, destination: `${shadersDir}/windscreen_fs`, title: 'windscreen, FS', defines: { 'MODE_WINDSCREEN_FX': 1, 'ALLOW_DYNAMIC_SHADOWS': 1 }, withVss: true },

  { filter: CategoryFilters.Object, destination: `${shadersDir}/gbuff_nat`, title: 'G-buf., A', defines: { 'MODE_GBUFFER': 1, 'USE_ALPHATEST': 1 } },
  { filter: CategoryFilters.ObjectOrLayers, destination: `${shadersDir}/gbuff_nop`, title: 'G-buf.', defines: { 'MODE_GBUFFER': 1 } },
  { filter: CategoryFilters.Object, destination: `${shadersDir}/gbuff_rat`, title: 'G-buf., R+A', defines: { 'MODE_GBUFFER': 1, 'CREATE_REFLECTION_BUFFER': 1, 'USE_ALPHATEST': 1 } },
  { filter: CategoryFilters.ObjectOrLayers, destination: `${shadersDir}/gbuff_rop`, title: 'G-buf., R', defines: { 'MODE_GBUFFER': 1, 'CREATE_REFLECTION_BUFFER': 1 } },

  { filter: CategoryFilters.Wet, destination: `${shadersDir}/wet_fx`, title: 'wet, FX', defines: { 'MODE_MAIN_FX': 1, 'ALLOW_RAINFX': 1 }, withVss: true },
  { filter: CategoryFilters.Wet, destination: `${shadersDir}/wet_fs`, title: 'wet, FS', defines: { 'MODE_MAIN_FX': 1, 'ALLOW_RAINFX': 1, 'ALLOW_DYNAMIC_SHADOWS': 1 }, withVss: true },
  { filter: CategoryFilters.WetDrops, destination: `${shadersDir}/wed_fx`, title: 'wet, DX', defines: { 'MODE_MAIN_FX': 1, 'ALLOW_RAINFX': 1, 'RAINFX_USE_DROPS': 1 }, withVss: true },
  { filter: CategoryFilters.WetDrops, destination: `${shadersDir}/wed_fs`, title: 'wet, DS', defines: { 'MODE_MAIN_FX': 1, 'ALLOW_RAINFX': 1, 'RAINFX_USE_DROPS': 1, 'ALLOW_DYNAMIC_SHADOWS': 1 }, withVss: true },
  // { filter: CategoryFilters.Wet, destination: `${shadersDir}/wet_gbuff_nat`, title: 'wet, G, A', defines: { 'MODE_GBUFFER': 1, 'USE_ALPHATEST': 1, 'ALLOW_RAINFX_': 1 /* WHAT?! */ } },
  // { filter: CategoryFilters.Wet, destination: `${shadersDir}/wet_gbuff_nop`, title: 'wet, G', defines: { 'MODE_GBUFFER': 1, 'ALLOW_RAINFX_': 1 } },
  // { filter: CategoryFilters.Wet, destination: `${shadersDir}/wet_gbuff_rat`, title: 'wet, G, R+A', defines: { 'MODE_GBUFFER': 1, 'CREATE_REFLECTION_BUFFER': 1, 'USE_ALPHATEST': 1, 'ALLOW_RAINFX_': 1 } },
  // { filter: CategoryFilters.Wet, destination: `${shadersDir}/wet_gbuff_rop`, title: 'wet, G, R', defines: { 'MODE_GBUFFER': 1, 'CREATE_REFLECTION_BUFFER': 1, 'ALLOW_RAINFX_': 1 } },
  { filter: CategoryFilters.WetPuddles, destination: `${shadersDir}/wep_fx`, title: 'wet, PX', defines: { 'MODE_MAIN_FX': 1, 'ALLOW_RAINFX': 1, 'RAINFX_USE_PUDDLES_MASK': 1 }, withVss: true },
  { filter: CategoryFilters.WetPuddles, destination: `${shadersDir}/wep_fs`, title: 'wet, PS', defines: { 'MODE_MAIN_FX': 1, 'ALLOW_RAINFX': 1, 'RAINFX_USE_PUDDLES_MASK': 1, 'ALLOW_DYNAMIC_SHADOWS': 1 }, withVss: true },
  { filter: CategoryFilters.WetPuddles, destination: `${shadersDir}/wep_gbuff_nat`, title: 'wet, P+G, A', defines: { 'MODE_GBUFFER': 1, 'USE_ALPHATEST': 1, 'ALLOW_RAINFX': 1, 'RAINFX_USE_PUDDLES_MASK': 1 } },
  { filter: CategoryFilters.WetPuddles, destination: `${shadersDir}/wep_gbuff_nop`, title: 'wet, P+G', defines: { 'MODE_GBUFFER': 1, 'ALLOW_RAINFX': 1, 'RAINFX_USE_PUDDLES_MASK': 1 } },
  { filter: CategoryFilters.WetPuddles, destination: `${shadersDir}/wep_gbuff_rat`, title: 'wet, P+G, R+A', defines: { 'MODE_GBUFFER': 1, 'CREATE_REFLECTION_BUFFER': 1, 'USE_ALPHATEST': 1, 'ALLOW_RAINFX': 1, 'RAINFX_USE_PUDDLES_MASK': 1 } },
  { filter: CategoryFilters.WetPuddles, destination: `${shadersDir}/wep_gbuff_rop`, title: 'wet, P+G, R', defines: { 'MODE_GBUFFER': 1, 'CREATE_REFLECTION_BUFFER': 1, 'ALLOW_RAINFX': 1, 'RAINFX_USE_PUDDLES_MASK': 1 } },
  { filter: CategoryFilters.WetPuddles, destination: `${shadersDir}/puddles`, title: 'puddles', defines: { 'MODE_PUDDLES': 1, 'ALLOW_RAINFX': 1, 'RAINFX_USE_PUDDLES_MASK': 1 } },
  { filter: CategoryFilters.WetPuddles, destination: `${shadersDir}/shadows_heat`, title: 'shadows heat', defines: { 'MODE_SHADOWS_HEAT': 1 } },
  { filter: CategoryFilters.WetWipermask, destination: `${shadersDir}/wet_fx_wipermask`, title: 'wet, WX', defines: { 'MODE_MAIN_FX': 1, 'ALLOW_RAINFX': 1, 'RAINFX_USE_WIPERS_MASK': 1, 'RAINFX_USE_DROPS': 1 }, withVss: true },
  { filter: CategoryFilters.WetWipermask, destination: `${shadersDir}/wet_fs_wipermask`, title: 'wet, WS', defines: { 'MODE_MAIN_FX': 1, 'ALLOW_RAINFX': 1, 'RAINFX_USE_WIPERS_MASK': 1, 'RAINFX_USE_DROPS': 1, 'ALLOW_DYNAMIC_SHADOWS': 1 }, withVss: true },

  { filter: CategoryFilters.Object, destination: `${shadersDir}/simplified_fx`, title: 'simpler, FX', defines: { 'MODE_SIMPLIFIED_FX': 1 } },
  { filter: CategoryFilters.Object, destination: `${shadersDir}/simplified_nofx`, title: 'simpler', defines: { 'MODE_SIMPLIFIED_NOFX': 1 } },
  { filter: CategoryFilters.Wet, destination: `${shadersDir}/simplified_wet`, title: 'simpler, wet', defines: { 'MODE_SIMPLIFIED_FX': 1, 'ALLOW_RAINFX': 1 } },
  { filter: CategoryFilters.Object, destination: `${shadersDir}/simplest`, title: 'simplest', defines: { 'MODE_SIMPLEST': 1 } },
  { filter: CategoryFilters.GrassFX, destination: `${shadersDir}/fxgrass`, title: 'grassFX', defines: { 'MODE_GRASSFX': 1 } },
  { filter: CategoryFilters.LightMap, destination: `${shadersDir}/lightmap`, title: 'light map', defines: { 'MODE_LIGHTMAP': 1 } },
  { filter: CategoryFilters.LightMap, destination: `${shadersDir}/lightmap_fs`, title: 'light map, FS', defines: { 'MODE_LIGHTMAP': 1, 'ALLOW_DYNAMIC_SHADOWS': 1 } },
  { filter: CategoryFilters.Object, destination: `${shadersDir}/sample_color`, title: 'color sample', defines: { 'MODE_COLORSAMPLE': 1 }, withVss: true },
  { filter: CategoryFilters.Object, destination: `${shadersDir}/sample_surface`, title: 'surface sample', defines: { 'MODE_SURFACESAMPLE': 1 } },
  { filter: CategoryFilters.SampleNormal, destination: `${shadersDir}/sample_normal`, title: 'normal sample', defines: { 'MODE_NORMALSAMPLE': 1 } },
  { filter: CategoryFilters.SampleEmissive, destination: `${shadersDir}/sample_emissive`, title: 'emissive', defines: { 'MODE_EMISSIVESAMPLE': 1 } },
  { filter: CategoryFilters.ColorMask, destination: `${shadersDir}/colormask`, title: 'color mask', defines: { 'MODE_COLORMASK': 1 }, withVss: true },
  { filter: CategoryFilters.ColorMaskSeeThrough, destination: `${shadersDir}/colormask_st`, title: 'color mask, ST', defines: { 'MODE_COLORMASK': 1, 'COLORMARK_SEETHROUGH': 1 }, withVss: true },
];

let gammaFixSkip = [
  'MODE_SIMPLEST',
  'MODE_PUDDLES',
  'MODE_GRASSFX',
  'MODE_EMISSIVESAMPLE',
  'MODE_NORMALSAMPLE',
  'MODE_COLORSAMPLE',
  'MODE_SURFACESAMPLE',
  'MODE_SHADOWS_HEAT',
  'MODE_SHADOWS_ADVANCED',
];
Categories = Categories.concat(...Categories.filter(x => gammaFixSkip.every(y => !x.defines[y])).map(x => Object.assign({}, x, 
  { destination: x.destination + '_l', title: x.title + '+Γ', defines: Object.assign({}, x.defines, { 'GAMMA_FIX': 1 }) })));

let Sources = [
  `custom/**/*`,
  `custom_objects/*`,
  `custom_novariations/*`,
  `recreated/*`
];

let SourcesFilter = [/./];
let SDKFilter = [];

if (!processAll && fs.existsSync(shaderParamsFilename)) {
  const params = $.readText(shaderParamsFilename).split('\n').map(x => x.replace(/\s*([#;].*)?$|^\s*/g, '')).filter(x => x);
  const paramsArgs = params.filter(x => /^--/.test(x));
  const paramsSkip = params.map(x => /^skip\s*:\s*(.+)/i.test(x) ? RegExp.$1 : null).filter(x => x).map(β.toRegExp);
  const paramsProcessOnly = params.map(x => /^process\s+only\s*:\s*(.+)/i.test(x) ? RegExp.$1 : null).filter(x => x).map(β.toRegExp);
  SDKFilter = params.map(x => /^sdk\s*:\s*(.+)/i.test(x) ? RegExp.$1 : null).filter(x => x).map(β.toRegExp);
  const paramsInclude = params.filter(x => /^(?!skip|sdk|--)/i.test(x)).map(β.toRegExp);
  if (paramsArgs.indexOf('--force') !== -1) forceCompile = true;
  if (paramsArgs.indexOf('--verbose') !== -1) verboseRun = true;
  if (paramsArgs.indexOf('--single-thread') !== -1) concurrency = 1;
  if (paramsArgs.indexOf('--use-dxc') !== -1) useDxcForAll = true;
  Categories = Categories.filter(x => !paramsSkip.some(y => y.test(x.title)) && (!paramsProcessOnly.length || paramsProcessOnly.some(y => y.test(x.title))));
  SourcesFilter = paramsInclude;
}

// concurrency = 1;

let sdkDirsCreated = false;
async function buildSDKShader(name, vertexFile, pixelFile) {
  if (sdkDirsCreated === 0) return;

  let BinaryFile, md;
  try {
    BinaryFile = require('binary-file');
    md = require('markdown-it')();
  } catch (e){
    sdkDirsCreated = 0;
    $.echo(β.yellow('To prepare SDK shaders, first install `binary-file` and `markdown-it`'));
    return;
  }

  if (!sdkDirsCreated) {
    sdkDirsCreated = true;
    $.silent(() => $.rm('-r', `${sdkShadersDir}/old/*`, `${sdkShadersDir}/new/*`));
    $.mkdir('-p', `${sdkShadersDir}/old/html`, `${sdkShadersDir}/new/html`, `${sdkShadersDir}/new/win`);
  }

  const docsFile = `${source}/docs/${name}.md`;
  if (fs.existsSync(docsFile)) {
    $.mkdir(`${sdkShadersDir}/old/html/${name}`);
    fs.writeFileSync(`${sdkShadersDir}/old/html/${name}/${name}.html`,
      `<style>body{font-family:sans-serif}code{font-family:monospace}</style><h1>${name}</h1>${md.render('' + await fs.promises.readFile(docsFile))}`);
    $.mkdir(`${sdkShadersDir}/new/html/${name}`);
    fs.writeFileSync(`${sdkShadersDir}/new/html/${name}/${name}.html`,
      `<style>body{font-family:sans-serif}code{font-family:monospace}</style><h1>${name}</h1>${md.render('' + await fs.promises.readFile(docsFile))}`);
  }

  $.cp(vertexFile, `${sdkShadersDir}/new/win/${path.basename(vertexFile)}`);
  $.cp(pixelFile, `${sdkShadersDir}/new/win/${path.basename(pixelFile)}`);

  const metaFile = `${acShadersDir}/meta/${name}_meta.ini`;
  if (acShadersDir && fs.existsSync(metaFile)) {
    $.cp(metaFile, `${sdkShadersDir}/new/win/${name}_meta.ini`);
  } else {
    await fs.promises.writeFile(metaFile, `[METADATA]\nALPHATEST=${/AT/.test(name) ? 1 : 0}\nSKINNED=0\nPARTICLE=0\n2D=0\n`)
  }

  let vertexData = await fs.promises.readFile(vertexFile);
  let pixelData = await fs.promises.readFile(pixelFile);

  let shaderBinNew = new BinaryFile(`${sdkShadersDir}/new/${name}.shader`, 'w', true);
  await shaderBinNew.open();
  await shaderBinNew.writeUInt8(2);
  await shaderBinNew.writeUInt8(0);
  await shaderBinNew.writeUInt32(0);
  await shaderBinNew.writeUInt32(vertexData.length);
  await shaderBinNew.write(vertexData);
  await shaderBinNew.writeUInt32(pixelData.length);
  await shaderBinNew.write(pixelData);
  await shaderBinNew.close();

  let shaderBinOld = new BinaryFile(`${sdkShadersDir}/old/${name}.shader`, 'w', true);
  await shaderBinOld.open();
  await shaderBinOld.writeUInt16(0);
  await shaderBinOld.writeUInt8(0);
  await shaderBinOld.writeUInt32(vertexData.length);
  await shaderBinOld.write(vertexData);
  await shaderBinOld.writeUInt32(pixelData.length);
  await shaderBinOld.write(pixelData);
  await shaderBinOld.writeUInt32(0);
  await shaderBinOld.close();
}

// Preparing directories:
$.mkdir('-p', [source, temporary].concat(Categories.map(x => x.destination)));
if (acShadersDir) {
  $.mkdir('-p', [source, temporary].concat(Categories.filter(x => !x.skipCopying).map(x => x.destination.replace(shadersDir, acShadersDir))));
}

// Utils:
function cutName(n) {
  return n
    .replace(/PerPixel(?!_[vp]s)/, 'P…')
    .replace(/multisampling(?=_)/, 'ms…')
    .replace(/Windscreen(?!_[vp]s)/, 'W…n')
    .replace(/wipers(?!_[vp]s)/, 'w…s')
    .replace(/animated(?!_[vp]s)/, 'anim…')
    .replace(/exterior(?=_)/, 'ext…')
    .replace(/MultiMap(?!_[vp]s)/, 'M…');
}

function guessTarget(basename, strict, isVss) {
  if (/_cs(_[a-z_]+)?\.fxo$/.test(basename)) return FX.Target.CS;
  if (/_gs(_[a-z_]+)?\.fxo$/.test(basename)) return FX.Target.GS;
  if (/_ds(_[a-z_]+)?\.fxo$/.test(basename)) return FX.Target.DS;
  if (/_hs(_[a-z_]+)?\.fxo$/.test(basename)) return FX.Target.HS;
  if (/_ps(_[a-z_]+)?\.fxo$/.test(basename)) return FX.Target.PS;
  if (/_vs(_[a-z_]+)?\.fxo$/.test(basename)) return FX.Target.VS;
  if (/_vss(_[a-z_]+)?\.fxo$/.test(basename)) { isVss[0] = true; return FX.Target.VS; }
  if (/_ps_fastColor/.test(basename)) return FX.Target.PS;
  if (strict) $.fail(`couldn’t figure out target of ${basename}`);
  return FX.Target.PS;
}

function makeFxcOutputNicer(m) {
  return m.trim()
    .replace(/[\n\r]+/g, '\n')
    .replace(/compilation failed; no code produced/, '')
    .replace(/([A-Z]:\\.+\.fx)\((\d+),(\d+).*\):/g, (_, f, l, c) => `${f.substr(source.length + 1)}:${l}:${c}:`);
}

const SearchDirs = [
  `${source}/custom`,
  `${source}/custom_objects/common`,
  `${source}/recreated`
];

const generateVss = listFilter('VSS')

const findFile = (function (dir, file, ctx) {
  if (/\.h$/.test(file)) return null;
  if (!fs.existsSync(`${dir}/${file}`)) {
    for (let searchDir of SearchDirs) {
      if (fs.existsSync(`${searchDir}/${file}`)) {
        return `${searchDir}/${file}`;
      }
    }
    $.echo(β.red(`File might be missing: ${dir}/${file} (${ctx})`));
    return null;
  }
  return `${dir}/${file}`;
}).bind({});

const lastModified = (function (s, f) {
  if (!s) return 0;
  const k = path.normalize(s).toLowerCase();
  if (k in this) return this[k];
  let r = 0;
  try {
    if (!fs.existsSync(s) && f) {
      return lastModified(f);
    }
    r = +fs.statSync(s).mtime;
    if (path.extIs(s, '.fx', '.hlsl')) {
      $.readText(s).replace(/#include\s*"(.+)"/g, (_, v) => {
        r = Math.max(r, lastModified(findFile(path.dirname(s), v, s), `${source}/recreated/${v}`));
      });
    }
  } catch (e) { }
  return this[k] = r;
}).bind({});

// Simple queue:
let queue = (() => {
  let list = [];

  function addScript(filename) {
    if (!path.extIs(filename, '.js')) $.fail(`wrong extension for a script of “${filename}”`);
    let scriptData = $.readText(filename);
    if (/\/\/|\/\*/.test(scriptData)) $.echo(`comment detected in ${filename}`);
    $.pushd(path.dirname(filename));
    try {
      let r = eval(`(${scriptData});`);
      if (r === undefined) throw new Error('SyntaxError');
      addFromScript(r);
    } catch (e) {
      if (!/SyntaxError/.test(e)) $.fail(e);
      addFromScript(eval(`(function (add){ ${scriptData};\n})`)(addFromScript));
    }
    $.popd();
    function addFromScript(v, o) {
      if (!v) return;
      if (Array.isArray(v)) v.forEach(x => addFx(x, o, filename));
      else addFx(v, o, filename);
    }
  }

  function addFx(filename, opts, origin) {
    if (/_vs(?:_.*)?\.fx$/.test(filename) && (!opts || !opts.vssMode) && generateVss.test(filename) 
      && !/tessellation/.test(filename)){
      if (false && /_taa/.test(filename)) {
        $.echo('No VSS: ' + filename);
      } else {
        addFx(filename, { saveAs: path.basename(opts.saveAs || filename).replace(/_vs(?=[._])/, '_vss').replace(/\.fx$/, '.fxo'), vssMode: true });
      }
    }

    if (typeof filename == 'object') [filename, opts] = [filename.source, filename];
    else if (typeof opts !== 'object') opts = null;
    filename = path.resolve(filename);
    opts = Object.assign({ strict: opts != null, origin: origin || filename }, opts);
    let any = false;
    for (const c of Categories) {
      if (opts.vssMode && !c.withVss) {
        continue;
      }
      if (c.filter.test(filename.substr(source.length + 1))) {
        list.push(Object.assign(
          Object.assign({ category: c, origin: filename }, opts),
          { source: filename, defines: Object.assign(Object.assign({}, c.defines), opts.defines) }));
        any = true;
      }
    }
    if (!any && verboseRun) $.echo(β.yellow(`No suitable categories: ${filename}`));
  }

  function add(filename, opts) {
    if (!fs.existsSync(filename)) $.fail(`file “${filename}” is missing`);
    else if (path.extIs(filename, '.js')) addScript(filename, opts);
    else if (path.extIs(filename, '.fx')) addFx(filename, opts);
  }

  return { get: list, add: add, size: () => list.length };
})();

// Main compilation function:
let queueTotal = 0, queueActualTotal = 0;
let compiledTotal = 0;
let compiledInstructionsTotal = 0;
let queueStart = 0;
let progressShown = false;
let compiledInfo = { list: [], size: 0, time: 0, warnings: [], compiled: 0 };
let showHeader = () => {
  $.echo(β.invert(`  ${'Shader'.padEnd(30)}       Category            Target      Origin${' '.repeat(25)}Instructions    Compile time   Compiled size  `));
  showHeader = () => { };
};

function prefix(clearFull) {
  let ret = progressShown ? (clearFull ? '[1A[s' + new Array(120).join(' ') + '[u' : '[1A') : '';
  progressShown = false;
  return ret;
}

function readableTime(seconds) {
  if (seconds > 60) {
    return (seconds / 60).toFixed(1) + ' min';
  } else {
    return seconds.toFixed(0) + ' s';
  }
}

async function mirrorMetaFolder(){
  $.mkdir('-p', `${acShadersDir}/meta`);
  const e = await fs.promises.readdir(`${acShadersDir}/meta`);
  for (let s of await fs.promises.readdir(`${source}/meta`)){
    if (!/\.ini$/.test(s)) continue;
    const d = `${acShadersDir}/meta/${s}`;
    if (!e.includes(d) || (await fs.promises.stat(`${source}/meta/${s}`)).mtimeMs > (await fs.promises.stat(d)).mtimeMs){
      $.cp(`${source}/meta/${s}`, d);
    }
  }
}

if (acShadersDir && processAll){
  mirrorMetaFolder().then(() => {}, r => { $.warn(r); });
}

let maxInstructions = 0;
async function compileShader(opts) {
  const start = process.hrtime();
  const destination = opts.saveAs || `${path.basename(opts.source).replace(/\.\w+$/, '')}.fxo`;
  const finalLocation = `${opts.category.destination}/${destination}`;
  const name = cutName(destination.replace(/\.fxo$/, ''));
  const displayCategory = opts.category.title.padEnd(19);
  const displayOrigin = cutName(path.basename(opts.origin));
  const isVss = [false];
  const target = opts.target || guessTarget(destination, opts.strict, isVss);

  const id = finalLocation;
  const index = compiledInfo.list.length + (Math.random() * 1e9 | 0).toString(32);
  if (compiledInfo.list.indexOf(id) !== -1) $.fail(`Shader ${id} (${opts.saveAs}) already was compiled`);
  compiledInfo.list.push(id);

  if (!forceCompile && lastModified(finalLocation) > Math.max(lastModified(opts.source), lastModified(opts.origin))) {
    if (verboseRun) {
      showHeader();
      $.echo(`${prefix()}  ${name.padEnd(36)} ${displayCategory} ${target.padEnd(11)} ${displayOrigin.padEnd(35)} ${β.grey('skipped' + [13, 15, 0].map(` `.repeat.bind(` `)).join('?'))}`);
    }
    queueTotal--;
    return;
  }

  showHeader();
  const sourceData = $.cleanText(await fs.promises.readFile(opts.source));
  const useAlternativeCompiler = /\/\/\s*use alternative compiler\s*:\s*y/gi.test(sourceData);

  let localDefines = {
    [`TARGET_${target[0].toUpperCase()}S`]: 1
  };

  if (localDefines.TARGET_GS){
    localDefines.TARGET_VS = 1;
  }

  if (isVss[0]){
    localDefines.USE_SPS = 1;
  }

  let subtargetAccumulate = '';
  for (let subtarget of (name.split(/_[cdhpv]s_/)[1] || '').split('_')) {
    if (!subtarget) continue;
    subtargetAccumulate = (subtargetAccumulate ? subtargetAccumulate + '_' : '') + subtarget;
    localDefines[`SUBTARGET_${subtargetAccumulate.toUpperCase()}`] = 1;
  }

  const useDxc = useDxcForAll || /\/\/\s*use dxc\s*:\s*y/gi.test(sourceData);
  const compileTarget = /\/\/\s*override target\s*:\s*(\S+)/gi.test(sourceData) ? RegExp.$1 : target;
  const customArgs = [];
  sourceData.replace(/\/\/\s*compile argument\s*:\s*(\S+)/gi, (_, arg) => customArgs.push(arg));

  if (opts.source.includes('\\custom\\')){
    localDefines['CUSTOM_SHADER'] = 1;
  } else {
    localDefines['OBJECT_SHADER'] = 1;
  }

  if (useDxcForAll){
    customArgs.push('-HV', '2016', '-flegacy-macro-expansion');
  }

  let stderr = '';
  let args = useDxc 
  ? [
    '-Ni', '-nologo', '-O3', '-T', compileTarget, '-E', opts.entry || 'main', ...customArgs,
    `-Fo`, `${temporary}/compiled_${index}.fxo`, `-Fc`, `${temporary}/compiled_${index}.txt`, '-I', path.dirname(opts.source), '-I', source,
    { stderrCallback: c => stderr += c, fail: false, cwd: path.dirname(opts.source) }
  ]
    .concat(Object.entries(opts.defines || {}).map(([k, v]) => [`-D`, `${k}=${v}`]))
    .concat(Object.entries(localDefines).map(([k, v]) => [`-D`, `${k}=${v}`]))
    .concat(SearchDirs.map(x => [`-I`, x.replace(/\//g, '\\')]))
    .concat([ opts.source ])
    .flat(Infinity)
    : [
      '/Ni', '/nologo', '/' + (process.env['SHADERS_OPTIMIZATION'] || 'O3'),
      opts.source, '/T', compileTarget, '/E', opts.entry || 'main', ...customArgs,
      `/Fo${temporary}/compiled_${index}.fxo`, `/Fc${temporary}/compiled_${index}.txt`,
      { stderrCallback: c => stderr += c, fail: false }]
      .concat(Object.entries(opts.defines || {}).map(([k, v]) => `/D${k}=${v}`))
      .concat(Object.entries(localDefines).map(([k, v]) => `/D${k}=${v}`))
      .concat(SearchDirs.map(x => `/I${x}`));

  if (opts.category.title === 'custom'){
    args.push(useDxc ? '-Qstrip_reflect' : '/Qstrip_reflect');
  }

  if (useDxc){
    $.echo(args);
  }
  
  if (!await (useDxc ? $dxc : useAlternativeCompiler ? $fxcAlt : $fxc).apply($fxc, args)) {
    $.fail(`compilation of ${name} (${displayCategory.trim()}) failed\n  full path: ${opts.source}\n${makeFxcOutputNicer(stderr).tab(2)}`);
  }

  $.mv(`${temporary}/compiled_${index}.fxo`, finalLocation);
  if (acShadersDir && !opts.category.skipCopying) {
    // $.cp(finalLocation, finalLocation.replace(shadersDir, acShadersDir));
  }

  sourceData.replace(/\/\/\s*alias\s*:\s*(\w+)(?:>(\w+))?/gi, (_, v, t) => {
    if (t){
      v = path.basename(finalLocation, '.fxo').replace(v, t);
    }

    if (/_vss\.fxo$/.test(finalLocation) && /_vs$/.test(v)){
      v = v + 's';
    }

    if (v === path.basename(finalLocation, '.fxo')) return;
    let nameToTest = `${path.basename(path.dirname(opts.origin))}\\${v}.fx`;
    if (!opts.category.filter.test(nameToTest)) {
      return;
    }

    $.silent(() => $.rm(`${opts.category.destination}/${v}.fxo`));
    fs.linkSync(finalLocation, `${opts.category.destination}/${v}.fxo`);
    if (acShadersDir && !opts.category.skipCopying) {
      $.silent(() => $.rm(`${opts.category.destination.replace(shadersDir, acShadersDir)}/${v}.fxo`));
      $.cp(finalLocation, `${opts.category.destination.replace(shadersDir, acShadersDir)}/${v}.fxo`);
    }
  });

  const info = $.cleanText(await fs.promises.readFile(`${temporary}/compiled_${index}.txt`));
  let maxRegister = 0;
  info.replace(/\br(\d+)\.x\b/g, (_, v) => maxRegister = Math.max(maxRegister, +v));
  const instructions = '' + hlslCost(info);
  const instructionsLine = β[instructions > 300 ? 'red' : instructions > 150 ? 'yellow' : 'green'](instructions.padStart(4) + ':' + ('' + maxRegister).padStart(3));
  const fileSize = fs.statSync(finalLocation).size;
  const displaySize = ((fileSize / 1024).toFixed(1) + ' KB').padStart(15);
  compiledInfo.compiled++;
  compiledInfo.size += fileSize;
  const displayTime = (β.hrToMs(process.hrtime(start)) + ' ms').padStart(15);
  compiledInfo.time += β.hrToSeconds(process.hrtime(start));
  $.echo(`${prefix()}  ${name.padEnd(36)} ${displayCategory} ${compileTarget.padEnd(11)} ${displayOrigin.padEnd(32)} ${instructionsLine} ${displayTime} ${displaySize}`);

  maxInstructions = Math.max(instructions, maxInstructions);
  if (stderr) {
    compiledInfo.warnings.push(makeFxcOutputNicer(stderr));
    $.echo(β.yellow(makeFxcOutputNicer(stderr).tab(4)));
  }

  compiledTotal++;
  compiledInstructionsTotal += +instructions;
  let queueInstructionsTotal = queueTotal * compiledInstructionsTotal / compiledTotal;
  let timePassed = (Date.now() - queueStart) / 1000;
  let speedApproximation = compiledInstructionsTotal / timePassed;

  let progress = compiledTotal / queueTotal;
  let progressBarColor = '200;140;40';
  let progressBarStyleBase = `[38;2;${progressBarColor}m`;
  let progressBarStyleFilled = `[48;2;${progressBarColor}m[38;2;0;0;0m`;
  let progressBar = `${progressBarStyleBase}[${progressBarStyleFilled}`;
  let flipped = false;
  let progressBarText = `${compiledTotal} out of ${queueTotal}, ETA: ${readableTime((queueInstructionsTotal - compiledInstructionsTotal) / speedApproximation)}`;
  let progressBarTextOffset = (100 - progressBarText.length) / 2 | 0;
  for (let i = 0; i < 100; ++i) {
    if (i / 100 > progress && !flipped) {
      flipped = true;
      progressBar += `[0;1m${progressBarStyleBase}`;
    }
    progressBar += progressBarText[i - progressBarTextOffset] || ' ';
  }
  progressBar += `[0;1m${progressBarStyleBase}][0;1m`;
  $.rm(`${temporary}/compiled_${index}.txt`);
  $.echo(progressBar);

  // process.stdout.write(β.grey(`Compiled ${compiledTotal} out of ${queueTotal} shaders (${queueActualTotal} in total), `
  //   + `passed ${timePassed.toFixed(1)} min, `
  //   + `ETA: ${((queueInstructionsTotal - compiledInstructionsTotal) / speedApproximation).toFixed(1)} min\n`));
  progressShown = true;

  if (concurrency == 1) {
    await sleep(100);
  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// List of files to compile
Sources.forEach(x => $.glob(x).filter(y => SourcesFilter.some(z => z.test(path.basename(y)))).forEach(queue.add));

// Running compilation in parallel:
queueTotal = queueActualTotal = queue.size();
queueStart = Date.now();
await queue.get.parallel(compileShader, concurrency);

// Final touches:
if (!compiledInfo.compiled) {
  $.echo('All shaders are up to date (use “--force” to recompile everything)');
  return;
} else if (compiledInfo.warnings.length == 0) {
  $.echo(prefix(true) + β.green('No warnings 👌'));
} else if (compiledInfo.list.length > 20) {
  $.echo(prefix(true) + β.yellow(`Warnings:\n` + compiledInfo.warnings.join('\n').tab(2)));
}

$.silent(() => $.rm(`${temporary}/compiled_*.*`));
β.config.measureTimeCallback = t => `${compiledInfo.list.length} shaders in total, ${compiledInfo.compiled} compiled to ${(compiledInfo.size / 1024).toFixed(2)} KB, took ${t ? readableTime(t) : t}, most complex is ${maxInstructions} instructions`;

if (SDKFilter.length > 0) {
  $.mkdir('-p', sdkShadersDir);

  for (let shader of $.glob(`${shadersDir}/basic/*_ps.fxo`).filter(y => SDKFilter.some(z => z.test(path.basename(y).replace('_ps.fxo', ''))))) {
    await buildSDKShader(path.basename(shader).replace('_ps.fxo', ''), shader.replace('_ps.fxo', '_vs.fxo'), shader);
  }

  await $.zip([
    { key: 'MODS/CSP Original AC Patch/system/shaders/win', dir: `${sdkShadersDir}/new/win` },
    { key: 'MODS/CSP Original AC Patch/Description.jsgme', data: `Some custom shaders from CSP for original AC` },
  ], {
    to: `${sdkShadersDir}/ac-new.zip`, comment: `Some of custom shaders from Custom Shaders Patch compiled for original AC.

New shaders:
- ${$.glob(`${sdkShadersDir}/new/*.shader`).map(x => path.basename(x, '.shader')).join(';\n- ')}.`
  });
  await $.zip([
    { key: 'MODS/CSP New SDK Patch/sdk/editor/system/shaders', dir: `${sdkShadersDir}/new`, rec: true },
    { key: 'MODS/CSP New SDK Patch/system/csp-sdk.txt', data: `CSP Old SDK Patch is applied` },
    { key: 'MODS/CSP New SDK Patch/Description.jsgme', data: `Some custom shaders from CSP for AC SDK (newer)` },
  ], {
    to: `${sdkShadersDir}/sdk-new.zip`, comment: `Some of custom shaders from Custom Shaders Patch compiled for AC SDK (ksEditor).
Require newer AC and ksEditor (with grey/colored background, after 1.2.2 update).

New shaders:
- ${$.glob(`${sdkShadersDir}/new/*.shader`).map(x => path.basename(x, '.shader')).join(';\n- ')}.`
  });
  await $.zip([
    { key: 'MODS/CSP Old SDK Patch/sdk/editor/system/shaders', dir: `${sdkShadersDir}/old`, rec: true },
    { key: 'MODS/CSP Old SDK Patch/system/csp-sdk.txt', data: `CSP Old SDK Patch is applied` },
    { key: 'MODS/CSP Old SDK Patch/Description.jsgme', data: `Some custom shaders from CSP for AC SDK (older)` },
  ], {
    to: `${sdkShadersDir}/sdk-old.zip`, comment: `Some of custom shaders from Custom Shaders Patch compiled for AC SDK (ksEditor).
Require older ksEditor version (with white background, before 1.2.2 update).

New shaders:
- ${$.glob(`${sdkShadersDir}/old/*.shader`).map(x => path.basename(x, '.shader')).join(';\n- ')}.`
  });
}