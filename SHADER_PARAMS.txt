# ---
# Options:
# ---

# --force
# --verbose
# --single-thread
# --auto-mt
# --use-dxc

# ---
# Categories to skip:
# Masks are allowed
# ---

# Skip: custom
# Skip: basic
# Skip: G-buf*
# Skip: main*
# Skip: *-S
# Skip: *, FS*
# Skip: simpler*
# Skip: simplest
# Skip: color sample
# Skip: grassFX
# Skip: light map
# Skip: we*
# Skip: wet, FS
# Skip: wet, DS
# Skip: wet, WS
# Skip: wet, PS
# Skip: wet, G*

Process only: *
# Process only: *Γ
# Process only: main, FX
# Process only: main, FX+Γ
# Process only: main, FS+Γ
# Process only: simpler, FX+Γ
# Process only: main, NL+Γ
# Process only: custom
Process only: custom+Γ
# Process only: custom, NV
# Process only: basic
# Process only: normal sample
# Process only: surface sample
# Process only: *FS*
# Process only: G-buf*
# Process only: G-buf*R
# Process only: G-buf., R+A
# Process only: G-buf., R
# Process only: G-buf., A
# Process only: G-buf.
# Process only: G-buf.+Γ
# Process only: color mask
# Process only: color sample
# Process only: emissive
# Process only: grassFX
# Process only: grass*
# Process only: light map
# Process only: light map, FS
# Process only: main*
# Process only: main, FS
# Process only: shadows
# Process only: windscreen, FX
# Process only: windscreen, FS
# Process only: *, FX
# Process only: main, *
# Process only: main, NL
# Process only: simpler, FX
# Process only: simpler, FX+Γ
# Process only: simpler+Γ
# Process only: simpler, wet+Γ
# Process only: simpler*
# Process only: main*
# Process only: simplest
# Process only: puddles
# Process only: shadows
# Process only: shadows heat
# Process only: we*
# Process only: wet
# Process only: wet, *
# Process only: wet, FX
# Process only: wet, FX+Γ
# Process only: wet, FX
# Process only: wet, DS
# Process only: wet, DX
# Process only: wet, DX+Γ
# Process only: wet, G*
# Process only: wet, G, R
# Process only: wet, G, R+A
# Process only: wet, P+G*
# Process only: wet, P./
# Process only: wet, PX
# Process only: simpler, wet
# Process only: wet, PS
# Process only: wet, PS+Γ
# Process only: wet, PX
# Process only: wet, PX+Γ
# Process only: wet, PX
# Process only: wet, P*
# Process only: wet, WM
# Process only: wet, WS
# Process only: wet, WX

# ---
# Shaders to prepare for SDK (ksEditor):
# ---
# SDK: st*
# SDK: ks*emissive
# SDK: ksMultilayer_fresnel_nm4
# SDK: ksMultilayer_objsp_nm4

# ---
# Shaders to compile:
# Comment “*” out to limit which shaders will be recompiled, to speed things up
# ---

# *Skinned*
# ksTree_ps*
# ksTreeFX_vs*
# ksTree_vs*
accHe*
GL*
# ksPerPixelMultiMap_emissive_ps.fx
# nePerPixel_light*
# ksPerPixel_ps.fx
# wfx*
# pfx*
# ks*
# accC*
# ks*
# ksPerPixelMultiMap_ps.fx
# ksPerPixelMultiMap_emissive_ps.fx
# ksTy*
# flg*
# ins*
# dcl*
# ksSkinnedMesh_vs.fx
# ui*
# accSSLR_new_simple_ps.fx
# accSSLR_filter_ps.fx
# accSSLR*
# acc*
# *
# ne*
# wfx*
# ks*
# acc*
# ins*
# ksPerPixel*
# ksMultilayer_fresnel_nm_ps.fx
# flg*
# pfx*
# acc*
# ksTreeFX_vs.fx
# accSnow*
# *
# smCarPaint_ps.fx
# accRain*
# ks*
# ks*
# smTyreMarks*
# pfx*
# *
# ksPerPixel_ps.fx
# acc*IBL*
# ksMultilayer_fresnel_nm_ps.fx
# ksPerPixelMultiMap_vs.fx
# ksPerPixelNM_vs.fx
# pfx*
# accR*
# # ins*
# flg*
# # flgTree*shadow*fx
# flgTree.js
# flgTree_bb_array_shadow_ps.fx
# flgTree_bb_array_shadow_vs.fx
flgTree_bb_array_mainobj_a2c_ps.fx
# ks*
# smT*
# dcl*
# pfx*
# ksTree*
# ksSkinnedMesh_vs.fx
# flg*
# smDigitalScreen_ps.fx
# ksTyresFX_ps.fx
# ksTyresFX_vs.fx
# flgGrass_ps.fx
# flgGrass_ps_noss.fx
# flgGrass_vs.fx
# flgGrass_cs_generation.fx
# smDigitalScreen_ps.fx
# tf*
# ui*
# ks*
# *
# accFakeCarShadows_ps_fx_reprojection.fx
# acc*
# ksWindscreenFX_new_ps.fx
# ac*
# dcl*
# smTrackLines*
# accCopyBlurred_ps.fx
# smWater*
# ksMultilayer_fresnel_nm*
# pfx*
# accDriverTag*
# utlT*
# wfxSkyV2_ps_quality.fx
# *
# accCopy_ps.fx
# pfxSmoke_ps.fx
# pfxSmoke_cs*
# pfxSmoke_vs.fx
# ks*
# ksPerPixelAT_ps.fx
# flgTree_model_mainobj_ps.fx
# flgTree_model_mainobj_a2c_ps.fx
# accSSLR*
# accFakeCarShadows_ps_gbuffer.fx
# flgGrass_cs*
# ksPerPixelMultiMap_NMDetail_ps.fx
# ksMultilayer_fresnel_nm_ps.fx
# flgGrass_vs.fx
# accWind_ps.fx
# accSSLR*
# accRain*
# ksFlags*
# ksMultilayer_fresnel_nm_ps.fx
# *
# utl*
# pfxSmoke_ps
# fuPBR_multilayer_parallax2_ps.fx
# nePBR_MultiMap_Cloth_ps.fx
# nePBR_MultiMap_NMDetail_skinned_vs.fx
# ksTyresFX_ps.fx
# accFakeCar*
# smHumanSurface_ps.fx
# accFakeCarShadows_ps_gbuffer.fx
# nePerPixelMultiMap_bending_*
# nePerPixelMultiMap_tes*
# fuPBR_ps.fx
# smRef*
# smRefractingCover_taa_ps.fx
# wfx*
# flgG*vs*
# flgG*
# ksPerPixelMultiMap_emissive_ps.fx
# ksPerPixelMultiMap_NMDetail_emissive_ps.fx
# nePerPixelReflection_heating*
# nePerPixel_heating*
# nePerPixelMultiMap_heating_ps.fx
# acc*
# wfxEarth_ps.fx
# accSSLR*
# flgGrass_vs.fx
# ksPerPixelMultiMap_ps.fx
# accTrackFakeShadow_vs.fx
# dcl*
# dclSkidmarks_ps_main.js
# ksPerPixel_ps.fx
# ksFakeCarShadows_*
# dclSkidmarks_vs_mirror.fx
# accFa*
# dclSkidmarks_ps_mirror.fx
# dcl*_cs*.fx
# ksPerPixelMultiMap_NMDetail_ps.fx
# * 
# flgGrass_cs_generation.fx
# flgGrass_ps_noss.fx
# flgGrass_*vs*
# smTyre*
# flgTree_bb_sides_fake_shadow_vs.fx
# flgTree_bb_array_mainobj_a2c_ps.fx
# flgTree_model_mainobj_a2c_ps.fx
# accDriverTag_ps.fx
# ksTyresFX_vs.fx
# ksPerPixel_ps.fx
# ksFake*
# accB*
# ksSkinnedMesh_toon_ps.fx
# smTyreMarks_*
# dcl*
# *
# accSSLR*
# accM*
# fuPBR_ps.fx
# fuPBR_parallax_ps.fx
# ksPerPixel_ps.fx
# flg*
# fuPBR*
# *
# ksMultilayer_fresnel_nm_ps.fx
# ksPerPixelMultiMap_ps.fx
# accRes*
# ksPerPixel_ps.fx
# smGlass*
# fx*
# msaa.js
# dcl*
# flg*
# ksPerPixelMultiMap_vs.fx
# ksPerPixelMultiMap_damage_dirt_ps.fx
# ksMultilayer_fresnel_nm_ps.fx
# smGlass_ps.fx
# accBlur*
# smWaterSurface_ps.fx
# flgGrass_ps_noss.fx
# flgGrass_cs_generation.fx
# wfxEarth_ps.fx
# pfxSparks*
# pfxFl*
# ksTyresFX*
# accV*
# ksPerPixel_ps.fx
# pfxP*
# ksMultilayer_fresnel_nm_vs.fx
# ksMultilayer_fresnel_nm_ps.fx
# ksPerPixelAT_NM_ps.fx
# nePBR_MultiMap_NMDetail_vs.fx
# nePBR_MultiMap_NMDetail_ps.fx
# smCarPaint_vs.fx
# smCarPaint_ps.fx
# *
# smGlass_ps.fx
# accBlur*
# neP*
# accCopy*
# flgTree_bb_array_mainobj_a2c_ps.fx
# flg*
# ksPar*
# ksBr*
# wfx*
# ksTree*
# nePerPixel_light_glowDot_*
# nePerPixel_light_glow_*
# ksPerPixelMultiMap_NMDetail*
# acc*
# pfx*
# fw*
# accFakeCar*
# ksMultilayer_fresnel_nm_ps.fx
# accTrack*
# flgGra*
# ksGrass*
# nePerPixelMultiMap_digitalScreen*
# nePBR_MultiMap_NMDetail_ps.fx
# tf*
# accRes*
# msaa.js
# smDig*
# wfxSkyV2_ps_quality.fx
# wfxClou*
# ksPer*
# accF*
# accS*
# wfxMoon*
# wfxPla*
# wfxEar*
# nePerPixel_light*
# ks*
# ksPerPixel_ps.fx
# ksPerPixel_vs.fx
# ksPerPixel_ps.fx
# ksPerPixelMultiMap_emissive_ps.fx
# ksTree_ppshadows_ps.fx
# ksTreeFX_ps.fx
# *
# ksPerPixel_vs.fx
# ksPerPixel_ps.fx
# ksPerPixelMultiMap_AT_ps.fx
# wfx*
# smWaterSurface_ps*
# smRef*
# accText*
# ksPerPixelReflection_ps*
# ksPerPixelAT_NM_emissive_ps*
# fuPBR*
# ksPerPixelNM*
# ksPerPixelAT_NM_emissive_ps*
# ksFo*
# nePBR_MultiMap_NMDetail_ps.fx
# nePBR*
# smGlass*
# nePBR_MultiMap_NMDetail_ps.fx
# ksPerPixelMultiMap_NMDetail_ps.fx
# smCarPaint_ps.fx
# accSSL*
# flgGra*
# nePBR*
# ksPerPixel_ps.fx
# ksPerPixel_vs.fx
# fuPBR_multilayer_parallax2_ps.fx
# smRefractingCover_taa_ps.fx
# smTyre*
# wfx*
# acc*
# pfxSmo*
# ksWin*
# nePBR_MultiMap_Cloth_ps.fx
# ksPerPixelReflection_ps.fx
# accText*
# accTe*
# acc*
# pfxScre*
# nePBR_MultiMap_NMDetail_ps.fx
# *
# accTrack*
# ksPerPixel_ps.fx
# ksPerPixelReflection_ps.fx
# ksMultilayer_fresnel_nm_ps.fx
# smTyreMarks_ps.fx
# fuPBR_multilayer_parallax4_ps.fx
# fuPBR_multilayer_parallax2_ps.fx
# ksPerPixelMultiMap_NMDetail_ps.fx
# accCarLi*
# ksPerPixel_nosdw_ps.fx
# flg*
# ksMultilayer_ps.fx
# flgGrass_cs_generation.fx
# smGlass_ps.fx
# ksWind*
# ksPerPixelNM_ps.fx
# fuPBR_ps.fx
# fuPBR_multilayer_ps.fx
# fuPBR_parallax_ps.fx
# flgG*
# smCarPaint_old_ps.fx
# ksMultilayer_fresnel_nm_ps.fx
# ksMultilayer_fresnel_nm_ps.fx
# smWaterSurface_ps.fx
# ksTree_ps.fx
# ksTreeFX_ps.fx
# nePBR_MultiMap_NMDetail_ps.fx
# ksPerPixel_ps.fx
# smCarPaint_ps.fx
# smGlass_ps.fx
# flgTree_model_mainobj_a2c_ps.fx
# accC*
# pfx*
# accTra*
# accTrue*
# accTrueEmissive_ps.fx
# # flgTree_model_mainobj_ps.fx
# flgTree_bb_array_mainobj_a2c_ps.fx
# ksPerPixelMultiMap_ps.fx
# ksPerPixel_tilingfix_ps.fx
# ksPerPixelMultiMap_ps.fx
# # smCarPaint_old_ps.fx
# smCarPaint_ps.fx
# accHeadlightGlare_ps.fx
# accSS*
# ksTyresFX_ps.fx
# accT*