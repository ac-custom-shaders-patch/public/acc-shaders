## Custom Shaders for Assetto Corsa

[![License: MIT](https://img.shields.io/badge/license-MIT-orange.svg)](https://trello.com/b/xq54vHsX/ac-patch) 
[![More information: https://trello.com/b/xq54vHsX/ac-patch](https://img.shields.io/badge/trello-more%20info-brightgreen.svg)](https://trello.com/b/xq54vHsX/ac-patch) 
[![Join the chat in Discord: https://discord.gg/buxkYNT](https://img.shields.io/badge/discord-join%20chat-brightgreen.svg)](https://discord.gg/W2KQCMH)

This is a part of [Custom Shaders Patch](https://trello.com/b/xq54vHsX/ac-patch), an extension for racing simulator Assetto Corsa adding some small visual enhancements here and there. Some shaders here were recreated by hand to match original ones in look as close as possible, while others are new and used for some extra features like Weather FX.

[![Screenshot by kelnor34](https://i.imgur.com/JnqMoEY.png)](https://i.imgur.com/JnqMoEY.png)

For more information check out [this webpage](https://acstuff.ru/patch/). 

Feel free to create your own shaders based on these. And if you do, please consider sending them back in a pull request: unless shaders are included with Shaders Patch, they might not always work as intended. For example: a new fog algorithm was added with one of Weather FX updates, and after that third-party shaders started to look out-of-place in some conditions.

## Notes

* To compile things, check out [.scripts](https://gitlab.com/ac-custom-shaders-patch/public/acc-shaders/-/blob/master/.scripts) directory and its README.
* Alternatively you can just use [Visual Code](https://code.visualstudio.com/), but don’t forget to verify envinromental variables in [.vscode/tasks.json](https://gitlab.com/ac-custom-shaders-patch/public/acc-shaders/-/blob/master/.vscode/tasks.json).
* Unless `--force` argument is given to [compile.js](https://gitlab.com/ac-custom-shaders-patch/public/acc-shaders/-/blob/master/.build/compile.js), only shaders listed in [SHADER_PARAMS.txt](https://gitlab.com/ac-custom-shaders-patch/public/acc-shaders/-/blob/master/SHADER_PARAMS.txt) will be compiled.
* For CSP to use your freshly compiled shaders instead of ones it’s shipped with, remove “shaders.zip” from “assettocorsa/extension” and recompile all shaders.
* The way things are set, there are currently more than 30 different shader types. Regular object shaders compile into a shader of each of that type (for example, advanced shader for main render pass, simplified for mirrors, shaders with different kinds of RainFX wet effects, shader for surface color and height sampling for GrassFX, etc.)
* Some shaders only compile in few of shader types. For example, car paint shaders can’t have grass on them, so they don’t compile with GrassFX shaders type. [SHADER_LISTS.txt](https://gitlab.com/ac-custom-shaders-patch/public/acc-shaders/-/blob/master/SHADER_LISTS.txt) controls some of associations (and others are set in [compile.js](https://gitlab.com/ac-custom-shaders-patch/public/acc-shaders/-/blob/master/.build/compile.js)).
* To build shaders for ksEditor and original AC, install ”markdown-it” and “binary-file” first (`.scripts/install-all.bat` can do it for you).
* For shaders with skinning, tesellation or fur [a meta file](https://gitlab.com/ac-custom-shaders-patch/public/acc-shaders/-/blob/master/meta/) needed describing shader properties.

## More details

Because of my lack of experience dealing with HLSL shaders the whole thing is an absolute mess. Hopefully in the future I’ll be able to rewrite things, but for now here are some pointers:
- Directory `recreated` contains my attempts to recreate original AC shaders with new additions;
- Those shaders rely heavily on files in `recreated/include_new/…`, which unfortunately have a lot of similar names (what can I say, I thought it was a neat idea at first);
- Dynamic lights are in `recreated/include_new/ext_lightingfx`. And I use exponential shadow maps for those: single sample per pixel is pretty neat.
- File `recreated/include_new/base/_flags.fx` sets a lot of defines based on a current shader type;
- New style is to keep included files in `custom/include`, the one file there called “common.hlsl” is used pretty much by everything now.

Compiling script (`compile.js`) automatically adds macros `TARGET_PS`/`TARGET_VS`/`TARGET_CS`/etc. based on shader type, and shader type is detected automatically based on a file postfix. Only “.fx” files are compiled, so “.hlsl” is a good postfix for files that are being included. Oh, and “.js” files will be evaluated as well, allowing to create a bunch of configurations with ease (for example, check out [cmaa.js](https://gitlab.com/ac-custom-shaders-patch/public/acc-shaders/-/blob/master/custom/ppaa/cmaa/cmaa.js)).

## Shader types

- Regular shaders:
  - `main_fx`/`main_fs`: main render pass without and with dynamic shadows. 
  - `main_nolighting`: variants of a few main render pass shaders without dynamic lights (for faster trees render). 
  - `gbuff_nop`/`gbuff_nat`: G-buffer pass without screen-space reflections, opaque and alpha testing version. 
  - `gbuff_rop`/`gbuff_rat`: G-buffer pass with screen-space reflections, opaque and alpha testing version. 
  - `simplified_fx`/`simplified_nofx`: shaders for mirrors and reflection cubemap, with and without dynamic lights.

- Rain shaders:
  - `wet_fx`/`wet_fs`: wet shaders (a bit of wet darkening).
  - `wet_fx_wipermask`/`wet_fs_wipermask`: wet with wipers traces. 
  - `wed_fx`/`wed_fs`: version with raindrops. 
  - `wep_fx`/`wep_fs`: version with puddles. 
  - `wep_gbuff_nop`/`wep_gbuff_nat`: G-buffer pass without screen-space reflections, opaque and alpha testing version.
  - `wep_gbuff_rop`/`wep_gbuff_rat`: G-buffer pass with screen-space reflections, opaque and alpha testing version.
  - `simplified_wet`: wet shaders for mirrors and cubemap (with dynamic lights only for now).

- Special overlays:
  - `colormask`: shader drawn before regular semi-transparent colored glass, finds out solid color for multiply blending.

- Extra passes:
  - `colormask_st`: semi-transparent colored glass used by colored shadows and ambient module.
  - `fxgrass`: GrassFX ground height, normals and color sampling.
  - `lightmap`/`lightmap_fs`: used by bounced light of Extra FX, version without and with dynamic shadows.
  - `puddles`: generates puddle map from laserscan meshes.
  - `sample_color`: samples surface color for, for example, estimating bits color for particles on collision.
  - `sample_emissive`: samples emissive color for preparing maps for refracting headlights.
  - `sample_normal`: samples normals for aligning trees lighting along surface or for making a shot of tyre pattern for Skidmarks FX.
  - `sample_surface`: samples the whole surface (color, normals, shape) for light bouncing off car.
  - `shadows_advanced`: advanced shaders for sun shadows (for complex objects which can’t use generic ones, like flags or spectators). 
  - `shadows_heat`: shaders for sampling shadowed state for shadows affecting surface temperature used by physics tyre model.
  - `simplest`: simplest shading. For example, used by local car cubemaps.

- Custom shaders:
  - `custom`: anything unrelated to original AC meshes, from FXAA to IMGUI shaders.
  - `custom_nv`: shaders taking AC meshes as input, but drawing them very customly (“nv” stands for “no variation”).

## Credits

* First of all, of course, we’d first like to thank the Kunos developers for all of the effort they put into making AC such a great simulator and giving us an excellent basis for our work.
* Clouds noise texture based [on demo](https://www.shadertoy.com/view/3dVXDc) by piyushslayer;
* GPU-accelerated particles based [on this article](https://wickedengine.net/2017/11/07/gpu-based-particle-simulation/) by turanszkij;
* Grass idea [from GRID](http://blog.codemasters.com/wp-content/uploads/2014/10/Rendering-Fields-of-Grass-using-DirectX11-in-GRID-Autosport.pdf) by Richard Kettlewell (Codemasters);
* Lighting algorithm based on [a great idea](https://worldoffries.wordpress.com/2015/02/19/simple-alternative-to-clustered-shading-for-thousands-of-lights/) by Luke Mamacos;
* Old sky shader forked from [this shader](https://www.shadertoy.com/view/Ml2cWG) made by robobo1221.

## More credits

* ASSAO by Intel;
* CMAA2 by Intel;
* FidelityFX by AMD;
* FXAA by NVIDIA;
* HBAO+ by NVIDIA;
* SMAA.